#include "transformation.h"
#include "maestro/logging.h"
#include "attributes/maestro-schema.h"
#include <inttypes.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_TRANSF,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_TRANSF,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_TRANSF,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_TRANSF,__VA_ARGS__)



/** A transformation function will check its arguments and try to
 * perform the necessary transformation.
 *
 * It return MSTRO_OK if it managed to transform things properly. It
 * will return MSTRO_NOMATCH if it is not applicable to the requested
 * transformation. Other return codes designate an error.
 *
 * Only the first successful transformation will be executed, as
 * otherwise we'd need to record incremental attribute changes.
 */
/** a function prototype to check whether a transformation is applicable */
typedef mstro_status(*mstro_transformation)(
    mstro_cdo src, mstro_cdo dst,
    mstro_attribute_dict src_attr, mstro_attribute_dict dst_attr);


/** Number of built-in transformations */
#define NUM_BUILTIN_TRANSFORMATIONS 1

/** Table of supported transformations */
static struct {
  /** transformation name */
  const char *name;
  /** operation */
  mstro_transformation transformation;
  
} g_builtin_transformations[NUM_BUILTIN_TRANSFORMATIONS] = {
  { "Transposition", mstro_transform_transpose }
};
  


mstro_status
mstro_transform_transpose(
    mstro_cdo src,
    mstro_cdo dst,
    mstro_attribute_dict src_attr,
    mstro_attribute_dict dst_attr)
{
  mstro_status s1,s2;
  mmbArray *m_src=NULL, *m_dst=NULL;

  if(src==NULL || dst==NULL ||src_attr==NULL || dst_attr==NULL) {
    ERR("Invalid arguments: %p, %p, %p, %p\n",
        src, dst, src_attr, dst_attr);
    return MSTRO_INVARG;
  }
  if(src_attr==dst_attr) {
    /* nothing to be done */
    return MSTRO_OK;
  }
  if(src==dst) {
    ERR("FIXME: in-place transposition unimplemented\n");
    return MSTRO_UNIMPL;
  }

  m_src = src->mamba_array;
  m_dst = dst->mamba_array;
  if(m_src==NULL || m_dst==NULL) {
    ERR("SRC or DST missing mamba array\n");
    return MSTRO_FAIL;
  }

  /* check applicability */
  /* Fetch layout attributes */
  const void *src_ndims, *src_patt, *src_localsz, *src_elsz, *src_dimsz;
  const void *dst_ndims, *dst_patt, *dst_localsz, *dst_elsz, *dst_dimsz;
  enum mstro_cdo_attr_value_type vt=MSTRO_CDO_ATTR_VALUE_INVALID;

  s1 = mstro_attribute_dict_get(src_attr,
                                MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                NULL, &src_localsz, NULL, false);
  s2 = mstro_attribute_dict_get(dst_attr,
                                MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                NULL, &dst_localsz, NULL, false);
  if(s1!=MSTRO_OK || s2!=MSTRO_OK) {
    DEBUG("No %s attribute on src or dst\n",
          MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE);
    return MSTRO_NOMATCH;
  }
  
  int64_t src_size =*(int64_t*)src_localsz,
      dst_size = *(int64_t*)dst_localsz;

  if(src_size!=dst_size) {
    DEBUG("Cannot handle differing local-size (%" PRIi64 " src vs %" PRIi64 " dst)\n",
          src_size, dst_size);
    return MSTRO_NOMATCH;
  }

  
  s1 = mstro_attribute_dict_get(src_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE,
                                NULL, &src_elsz, NULL, false);
  s2 = mstro_attribute_dict_get(dst->attributes,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE,
                                NULL, &dst_elsz, NULL, false);
  if(s1!=MSTRO_OK || s2!=MSTRO_OK) {
    DEBUG("No %s attribute on src or dst\n",
          MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE);
    return MSTRO_NOMATCH;
  }
  
  int64_t src_eltsize =*(int64_t*)src_elsz,
      dst_eltsize = *(int64_t*)dst_elsz;
  
  if(src_eltsize!=dst_eltsize) {
    DEBUG("Cannot handle differing element-size (%" PRIi64 " src vs %" PRIi64 " dst)\n",
          src_eltsize, dst_eltsize);
    return MSTRO_NOMATCH;
  }
  

  s1 = mstro_attribute_dict_get(src_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS,
                                NULL, &src_ndims, NULL, false);
  s2 = mstro_attribute_dict_get(dst_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS,
                                NULL, &dst_ndims, NULL, false);
  if(s1!=MSTRO_OK || s2!=MSTRO_OK) {
    DEBUG("No %s attribute on src or dst\n",
          MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS);
    return MSTRO_NOMATCH;
  }

  int64_t src_nd =*(int64_t*)src_ndims,
      dst_nd = *(int64_t*)dst_ndims;
  
  if(src_nd!=dst_nd) {
    DEBUG("Cannot handle differing ndims (%" PRIi64 " src vs %" PRIi64 " dst)\n",
          src_nd, dst_nd);
    return MSTRO_NOMATCH;
  }

  if(! (src_nd==2 && dst_nd==2)) {
    DEBUG("Cannot handle transposition unless dim=2\n");
    return MSTRO_NOMATCH;
  }

  s1 = mstro_attribute_dict_get(src_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER,
                                NULL, &src_patt, NULL, false); 
  s2 = mstro_attribute_dict_get(dst_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER,
                                NULL, &dst_patt, NULL, false); 
  if(s1!=MSTRO_OK || s2!=MSTRO_OK) {
    DEBUG("No %s attribute on src or dst\n",
          MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER);
    return MSTRO_NOMATCH;
  }

  int64_t src_order =*(int64_t*)src_patt,
      dst_order = *(int64_t*)dst_patt;

  DEBUG("FIXME: using hard-coded layout order values. Need to be replaced by schema lookups\n");
  if(  (src_order==MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_ROWMAJOR &&
        dst_order==MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_COLMAJOR)
     ||(src_order==MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_COLMAJOR &&
        dst_order==MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_ROWMAJOR)) {
    /* great! */
    ;
  } else {
    DEBUG("Cannot handle layout order pair (%" PRIi64 " src vs %" PRIi64 " dst)\n",
          src_order, dst_order);
    return MSTRO_NOMATCH;
  }

  /* at this point we have same size, elt size, dimensionality 2, row/col pair */
  
  /* FIXME: See issue #87 -- this only works if no PM transport was involved */
  s1 = mstro_attribute_dict_get(src_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE,
                                NULL, &src_dimsz, NULL, true);  
  s2 = mstro_attribute_dict_get(dst_attr,
                                MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE,
                                NULL, &dst_dimsz, NULL, true);  
  if(s1!=MSTRO_OK || s2!=MSTRO_OK) {
    DEBUG("No %s attribute on src or dst\n",
          MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE);
    return MSTRO_NOMATCH;
  }

  int64_t src_d0 = ((const int64_t*)src_dimsz)[0];
  int64_t src_d1 = ((const int64_t*)src_dimsz)[1];
  int64_t dst_d0 = ((const int64_t*)dst_dimsz)[0];
  int64_t dst_d1 = ((const int64_t*)dst_dimsz)[1];
/*
  if(! (src_d0==src_d1 && src_d0==dst_d1 && dst_d0==dst_d1)) {
    DEBUG("Can only handle square matrices\n");
    return MSTRO_NOMATCH;
  }
*/
  /* actual transposition */
  /* FIXME: should be using mamba API */
  {
  char* src_ptr, *dst_ptr;
  size_t* dimsz, *pel;
  int64_t eltsize;
  int i,j;
  int64_t k;

  src_ptr = m_src->allocation->ptr;
  dst_ptr = m_dst->allocation->ptr;

  eltsize = src_eltsize;
  for (i=0; i<dst_d0; i++)
    for (j=0; j<dst_d1; j++)
      for (k=0; k<eltsize; k++)
        dst_ptr[i*dst_d1*eltsize + j*eltsize +k]
            = src_ptr[j*dst_d0*eltsize + i*eltsize+k];
  }

  return MSTRO_OK;

}



mstro_status
mstro_transform_reshape()
{


  return MSTRO_UNIMPL;
}

mstro_status
mstro_transform_permute()
{


  return MSTRO_UNIMPL;
}

mstro_status
mstro_transform_reverse()
{


  return MSTRO_UNIMPL;
}



mstro_status
mstro_transform_layout(
		mstro_cdo src,
		mstro_cdo dst,
                mstro_attribute_dict src_attr,
                mstro_attribute_dict dst_attr)
{
  if(src==NULL || dst==NULL ||src_attr==NULL || dst_attr==NULL) {
    ERR("Invalid arguments: %p, %p, %p, %p\n",
        src, dst, src_attr, dst_attr);
    return MSTRO_INVARG;
  }
  if(src_attr==dst_attr) {
    /* nothing to be done */
    return MSTRO_OK;
  }
  if(src==dst) {
    ERR("FIXME: In-Place transformation unimplemented\n");
    return MSTRO_UNIMPL;
  }

  if(src->mamba_array==NULL && dst->mamba_array==NULL) {
    DEBUG("Two type-0 CDOs, cowardly refusing to apply transformations\n");
    return MSTRO_NOMATCH;
  }

  assert(src->mamba_array!=NULL && dst->mamba_array!=NULL);

  /* check which transformation to perform and call it. */
  mstro_status s=MSTRO_UNIMPL;
  for(size_t i=0; i<NUM_BUILTIN_TRANSFORMATIONS; i++) {
    s = g_builtin_transformations[i]
        .transformation(src, dst, src_attr, dst_attr);
    if(s==MSTRO_NOMATCH) {
      DEBUG("Transformation '%s' not applicable\n",
            g_builtin_transformations[i].name);
      continue;
    }
    if(s!=MSTRO_OK) {
      ERR("Transformation '%s' failed: %d (%s)\n",
          g_builtin_transformations[i].name,
          s, mstro_status_description(s));
    }
    DEBUG("Successfully executed '%s' transformation\n",
          g_builtin_transformations[i].name);
    /* We only support a single transformation, since otherwise we'd
     * need to record incremental attribute changes, but we don't
     * modify the attribute args of the caller. */
    break;
  }
  if(s!=MSTRO_OK) {
    ERR("No built-in transformation applicable, or failure excuting them\n");
  }
  return s;
}
