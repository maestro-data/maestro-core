/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Transformations implementation API
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_TRANSFORMATION_H_
#define MAESTRO_TRANSFORMATION_H_ 1

/**@addtogroup MSTRO_Core
 **@{
 **/

/**@defgroup MSTRO_Transformation CDO transformations
 **@{
 ** This is the CDO Transformation API
 **
 **
 **/


#include "maestro/status.h"
#include "maestro/i_cdo.h"
#include "attributes/maestro-schema.h"


/** @brief Transform CDO according to layout attributes requirements
 **
 ** Transforms @arg src to @arg dst using the difference in attributes
 ** between @arg src_attr and @arg dst_attr.
 **
 ** Typically @arg src and @arg dst are two different CDO handles for
 ** the same CDO, and the attribute arguments are the attribute dicts
 ** of these two. This function, however, does not require this. It
 ** can also handle @arg src and @arg dst to be the same handle, and
 ** the dictionaries to be not the ones in the the handles.
 **
 ** The caller is responsible for fixing up the handle's attributes
 ** after successful completion to match the layout transformation
 ** performed.
 **
 ** Both CDO handles must have a valid mamba array at invocation time.
 **
 ** @param[in] 		src		The CDO with original layout
 ** @param[inout]	dst		The CDO with to be transformed layout
 ** @param[in]          src_attr        The @arg src attributes
 ** @param[in]          dst_attr        The @arg dst attributes
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_transform_layout(
    mstro_cdo src,
    mstro_cdo dst,
    mstro_attribute_dict src_attr,
    mstro_attribute_dict dst_attr);



mstro_status
mstro_transform_transpose(mstro_cdo src, mstro_cdo dst,
                          mstro_attribute_dict src_attr, mstro_attribute_dict dst_attr);
/* mstro_status */
/* mstro_transform_reshape(); */
/* mstro_status */
/* mstro_transform_permute(); */
/* mstro_status */
/* mstro_transform_reverse(); */


/**@} (end of group MSTRO_Transformation) */
/**@} (end of addtogroup MSTRO_Core) */

#endif
