%module maestro_core
%include "typemaps.i"
%include "stdint.i"
%include "cpointer.i"
%include "exception.i"

/* No identifier conversion here. PEP-8 says
 *
 * > Function names should be lowercase, with words separated by underscores as
 * > necessary to improve readability.
 *
 * so our C API will be just fine.
 */

/***** typemaps *****/
/* mstro_status codes: suppress unless an error needs to be indicated.
   In that case: throw exeption */
%typemap(out) mstro_status  {
  $result = SWIG_Py_Void();
  if(MSTRO_OK!=$1) {
    char msg[1024];
    snprintf(msg, 1024,
             "failure in %s -- %s\n", "$symname", mstro_status_description($1));
    SWIG_exception(SWIG_RuntimeError, msg);
  }
}

/* swig needs help for opaque data types to generate a proper descriptor
   for the cell around them */
//typedef void* mstro_cdo;

/** numeric return values **/
/* CDO group return values: */
%typemap(in, numinputs=0) size_t *result_count (size_t temp) {
  $1 = &temp;
}
%typemap(argout) size_t *result_count {
  /* Create Python Integer from size_t */
  PyObject * temp = PyLong_FromSize_t(*$1);

  /* if multiple results: ensure result is a list */
  if(VOID_Object==$result) {
    /* we're producing the first result */
    $result = temp;
  } else {
    /* coerce previous result to list and append */
    if (!PyList_Check($result)) {
      PyObject * tt = $result;
      $result = PyList_New(1);
      PyList_SetItem($result, 0, tt);
    }
    PyList_Append($result, temp);
    Py_DECREF(temp);
  }
}



/* char ** result args */
%typemap(in, numinputs=0) char **result_p (char *tmp) {
  $1 = &tmp;
}
%typemap(argout) char **result_p (PyObject* obj) {
 /* wrap char** putput to python unicode object */
 obj = PyUnicode_FromString(*$1);
 $result = SWIG_Python_AppendOutput($result,obj);
}
/* unicode object is a copy and owned by python, so temporary object
 * returned by maestro can be freed */
%typemap(freearg) char** result_p {
    free(*$1);
}


/* Similar to size_t results, a whole class of typemaps we need
 * repeatedly is for return arguments of various maestro-defined
 * types. Hence the following parameterized definition: */
%define TYPEMAP_MSTRO_RETURN_ARGUMENT(C_TYPE, C_VARNAME)

  /* allocate C-side temporary for such an argument on input */
  %typemap(in, numinputs=0) C_TYPE * C_VARNAME (C_TYPE temp) {
    $1 = &temp;
  }

  /* ... and wrap result into a python object, returning a suitable
   * list-ified return value */

  %typemap(argout) C_TYPE * C_VARNAME {
    /* Create shadow object (do not use SWIG_POINTER_NEW) */
    PyObject * temp = SWIG_NewPointerObj(*$1,
                                       $descriptor(C_TYPE),
                                       SWIG_POINTER_NOSHADOW);
    /* if multiple results: ensure result is a list */
    if(VOID_Object==$result) {
      /* we're producing the first result */
      $result = temp;
    } else {
      /* coerce previous result to list and append */
      if (!PyList_Check($result)) {
        PyObject * tt = $result;
        $result = PyList_New(1);
        PyList_SetItem($result, 0, tt);
      }
      PyList_Append($result, temp);
      Py_DECREF(temp);
    }
  }
%enddef

/* we apply this to: */

TYPEMAP_MSTRO_RETURN_ARGUMENT(mstro_cdo, result);
TYPEMAP_MSTRO_RETURN_ARGUMENT(mstro_cdo, next_cdo_p);
TYPEMAP_MSTRO_RETURN_ARGUMENT(mstro_group, result);
TYPEMAP_MSTRO_RETURN_ARGUMENT(mstro_cdo_selector, result);
TYPEMAP_MSTRO_RETURN_ARGUMENT(mstro_subscription, result);
TYPEMAP_MSTRO_RETURN_ARGUMENT(mstro_request, request);


/* built-in constant strings should not be wrapped with getter/setter
 * methods. Unfortunately we have to list them all explicitly, swig
 * does not have name wildcarding */

/* (from attributes.h): */
%immutable MSTRO_ATTR_CORE_CDO_NAME;
%immutable MSTRO_ATTR_CORE_CDO_ALLOCATE_NOW;
%immutable MSTRO_ATTR_CORE_CDO_MAESTRO_PROVIDED_STORAGE;
%immutable MSTRO_ATTR_CORE_CDO_RAW_PTR;
%immutable MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE;
%immutable MSTRO_ATTR_CORE_CDO_ISGROUP;
%immutable MSTRO_ATTR_CORE_CDO_GROUP_MEMBERS;
%immutable MSTRO_ATTR_CORE_CDO_ISDISTRIBUTED;
%immutable MSTRO_ATTR_CORE_CDO_DIST_LAYOUT;
%immutable MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY;
%immutable MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE;
%immutable MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS;
%immutable MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER;
%immutable MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE;
%immutable MSTRO_SCHEMA_BUILTIN_YAML_CORE;
%immutable MSTRO_SCHEMA_BUILTIN_YAML_ECMWF;

/* mstro_cdo in-argument objects are quite common, and to make it easy
 * to access them during the manipulation of other arguments we keep a
 * reference around. Otherwise they'll simply be extracted like swig
 * would do by default */
%typemap(in) mstro_cdo cdo (mstro_cdo _global_input_cdo = NULL) {
  void *argp = NULL;
  int res = SWIG_ConvertPtr($input, &argp, $1_descriptor, 0);
  if (!SWIG_IsOK(res)) {
    SWIG_exception_fail(SWIG_ArgError(res),
                        "in method '$symname', "
                        "argument $argnum"
                        " of type '$1_type'");
  }
  _global_input_cdo = (mstro_cdo) argp;
  $1 = _global_input_cdo;
}

%{
#include "maestro/cdo.h"
#include "maestro/attributes.h"

  /* including maestro/i_globals.h leads to a whole bunch of other
   * internal includes which force us to grow the -I path
   * tremendously. We only need one global here: */
extern mstro_schema g_mstro_core_schema_instance;

/* type of a dealloctor for C-side value objects */
typedef void(*val_dtor)(void*);

/* converter functions from python values to
   freshly allocated maestro attribute value objects */

/* general API: PyObject, dest pointer, deallocator dest pointer
 * naming convention: by maestro data type */
mstro_status
mstro_py__conv_py_c__bool(PyObject *in, bool **out, val_dtor* dtor)
{
  bool retstat=MSTRO_OK;
  bool *tmp = malloc(sizeof(bool));
  if(tmp!=NULL) {
    if(PyObject_IsTrue(in)==1) {
      *tmp = true;
    } else {
      *tmp = false;
    }
    *out  = tmp;
    *dtor = free;
  } else {
    free(tmp);
    fprintf(stderr, "Failed to allocate converted attribute value\n");
    retstat=MSTRO_NOMEM;
  }
  return retstat;
}

mstro_status
mstro_py__conv_py_c__uint64(PyObject *in, uint64_t **out, val_dtor* dtor)
{
  mstro_status retstat=MSTRO_OK;

  if(PyLong_Check(in)) {
    int overflow;
    long long t = PyLong_AsLongLongAndOverflow(in, &overflow);
    if(overflow==0) {
      if(t>=0) {
        if(t <= UINT64_MAX) {
          uint64_t *tmp = malloc(sizeof(uint64_t));
          if(tmp!=NULL) {
            *tmp = (uint64_t)t;
            *out = tmp;
            *dtor = free;
          } else {
            fprintf(stderr, "Failed to allocate converted attribute value\n");
            retstat=MSTRO_NOMEM;
          }
        } else {
          fprintf(stderr, "Value too large for uint64 attribute\n");
          retstat=MSTRO_INVARG;
        }
      } else {
        fprintf(stderr, "Negative value on unsigned attribute\n");
        retstat=MSTRO_INVARG;
      }
    } else {
      fprintf(stderr, "Overflow on integer value\n");
      retstat = MSTRO_INVARG;
    }
  } else {
    fprintf(stderr, "non-integral argument to uint64_t attribute\n");
    retstat=MSTRO_INVARG;
  }

  return retstat;
}

mstro_status
mstro_py__conv_py_c__int64(PyObject *in, int64_t **out, val_dtor* dtor)
{
  mstro_status retstat = MSTRO_OK;
  if(PyLong_Check(in)) {
    int overflow;
    long long t = PyLong_AsLongLongAndOverflow(in, &overflow);
    if(overflow==0) {
      if(t>=INT64_MIN) {
        if(t <= INT64_MAX) {
          int64_t *tmp = malloc(sizeof(int64_t));
          if(tmp!=NULL) {
            *tmp = (int64_t)t;
            *out = tmp;
            *dtor = free;
          } else {
            fprintf(stderr, "Failed to allocate converted attribute value\n");
            retstat=MSTRO_NOMEM;
          }
        } else {
          fprintf(stderr, "Value too large for int64 attribute\n");
          retstat=MSTRO_INVARG;
        }
      } else {
        fprintf(stderr, "Value too small for int64 attribute\n");
        retstat=MSTRO_INVARG;
      }
    } else {
      fprintf(stderr, "Overflow on integer value\n");
      retstat = MSTRO_INVARG;
    }
  } else {
    fprintf(stderr, "non-integral argument to int64_t attribute\n");
    retstat=MSTRO_INVARG;
  }
  return retstat;
}

mstro_status
mstro_py__conv_py_c__float(PyObject *in, float **out, val_dtor* dtor)
{
  mstro_status retstat=MSTRO_OK;

  if(PyFloat_Check(in)) {
    double t = PyFloat_AsDouble(in);
    if(!PyErr_Occurred()) {
      /* no elaborate rounding error checks */
      float *tmp = malloc(sizeof(float));
      if(tmp!=NULL) {
        *tmp = (float)t;
        *out = tmp;
        *dtor = free;
      } else {
        fprintf(stderr, "Failed to allocate converted attribute value\n");
        retstat=MSTRO_NOMEM;
      }
    } else {
      fprintf(stderr, "Error in PyFloat to double conversion\n");
    }
  } else {
    fprintf(stderr, "non-PyFloat argument to float attribute\n");
    retstat=MSTRO_INVARG;
  }
  return retstat;
}

mstro_status
mstro_py__conv_py_c__double(PyObject *in, double **out, val_dtor* dtor)
{
  mstro_status retstat=MSTRO_OK;
  if(PyFloat_Check(in)) {
    double t = PyFloat_AsDouble(in);
    if(!PyErr_Occurred()) {
      /* no elaborate rounding error checks */
      double *tmp = malloc(sizeof(double));
      if(tmp!=NULL) {
        *tmp = t;
        *out = tmp;
        *dtor = free;
      } else {
        fprintf(stderr, "Failed to allocate converted attribute value\n");
        retstat=MSTRO_NOMEM;
      }
    } else {
      fprintf(stderr, "Error in PyFloat to double conversion\n");
    }
  } else {
    fprintf(stderr, "non-PyFloat argument to double attribute\n");
    retstat=MSTRO_INVARG;
  }
  return retstat;
}

mstro_status
mstro_py__conv_py_c__string(PyObject *in, char **out, val_dtor* dtor)
{
  mstro_status retstat=MSTRO_OK;
  if(PyUnicode_Check(in)) {
    PyObject *k = PyObject_Str(in);
    const char *k_str = PyUnicode_AsUTF8(k);
    if(k_str) {
      *out = strdup(k_str);
      if(*out!=NULL) {
        *dtor = free;
      } else {
        fprintf(stderr, "Failed to allocate C-side string\n");
        retstat = MSTRO_NOMEM;
      }
    } else {
      fprintf(stderr, "Failed to convert value to UTF8\n");
      retstat = MSTRO_FAIL;
    }
  } else {
    fprintf(stderr, "Not a string-like value\n");
    retstat = MSTRO_INVARG;
  }
  return retstat;
}

mstro_status
mstro_py__conv_py_c__blob(PyObject *in, mstro_blob **out, val_dtor* dtor)
{
  mstro_status retstat=MSTRO_OK;
  if(PyObject_CheckBuffer(in)) {
    /* object supports buffer protocol */
    PyObject *explicit_bytes=NULL;
    if(!PyBytes_Check(in)) {
      /* need to create explicit byte object first */
      explicit_bytes = PyBytes_FromObject(in);
    } else {
      explicit_bytes = in;
    }
    assert(PyBytes_Check(explicit_bytes));

    Py_ssize_t len;
    char *buf;
    int k = PyBytes_AsStringAndSize(explicit_bytes, &buf, &len);
    if(k==0) {
      /* buf refers to internal buffer inside explicit_bytes, so need to copy */
      char *data=malloc(len);
      if(data!=NULL) {
        mstro_blob *blob;
        memcpy(data, buf, len);
        retstat = mstro_blob_create(len, data, &blob);
        if(retstat==MSTRO_OK) {
          *out = blob;
          *dtor = (val_dtor)mstro_blob_dispose;
        }
      } else {
        fprintf(stderr, "Cannot allocate blob copy\n");
        retstat=MSTRO_NOMEM;
      }
    } else {
      fprintf(stderr, "Cannot convert blob value data\n");
      retstat=MSTRO_FAIL;
    }
  } else {
    fprintf(stderr, "blob value does not support buffer protocol, can't serialize it\n");
    retstat = MSTRO_INVARG;
  }
  return retstat;
}

#include <datetime.h>
mstro_status
mstro_py__conv_py_c__timestamp(PyObject *in,
                               mstro_timestamp **out,
                               val_dtor* dtor)
{
  mstro_status retstat = MSTRO_OK;

  if(PyDateTime_Check(in)) {
    /* great, directly convertable */
    fprintf(stderr, "FIXME: conversion of datetime to epoch value missing\n");
    retstat=MSTRO_UNIMPL;
    goto BAILOUT;
  } else if(PyUnicode_Check(in)) {
    /* try to parse */
    PyObject *v = PyObject_Str(in);
    const char *v_str = PyUnicode_AsUTF8(v);
    *out= malloc(sizeof(mstro_timestamp));
    if(out!=NULL) {
      retstat = mstro_timestamp_parse(v_str, strlen(v_str), *out);
      if(retstat!=MSTRO_OK) {
        free(*out);
        fprintf(stderr, "Failed to parse "
                "timestamp value as RFC3339 timestamp\n");
      } else {
        *dtor = free;
      }
    } else {
      fprintf(stderr, "Failed to allocate timestamp cells\n");
      retstat=MSTRO_NOMEM;
    }
  } else {
    fprintf(stderr, "Not a supported "
            "timestamp format (needs datetime or RFC3339 string)\n");
    retstat= MSTRO_INVARG;
  }

BAILOUT:
  return MSTRO_UNIMPL;
}




mstro_status
mstro_py__conv_py_c__mmblayout(PyObject *in, mmbLayout **out, val_dtor* dtor)
{
  return MSTRO_UNIMPL;
}

/* reverse conversions: creating new python objects from maestro
 * attributes.  note that we always create self-sufficient python
 * objects to decouple the CDO internal data allocations from
 * python-land. For now this seems like the preferred way due to
 * simplicity and safety from bad (possibly unintendeds) intrusions
 * into maestro's data structures by script kiddies
 * API: C object, PyObject destination
 */

mstro_status
mstro_py__conv_c_py__timestamp(const void* in, PyObject **out)
{
  return MSTRO_UNIMPL;
}

/* dispatcher for the above */
mstro_status
mstro_py__conv_py_c(const char *key, enum mstro_cdo_attr_value_type val_type,
                    PyObject *value, void** dst_p, val_dtor* dtor_p,
                    mstro_cdo context_cdo,
                    bool *copy_value_p, bool *user_owned_value_p)
{
  mstro_status s;

  switch(val_type) {
    case MSTRO_CDO_ATTR_VALUE_bool:
      s = mstro_py__conv_py_c__bool(value, (bool**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_uint64:
      s = mstro_py__conv_py_c__uint64(value, (uint64_t**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_int64:
      s = mstro_py__conv_py_c__int64(value, (int64_t**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_float:
      s = mstro_py__conv_py_c__float(value, (float**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_double:
      s = mstro_py__conv_py_c__double(value, (double**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_cstring:
      s = mstro_py__conv_py_c__string(value, (char**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_blob:
      s = mstro_py__conv_py_c__blob(value, (mstro_blob**)dst_p, dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_timestamp:
      s = mstro_py__conv_py_c__timestamp(value,
                                         (mstro_timestamp**)dst_p,
                                         dtor_p);
      break;

    case MSTRO_CDO_ATTR_VALUE_pointer: {
      /* special case for payload attributes */
      if(strcmp(key, MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY)==0) {
        PyObject *rep = PyObject_Repr(value);
        assert(PyUnicode_Check(rep));
        const char *str_rep = PyUnicode_AsUTF8(rep);
        fprintf(stderr, "FIXME (unimplemented): "
                "found mamba array attribute (python arg: %s)\n",
                str_rep);
        s=MSTRO_UNIMPL;
        goto BAILOUT;
      } else if(strcmp(key, MSTRO_ATTR_CORE_CDO_RAW_PTR)==0) {
        /* check that no size is set -- we'll replace it by the
         * serialized object's size */
        fprintf(stderr,
                "FIXME: Overriding any previously set size of the CDO\n");
        PyObject *rep = PyObject_Repr(value);
        assert(PyUnicode_Check(rep));
        const char *str_rep = PyUnicode_AsUTF8(rep);
        fprintf(stderr, "Found raw-ptr attribute, "
                /* " pickling python arg: %s\n", */
                /* str_rep); */
                " pickling python arg\n");

        if(PyObject_CheckBuffer(value)!=1) {
          fprintf(stderr, "Object %s does not support python buffer protocol,"
                  " cannot assign it to maestro.cdo.raw-ptr\n", str_rep);
          s=MSTRO_INVARG;
          goto BAILOUT;
        } else {
          /* buffer protocol supported, get size and linear representation */
          Py_buffer buffer;
          if(PyObject_GetBuffer(value, &buffer, PyBUF_SIMPLE) == -1) {
            fprintf(stderr, "Failed to obtain buffer for object\n");
            s=MSTRO_FAIL;
            goto BAILOUT;
          } else {
            assert(buffer.ndim==1);
            fprintf(stderr, "Got %zu length linear buffer for object\n",
                    buffer.len);
            void *buf_copy = malloc(buffer.len);
            if(buf_copy==NULL) {
              fprintf(stderr, "cannot allocate buffer copy of raw-ptr value\n");
            } else {
              memcpy(buf_copy, buffer.buf, buffer.len);
              /* SIDE EFFECT: store size and raw-ptr (of copy) */
              s=mstro_cdo_attribute_set(context_cdo,
                                        MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                        &buffer.len, true, false);
              if(s!=MSTRO_OK) {
                fprintf(stderr, "Failed to set length of CDO data\n");
              } else {
                *dst_p = buf_copy;
                *dtor_p = free;
                /* subtle! value ownership transfer here: */
                *copy_value_p       = false;
                *user_owned_value_p = false;
                s=MSTRO_OK;
              }
            }
            /* release view */
            PyBuffer_Release(&buffer);
          }
        }
      } else {
        fprintf(stderr, "Unimplemented: pointer typed CDO attribute %s"
                " unsupported in python wrapper"
                " (can only handle mamba or raw)\n", key);
        s=MSTRO_UNIMPL;
        goto BAILOUT;
      }
    }
      break;
    case MSTRO_CDO_ATTR_VALUE_mmblayout:
      s = mstro_py__conv_py_c__mmblayout(value, (mmbLayout**)dst_p, dtor_p);
      break;
    case MSTRO_CDO_ATTR_VALUE_INVALID:
      /*fallthrough*/
    default:
      fprintf(stderr, "FIXME: Unhandled CDO attribute type %d\n", val_type);
      s = MSTRO_UNIMPL;
  }

BAILOUT:
  return s;
}

/* add attributes from dict OBJ to CDO */
/* static */
/* mstro_status */
/*     pydict_to_attributes(PyObject *obj, mstro_cdo_attributes *attr) */
/* { */
/*   mstro_status retstat = MSTRO_OK; */
/*   PyObject *key, *value; */
/*   Py_ssize_t pos = 0; */
/*   size_t i=0; */

/*   if(attr==NULL) */
/*     return MSTRO_INVARG; */

/*   assert(PyDict_Check(obj)); */

/*   mstro_status s = mstro_cdo_attributes_create(NULL, attr); */
/*   if(s!=MSTRO_OK) { */
/*     fprintf(stderr, "Failed to convert attribute dictionary: no space\n"); */
/*     *attr = MSTRO_ATTR_DEFAULT; */
/*   } else { */
/*     while (PyDict_Next(obj, &pos, &key, &value)) { */
/*       printf("entry %zu: \n", i++); */
/*       PyObject_Print(key, stdout, Py_PRINT_RAW); */
/*       PyObject *k = PyObject_Str(key); */
/*       const char *k_str = PyUnicode_AsUTF8(k); */
/*       printf(" : "); */
/*       PyObject_Print(value, stdout, Py_PRINT_RAW); */
/*       printf("\n"); */
/*       void *val = NULL; */
/*       val_dtor dtor = NULL; */

/*       /\* find key in global schema and value type so we can convert it *\/ */
/*       /\* (this duplicates some code from mstro_attribute_dict_set, */
/*          should have a helper function for both) *\/ */
/*       /\* FIXME: we assume that k_str is the fully qualified key name *\/ */

/*       bool knownp=false; */
/*       enum mstro_cdo_attr_value_type val_type; */

/*       s = mstro_attribute_lookup_type(g_mstro_core_schema_instance, */
/*                                       k_str, */
/*                                       &knownp, &val_type); */
/*       if(s!=MSTRO_OK) { */
/*         fprintf(stderr, "Key |%s| not found in current global schema\n", k_str); */
/*         retstat = MSTRO_NOENT; */
/*         goto BAILOUT; */
/*       } */


/*       retstat |= mstro_py__conv_py_c(k_str, val_type, value, &val, &dtor, */
/*                                      _global_input_cdo); */

/*       if(retstat!=MSTRO_OK) { */
/*         fprintf(stderr, "Failed to add key/val pair (index %zd): %d (%s)\n", */
/*                 pos, s, mstro_status_description(s)); */
/*         retstat = s; */
/*       } */
/*     } */
/*   } */

/* //    long i = PyLong_AsLong(value); */
/* //    if (i == -1 && PyErr_Occurred()) { */
/* //        return -1; */
/* //    } */
/* //    PyObject *o = PyLong_FromLong(i + 1); */
/* //    if (o == NULL) */
/* //        return -1; */
/* //    if (PyDict_SetItem(self->dict, key, o) < 0) { */
/* //        Py_DECREF(o); */
/* //        return -1; */
/* //    } */
/* //    Py_DECREF(o); */
/* //  } */
/* BAILOUT: */
/*   return retstat; */
/* } */
%}

/* dictionary to CDO attributes,
   mostly for mstro_cdo_declare with non-NULL attribute argument */
%typemap(default) mstro_cdo_attributes {
  /* allow user to drop default argument */
  $1 = MSTRO_ATTR_DEFAULT;
}
%typemap(in) mstro_cdo_attributes attributes {
  if(Py_None == $input) {
    $1 = MSTRO_ATTR_DEFAULT; /* not strictly necessary but cleaner */
  } else if(PyDict_Check($input)) {
    fprintf(stderr, "Found Dictionary object for attributes, parsing\n");
    //mstro_status s = pydict_to_attributes($input, &$1);
    mstro_status s= MSTRO_UNIMPL;
    if(s!=MSTRO_OK) {
      char msg[1024];
      snprintf(msg, 1024,
               "Failed to convert one or more attributes "
               "from dictionary, last error %d (%s)\n",
               s, mstro_status_description(s));
      SWIG_exception(SWIG_RuntimeError, msg);
    }
  } else {
    SWIG_exception(SWIG_RuntimeError,
                   "Invalid argument: \"$1_name\""
                   " needs to be None or a dictionary object");
  }
}

/** attribute setting
 * We take the key,
 * lookup the appropriate type,
 * then create a suitable argument,
 * and always have maestro-core create an owned copy */
%typemap(in) const char *attribute_key (
    bool knownp,
    enum mstro_cdo_attr_value_type _global_val_type,
    const char *_global_k_str,
    mstro_status stat) {
  /* determine value type we need to create */
  knownp=false;
  /* Identify the kind of attribute key we have.

     We currently have no convenient way to wrap the built-in
     attributes (like MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE) as pointer
     values, instead they are being turned into PyObjects with string
     content by the %immutable declaration.

     Hence we expect the key to be a Python string,
     which we need to turn into a temporary C string.
  */
  _global_k_str = NULL;

  if(PyUnicode_Check($input)) {
    /* python string */
    _global_k_str = PyUnicode_AsUTF8($input);
    //fprintf(stderr, "Got attribute key from Python: |%s|\n", _global_k_str);
    /*  } else if(directly-wrapped built-in-key pointer valye) {
        k_str = xxx;
    */
  } else {
      char msg[1024];
      PyObject* repr = PyObject_Repr($input);
      PyObject* str = PyUnicode_AsEncodedString(repr, "utf-8", "~E~");
      const char *bytes = PyBytes_AS_STRING(str);

      snprintf(msg, 1024,
               "Attribute key must be a maestro-core builtin "
               "keyword or a python string, got %s\n",
               bytes);
      Py_XDECREF(repr);
      Py_XDECREF(str);
      SWIG_exception(SWIG_RuntimeError, msg);
  }

  stat = mstro_attribute_lookup_type(g_mstro_core_schema_instance,
                                     _global_k_str, &knownp, &_global_val_type);
  if(stat!=MSTRO_OK) {
    char msg[1024];
    snprintf(msg, 1024,
             "Failed to lookup key |%s| in current global schema:"
             " %s\n", _global_k_str, mstro_status_description(stat));
    SWIG_exception(SWIG_RuntimeError, msg);
  } else if(!knownp) {
    char msg[1024];
    snprintf(msg, 1024,
             "Key |%s| not found in current global schema: %s\n",
             _global_k_str, mstro_status_description(MSTRO_NOENT));
    SWIG_exception(SWIG_RuntimeError, msg);
  }
  $1 = (char*)_global_k_str;
}

%typemap(in) void * attribute_val (void *val,
                                   val_dtor dtor,
                                   mstro_status stat)
{
  /* init our temp values */
  val = NULL;
  dtor = NULL;

  /* convert python argument to proper temporary */
  stat = mstro_py__conv_py_c(_global_k_str,
                             _global_val_type,
                             $input, &val, &dtor,
                             _global_input_cdo, _global_copy_attribute_value_p, _global_user_owned_attribute_value_p);

  //  fprintf(stderr, "Converted %p to allocation at %p, status %d\n",
  //  $input, val, stat);
  if(stat!=MSTRO_OK) {
    char msg[1024];
    snprintf(msg, 1024,
             "Error converting attribute value: %s\n",
             mstro_status_description(stat));
    SWIG_exception(SWIG_RuntimeError, msg);
  }
  /* pass to maestro-core */
  $1 = val;
  /* (value copied by maestro-core, so no GC bookeeping on temp_val) */
}

%typemap(in, numinputs=0) bool copy_attribute_value
                          (bool *_global_copy_attribute_value_p) {
  /* suppress argument, we behave as through the python caller had set
     the 'true' arg on the C API, but we create a way for the
     converter fragments to change that behavior for special cases */
  $1 = true;
  _global_copy_attribute_value_p = &$1;

}

%typemap(in, numinputs=0) bool user_owned_attribute_value
                          (bool *_global_user_owned_attribute_value_p) {
  /* suppress argument, we behave as through the python caller had set
     the 'true' arg on the C API, but we create a way for the
     converter fragments to change that behavior for special cases */
  $1 = false;
  _global_user_owned_attribute_value_p = &$1;
}


/** Attribute retrieval

 */
/* allocate C-side temporary for such an argument on input */
%typemap(in, numinputs=0) void ** attribute_val_p (void * temp) {
  $1 = &temp;
 }

/* ... and wrap result into a python object, returning a suitable
 * list-ified return value */

%typemap(argout) void ** attribute_val_p {
  /* need to look at the context, in particular the value type for the key */
  PyObject *res = NULL;

  switch(_global_val_type) {
    case MSTRO_CDO_ATTR_VALUE_bool: {
      bool *val =*$1;
      if(*val==true)
        res = Py_True;
      else
        res = Py_False;
      break;
    }
      
    case MSTRO_CDO_ATTR_VALUE_uint64: {
      uint64_t *val =*$1;
      assert(sizeof(unsigned long long)>=sizeof(uint64_t));
      unsigned long long val_ul = (unsigned long long)*val;
      res = PyLong_FromUnsignedLongLong(val_ul);
      break;
    }

    case MSTRO_CDO_ATTR_VALUE_int64: {
      int64_t *val =*$1;
      assert(sizeof(long long)>=sizeof(int64_t));
      long val_l = (long long )*val;
      res = PyLong_FromLongLong(val_l);
      break;
    }

    case MSTRO_CDO_ATTR_VALUE_float: {
      float *val =*$1;
      double d = (double)*val;
      res = PyFloat_FromDouble(d);
      break;
    }

    case MSTRO_CDO_ATTR_VALUE_double: {
      double *val =*$1;
      res = PyFloat_FromDouble(*val);
      break;
    }

    case MSTRO_CDO_ATTR_VALUE_cstring: {
      char *val = *$1;
      res = PyUnicode_FromString(val);
      break;
    }

    case MSTRO_CDO_ATTR_VALUE_pointer: {
      char *val = *$1;
      const int64_t *l_p = NULL;

      /* try to find length; if unavailable that is an error */
      mstro_status s=mstro_cdo_attribute_get(_global_input_cdo,
                                              MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                              NULL, (const void**)&l_p);
      if(s!=MSTRO_OK) {
        char msg[1024];
        snprintf(msg, 1024,
                 "CDO is missing value for attribute %s when trying to fetch and convert %s attribute, status: %d (%s)\n",
                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, _global_k_str,
                 s, mstro_status_description(s));
        SWIG_exception_fail(SWIG_RuntimeError, msg);
      } else {
        fprintf(stderr, "Creating result of size %" PRIi64 "\n", *l_p);
        res = PyByteArray_FromStringAndSize(val, *l_p);
        if(res==NULL) {
          char msg[1024];
          snprintf(msg, 1024,
                   "Failed to convert argument to ByteArray (len: %" PRIi64 ")\n", *l_p);
          SWIG_exception_fail(SWIG_RuntimeError, msg);                              
        }
      }
      break;        
    }
      
    case MSTRO_CDO_ATTR_VALUE_timestamp:
      /*fallthrough*/
    case MSTRO_CDO_ATTR_VALUE_mmblayout:
      /*fallthrough*/
    case MSTRO_CDO_ATTR_VALUE_INVALID:
      /*fallthrough*/
    default: {
      char msg[1024];
      snprintf(msg, 1024,
               "Value type for attribute %s (%d) unsupported in python wrapper\n",
               _global_k_str, _global_val_type);

      SWIG_exception_fail(SWIG_RuntimeError, msg);
    };
  }

  /* if multiple results: ensure result is a list */
  if(VOID_Object==$result) {
    /* we're producing the first result */
    $result = res;
  } else {
    /* coerce previous result to list and append */
    if (!PyList_Check($result)) {
      PyObject * tt = $result;
      $result = PyList_New(1);
      PyList_SetItem($result, 0, tt);
    }
    PyList_Append($result, res);
    Py_DECREF(res);
  }
 }


/** Pool Events.
    
    An event return value is NOT mapped using the
    TYPEMAP_MSTRO_RETURN_ARGUMENT macro above, since events form a
    linked list structure that we want to enable iteration on.
    Instead we convert the list into a python list element-by-element,
    and detach them from each other, so that each swig object can be
    GCed separately The code below enables that.
*/

/* allocate C-side temporaryon input */
%typemap(in, numinputs=0) mstro_pool_event * events (mstro_pool_event temp) {
  $1 = &temp;
}

/* create a python list of the indivdual items and splice to result list */
%typemap(argout) mstro_pool_event * events {
  PyObject *reslist = PyList_New(0);
  if(!reslist) {
    SWIG_exception_fail(SWIG_RuntimeError, "Can't create result list");
  } else {
    mstro_pool_event e = *$1;
    while(e!=NULL) {
      /* Create shadow object (do not use SWIG_POINTER_NEW) */
      PyObject * temp = SWIG_NewPointerObj(e,
                                           $descriptor(mstro_pool_event),
                                           SWIG_POINTER_NOSHADOW);
      if(PyList_Insert(reslist, 0, temp)!=0) {
        SWIG_exception_fail(SWIG_RuntimeError, "Failed to insert event into result");
      }
      mstro_pool_event next = e->next;
      e->next=NULL; /* detach tail: python is doing the CONSing for us */
      e=next;
    }
  }
  /* result list is reversed to avoid APPENDs above, so */
  if(PyList_Reverse(reslist)!=0) {
    SWIG_exception_fail(SWIG_RuntimeError, "Can't reverse result list");
  } else {  
    /* if multiple results: ensure result is a list */
    if(VOID_Object==$result) {
      /* we're producing the first result */
      $result = reslist;
    } else {
      /* coerce previous result to list and append */
      if (!PyList_Check($result)) {
        PyObject * tt = $result;
        $result = PyList_New(1);
        PyList_SetItem($result, 0, tt);
      }
      PyList_Append($result, reslist);
      Py_DECREF(reslist);
    }
  }
 }

/* similarly, for the acknowledgemet path: if we get a python list,
 * try to iterate over it, otherwise hand the wrapped (potentially
 * list-like mstro_pool_event over directly */

%typemap(typecheck) mstro_pool_event events {
  if((0==SWIG_ConvertPtr($input, 0, $1_descriptor, 0))
     || PyList_Check($input))
    $1 = 1;
  else
    $1 = 0;
}

%typemap(in) mstro_pool_event events {
  if(Py_None == $input) {
    SWIG_exception_fail(SWIG_RuntimeError, "Invalid argument: \"$1_name\" cannot be None or null\n");
  }
  
  if(PyList_Check($input)) {
    /* should be a list of events */
    mstro_pool_event *tail=NULL;
    Py_ssize_t index=0;
    PyObject *o;

    $1 = NULL; /* default */

    /* unfortunately there is no 'CDR(l)' style iterator in python */
    Py_ssize_t len = PyList_Size($input);
    for(index = 0; index<len; index++) {
      o=PyList_GetItem($input, index);
      mstro_pool_event e;
      int res = SWIG_ConvertPtr(o, (void**)&e, $1_descriptor, 0);
      if(!SWIG_IsOK(res)) {
        SWIG_exception_fail(SWIG_TypeError, "in method '$symname', expecting argument of type mstro_pool_event");
      }
      //fprintf(stderr, "parsed list member %zd event as %s\n", index, e==NULL? "null" : mstro_pool_event_description(e->kind));

      if(!tail) {
        /* have first item */
        $1 = e;
        tail = &$1;
      } else {
        /* already have at least one entry: append */
        (*tail)->next = e;
        }
      /* this sliently splices event tails; this reorders events, but maestro-core does not guarantee ordering at the list level, so that is ok */
      while((*tail)->next!=NULL) {
        tail=& ((*tail)->next);
      }
    }
    /* $1 was set at first item found, or is still NULL */
    if($1==NULL) {
      SWIG_exception_fail(SWIG_RuntimeError, "Invalid argument: \"$1_name\" cannot be an empty list\n");
    }
  } else {
    int res = SWIG_ConvertPtr($input, (void**)&$1, $1_descriptor, 0);
    if(!SWIG_IsOK(res)) {
      SWIG_exception_fail(SWIG_TypeError, "in method '$symname', expecting argument of type mstro_pool_event");
    }
    //PyObject *v = PyObject_Str($input);
    //const char *v_str = PyUnicode_AsUTF8(v);
    // fprintf(stderr, "parsed %s as %s\n", v_str, $1==NULL? "null" : mstro_pool_event_description($1->kind));

  }
  // fprintf(stderr, "parsed event as %s\n", $1==NULL? "null" : mstro_pool_event_description($1->kind));
 }
      


%{
#define SWIG_FILE_WITH_INIT
  #include "maestro.h"
  #include "maestro/attributes.h"
  #include "maestro/cdo.h"
  #include "maestro/config.h"
  #include "maestro/core.h"
  #include "maestro/env.h"
  #include "maestro/groups.h"
  #include "maestro/logging.h"
  #include "maestro/msm.h"
  #include "maestro/pool.h"
  #include "maestro/pool_manager.h"
  #include "maestro/scope.h"
  #include "maestro/status.h"
%}


%include "maestro.h"
%include "maestro/attributes.h"
%include "maestro/cdo.h"
%include "maestro/config.h"
%include "maestro/core.h"
%include "maestro/env.h"
%include "maestro/groups.h"
// %include "maestro/logging.h"
%include "maestro/msm.h"
%include "maestro/pool.h"
%include "maestro/pool_manager.h"
%include "maestro/scope.h"
%include "maestro/status.h"
