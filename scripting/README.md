Scripting interface to the maestro library
==========================================

At this time we only provide a low-level python wrapper for the libmaestro library.

To avoid confusion with other packages called 'maestro' on pypi.org it is called `maestro_core`.

To build it, install swig from your system repositories, or from swig.org. A version >=4.0.1 is recommended.

You also need a development environment for python3; if `configure` fails to detect it, try

```
PYTHON=python3 ./configure
```

(replacing the `python3` by whatever your interpreter is called), or 

```
./configure PYTHON_VERSION=3
```

Automatic (default) Installation
--------------------------------

When you install maestro-core the python wrappers will be installed into

```
$prefix/lib/python<version>/site-packages
```

To use them you may need to to add this directory to your python's search path, e.g., by calling python via

```
PYTHONPATH="$prefix/lib/python<version>/site-packages:$PYTHONPATH" python3
```




Installation using setup.py
---------------------------

If you use the `setup.py` file provided you should be able to install
the wrappers into 'the ususal place', e.g., the global python installation,
user site-packages location or in a virtual environment as provided by the 
`venv` package.

Examples:

```
./setup.py install 
```

for a global installation.

Using a virtual environment (assuming you have installed the `venv` and `wheel` packages, e.g., via `pip`):

```
# ensure virtual environment has wheel installed:
pip install wheel
# in $top_srcdir/scripting do:
pip install .
```


Installation into user-site-packages directory
------------------------------------------------

Use the `setup.py` script provided:

```
./setup.py install --user
```


Testing without installation
----------------------------

The script in `scripting/mpython` wraps the python interpreter discovered at
configure time with enough PYTHONPATH so that the maestro package can be
imported successfully even while not yet installed. This is used during `make
check`, but may also be useful for experimenting.


Usage
-----

All public API functions and constants are available in the
`maestro_core` package via their C name. Return status codes are
mapped to python exceptions, and functions that have
effectively-output arguments in the C API are mapped to properly
return (one or more) python return values.

Example:

```
import maestro_core as M

# initialize maestro runtime
M.mstro_init("sample-workflow", "sample-component-1", 0)

# Create two CDOs, no attributes so far
cdo1 = M.mstro_cdo_declare("CDO 1", None)
cdo2 = M.mstro_cdo_declare("CDO 2", None)

# offer them
M.mstro_cdo_offer(cdo1)
M.mstro_cdo_offer(cdo2)

# create another handle for the CDO named "CDO 2"
cdo2_handle2 = M.mstro_cdo_declare("CDO 2", None)

# announce we'll need it
M.mstro_cdo_require(cdo2_handle2)
M.mstro_cdo_demand(cdo2_handle2)

# withdraw the initially offered CDOs
M.mstro_cdo_withdraw(cdo1)
M.mstro_cdo_withdraw(cdo2)

# dispose handles
M.mstro_cdo_dispose(cdo1)
M.mstro_cdo_dispose(cdo2)
M.mstro_cdo_dispose(cdo2_handle2)

# stop runtime (can be restarted later)
M.mstro_finalize()

```



