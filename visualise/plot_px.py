##################################################################################
# Plotting maestro-log events using plotly                       	             #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################


import plotly.express as px

import helper as hp
import pandas as pd

#plotly backend
def saveplot_px(fig, outPath):
    fig.write_image(outPath, format='pdf')

def plotEvents_px(events, zoom):
    nano = 1000000000
    if events == []:
        fig = px.bar()
        fig.show()
        return fig

    df = pd.DataFrame(events)


    fig = px.bar(df,  base="start", x="duration", y="componentName", color= "name",
                             color_discrete_map=hp.eventsColors,text="parameter", orientation='h', 
                             labels=dict(componentName = "Components", duration = "Time (s)", name = "Function name"))
    fig.update_traces(width=0.2, textposition='outside', insidetextanchor="middle", textangle=270, textfont_color="black")
    #fig.update_yaxes(fixedrange=True)
    fig.update_yaxes(categoryorder="total ascending")
    fig.for_each_trace(lambda t: t.update(textfont_color=t.marker.color, textposition='outside'))
    
    #Apply zoom
    if zoom != "":
        if zoom == "auto":
            import zoomHelper as zh
            xrange = zh.autoZoom(df["start"])
        else:
            xrange = zoom.split(",") 
            xrange = [ float(x) for x in xrange]
        fig.update_layout(xaxis={"range": xrange})

    fig.update_layout(uniformtext_minsize=8, uniformtext_mode='show')
    
    #fig.update_traces(width=0.2)
    # annotations1 = [dict(
    #         x=xi,
    #         y=yi,
    #         text=t,
    #         xanchor='right',
    #         yanchor='bottom',
    #         yshift=20,
    #         align = "right",
    #         valign = "middle",
    #         textangle=270,
    #         showarrow=False,
    #     ) for xi, yi, t in zip(df["start"], df["componentName"], df["parameter"])]
    # fig.update_layout(annotations = annotations1)
    
    #Enable saving to svg instead of png (default)
    configuration  = dict({"toImageButtonOptions": dict( format = "svg", filename = "maestro_visualization")})

    fig.show(config = configuration)
    return fig

