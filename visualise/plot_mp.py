##################################################################################
# Plotting maestro-log events using matplotlib                   	             #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################


import matplotlib as mpl
from matplotlib import rc
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
import helper as hp
import pandas as pd

mpl.rcParams.update({'font.size': 14})
bar_width = 0.2  


#matplotlib backend

def plotEvents_mp(events, zoom =""):
    
    nano = 1000000000

    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    df = pd.DataFrame(events)

    #calculate colors 
    colors =  [hp.eventsColors[name] for name in df["name"]]
    
    # add data
    ax.barh(y=df["componentName"], left =df["start"], width = df["duration"], height = bar_width, align='center', color=colors)    
    #add text
    for index, row in df.iterrows():
        x = row["start"] + row["duration"]/2
        y = row["componentName"]
        ax.annotate("      " + row["parameter"] , xy =(x, y), xytext =(x, y),  ha='center', va='bottom', rotation=90, fontsize = 12, color=colors[index])

    ax.invert_yaxis()
    
    #apply zoom
    if zoom != "":
        if zoom == "auto":
            import zoomHelper as zh
            xrange = zh.autoZoom(df["start"])
        else:
            xrange = zoom.split(",") 
            xrange = [ float(x) for x in xrange]
        ax.set_xbound(xrange)

    #Custom build the legend
    legend_handles = []
    legend_labels = []
    for e in events:
        if e.name not in legend_labels:
            legend_handles.append(mpatches.Patch(color=hp.eventsColors[e.name], label=e.name))
            legend_labels.append(e.name)
    
    legendcols = len(legend_labels)/2
    ax.legend(ncol = int(legendcols), handles = legend_handles, fontsize = 12,  bbox_to_anchor=(0., 1.02, 1., .102), loc=3, mode="expand", borderaxespad=0.)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Components")
    fig = plt.gcf()
    plt.show()
    
    return fig 

#saving the plot 
def saveplot_mp(fig, outPath):    
    fig.savefig(outPath, bbox_inches='tight')
    plt.close()

