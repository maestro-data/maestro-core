#!/usr/bin/env python
##################################################################################
# Visualize maestro-core logs 													 #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################


import math
import numpy as np
import glob
import re
import matplotlib
#matplotlib.use('MacOSX') 
#matplotlib.use('PS')
from matplotlib.font_manager import FontProperties
from collections import namedtuple
import helper as hp
import matplotlib.pyplot as plt
import csv
import os


import sys
from optparse import OptionParser


import matplotlib


"""

python3 vis.py --path check_pm_interlock.log 
python3 vis.py --path check_pm_interlock.log --zoom 0.08,0.1 --plotlib plotly
python3 vis.py --path check_pm_interlock.log --zoom 0.08,0.1 --plotlib matplotlib --events mstro_cdo_declare,mstro_cdo_offer,mstro_cdo_withdraw,mstro_cdo_demand,mstro_cdo_dispose

"""

parser = OptionParser()

parser.add_option("-f", "--path", dest="path",
                  help="data file path", default=".")

parser.add_option("-o", "--outpath", dest="outpath",
                  help="output figure directory path", default="figure.pdf")


parser.add_option("--zoom", dest="tlimits",
                  help="start-time,end-time", default="")

parser.add_option("--plotlib", dest="plib",
                  help="Specify the plotting library, either plotly (default) or matplotlib", default="plotly")

parser.add_option("--events", dest="events",
                  help = "Define which events to grep - either all (default) or a list from Supported Events" +
                  "".join("," + e for e in hp.eventsDict.keys()), default="all") 
                  
parser.add_option("--sync", dest="sync",
                  help = "align the start time of all components", default="off") 

parser.add_option("-t","--type", dest="type",
                  help = "a debug log file or telemetry log ", default="debug") 


(options, args) = parser.parse_args()


eventsOfInterest =  hp.parseInputEvents(options.events) 

#Synchronise components start time
if options.sync in {"on", "ON", "yes", "YES", "y", "Y", "true", "True"}:
    sync = True
else:
    sync = False

if options.type in {"debug", "DEBUG", "D", "d", "Debug"}:
    components, events, startTime = hp.getEventsFromFile(options.path, eventsOfInterest, sync)
    print(startTime)
elif options.type in {"telemetry", "t"}:
    events = hp.readTelemetryEvents(options.path, 0, sync)
elif options.type in {"both", "b", "combined"}: 
    components, events, startTime = hp.getEventsFromFile(options.path, eventsOfInterest, sync)
    # startTime should be keyed by component names if we need to sync ...
    if sync:
        startTime = hp.startTimePID2Name(startTime, components)
    print(startTime)
    telemetryEvents = hp.readTelemetryEvents(options.path, startTime, sync)
    #concatenate both event lists 
    events = events + telemetryEvents
    #print(events)
else:
    print(hp.bcolors.FAIL + "Error: unrecongnized file type \n" + hp.bcolors.ENDC)
    quit()


print("Drawing ...")
fig = hp.plotEvents(events, zoom=options.tlimits, plib = options.plib)


#saving the plot 
hp.savePlot(fig, options.outpath, options.plib)
