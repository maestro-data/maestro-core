##################################################################################
#Helper functions to read raw input files and identify best combinations         #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################

from __future__ import annotations
import glob
import re
import numpy as np
import csv
import math

import os
from collections import namedtuple
from dataclasses import dataclass, field
from tqdm import tqdm


import plot_px as pxh
import plot_mp as mph

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


#application times regex dictionary
regexTime = {}
regexTime["declare"] = "Throughput \(declare/offer\): "
regexTime["withdraw"] = "Throughput \(withdraw/dispose\): "
regexTime["require"] = "Throughput \(declare/require\): "


regex = {}
regex["gid"] = "\w+-\w+-\w+-\w+-\w+."
regex["declare"] = "mstro_pm_cdo_registry_declare.*"
regex["offer"] = "mstro_pm_cdo_registry__set_state.*to 16 \(OFFERED,POOLED\)"
regex["require"] = "mstro_pm_cdo_registry__set_state.*to 32 \(REQUIRED,POOLED\)"
regex["demand"] = "mstro_pm_cdo_registry__set_state.*DEMANDED"
regex["withdraw"] = "mstro_pm_cdo_registry__set_state.*WITHDRAWN"
regex["trans-init"] = ".*Sent initiate-transfer for.*"
regex["trans-comp"] = ".*TRANSFER COMPLETION from app.*"


eventsColors = {}
eventsColors["mstro_*_init"] = 'gray'
eventsColors["mstro_*_finalize"] = 'black'
eventsColors["mstro_cdo_declare"] = "#bcd4e6" # blue
eventsColors["mstro_cdo_offer"] = '#6cbfee' # blue
eventsColors["mstro_cdo_demand"] = '#1f75fe' # blue
eventsColors["mstro_cdo_withdraw"] = '#00008b' #blue
eventsColors["mstro_cdo_dispose"] = '#1f305e' #blue
#https://htmlcolorcodes.com/colors/shades-of-green/
eventsColors["mstro_pm_attach"] = '#7FFFD4' #green
eventsColors["mstro_pc_handle_msg"] = '#088F8F' #green
eventsColors["mstro_pm_handle_msg"] = '#d20538' #red
eventsColors["mstro_pm__handle_join"] = '#d20538' #red
eventsColors["mstro_pm_app_register"] = '#d20538' #red
eventsColors["mstro_pm__handle_"] = '#d20538' #red
eventsColors["mstro_pm__handle_transfer_completed"] = '#454B1B' #green
eventsColors["mstro_pc_app_befriend"] = '#454B1B' #green
eventsColors["mstro_pc__handle_initiate_transfer"] = '#003366' #very dark blue
"""
Amber   #FFBF00 rgb(255, 191, 0)
Apricot #FBCEB1 rgb(251, 206, 177)
Bisque  #F2D2BD rgb(242, 210, 189)
Bronze  #CD7F32 rgb(205, 127, 50)
Buff    #DAA06D rgb(218, 160, 109)
Burnt Orange    #CC5500 rgb(204, 85, 0)
Burnt Sienna    #E97451 rgb(233, 116, 81)
Butterscotch    #E3963E rgb(227, 150, 62)
"""
#telemetry events 
eventsColors["JOIN"] = '#FFBF00' #Amber
eventsColors["LEAVE"] = '#FBCEB1' #Apricot
eventsColors["DECLARE"] = '#F2D2BD' #Bisque
eventsColors["SEAL"] = '#CD7F32' #Bronze



@dataclass(unsafe_hash=True)
class CDO:
    '''Class for keeping track of an item in inventory.'''
    gid: str
    declare: float = field(init=False, default=0)
    offer: float = field(init=False, default=0)
    demand: float = field(init=False, default=0)
    transport_init: float = field(init=False, default=0)
    transport_complete: float = field(init=False, default=0)
    withdraw: float = field(init=False, default=0)
    require: float  = field(init=False, default=0)


    def require_demand(self) -> float:
        return self.demand - self.require

    def declare_require(self) -> float:
        return self.require - self.declare

    def demand_to_transport(self) -> float:
        return self.transport_init - self.demand

    def demand_to_transportComplete(self) -> float:
        return self.transport_complete - self.demand

    def offer_to_withdraw(self) -> float:
        return self.withdraw - self.offer

    def lifetime(self) -> float:
        return self.withdraw - self.declare


def calculateRequireDemand(cdos):
    require_demand = []
    for key, cdo in cdos.items():
        v = cdo.require_demand()
        if v > 0:
            require_demand.append(v)

    return require_demand

def calculateDeclareRequire(cdos):
    declare_require = []
    for key, cdo in cdos.items():
        v = cdo.declare_require()
        if v > 0:
            declare_require.append(v)

    return declare_require

def calculateDemandTransport(cdos):
    demand_to_transport = []
    for key, cdo in cdos.items():
        v = cdo.demand_to_transport()
        if v > 0:
            demand_to_transport.append(v)

    return demand_to_transport

def calculateDemandTransportComplete(cdos):
    demand_to_transportComplete = []
    for key, cdo in cdos.items():
        v = cdo.demand_to_transportComplete()
        if v > 0:
            demand_to_transportComplete.append(v)

    return demand_to_transportComplete


def calculateLifeTime(cdos):
    lifetime = []
    for key, cdo in cdos.items():
        v = cdo.lifetime()
        if v > 0:
            lifetime.append(cdo.lifetime())

    return lifetime

def getCDOs(filename):

    all_gid = []
    cdos = {}
    i = 0
    n_lines = -1
    for line in open(filename, errors='ignore'):
        n_lines = n_lines + 1
        match = re.match( ".*" + r"(?P<gid>"+ regex["gid"] + ")" + ".*", line)
        if match:
            #print(line)
            gid = match.group("gid")

            if gid not in all_gid:
                i = i + 1
                cdos[gid] = CDO(gid = gid)
                all_gid.append(gid)
                if i%1000 == 0:
                    print("read " +str(i)+ " cdos after " +str(n_lines) + " lines")

            #get delare time 
            if cdos[gid].declare == 0:
                declare_time = getTime(line, regex["declare"])
                if declare_time:
                    cdos[gid].declare = declare_time
                    continue
            #get offer time
            if cdos[gid].offer == 0:
                offer_time = getTime(line, regex["offer"])
                if offer_time:
                    cdos[gid].offer = offer_time
                    continue
            #get demand time
            if cdos[gid].demand == 0:
                demand_time = getTime(line,regex["demand"])
                if demand_time:
                    cdos[gid].demand = demand_time
                    continue
            #get transport_init
            if cdos[gid].transport_init == 0:
                transport_init = getTime(line, regex["trans-init"])
                if transport_init:
                    cdos[gid].transport_init= transport_init
                    continue
            #get transport_compelete
            if cdos[gid].transport_complete == 0 :
                transport_complete = getTime(line, regex["trans-comp"])
                if transport_complete:
                    cdos[gid].transport_complete = transport_complete
                    continue
            #get withdraw time
            if cdos[gid].withdraw == 0 :
                withdraw_time = getTime(line, regex["withdraw"])
                if withdraw_time:
                    cdos[gid].withdraw = withdraw_time
                    continue
            #get require time
            if cdos[gid].require == 0 :
                require_time = getTime(line, regex["require"])
                if require_time:
                    cdos[gid].require = require_time
    return cdos


def getTime(line, regex):
    match = re.match(".*\(\w+ \d+\) "+r"(?P<time>"+ intregex + "): " + regex, line)
    if match:
        time = float(match.group("time"))
        #print(time)
    else:
        time = None

    return time


units = {}
units["us"] = 1
units["ms"] = 1000
units["s"]  = 1000000

unitText = {}
unitText["us"] = "$\mu$s"
unitText["ms"] = "$ms$"
unitText["s"]  = "$s$"


floatregex = "[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?"
intregex = "\d+"
processregex = "t\d+"
nameregex = "\w+:\d+"

legend_labels = []

event = namedtuple("event", "name start duration processID appID componentName parameter")

eventPattern = namedtuple("eventPattern", "pattern parameter oneofakind")

eventsDict = {}
eventsDict["mstro_cdo_declare"] = eventPattern(pattern = "mstro_cdo_declare.*\(", parameter="cdo_name", oneofakind=False)
eventsDict["mstro_cdo_offer"] = eventPattern(pattern = "mstro_cdo_offer.*\(", parameter="cdo_name", oneofakind=False)
eventsDict["mstro_cdo_withdraw"] = eventPattern(pattern = "mstro_cdo_withdraw.*\(", parameter="cdo_name", oneofakind=False)
eventsDict["mstro_cdo_require"] = eventPattern(pattern = "mstro_cdo_require.*\(", parameter="cdo_name", oneofakind=False)
eventsDict["mstro_cdo_demand"] = eventPattern(pattern = "mstro_cdo_demand.*\(", parameter="cdo_name", oneofakind=False)
eventsDict["mstro_cdo_dispose"] = eventPattern(pattern = "mstro_cdo_dispose.*\(", parameter="cdo_name", oneofakind=False)
eventsDict["mstro_pc_handle_msg"] = eventPattern(pattern = "mstro_pc_handle_msg.*\(", parameter="msg_type", oneofakind=True)
eventsDict["mstro_pc__handle_initiate_transfer"] = eventPattern(pattern = "mstro_pc__handle_initiate_transfer\(", parameter="dst_app_cdo", oneofakind=True)
eventsDict["mstro_pc_app_befriend"] = eventPattern(pattern = "mstro_pc_app_befriend\(", parameter="friends", oneofakind=True)
eventsDict["mstro_*_init"] =  eventPattern(pattern = "mstro_(?!pm).*_init\(", parameter="", oneofakind=False)
eventsDict["mstro_pm_attach"] = eventPattern(pattern = "mstro_pm_attach.*\(", parameter="", oneofakind=False)
eventsDict["mstro_*_finalize"] = eventPattern(pattern = "mstro_.*finalize\(", parameter="", oneofakind=False)
eventsDict["mstro_pm_handle_msg"] = eventPattern(pattern = "mstro_pm_handle_msg.*\(", parameter="msg_type", oneofakind=True)
eventsDict["mstro_pm_app_register"] = eventPattern(pattern = "mstro_pm_app_register\(", parameter="registered_app", oneofakind=True)
eventsDict["mstro_pm__handle_join"] = eventPattern(pattern = "mstro_pm__handle_join\(", parameter="caller", oneofakind=True)
eventsDict["mstro_pm__handle_transfer_completed"] = eventPattern(pattern = "mstro_pm__handle_transfer_completed\(", parameter="fromapp", oneofakind=True)
#eventsDict["mstro_pm__handle_"] = eventPattern(pattern = "mstro_pm__handle_.*\(", parameter="all", oneofakind=True)





parameters = {}
parameters["cdo_name"] = r".*CDO `(?P<parameter>.*)'"
parameters["all"] = r".*\) (?P<parameter>.*)"
parameters["fromapp"] = r".*\) TRANSFER COMPLETION (?P<parameter>from app \d+)"
parameters["caller"] = r".*Caller (?P<parameter>.*:\d+)"
parameters["registered_app"] = r".* (?P<parameter>Registered app \d+ for .*:\d+)"
parameters["msg_type"] = r".* ... unpacked msg: type mstro.pool.MstroMsg \((?P<parameter>.*)\)"
parameters["friends"] = r".*\) (?P<parameter>.*)"
parameters["dst_app_cdo"] = r".*\) Initiating transfer.*(?P<parameter>dst app \d+ of CDO \w+)"

def compileGeneralPatterns():
    compiledPatterns = {}
    #compile a regex for processID
    cpattern  = re.compile(".*\[.*\] :\d+ \d+ " + r"(?P<ProcessID>"+ processregex + ") ")
    compiledPatterns["processID"] = cpattern
    #compile a regex for startTime
    cpattern  = re.compile(".* " + r"(?P<time>"+ intregex + "): " + "mstro_pm_start\(")
    compiledPatterns["startTime"] = cpattern
    
    return compiledPatterns

def compileComponentNamePatterns(key, compiledPatterns):
    if ("componentName"+key) not in compiledPatterns:
        cpattern  = re.compile(".*\[.*\] " +  r"(?P<componentName>"+ nameregex + ") \d+ " + key)
        compiledPatterns["componentName"+key] = cpattern 

    if("startTime"+key) not in compiledPatterns:
        cpattern  = re.compile(".* "+key+" \(.*\) " + r"(?P<time>"+ intregex + "): " + "mstro_init\(.*\) Maestro Middleware startup:")
        compiledPatterns["startTime"+key] = cpattern 
    return compiledPatterns

def compileEventsRegexProcessID(eventsOfInterest, processID, compiledEvents, compiledEventsWithParams):

    compiledEvents[processID] = {}
    compiledEventsWithParams[processID] = {}
    for eOfInterest in eventsOfInterest:
        eventNamePattern = eventsDict[eOfInterest].pattern
        if eventsDict[eOfInterest].parameter == "":
            compiledEvents[processID][eOfInterest] = []
            pattern  = re.compile(".*\[.*\] :\d+ \d+ " + processID + " .* " + r"(?P<time>"+ intregex + "): " + eventNamePattern)
            compiledEvents[processID][eOfInterest].append(pattern)
        else:
            parameterPattern = parameters[eventsDict[eOfInterest].parameter]
            #compile pattern once with PID and once with component name
            compiledEventsWithParams[processID][eOfInterest] = []
            pattern  = re.compile(".*\[.*\] :\d+ \d+ " + processID + " .* " + r"(?P<time>"+ intregex + "): " + eventNamePattern + parameterPattern)
            compiledEventsWithParams[processID][eOfInterest].append(pattern)
    return compiledEvents, compiledEventsWithParams

def compileEventsRegexCname(eventsOfInterest, processID, componentName, compiledEvents, compiledEventsWithParams):
    
    for eOfInterest in eventsOfInterest:
        eventNamePattern = eventsDict[eOfInterest].pattern
        if eventsDict[eOfInterest].parameter == "":
            compiledEvents[processID][eOfInterest] = []
            compiledEvents[processID][eOfInterest].append(re.compile(".*\[.*\] " + componentName +" .* " + r"(?P<time>"+ intregex + "): " + eventNamePattern))
        else:
            parameterPattern = parameters[eventsDict[eOfInterest].parameter]
            #compile pattern once with PID and once with component name
            compiledEventsWithParams[processID][eOfInterest] = []
            compiledEventsWithParams[processID][eOfInterest].append(re.compile(".*\[.*\] " + componentName +" .* " + r"(?P<time>"+ intregex + "): " + eventNamePattern + parameterPattern))
    return compiledEvents, compiledEventsWithParams


def aggregateEvents(aggList, newEvents):
    #check if newEvents is a list
    if isinstance(newEvents, list):
        #concatenate the two lists
        aggList = aggList + newEvents
    else:
        aggList.append(newEvents)
    return aggList


def addProcessID(line, components, compiledPatterns):
    newID = ""
    match = compiledPatterns["processID"].match(line)
    if match:
        ProcessID = match.group("ProcessID")
        if not ProcessID in components:
            components[ProcessID] = ""
            newID = ProcessID
    return components, newID



def addComponentName(line, components, compiledPatterns):
    newName = ""
    newID = ""
    for key in components.keys():
        # we did not find the component name yet
        if components[key] == "":
            #try match the name
            match = compiledPatterns["componentName"+key].match(line) 
            if match:
                componentName = match.group("componentName")
                components[key] = componentName
                newName = componentName
                newID = key
                #print("found id: " +  str(key) + " name: " + str(componentName))
                break
    return components , newName, newID

#find pm start time of pm
def pmStartTime(line, startTime, compiledPatterns):
    if startTime == 0:
        match = compiledPatterns["startTime"].match(line)
        if match:
            #print(line)
            startTime = float(match.group("time"))
    return startTime

def componentsStartTime(line, startTime, compiledPatterns):
    for key in startTime.keys():
        if startTime[key] == "":
            match = compiledPatterns["startTime"+key].match(line)
            if match:
                #print(line)
                startTime[key] = float(match.group("time"))
    return startTime
    
#start time
def findStartTime(line, startTime, compiledPatterns, sync):
    if sync: 
        return componentsStartTime(line, startTime, compiledPatterns)
    else:
        return pmStartTime(line, startTime, compiledPatterns)

def parseInputEvents(eventsString):
    eventsList = []
    if eventsString == "all":
        eventsList = eventsDict.keys()
    else:
        eventsList = eventsString.split(",")
    return eventsList

def getEventsFromFile(filename, EventsOfInterest, sync=False):

    components = {}
    #Synchronise components start time
    if sync:
        startTime = {}
    else:
        startTime = 0

    events = []

    compiledEvents = {}
    compiledEventsWithParams = {}
    
    # incompleteEvents to hold the events that spans a long duration of time, 
    #i.e. we need to read the whole file to create them
    incompleteEvents = {}

    #compile general events
    gPatterns = compileGeneralPatterns()
    #count number of lines 
    num_lines = sum(1 for line in open(filename, 'r', errors= 'ignore'))
    #open file and read line by line
    with tqdm(total=num_lines) as pbar:
        for line in open(filename, 'r', errors= 'ignore'):
            #do we see a new process ID?
            components, newID = addProcessID(line, components, gPatterns)
            if  newID != "":
                compiledEvents, compiledEventsWithParams = compileEventsRegexProcessID(EventsOfInterest, newID, compiledEvents, compiledEventsWithParams) #compile for the last one only
                gPatterns = compileComponentNamePatterns(newID, gPatterns)
                if sync:
                    startTime[newID] = "" # add key to starttime 
            #maybe we are missing any component names 
            components, newName, newID = addComponentName(line, components, gPatterns)
            if  newName != "":
                compiledEvents, compiledEventsWithParams = compileEventsRegexCname(EventsOfInterest, newID, newName, compiledEvents, compiledEventsWithParams) #compile for the last one only
            # if start time = 0, try to find the start time of the PM
            startTime = findStartTime(line, startTime, gPatterns,sync) 
            event, incompleteEvents = matchEventswithParameters(line, incompleteEvents, components, compiledEventsWithParams, startTime, sync)
            pbar.update(1)
            if event != []:
                events.append(event)
                continue
            event, incompleteEvents = matchEvents(line, incompleteEvents, components, compiledEvents, startTime, sync)
            if event != []:
                events.append(event)
            

    iEvents = processIncompleteEvents(incompleteEvents, components, startTime, sync)
    events = events + iEvents
    return components, events, startTime


def matchEvents(line, incompleteEvents, components, compiledEvents, startTime, sync):
    eventslist = []
    for processID in components.keys(): # over components
        param = ""
        for eventName in compiledEvents[processID].keys(): # over events
            oneofakind = eventsDict[eventName].oneofakind
            for pattern in compiledEvents[processID][eventName]: # over patterns per event ... one with processID and one with componentName
                match = pattern.match(line)
                if match:
                    #print(line)
                    t  = float(match.group("time"))
                    eventslist, incompleteEvents = createEventOrIncomplete(processID, components[processID], eventName, t, param, oneofakind, incompleteEvents, startTime, sync)
                    break
    return eventslist, incompleteEvents

def matchEventswithParameters(line, incompleteEvents, components, compiledEventsWithParams, startTime, sync):
    eventslist = []
    for processID in components.keys(): # over components
        for eventName in compiledEventsWithParams[processID].keys(): # over events
            oneofakind = eventsDict[eventName].oneofakind
            for pattern in compiledEventsWithParams[processID][eventName]: # over patterns per event ... one with processID and one with componentName
                match = pattern.match(line)
                if match:
                    #print(line)
                    t  = float(match.group("time"))
                    param = match.group("parameter")
                    eventslist, incompleteEvents = createEventOrIncomplete(processID, components[processID], eventName, t, param, oneofakind, incompleteEvents, startTime, sync)
                    break
    return eventslist, incompleteEvents

def processIncompleteEvents(incompleteEvents, components, startTime, sync):
    nano = 1000000000
    events = []
    for key in incompleteEvents.keys():
        #decode the key
        tokens = key.split(";")
        processID = tokens[0]
        eventName = tokens[1]
        param = tokens[2]
        #create an event 
        start = np.min(incompleteEvents[key])
        end = np.max(incompleteEvents[key])
        duration = end -start 
        if duration == 0:
            duration = 100000 #default
        duration = duration/nano
        if sync:
            start = (start - startTime[processID])/nano
        else:
            start = (start - startTime)/nano
        events.append(event(name= eventName, start = start, duration=duration, processID=processID, appID="", componentName=components[processID], parameter=param))
    return events

def createEventOrIncomplete(processID, componentName, eventName, time, param, oneofakind, incompleteEvents,startTime, sync):
    nano = 1000000000
    eventslist = []
    if oneofakind == True:
        # create the event directly
        duration = 100000 #default
        duration = duration/nano
        if sync:
            time = (time - startTime[processID])/nano
        else:
            time = (time - startTime)/nano
        eventslist = event(name = eventName, start = time, duration = duration, processID= processID, appID ="", componentName = componentName, parameter=param)
    else:
        #create an incompleteEvent key
        eventKey = processID+";"+ eventName+";"+param
        if not eventKey in incompleteEvents:
            temp = []
            temp.append(time)
            incompleteEvents[eventKey] = temp
        else:
            incompleteEvents[eventKey].append(time)
    return eventslist, incompleteEvents

def scaleDatabyFactor(data, factor):
    scaled  = []
    scaled.append([float(x) / factor for x in data])
    return scaled


def scaleDataUnit(data):
    unit = r' $(ns)$'
    factor = 1

    maxvalue = np.max(data, initial=0)
    #print(maxvalue)

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        unit = r' $(\mu s)$'
        factor *= 1000

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        unit = r' $(ms)$'
        factor *= 1000

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        unit = r' $(s)$'
        factor *= 1000

    #print(magnitude)
    scaledData = scaleDatabyFactor(data, factor)

    return scaledData, unit


##################### telemetry related functions ###########################
def setStartTime(startTime,time, componentName, sync):
    if sync:
        if componentName not in startTime:
            startTime[componentName] = time
    else:
        if startTime == 0:
            startTime = time

    return startTime

def startTimePID2Name(startTimesPID, components):
    startTimesName = {}
    for key in components.keys():
        startTimesName[components[key]] = startTimesPID[key]
    return startTimesName

def readTelemetryEvents(filename, g_startTime=0, sync =False):
    nano = 1000000000
    apps = {}
    events = []
    #compile the pattern
    pattern = re.compile(r"(?P<time>"+ intregex +r"),(?P<type>\w+),\d+, { (?P<appID>\d+)(,\"(?P<name>.*)\")?\s?}" )
    #If we have start time given to us, we use it
    if g_startTime != 0:
        startTime = g_startTime
    else:
        if sync:
            startTime = {}
        else:
            startTime = 0

    for line in open(filename, errors='ignore'):
        match = pattern.match(line)
        if match:
            print(line)
            
            # create dict apps with appID -- with JOIN events 
            eventName = match.group("type")
            appID = match.group("appID")
            start = float(match.group("time"))
            #fake a very small duration to register the event
            duration = 100000 / nano  
            
            if  eventName == "JOIN":
                name = match.group("name")                
                apps[appID] = name
                startTime = setStartTime(startTime, start, apps[appID], sync)
                if sync:
                    start = (start - startTime[apps[appID]])/nano
                else:
                    start = (start - startTime)/nano
                events.append(event(name = eventName, start = start, duration = duration, processID= 0, appID = appID, componentName = apps[appID], parameter= "app " + appID))

            if eventName == "LEAVE":
                name = apps[appID]
                if sync:
                    start = (start - startTime[apps[appID]])/nano
                else:
                    start = (start - startTime)/nano
                events.append(event(name = eventName, start = start, duration = duration, processID= 0, appID = appID, componentName = apps[appID], parameter= "app " + appID))


            if eventName == "DECLARE" or eventName == "SEAL":
                cdoName =  match.group("name")
                name = apps[appID]
                if sync:
                    start = (start - startTime[apps[appID]])/nano
                else:
                    start = (start - startTime)/nano
                events.append(event(name = eventName, start = start, duration = duration, processID= 0, appID = appID, componentName = apps[appID], parameter= cdoName))

    return events


def formatValue(value):
    magnitude = 1024

    label = ""
    if value >= magnitude:
        value = value/magnitude
        label = "k"

    if value >= magnitude:
        value = value/magnitude
        label = "M"

    if value >= magnitude:
        value = value/magnitude
        label = "G"

    return str(int(value)) + label

#convert dict to list
def dict2List(dict):

    dictlist = []

    for key in dict.keys():

        dictlist.append(dict[key])

    return dictlist



########### ploting interface ########################
def savePlot(fig, outPath, plib="plotly"):
    if plib == "Plotly" or plib == "plotly":
        pxh.saveplot_px(fig, outPath)
    elif plib == "Matplotlib" or plib == "matplotlib":
        mph.saveplot_mp(fig, outPath)
    else:
        print("Error: unsupported plotting library")


def plotEvents(events, zoom = "", plib="plotly"):
    fig = []
    if plib == "Plotly" or plib == "plotly":
        fig = pxh.plotEvents_px(events, zoom)
    elif plib == "Matplotlib" or plib == "matplotlib":
        fig = mph.plotEvents_mp(events, zoom)
    else:
        print("Error: unsupported plotting library")
    return fig

