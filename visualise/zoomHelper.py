# Copyright (C) 2022 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################
# auto zoom and segmenting/clustering input                                      #
##################################################################################

from sklearn.cluster import KMeans
from collections import Counter

import numpy as np

################### auto zoom #######################
def autoZoom(startTimes):
    data = np.sort(startTimes)
    km = KMeans()
    km.fit(data.reshape(-1,1)) 
    #print(km.cluster_centers_)
    #find clusters 
    labels = km.predict(data.reshape(-1,1))
    #count the size of each cluster
    clusters_length = Counter(labels)
    #find the largest cluster
    cluster_index = clusters_length.most_common()[0][0]
    # find the members of largest cluster
    data_idx = np.where(labels == cluster_index)[0]
    #print(labels)
    #return the min and max time of this cluster to zoom in 
    return [np.min(data[data_idx]), np.max(data[data_idx])]
    




    # #finding difference
    # diff = data[1:] - data[:-1]
    # # diff = [x - data[i - 1] for i, x in enumerate(data)][1:]
    # sorted = np.sort(diff)
    # #print(sorted[-5:])
    # #print(sorted[-5])
    # #print(diff)
    # ind = np.where(diff > sorted[-5])[0]
    # #print(ind)
    # #print(data[ind])

    # return [data[ind[3]]-sorted[-4], data[ind[3]]+sorted[-4]]

    # data = data.reshape(-1, 1)
    #print(data)
    # kde = KernelDensity(kernel='tophat', bandwidth=1).fit(data)
    # s = linspace(0,50)
    # e = kde.score_samples(s.reshape(-1,1))
    
    # mi, ma = argrelextrema(e, np.less)[0], argrelextrema(e, np.greater)[0]
    # print("Minima:", s[mi])
    # print("Maxima:", s[ma])
    # print("mi: ", mi, "ma: ", ma)
    # limits = data[data < s[ma][0], data[(data >= s[ma][0]) * (data <= s[ma][0])], data[data >= s[ma][0]]]
    # #limits = data[data < 0, data[(data >= 0) * (data <= mi[1])], data[data >= mi[1]]
    # ms = MeanShift(bandwidth=None, bin_seeding=True)
    # ms.fit(data)
    # labels = ms.labels_
    # cluster_centers = ms.cluster_centers_

    # labels_unique = np.unique(labels)
    # n_clusters_ = len(labels_unique)

    # print("number of estimated clusters : %d" % n_clusters_)
    # print(labels)
