##################################################################################
# Extract information from maestro-core logs and create histograms	             #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################

import math
import numpy as np
import glob
import re
import matplotlib
#matplotlib.use('MacOSX') 
#matplotlib.use('PS')
from matplotlib.font_manager import FontProperties
from collections import namedtuple
import helper as hp
import matplotlib.pyplot as plt
import csv
import os


import sys
from optparse import OptionParser


import matplotlib

parser = OptionParser()

parser.add_option("-f", "--path", dest="path",
                  help="data file path", default=".",  metavar="FILE")

parser.add_option("-o", "--outpath", dest="outpath",
                  help="figure directory path", default="figure.pdf",  metavar="FILE")


parser.add_option("--zoom", dest="tlimits",
                  help="start-time,end-time", default="",  metavar="FILE")

(options, args) = parser.parse_args()




cdos = hp.getCDOs(options.path)
print("completed reading cdos")

require_demand = hp.calculateRequireDemand(cdos)
print("completed calculating require_demand")

declare_require = hp.calculateDeclareRequire(cdos)
print("completed calculating declare_require")

demand_to_transport = hp.calculateDemandTransport(cdos)
print("completed calculating demand_to_transport")

demand_to_transportComplete = hp.calculateDemandTransportComplete(cdos)
print("completed calculating demand_to_transportComplete")

lifetime = hp.calculateLifeTime(cdos)
print("completed calculating lifetime")

# print(cdos)
# print("require_demand")
# print(require_demand)

# print("declare_require")
# print(declare_require)

# print("lifetime")
# print(lifetime)

n_cdos = len(cdos)

print("#CDOs = " + str(n_cdos))
##################################################################################################
matplotlib.rcParams.update({'font.size': 20})
fig2, ax2 = plt.subplots()
organized_data = []
labels = []
organized_data.append(require_demand)
organized_data.append(declare_require)
organized_data.append(demand_to_transport)
organized_data.append(demand_to_transportComplete)
organized_data.append(lifetime)

labels.append("require-demand(#" +str(len(require_demand))+")")
labels.append("declare-require(#" +str(len(demand_to_transport))+")")
labels.append("demand-transport init(#" +str(len(demand_to_transport))+")")
labels.append("demand-transport Complete(#" +str(len(demand_to_transportComplete))+")")
labels.append("life time(#" +str(len(lifetime))+")")

bd = ax2.boxplot(organized_data,whis=np.inf)
for patch in bd['boxes']:
	patch.set(linewidth=3)
for patch in bd['medians']: 
	patch.set(linewidth=3)
ax2.set_xticklabels(labels)
plt.xticks(rotation=90,  ha='center')
ax2.set_xlabel("#CDOs = " + str(n_cdos))
ax2.set_ylabel('Time (ns)')
#ax2.set_autoscaley_on(False)
#ax2.tick_params(axis = 'x', labelsize = 16)

#plt.autoscale(True)
plt.savefig(options.path+'_box.pdf', bbox_inches='tight')
################################################################################################
matplotlib.rcParams.update({'font.size': 20})
i = -1
for data in organized_data:
	i = i +1
	fig2, ax2 = plt.subplots()
	scaledData, unit = hp.scaleDataUnit(data)
	print(labels[i] + ": Average: " + str(np.mean(scaledData)) + " median: " + str(np.median(scaledData)) + unit)
	bd = ax2.boxplot(scaledData, showfliers=False) # whis=inf
	for patch in bd['boxes']:
		patch.set(linewidth=3)
	for patch in bd['medians']: 
		patch.set(linewidth=3)
	ax2.set_xticklabels([labels[i]])
	#plt.xticks(rotation=90,  ha='center')
	ax2.set_xlabel("#CDOs = " + str(n_cdos))
	ax2.set_ylabel('Time ' + unit)
	plt.savefig(options.path+'_box_'+labels[i]+'.pdf', bbox_inches='tight')
################################################################################################
matplotlib.rcParams.update({'font.size': 20})
plt.style.use('ggplot')
i = -1
for data in organized_data:
	i = i +1
	scaledData, unit = hp.scaleDataUnit(data)
	fig2, ax2 = plt.subplots()
	bd = ax2.hist(scaledData,bins='auto') #bins='fd'
	#ax2.set_xticklabels([labels[i]])
	plt.xticks(rotation=90,  ha='center')
	ax2.set_title(labels[i])
	ax2.set_xlabel("Time " + unit)
	#ax2.set_ylabel('Time (ns)')
	plt.savefig(options.path+'_hist_'+labels[i]+'.pdf', bbox_inches='tight')
     