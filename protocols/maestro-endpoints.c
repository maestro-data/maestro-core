/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Endpoint (De-)Serialization
 **/
/*
 * Copyright (C) 2021-2022 Hewlett-Packard (Schweiz) GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro-endpoints.h"
#include <stdlib.h>
#include "maestro/logging.h"


#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> 
#include <arpa/inet.h>

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)


mstro_status
mstro_ep_fi_to_ep(const struct fi_info *fi, struct fid_ep *ep,
                  Mstro__Endpoint **result)
{
  mstro_status s=MSTRO_UNIMPL;
  
  if(fi==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;

  Mstro__Endpoint *me = malloc(sizeof(Mstro__Endpoint));
  if(me==NULL) {
    s=MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    mstro__endpoint__init(me);
    me->proto_case = MSTRO__ENDPOINT__PROTO_OFIPROTO;
    me->addr_case  = MSTRO__ENDPOINT__ADDR_OFIADDR;
  }

  /* set endpoint protocol */
  switch(fi->ep_attr->protocol) {
    case FI_PROTO_UNSPEC:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__UNSPEC; break;
    case FI_PROTO_RDMA_CM_IB_RC:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__RDMA_CM_IB_RC; break;
    case FI_PROTO_IWARP:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__IWARP; break;
    case FI_PROTO_IB_UD:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__IB_UD; break;
    case FI_PROTO_PSMX:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__PSMX; break;
    case FI_PROTO_UDP:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__UDP; break;
    case FI_PROTO_SOCK_TCP:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__SOCK_TCP; break;
    case FI_PROTO_MXM:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__MXM; break;
    case FI_PROTO_IWARP_RDM:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__IWARP_RDM; break;
    case FI_PROTO_IB_RDM:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__IB_RDM; break;
    case FI_PROTO_GNI:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__GNI; break;
    case FI_PROTO_RXM:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__RXM; break;
    case FI_PROTO_RXD:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__RXD; break;
    case FI_PROTO_MLX:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__MLX; break;
    case FI_PROTO_NETWORKDIRECT:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__NETWORKDIRECT; break;
    case FI_PROTO_PSMX2:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__PSMX2; break;
    case FI_PROTO_SHM:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__SHM; break;
    case FI_PROTO_MRAIL:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__MRAIL; break;
    case FI_PROTO_RSTREAM:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__RSTREAM; break;
    case FI_PROTO_RDMA_CM_IB_XRC:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__RDMA_CM_IB_XRC; break;
    case FI_PROTO_EFA:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__EFA; break;
    case FI_PROTO_PSMX3:
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__PSMX3; break;
    case FI_PROTO_CXI:
    /*CXI and CXI_COMPAT are the same due to the compatibility issue in 1.15.2*/
    #if FI_VERSION_GE(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
    #define FI_PROTO_CXI_COMPAT (FI_PROTO_PSMX3+2)
    case FI_PROTO_CXI_COMPAT:
    #endif
      me->ofiproto = MSTRO__OFI_ENDPOINT_KIND__CXI; break;
    default:
      ERR("Unsupported OFI protocol: %d (%s)\n",
          fi->ep_attr->protocol,
          fi_tostr(&fi->ep_attr->protocol, FI_TYPE_PROTOCOL));
      s=MSTRO_UNIMPL;
      goto BAILOUT;
  }
  
  Mstro__OfiAddr *addr = malloc(sizeof(Mstro__OfiAddr));
  if(addr==NULL) {
    s=MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    mstro__ofi_addr__init(addr);
    me->ofiaddr = addr;
  }


  switch(fi->addr_format) {
    case FI_FORMAT_UNSPEC:
      ERR("Unsupported address format FI_FORMAT_UNSPEC\n");
      s=MSTRO_UNIMPL;
      goto BAILOUT;
    case FI_SOCKADDR: {
      ERR("Unusupported address format FI_SOCKADDR\n");
      s=MSTRO_UNIMPL;
      goto BAILOUT;
    }
    case FI_SOCKADDR_IN: {
      size_t addrlen = sizeof(struct sockaddr_in);
      struct sockaddr_in buf;
      int r = fi_getname(&ep->fid, &buf, &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_IN4;
      addr->in4 = malloc(sizeof(Mstro__AddrSockaddrIN4));
      if(addr->in4==NULL) {
        s=MSTRO_NOMEM;
        goto BAILOUT;
      } else {
        mstro__addr_sockaddr__in4__init(addr->in4);
        addr->in4->sin_family = buf.sin_family;
        addr->in4->sin_port   = buf.sin_port;
        addr->in4->sin_addr   = buf.sin_addr.s_addr;
      }
      break;
    }
    case FI_SOCKADDR_IN6: {
      size_t addrlen = sizeof(struct sockaddr_in6);
      struct sockaddr_in6 buf;
      int r = fi_getname(&ep->fid, &buf, &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_IN6;
      addr->in6 = malloc(sizeof(Mstro__AddrSockaddrIN6));
      if(addr->in6==NULL) {
        s=MSTRO_NOMEM;
        goto BAILOUT;
      } else {
        mstro__addr_sockaddr__in6__init(addr->in6);
        addr->in6->sin6_family = buf.sin6_family;
        addr->in6->sin6_port   = buf.sin6_port;
        addr->in6->sin6_flowinfo = buf.sin6_flowinfo;
        /* things are already in network byte order */
        addr->in6->sin6_addr_0 = (   buf.sin6_addr.s6_addr[0]
                                  | (buf.sin6_addr.s6_addr[1] <<  8)
                                  | (buf.sin6_addr.s6_addr[2] << 16)
                                  | (buf.sin6_addr.s6_addr[3] << 24));
        addr->in6->sin6_addr_1 = (   buf.sin6_addr.s6_addr[4]
                                  | (buf.sin6_addr.s6_addr[5] <<  8)
                                  | (buf.sin6_addr.s6_addr[6] << 16)
                                  | (buf.sin6_addr.s6_addr[7] << 24));
        addr->in6->sin6_scope_id = buf.sin6_scope_id;
      }
      break;
    }
    case FI_ADDR_GNI: {
      uint64_t buf[6];
      size_t addrlen = sizeof(buf);

      int r = fi_getname(&ep->fid, &(buf[0]), &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_GNI;
      addr->gni = malloc(sizeof(Mstro__AddrGNI));
      if(addr->gni==NULL) {
        s=MSTRO_NOMEM;
        goto BAILOUT;
      } else {
        mstro__addr_gni__init(addr->gni);
        addr->gni->a0 = buf[0];
        addr->gni->a1 = buf[1];
        addr->gni->a2 = buf[2];
        addr->gni->a3 = buf[3];
        addr->gni->a4 = buf[4];
        addr->gni->a5 = buf[5];
      }
      break;
      
    }
    case FI_ADDR_PSMX: {
      uint64_t buf[1];
      size_t addrlen = sizeof(buf);

      int r = fi_getname(&ep->fid, &(buf[0]), &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_PSMX;
      addr->psmx = buf[0];
      break;
    }
      
    case FI_ADDR_PSMX2: {
      uint64_t buf[2];
      size_t addrlen = sizeof(buf);

      int r = fi_getname(&ep->fid, &(buf[0]), &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_PSMX2;
      addr->psmx2 = malloc(sizeof(Mstro__AddrPSMX2));
      if(addr->psmx2==NULL) {
        s=MSTRO_NOMEM;
        goto BAILOUT;
      } else {
        mstro__addr_psmx2__init(addr->psmx2);
        addr->psmx2->a0 = buf[0];
        addr->psmx2->a1 = buf[1];
      }
      break;
    }
    case FI_ADDR_PSMX3: {
      uint64_t buf[2];
      size_t addrlen = sizeof(buf);

      int r = fi_getname(&ep->fid, &(buf[0]), &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_PSMX3;
      addr->psmx3 = malloc(sizeof(Mstro__AddrPSMX3));
      if(addr->psmx3==NULL) {
        s=MSTRO_NOMEM;
        goto BAILOUT;
      } else {
        mstro__addr_psmx3__init(addr->psmx3);
        addr->psmx3->a0 = buf[0];
        addr->psmx3->a1 = buf[1];
      }
      break;
    }
    case FI_ADDR_STR: {
      size_t addrlen=0;
      void *buf=NULL;
      int r = fi_getname(&ep->fid, buf, &addrlen);
      if(r!=0 && r!=-FI_ETOOSMALL) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      if(r==-FI_ETOOSMALL) {
        assert(addrlen>0);
        buf=malloc(addrlen);
        if(buf==NULL) {
          ERR("Failed to allocate %zu bytes for string-format address\n", addrlen);
          s=MSTRO_NOMEM;
          goto BAILOUT;
        }
      }
      assert(addrlen>0 && buf!=NULL);
      r = fi_getname(&ep->fid, buf, &addrlen);
      if(r!=0) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        free(buf);
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_STR;
      addr->str = buf;
      break;
    }
    /*CXI and CXI_COMPAT are the same due to the compatibility issue in 1.15.2*/
    #if FI_VERSION_GE(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
    #define FI_ADDR_CXI_COMPAT (FI_ADDR_PSMX3+1)
    case FI_ADDR_CXI_COMPAT:
    #endif
    case FI_ADDR_CXI: {
      uint64_t buf;
      size_t addrlen=sizeof(uint64_t);

      int r = fi_getname(&ep->fid, &buf, &addrlen);
      if(r!=0 && r!=-FI_ETOOSMALL) {
        ERR("Error obtaining the endpoint name (=local addr): %d (%s)\n",
            r, fi_strerror(-r));
        s=MSTRO_FAIL;
        goto BAILOUT;
      }
      if(r==-FI_ETOOSMALL) {
        ERR("Unexpected CXI addrlen: %d (expected: %d)\n", addrlen, sizeof(uint64_t));
        s=MSTRO_UNIMPL;
        goto BAILOUT;
      }
      addr->val_case = MSTRO__OFI_ADDR__VAL_CXI;
      addr->cxi = malloc(sizeof(Mstro__AddrCXI));
      if(addr->cxi==NULL) {
        s=MSTRO_NOMEM;
        goto BAILOUT;
      } else {
        mstro__addr_cxi__init(addr->cxi);
        addr->cxi->raw = buf;
      }
      break;
    }
      
      /* unimplemented: */
    case FI_SOCKADDR_IB:
    case FI_ADDR_BGQ:
    case FI_ADDR_MLX:
    case FI_ADDR_IB_UD:
    case FI_ADDR_EFA:
        ERR("Unimplemented address format: %d (%s)\n",
            fi->addr_format,
            fi_tostr(&fi->addr_format, FI_TYPE_ADDR_FORMAT));
        s=MSTRO_UNIMPL;
        goto BAILOUT;
    default:
      ERR("Unknown address format: %d (%s)\n",
          fi->addr_format,
          fi_tostr(&fi->addr_format, FI_TYPE_ADDR_FORMAT));
      s=MSTRO_UNIMPL;
      goto BAILOUT;
  }
  
  s=MSTRO_OK;

BAILOUT:
  if(s!=MSTRO_OK) {
    if(me) {
      if(me->ofiaddr)
        free(me->ofiaddr);
      free(me);
    }
  } else {
    *result = me;
  }
  return s;
}


void
mstro_ep__addr_describe(const Mstro__Endpoint *ep, char *buf, size_t buflen)
{
  assert(ep->addr_case==MSTRO__ENDPOINT__ADDR_OFIADDR); 
  assert(ep->ofiaddr!=NULL);

  assert(buf!=NULL);
  assert(buflen>4);
  size_t num_written;
  const size_t unabbrev_len = buflen-3;
  
  switch(ep->ofiaddr->val_case) {
    case MSTRO__OFI_ADDR__VAL__NOT_SET: 
      num_written = snprintf(buf,unabbrev_len,"(unset)");
      break;
    case MSTRO__OFI_ADDR__VAL_UNSPEC:
      num_written = snprintf(buf,unabbrev_len,"(unspecified)");
      break;
    case MSTRO__OFI_ADDR__VAL_IN4: {
      uint32_t addr = ntohl(ep->ofiaddr->in4->sin_addr);
      /* we don't print family as it needs to be AF_INET anyway */
      num_written = snprintf(buf,unabbrev_len,"IPv4 %d.%d.%d.%d:%d",
                             (addr & 0xff000000)>>24,
                             (addr & 0x00ff0000)>>16,
                             (addr & 0x0000ff00)>>8,
                             (addr & 0x000000ff)>>0,
                             ep->ofiaddr->in4->sin_port);
      break;
    }
    case MSTRO__OFI_ADDR__VAL_IN6: {
      uint64_t
          addr0 = ep->ofiaddr->in6->sin6_addr_0,
          addr1 = ep->ofiaddr->in6->sin6_addr_1;
      struct sockaddr_in6 tmp = { .sin6_family          = ep->ofiaddr->in6->sin6_family,
                                  .sin6_port            = ep->ofiaddr->in6->sin6_port,
                                  .sin6_flowinfo        = ep->ofiaddr->in6->sin6_flowinfo,
                                  .sin6_addr.s6_addr[0] = addr0 & 0x000000ff,
                                  .sin6_addr.s6_addr[1] = addr0 & 0x0000ff00,
                                  .sin6_addr.s6_addr[2] = addr0 & 0x00ff0000,
                                  .sin6_addr.s6_addr[3] = addr0 & 0xff000000,
                                  .sin6_addr.s6_addr[4] = addr1 & 0x000000ff,
                                  .sin6_addr.s6_addr[5] = addr1 & 0x0000ff00,
                                  .sin6_addr.s6_addr[6] = addr1 & 0x00ff0000,
                                  .sin6_addr.s6_addr[7] = addr1 & 0xff000000,
                                  .sin6_scope_id        = ep->ofiaddr->in6->sin6_scope_id};
      char astring[INET6_ADDRSTRLEN];
      
      inet_ntop(AF_INET6, &(tmp.sin6_addr), astring, INET6_ADDRSTRLEN);
      num_written = snprintf(buf,unabbrev_len,"IPv6 %s", astring);
      break;
    }
    case MSTRO__OFI_ADDR__VAL_GNI: {
      /* see _gnix_ep_name_to_str() */
      union gnix {
        uint64_t raw;
        struct {
          uint32_t dev_addr;
          uint32_t cdm_id;
        };
      };
      union gnix tmp = {.raw = ep->ofiaddr->gni->a0 };
      num_written = snprintf(buf,unabbrev_len,
                             "0x%08" PRIx32 ";0x%08" PRIx32 "",
                             tmp.dev_addr, tmp.cdm_id);
      break;
    }
    case MSTRO__OFI_ADDR__VAL_STR:
      num_written = snprintf(buf,unabbrev_len,"%s", ep->ofiaddr->str);
      break;
    case MSTRO__OFI_ADDR__VAL_CXI: {
      union cxi_addr_ {
        struct {
          uint32_t pid		: 9;
          uint32_t nic		: 20;
          uint16_t vni;  /*since libfabric 1.20*/
        };
        uint64_t raw;
      };
      union cxi_addr_ tmp = { .raw = ep->ofiaddr->cxi->raw };
      
      num_written = snprintf(buf,unabbrev_len,"%d:%d:%"PRIu16, tmp.nic, tmp.pid, tmp.vni);
      break;
    }
      
    case MSTRO__OFI_ADDR__VAL_SOCK:
    case MSTRO__OFI_ADDR__VAL_IB:
    case MSTRO__OFI_ADDR__VAL_PSMX:
    case MSTRO__OFI_ADDR__VAL_BGQ:
    case MSTRO__OFI_ADDR__VAL_MLX:
    case MSTRO__OFI_ADDR__VAL_PSMX2:
    case MSTRO__OFI_ADDR__VAL_IB_UD:
    case MSTRO__OFI_ADDR__VAL_EFA:
    case MSTRO__OFI_ADDR__VAL_PSMX3:
      num_written = snprintf(buf,unabbrev_len,"(FIXME)");
      break;
    default:
      ERR("Unsupported address type: %d\n", ep->ofiaddr->val_case);
      num_written = snprintf(buf,unabbrev_len,"(FIXME)");
      break;
  }
  if(num_written>=unabbrev_len) 
    strncpy(buf+unabbrev_len-1,"...",4);
  
  return;
}
      
