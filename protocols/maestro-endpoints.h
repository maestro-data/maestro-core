/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Endpoint (De-)Serialization
 **/
/*
 * Copyright (C) 2021 Hewlett-Packaged (Schweiz) GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_ENDPOINTS_H_
#define MAESTRO_ENDPOINTS_H_ 1


#include "rdma/fabric.h"
/* until CXI hits upstream: */
#define FI_FULL_VERSION(major,minor,revision) (((major) << 16) | ((minor) << 8) | (revision)) 
#if FI_VERSION_LT(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
#define FI_ADDR_CXI  (FI_ADDR_PSMX3 + 1)
#define FI_PROTO_CXI (FI_PROTO_PSMX3 + 1)
#endif

#include "rdma/fi_cm.h"

#include "maestro/i_base64.h"
#include "mstro_ep.pb-c.h"
#include "maestro/status.h"

/** Create a protobuf endpoint descriptor for the fi_info and (enabled) endpoint */
mstro_status
mstro_ep_fi_to_ep(const struct fi_info *fi, struct fid_ep *ep,
                  Mstro__Endpoint **result);

/** maximal buffer size needed to print an endpoint's address */
#define MSTRO_EP__ADDR_STR_MAX 512

/** create human-readable description of endpoint's address in @arg buf (truncating if @arg buflen is too small) */
void
mstro_ep__addr_describe(const Mstro__Endpoint *ep, char *buf, size_t buflen);

#define MSTRO_EP_DESC_STRMAX 1024


/** Bind @arg buf to a temporary buffer with a human-readable endpoint
 * description of @arg endpoint/@arg credential/@arg inforegion while
 * executing @body */
#define WITH_MSTRO_EP_CRED_REG_DESCRIPTION(buf,ep,cred,reg,prefix,body) \
  do {                                                                  \
    assert(ep->proto_case==MSTRO__ENDPOINT__PROTO_OFIPROTO);            \
    assert(ep->addr_case==MSTRO__ENDPOINT__ADDR_OFIADDR);               \
    assert(ep->ofiaddr!=NULL);                                          \
    assert(cred==NULL                                                   \
           || cred->val_case==MSTRO__OFI_CREDENTIAL__VAL__NOT_SET       \
           || (cred->val_case==MSTRO__OFI_CREDENTIAL__VAL_DRC && cred->drc!=NULL)\
           || (cred->val_case==MSTRO__OFI_CREDENTIAL__VAL_CXI));        \
    assert(reg!=NULL);                                                  \
                                                                        \
    char addrbuf_[MSTRO_EP__ADDR_STR_MAX];                              \
    mstro_ep__addr_describe(ep, addrbuf_, MSTRO_EP__ADDR_STR_MAX);      \
                                                                        \
    const size_t mrkeylen_ = 2*reg->raw_key.len + 1;                    \
    char mrkeybuf_[mrkeylen_];                                          \
    mrkeybuf_[0]='\0';                                                  \
    for(size_t i_=0; i_<reg->raw_key.len; i_++) {                       \
      snprintf(mrkeybuf_+2*i_, 3, "%02X",                               \
               reg->raw_key.data[i_]);                                  \
    }                                                                   \
                                                                        \
    do {                                                                \
      size_t prefix_l_=strlen(prefix);                                  \
      char buf[MSTRO_EP_DESC_STRMAX+prefix_l_];                         \
      uint32_t cred_val = 0;                                            \
      cred_val = (cred)?                                                 \
      (cred->val_case==MSTRO__OFI_CREDENTIAL__VAL_DRC)? cred->drc->credential: \
      (cred->val_case==MSTRO__OFI_CREDENTIAL__VAL_CXI)?cred->cxi->vni:0 \
      :0;                                                               \
      snprintf(buf, MSTRO_EP_DESC_STRMAX+prefix_l_,                     \
               "%s"                                                     \
               "proto %s addr %s "                                       \
               "(credential: %" PRIu32 ", mraddr %" PRIx64 ", mrkey %s)\n", \
               prefix,                                                  \
               (protobuf_c_enum_descriptor_get_value(                   \
                   &mstro__ofi_endpoint_kind__descriptor, ep->ofiproto) \
                ->name),                                                \
               addrbuf_,                                                \
               cred_val,                                                \
               reg->baseaddr,                                           \
               mrkeybuf_);                                              \
                                                                        \
      body;                                                             \
    } while(0);                                                         \
  } while(0)


/** Bind @arg buf to a temporary buffer with a human-readable endpoint
 * description of @arg ep while executing @arg body */
#define WITH_MSTRO_EP_DESCRIPTION(buf,ep,body) do {                     \
    const Mstro__Endpoint *ep_ = (ep)->pbep;                            \
    const Mstro__OfiMemoryRegion *omr_ = (ep)->inforeg;                 \
    const Mstro__OfiCredential *oc_ = (ep)->cred;                       \
    WITH_MSTRO_EP_CRED_REG_DESCRIPTION(buf,ep_,oc_,omr_,"",body);       \
  } while(0)

/** Bind @arg buf to a temporary buffer with a human-readable endpoint
 * description of @arg epl (an Maestro_EndpointList*) while executing
 * @arg body */
#define WITH_MSTRO_EPL_DESCRIPTION(buf,epl,body) \
  do {                                                                  \
    assert(epl->n_eps==epl->n_credentials);                             \
    assert(epl->n_eps==epl->n_inforegs);                                \
    for(size_t i_=0; i_<epl->n_eps; i_++) {                             \
      const Mstro__Endpoint *ep_ = epl->eps[i_];                        \
      const Mstro__OfiMemoryRegion *omr_ = epl->inforegs[i_];           \
      const Mstro__OfiCredential *oc_ = epl->credentials[i_];           \
      char prefix_[16];                                                  \
      snprintf(prefix_, 16, "EP %zu: ", i_);                             \
      WITH_MSTRO_EP_CRED_REG_DESCRIPTION(buf,ep_,oc_,omr_,prefix_,body); \
    }                                                                   \
  } while(0)


/** log a human-readable description of the endpoint list @arg el
 * value at index @arg idx with @arg loglevel under @arg logmodule
 * and with prefix @arg header */
#define MSTRO_EP__EL_DESCRIBE(loglevel,logmodule,header,el,idx) do {    \
    assert(el!=NULL);                                                   \
    assert(el->n_eps>idx); assert(el->n_inforegs>idx); assert(el->n_credentials>idx); \
    assert(el->eps!=NULL); assert(el->inforegs!=NULL); assert(el->credentials!=NULL); \
    assert(el->eps[idx]!=NULL); assert(el->inforegs[idx]!=NULL);        \
                                                                        \
    WITH_MSTRO_EP_CRED_REG_DESCRIPTION(                                 \
        buf, el->eps[idx], el->credentials[idx], el->inforegs[idx],     \
        "",                                                             \
        LOG(logmodule, loglevel, "%s: %s\n", header, buf););            \
  } while(0);

/** Bind @arg buf to a human-readable description of the endpoint list @arg el
 * value at index @arg idx while executing @arg body */
#define WITH_MSTRO_EPL_ENTRY_DESCRIPTION(buf,epl,idx,body) do {         \
    assert(epl!=NULL);                                                  \
    assert(epl->n_eps>idx); assert(epl->eps!=NULL);                     \
    assert(epl->n_inforegs>idx); assert(epl->inforegs!=NULL);           \
    assert(epl->n_credentials>idx); assert(epl->credentials!=NULL);     \
    assert(epl->eps[idx]!=NULL); assert(epl->inforegs[idx]!=NULL);      \
                                                                        \
    WITH_MSTRO_EP_CRED_REG_DESCRIPTION(                                 \
        buf, epl->eps[idx], epl->credentials[idx], epl->inforegs[idx],\
        "",                                                             \
        body);                                                          \
  } while(0);



#endif
