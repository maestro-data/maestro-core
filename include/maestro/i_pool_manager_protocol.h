/* -*- mode:c -*- */
/** @file
 ** @brief Abstract core lib <-> pool manager protocol
 **
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MSTRO_POOL_MANAGER_PROTOCOL_H_
#define MSTRO_POOL_MANAGER_PROTOCOL_H_ 1

#include <stdalign.h>

#include "maestro.h"
#include "maestro/core.h"

#include <rdma/fabric.h> // for fi_addr_t only


/** every implementation will need to provide an abstraction for an endpoint */
struct mstro_endpoint;

#include "maestro/i_cdo.h"
#include "maestro/i_event.h"

#include "protocols/mstro_pool.pb-c.h"

/*** Protobuf-based messaging between pool participants */


/** Normal pool manager messages are 'short' messages. These are their
 * envelopes, just big enough to fit all protobuf-defined message types and
 * a bit of payload. protobuf serialization will raise an error if this is too small.
 *
 * Larger messages can be allocated by self-extension of the structure.
 *
 * Note that the transport layer will only send the actually used
 * amount of data (payload_size), so the size definition should be
 * tuned for memory usage only, not network data transfer size.
 */
/* it would be nice to have a formula based on alignof and sizeof here: */
#define MSTRO_MSG_SHORT_SIZE ((1<<15) - 16 - 48)

/** Envelope is either small message or allocation-time extended. */
struct mstro_msg_envelope {
  /* packed message data */
  uint64_t                            size;                        /**< size of data allocation */
  uint64_t                            payload_size;                /**< size of valid octets in data field */
  uint64_t                            tag;                         /**< possibly a tag to use for communication (default: 0 for no tag) */
  const   ProtobufCMessageDescriptor *descriptor;                  /**< Introspection: what has been packed here? */
  struct mstro_endpoint        *ep;                          /**< endpoint that this message is associated with. E.g., which one did it come in on? */
  /* possible padding in this region */
  _Alignas(max_align_t) uint8_t       data[MSTRO_MSG_SHORT_SIZE];  /** data region, minimum: a small msg */
};

/** check whether a message envelope is small  */
#define MSTRO_MSG_IS_SHORT(msgenv) (msgenv!=NULL && msgenv->size<=MSTRO_MSG_SHORT_SIZE)

/** check whether a message envelope is large  */
#define MSTRO_MSG_IS_LONG(msgenv)  (msgenv!=NULL && msgenv->size>MSTRO_MSG_SHORT_SIZE)

/** check whether message needs tagged communication */
#define MSTRO_MSG_HAS_TAG(msgenv) (msgenv!=NULL && msgenv->tag!=0)
    
mstro_status
mstro_msg_envelope_allocate(struct mstro_msg_envelope **res);
mstro_status
mstro_msg_envelope_free(struct mstro_msg_envelope *e);


/** Take protobuf-based message and pack it into a message envelope
 * and send it to @arg TARGET. The @arg msg argument can be a
 * local-scope object at the call-site of this function (it will be
 * serialized immediately on entry and not referenced afterwards) */
mstro_status
mstro_pmp_send_nowait(mstro_app_id target, const Mstro__Pool__MstroMsg *msg);

/** Take protobuf-based message and pack it into a message envelope
 * and send it to the the ep/addr designated. The @arg msg argument can be a
 * local-scope object at the call-site of this function (it will be
 * serialized immediately on entry and not referenced afterwards) */
mstro_status
mstro_pmp_send_nowait_ep(const struct mstro_endpoint *ep, fi_addr_t addr, const Mstro__Pool__MstroMsg *msg);

/** Take protobuf-based message and pack it into a message envelope
 * and send it to @arg TARGET. The @arg msg argument can be a
 * local-scope object at the call-site of this function (it will be
 * serialized immediately on entry and not referenced afterwards) 
 * puts the msg on a send queue to be send an OFI_thread and executes 
 * the completion event when send completes*/
mstro_status
mstro_pmp_send_w_completion(mstro_app_id target, const Mstro__Pool__MstroMsg *msg, mstro_event completion_event);

/** Take protobuf-based message and pack it into a message envelope
 * and send it to the pool manager. The @arg msg argument can be a
 * local-scope object at the call-site of this function (it will be
 * serialized immediately on entry and not referenced afterwards)
 *
 * Waits for completion of the send operation.
 */
mstro_status
mstro_pmp_send_wait(mstro_app_id target, const Mstro__Pool__MstroMsg *msg);

/* FIXME: should be in pool_client file or so */
/** endpoint we use to talk to pool manager */
extern struct mstro_endpoint *g_pm_endpoint;
/** address of pool manager on g_pm_endpoint */
/* FIXME: fi_addr_t should not be exposed here */
#include <rdma/fabric.h>
extern fi_addr_t g_pm_addr;

/** Wrap a message INNER that is a legal MstroMsg in MSG (allocated by caller) */
mstro_status
mstro_pmp_package(Mstro__Pool__MstroMsg *msg,
                  ProtobufCMessage *inner);


/** each transport needs to send completion via this function when
 * appropriate. PM will always be notified, @arg srcid only if
 * src_wants_completion is true */
mstro_status
mstro_pc__transport_send_completion(mstro_app_id srcid,
                                    const struct mstro_cdo_id *srccdoid,
                                    const struct mstro_cdo_id *dstcdoid,
                                    int src_wants_completion);


/** initialize PMP infrastructure */
mstro_status
mstro_pmp_init(void);

/** finalize PMP infrastructure */
mstro_status
mstro_pmp_finalize(void);

#include "maestro/i_pool_manager_registry.h"


#endif /* MSTRO_POOL_MANAGER_PROTOCOL_H_ */
