/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Event handling infrastructure
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_EVENT_H_
#define MAESTRO_I_EVENT_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_EVENT Maestro Event handling implementation
 **@{
 **
 ** This is the event handling infrastructure: An abstraction similar
 ** to signal handling, but for large event identifier domains.
 **/


#include "maestro/status.h"
#include <stdbool.h>
#include <stdatomic.h>
#include <stdint.h>
#include <pthread.h>
#include <errno.h>
#include <sys/time.h>


/* how much (in sec) are we willing to wait for generic table to be cleared */
#define MSTRO_TIMED_WAIT	5

/** Generic timed wait on hash table proper cleanup
 **/
/* this should really be a function, not a macro, but HTABLE_COUNT does not take generic args */
#define mstro_cleanup_waitup(retstat,htable,lock,cond,reason) {         \
    retstat = MSTRO_OK;                                                 \
    do {                                                                \
      struct timeval tv_;                                               \
      struct timespec ts_;                                              \
      gettimeofday(&tv_, NULL);                                         \
      ts_.tv_sec = tv_.tv_sec + MSTRO_TIMED_WAIT;                       \
      ts_.tv_nsec = 0;                                                  \
      int rc_ = 0;                                                      \
      int rl_ = 0;                                                      \
                                                                        \
      rl_ = pthread_mutex_lock(lock);                                   \
      if(rl_!=0) {                                                      \
        ERR("Failed to lock `%s` hash table: %d (%s)\n",                \
            reason, rl_, strerror(rl_));                                \
        retstat = MSTRO_FAIL;                                           \
        break;                                                          \
      }                                                                 \
                                                                        \
      unsigned rem_ = HASH_COUNT(htable);                               \
      if(rem_==0) {                                                     \
        DEBUG("All items in `%s` already completed cleanly\n", reason); \
      } else {                                                          \
        DEBUG("In `%s`, %d item(s) still pending, waiting up to %d sec\n",\
             reason, rem_, MSTRO_TIMED_WAIT);                           \
        do {                                                            \
	  rc_ = pthread_cond_timedwait(cond, lock, &ts_);               \
	} while (!rc_ && HASH_COUNT(htable)>0 );                        \
									\
        if(rc_!=0) {                                                    \
	  if(rc_==ETIMEDOUT) {                                          \
	    ERR("Waited %zu seconds without change to table\n",         \
                (uint64_t)MSTRO_TIMED_WAIT);                            \
	  } else {                                                      \
	    ERR("Failed to perform timedwait for %s: %d (%s)\n",        \
                reason, rc_, strerror(rc_));                            \
	  }                                                             \
          retstat = MSTRO_FAIL;                                         \
          break;                                                        \
        }                                                               \
      }                                                                 \
                                                                        \
      if (HASH_COUNT(htable)) {                                         \
        ERR("Items still remaining unhandled in `%s`\n", reason);       \
        retstat = MSTRO_TIMEOUT;                                        \
      }                                                                 \
      rl_ = pthread_mutex_unlock(lock);                                 \
      if(rl_) {                                                         \
        ERR("Lock issue on `%s` table (errno: %d (%s))\n",              \
            reason, rl_, strerror(rl_));                                \
        retstat = MSTRO_FAIL;                                           \
      }                                                                 \
    } while (0);                                                        \
  }

/** An event descriptor */
typedef struct mstro_event_ *mstro_event;

/** An event handler. At invocation time will be passed the event, and
 * the user-provided context closure */
typedef void (*mstro_event_handler)(mstro_event event,
                                    void *context);

/** An event domain. Event registration always happens inside a
 * domain, and event IDs are unique within the domain. */
typedef struct mstro_event_domain_ *mstro_event_domain;

/** events are identified by a unique ID (unique inside a given event ID */
typedef uint64_t mstro_event_id;

/** Create a new event domain.
 *
 * NAME may be NULL, but giving a name will improve debugging and logging.
 */
mstro_status
mstro_event_domain_create(const char *name, mstro_event_domain *result);

/** constant designating that an anonymous domain is desired */
#define MSTRO_EVENT_DOMAIN_ANONYMOUS NULL

/** Destroy an event domain.
 *
 * If there are events registered that have not occured they will be canceled.
 */
mstro_status
mstro_event_domain_destroy(mstro_event_domain d);


/** Create a new event.
 *
 * Events can be triggered multiple times, with concurrent triggers
 * being serialized by a mutex.
 *
 * The flag @arg auto_destroy specifies that the event structure (and
 * the closure, if applicable) should be automatically submitted to
 * @ref mstro_event_destroy after the event was handled, making such
 * an event effectively one-shot.
 *
 * To obtain one-shot behavior without auto_destroy the user code
 * needs to explicitly destroy the event using mstro_event_destroy().
 *
 * The @arg closure_dtor
 * can be set to function that can be called to de-allocate the
 * closure argument when the event is destroyed.
 *
 * If set to NULL, the closure will have to be de-allocated by the
 * user during or after the event has been handled.
 *
 * Note that when an event domain is destroyed with unhandled events
 * in it, the implicit call to @ref mstro_event_destroy() will only
 * free the closure if you pass a suitable function here.
 *
 **/
mstro_status
mstro_event_create(mstro_event_domain d, 
                   mstro_event_handler handler, void *closure,
                   void(*closure_dtor)(void* closure),
                   bool auto_destroy,
                   mstro_event *result);

/** Obtain event ID.
 *
 * This ID is sufficient to locate the event in its domain later.
 */
mstro_status
mstro_event_id_get(mstro_event event, mstro_event_id *id);


/** Obtain domain name by event
 **/
mstro_status
mstro_event_domain_name_get(mstro_event event, char** name);


/** Destroy an event.
 *
 * If it had a non-NULL closure_dtor set at creation time, that
 * destructor will be called on the closure argument.
 *
 * Of course, this function must not be called multiple times for the
 * same event. In particular the registered closure destructor will
 * not be protected against multiple invocations.
 */
mstro_status
mstro_event_destroy(mstro_event event);

/** Destroy an event, by ID.
 *
 * If it had a non-NULL closure_dtor set at creation time, that
 * destructor will be called on the closure argument.
 */
mstro_status
mstro_event_destroy_by_id(mstro_event_domain d, mstro_event_id id);

/** Trigger an event.
 *
 * If the event had auto-destroy set at creation it will be passed to
 * mstro_event_destroy(), and *result will be set to NULL.  Otherwise
 * *result will be set to the closure argument passed at event
 * creation time (which permits returning a status or other value from
 * the handler).
 *
 */
mstro_status
mstro_event_trigger(mstro_event e,
                    void **result);

/** Trigger an event, by event ID
 *
 * See mstro_event_trigger()
 */
mstro_status
mstro_event_trigger_by_id(mstro_event_domain d,
                          mstro_event_id id,
                          void **result);



/**@} (end of group MSTRO_I_EVENT) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_EVENT_H_ */
