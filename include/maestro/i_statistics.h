/* -*- mode:c -*- */
/** @file
 ** @brief Statistics collection for Maestro
 **
 **/
/*
 * Copyright (C) 2018-2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MSTRO_STATS_H_
#define MSTRO_STATS_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

#include "maestro/status.h"
#include "maestro/core.h"

#include <stdint.h>
#include <stdio.h>

/** @defgroup MSTROstats Maestro statistics collection
 ** @{
 **
 ** This code provides an infrastructure to collect statistics.
 **
 ** We will support a number of predefined categories (see @ref
 ** mstro_stats_category) and in each category labels for a group of
 ** measurements can be registered. Under each label an arbitrary
 ** number of keys and values can be stored.
 **
 ** Keys can be bucketed at the time of inserting values by setting
 ** the statistics type for the label appropriately.
 **
 **/

/** supported categories */
enum mstro_stats_category {
  MSTRO_STATS_CAT_GENERAL = 0,
  MSTRO_STATS_CAT_POOL,
  MSTRO_STATS_CAT_OFI,
  MSTRO_STATS_CAT_PROTOCOL,
  MSTRO_STATS_CAT_OBJECTS,
  MSTRO_STATS_CAT_MEMORY,
  MSTRO_STATS_CAT_TRANSPORT,
  MSTRO_STATS_CAT_THREADING,
  MSTRO_STATS_CATEGORY__MAX
};

/** supported measurement types */
enum mstro_stats_collection_type {
  MSTRO_STATS_COLL_COUNTER, /**< count occurence */
  MSTRO_STATS_COLL_KEY_VAL, /**< pairs of (key,value) */
  MSTRO_STATS_COLL_KEY_TIME, /**< pairs of key and timespan */
  MSTRO_STATS_COLLECTION__MAX
};

/** initialize statistics functionality */
mstro_status
mstro_stats_init(void);

/** finalize statistics functionality */
mstro_status
mstro_stats_finalize(void);


/** Define a new label to collecting information */
mstro_status
mstro_stats_register_label(enum mstro_stats_category category,
                           const char *label, enum mstro_stats_collection_type type);



/** store a timing for LABEL in CATEGORY under KEY */
mstro_status
mstro_stats_store_timing(enum mstro_stats_category category, const char *label,
                  uint64_t key, mstro_nanosec_t time);

/** store a 'count' event for LABEL in CATEGORY */
mstro_status
mstro_stats_store_counter(enum mstro_stats_category category, const char *label,
                   uint64_t counter);

/** add a count to the most recently stored counter event for LABEL in
 * CATEGORY */
mstro_status
mstro_stats_add_counter(enum mstro_stats_category category, const char *label,
                 uint64_t increment);

/**
 ** @brief Print a report of current statistics to DST.
 **
 ** If DST is NULL use the normal logging facility with 'INFO' priority.
 **
 ** Currently this is rank-local, but should really aggregate across
 ** all ranks.
 **/
mstro_status
mstro_stats_report(FILE *dst);

/**
 ** @brief Print a report of current statistics to DST in CSV format.
 **
 ** If DST is NULL use the normal logging facility with 'INFO' priority.
 **
 ** Currently this is rank-local, but should really aggregate across
 ** all ranks.
 **/
mstro_status
mstro_stats_report_csv(FILE *dst);


/** Predefined labels (cf. WP5.4 telemetry document and https://gitlab.jsc.fz-juelich.de/maestro/maestro-core/-/issues/46) */

#define MSTRO_STATS_L_COMPONENT_RUNTIME               "component-runtime"
#define MSTRO_STATS_L_POOL_ASSOCIATION_TIME           "pool-association-time"
#define MSTRO_STATS_L_NUM_CDOS_CREATED                "num-cdos-created"
#define MSTRO_STATS_L_BYTES_POOLED                    "total-bytes-pooled"
#define MSTRO_STATS_L_BYTES_MOVED                     "total-bytes-moved"

/* PC */
#define MSTRO_STATS_L_PC_NUM_DECLARE                  "pc-num-declare"
#define MSTRO_STATS_L_PC_NUM_ASYNC_DECLARE            "pc-num-async-declare"
#define MSTRO_STATS_L_PC_NUM_ASYNC_DECLARE_BLOCKED    "pc-num-async-declare-blocked"
#define MSTRO_STATS_L_PC_NUM_SEAL                     "pc-num-seal"
#define MSTRO_STATS_L_PC_NUM_ASYNC_SEAL               "pc-num-async-seal"
#define MSTRO_STATS_L_PC_NUM_ASYNC_SEAL_BLOCKED       "pc-num-async-seal-blocked"
#define MSTRO_STATS_L_PC_NUM_SEAL_GROUP               "pc-num-sealgroup"
#define MSTRO_STATS_L_PC_NUM_OFFER                    "pc-num-offer"
#define MSTRO_STATS_L_PC_NUM_ASYNC_OFFER              "pc-num-async-offer"
#define MSTRO_STATS_L_PC_NUM_ASYNC_OFFER_BLOCKED      "pc-num-async-offer-blocked"
#define MSTRO_STATS_L_PC_NUM_WITHDRAW                 "pc-num-withdraw"
#define MSTRO_STATS_L_PC_NUM_ASYNC_WITHDRAW           "pc-num-async-withdraw"
#define MSTRO_STATS_L_PC_NUM_ASYNC_WITHDRAW_BLOCKED   "pc-num-async-withdraw-blocked"
#define MSTRO_STATS_L_PC_NUM_REQUIRE                  "pc-num-require"
#define MSTRO_STATS_L_PC_NUM_ASYNC_REQUIRE            "pc-num-async-require"
#define MSTRO_STATS_L_PC_NUM_ASYNC_REQUIRE_BLOCKED    "pc-num-async-require-blocked"
#define MSTRO_STATS_L_PC_NUM_RETRACT                  "pc-num-retract"
#define MSTRO_STATS_L_PC_NUM_ASYNC_RETRACT            "pc-num-async-retract"
#define MSTRO_STATS_L_PC_NUM_ASYNC_RETRACT_BLOCKED    "pc-num-async-retract-blocked"
#define MSTRO_STATS_L_PC_NUM_DEMAND                   "pc-num-demand"
#define MSTRO_STATS_L_PC_NUM_ASYNC_DEMAND             "pc-num-async-demand"
#define MSTRO_STATS_L_PC_NUM_ASYNC_DEMAND_BLOCKED     "pc-num-async-demand-blocked"
#define MSTRO_STATS_L_PC_NUM_DISPOSE                  "pc-num-dispose"
#define MSTRO_STATS_L_PC_NUM_SUBSCRIBE                "pc-num-subscribe"
#define MSTRO_STATS_L_PC_NUM_POOL_EVENTS              "pc-num-pool-events"
#define MSTRO_STATS_L_PC_NUM_TICKETS_OUT              "pc-num-tickets-in"
#define MSTRO_STATS_L_PC_NUM_TICKETS_IN               "pc-num-tickets-out"
#define MSTRO_STATS_L_PC_NUM_RESOLVE                  "pc-num-resolve"

/* PM */
#define MSTRO_STATS_L_PM_NUM_JOIN                 "pm-num-join"
#define MSTRO_STATS_L_PM_NUM_LEAVE                "pm-num-leave"
#define MSTRO_STATS_L_PM_NUM_DECLARE              "pm-num-declare"
#define MSTRO_STATS_L_PM_NUM_SEAL                 "pm-num-seal"
#define MSTRO_STATS_L_PM_NUM_SEAL_GROUP           "pm-num-sealgroup"
#define MSTRO_STATS_L_PM_NUM_OFFER                "pm-num-offer"
#define MSTRO_STATS_L_PM_NUM_WITHDRAW             "pm-num-withdraw"
#define MSTRO_STATS_L_PM_NUM_REQUIRE              "pm-num-require"
#define MSTRO_STATS_L_PM_NUM_RETRACT              "pm-num-retract"
#define MSTRO_STATS_L_PM_NUM_DEMAND               "pm-num-demand"
#define MSTRO_STATS_L_PM_NUM_DISPOSE              "pm-num-dispose"
#define MSTRO_STATS_L_PM_NUM_SUBSCRIBE            "pm-num-subscribe"
#define MSTRO_STATS_L_PM_NUM_POOL_EVENTS          "pm-num-pool-events"
#define MSTRO_STATS_L_PM_NUM_TICKETS              "pm-num-tickets"
#define MSTRO_STATS_L_PM_NUM_TRANSFER_COMPLETIONS "pm-num-transport-completions"
#define MSTRO_STATS_L_PM_NUM_RESOLVE              "pm-num-resolve"
  /* number of withdraw ops that succeeded immediately */
#define MSTRO_STATS_L_PM_NUM_IMM_WITHDRAWS        "pm-num-imm-withdraws"
  /* number of withdraw wakeups (over all CDOs), i.e., blocking events */
#define MSTRO_STATS_L_PM_NUM_WITHDRAW_WAKEUPS     "pm-num-withdraw-wakeups"



/* OFI */
#define MSTRO_STATS_L_PC_NUM_VSM_IN                   "pc-num-vsm-in"
#define MSTRO_STATS_L_PC_NUM_VSM_OUT                  "pc-num-vsm-out"
#define MSTRO_STATS_L_PM_NUM_VSM_IN                   "pm-num-vsm-in"
#define MSTRO_STATS_L_PM_NUM_VSM_OUT                  "pm-num-vsm-out"


/**@}*/
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MSTRO_STATS_H_ */
