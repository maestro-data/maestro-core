/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO Attributes -- internal header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_ATTRIBUTES_H_
#define MAESTRO_I_ATTRIBUTES_H_ 1

#include "maestro/attributes.h"
#include "maestro/status.h"
#include "maestro/i_cdo.h"
#include <stdint.h>

/** the structure describing the scope object attributes */

struct mstro_cdo_attr_layout_regular_1d_ {
  int element_size;
};

struct mstro_cdo_attr_layout_ {
  struct mstro_cdo_attr_layout_regular_1d_ regular_1d;
};

struct mstro_cdo_attr_scope_ {
  uint64_t local_size;
  struct mstro_cdo_attr_layout_ layout;
  unsigned char* distribution;
};

typedef int boolean;
/** Opaque attribute table */
struct mstro_cdo_attr_table_ {
  int allocate_now; /**< Indicate that Maestro should perform allocation before DECLARE completes */
  int maestro_provided_storage; /**< Indicate that maestro should perform resource management */
  unsigned char* name; /**< user-provided name */
  int level; /**< CDO level */
  unsigned char* id; /**< user-provided CDO id */
  unsigned char* lifetime; /**< Lifetime specifier tbd. */
  unsigned char* redundancy; /**< Redundancy specifier tbd. */
  int persist; /**< Indicate if CDO is to persist */
  int desist; /**< Indicate if CDO is to desist */
  struct mstro_cdo_attr_scope_ scope; /**< Scope object structure */
};

typedef struct mstro_cdo_attr_table_* mstro_cdo_attr_table;
typedef struct mstro_cdo_attr_scope_* mstro_cdo_attr_scope;


/* attributes missing? -----v
  memory object
  scope object:
 (attributes: )this_scope_cdo_handles, size, dependences, data_shape, index sets, etc.
 (transformations:) transpose, split, alignment, pad, reshape, etc.
  user-defined metadata
 */

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_Attributes Internal Maestro Attributes API
 **@{
 ** This is the Internal Attributes API, as developed for D3.2
 **/

/**@brief Create a fresh attribute table
 **
 ** @param[out]	result	Freshly allocated table
 */
mstro_status
mstro_cdo_attr_table__alloc(mstro_cdo_attr_table* result);

/**@brief Destroy an attribute table
 **
 ** @param[in] 	tab	Attribute table to destroy
 */
mstro_status
mstro_cdo_attr_table__destroy(mstro_cdo_attr_table tab);


/**@brief Query an attribute, based on string key.
 **
 ** An unsuccessful query will see *valtype* set to
 ** MSTRO_CDO_ATTR_VALUE_NA and *value_dst* set to NULL
 **
 ** Consider using the predefined constant string keys from
 ** attributes.h, they will make lookups faster.
 **
 ** The value type will be returned in the *valtype* argument. If
 ** it is NULL, no value type will be returned.
 **
 ** The value can be cast to the expected type for well-known keys;
 ** for value types that are more complicated than the built-ins, we
 ** return MSTRO_CDO_ATTR_VAL_OPAQUE, and extra tools will be needed
 ** to obtain the type specification for the key and parse the opaque
 ** value returned here.
 **
 ** @param[in]	cdo	A CDO handle
 ** @param[in]	key	Attribute key string
 ** @param[out] valtype	Type of the queried value
 ** @param[out] val_p	Pointer to the queried value
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_attr_table__lookup(const mstro_cdo cdo,
                             const char *key,
                             enum mstro_cdo_attr_value_type *valtype,
                             void **value_dst);

/**@brief Insert an attribute, based on YAML string (key,value) pair
 **
 ** Update attributes of *cdo* using a YAML string *keyval_as_yaml*
 ** representing (key,value) pairs
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[in]	keyval_in_yaml	(key,value) pair(s) in a YAML string format
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_attr__insert_yaml(mstro_cdo cdo,
                                  const char *keyval_as_yaml);

/** Parse irregular_1D mmbLayout from string for distributed CDOs */
mstro_status
mstro_parse_mmbLayout_irregular_1D(char *str, mmbLayout **dist_layout);

/** Computes total size of distributed array */
uint64_t
mstro_mmbLayout_get_totalsize(mmbLayout* layout);

/** Fetches total size of non-distributed array from msg */
uint64_t
mstro_attribute_msg_get_totalsize(Mstro__Pool__Attributes *attributes);

/*Parse a size_t array from a string, contained in brackets, i.e. "[100, 200, 300]" */
static inline
mstro_status
mstro_parse_number_array(char *str, size_t *num);


/* Automaticallly set cdo.isdistributed attributed if the user set any of the
   distributed layout attributes */
static inline
mstro_status
mstro_cdo_attribute_set_isdistributed(mstro_cdo cdo, const char* key);


/**@} (end of group MSTRO_I_Attributes) */
/**@} (end of addtogroup MSTRO_Internal) */

#endif /* MAESTRO_I_ATTRIBUTES_H_ */
