/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO Groups public header file
 **/
/*
 * Copyright (C) 2019-20 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_GROUPS_H_
#define MAESTRO_GROUPS_H_ 1

#include "maestro/status.h"
#include "maestro/cdo.h"

/**@addtogroup MSTRO_Core
 **@{
 **/

/**@defgroup MSTRO_GROUPS Maestro CDO Groups
 **@{
 ** This is the CDO Groups API
 **
 **/

/** CDO Group handle */
typedef struct mstro_group_ *mstro_group;

/** D3.1 also states that:
 ** - CDO can be in multiple groups 
 ** - A CDO group can be created so that its elements refer to parts (elements, tiles, ...)
 ** of one or more CDOs 
 **
 **
 **/

/** TODO think about how we allow convenience declaration of a group of many CDOs
 ** (bounded selector?) 
 **/

/** @brief Declare a CDO group
 **
 **/
mstro_status
mstro_group_declare(const char *name, mstro_group* result);

/** @brief Add a CDO to a CDO group
 **
 **/
mstro_status
mstro_group_add(mstro_group group, mstro_cdo cdo);


/** @brief Add a CDO to a CDO group, by name
 **
 ** similar to @ref mstro_group_add(), but takes a CDO name.
 **/
mstro_status
mstro_group_add_by_name(mstro_group group, const char* name);

/* + declare with a selector, resolution at SEAL time */

/** @brief Seal a group declaration.
 **
 ** The CDO members are decided at SEAL time (by the member list, or
 ** by the CDO selector), and will not change after that.
 **
 ** It is an error to SEAL another group declaration for the same
 ** group later if that would lead to a contradicting membership
 ** set. That means that
 **
 ** - the explicit members must always be the same, or that later
 **   declaration/seal occurences will only contain CDO
 **   selectors. Such selectors must always be the same too.
 **
 ** - Later CDO selector based SEALs will not modify the membership
 **      (behave as though they were not evaluated), since the
 **      membership list is fixed the first time the group is SEALed.
 **
 ** A group can be sealed without having any members or selector
 ** added. In that case it will either contain the members decided by
 ** the previous SEAL of the same group, or, if there was none, it
 ** will be empty.
 **
 ** This operation batch-declares any members not yet DECLAREd.
 **
 ** This also SEALs the CDOs not SEALed yet.
 **
 ** This means that all CDOs added to the group at the time SEAL is executed must either
 ** * be freshly auto-declared and auto-sealed (as described above), or
 ** * have been declared, or
 ** * have been delcared and sealed, or
 ** * have been offered or required
 **
 ** They can not be in any other state (retracted, withrdrawn, demanded).
 **
 **
 ** BEWARE: You cannot have multiple 'producers' cooperate to decide
 ** the membership of a group. Instead, each producer should build
 ** their own group, and then one should invoke @ref
 ** mstro_group_union().
 **/
mstro_status
mstro_group_seal(mstro_group group);

/** also OFFERs the CDOs not OFFERed yet
 ** "group definition at a global scope", allows forward referencing, that is type-0 CDOs 
 **/
mstro_status
mstro_group_offer(mstro_group group);

/** You get a Group handle, (maybe -- could be explicited in an attribute) not
 ** the CDOs themselves that you have to PEEK/DEMAND/DEMAND_ALL 
 **/
mstro_status
mstro_group_demand(mstro_group group);

/** Also WITHDRAWs all CDOs if applicable? (be careful with CDOs pertaining to
 ** multiple groups) 
 ** + CDOs cna't be withdrawn if all their Groups haven't been withdrawn?
 ** - dropping ref-count
 ** - recipient must have disposed group after demand group 
 **/
mstro_status
mstro_group_withdraw(mstro_group group);

/** Maybe dispose the CDOs as well. In a kind of a convenience API way just
 ** like DECLARE. Again let's be careful with CDOs within multiple Groups 
 **/
mstro_status
mstro_group_dispose(mstro_group group);

/** @brief Return the number of CDOs in the group @arg group.
 **
 ** Requires @arg group to have been OFFERED, REQUIRED, or DEMANDED.
 **
 **
 ** @param[in]  group        A group handle
 ** @param[out] result_count The number of CDOs in the group

 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_group_size_get(mstro_group group, size_t* result_count);

/** @brief Iterate through members of group @arg group.
 **
 ** Requires that the group @arg group has been DEMANDed using @ref
 ** mstro_group_demand().
 **
 ** Each invocation returns 'the next' member of the group, in a
 ** Maestro-decided order. (The order is not stable for multiple
 ** handles for the same group!)
 **
 ** Each value returned via @arg *next_cdo_p is a fresh CDO handle
 ** that behaves as though it were DECLARED, REQUIRED, DEMANDED. It
 ** needs to be DISPOSED by the user using @ref mstro_cdo_dispose().
 **
 ** When NULL is returned as the value in *next_cdo_p, iteration is
 ** done. Repeated invocation will restart iteration, not necessarily
 ** in the same order, with fresh handles being created again for each
 ** member CDO.
 **
 **/
mstro_status
mstro_group_next(mstro_group group, mstro_cdo* next_cdo_p);


mstro_status
mstro_group_union(mstro_group* result, mstro_group a, mstro_group b);
mstro_status
mstro_group_inter(mstro_group* result, mstro_group a, mstro_group b);

mstro_status
mstro_group_retract(mstro_group group);
mstro_status
mstro_group_require(mstro_group group);





/**@} (end of group MSTRO_GROUPS) */
/**@} (end of addtogroup MSTRO_Core) */


#endif
