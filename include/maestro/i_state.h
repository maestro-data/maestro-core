/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Internal state query/modification interface
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_STATE_H_
#define MAESTRO_I_STATE_H_ 1

#include "maestro/status.h"
#include <stdint.h>

/**@defgroup MSTRO_I_STATE Maestro internal state interface
 **
 **@{
 **/

/** State of each participant in a Maestro run: */
struct mstro_core_initdata {
  char *workflow_name;      /**< The workflow name (from init) */
  char *component_name;     /**< The component name (from init) */
  uint64_t component_index; /**< The component index (from init) */
  /* more to come ...*/
};

/** @brief obtain a constant reference to the state structure
 *
 *
 * Must only be called after mstro_core_init().
 *
 */

/* FIXME: this only works as long as this structure does not change
 * after being initialized. For now this is true */
mstro_status
mstro_core_state_get(const struct mstro_core_initdata **res); 

/**@} (end of group MSTRO_I_STATE) */


#endif /* MAESTRO_I_STATE_H_ */
