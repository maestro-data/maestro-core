/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool Manager interface 
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_POOL_MANAGER_H_
#define MAESTRO_I_POOL_MANAGER_H_ 1


#include "maestro/status.h"
#include "maestro/i_pool_manager_protocol.h"
#include "maestro/i_pool_manager_registry.h"
#include "protocols/mstro_pool.pb-c.h"
#include "maestro/i_pool_operations.h"

#include "maestro/i_event.h"

#include <stdbool.h>
#include <stdatomic.h>


/** endpoint abstraction */
struct mstro_endpoint;


/* Pool manager "Handle incoming Message" dispatcher function */
mstro_status
mstro_pm_handle_msg(const struct mstro_msg_envelope *envelope);

void
mstro_pm__msg_free(Mstro__Pool__MstroMsg *msg);

mstro_status
mstro_pm__possibly_fill_event_cdoname(const struct mstro_cdo_id *cdoid, 
                                                mstro_app_id recipient,
                                                 Mstro__Pool__Event *ev);


mstro_status 
mstro_pm__event_notify_before(mstro_pool_operation op);

mstro_status
mstro_pm__event_notify_after(mstro_pool_operation op);

mstro_status 
mstro_pm__check_acks(mstro_pool_operation op);

mstro_status
mstro_pm__op_maker(Mstro__Pool__MstroMsg *msg, const struct mstro_endpoint *ep);

/**Handle pm cdo seal operation and send ack*/
mstro_status
mstro_pm__cdo_seal(mstro_pool_operation op);

/**
 * @brief wrapper for mstro_pm__send_ack to use within operations
 * 
 * @param op operation handle
 * @return mstro_status 
 */
mstro_status
mstro_pm__send_ack_op(mstro_pool_operation op);

/**
 * @brief perm pm cdo demand
 * update cdo state and insert it in the demand queue
 * @param op operation handle
 * @return mstro_status 
 */
mstro_status
mstro_pm__cdo_demand(mstro_pool_operation op);

/**
 * @brief Send bye reply for application
 * Send a bye msg to an application after processing a leave operation
 * @param op operation handle
 * @return mstro_status out status
 */
mstro_status
mstro_pm__send_bye(mstro_pool_operation op);

/**
 * @brief Send welcome msg to joining apps
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__send_welcome(mstro_pool_operation op);

/**
 * @brief Handle subscribe operation at the pool manager
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__subscribe(mstro_pool_operation op);

/**
 * @brief wrapper for mstro_subscription_message_unregister
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__unsubscribe(mstro_pool_operation op);

/**
 * @brief Send subscribe ack message
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__send_subscribe_ack(mstro_pool_operation op);

/**
 * @brief Update the statistics for completed PM operations
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__update_stats(mstro_pool_operation op);

/**
 * @brief handle ack msg on the PM
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__event_ack(mstro_pool_operation op);

/**
 * @brief handle msg resolve at pm
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__msg_resolve(mstro_pool_operation op);

/**
 * @brief handle join operation
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__app_join(mstro_pool_operation op);



/* /\** All of these functions are useable only after the @ref */
/*  * mstro_pm_attach has been performed *\/ */

/* /\** Send a message (not expecting a reply) *\/ */
/* mstro_status */
/* mstro_pm_send(struct mstro_msg *msg); */

/* /\** Send a message (blocking for reply). *\/ */
/* mstro_status */
/* mstro_pm_sendrecv(struct mstro_msg *msg, struct mstro_msg *reply); */

/* /\** handle for async messages *\/ */
/* typedef struct mstro_msg_handle_ *mstro_msg_handle; */

/* /\** Send message async, not expecting a reply *\/ */
/* mstro_status */
/* mstro_pm_send_async(struct mstro_msg *msg, mstro_msg_handle *handle); */

/* /\** Send a message async, reply can be extracted from handle. *\/ */
/* mstro_status */
/* mstro_pm_sendrecv_async(struct mstro_msg *msg, mstro_msg_handle *handle); */

/* /\** block until communication associated with handle has completed *\/ */
/* mstro_status */
/* mstro_msg_wait(mstro_msg_handle handle); */

/* /\** check if communication associated with handle has completed *\/ */
/* mstro_status */
/* mstro_msg_test(mstro_msg_handle handle, bool done); */

/* /\** retrieve reply message of communication associated with handle *\/ */
/* mstro_status */
/* mstro_msg_handle_reply(mstro_msg_handle handle, struct mstro_msg *reply); */




#endif /* MAESTRO_I_POOL_MANAGER_H_ */
