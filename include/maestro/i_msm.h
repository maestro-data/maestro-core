/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Memory System Model -- internal header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_I_MSM_H_
#define MAESTRO_I_MSM_H_ 1
#include "maestro/msm.h"

typedef enum {
  MSTRO_MSM_LAYER_INVALID = 0,
  MSTRO_MSM_LAYER_DRAM,
  MSTRO_MSM_LAYER_DRAM_PINNED,
  MSTRO_MSM_LAYER_NUMA,
  MSTRO_MSM_LAYER_NUMA_PINNED,
  MSTRO_MSM_LAYER_CUDA,
  MSTRO_MSM_LAYER_MCDRAM,
  MSTRO_MSM_LAYER_OBJSTORE,
  MSTRO_MSM_LAYER_POSIXFS,
  mstro_msm_layer_MAX
} mstro_msm_layer;

typedef union {
  struct {
    int domain;
  } numa;
  struct {
    int device;
  } cuda;
  struct {
    char *root;
  } posixfs;
} mstro_msm_component;
  
  
struct mstro_location_ {
  char *host;                      /**< host */
  mstro_msm_layer layer;           /**< layer */
  mstro_msm_component component;   /**< layer-specific subcomponent */
};
  
#endif
