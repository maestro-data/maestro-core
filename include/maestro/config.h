/* -*- mode:c -*- */
/** @file
 ** @brief Maestro core configuration
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_CONFIG_H_
#define MAESTRO_CONFIG_H_ 1

#ifdef __cplusplus
extern "C" {
#endif
  #include "status.h"

/******************************************************************************
 * This is what we want to load the YAML into.
 ******************************************************************************/

struct mstro_config_cdo_ {
  char* name;
  int user_provided_storage; // bool
  int layer; // enum
  int size;
};

struct mstro_config_wc_charybdis_ {
  char* bin;
  int nranks;
}; 

struct mstro_config_wc_spare_ {
  char* bin;
  int nranks;
}; 

struct mstro_config_wc_pool_manager_ {
  char* bin;
  int nranks;
}; 

struct mstro_config_wc_consumer_ {
  struct mstro_config_cdo_* cdo;
  char* bin;
  int nranks;
  unsigned cdo_count;
}; 

struct mstro_config_wc_producer_ {
  struct mstro_config_cdo_* cdo;
  char* bin;
  int nranks;
  unsigned cdo_count;
}; 

struct mstro_config_workflow_ {
  struct mstro_config_wc_producer_ producer;
  struct mstro_config_wc_consumer_ consumer;
  struct mstro_config_wc_pool_manager_ pool_manager;
  struct mstro_config_wc_spare_ spare;
  struct mstro_config_wc_charybdis_ charybdis;
};

struct mstro_config_ {
  char *version;
  int token;
  struct mstro_config_workflow_ workflow;
};


  /**@defgroup MSTRO_CONFIG Maestro core configuration
   **@{
   ** This is the Maestro core configuration 
   **/

  /** a memory handle for maestro configuration */ 
  typedef struct mstro_config_ *mstro_config;

  /**
   ** @brief Parse maestro core config file 
   ** @param handle	A handle on a structure containing the whole config
   ** @param filename   Path to the input yaml config
   ** @return { a status value, @ref MSTRO_OK on success }
   **/
  mstro_status
  mstro_config_parse(mstro_config* handle, const char* filename);

  /**@} (end of group MSTRO_CONFIG) */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MAESTRO_CONFIG_H_ */
