/* -*- mode:c -*- */
/** @file
 ** @brief HPE SS11 Virtual Network Identifier interface
 **
 */
/* Copyright © 2023 Hewlett Packard Enterprise Development LP
 * Copyright (C) 2019 Cray Computer GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MSTRO_I_VNI_H_
#define MSTRO_I_VNI_H_ 1

#include "maestro/status.h"
#include "rdma/fabric.h"
#include "protocols/mstro_ep.pb-c.h"

/**@ingroup Internal
 * @{
 **@defgroup InternalVNI HPE SS11 Virtual Network Identifier for fabric keys
 **@{
 **/

/* CXI Authorization Key */
typedef struct {
    uint32_t svc_id;
    uint16_t vni;
} fi_vni_auth_key;

#define MSTRO_VNI_SERIALIZED_LEN 22 /* uint32_t max is 4294967295 in decimal, +',' +service_id+ \0 */
struct mstro_vni_info_ {
  fi_vni_auth_key *auth_key;                /**< the actual key */
  char serialized_id[MSTRO_VNI_SERIALIZED_LEN];    /**< the serialized ID */
};

/** CXI information is stored in a mstro_vni_info_ structure */
struct mstro_vni_info_;
typedef struct mstro_vni_info_ *mstro_vni_info;

/*Read VNI info from environment, as set by launcher, such as SLURM, 
 * select the proper service id based on the gived device name*/
mstro_status
mstro_vni_get_auth_key_from_env(fi_vni_auth_key **auth_key, const char *device_name);

/** Insert vni credentials into a libfabric object */
mstro_status
mstro_vni_insert_ofi(struct fi_info *fi, const mstro_vni_info info);

/** obtain VNI credentials */
mstro_status
mstro_vni_init(mstro_vni_info *result_p);

/** free VNI credentials */
mstro_status
mstro_vni_destroy(mstro_vni_info info);

mstro_status
mstro_vni_init_from_credential(mstro_vni_info *result_p,
                               Mstro__CredCXI *credential);

/**@} (InternalVNI) */
/**@} (Internal) */

#endif /* I_VNI_H_ */

