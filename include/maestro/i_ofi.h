/* -*- mode:c -*- */
/** @file
 ** @brief Maestro OpenFabric connections header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_OFI_H_
#define MAESTRO_I_OFI_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_OFI Network layer
 **@{
 ** This is the Internal Network layer interface
 **/


#include "maestro/status.h"
#include "protocols/mstro_ep.pb-c.h"
#include "maestro/i_hashing.h"
#include <stdatomic.h>
#include "maestro/i_mempool.h"

/* we assume everyone has IP */
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

/* we checked for libfabric */
#include <rdma/fabric.h>
#include <rdma/fi_domain.h>

#ifdef HAVE_IB
#include <infiniband/ib.h>
#endif

/* for message envelopes: */
#include "maestro/i_pool_manager_protocol.h" 

/**separator of the blocks defining ofi threads on the "PM"*/
#define PM_INFO_THREAD_SEP  ";"

/** function prototype of message handlers */
typedef mstro_status (*mstro_msg_handler)(const struct mstro_msg_envelope *envelope);

/** messaging context: To make the communication fully asynchronous we
    need to store some information about the send/receive message for
    the completion queue. Since we sometimes also have to wait for
    completions (in the worker in particulary) we provide an
    (optional) semaphore mechanism. And finally, some open fabric
    providers need us to provide scratch space. While we're at it we
    do that.

    (No timings done whether this helps for providers that don't
    require it, but we really have no choice, since we want to support verbs) */
struct mstro_ofi_msg_context_ {
  union {
    struct fi_context  ofi_ctx;     /**< scratch space for OFI providers, don't touch */
    struct fi_context2 ofi_ctx2;    /**< scratch space for OFI providers, don't touch */
  };
  bool needs_repost;                /**< indicates that the context is
                                     * one of our 'pre-posted'
                                     * incoming ones, not an ad-hoc
                                     * one */
  uint8_t has_completion;           /**< indicates if pthread waiting
                                     * infrastructure is initialized in this
                                     * context and should be used*/       
  pthread_cond_t completion;        /**< Condition variable for originator of
                                     * message to */
  pthread_mutex_t lock;             /**< a lock, mostly because cond_wait
                                     * requires one */
  uint64_t fi_cq_entry_flags;       /**< the flags that were set on the
                                     * completion entry for this context */
  mstro_msg_handler msg_handler;    /**< incoming message handler
                                     * assigned to context for processing */
  struct mstro_msg_envelope *msg;   /**< the message */
  struct mstro_endpoint *ep;        /**< the endpoint that this context is currently submitted to (or NULL) */
  struct fid_mr *msg_mr;           /**< memory registeration of the msg field */
  void *msg_mr_desc;               /**< mr desc */
  mstro_event ev;                   /**< RDMA completion event */
  mstro_mempool pool;               /**< parent memory pool*/
};
typedef struct mstro_ofi_msg_context_ * mstro_ofi_msg_context;

/** Create a message completion context for @arg msg.
    
    If @arg want_completion is true a mutex and a condition variable
    will be initialized and the completion handler can signal
    completion on that condition variable to the originator.

    Typical usage: Submission of the ctx to a libfabric call occurs
    while holding the lock, and then the caller does a cond_wait
    immediately to be woken up when the operation has completed.
    */
mstro_status
mstro_ofi__msg_context_create(mstro_ofi_msg_context *result_p,
                              struct mstro_msg_envelope *msg,
                              bool want_completion,
                              bool want_repost,
                              mstro_mempool pool);

/** destroy an OFI message context */
mstro_status
mstro_ofi__msg_context_destroy(mstro_ofi_msg_context ctx);

/** remember context as 'live'. FIXME: should not be external, but rdma transport code needs it currently */
mstro_status
mstro_ofi__remember_ctx(struct mstro_endpoint *ep,
                        mstro_ofi_msg_context ctx);

/** drop CTX from table of live contexts */
mstro_status
mstro_ofi__forget_ctx(struct mstro_endpoint *ep,
                      mstro_ofi_msg_context ctx);

/** registers @arg buf with len for fi @arg access
 * @arg mr and local_buf_mr_desc are the output memory registeration and local descriptor */
mstro_status
mstro_ofi__mreg_attr(enum fi_hmem_iface iface, uint64_t device,
                struct fi_domain_attr *domain_attr,struct fid_domain *domain, uint64_t access,
                void* buf, size_t len,
                struct fid_mr **mr, void **local_buf_mr_desc);

/** check if @arg mr_mode indicates that @arg mr needs to be bound to @arg ep and enabled before being usable. If so: do it
 *
 * FIXME: should not be external, but rdma transport code needs it currently 
 */
mstro_status
mstro_ofi__maybe_bind_and_enable_mreg(struct fid_ep *ep, int mr_mode, struct fid_mr *mr);

/** Check if we need to register local buffers for fi_read (writes into buffer)
 *  - Check if it host and device memory @arg iface
 *  - Register memory regoin @arg buf
 *  - Check if binding registration to endpoint is needed
 *  @arg mr and @arg local_buf_mr_desc are output variables that could be NULL if registeration is not required
 */
mstro_status
mstro_ofi__maybe_register_local_buffer(
                enum fi_hmem_iface iface, uint64_t device,
                struct fi_domain_attr *domain_attr, struct fid_ep *ep,
                struct fid_domain *domain, void* buf, size_t len,
                struct fid_mr **mr, void **local_buf_mr_desc);

/* a hash table for the set of live message contexts */
KHASH_INIT(ctxtab, mstro_ofi_msg_context,
           /* dummy: value type */ char,
           /* not a map */ 0,
           kh_voidptr_hash_func, kh_voidptr_hash_equal)



/** structure holding information about an endpoint */
struct mstro_endpoint {
  struct fid_cq     *cq;
#ifdef WITH_RMA_COUNTER
  struct fid_cntr   *rmacnt; /**< unused unless WITH_RMA_COUNT is enabled */
  uint64_t last_rma_cnt;
#endif
  struct fid_av     *av;
  struct fid_ep     *ep;
  struct fid_domain *domain;
  struct fid_eq     *eq;
  struct fid_fabric *fabric;
  struct fi_info    *fi;
  struct fid_mr     *component_info_mr;
  uint64_t           component_info_addr;
  size_t             component_info_keysize;
  uint8_t           *component_info_raw_key;
  struct fid_mr     *peer_info_mr; /**< memory registration for reading config block from a peer */
  /** the address to be passed to clients */
  /* void *addr;            /\**< address buffer *\/ */
  /* size_t addrlen;        /\**< address length *\/ */
    /* protobuf-based descriptor */
  Mstro__Endpoint        *pbep;    /**< endpoint protocol and address */
  Mstro__OfiCredential   *cred;    /**< credential -- NULL if none */
  Mstro__OfiMemoryRegion *inforeg; /**< config block MR location */

  char *serialized; /**< string-form of an endpoint
                          * for passing OOB */

  /** table of in-flight contexts of this endpoint */
  khash_t(ctxtab) *live_ctxs;
  /** lock for live_ctxs tab */
  pthread_mutex_t live_ctxs_mtx;

  struct mstro_endpoint *next; /**< make structure sinly-linked list linkable */
};

struct mstro_endpoint_set {
  size_t size; /**< number of endpoints */
  size_t num_free; /**< number of free EPS entries beyond SIZE */
  char *serialized_eps; /**< if non-NULL, a serialized version of the set */
  struct mstro_endpoint eps[1]; /**< endpoints; auto-extended structure */
};

/**
 * @brief register app when joining
 * 
 * @param op operation handle
 * @param entry_p registry entry handle
 * @return mstro_status return status
 */
mstro_status
mstro_pm__register_app(mstro_pool_operation op,
                       struct mstro_pm_app_registry_entry **entry_p);

mstro_status
mstro_appinfo_deserialize(const char *serialized_eps,
                          Mstro__AppInfo **result_p);

mstro_status
mstro_appinfo_serialize(const struct mstro_endpoint * ep,
                        char **result_p);


const char *
mstro_endpoint_describe(const struct mstro_endpoint *ep);


mstro_status
mstro_mr_key_get(struct fi_info* fi, struct fid_mr* mr, 
                 uint8_t** mr_key, size_t* keysize, uint64_t* addr);

/* mstro_status */
/* mstro_ofi__check_compatibility(bool *suitable_p, */
/*                                const struct mstro_endpoint *remote, */
/*                                const struct mstro_endpoint *local); */

/**@brief Initialize OFI layer.
 **
 **
 ** Populates OFI related data structures to enable OFI threads 
 **
 **/
mstro_status
mstro_ofi_init(void);

/** atexit cleanup function for OFI layer */
mstro_status
mstro_ofi__atexit(void);

/**@brief De-initialize OFI layer.
 **
 ** Clears any other OFI-related infrastructure (memory registrations etc.)
 **
 ** Must only be called if no more interactions with the OFI layer are
 ** happening, in particular no further CQ reads, or read/send/recv
 ** ops on the endpoints.
 **/
mstro_status
mstro_ofi_finalize(bool destroy_drc_info);



#ifdef DEPRECATED
mstro_status
mstro_ofi__submit_message_wait(struct mstro_endpoint *ep, fi_addr_t dst,
                               struct mstro_msg_envelope *msg);
#endif

size_t
mstro_ofi__get_num_recv(const struct mstro_endpoint *ep);

mstro_status
mstro_ofi__loop_post_recv(struct mstro_endpoint *ep, mstro_mempool pool);

mstro_status
mstro_ofi__check_and_handle_cq(struct mstro_endpoint *ep,
                               mstro_msg_handler incoming_msg_handler);

mstro_status
mstro_ofi_destroy_ep_set(struct mstro_endpoint_set *endpoints);


/**
 * @brief bottom half of ofi_init, look for endpoints available and build them
 * 
 * @param ep_set Set of endpoints found and build (ouput)
 * @param threading requested OFI threading level
 * @return mstro_status MSTRO_OK on success
 */
mstro_status
mstro_ofi_discover_and_build_eps(struct mstro_endpoint_set **ep_set, enum fi_threading threading);

/** select the best match between remote and local endpoints.
 * This is an asynchronous function whose completion can be observed via the request handle (using test/wait)
 */
mstro_status
mstro_ofi__select_endpoint(const Mstro__AppInfo *remote,
                           struct mstro_endpoint **local_p,
                           fi_addr_t *remote_addr_p, struct mstro_endpoint_set *my_endpoints,
                           mstro_request *request_handle);

/**@} (end of group MSTRO_I_OFI) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_OFI_H_ */
