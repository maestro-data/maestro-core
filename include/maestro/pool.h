/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool public header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_POOL_H_
#define MAESTRO_POOL_H_ 1

#include "maestro/status.h"
#include "maestro/attributes.h"
#include "maestro/core.h"
 
#include <pthread.h>

/**@addtogroup MSTRO_Core
 **@{
  **/

/**@defgroup MSTRO_Pool Maestro Pool Management
 **@{
 ** This is the CDO Management API, as developed for D3.2
 **/

/** opaque CDO handle */
typedef struct mstro_cdo_ *mstro_cdo;

/**
 ** @brief Submit CDO to pool (for producers)
 **
 ** This function is a blocking call, it waits until the cdo state is updated in the pool manager.
 **
 ** After this function returns the *cdo* is @ref POOLED and no longer
 ** @ref ACCESSIBLE
 **
 ** The only operations permitted are attribute queries and @ref
 ** mstro_cdo_withdraw()
 **
 ** If cdo is not sealed (@ref mstro_cdo_declaration_seal()) it will
 ** be sealed before this call returns.
 **
 ** @param[in] cdo A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_offer(mstro_cdo cdo);


/**
 ** @brief Submit CDO to pool (for producers)
 **
 ** This function is non-blocking version of @ref mstro_cdo_offer, even if the cdo is distributed.
 **
 ** After this function returns the *cdo* is @ref POOLED and no longer
 ** @ref ACCESSIBLE
 **
 ** The only operations permitted are attribute queries and @ref
 ** mstro_cdo_withdraw()
 **
 ** Unlike @ref mstro_cdo_offer, this function requires that the cdo is sealed, otherwise it resturns an error.
 **
 ** @param[in] cdo A CDO handle.
 ** @param[out] request a maestro cdo operation request handler
 ** @returns A status code, ::MSTRO_OK on success.
 **/
 mstro_status
 mstro_cdo_offer_async(mstro_cdo cdo, mstro_request *request);


/**
 ** @brief Withdraw a CDO from the pool (for producers)
 **
 ** The *cdo* must have been previously submitted using @ref
 ** mstro_cdo_offer().
 **
 ** This operation is blocking until the cdo data is actually available for
 ** local access as specified by the access intent attributes.
 **
 ** @param[in] cdo A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_withdraw(mstro_cdo cdo);


/**
 ** @brief Withdraw a CDO from the pool (for producers)
 **
 ** The *cdo* must have been previously submitted using @ref
 ** mstro_cdo_offer() or @ref mstro_cdo_offer_async().
 **
 ** This operation is non-blocking version of @ref mstro_cdo_withdraw()
 **
 ** The user should check that the operation is completed before accessing cdo data
 ** locallly.
 **
 ** @param[in] cdo A CDO handle.
 ** @param[out] request a maestro cdo operation request handler
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_withdraw_async(mstro_cdo cdo, mstro_request *request);

/**
 ** @brief Submit CDO to pool (for consumers)
 **
 ** This function is non-blocking, even if the *cdo* is distributed.
 **
 ** After this function returns the *cdo* is @ref POOLED and no longer
 ** @ref ACCESSIBLE
 **
 ** The only operations permitted are attribute queries and @ref
 ** mstro_cdo_demand() or @ref mstro_cdo_retract()
 **
 ** If *cdo* is not sealed (@ref mstro_cdo_declaration_seal()) it will
 ** be sealed before this call returns.
 **
 ** @param[in] cdo A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_require(mstro_cdo cdo);

/**
 ** @brief Submit CDO to pool (for consumers)
 **
 ** This function is non-blocking, even if the *cdo* is distributed.
 ** Currently equivalent to @ref mstro_cdo_require, it creates and fills a mstro_request for convenience.
 **
 ** After this function returns the *cdo* is @ref POOLED and no longer
 ** @ref ACCESSIBLE
 **
 ** The only operations permitted are attribute queries and @ref
 ** mstro_cdo_demand() or @ref mstro_cdo_retract()
 **
 ** Unlike @ref mstro_cdo_require(), the *cdo* should be sealed (@ref mstro_cdo_declaration_seal()).
 **
 ** @param[in] cdo A CDO handle.
 ** @param[out] request a maestro cdo operation request handler
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_require_async(mstro_cdo cdo, mstro_request *request);

/**
 ** @brief Withdraw a CDO from the pool (for consumers)
 **
 ** The *cdo* must have been previously submitted using @ref
 ** mstro_cdo_require().
 **
 ** This operation is blocking until the *cdo* data is actually available for
 ** local access as specified by the access intent attributes.
 **
 ** @param[in] cdo A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_demand(mstro_cdo cdo);

/**
 ** @brief Withdraw a CDO from the pool (for consumers)
 **
 ** The *cdo* must have been previously submitted using @ref
 ** mstro_cdo_require() or @mstro_cdo_require_async().
 **
 ** This operation is non-blocking version of @ref mstro_cdo_demand() 
 ** Use @ref mstro_request_wait() or @ref mstro_request_test() to check that the *cdo* data is actually available for
 ** local access as specified by the access intent attributes.
 **
 ** @param[in] cdo A CDO handle.
 ** @param[out] request a maestro cdo operation request handler
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_demand_async(mstro_cdo cdo, mstro_request *request);

/**
 ** @brief Retract an outstanding CDO request from the pool (for consumers)
 **
 ** The *cdo* must have been previously submitted using @ref
 ** mstro_cdo_require().
 **
 ** This operation is blocking.
 **
 ** The CDO is removed from the pool, without data becoming available
 ** for use by the requesting process. It amounts to canceling the
 ** mstro_cdo_require() request.
 **
 ** @param[in] cdo A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_retract(mstro_cdo cdo);

/**
 ** @brief Asynchronously retract an outstanding CDO request from the pool (for consumers)
 **
 ** The *cdo* must have been previously submitted using @ref
 ** mstro_cdo_require().
 **
 ** This operation is non-blocking.
 **
 ** The CDO is removed from the pool, without data becoming available
 ** for use by the requesting process. It amounts to canceling the
 ** mstro_cdo_require() request.
 **
 ** @param[in] cdo A CDO handle.
 ** @param[out] request a maestro cdo operation request handler
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_retract_async(mstro_cdo cdo, mstro_request *request);

/** A handle type for pool event subscriptions */
typedef struct mstro_subscription_ * mstro_subscription;

enum mstro_pool_event_kind {
  MSTRO_POOL_EVENT_NONE                = 0UL,

  /* cdo-related */
  MSTRO_POOL_EVENT_DECLARE             = 1U <<  0,
  MSTRO_POOL_EVENT_DECLARE_ACK         = 1U <<  1,
  MSTRO_POOL_EVENT_SEAL                = 1U <<  2,
  MSTRO_POOL_EVENT_SEAL_GROUP          = 1U <<  3,
  MSTRO_POOL_EVENT_OFFER               = 1U <<  4,
  MSTRO_POOL_EVENT_REQUIRE             = 1U <<  5,
  MSTRO_POOL_EVENT_WITHDRAW            = 1U <<  6,
  MSTRO_POOL_EVENT_DEMAND              = 1U <<  7,
  MSTRO_POOL_EVENT_RETRACT             = 1U <<  8,
  MSTRO_POOL_EVENT_DISPOSE             = 1U <<  9,

  MSTRO_POOL_EVENT_TRANSPORT_INIT      = 1U << 13,
  MSTRO_POOL_EVENT_TRANSPORT_TICKET    = 1U << 14,
  MSTRO_POOL_EVENT_TRANSPORT_COMPLETED = 1U << 15,

  /* pool-related */
  MSTRO_POOL_EVENT_APP_JOIN            = 1U << 24,
  MSTRO_POOL_EVENT_APP_WELCOME         = 1U << 25,
  MSTRO_POOL_EVENT_APP_LEAVE           = 1U << 26,
  MSTRO_POOL_EVENT_APP_BYE             = 1U << 27,
  MSTRO_POOL_EVENT_POOL_CHECKPOINT     = 1U << 28,
  MSTRO_POOL_EVENT_SUBSCRIBE           = 1U << 29,
  MSTRO_POOL_EVENT_UNSUBSCRIBE         = 1U << 30,

  MSTRO_POOL_EVENT_ALL                 = ~(int)0
};
typedef enum mstro_pool_event_kind mstro_pool_event_kinds;

/** Return a printable description of the event @arg event */
const char*
mstro_pool_event_description(enum mstro_pool_event_kind event);

/** A selector object for CDOs */
typedef struct mstro_cdo_selector_ *mstro_cdo_selector;

/**@brief Construct a CDO selector
 *
 * The @arg schema can be NULL, in which case the default maestro-core schema will be used.
 *
 * The @arg namespace can be NULL, in which case only fully-qualified attribute names will used.
 *
 * The syntax for CDO selector construction is as follows:
 *
 * selector <- ''
             | attribute-selector
 *           | union-selector
 *           | intersection-selector
 *
 * attribute-selector <- attribute-key kv-op value
 *                    |  '(' 'has' attribute-key ')'
 *                    |  '(' attribute-selector ')'
 *                    |  '(' 'not' attribute-key ')'
 * kv-op <- '=' | '!='                   # type-based (non-)equality
 *        | '<' | '<=' | '>' | '>='      # type-based comparison (numerical types)
 *        | '~='                         # regex-match on string representation of value
 *
 * union-selector <- '(' 'or' selector-list ')'
 *
 * intersection-selector <- '(' 'and' selector-list ')'
 *
 * selector-list <- selector
 *               | selector ' ' selector-list
 *
 * attribute-key must be a valid fully-qualified attribute name, or an
 * unqualified attribute in the default namespace given by the @arg
 * name_space argument.

 * values must be parsable according to the type; strings must be
 * written as "text". Regular expressions must be written as like
 * strings but can have a vim-style postfix for 'case insensitive'
 * matches "regex"/i
 *
 * Example: (and (.maestro.core.cdo.name ~= "^CDO.*"/i)
 *               (.maestro.core.cdo.persist = true))
 *            -- match all CDOs whose name starts with CDO (ignoring case) and which have the persist flag set
 *
 * A NULL @arg query indicates 'no restriction', just like the 0-element string.
 */
mstro_status
mstro_cdo_selector_create(mstro_schema schema,
                          const char *name_space,
                          const char *query,
                          mstro_cdo_selector *result);

/**@brief dispose a CDO selector
 */
mstro_status
mstro_cdo_selector_dispose(mstro_cdo_selector s);

/** Subscription options */
enum mstro_subscription_opts {
  /** Default options set: No ack required, IDs resolved to string
   * names, event notification after processing the message */
  MSTRO_SUBSCRIPTION_OPTS_DEFAULT     = 0,

  /** Intercept operation that caused the event notification until the
   * subscriber sends an ACK using mstro_subscription_ack() */
  MSTRO_SUBSCRIPTION_OPTS_REQUIRE_ACK = (1 << 0),

  /** Do not resolve IDs in the event message (CDO-ID, application ID,
   * ...)
   *
   * This may be useful to implement lightweight events (counting
   * only, or when the selector makes it possible for the subscriber
   * to know the IDs implicitly.
   *
   * All string fields in the event data structure will be NULL in
   * this case.
  */
  MSTRO_SUBSCRIPTION_OPTS_NO_RESOLVE  = (1 << 1),

  /** Perform event notification before even handling the message that
   *  caused the event.
   *
   * Note that any side effects to the PM state are not visible at
   * that time, so, for instance, CDO selection evaluation may be very
   * limited */
  MSTRO_SUBSCRIPTION_OPTS_SIGNAL_BEFORE = (1 << 2),

  /** Do not notify the subscriber of local events.
   *
   * This is especially useful when requiring acknowledgement -- in particular
   * for both-sided ops such as DECLARE/SEAL -- which may unnecessarily stall
   * our own process with its own own ops, and also pollute our polls with
   * unhelpful notifications.
   */
  MSTRO_SUBSCRIPTION_OPTS_NO_LOCAL_EVENTS = (1 << 3)

};

/** @brief Create a subscription for CDO events
 *
 * @arg cdos specifies a CDO selector (see ...); @arg events is a ORed
 * set of events that the caller is interested in.
 *
 * If @arg require_ack is set, indicates that the subscriber wants the
 * pool manager to wait with with the next steps in the event until a
 * call to mstro_subscription_ack().
 *
 * The CDO selector can be disposed after the call to this function,
 * or re-used for other subscriptions.
 */
mstro_status
mstro_subscribe(mstro_cdo_selector cdos, mstro_pool_event_kinds events,
                enum mstro_subscription_opts flags,
                mstro_subscription *result);


/** An event descriptor */
struct mstro_pool_event_ {
  enum mstro_pool_event_kind kind;  /**< the event kind */
  uint64_t serial;             /**< the event identifier */
  mstro_nanosec_t  ctime;      /**< the time the event was created at the source */
  union {
    /* for DECLARE events */
    struct {
      mstro_app_id         appid;     /**< component that declares */
      char                *cdo_name;  /**< the CDO name */
    } declare;

    /* for DECLARE-ACK events */
    struct {
      mstro_app_id         appid;     /**< component that declared */
      char                *cdo_name;  /**< the CDO name for which an ID was assined */
      /* FIXME: expose ID or not? */
    } declare_ack;

    /* for SEAL events */
    struct {
      mstro_app_id        appid;    /**< component that seals */
      char               *cdo_name; /**< CDO sealed */
      uint64_t          total_size; /**< cumulated CDO size over (possibly distributed chunks) */
      int              n_participants; /* number of participating processes */
    } seal;

    /* for SEAL-GROUP events */
    struct {
      mstro_app_id        appid;  /**< component that seals */
    } seal_group;

    /* for OFFER */
    struct {
      mstro_app_id        appid;    /**< component that offers */
      char               *cdo_name; /**< CDO offered */
    } offer;

    /* for REQUIRE */
    struct {
      mstro_app_id        appid;  /**< component that requires */
      char               *cdo_name; /**< CDO required */
    } require;

    /* for WITHDRAW */
    struct {
      mstro_app_id        appid;  /**< component that withdraws */
      char               *cdo_name; /**< CDO withdrawn */
    } withdraw;

    /* for DEMAND */
    struct {
      mstro_app_id        appid;  /**< component that demands */
      char               *cdo_name; /**< CDO demanded */
    } demand;

    /* for RETRACT */
    struct {
      mstro_app_id        appid;  /**< component that retracts */
      char               *cdo_name; /**< CDO retracted */
    } retract;

    /* for DISPOSE */
    struct {
      mstro_app_id        appid;  /**< component that disposes */
      char               *cdo_name; /**< CDO disposed */
    } dispose;

    /* for TRANSPORT_INIT */
    struct {
      mstro_app_id        provider; /**< component that is asked to provide */
      char               *cdo_name; /**< CDO being transported */
      mstro_app_id        target;   /**< component that will receive */
    } transport_init;

    /* for TRANSPORT_TICKET */
    struct {
      mstro_app_id        provider; /**< component that is asked to provide */
      char               *cdo_name; /**< CDO being transported */
      mstro_app_id        target;   /**< component that will receive */
    } transport_ticket;

    /* for TRANSPORT_COMPLETED */
    struct {
      mstro_app_id        provider; /**< component that has provided */
      char               *cdo_name; /**< CDO being transported */
      mstro_app_id        target;   /**< component that has received */
      mstro_app_id        completion_target; /**< component that is receiving the notification */
    } transport_completed;

    /* JOIN */
    struct {
      char               *component_name; /**< component that is trying to join */
      mstro_app_id        appid; /**< the app ID assigned (MSTRO_APP_ID_INVALID if JOIN:pre is observed) */
    } join;

    /* WELCOME */
    struct {
      /** app ID assigned to component */
      mstro_app_id        appid;
      /** component that is welcomed */
      char               *component_name;
    } welcome;

    /* LEAVE */
    struct {
      /** component that is trying to leave */
      mstro_app_id        appid;
      char               *component_name;
    } leave;

    /* BYE */
    struct {
      mstro_app_id        appid;          /**< component that is leaving */
    } bye;

    /* POOL_CHECKPOINT */
    struct {
      void *              dummy;         /**< dummy payload */
    } pool_checkpoint;
  } payload;

  struct mstro_pool_event_ *next; /**< linked-list: next event */

  size_t refcount; /**< reference count */
};

/** An event descriptor pointer */
typedef struct mstro_pool_event_ *mstro_pool_event;

/** @brief Wait until an event happens on @arg s.
 */
mstro_status
mstro_subscription_wait(mstro_subscription s, mstro_pool_event *event);

/** @brief Wait until an event happens on @arg s, or timeout occurs
 *
 * @arg deadline is an absolute time, like for
 * pthread_cond_timedwait(). If you want to set a relative timeout use
 * code like
 *
 *     struct timeval tv;
 *     struct timespec deadline;
 *     gettimeofday(&tv, NULL);
 *     deadline.tv_sec = tv.tv_sec + 0;
 *     deadline.tv_nsec = 0;
 *
 * and add the desired relative time offset to that @ref deadline value.
 */
mstro_status
mstro_subscription_timedwait(mstro_subscription s,
                             const struct timespec *deadline,
                             mstro_pool_event *event);

/** @brief Poll subscription @args s
 *
 * @arg *events will be NULL if no event is available, otherwise
 * contain a list of events that occured on @arg s between the last
 * successful call to a poll or wait operation.

 * @returns ::MSTRO_OK on success, including the case when @arg *event==NULL
 */
mstro_status
mstro_subscription_poll(mstro_subscription s, mstro_pool_event * events);

/** @brief Acknowledge processing of event @arg event
 *
 * If @arg event is a list of multiple events, acknowledge all of them.
 */
mstro_status
mstro_subscription_ack(mstro_subscription s, mstro_pool_event events);

/** @brief dispose an event object list
 *
 * If @arg event is a list of multiple events, dispose all of them.
 */
mstro_status
mstro_pool_event_dispose(mstro_pool_event events);

/** @brief dispose a event object entry
 *
 * If @arg If the event object is a list, ignore that fact and just delete the pointed one.
 */
mstro_status
mstro_pool_event_dispose_single(mstro_pool_event e);

/* End subscription @arg s.
 *
 * For subscriptions with MSTRO_SUBSCRIPTION_OPTS_REQUIRE_ACK set, all
 * unhandled events will be automatically acknowledged.
 */
mstro_status
mstro_subscription_dispose(mstro_subscription s);


/**@} (end of group MSTRO_Pool) */
/**@} (end of group MSTRO_Core) */

#endif
