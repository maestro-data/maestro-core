/* -*- mode:c -*- */
/** @file
 ** @brief Maestro common hash types and support functions
 **/
/*
 * Copyright (C) 2020 HP Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_HASHING_H_
#define MAESTRO_I_HASHING_H_ 1

#include "maestro/i_cdo.h"
#include "maestro/i_uthash.h"
#include "maestro/i_khash.h"

/**@ingroup MSTRO_Internal Internal Maestro Core API
 **@{
 */

/* hash function for 64bit int */
static inline uint64_t Wang_hash_64(uint64_t key)
{
  key += ~(key << 32);
  key ^= (key >> 22);
  key += ~(key << 13);
  key ^= (key >> 8);
  key += (key << 3);
  key ^= (key >> 15);
  key += ~(key << 27);
  key ^= (key >> 31);
  return key;
}

/* hash function for void* */
static inline khint_t kh_voidptr_hash_func(void *key)
{
  uintptr_t k = (uintptr_t)key;
  if(sizeof(k)==64/8) {
    return kh_int64_hash_func(Wang_hash_64(k));
  } else {
    khint_t res=0;
    for(size_t word=0; word<sizeof(uintptr_t); word++) {
      res ^= kh_int_hash_func( (khint32_t)(k >> 32*word));
    }
    return res;
  }
}
/* comparison for void* khash table keys */
#define kh_voidptr_hash_equal(a,b) ((a)==(b))

/* hash (deep) for CDOID pointers in hashing */
static inline
khint_t kh_cdoidptr_hash_func(const struct mstro_cdo_id *key)
{
  khint_t hash = 0;
  
  uint32_t h1 = key->qw[0] ^ key->qw[1];

  uint64_t tmp = key->local_id;
  h1 ^= ((tmp)>>33^(tmp)^(tmp)<<11);

  hash=(khint_t)h1;
  return hash;
}

/* comparison (deep) for CDOID pointers in hashing */
#define kh_cdoidptr_hash_equal(a,b) mstro_cdo_id__equal(a,b)



/* hash iteration for hashtables  that have no values */

/*! @function
  @abstract     Iterate over the keys in the hash table
  @param  h     Pointer to the hash table [khash_t(name)*]
  @param  kvar  Variable to which key will be assigned
  @param  code  Block of code to execute
 */
#define kh_foreach_key(h, kvar, code) do {                              \
    khint_t __i;                                                        \
    for (__i = kh_begin(h); __i != kh_end(h); ++__i) {                  \
      if (!kh_exist(h,__i)) continue;                                   \
      (kvar) = kh_key(h,__i);                                           \
      code;                                                             \
    }                                                                   \
  } while(0)

static inline
khint_t mstro_khash_combine(khint_t s, khint_t v) {
  khint_t h;
  s^= v + 0x9e3779b9 + (s<< 6) + (s>> 2);
  h = s;
  return h;
}

/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_HASHING_H_ */
