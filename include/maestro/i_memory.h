/* -*- mode:c -*- */
/** @file
 ** @brief Maestro lockable memory utility implementation
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_MEMORY_H_
#define MAESTRO_I_MEMORY_H_

#include "maestro.h"

#include <stdint.h>
#include <stdbool.h>

void *
mstro_alloc_lockable(size_t size, bool lock_immediately);
void
mstro_unlock_lockable(void *ptr, size_t size);
mstro_status 
mstro_free_lockable(void *ptr, size_t size, bool needs_unlock);

/** memory key requested for the component descriptor */
#define MSTRO_MEM_KEY_COMPONENT_DESCRIPTOR ((uint64_t)1)
/* other low values reserved */
/** first memory key value used for ad-hoc memory keys */
#define MSTRO_MEM_KEY_BASE                 ((uint64_t)42)

/** Obtain a new (unique per PC) memory key for RDMA purposes */
uint64_t mstro_memory_new_key(void);

#endif /* MAESTRO_I_MEMORY_H_ */
