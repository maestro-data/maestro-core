/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Global Variable declarations
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_GLOBALS_H_
#define MAESTRO_I_GLOBALS_H_ 1

#include "maestro.h"
#include "maestro/i_state.h"
#include "maestro/i_pool_manager_registry.h"
#include "protocols/mstro_pool.pb-c.h"
#include "attributes/maestro-schema.h"
#include <stdbool.h>
#include <stdatomic.h>
#include <pthread.h>
#include <mamba.h>
#include "maestro/env.h"
#include "logging.h"

/** Init data; set at mstro_core_init() time; protected by @ref g_initdata_mtx */
extern struct mstro_core_initdata * g_initdata;
/** lock to protect @ref g_initdata */
extern pthread_mutex_t g_initdata_mtx;


/** Hash table of local CDO handles. Protected by g_mstro_pool_local_mtx */
extern struct mstro_pool_entry * g_mstro_pool_local;

/** Lock protecting @ref g_mstro_pool_local */
extern pthread_mutex_t g_mstro_pool_local_mtx;

/** Key for thread-local descriptor (mainly for logging) */
extern pthread_key_t g_thread_descriptor_key;

/** The NULL CDO-ID */
extern struct mstro_cdo_id MSTRO_CDO_ID_NULL;

/** Set to True if this application has a pool manager to talk to */
extern _Atomic bool g_mstro_pm_attached;

/** Set to True if this application's JOIN was rejected by the PM */
extern _Atomic bool g_mstro_pm_rejected;

/** Transport initiator thread.

    This one picks entries off the @ref g_mstro_pm_demand_queue and
    initiaties transport */
extern pthread_t g_pm_transport_init_thread;

/** Pool client thread. */
extern pthread_t g_pc_thread;

/** The workflow name specified at mstro_init() time */
extern char *g_workflow_name;

/** The component name specified at mstro_init() time */
extern char *g_component_name;

/** The pool manager-assigned app ID of the process. MSTRO_APP_ID_INVALID if not associated with the pool */
extern _Atomic(mstro_app_id) g_pool_app_id;

/** the default (DRAM) memspace */
extern mmbMemSpace *g_default_cdo_memspace;

/** the default (DRAM) memory interface */
extern mmbMemInterface *g_default_cdo_interface;

/** MIO can be compiled in, but run time config may be missing, or MIO may fail
 * to initialize. This flag is TRUE if MIO is properly initialized and usable
 * */
extern bool g_mio_available;


/** the app token used in communicating to the pool manager. Set after successfully JOINing, cleared after LEAVEing */
extern Mstro__Pool__Apptoken g_pool_apptoken;

/** the app ID (packed version of @ref g_pool_app_id) used in communicating with the pool manager. */
extern Mstro__Pool__Appid    g_pool_appid;

/** the fundamental built-in attribute schema. Filled early in mstro_core_init(), then constant */
extern mstro_schema g_mstro_core_schema_instance;

/** flag set if numa is working */
extern bool g_have_numa;
/** Number of cpus in the system, derived from the cpu numbers in /sys/devices/system/cpu */
extern int g_numa_configured_cpus;
/** number of memory nodes in the system, derived from the node numbers in /sys/devices/system/node */
extern int g_numa_configured_nodes;
/* list of cores to bind operation threads to it, populated by parsing MSTRO_ENV_BIND_OP_HANDLER at mstro_core__numa_init */
extern int *g_numa_op_bind_cores;
/* length of g_numa_op_bind_cores */
extern int g_numa_op_bind_cores_len;
/* id of the next core to pin from g_numa_op_bind_cores list */
extern _Atomic(int) g_numa_op_bind_next;

/* list of cores to bind PM/PC threads to it, populated by parsing MSTRO_BIND_PM_PC at mstro_core__numa_init */
extern int *g_numa_pm_pc_bind_cores;
/* length of g_numa_pm_pc_bind_cores */
extern int g_numa_pm_pc_bind_cores_len;
/* id of the next core to pin from g_numa_pm_pc_bind_cores list */
extern _Atomic(int) g_numa_pm_pc_bind_next;

/* list of cores to bind transport threads to it, populated by parsing MSTRO_BIND_TRANSPORT_THREAD at mstro_core__numa_init */
extern int *g_numa_transport_bind_cores;
/* length of g_numa_transport_bind_cores */
extern int g_numa_transport_bind_cores_len;
/* id of the next core to pin from g_numa_transport_bind_cores list */
extern _Atomic(int) g_numa_transport_bind_next;

/** Indicator for component block type 'undefined' */
#define MSTRO_COMPONENT_TYPE_UNDEF  0
/** Indicator for component block type Pool Manager */
#define MSTRO_COMPONENT_TYPE_PM     1
/** Indicator for component block type non-PM App */
#define MSTRO_COMPONENT_TYPE_APP    2

/** the pool protocol version: MAJOR.MINOR.PATCH */
#define MSTRO_POOL_PROTOCOL_VERSION_MAJOR 2
#define MSTRO_POOL_PROTOCOL_VERSION_MINOR 1
#define MSTRO_POOL_PROTOCOL_VERSION_PATCH 0

/** Descriptor block for the entity */
union mstro_component_descriptor {
  struct {
    /** the type of descriptor block */
    int64_t type;
    /** the pool protocol version used */
    union {
      struct {
        uint8_t major; uint8_t minor; uint8_t patch;
      };
      uint64_t v;
    } protocol_version;
    /** the workflow name */
    char workflow_name[MSTRO_WORKFLOW_NAME_MAX];
    /** the component name */
    char component_name[MSTRO_WORKFLOW_NAME_MAX];
    /** the list of user schemas */
    char schema_list[MSTRO_WORKFLOW_NAME_MAX];
    /** the maestro core version */
    char version[128];
  }; /*<** descriptor data */
  /** padding to 16k */
  uint8_t padding[2U<<14];
};

/** component decriptor of this entity */
extern union mstro_component_descriptor g_component_descriptor;

/** maximum size for CDOs to be transported inline */
extern size_t g_transport_inline_max;

/** the pool manager protocol version */
#define MSTRO_POOL_PROTOCOL_VERSION ((uint32_t)        \
	(  (MSTRO_POOL_PROTOCOL_VERSION_MAJOR <<16)    \
         | (MSTRO_POOL_PROTOCOL_VERSION_MINOR << 8)    \
         | (MSTRO_POOL_PROTOCOL_VERSION_PATCH << 0)))

/** separators for user defined schema lists and paths enviroment variables */
#define SCHEMA_LIST_SEP ";"
#define SCHEMA_PATH_SEP ":"

/** the precomputed default path for GFS transport (incl. trailing '/') */
extern char *g_mstro_transport_gfs_dir;
/** string length of @ref g_mstro_transport_gfs_dir */
extern size_t g_mstro_transport_gfs_dir_len;

/** handle to the pool operations handling thread team */
extern struct erl_thread_team *g_pool_operations_team;

/** macro to check whether we are initilized and return suitable error immediately if not */
#define MSTRO_API_NEEDS_INIT() do { \
       	if(NULL==g_initdata) {    \
	    LOG_ERR(MSTRO_LOG_MODULE_CORE, "Function %s cannot be called before mstro_init()\n", __func__); \
	    return MSTRO_UNINIT;  \
       	}                         \
   } while(0)

#endif /* MAESTRO_I_GLOBALS_H_ */
