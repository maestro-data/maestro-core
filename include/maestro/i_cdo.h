/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO implementation header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_CDO_H_
#define MAESTRO_I_CDO_H_ 1

#include "maestro/msm.h"
#include "maestro/scope.h"
#include "maestro/status.h"
#include "maestro/cdo.h"
#include "maestro/i_uthash.h"

#include "protocols/mstro_pool.pb-c.h"

#include <mamba.h>

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <yaml.h>
#include <assert.h>
#include <pthread.h>

/* alloca is a bit special ... */
#if HAVE_ALLOCA_H
# include <alloca.h>
#elif defined __GNUC__
# define alloca __builtin_alloca
#elif defined _AIX
# define alloca __alloca
#elif defined _MSC_VER
# include <malloc.h>
# define alloca _alloca
#else
# include <stddef.h>
# ifdef  __cplusplus
extern "C"
# endif
void *alloca (size_t);
#endif

/** CDO state bitfield */
typedef uint32_t mstro_cdo_state;

/** invalid CDO state */
#define MSTRO_CDO_STATE_INVALID    0
/** CDO in declaration phase */
#define MSTRO_CDO_STATE_CREATED   (1U<<0)
/** CDO fully declared. Set after DECLARE_ACK */
#define MSTRO_CDO_STATE_DECLARED  (1U<<1)
/** CDO sealer. Set after SEAL */
#define MSTRO_CDO_STATE_SEALED    (1U<<2)


/** CDO OFFERED to local pool. This is a temporary state when the CDO
 * has made it to the local pool, but the global OFFER is still
 * outstanding. If there is no pool manager it will quickly move to
 * ::MSTRO_CDO_STATE_OFFERED too. */
#define MSTRO_CDO_STATE_OFFERED_LOCALLY  (1U<<3)
/** CDO pooled by OFFER, globally (with or without pool manager) */
#define MSTRO_CDO_STATE_OFFERED          (1U<<4)
/** CDO pooled by REQUIRE */
#define MSTRO_CDO_STATE_REQUIRED         (1U<<5)
/** CDO pooled */
/* FIXME: check whether OFFERED_LOCALLY should be part of this. There
 * is a potential issue with WITHDRAW against a LOCALLY offered CDO
 * while the pool manager communication about the OFFER is not
 * completed. We might want to implement cancelation the OFFER message
 * in that case ... */
#define MSTRO_CDO_STATE_POOLED    (  MSTRO_CDO_STATE_OFFERED   \
                                   | MSTRO_CDO_STATE_REQUIRED)

/** CDO returned from global pool by WITHDRAW */
#define MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY (1U<<6)
/** CDO returned from pool by WITHDRAW */
#define MSTRO_CDO_STATE_WITHDRAWN          (1U<<7)
/** CDO returned from pool by DEMAND */
#define MSTRO_CDO_STATE_DEMANDED           (1U<<8)
/** CDO returned from pool by RETRACT */
#define MSTRO_CDO_STATE_RETRACTED          (1U<<9)
/** CDO returned from global pool by RETRACT */
#define MSTRO_CDO_STATE_RETRACTED_GLOBALLY (1U<<10)
//#define MSTRO_CDO_STATE_DISPOSED_GLOBALLY (1U<<11)
/** CDO REQUIRED to local pool. This is a temporary state when the CDO
 * has made it to the local pool, but the global REQUIRE is still
 * outstanding. If there is no pool manager it will quickly move to
 * ::MSTRO_CDO_STATE_REQUIRED too. */
#define MSTRO_CDO_STATE_REQUIRED_LOCALLY (1U<<11)

/** CDO returned from pool */
#define MSTRO_CDO_STATE_RETURNED  (  MSTRO_CDO_STATE_WITHDRAWN  \
                                   | MSTRO_CDO_STATE_DEMANDED   \
                                   | MSTRO_CDO_STATE_RETRACTED)

/** CDO that is withdrawable */
#define MSTRO_CDO_STATE_DISPOSABLE (  MSTRO_CDO_STATE_DECLARED  \
                                    | MSTRO_CDO_STATE_SEALED    \
                                    | MSTRO_CDO_STATE_WITHDRAWN \
                                    | MSTRO_CDO_STATE_DEMANDED  \
                                    | MSTRO_CDO_STATE_RETRACTED)
/** CDO dead. This is used in the PM for CDOs where we've seen a DISPOSE message. */
#define MSTRO_CDO_STATE_DEAD      (1U<<12)

/** extra flags used on pool manager */

/** CDO has been injected into the CDO registry for an application,
 * not DECLARED/SEALED by the app itself. This flag will be cleared
 * once the OFFER comes in from the respective app, which happens
 * after an unsolicited transport ticket is processed. */
#define MSTRO_CDO_STATE_INJECTED       (1U<<29)
/** CDO is currently part of an outstanding transport operation. ORed into REQUIRED or DEMANDED state */
#define MSTRO_CDO_STATE_IN_TRANSPORT   (1U<<30)
/** CDO has been transported in response to a REQUIRE and is pooled
 * (no DEMAND for it yet) REQUIRED|SATISFIED is
 * equivalent to OFFERED */
#define MSTRO_CDO_STATE_SATISFIED      (1U<<31)



/** CDO state flags that are to be preserved across state changes */
#define MSTRO_CDO_STATE_FLAGS                   \
    (MSTRO_CDO_STATE_INJECTED                   \
    |MSTRO_CDO_STATE_IN_TRANSPORT               \
    |MSTRO_CDO_STATE_SATISFIED)

/** Length of a CDO ID: 128 bit UUID in bytes */
#define MSTRO_CDO_ID_NUMBYTES (128/8)

/** length of the string representation of a CDO_ID: UUID/0xXXXX */
#define MSTRO_CDO_ID_STR_LEN (36+3+MSTRO_LOCAL_ID_STR_LEN+1)

/** The NIL UUID */
#define MSTRO_CDO_ID_INVALID { .id = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, .local_id = 0}

/** return a string describing the CDO state @arg s
 **
 ** The string is owned by this function, but this function can safely
 ** be called from multiple threads (the buffer is thread-local). The
 ** buffer will be re-used by subsequent calls in the same thread
 ** after a small number of invocations (currently: 3, see @ref
 ** MAX_STATE_DESCRIPTION_BUFFERS).
 **/
const char *
mstro_cdo_state_describe(mstro_cdo_state s);

/**@brief Provide a convenient scope in which VARNAME contains the CDO
 * ID as a string */
#define WITH_CDO_ID_STR(varname, cdoidptr, body)                          \
  do {                                                                  \
    char *varname = alloca(MSTRO_CDO_ID_STR_LEN);                       \
    if(MSTRO_OK!=mstro_cdo_id__str(cdoidptr, &varname)) {               \
      ERR("Failed to construct printable CDO ID for %p\n", cdoidptr);   \
    } else {                                                            \
      body                                                              \
          }                                                             \
  } while(0)
#define WITH_MIO_OBJ_STR(x,y,z) WITH_CDO_ID_STR(x,y,z)

/** Line feed character size */
#define BYTE_ASCII_LF 	sizeof(char)

/** Null character size */
#define BYTE_ASCII_NUL	sizeof(char)


#define MSTRO_CDO_ATTR_NAMESPACE_DEFAULT ".maestro.core.cdo."

/**@defgroup MSTRO_Internal Internal Maestro Core API
 **@{
 ** This is the Internal API, as developed for D3.2
 **/

/**@defgroup MSTRO_I_CDO Internal Maestro Core Data Objects
 **@{
 ** This is the Internal CDO API, as developed for D3.2
 **/

/** a CDO-ID is given by the octets of the binary representation of the UUID */
struct mstro_cdo_id {
  union {
    uint8_t  id[MSTRO_CDO_ID_NUMBYTES]; /**< octet view, network byte order */
    uint64_t qw[2];                     /**< 2 quadwords view on same */
  };
  uint64_t local_id;                    /**< uniquifier for multiple handles of same cdoid */
};

/** constant used to indicate that no local-id is available/known/needed */
#define MSTRO_CDO_LOCAL_ID_NONE  ((uint64_t)0)
/** constant used to indicate that the local-id is assigned by the PM for an undeclared CDO (e.g., name-only group member) */
#define MSTRO_CDO_LOCAL_ID_UNDCL ((uint64_t)1)

/** printing specification for local-id values */
#define PRIlocalid PRIx64

/** number of characters required to print local-id value using PRIlocalid */
#define MSTRO_LOCAL_ID_STR_LEN (16) // hex, no sign, no NUL char

/**
 ** @brief Produce canonical string representation for *cdoid* in *name*.
 **
 **  If *name* is NULL, allocate (caller must then free). Otherwise it
 **  must be >= UDJO_CDO_ID_STR_LEN or longer and is simply written
 **  to.
 **
 **  Remember that for a stack-allocated array
 **    char foo[MSTRO_CDO_ID_STR_LEN];
 **  it is not enough to use &foo as argument; you need to use &dest where
 **    char *dest=&foo[0];
 **
 ** @param[in]	cdoid	ID of a given CDO
 ** @param[out] name	String representation of cdoid
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_id__str(const struct mstro_cdo_id *cdoid, char **name);

/**
 ** @brief Produce an integral hash for CDOID (incl. UUID and local_id) in *hash.
 **
 ** @param[in]	cdoid	ID of a given CDO
 ** @param[out]	hash	Fresh hash of cdoid
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
static inline
mstro_status
mstro_cdo_id__hash(const struct mstro_cdo_id *cdoid, uint32_t *hash)
{
  if(cdoid==NULL)
    return MSTRO_INVARG;
  if(hash==NULL)
    return MSTRO_INVOUT;

  *hash = cdoid->qw[0] ^ cdoid->qw[1];
  uint64_t tmp = cdoid->local_id;

  *hash ^= ((tmp)>>33^(tmp)^(tmp)<<11);
  return MSTRO_OK;
}
/**
 ** @brief Implements core operations of mstro_cdo_declare.
 ** Used by both the normal and async versions of cdo declare.
 **
 ** @param[in]  name        The name, a valid UTF-8 string
 ** @param[in]  attributes  Initial attribute set (consider @ref MSTRO_ATTR_DEFAULT)
 ** @param[out] result      The CDO handle if return status is ::MSTRO_OK
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_declare_core(const char *name,
                  mstro_cdo_attributes attributes,
		  mstro_cdo *result);

/**
 ** @brief Produce an integral hash for CDOID (incl. only UUID, skipping local_id) in *hash.
 **
 ** @param[in]	cdoid	ID of a given CDO
 ** @param[out]	hash	Fresh hash of cdoid
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
static inline
mstro_status
mstro_cdo_id__hash_uuid_only(const struct mstro_cdo_id *cdoid, uint32_t *hash)
{
  if(cdoid==NULL)
    return MSTRO_INVARG;
  if(hash==NULL)
    return MSTRO_INVOUT;

  *hash = cdoid->qw[0] ^ cdoid->qw[1];
  return MSTRO_OK;
}

/**
 ** @brief Compare CDO IDs (incl local_id)
 **
 ** @param[in]	id1	ID of CDO 1
 ** @param[in]	id2	ID of CDO 2
 **
 ** @returns TRUE if IDs are equal, FALSE otherwise
 **/
static inline
bool
mstro_cdo_id__equal(const struct mstro_cdo_id *id1,
                    const struct mstro_cdo_id *id2)
{
  /** the memcmp on the union will only work if the structure is not
   * padded */
  assert(sizeof(struct mstro_cdo_id)==3*64/8);

  if(memcmp(id1, id2, 3*64/8)==0)
    return true;
  else
    return false;
}

/**
** @brief Advance the target state of mstro request to wait on the next cdo state
**
** @param[in]	mstro request
**
**
** @returns void
**/
static inline
void
advance_target_state(mstro_request request);

/**
 ** @brief Compare CDO IDs (excl local_id)
 **
 ** @param[in]	id1	ID of CDO 1
 ** @param[in]	id2	ID of CDO 2
 **
 ** @returns TRUE if IDs are equal, FALSE otherwise
 **/
static inline
bool
mstro_cdo_id__equal_uuid_only(const struct mstro_cdo_id *id1,
                              const struct mstro_cdo_id *id2)
{
  /** the memcmp on the union will only work if the structure is not
   * padded */
  assert(sizeof(struct mstro_cdo_id)==3*64/8);

  if(memcmp(id1, id2, 2*64/8)==0)
    return true;
  else
    return false;
}

/**
 ** @brief populate the (preallocated) CDO ID from name
 **
 ** Computes a v5 SHA-1 UUID for NAME and write it to the preallocated *result.
 **
 ** Initializes the local-id field to MSTRO_CDO_LOCAL_ID_NONE.
 **
 ** @param[in]	name	Name of the CDO
 ** @param[out]	result	ID associated with given cdo name
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_id_from_name(const char *name,
                       struct mstro_cdo_id *result);

/**
 ** @brief populate the (preallocated) CDO ID from Group name
 **
 ** Computes a v5 SHA-1 UUID for NAME and write it to the preallocated *result
 **
 ** Initializes the local-id field to MSTRO_CDO_LOCAL_ID_NONE.
 **
 ** This is similar, but not interchangeable with @ref
 ** mstro_cdo_id_from_name(), as it uses a different namespace to
 ** construct the ID.
 **
 ** @param[in]	name	Name of the CDO
 ** @param[out]	result	ID associated with given cdo group
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdogrp_id_from_name(const char *grpname,
                          struct mstro_cdo_id *result);

/**
 ** @brief allocate and fill a random string
 **
 ** Creating semaphore object requires an ID, with a random string it is
 ** possible to generate a name using `mstro_cdo_id_from_name` and TODO retry on
 ** collision.
 **/
mstro_status
mstro_str_random(char** s, size_t len);


/** the structure describing a CDO */
struct mstro_cdo_ {
  UT_hash_handle  hh;            /**< hashable */
  uint64_t serial;               /**< unique index (per rank), used in DECLARE op */
  uint64_t channel;              /**< dedicated communication channel assigned in DECLARE_ACK */
  _Atomic mstro_cdo_state state; /**< current state of the CDO */
  int preferred_numa_node;       /**< the preferred NUMA node for this CDO. -1
                                    for don't care. Typically set from raw-ptr
                                    or mamba-array information */
  /* direct attributes */
  struct mstro_cdo_id id;        /**< the locally unique ID */
  struct mstro_cdo_id gid;       /**< the globally unique ID */

  char *name;                    /**< user-provided name */
  _Atomic int64_t n_segments;    /**< number of outstanding transmissions 
                                    required to fill all the required data for a distributed CDO */

  /* Cached copies of the data attributes. Caching occurs at SEAL
   * time, after which the attribute setters will refuse changing the
   * dictionary entries, so caching is safe */
  void *raw_ptr;                 /**< user-provided pointer (if any) */
  mmbArray *mamba_array;         /**< the mamba array for handling content (if any) */
  bool core_owned_raw_ptr;       /**< indicator that the raw_ptr was
                                  * allocated by maestro, not passed
                                  * by the user. FIXME: This is a
                                  * workaround until we can ask mamba
                                  * to allocate suitably aligned
                                  * arrays and return CDOs with only a
                                  * mamba-array for DEMAND on a thin
                                  * CDO */

  /* all other attributes and name and id are stored in the following table */
  mstro_cdo_attributes attributes; /**< attribute and schema information */
  Mstro__Pool__Attributes *attributes_msg; /**< attributes in serialized form */

  /* FIXME: we could store the current_namespace in a global namespace
   * symtab ... millions of CDOs that all have the same string copied
   * seems overkill */
  char* current_namespace; 	   /**< namespace string; default: ".maestro.core.cdo" */
};

/* cdo.h has typedef struct mstro_cdo_ * mstro_cdo; */

/** maestro request handle flavors: */
enum mstro_request_kind {
  MSTRO_REQUEST_INVALID = 0, /**< invalid request kind */
  MSTRO_REQUEST_GENERIC,     /**< generic operation */
  MSTRO_REQUEST_CDO,         /**< async CDO operation */
  MSTRO_REQUEST__max
};
  /** the structure describing a maestro request */
struct mstro_request_ {
  enum mstro_request_kind kind; /**< kind of request, determining content and ops */
  union {
    struct {
      _Atomic(bool) completed; /**< flag indicating whether the request has completed */
      pthread_mutex_t lock;    /**< lock for waiting on the request */
      pthread_cond_t  cvar;    /**< condition variable to wait on */
      mstro_status status;     /**< status of the async request */
      void *payload;           /**< payload slot */
    } generic;     /**< for kind == MSTRO_REQUEST_GENERIC */

    struct {
      struct mstro_cdo_ *cdo;       /**< the CDO concerned */
      mstro_cdo_state target_state; /**< the state which is needs to
                                     * be reached to achieve
                                     * completion */
      mstro_cdo_state orig_state;   /**< the initial state */
      int wait_flag;                /**< flag indicating someone
                                     * waited on completion */
    } cdo;     /**< for kind == MSTRO_REQUEST_CDO */
  };
};

/** allocate new async request handle */
mstro_status
mstro_request__allocate(enum mstro_request_kind kind,
                        struct mstro_request_ **res);

/** deallocate async request handle */
mstro_status
mstro_request__free(struct mstro_request_ *res);

/**
 * @brief signal waiting entities that the request is complete
 * 
 * @param request input, maestro request
 * @param status  status of the requested operation
 * @param payload any data needed to be passed to the requester
 * @return mstro_status 
 */
mstro_status
mstro_request__generic__mark_complete(mstro_request request,
                                      mstro_status status,
                                      void *payload);

/**
 ** @brief Resolve CDO demand and copy
 **
 ** Fill *dst* from *src*. Must each be in the appropriate state, i.e., *src*
 ** @ref OFFERED and *dst* @ref REQUIRED
 **
 ** @param[in]		src	An OFFERED CDO handle
 ** @param[inout]	dst	REQUIRED CDO on input, contains copy of CDO src on output
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo__satisfy(mstro_cdo src, mstro_cdo dst);

/** check whether CDO has state s. */
bool
mstro_cdo_state_check(mstro_cdo cdo, mstro_cdo_state s);

/** Increment ASYNC operation BLOCKed counter with the request->wait_flag*/
void
mstro_increment_async_blocked_counter(mstro_request request);

/** set state. Notify all waiters on state change queue. */
mstro_status
mstro_cdo_state_set(mstro_cdo cdo, mstro_cdo_state s);
mstro_status
mstro_cdo_state_set_safe_flags(mstro_cdo cdo, mstro_cdo_state s);

/** get state. May be stale by the time you read it */
mstro_cdo_state
mstro_cdo_state_get(mstro_cdo cdo);

/** Wait for CDO state change. Spurious returns may happen,
 * caller must check state explicitly.  */
mstro_cdo_state wait_for_cdo_state_change(mstro_cdo cdo, mstro_cdo_state s);

/* check if the async cdo operation request is valid on wait and test request*/
bool mstro_request_is_valid(mstro_request request);


/* Block caller until CDO state includes the bit in *s*
 * If state already contains the necessary bits, proceeds very quickly without taking any lock.
 * */
void
mstro_cdo_block_until(mstro_cdo cdo, mstro_cdo_state s, const char* debug);


/** Complete a CDO declaration with data received from pool manager */
mstro_status
mstro_cdo_declare_completion(uint64_t serial, const struct mstro_cdo_id *global_id, uint64_t channel);

/** Update CDO attributes from the ones received in an incoming transport
 *
 * This will overwrite existing values and fill in those that are new,
 * except for raw-ptr and mamba-array. Local sizes must match if set on both ends.
 *
 * The @arg num_precious_attr attributenames given in @arg
 * precious_attributes will not be touched.
 */
mstro_status
mstro_cdo_attributes_update_incoming(mstro_cdo cdo, const Mstro__Pool__Attributes *attr,
                                     size_t num_precious_attr, const char **precious_attributes);


/** Tell the CDO layer that data has been transferred into the CDO */
mstro_status
mstro_cdo__mark_transfer_complete(mstro_cdo cdo);

/** Find CDO handle by CDO-ID. Returns MSTRO_FAIL if not found */
mstro_status
mstro_cdo__find_cdo(const struct mstro_cdo_id *id,
                    mstro_cdo *result);

/** Synchronize raw-ptr and mamba_array and layout attributes of @arg cdo.  */
mstro_status
mstro_cdo_allocate_data(mstro_cdo cdo);

/** Adjust @arg cdo to have space for @arg size bytes if possible,
 * given an allocation @policy. Changes the local-size attribute
 * appropriately */
mstro_status
mstro_cdo__adjust_space(mstro_cdo cdo, int64_t size, int policy);

/** obtain the CDO attributes message from a (local) CDO by CDO-ID
 */
mstro_status
mstro_cdo_attribute_msg_get(const struct mstro_cdo_id *id, Mstro__Pool__Attributes **attributes);

/** @brief Create a CDO handle in DEMANDed state for a CDO known by ID that is already DEMANDABLE in the pool.
 **
 ** (this is a helper mostly for group functionality and bypasses some sanity checks)
 **/
mstro_status
mstro_cdo__demand_by_id(const struct mstro_cdo_id *id, mstro_cdo *result);

/**@} (end of group MSTRO_I_CDO) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_CDO_H_ */
