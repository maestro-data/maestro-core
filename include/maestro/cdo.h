/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO public header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_CDO_H_
#define MAESTRO_CDO_H_ 1

#include "maestro/status.h"
#include "maestro/attributes.h"
#include <stdint.h>

/* we don't distribute the mamba header right now, so */
/** Abstract MAMBA array type */
typedef struct s_mmbArray mmbArray;

/**@addtogroup MSTRO_Core
 **@{
 **/

/**@defgroup MSTRO_CDO Maestro Core Data Objects
 **@{
 ** This is the CDO API, as developed for D3.2
 **
 ** Unless specified differently, all of these function are not to be
 ** called concurrently with the same @ref mstro_cdo handle from
 ** multiple threads.
 **
 **/

/** opaque CDO handle */
typedef struct mstro_cdo_ *mstro_cdo;

/** opaque maestro request handle */
typedef struct mstro_request_ *mstro_request;

/**
 **@brief Obtain the CDO name
 **
 ** The string is constant as long as the *cdo* is valid
 **
 ** @returns The name specified at CDO creation time, NULL if
 ** *cdo* is invalid.
 **/
const char *
mstro_cdo_name(mstro_cdo cdo);

/** The length of a CDO name is currently limited to be less than
 * MSTRO_CDO_NAME_MAX */
#define MSTRO_CDO_NAME_MAX 900

/**
 ** @brief Create new CDO for *name* with attribute *attributes*
 **
 ** Sets default prefix of maestro.core.cdo. Attribute
 ** names specified mstro_cdo_attribute_set() and mstro_cdo_attribute_set_yaml()
 ** are relative to that, unless a different prefix is set using
 ** mstro_attr_namespace_set().
 **
 ** @param[in]  name        The name, a valid UTF-8 string
 ** @param[in]  attributes  Initial attribute set (consider @ref MSTRO_ATTR_DEFAULT)
 ** @param[out] result      The CDO handle if return status is ::MSTRO_OK
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_declare(const char *name,
                  mstro_cdo_attributes attributes,
		  mstro_cdo *result);


/**
** @brief Create new CDO for *name* with attribute *attributes*
**
** Asynchronous version of @ref mstro_cdo_declare
** Sets default prefix of maestro.core.cdo. Attribute
** names specified mstro_cdo_attribute_set() and mstro_cdo_attribute_set_yaml()
** are relative to that, unless a different prefix is set using
** mstro_attr_namespace_set().
**
** @param[in]  name        The name, a valid UTF-8 string
** @param[in]  attributes  Initial attribute set (consider @ref MSTRO_ATTR_DEFAULT)
** @param[out] result      The CDO handle if return status is ::MSTRO_OK
** @param[out] request     The CDO operation handle
**
** @returns A status code, ::MSTRO_OK on success.
**/
mstro_status
mstro_cdo_declare_async(const char *name,
                        mstro_cdo_attributes attributes,
                        mstro_cdo *result,
                        mstro_request *request);

/** @brief Access the raw-pointer buffer of a CDO
 **
 ** May not succeed if the CDO allocation is non-contiguous. Try
 ** mstro_cdo_access_mamba_array() in that case.
 **
 ** Note that you cannot obtain both the raw-ptr *data* and the mamba-array
 ** view of the same CDO.
 **
 ** @param[in]	cdo		The CDO handle
 ** @param[out]	data		Pointer to the raw data
 ** @param[out] data_len	Size of the data in bytes
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_access_ptr(mstro_cdo cdo, void **data, int64_t *data_len);

/** @brief Access the mamba-array view of the buffer of a CDO
 **
 ** Every CDO of type 1 has a linear 1D array mamba array view when accessible.
 **
 ** A CDO of type 2 will expose a mamba-array of the layout specified
 ** in the CDO's attribute declaration.

 ** Note that you cannot obtain both the raw-ptr and the mamba-array *array*
 ** view of the same CDO.
 **
 ** @param[in] 	cdo	The CDO handle
 ** @param[out]	array	Mamba array view of the CDO buffer
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_access_mamba_array(mstro_cdo cdo, mmbArray **array);


/**
 ** @brief Mark CDO declaration as complete.
 **
 ** Makes the declaration of *cdo* immutable.
 **
 ** @param[in] cdo 	A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_seal(mstro_cdo cdo);

/**
 ** @brief Mark CDO declaration as complete - asynchronous version.
 **
 ** Makes the declaration of *cdo* immutable.
 **
 ** Asynchronous version of @ref mstro_cdo_seal. It sends the sealing message to the pool manager and returns immediately.
 ** User can test or wait on the completion of the operation with @ref mstro_request_test and @ref mstro_request_wait respectively.
 ** mstro_request handle should be passed to the test and wait functions.
 **
 ** @param[in] cdo 	A CDO handle.
 ** @param[out] request 	A request handle to check the completion of the operation.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_seal_async(mstro_cdo cdo, mstro_request *request);

/**
 ** @brief Wait for a cdo operation to complete.
 **
 ** Blocks the calling thread until the cdo operation passed by the request is complete.
 **
 ** Required to ensure the correctness of the asynchronous cdo operations such as @ref mstro_cdo_declare_async and @ref mstro_cdo_seal_async and sisters.
 ** @param[in] request 	Maestro request handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status mstro_request_wait(mstro_request request);

/**
 ** @brief Test if a cdo operation has completed.
 **
 ** Tests if the cdo operation pointed by the request has completed or not, without blocking the calling thread.
 **
 ** User can test for the completion of the asynchronous cdo operations such as @ref mstro_cdo_declare_async and @ref mstro_cdo_seal_async and sisters.
 **
 ** @param[in] request 	Maestro request handle.
 ** @returns A boolean value ::True if the operation is completed.
 **/
bool mstro_request_test(mstro_request request);

/**
 ** @brief Frees a maestro cdo operation request.
 **
 ** The freed request can not be used later to check on  asynchronous cdo operations such as @ref mstro_cdo_declare_async and @ref mstro_cdo_seal_async and sisters.
 **
 ** @param[in] request 	Maestro request handle.
 ** @returns status.
 **/
mstro_status mstro_request_free(mstro_request request);


/**
 ** @deprecated old name of @ref mstro_cdo_seal
 **/
mstro_status
mstro_cdo_declaration_seal(mstro_cdo cdo);


/**
 ** @brief Release CDO declaration.
 **
 ** The *cdo* must have been previously declared using @ref
 ** mstro_cdo_declare().
 **
 ** All maestro-internal resources associated with *cdo* are
 ** released; user process owned resources attached to the CDO are
 ** returned entirely under control of the user process.
 **
 ** This operation is non-blocking.
 **
 **
 ** @param[in] cdo 	A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_dispose(mstro_cdo cdo);

/**
 ** @brief Reuse disposed CDO.
 **
 ** Disposes the *cdo* and declares it again, keeping the same handle, for
 ** speed and convenience. User attributes are also preserved.
 **
 ** @param[in] cdo 	A CDO handle.
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_dispose_and_reuse(mstro_cdo cdo);



/**@} (end of group MSTRO_CDO) */
/**@} (end of addtogroup MSTRO_Core) */

#endif
