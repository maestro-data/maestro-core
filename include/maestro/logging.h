/* -*- mode:c -*- */
/** @file
 ** @brief Logging infrastructure
 **
 ** This header exposes an API to do sensible logging, from debugging
 ** to production level information
 **
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MSTRO_LOGGING_H_
#define MSTRO_LOGGING_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

#include "maestro.h"

#include <stdio.h>
#include <math.h>
#include <stdarg.h>

/** @defgroup Logging Logging
 * @{
 *
 * @brief Simple logging API, from debugging to production level information
 *
 */
/** log level for errors */
#define MSTRO_LOG_ERR   0
/** log level for warnings */
#define MSTRO_LOG_WARN  1
/** log level for informational messages */
#define MSTRO_LOG_INFO  2
/** log level for debugging messages */
#define MSTRO_LOG_DEBUG 3
/** log level for really chatty logging */
#define MSTRO_LOG_NOISE 4

/** number of log levels */
#define MSTRO_log__MAX  5

  
#ifndef MSTRO_MAX_LOG_LEVEL
/** compile-time log level cutoff */
#define MSTRO_MAX_LOG_LEVEL MSTRO_LOG_DEBUG
#endif

/** Every module that can log needs to be defined by one of these structures: */
struct mstro_log_module_descriptor {
  uint64_t selector; /* a single-bit component selector */
  char *name;
  char *description;
};

/* single-bit component selector constants */
#define MSTRO_LOG_MODULE_CORE   1<<0
#define MSTRO_LOG_MODULE_CDO    1<<1
#define MSTRO_LOG_MODULE_EVENT  1<<2
#define MSTRO_LOG_MODULE_ATTR   1<<3
#define MSTRO_LOG_MODULE_PC     1<<4
#define MSTRO_LOG_MODULE_PM     1<<5
#define MSTRO_LOG_MODULE_COMM   1<<6
#define MSTRO_LOG_MODULE_MAMBA  1<<7
#define MSTRO_LOG_MODULE_TRANSF 1<<8
#define MSTRO_LOG_MODULE_TRANSP 1<<9
#define MSTRO_LOG_MODULE_STATS  1<<10
#define MSTRO_LOG_MODULE_USER   1<<11
#define MSTRO_LOG_MODULE_TELEMETRY 1<<12
#define MSTRO_LOG_MODULE_ERL    1<<13
#define MSTRO_LOG_MODULE_MSG    1<<14

#define MSTRO_LOG_MODULE_ALL    ((uint64_t)0xffffffffffffffff)
  
/** An list (suitable as initializer-list) of @ref
 * mstro_log_component_descriptor structures to classify logging
 * sources */
#define MSTRO_LOG_MODULES {                                             \
    {MSTRO_LOG_MODULE_CORE,   "core", "core infrastructure"},           \
    {MSTRO_LOG_MODULE_CDO,    "cdo", "CDO handling"},                   \
    {MSTRO_LOG_MODULE_EVENT,  "event", "Event generation and handling" }, \
    {MSTRO_LOG_MODULE_ATTR,   "attr", "Attribute and Schema handling" }, \
    {MSTRO_LOG_MODULE_PC,     "pc", "Pool client"    },                 \
    {MSTRO_LOG_MODULE_PM,     "pm", "Pool manager and local pool"},     \
    {MSTRO_LOG_MODULE_COMM,   "comm", "Communication level"},           \
    {MSTRO_LOG_MODULE_MAMBA,  "mamba", "Mamba memory management"},      \
    {MSTRO_LOG_MODULE_ERL,    "erl", "libERL components"},              \
    {MSTRO_LOG_MODULE_TRANSF, "transf", "CDO Transformations"},         \
    {MSTRO_LOG_MODULE_TRANSP, "transp", "CDO Transport"},               \
    {MSTRO_LOG_MODULE_STATS,  "stats", "Statistics and telemetry"},     \
    {MSTRO_LOG_MODULE_USER,   "user", "User code using maestro logging"}, \
    {MSTRO_LOG_MODULE_TELEMETRY,"telemetry", "Telemetry data"},           \
    {MSTRO_LOG_MODULE_ERL,    "erl", "libERL layer"},           \
    {MSTRO_LOG_MODULE_MSG,    "msg", "Message handling layer"},           \
  }


/** @brief core logging worker function.
 */
void
mstro_location_aware_log(int level,
                         uint64_t module,
                         const char *func, const char* file, int line,
                         const char *fmtstring, ...);

/** @brief logging macro around location_aware_log()
 */
#define LOG(module, level, ...) do {                                 \
    if(level<=MSTRO_MAX_LOG_LEVEL)                                      \
      mstro_location_aware_log(level, module, __func__, __FILE__, __LINE__, \
                               __VA_ARGS__);                            \
  } while(0)


/** @brief core logging worker function - varargs version */
  void
mstro_vlocation_aware_log(int level,
                          uint64_t module,
		          const char *func, const char* file, int line,
                          const char *fmtstring, va_list ap);

/** @brief mamba-compatible logging interface
 *
 * This permits us to ensure mamba will log through the maestro infrastructure
 */
void mstro_mamba_logging_bridge(int level, const char *func, const char *file,
		                int line, const char *fmtstring, ...);

/** @brief libERL-compatible logging interface
 *
 * This permits us to ensure libERL will log through the maestro infrastructure
 */
void mstro_liberl_logging_bridge(int level, const char *func, const char *file,
		                 int line, const char *fmtstring, ...);

/** @defgroup LoggingMacros Logging Macros
 * @{
 *
 * @brief Intended API
 */

/** error messages */
#define LOG_ERR(module, ...)    LOG(module,MSTRO_LOG_ERR,__VA_ARGS__)
/** warning messages */
#define LOG_WARN(module, ...)   LOG(module,MSTRO_LOG_WARN,__VA_ARGS__)
/** informational messages */
#define LOG_INFO(module, ...)   LOG(module,MSTRO_LOG_INFO,__VA_ARGS__)
/** debug messages */
#define LOG_DEBUG(module, ...)  LOG(module,MSTRO_LOG_DEBUG,__VA_ARGS__)
/** chatty messages */
#define LOG_NOISE(module, ...)  LOG(module,MSTRO_LOG_NOISE,__VA_ARGS__)

/** @} (group macros) */

  /**@defgroup LoggingConvenience Convenience functions for custom logging
   **@{
   ** @brief Safe introspection functions for building custom logging output
   **/
  const char *
  mstro_workflow_id(void);
  const char *
  mstro_component_name(void);
  mstro_app_id
  mstro_appid(void);
  const char *
  mstro_hostname(void);
  pid_t
  mstro_pid(void);
  const char *
  mstro_threadid(void);
  

   /**@} (group LoggingConvenience) */
/** @} (group Logging) */
     

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif /* MSTRO_LOGGING_H_ */
