/* -*- mode:c -*- */
/** @file
 ** @brief Maestro memory locking abstraction
 **/
/*
 * Copyright (C) 2020 HPE, HP Schweiz GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_MEMLOCK_H_
#define MAESTRO_I_MEMLOCK_H_ 1

#include "maestro.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <assert.h>

/**@ingroup MSTRO_Internal
 **/

/**@defgroup MSTRO_I_MEMLOCK Recursive, overlapping memory locking support
 **@{
 **/

/* maestro-side allocation policies */
#define MSTRO_ALLOC_POLICY_NONE  	0
#define MSTRO_ALLOC_POLICY_LOCKED   (1U<<0)
#define MSTRO_ALLOC_POLICY_ALIGNED  (1U<<1)

/**@brief Lock @arg LEN bytes of memory starting at @arg ADDR
 *
 * In contrast to mlock() this functions handles locking of multiple
 * allocations that are on the same page correctly.
 *
 * This function can fail for resource limit reasons, but such
 * failures need not be catastrophic for the application overall. If
 * the caller can deal with it, they can try again later, or for
 * smaller @arg len allocations.
 */
mstro_status
mstro_memlock(void* addr, size_t len);

/**@brief Unlock @arg LEN bytes of memory starting at @arg ADDR
 *
 * In contrast to mlock() this functions handles locking of multiple
 * allocations that are on the same page correctly. It will unlock
 * only the pages which are not holding other allocations locked using
 * @ref mstro_memlock()
 */
mstro_status
mstro_memunlock(void* addr, size_t len);


/**@brief Initialize the memlock subsystem
 *
 * Check resource limits whether at least min_required bytes can be locked (RLIMIT_MEMLOCK), and
 * if not, return MSTRO_NOMEM.
 */
mstro_status
mstro_memlock_init(size_t min_required);

/**@brief De-Initialize the memlock subsystem
 *
 */
mstro_status
mstro_memlock_finalize(void);

/**@} (end of group MSTRO_I_MEMLOCK) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_CDO_H_ */
