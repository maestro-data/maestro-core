/* -*- mode:c -*- */
/** @file
 ** @brief Maestro status codes
 **/
/*
 * Copyright (C) 2018 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_STATUS_H_
#define MAESTRO_STATUS_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

  /**@defgroup MSTRO_Status Maestro Status codes
   **@{
   **/

  /**
   ** @brief Maestro API status codes
   **
   ** Almost all functions return one of these status codes. To
   ** translate a status code to a textual representation call @ref
   ** mstro_status_description().
   **
   **/
  typedef enum {
    MSTRO_OK = 0,     /**< successful completion of an operation */
    MSTRO_FAIL = 1,   /**< (generic) failure of an operation */
    MSTRO_UNINIT,     /**< maestro-core not initialized */
    MSTRO_UNIMPL,     /**< function is yet unimplemented */
    MSTRO_INVARG,     /**< invalid argument */
    MSTRO_INVOUT,     /**< invalid output argument location */
    MSTRO_NOMEM,      /**< insufficient memory to satisfy request */
    MSTRO_INVMSG,     /**< invalid pool protocol message */
    MSTRO_OFI_FAIL,   /**< fabric error */
    MSTRO_TIMEOUT,    /**< timeout occured */
    MSTRO_NOT_TWICE,  /**< can only be called once from a process */
    MSTRO_WOULDBLOCK, /**< operation would block the current thread,
                       * and should not. Operation has been without
                       * 'dangerous' side effects (except maybe
                       * starting progress towards not having it block
                       * on a later attempt: Can be retried. */

    /**@private */
    MSTRO_NO_PM,      /**< an operation was not performed because
                       * there is no pool manager. This may or may not
                       * be an error for the caller. No resources will
                       * be leaked and no other errors masked by this
                       * status. */
    MSTRO_NOMATCH,    /**< A regular expression or string comparison
                       * did not match */
    MSTRO_NOENT,      /**< A lookup did not find a suitable entry */

    mstro_status_MAX /* number of enum values */
  } mstro_status;


  /**
   ** @brief Obtain a printable status description
   **
   ** @returns an unmodifiable string
   **/
  const char *
  mstro_status_description(mstro_status s);

  /** @} (end of Groyp MSTRO_Status) */
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MAESTRO_STATUS_H_ */
