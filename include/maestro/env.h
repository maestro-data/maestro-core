/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Environment Variables
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_ENV_H_
#define MAESTRO_ENV_H_ 1

#include "maestro/status.h"

/**@defgroup MSTRO_ENV Maestro Environment Variables
 **@{
 ** The following environment variables influence the behavior of Maestro
 **/

/**@brief Log level. (Optional)
 *
 * Maestro logging verbosity can be changed at application startup by
 * setting the environment variable @ref MSTRO_ENV_LOG_LEVEL to a
 * numeric value from the following list:
 *
 * - 0 (@ref MSTRO_LOG_ERR)   -- Only errors displayed
 * - 1 (@ref MSTRO_LOG_WARN)  -- Warnings and errors displayed
 * - 2 (@ref MSTRO_LOG_INFO)  -- Informational, warning, and errors
 * - 3 (@ref MSTRO_LOG_DEBUG) -- Debug messages and all of the above
 * - 4 (@ref MSTRO_LOG_NOISE) -- Extra verbosity
 *
 * The actual amount of logging supported depends on the compile-time
 * setting of * @ref MSTRO_MAX_LOG_LEVEL, so if the library was built
 * with less debugging support it may not actually show extra
 * information at high debug levels.
 *
 * The logging output destination can be selected by @ref MSTRO_ENV_LOG_DST.
 */
#define MSTRO_ENV_LOG_LEVEL "MSTRO_LOG_LEVEL"

/**@brief Selection of modules that should log (Optional)
 *
 * Maestro log messages are annotated with a module name; by default
 * all modules are permitted to log. This variable can be set to
 * selectively disable or enable certain modules.
 *
 * The syntax is
 *   component1,component2,^component3,...
 *
 * where 'component1' indicates that it should be included, and
 * '^component3' indicates that component3 should be excluded.
 *
 * The pseudo-component 'all' may be used to indicate all
 * components. This can be used to set a 'background', and either
 * selectively include or exclude components.
 *
 * If unset, the default is "all".
 */
#define MSTRO_ENV_LOG_MODULES "MSTRO_LOG_MODULES"

/**@brief Log color. (Optional)
 *
 * Maestro logging color (ANSI) can be changed at application startup by
 * setting the environment variable @ref MSTRO_ENV_LOG_COLOR to a
 * string value from the following list:
 * red, yellow, green, blue, magenta, cyan, white
 * as well as (on most color terminals)
 * brightgreen, brightblue, brightmagenta, brightcyan, brightwhite
 *
 * Bright red and bright yellow are reserved for error and warnings messages. See @ref MSTRO_ENV_COLOR_ERRORS.
 */
#define MSTRO_ENV_LOG_COLOR "MSTRO_LOG_COLOR"

/**@brief lists the set of Schemata (in order from left to right) that should be loaded after the maestro-core schema is loaded.
 * If set,mstro_init will merge these onto the built-ins. Default: core + ecmwf
 * example "export MSTRO_SCHEMA_LIST=ecmwf.yaml;benchmark.yaml"
*/
#define MSTRO_ENV_SCHEMA_LIST "MSTRO_SCHEMA_LIST"


/**@brief path to search for schemata (in order from left to right).
 *  Default: current directory "."
 * example export MSTRO_SCHEMA_PATH=".:/usr/share/maestro/schemata"
*/
#define MSTRO_ENV_SCHEMA_PATH "MSTRO_SCHEMA_PATH"


/**@brief enable coloring of error log messages
 *
 * If set, enable coloring of error and warning messages. May not be a good idea if logging to syslog (see @ref MSTRO_LOG_DST).
 */
#define MSTRO_ENV_COLOR_ERRORS "MSTRO_LOG_COLOR_ERRORS"

/**@brief Select logging output channel. (Optional)
 *
 * By default log output goes to standard error, but an alternative
 * value can be set here by specifying one of the strings below:
 *
 * - @b stdout -- print to standard output
 * - @b stderr -- log to standard output (default)
 * - @b syslog -- log using syslog
 *
 * If using @b syslog the syslog level argument is chosen according to
 * the maestro log level, and the logging facility is set by the
 * (compile-time) value @ref MSTRO_LOG_SYSLOG_FACILITY.
 */
#define MSTRO_ENV_LOG_DST "MSTRO_LOG_DST"

/**
 ** @brief The workflow ID. (Required)
 **
 ** All applications that are to be part of the same workflow need to
 ** share the same workflow identifier. This is an alphanumeric string
 ** matching regular expression [A-Za-z0-9._-]+ (as defined in @ref
 ** MSTRO_WORKFLOW_ID_REGEX)
 **
 ** There is a limit to the length of the workflow name (which can be
 ** increased at compile time), @ref MSTRO_WORKFLOW_NAME_MAX .
 **/
#define MSTRO_ENV_WORKFLOW_NAME "MSTRO_WORKFLOW_NAME"

/**
 ** @brief The maximum length of a workflow name (@ref MSTRO_ENV_WORKFLOW_NAME).
 **
 ** If this limit is changed, it must be changed consistently across
 ** all applications communicating through maestro, as it becomes part
 ** of the network-side communication ABI.
 **/
#define MSTRO_WORKFLOW_NAME_MAX (1U<<12)

/** The permitted string contents in @ref MSTRO_ENV_WORKFLOW_ID */
#define MSTRO_WORKFLOW_ID_REGEX "^[A-Za-z0-9._-]\\{1,64\\}$"



/**
 ** @brief An application token. (Required for distributed applications)
 *
 * All parts of a distributed application need to share an application ID.
 *
 * This is an alphanumeric string with the same constraints as @ref
 * MSTRO_ENV_WORKFLOW_ID, i.e. @ref MSTRO_WORKFLOW_ID_REGEX and @ref
 * MSTRO_WORKFLOW_NAME_MAX.
 */
#define MSTRO_ENV_COMPONENT_NAME "MSTRO_COMPONENT_NAME"


/**
 ** @brief Pool manager data. (Optional)
 **
 ** If joining a workflow with existing pool manager, this variable
 ** must contain the contact information for the pool as returned by
 ** @ref mstro_pm_getinfo().
 **
 ** See also @ref MSTRO_ENV_START_POOL_MANAGER
 **/
#define MSTRO_ENV_POOL_INFO "MSTRO_POOL_MANAGER_INFO"

/**
 ** @brief Whether to start a pool manager instance at @ref mstro_init() time.
 **
 ** Normally applications do not start a pool manager unless @ref
 ** mstro_om_start() is invoked. Use this variable to automatically
 ** start a pool manager at @ref mstro_init() time.
 **
 ** If @ref MSTRO_ENV_POOL_INFO is also set the new pool manager will
 ** negotiate with the existing one to either hand over pool
 ** functionality or join a distributed pool.
 **
 ** FIXME: Handover and distributed pool functionality is currently
 ** unimplemented.
 **
 ** Values supported: "1" or "yes", "0" or "no"
 **/
#define MSTRO_ENV_START_POOL_MANAGER "MSTRO_START_POOL_MANAGER"

/**
 ** @brief Enable hearbeat timer
 **
 ** Enable a heartbeat timer that generates telemetry events at
 ** regular intervals.
 **
 ** The time specified is in seconds, but (decimal) fractional values
 ** are supported, subject to the timer resolution of the underlying
 ** OS (and the posix timers API).
 **
 ** A (mostly idle) thread is used for this purpose, so jitter may be
 ** introduced by this, which is why it is disabled by default.
 **/
#define MSTRO_ENV_TELEMETRY_HEARTBEAT "MSTRO_TELEMETRY_HEARTBEAT"

/**
 ** @brief Which transport method to choose by default
 **/
#define MSTRO_ENV_TRANSPORT_DEFAULT "MSTRO_TRANSPORT_DEFAULT"

/**
 ** @brief Maximum CDO size to be transported inline (via the pool protocol message)
 **
 ** Inline transport is an optimization to use spare space in the pool
 ** protocol messages to send small CDOs directly in the Transport
 ** Ticket, and thus bypass an actual separate transport step.
 **
 ** default value: depends on the value of @ref MSTRO_ENV_POOL_MSG_SMALL_MAX
 ** so that no message fragmentation occurs due to inline transport
 **
 ** Set to 0 to disable inline transport.
 **
 **/
#define MSTRO_ENV_TRANSPORT_INLINE_MAX "MSTRO_TRANSPORT_INLINE_MAX"

/**
 ** @brief Directory for GFS transport
 **
 ** Must be visible on all workflow components that want to use GFS
 ** transport. Should be a high-performance global file system location.
 **
 ** Default is "./", the directory where the application is started
 ** (which will often work, but not always).
 **/
#define MSTRO_ENV_TRANSPORT_GFS_DIR "MSTRO_TRANSPORT_GFS_DIR"

/**
 ** @brief Path to the MIO transport config file 
 **
 ** This is needed to initialize MIO
 **
 **/
#define MSTRO_ENV_MIO_CONFIG "MSTRO_MIO_CONFIG"

/**
 ** @brief Flag to enable higher network security on Cray GNI interfaces
 **
 ** By default, Cray GNI (Aries) networks allow only jobs of the same
 ** job allocation to use the HSN between each other. Despite using
 ** user-id based DRC credentials (which allows cross-talk for jobs on
 ** different nodes if the user's UID matches), jobs running on the
 ** same node of an allocation can not talk to each other unless we
 ** use DRC_FLAGS_FLEX_CREDENTIAL.
 **
 ** By default we do set DRC_FLAGS_FLEX_CREDENTIAL, as that allows
 ** users to schedule jobs of the same workflow on the same or
 ** different nodes without worrying about this. If you are sure you
 ** will only run one job per compute node, consider enabling @ref
 ** MSTRO_ENV_DRC_NON_FLEX to disable flex-credential usage.
 **
 **/
#define MSTRO_ENV_DRC_NON_FLEX "MSTRO_DRC_NON_FLEX"

/**@brief specify an FI_PROVIDER for libfabric,
 * avoid build endpoints on all available endpoints, 
 * which might impact the performance"
*/
#define MSTRO_ENV_OFI_PROVIDER "MSTRO_OFI_PROVIDER"

/**
 * @brief Number of times ofi threads will retry fi_send/fi_read,
 * when these operations return FI_EAGAIN (default 3)
 */
#define MSTRO_ENV_OFI_NUM_RETRY "MSTRO_OFI_NUM_RETRY"

/**
 ** @brief Number of concurrent receive operations per endpoint
 **
 ** The default is a provider-specific value, roughly based on the
 ** endpoint's rx_attr->size field
 **/
#define MSTRO_ENV_OFI_NUM_RECV "MSTRO_OFI_NUM_RECV"

/**
 ** @brief Number of threads servicing OFI completion events
 **
 ** The default is 1.
 **/
#define MSTRO_ENV_PM_PC_NUM_THREADS "MSTRO_PM_PC_NUM_THREADS"

/**
 ** @brief Number of threads servicing PM or PC operations
 **
 ** The default is 1.
 **/
#define MSTRO_ENV_OPERATIONS_NUM_THREADS "MSTRO_OPERATIONS_NUM_THREADS"

/**
 * @berief Pin PM/PC thread to a certain core
 *
 * By default threads will not be pinned to a certain core
 * User can specify a range of core, e.g. 4-10 to pin PM/PC threads to it. 
 * */
#define MSTRO_ENV_BIND_PM_PC "MSTRO_BIND_PM_PC"
/**
 * @berief Pin transport thread on the pool manager to a certain core
 *
 * By default threads will not be pinned to a certain core
 * */
#define MSTRO_ENV_BIND_TRANSPORT_THREAD "MSTRO_BIND_TRANSPORT_THREAD"

/**
 * @berief Pin operation threads to a certain core(s)
 *
 * By default threads will not be pinned to a certain core
 * */
#define MSTRO_ENV_BIND_OP_THREAD "MSTRO_BIND_OP_THREAD"

/**@} (end of group MSTRO_ENV) */

#endif
