/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool Manager Interface 
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_POOL_MANAGER_H_
#define MAESTRO_POOL_MANAGER_H_ 1

#include "maestro/status.h"
#include <stdint.h>

/**@defgroup MSTRO_POOL_MANAGER Maestro Pool Manager
 **
 **@{
 **
 ** The Pool Manager is the central bookkeeping and management
 ** component of the Maestro framework. At least one component of a
 ** Maestro run needs to run the pool manager service. 
 **
 ** The Pool Manager is potentially able to do self-organizing
 ** distributed operation. Implementation of this feature will will
 ** happen later. */

/**@brief Make the current application a member of the pool manager.
 *
 * Must be called after @ref mstro_init().
 *
 * This function dedicates one or more threads in the calling
 * application to pool manager work.

 * Successful return indicates that the pool manager has started. To
 * shut it down, @ref mstro_pm_terminate() must be called.
 *
 * @param pool manager_size The number of partners in the pool manager
 *
 * @return { a status value, @ref ::MSTRO_OK on success }
 */
mstro_status
mstro_pm_start(void);

/**@brief ask pool manager to terminate
 *
 * Will return when the pool manager has been terminated, which may
 * only happen if no part of the workflow still needs it.
 */
mstro_status
mstro_pm_terminate(void);

/**@brief Obtain textual pool manager descriptor.
 **
 ** This is the string that can be passed to other applications in the
 ** @ref MSTRO_ENV_POOL_INFO configuration so that they can connect to
 ** the pool manager.
 **
 ** The result is a string that needs to be freed by the user.
 **/
mstro_status
mstro_pm_getinfo(char **result_p);


/**@brief Attach a client app to a running pool manager */
mstro_status
mstro_pm_attach(const char *remote_pm_info);

/**@brief Deatch a client app from a running pool manager */
mstro_status
mstro_pm_detach(void);


/**@} (end of group MSTRO_POOL_MANAGER) */


#endif /* MAESTRO_POOL_MANAGER_H_ */
