/* -*- mode:c -*- */
/** @file
 ** @brief State keeping of the pool manager
 **
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MSTRO_POOL_MANAGER_REGISTRY_H_
#define MSTRO_POOL_MANAGER_REGISTRY_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_PM_Registry Pool Manager Registry
 **@{
 ** This is the Internal Pool manager registry
 **/


#include "maestro.h"
#include "maestro/core.h"
#include "maestro/i_cdo.h"
#include "maestro/i_pool_manager.h"
#include "protocols/mstro_pool.pb-c.h"
#include "maestro/i_pool_operations.h"

/** Each application (pool participant) is assigned an ID of this type
 * at JOIN time. IDs can be re-used during the lifetime of a
 * workflow. */
typedef uint64_t mstro_app_id;

/** an invalid app id */
#define MSTRO_APP_ID_INVALID ((mstro_app_id)0U)

/** the app ID indicating the manager */
#define MSTRO_APP_ID_MANAGER ((mstro_app_id)1U)


/* FIXME: can go once the 'addr' is out of the mstro_pm_app_registry_entry */
#include <rdma/fabric.h>

#include "maestro/i_event.h"

/** A table entry for a table to look up a message origin and map it to endpoint/address
 * for replies.
 */
/* FIXME: provide accessors and hide in implementation */
/* FIXME: free pointers on deregister? */
struct mstro_pm_app_registry_entry {
  UT_hash_handle hh;         /**< hashable by KEY */
  mstro_app_id appid;        /**< sender ID */
  struct mstro_endpoint *ep; /**< local endpoint to reach it */
  /* FIMXE: this should be OFI-independent */
  fi_addr_t addr;            /**< address to use on endpoint to talk to it */
  char* serialized_desc;	/**< needed to ship with InitiateTransfer,
                                   so apps can talk to each other without PM */
  /** transport methods advertised at JOIN time */
  Mstro__Pool__TransportMethods *transport_methods;
  char * component_name; /**< the component name provided at JOIN time */
  uint64_t component_index; /**< the component index provided at JOIN time */
  bool dead;                 /**< If an app LEAVES the app-registry entry stays around in 'dead' state. That allows us to resurrect it on re-JOIN */
  bool pending; /**< indicates that we have already submitted an fi_read to read the entry. Avoids issuing multiple reads for the same config block */
};

/** initialize PM registry infrastructure */
mstro_status
mstro_pm_reg_init(void);

/** finalize PM registry infrastructure */
mstro_status
mstro_pm_reg_finalize(void);


/** mstro_pm_candidates is a structure of arrays to hold which applications are we going to
 * contact to transfer all the pieces needed for a distributed cdo.
 *
 * number of elements of arrays inside mstro_pm_candidates is in n_sources;
**/
typedef struct mstro_pm_candidates {
  size_t n_sources; /* number of apps */
  struct per_app_cdo_entries ** app;
  uint64_t * local_id;
  bool is_distributed;
} mstro_pm_candidates;


/** @brief Register a new app.
 **
 ** Allocates a new registration entry and returns the key to identify
 ** it in @ref *id_p, the entry itself in @ref *entry_p.
 **
 ** The transport_methods reference will be kept.
 **
 ** One of the two return arguments may be NULL to indicate the caller
 ** is not interested in it, but not both.
 **
 ** The serialized_desc pointer will be consumed and owned by the
 ** registration.
 **
 **/

mstro_status
mstro_pm_app_register(const struct mstro_endpoint *ep,
                      fi_addr_t addr,
                      char* serialized_desc,
                      const Mstro__Pool__TransportMethods *transport_methods,
                      mstro_app_id *id_p,
                      const char *component_name, uint64_t component_index,
                      struct mstro_pm_app_registry_entry **entry_p);


/* @brief Ensure we know how to talk to the application *appid* */
mstro_status
mstro_pc_app_befriend(mstro_app_id appid, const char* serialized_ep, const Mstro__Pool__TransportMethods *methods);

/* @brief add a fake record to application registry to flag that this entry is being read elsewhere and 
 * will be added when fi_read completes */
mstro_status
mstro_pc_app_register_pending(mstro_app_id id);

/** @brief Register peer application ID
 **
 ** This is similar to mstro_pm_app_register(), except that it needs
 ** to be passed the app ID in the @arg id argument (to match what the
 ** pool manager assigned).
 **
 **/
mstro_status
mstro_pc_app_register(struct mstro_endpoint *ep,
                      fi_addr_t addr,
                      char* serialized_desc,
                      const Mstro__Pool__TransportMethods *transport_methods,
                      mstro_app_id id,
                      struct mstro_pm_app_registry_entry **entry_p);

/** @brief Register the pool manager as an application */
mstro_status
mstro_pm_app_register_manager(struct mstro_endpoint *ep,
                              fi_addr_t addr);

/** De-Register the application @arg op->appid */
mstro_status
mstro_pm_app_deregister(mstro_pool_operation op);


/** Look up the entry for app @arg appid. Return MSTRO_FAIL if not found */
mstro_status
mstro_pm_app_lookup(mstro_app_id appid,
                    struct mstro_pm_app_registry_entry **app_entry_p);

/** A table entry for a CDO reference in the global pool. Abstract, so
 * that implementations of a distributed registry do not need to
 * change the interface */
struct mstro_pm_cdo_registry_entry;

/** Handle DECLARE: APP is asking for a CDO_ID for CDO_NAME, serial number @arg local-id */
mstro_status
mstro_pm_cdo_registry_declare(mstro_app_id app,
                              const char *cdo_name,
                              uint64_t local_id,
                              struct mstro_cdo_id *cdo_id_p);


/*Wrapper to work with operations*/
mstro_status
mstro_pm_cdo_registry_declare_op(mstro_pool_operation op);

/** Check whether ID has been declared by APP. Returns MSTRO_OK if found, MSTRO_FAIL if not found. */
mstro_status
mstro_pm_cdo_registry_find(mstro_app_id app, const struct mstro_cdo_id *id);

/** Retrieve name of ID if it has been declared by any app. Returns
 * MSTRO_OK if found, and a static string for ID in *cdo_name,
 * MSTRO_NOMATCH otherwise */
mstro_status
mstro_pm_cdo_registry_cdo_name_lookup(const struct mstro_cdo_id *id,
                                      const char **cdo_name);


/* Printing attributes for a nicer debug */
void
mstro_pm_cdo_registry_print_attributes(
   const struct mstro_cdo_id *cdoid,
   const mstro_app_id app_id,
   Mstro__Pool__Attributes *attributes);

/** @brief Register ATTRIBUTES on CDOID for APP_ID.
 **
 ** When an APP SEALs a CDO it sends us the current attributes it
 ** wants the pool manager to know about the CDO. This function stores
 ** them on the global CDO registry.
 **
 ** CONSUMES the attributes pointer; caller must duplicate if
 ** necessary before call.
 **
 ** It is an error to call this if attributes have been stored before
 ** for the same app_id and cdoid.
 **
 **/
mstro_status mstro_pm_cdo_registry_store_attributes(
    const struct mstro_cdo_id *cdoid,
    const mstro_app_id app_id,
    Mstro__Pool__Attributes *attributes);

/**
 * @brief wrapper for mstro_pm_cdo_registry_update_state to work with operations
 * 
 * @param op pool operation being handled
 * @return mstro_status 
 */
mstro_status
mstro_pm_cdo_registry_update_state_op(mstro_pool_operation op);

/**
 * @brief tries to perform pm cdo withdraw
 * If the cdo can be widrawn immediately it retruns MSTRO_OK, Otherwise returns MSTRO_WOULDBLOCK to return to operation queue
 * @param op operation handle
 * @return mstro_status out status
 */
mstro_status
mstro_pm_cdo_registry_withdraw(mstro_pool_operation op);

/**
 * @brief send ack for pm cdo declare
 * 
 * @param op operation handle
 * @return mstro_status 
 */
mstro_status
mstro_pm_cdo_registry_send_declare_ack(mstro_pool_operation op);


/** @brief Handle some or all outstanding demand queue entries.
 **
 ** Called by transport initator thread to process the demand queue.
 ** Will consider all current outstanding DEMAND entries and assign
 ** them to transport tasks.
 **/
mstro_status
mstro_pm_handle_demand_queue(void);


/** @brief Insert a transport demand into the transport queue
 **
 ** Informs the transport subsystem that CDO_ID should be transported to REQUESTOR.
 **
 ** Typically this is called after a REQUIRE or DEMAND comes in from
 ** that app, but it can also happen at other times at the pool manager's discretion.
 **
 ** The CDO_ID needs to be in the CDO registry for the REQUESTOR.  If
 ** it has not been DECLARED/SEALED by the requestor in the CDO
 ** registry, the pool manager must have injected it in
 ** ::MSTRO_CDO_STATE_INJECTED.
 **
 ** it is a wise decision.
 **/
mstro_status
mstro_pm_demand_queue_insert(const struct mstro_cdo_id *cdo_id,
                             mstro_app_id requestor);


/** Perform DISPOSE on operation */

mstro_status
mstro_pm_cdo_registry_dispose(mstro_pool_operation op);


/** Record the fact that a transport we initiated has completed.
 **
 ** Called to update the state of CDO_ID for APP_ID as 'APP_ID has
 ** completely received the CDO.  If it was due to a REQUIRE it is now
 ** considered SATISFIED and availabe as source for others, if it was
 ** in response to a DEMAND we handled it is considered as the
 ** confirmation that the DEMAND is completed and the CDO is out of
 ** the global pool.
 **/
mstro_status
mstro_pm_cdo_registry_transfer_completed(mstro_pool_operation op);


/** Check if CDO_SELECTOR matches for CDO ID on ORIGIN's registry entry
 *
 * Return MSTRO_OK if yes, MSTRO_NOMATCH if not; other return values
 * are error codes.  It is not an error to call this when ORIGIN has
 * no live entry for ID -- the return value will be MSTRO_NOMATCH in
 * that case.
 *
 **/
mstro_status
mstro_pm_cdo_app_match(mstro_app_id origin, const struct mstro_cdo_id *id,
                       const struct mstro_cdo_selector_ *cdo_selector);

/** Convert the mapping between two distributed layouts found by mmb_layout_compute_intersection
  * to a list of candidates (apps, local cdo-ids, offsets, lengths) that satisfy
  * the requested distribution from a different source distribution.
  * Return MSTRO_OK on success.
**/
static inline
mstro_status
mstro_pm__mmbLayoutIntersecton_to_candidates(
                            mmbLayoutIntersection *intersection,
                            size_t dst_index,
                            mmbLayout *src_layout,
                            struct per_app_cdo_entries *app_to_attributes_table,
                            mstro_pm_candidates **candidates);


/** Find an offered distributed cdo with the required exact layout
  * (including index),i.e., 1:1 mapping
**/
static inline
mstro_status
mstro_pm__find_cdo_with_layout(
                          struct per_app_cdo_entries *app_to_attributes_table,
                          mmbLayout *s_layout,
                          struct per_app_cdo_entries **app,
                          uint64_t *local_id);


/** Create and allocate  mstro_pm_candidates needed to hold the list of
 * applications and local cdo ids that fullfill the current demand
**/
static inline
mstro_status
mstro__pool_create_candidates(mstro_pm_candidates **candidates, size_t length, bool distribution);

/** Destroy mstro_pm_candidates **/
static inline
mstro_status
mstro_pm_candidates_destroy(mstro_pm_candidates *candidates);



/**@} (end of group MSTRO_I_PM_Registry) */
/**@} (end of group MSTRO_Internal) */

#endif /* MSTRO_POOL_MANAGER_REGISTRY_H_ */
