/* -*- mode:c -*- */
/** @file
 ** @brief DRC Cray Dynamic Credential Manager interface
 **
 */
/*
 * Copyright (C) 2019 Cray Computer GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MSTRO_I_DRC_H_
#define MSTRO_I_DRC_H_ 1

#include "maestro/status.h"
#include "rdma/fabric.h"
#include <stdbool.h>

/**@ingroup Internal
 * @{
 **@defgroup InternalDRC Cray Dynamic Credential Management for fabric keys
 **@{
 **/

/** DRC information is stored in a mstro_drc_info_ structure */
struct mstro_drc_info_;
typedef struct mstro_drc_info_ *mstro_drc_info;

/** obtain DRC credentials */
mstro_status
mstro_drc_init(mstro_drc_info *result_p);

/** destroy DRC info structure, release DRC credential if do_release is TRUE */
mstro_status
mstro_drc_destroy(mstro_drc_info info, bool do_release);

/** Insert DRC credentials into a libfabric object */
mstro_status
mstro_drc_insert_ofi(struct fi_info *fi,
                     const mstro_drc_info info);

/** Obtain cookie for passing to partners (as string, suitably serialized). Allocates *result_p. */
mstro_status
mstro_drc_get_oob_string(char **result_p,
                         const mstro_drc_info info);

/** same, but return numeric DRC credential ID */
mstro_status
mstro_drc_get_credential(uint32_t *result_p,
                         const mstro_drc_info info);

/** Create drc object from OOB string info */
mstro_status
mstro_drc_init_from_oob_string(mstro_drc_info *result_p,
                               const char *info_string);

/** same, but from DRC credential ID */
mstro_status
mstro_drc_init_from_credential(mstro_drc_info *result_p,
                               uint32_t credential);


/**@} (InternalDRC) */
/**@} (Internal) */ 

#endif /* I_DRC_H_ */
