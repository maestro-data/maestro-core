/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO Attributes public header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_ATTRIBUTES_H_
#define MAESTRO_ATTRIBUTES_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#include "maestro/status.h"
#include "mamba.h"

/** an abstract schema type */
typedef struct mstro_schema_ *mstro_schema;


/**@addtogroup MSTRO_Core
 **@{
 **/

  /**@defgroup MSTRO_Attr Maestro Attributes API
   **@{
   ** This is the Attribute API, as developed for D3.2
   **/

/* /\** a structure holding a set of attributes *\/ */
typedef struct mstro_attribute_dict_* mstro_cdo_attributes;

/** Default attribute value specifier for a CDO */
#define MSTRO_ATTR_DEFAULT ((mstro_cdo_attributes)NULL)

/** create attribute container
 *
 * reserved_attributes must be NULL.
 */
mstro_status
mstro_cdo_attributes_create(void *reserved_attributes,
                            mstro_cdo_attributes* result);

/** Add (KEY, VAL) to ATTR.
 *
 * KEY will be copied and not referenced by the container.
 *
 * VAL_OWNED indicates that the value is to be considered owned by the
 * ATTR container after successful insertion and should be freed by
 * calling VAL_DESTRUCTOR when the container is destroyed.
 *
 * COPY_VAL_ON_USE indicates that if a function like
 * mstro_cdo_declare() uses this attribute to populate an object's
 * attribute dictionary, the value should be copied. Otherwise the
 * value will be referenced and the caller is responsible for ensuring
 * sufficient lifetime of the ATTR object. *
 *
 * Will return an error of MSTRO_NOT_TWICE if KEY is already present.
 *
 * Note that maestro has a concept of the 'current schemata loaded' at
 * any time, and keys and value sizes are therefore known implicitly.
 */
 
mstro_status
mstro_cdo_attributes_add(mstro_cdo_attributes attr,
                         const char *key,
                         void *val,
                         bool val_owned,
                         void (*val_destructor)(void*),
                         bool copy_val_on_use);

/** Destroy an attribute dictionary */
mstro_status
mstro_cdo_attributes_destroy(mstro_cdo_attributes attr);

/**@defgroup MSTRO_Attr_builtin Maestro Core Predefined Attributes
   @{
   Predefined symbolic key strings exist for the well-known attributes
**/

/**@brief maestro.core.cdo.name
 **
 ** The name of the CDO
 **
 ** C-side data type: `char *'
 ** default value: false (set automatically by mstro_cdo_declare())
 **/
extern const char *MSTRO_ATTR_CORE_CDO_NAME;

/**@brief maestro.core.cdo.allocate-now
 **
 ** Indicate that Maestro should perform allocation before DECLARE completes
 **
 ** C-side data type: `bool'
 ** default value: false
 **/
extern const char *MSTRO_ATTR_CORE_CDO_ALLOCATE_NOW;

/**@brief maestro.core.cdo.maestro-provided-storage
 **
 ** Indicate that maestro should perform resource management
 **
 ** C-side data type: `bool'
 ** default value: false
 **/
extern const char *MSTRO_ATTR_CORE_CDO_MAESTRO_PROVIDED_STORAGE;


/**@brief maestro.core.cdo.raw-ptr
 **
 ** The raw pointer value in user-accessible address space for the
 ** local data of the CDO, contiguous.
 **
 ** Interacts with maestro.core.cdo.scope.local-size.
 **
 ** Get only be set. To query use mstro_cdo_access_ptr().
 **
 ** If unavailable (non-contiguous CDO etc.) the value returned will be NULL.
 **
 ** If set, the maestro.core.cdo.scope.local-size value must also be set.
 **
 ** C-side data type: `void*'
 ** default value: none
 **/
extern const char *MSTRO_ATTR_CORE_CDO_RAW_PTR;

/**@brief maestro.core.cdo.local-size
 **
 ** Local size of the data pointed by raw-ptr.
 **
 ** Interacts with maestro.core.cdo.scope.raw-ptr.
 **
 ** If set, the maestro.core.cdo.scope.raw-ptr value must also be set.
 **
 ** C-side data type: `uint64_t'
 ** default value: none
 **/
extern const char *MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE;

/**@brief mstro.core.cdo.isgroup
 **
 ** Indicating that a CDO is actually representing a CDO group.
 **
 ** C-side data type: `bool`
 ** default value: not set/none
 **/
extern const char *MSTRO_ATTR_CORE_CDO_ISGROUP;

/**@brief mstro.core.cdo.group-members
 **
 ** Opaque group member descriptor.
 **
 ** C-side data type: `void*`
 ** default value: not set/none
 **/
extern const char *MSTRO_ATTR_CORE_CDO_GROUP_MEMBERS;

/**@brief mstro.core.cdo.isdistributed
 **
 ** Indicates that the CDO data are distributed among various components.  The
 ** current CDO only has part of the whole CDO data.
 ** Users are not expected to utilize or  set this attribute.
 **
 ** C-side data type: `bool`
 ** default value: False
 **/
extern const char *MSTRO_ATTR_CORE_CDO_ISDISTRIBUTED;

/**@brief mstro.core.cdo.dist-layout
 **
 ** Layout of the distrbuted CDO, i.e. which piece of the whole CDO data the
 ** current CDO represents.
 **
 ** C-side data type: `mmbLayout`
 ** default value: NIL
 **/
extern const char *MSTRO_ATTR_CORE_CDO_DIST_LAYOUT;


/* TODO DOC */
extern const char* MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY;
//extern const char* MSTRO_ATTR_CORE_CDO_STR_TABLE [];
//int MSTRO_ATTR_CORE_CDO_STR_TABLE_LEN(void);
extern const char* MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE;
extern const char* MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS;

  /* FIXME: these need to be replaced by schema names/lookups */
#define MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_ROWMAJOR 0
#define MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_COLMAJOR 1

extern const char* MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER;
extern const char* MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE;


/* FIXME: add the others:

   name: "my_cdo_1"

   level: 2

   id: "550e8400-e29b-11d4-a716-446655440000" # UUID default: NULL cdo, set to global id at declare

   lifetime: "Preserve this 24h" # optional, default: empty string

   redundancy: "Always keep 1 copy" # optional, default: empty string

   persist: "on" # optional, default: "off"

   desist: "off" # optional, default: "off"

   scope:	# optional

   size: 1024 # not optional

   layout: "TODO proper type for Mamba layout description"    # optional

   distribution: "Block cyclic" #optional, default empty
*/

/**@} (end of group mstro_attr_builtin) */


/** A datatype to hold an rfc3339 timestamp.
 *
 */
typedef struct {
  int64_t sec;    /* Number of seconds since the epoch of 1970-01-01T00:00:00Z */
  int32_t nsec;   /* Nanoseconds [0, 999999999] */
  int16_t offset; /* Offset from UTC in minutes [-1439, 1439] */
} mstro_timestamp;

/** Parse an RFC3339 timestamp.

 * Return result through (caller-allocated) *tsp.
 *
 * If tsp==NULL, the parse will be performed, but no result
 * returned. The status code indicates whether the string can be
 * successfully parsed.
 */
mstro_status
mstro_timestamp_parse(const char *str, size_t len, mstro_timestamp *tsp);

/** Check whether TSP is a valid timestamp. */
mstro_status
mstro_timestamp_valid(const mstro_timestamp *tsp, bool *valid);

/** Format timestamp as string. Returns length of formatted output, 0 on error. */
size_t
mstro_timestamp_format(char *dst, size_t len, const mstro_timestamp *tsp);

/** Format timestamp as string with a given precision.
 **
 ** Precision can be between 0 and 9, and designates the fractional seconds precision.
 **
 ** Returns length of formatted output, 0 on error. */
size_t
mstro_timestamp_format_precision(char *dst, size_t len, const mstro_timestamp *tsp, int precision);

/** Compare timestamps, returning <0, 0, >0 */
int
mstro_timestamp_compare(const mstro_timestamp *tsp1, const mstro_timestamp *tsp2);

/** Convert timestamp to C style (caller-allocated) struct tm, UTC */
mstro_status
mstro_timestamp_to_tm_utc(const mstro_timestamp *tsp, struct tm *tmp);

/** Convert timestamp to C style (caller-allocated) struct tm, local time zone */
mstro_status
mstro_timestamp_to_tm_local(const mstro_timestamp *tsp, struct tm *tmp);


/** Parse mmbLayout from string */
mstro_status
mstro_mmbLayout_parse(const char *str, mmbLayout **dist_layout);


/** Built-in Maestro attribute data types */
enum mstro_cdo_attr_value_type {
  /** Invalid value */
  MSTRO_CDO_ATTR_VALUE_INVALID = 0,

  /** Not applicable, since the key has not been found */
  MSTRO_CDO_ATTR_VALUE_NA,

  /** Not available (no value for key has been set) */
  MSTRO_CDO_ATTR_VALUE_NONE,

  /* types castable to the respective C types */
  MSTRO_CDO_ATTR_VALUE_bool,     /**< castable to bool */
  MSTRO_CDO_ATTR_VALUE_int32,    /**< castable to int32_t */
  MSTRO_CDO_ATTR_VALUE_uint32,   /**< castable to uint32_t */
  MSTRO_CDO_ATTR_VALUE_int64,    /**< castable to int64_t */
  MSTRO_CDO_ATTR_VALUE_uint64,   /**< castable to uint64_t */
  MSTRO_CDO_ATTR_VALUE_float,    /**< castable to float */
  MSTRO_CDO_ATTR_VALUE_double,   /**< castable to double */

  /* these are castable to const char* */
  MSTRO_CDO_ATTR_VALUE_cstring,  /**< castable to const char* */
  MSTRO_CDO_ATTR_VALUE_json,     /**< @deprecated JSON string, castable to const char * */
  MSTRO_CDO_ATTR_VALUE_yaml,     /**< @deprecated YAML string, castable to const char * */
  MSTRO_CDO_ATTR_VALUE_blob,     /**< Opaque blob type, represented by
                                  **  @ref mstro_blob. User must
                                  **  extract as appropriate. */

  MSTRO_CDO_ATTR_VALUE_timestamp, /**< castable to mstro_timestamp */
  MSTRO_CDO_ATTR_VALUE_cdoid,     /**< castable to mstro_cdo_id */

  MSTRO_CDO_ATTR_VALUE_pointer,   /**< castable to void*. Not transmitted to other localities. */

  MSTRO_CDO_ATTR_VALUE_mmblayout, /**< mmblayout ...mainly for distributed cdos support*/

  /* this is the number of different value types supported */
  MSTRO_CDO_ATTR_VALUE__MAX
};


  // All data types have an implicit length, so we need a wrapper
/** A data type to hold blobs.
 **
 ** This provides a wrapper around the user memory region so that we can know the size of it.
 **/
typedef struct {
  size_t len; /**< the length of the data at @ref mstro_blob.data */
  void *data; /**< an opaque memory region of size @ref mstro_blob.len */
} mstro_blob;

/** convenience function to wrap a blob and its size into a mstro_blob object.
 *
 * (You can also use stack-allocated mstro_blob structures, or allocate them yourself.)
 *
 * If you create a blob with this function you need to free it using mstro_blob_dispose()
 */
mstro_status
mstro_blob_create(size_t len, void *data, mstro_blob **result_p);

/** convenience function to dispose a blob allocated with mstro_blob_create()
 */
mstro_status
mstro_blob_dispose(mstro_blob *b);

/** Opaque CDO handle. */
typedef struct mstro_cdo_ *mstro_cdo;

/**
 ** @brief Add (*key*, *val*) pair to attribute set of *cdo*
 **
 ** @param[in] 	cdo                         A CDO handle
 ** @param[in]	key                         Attribute key string
 ** @param[in]  val                         Pointer to the value to be set
 ** @param[in]	copy_attribute_value        Create an internal allocation for the value and
 **		                            copy @arg val into it
 ** @param[in]  user_owned_attribute_value  Consider the value user-owned in the dictionary.
 **
 ** BEWARE: If @arg copy_attribute_value is set to false, the memory
 ** pointed to by @arg val must remain valid for the entire lifetime
 ** of the CDO. Stack-allocated variables passed in by address are a
 ** source of ugly to trace bugs.
 **
 ** It is most common to set @arg user_owned_attribute_value to ::TRUE
 ** if @arg copy_attribite_value is set to ::FALSE, and vice
 ** versa. Setting both to ::FALSE has a use case in explicitly
 ** passing the reference ownership to maestro. Setting both to ::TRUE
 ** is unsupported.
 **
 ** 
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_attribute_set(mstro_cdo cdo, const char* attribute_key, void* attribute_val,
                        bool copy_attribute_value,
                        bool user_owned_attribute_value);

/**
 ** @brief Retrieve value into *val_p* associated with *key* of *cdo*.
 **
 ** The value is returned as a pointer to const void*. It can be cast
 ** to the type appropriate to the key. The type of the value will be
 ** returned in *valtype* (if given as non-NULL).
 **
 ** @FIXME: for values that have type ::MSTRO_CDO_ATTR_VALUE_BLOB we
 ** need a way for the user to inquire about the blob length.
 **
 ** @param[in]	cdo	A CDO handle
 ** @param[in]	key	Attribute key string
 ** @param[out] valtype	Type of the queried value
 ** @param[out] val_p	Pointer to the queried value
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **
 **/
mstro_status
mstro_cdo_attribute_get(mstro_cdo cdo, const char* attribute_key,
                        enum mstro_cdo_attr_value_type *valtype,
                        const void ** attribute_val_p);

/**
 ** @brief Add *keyval_in_yaml* pair of size *len* to attribute set of *cdo*
 **
 ** Consider using predefined constants for the key (see attributes.h) as they will be looked up more efficiently.
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[in]	keyval_in_yaml	(key,value) pair(s) in a YAML string format
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_cdo_attribute_set_yaml(mstro_cdo cdo, const char *keyval_in_yaml);

/**
 ** @brief Parse CDO attributes from file
 **
 **/
mstro_status
mstro_cdo_attribute_set_yaml_from_file(mstro_cdo cdo, const char *fname);

/**
 ** @brief Set default attributes.
 **
 ** Initialize attribute table of *cdo* using default values.
 **
 ** @param[in]	cdo		A CDO handle
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/

mstro_status
mstro_cdo_attribute_set_default(mstro_cdo cdo);


/**
 ** @brief Inquire about an attribute's value type.
 **
 ** Check if KEY is known in SCHEMA, and if so, return its value type though *VAL_TYPE
 **
 ** @param[in]  schema          The schema in which to perform the lookup
 ** @param[in]  key             The fully qualified key name.
 ** @param[out] knownp          Variable that will be set to TRUE if the key is a known attribute in the schema
 ** @param[out] val_type        The value type for values associated with key, if the key is known.
 **
 ** @returns A status code, ::MSTRO_OK on success.
 */
mstro_status
mstro_attribute_lookup_type(mstro_schema schema,
                            const char *key,
                            bool *knownp,
                            enum mstro_cdo_attr_value_type *val_type);


/**
 ** @brief Print user set keys and values
 **
 */
void 
mstro_cdo_attributes_print(mstro_cdo cdo);


  /**@} (end of group MSTRO_Attr) */

  /** Pattern for RFC3339-timestamp string representation (see @ref
 * mstro_timestamp) */
#define RFC3339_PATTERN ("^([0-9]{4})-([0-9]{2})-([0-9]{2})"                     \
                         "([Tt]([0-9]{2}):([0-9]{2}):([0-9]{2})(\\.[0-9]+)?)?"   \
                         "(([Zz]|([+-])([0-9]{2}):([0-9]{2})))?")

/** Built-in schemata: */
extern const unsigned char *MSTRO_SCHEMA_BUILTIN_YAML_CORE;
extern const size_t         MSTRO_SCHEMA_BUILTIN_YAML_CORE_LEN;

extern const unsigned char *MSTRO_SCHEMA_BUILTIN_YAML_ECMWF;
extern const size_t         MSTRO_SCHEMA_BUILTIN_YAML_ECMWF_LEN;

/**@} (end of addtogroup MSTRO_Core) */

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif /* MAESTRO_ATTRIBUTES_H_ */
