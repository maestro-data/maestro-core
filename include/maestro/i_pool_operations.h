/* -*- mode:c -*- */
/** @file
 ** @brief Maestro pool operations infrastructure
 **/
/* Copyright © 2023 Hewlett Packard Enterprise Development LP
 * Copyright (C) 2022 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_POOL_OP_H_
#define MAESTRO_I_POOL_OP_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_POOL_OP Maestro Pool operations implementation
 **@{
 **
 ** Creates a self contained Maestro pool operation out of incoming messages
 ** that can processed later by different threads, blocked, and restarted
 ** until operation completes
 **/

#include "maestro/status.h"
#include <inttypes.h>
#include "protocols/mstro_pool.pb-c.h"
#include "protocols/mstro_ep.pb-c.h"
#include "maestro/i_cdo.h"
#include "maestro/core.h"
#include <rdma/fabric.h>


/** maestro pool operation kinds */
enum mstro_pool_operation_kind {
  MSTRO_OP_INVALID = 0,                  /**< invalid pool operation kind         */
  MSTRO_OP_PM_DECLARE,                   /**< cdo pm declare operation            */
  MSTRO_OP_PM_SEAL,                      /**< cdo pm seal operation               */
  MSTRO_OP_PM_OFFER,                     /**< cdo pm offer operation              */
  MSTRO_OP_PM_REQUIRE,                   /**< cdo pm require operation            */
  MSTRO_OP_PM_RETRACT,                   /**< cdo pm retract operation            */
  MSTRO_OP_PM_DEMAND,                    /**< cdo pm demand operation             */
  MSTRO_OP_PM_WITHDRAW,                  /**< cdo pm withdraw operation           */
  MSTRO_OP_PM_DISPOSE,                   /**< cdo pm dispose operation            */
  MSTRO_OP_PM_LEAVE,                     /**< app pm leave operation              */
  MSTRO_OP_PM_JOIN,                      /**< app pm join operation               */
  MSTRO_OP_PM_TRANSFER_COMPLETE,         /**< pm cdo transfer complete operation  */
  MSTRO_OP_PM_SUBSCRIBE,                 /**< pm subscribe operation              */
  MSTRO_OP_PM_UNSUBSCRIBE,               /**< pm unsubscribe operation            */
  MSTRO_OP_PM_EVENT_ACK,                 /**< pm event ack operation              */
  MSTRO_OP_PM_MSG_RESOLVE,               /**< pm msg resolve operation            */
  /**-------------------PC side operations----------------------------------------*/
  MSTRO_OP_PC_INIT_TRANSFER,             /**< pc init transfer                    */
  MSTRO_OP_PC_TRANSFER,                  /**< pc transfer ticket                  */
  MSTRO_OP_PC_DECLARE_ACK,               /**< pc declare ack                      */
  MSTRO_OP_PC_POOL_ACK,                  /**< pc pool operation ack               */
  MSTRO_OP_PC_SUBSCRIBE_ACK,             /**< pc subscribe ack                    */
  MSTRO_OP_PC_BYE,                       /**< pc bye (leave ack)                  */
  MSTRO_OP_PC_EVENT,                     /**< pc event                            */
  MSTRO_OP_PC_RESOLVE_REPLY,             /**< pc resolve reply                    */
  MSTRO_OP_PC_TRANSFER_COMPLETED,        /**< pc transfer completed               */
  MSTRO_OP_MAX        
};

/** maestro pool operation steps 
 * This list can grow as we need, and we can insert NULL (no-op) for the steps that do not apply for a certain op kind. 
*/
enum mstro_pool_operation_step {
  MSTRO_OP_ST_INVALID = 0,    /**<invalid pool operation step*/
  MSTRO_OP_ST_ANNOUNCE,       /**<announce before operation  */
  MSTRO_OP_ST_CHECK_ACK,      /**<check acks before          */
  MSTRO_OP_ST_EXE,            /**<execute operation          */
  MSTRO_OP_ST_AFTR,           /**<announce after operation   */
  MSTRO_OP_ST_CHECK_AFTR_ACK, /**<check acks after           */
  MSTRO_OP_ST_SEND_POOLOP_ACK,/**<PM ack operation completion*/
  MSTRO_OP_ST_UPDATE_STATS,   /**Update stats                */
  MSTRO_OP_ST_MAX         
};

/**operation state, for printing and debugging purpose*/
enum mstro_pool_operation_state {
  MSTRO_OP_STATE_INVALID = 0,
  MSTRO_OP_STATE_FAILED,           /**operation failed    */
  MSTRO_OP_STATE_BLOCKED,          /**operation is blocked*/
  MSTRO_OP_STATE_STEP_COMPLETED,   /**step completed      */
  MSTRO_OP_STATE_COMPLETED,        /**operation completed */
  MTRO_OP_STATE_MAX
};

/** opaque maestro request handle */
typedef struct mstro_pool_operation_ *mstro_pool_operation;

/** function prototype for pool operations step handler*/
typedef mstro_status (*mstro_pool_op_st_handler)(mstro_pool_operation op);

/** structure to capture a maestro pool operation */
struct mstro_pool_operation_ {
  enum mstro_pool_operation_kind kind;      /**<operation kind*/
  enum mstro_pool_operation_step step;      /**<step to execute*/
  _Atomic(uint64_t) nr_outstanding_acks;    /**<number of outstanding acks ... we should not need before and after counters!!*/
  mstro_app_id appid;                       /**<appid*/
  struct mstro_cdo_id cdoid;                /**<cdoid*/
  Mstro__Pool__MstroMsg  *msg;              /**<msg*/
  const mstro_pool_op_st_handler *handler_steps;  /**<steps to execute in this operation*/
  mstro_status status;            /**<status of the core operation for the PoolOP ack*/
  
  /**-- operation specific*/
  union
  {
    Mstro__Pool__SubscriptionHandle *subscription_handle; /**for subscribe operation*/
    bool send_attribute_update; /**for seal operation */
    struct { /** for join operation*/ 
      const struct mstro_endpoint *ep; 
      fi_addr_t translated_addr;
    } join;
    struct /** for init transfer*/
    {
      mstro_cdo target_cdo; 
      mstro_app_id target_appid;
      Mstro__Pool__TransportMethods *methods; 
      char *target_serialized_endpoint;
      struct mstro_endpoint *target_ep;
      fi_addr_t target_addr;
      mstro_request request;
      Mstro__AppInfo* dst_epd;
    } pc_transport;
    
    
  };
  
};


/** allocate new pool operation*/
mstro_status
mstro_pool_op__allocate(mstro_pool_operation *op);

/** deallocate pool operation*/
mstro_status
mstro_pool_op__free(mstro_pool_operation op);

/**
 * @brief convenience function to print the operation kind
 * Useful for debugging messages 
 * 
 * @param operation_type enum value for operation kind
 * @return char* string represents the operation type
 */
char *
mstro_pool_op_kind_to_string(enum mstro_pool_operation_kind operation_type);

/*Function to execute an operation*/
mstro_status
mstro_pool_op_engine(void *operation);


/**@} (end of group MSTRO_I_POOL_OP) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_POOL_OP_H_ */

