/* -*- mode:c -*- */
/** @file
 ** @brief Maestro core API public header file
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_CORE_H_
#define MAESTRO_CORE_H_ 1

#ifdef __cplusplus
extern "C" {
#endif
  #include <stdint.h>
  #include "maestro/config.h"
  #include "maestro/status.h"
  #include <stdint.h>
  #include <inttypes.h>

  /**@defgroup MSTRO_Core Maestro Core API
   **@{
   ** This is the core API, as developed for D3.2
   **/

  /**
   ** @brief Initialize the maestro core
   **
   ** Integrates the calling process into a Maestro run.
   **
   ** A Maestro run is defined by the *workflow_name* passed here.
   **
   ** All processes that use the same *component_name* will be
   ** considered part of a joint entity (e.g., the ranks of an MPI
   ** application, a set of processes in a worker pool, ...).
   **
   ** All processes that use the same *component_name* need to provide a
   ** unique *component_index*. The indices do not need to be
   ** consecutive. Consider using the MPI rank or UPC THREAD value, or
   ** a Unix PID. Negative index values are reserved for future
   ** extension.
   **
   ** Both *workflow_name* and *component_name* can be NULL, in which case the
   ** environment variables @ref MSTRO_ENV_WORKFLOW_NAME and @ref
   ** MSTRO_ENV_COMPONENT_NAME will be consulted.
   **
   ** @param[in] workflow_name     The workflow ID.
   **
   ** @param[in] component_name    The component ID.
   **
   ** @param[in] component_index   The unique index among all processes
   **                          using the same *component_name*.
   **
   ** @return A status value, @ref ::MSTRO_OK on success.
   **
   ** A return value of @ref ::MSTRO_NOT_TWICE is returned if a
   ** process is trying to use the same triple of
   ** (wokflow_name/component_name/component_index) that another
   ** process is using against the same Pool Manager at this time.
   **/
  mstro_status
  mstro_core_init(const char *workflow_name,
                  const char *component_name,
                  uint64_t component_index);

  /**
   ** @brief De-initialize the maestro core
   **
   ** @return A status value, @ref ::MSTRO_OK on success.
   **/
  mstro_status
  mstro_core_finalize(void);

  /**
   ** Application-in-workflow identifier type
   **/
  typedef uint64_t mstro_app_id;

  /**
   ** A printf format specifier suitable to print an @ref mstro_app_id value
   **/
  #define PRIappid PRIu64

  /** the number of characters needed to print an @ref mstro_app_id value using @ref PRIappid */
  #define MSTRO_APP_ID_STR_LEN (21) // decimal, no sign, no NUL

  /** type to express nanosecond time points (since an arbitrary point in time) */
  typedef uint64_t mstro_nanosec_t;

  /** Printing support */
  #define PRInanosec PRIu64
  
  /** Return the current time */
  mstro_nanosec_t
  mstro_clock(void);
  
  /** nsec-per-seconds multiplier */
  #define NSEC_PER_SEC ((mstro_nanosec_t)1000*1000*1000)
  

  /**@} (end of group MSTRO_Core) */

  /* include the remaining public API */
#include "maestro/cdo.h"
#include "maestro/pool.h"
#include "maestro/pool_manager.h"
#include "maestro/attributes.h"
#include "maestro/groups.h"
#include "maestro/env.h"


#ifdef __cplusplus
} /* end of extern "C" */
#endif
#endif /* MAESTRO_CORE_H_ */
