/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool functions, local and global
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_POOL_H_
#define MAESTRO_I_POOL_H_ 1

#include "maestro/i_state.h"
#include "maestro/i_cdo.h"
#include "maestro/i_uthash.h"
#include <pthread.h>

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_Pool Internal Maestro Pool Management
 **@{
 ** This is the Internal Pool Management API, as developed for D3.2
 **/

/**@brief Add CDO to pool, either in a fresh entry (for the CDO-ID) or add
 ** to existing entry for CDO-ID.
 **
 ** The *cdo* state will be set to the *new_state* upon successful addition.
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[out]	new_state	cdo new state
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool__add(mstro_cdo cdo, mstro_cdo_state new_state);

/**@brief Remove CDO from the pool
 **
 ** The *cdo* state will be set to the *new_state* upon success.
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[out]	new_state	cdo new state
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool__remove(mstro_cdo cdo, mstro_cdo_state new_state);


/**@brief Remove CDO from the pool asynchronously
 **
 ** Removes the cdo from the global pool as a first stage of @mstro_cdo_withdraw_async.
 ** @mstro_request_wait or @mstro_request_test should be called to check that the asynchronous operation is completed.
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[out]	new_state	cdo new state
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool__remove_async(mstro_cdo cdo, mstro_cdo_state new_state);

/**@brief wait for local requires for a cdo before being removed
 **
 ** The *cdo* state will be set to the target state specified in *request* upon success.
 **
 ** @param[in]	request a mstro cdo asynchronous operation handler
 **
 ** @returns void
 **/
void mstro_cdo_wait_local_requires(mstro_request request);

/**@brief wait for local demands for a cdo to be fulfilled by transport
 **
 ** The *cdo* state will be set to the target state specified in *request* upon success.
 **
 ** @param[in]	request a mstro cdo asynchronous operation handler
 **
 ** @returns void
 **/
void mstro_wait_local_demand(mstro_request request);


/**@brief test for local demands for a cdo to be fulfilled by transport
 **
 ** The *cdo* state will be set to the target state specified in *request* upon success.
 ** If there are waiters on this cdo, the test fails and returns false. It does not wait/block until waiters requests are fulfilled.
 **
 ** @param[in]	request a mstro cdo asynchronous operation handler
 **
 ** @returns A boolean value, true if it could change the cdo state to the target state
 **/
bool mstro_test_local_demand(mstro_request request);


/**@brief Increment or decrement the number of waiter on a cdo
 **
 **
 ** @param[in]	cdo a mstro cdo, where we need to change the number of waiters on it.
 ** @param[in]	change a signed integer value represents the increment or decrement in the number of waiters

 ** @returns MSTRO_OK on success or error status
 **/
mstro_status mstro_pool_cdo_waiters(mstro_cdo cdo, int change);

/**@brief test for local requires for a cdo before being removed
 **
 ** The *cdo* state will be set to the target state specified in *request* upon success.
 ** If there are waiters on this cdo, the test fails and returns false. It does not wait/block until waiters requests are fulfilled.
 **
 ** @param[in]	request a mstro cdo asynchronous operation handler
 **
 ** @returns A boolean value, true if it could change the cdo state to the target state
 **/
bool mstro_cdo_test_local_requires(mstro_request request);

/**@brief Demand CDO from pool
 **
 ** The *cdo* state will be set to the *new_state* upon success.
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[out]	new_state	cdo new state
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool__demand(mstro_cdo cdo, mstro_cdo_state new_state);

/**@brief Demand CDO from pool
 **
 ** Initiate the cdo demand operation
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[out]	new_state	cdo new state
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool__demand_async(mstro_cdo cdo, mstro_cdo_state new_state);

/**@brief Completes cdo demand operation with pool manager and performs clean up.
 **
 ** This function does not wait or block.
 **
 ** @param[in]	request		A CDO operation request handle
 **
 ** @returns void
 **/
void mstro_cdo_wait_demand_async_with_pm(mstro_request request);

/**@brief Retract CDO  op from local pool
 **
 ** The *cdo* state will be set to the *new_state* upon success.
 **
 ** @param[in]	cdo		A CDO handle
 ** @param[out]	new_state	cdo new state
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool__retract(mstro_cdo cdo, mstro_cdo_state new_state);


/**@brief Initialize pool
 **
 ** Initializes the local part of the pool, and notifies pool manager
 ** of it (if there is one)
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **
 **/
mstro_status
mstro_pool_init(void);

/**@brief Finalize pool
 **
 ** Shuts down local pool functionality.
 **
 ** Notifies pool manager to possibly pull CDOs that are still needed
 ** off the local resources, then shuts down local pool operations.
 **
 ** @returns A status code, ::MSTRO_OK on success.
 **/
mstro_status
mstro_pool_finalize(void);


/**@brief Find a CDO in the pool by CDOID with certain local-id
 **
 ** Locates a CDO by its CDOID.
 **
 ** *result is set to the CDO handle if found, or NULL if not.
 **
 **@returns A status code, ::MSTRO_OK on success, ::MSTRO_FAIL if not found
 **/
mstro_status
mstro_pool__find_cdo_with_local_id(
                                const struct mstro_cdo_id *cdoid,
                                mstro_cdo *result);

/**@brief Find a CDO in the pool by CDOID that has data
 **
 ** Locates a CDO by its CDOID that can provide the data of the CDO.
 **
 ** *result is set to the CDO handle if found, or NULL if not.
 **
 **@returns A status code, ::MSTRO_OK on success, ::MSTRO_FAIL if not found
 **/
mstro_status
mstro_pool__find_source_cdo(const struct mstro_cdo_id *cdoid,
                            const Mstro__Pool__Attributes *desired_attributes,
                            mstro_cdo *result);

/**@brief Find a CDO in the pool by CDOID that needs data
 **
 ** Locates a CDO by its CDOID that can is ready to accept data.
 **
 ** Typically this is a CDO that appeared as argument to REQUIRE or
 ** DEMAND, but could also be a 'shallow' CDO whose resources are
 ** currently in use for other purposes.
 **
 ** *result is set to the CDO handle if found, or NULL if not.
 **
 **@returns A status code, ::MSTRO_OK on success, ::MSTRO_FAIL if not found
 **/
mstro_status
mstro_pool__find_sink_cdo(const struct mstro_cdo_id *cdoid,
                          const Mstro__Pool__Attributes *available_attributes,
                          mstro_cdo *result);


/**@brief Notify the pool of a change of the state in the CDO
 **/
mstro_status
mstro_pool__notify(mstro_cdo cdo);


/* check if there is a risk of deadlock should this thread go into a cond-wait now */
void
mstro__abort_if_deadlock_imminent(const char *func, const char* file, int line);

/* code should use this macro instead of @ref
 * mstro__abort_if_deadlock_imminent() to get better source position
 * information */
#define mstro__abort_if_deadlock() do { mstro__abort_if_deadlock_imminent(__func__, __FILE__, __LINE__); } while(0)

/**@} (end of group MSTRO_I_Pool) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_POOL_H_ */
