/* -*- mode:c -*- */
/** @file
 ** @brief Maestro OpenFabric threads header file
 **/
/*
 * Copyright © 2023 Hewlett Packard Enterprise Development LP
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_OFI__THREADS_H_
#define MAESTRO_I_OFI_THREADS_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_OFI_THREADS Network layer
 **@{
 ** This is the Internal Network layer interface
 **/


#include "maestro/status.h"
#include "maestro/i_fifo.h"
#include "maestro/i_mempool.h"
#include "maestro/i_event.h"

/* a hash table type for the ofi operation queues*/
KHASH_INIT(ofi_operation_queue, struct mstro_endpoint *,
           /* value type */ mstro_fifo,
           /* a map */ 1,
           kh_voidptr_hash_func, kh_voidptr_hash_equal)

/**struct to hold an OFI message to send*/
struct mstro_ofi_threads_send_op_
{
    fi_addr_t dst;                  /**< address of the target component*/
    struct mstro_msg_envelope *msg; /**< msg to be sent*/
    mstro_event event;              /**< event to fire up when receiving the completion of the send*/
};

typedef struct mstro_ofi_threads_send_op_ *mstro_ofi_threads_send_op;

/**struct to hold an OFI read request*/
struct mstro_ofi_threads_read_op_
{
    fi_addr_t target;          /**< address of the target component*/
    void *buf;                 /**< local read buffer*/
    size_t len;                /**< length of local read buffer*/
    void *mr_desc;             /**< mr descriptor*/
    uint64_t mr_addr;          /**< mr address associated with the read operation */
    uint64_t mr_key;           /**< mr key*/
    struct mstro_endpoint *ep; /**< the endpoint that this operation */ 
    mstro_event event;         /**< event to fire up when receiving the completion of the read*/
};

typedef struct mstro_ofi_threads_read_op_ *mstro_ofi_threads_read_op;

/** maestro pool operation kinds */
enum mstro_ofi_threads_operation_kind {
  MSTRO_OFI_OP_INVALID = 0,  /**< invalid ofi thread operation kind */
  MSTRO_OFI_OP_SEND,         /**< send msg operation -> fi_send     */
  MSTRO_OFI_OP_READ,         /**< read operation     -> fi_read     */
  MSTRO_OFI_OP_MAX        
};

/**struct to hold an OFI operation queue */
struct mstro_ofi_threads_op_
{
  enum mstro_ofi_threads_operation_kind kind; /**< ofi operation kind*/
  mstro_request request;
  union
  {
    struct mstro_ofi_threads_read_op_ read;
    struct mstro_ofi_threads_send_op_ send;
  };    
};

typedef struct mstro_ofi_threads_op_ *mstro_ofi_threads_op;


/*Thread context*/
typedef struct 
{
  /**Thread specific*/
  char *tidprefix;  /**< thread ID prefix */
  int numa_node;    /**< thread NUMA domain target; -1 for don't care */
  size_t id;        /**< unique index */
  /**shared among the team*/
  _Atomic bool *team_stop;
  _Atomic(size_t) *threads_ready; /**track how many ofi threads have completed init and ready*/
  mstro_msg_handler msg_handler;

  /**created and destroyed on thread start/end*/
  mstro_fifo operation_queue; /**< queue to pull messages from */
  struct mstro_endpoint_set *ep_set;
  mstro_mempool msg_ctx_pool; /**context thread private mempool*/
} mstro_ofi_thread_context;

/*encapsulating ofi thread infomation*/
struct mstro_ofi_thread_team_member {
  pthread_t t;
  mstro_ofi_thread_context ctx;
};

/**
 * @brief enqueue a msg for sending on a send queue of an ofi thread
 * based on the associated endpoint, the message will be enqueued to
 * the respective send queue of the thread handling this endpoint
 * @param dst message destination, i.e. where to send the message
 * @param msg message to send "struct mstro_msg_envelope *"
 * @return mstro_status MSTRO_OK when succeed.
 */
mstro_status
mstro_ofi_threads_enqueue_for_send(fi_addr_t dst, struct mstro_msg_envelope *msg, mstro_event completion_event);

/* OFI thread team ... FIXME think how to coverage and merge with erl thread teams**/
struct mstro_ofi_thread_team {
  char *team_name;
  size_t num_members;
  _Atomic bool team_stop;
  _Atomic(size_t) threads_ready;
  struct mstro_ofi_thread_team_member members[];
};

/**
 * @brief enqueue an fi_read operation to an ofi thread
 * based on the associated endpoint, the read operation will be enqueued to
 * the respective operation queue of the thread handling this endpoint
 * 
 * @param target address of read target on this endpoint
 * @param buf local read target buffer
 * @param len length of the read operation
 * @param mr_desc mr descriptor
 * @param mr_addr mr address
 * @param mr_key mr key 
 * @param ep endpoint to perform this operation
 * @param completion_event an event to fire up when we recieve a completion for the fi_read
 * @param request maestro request to wait and check on the status of the requested operation, i.e. fi_read
 * @return mstro_status 
 */
mstro_status
mstro_ofi_threads_enqueue_rdma_read(fi_addr_t target, void *buf, size_t len, 
                                    void *mr_desc, uint64_t mr_addr, uint64_t mr_key, 
                                    struct mstro_endpoint *ep, mstro_event completion_event, mstro_request request);

mstro_status
mstro_ofi_thread_team_create(
    const char *team_name,
    const char *tidprefix,
    size_t num_threads,
    mstro_msg_handler msg_handler,
    struct mstro_ofi_thread_team **ofi_team);

/** stop threads in @arg ofi_team, deallocate resources in @arg ofi_team */
mstro_status
mstro_ofi_thread_team_stop_and_destroy(struct mstro_ofi_thread_team *ofi_team);

/**
 * @brief Wait until all threads in the team complete their initialization, 
 * build their endpoints, and 
 * are ready to post recieves and handle incoming messages
 * Blocking call !!
 * 
 * @param ofi_team OFI thread team
 */
void
mstro_ofi_thread_team_wait_ready(struct mstro_ofi_thread_team *ofi_team);

/**
 * @brief returns an endpoint set on this numa node. 
 * Operation threads wishing to befriend a new components need to use this function
 * to obtain an endpoint set on their same local numa 
 * and use it for connection with the new component
 * 
 * @param team ofi_thread team handle (pc or pm)
 * @param target_numa numa node
 * @return mstro_status enpointset on the target numa
 */
struct mstro_endpoint_set *
mstro_ofi_thread_get_endpointset(struct mstro_ofi_thread_team *team, int target_numa);

/**
 * @brief returns an endpoint set that contains this endpoint. 
 * Operation threads wishing to befriend a new components need to use this function
 * to obtain an endpoint set on the contacted thread.
 * and use it for connection with the new component
 * 
 * @param team ofi_thread team handle (pc or pm)
 * @param ep contacted endpoint
 * @return mstro_status enpointset
 */
struct mstro_endpoint_set *
mstro_ofi_thread_get_endpointset_ep(struct mstro_ofi_thread_team *team,const struct mstro_endpoint *ep);


/**@} (end of group MSTRO_I_OFI_THREADS) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_OFI__THREADS_H_ */
