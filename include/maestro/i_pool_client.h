/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool Client implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_POOL_CLIENT_H_
#define MAESTRO_I_POOL_CLIENT_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_POOL_CLIENT Maestro Pool Client implementation
 **@{
 ** This is the Pool Client handling interface
 **/


#include "maestro/status.h"
#include "maestro/i_pool_manager_protocol.h"
#include "protocols/mstro_pool.pb-c.h"

#include <stdbool.h>
#include <stdatomic.h>


/* Pool client "Handle incoming Message" dispatcher function*/
mstro_status
mstro_pc_handle_msg(const struct mstro_msg_envelope *envelope);


/**
 * @brief Check ticket and source cdo
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__prepare_init_transfer(mstro_pool_operation op);

/**
 * @brief Check ticket and source cdo for transfer ticket
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__prepare_transfer(mstro_pool_operation op);

/**
 * @brief Execute cdo transport
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__transfer_execute(mstro_pool_operation op);

/**
 * @brief handle event at the PC side
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__event(mstro_pool_operation op);

/**
 * @brief handle transfer completed at the PC side
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__transfer_completed(mstro_pool_operation op);

/**
 * @brief Checks dst app registry entry. 
 * If dst app is in the registry  returns MSTRO_OK, and jumps directly to writing the ticket
 * If dst app info is currently being read in another thread, returns MSTRO_WOULDBLOCK
 * If dst app is not in registry, issues a read (mstro_ofi__select_endpoint) and returns MSTRO_OK
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__app_befriend(mstro_pool_operation op);


/**
 * @brief Waits for app info to be read and registers the app when the read is completed
 * returns MSTRO_WOULDBLOCK is the read is not complete yet
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__reg_friend_app(mstro_pool_operation op);

/**
 * @brief Writes and sends the transfer ticket
 * 
 * @param op operation handle 
 * @return mstro_status return status
 */
mstro_status
mstro_pc__init_transfer_send_ticket(mstro_pool_operation op);

/**
 * @brief handle declare ack at pc side
 * 
 * @param op operation handle 
 * @return mstro_status return status
 */
mstro_status
mstro_pc__declare_ack(mstro_pool_operation op);

/**
 * @brief handle subscribe ack at PC side
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__subscribe_ack(mstro_pool_operation op);

/**
 * @brief handle resolve reply at the PC side
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__resolve_reply(mstro_pool_operation op);

/**
 * @brief Handle PoolOp ack at PC side
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__poolop_ack(mstro_pool_operation op);

/**
 * @brief handle bye operation at the PC side
 * 
 * @param op operation handle
 * @return mstro_status return status
 */
mstro_status
mstro_pc__bye(mstro_pool_operation op);

/**
 * @brief Create mstro_pool_operations from pc incoming msgs
 * pushes the created operations to the *queue*
 * 
 * @param msg message handle 
 * @return mstro_status return status
 */
mstro_status
mstro_pc__op_maker(Mstro__Pool__MstroMsg *msg);

/**@} (end of group MSTRO_I_POOL_CLIENT) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_POOL_CLIENT_H_ */
