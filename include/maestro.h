/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Data Orchestration -- Public API
 **
 **/
/*
 * Copyright (C) 2018 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef MAESTRO_H_
#define MAESTRO_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"

  /* status codes */
#include "maestro/status.h"

  /* the core */
#include "maestro/core.h"

  /**
   ** @brief Initialize the maestro library
   **
   ** Integrates the calling process into a Maestro run.
   **
   ** A Maestro run is defined by the *workflow_id* passed here.
   **
   ** All processes that use the same *component_name* will be
   ** considered part of a joint entity (e.g., the ranks of an MPI
   ** application, a set of processes in a worker pool, ...).
   **
   ** Multiple posix threads can be in the same component.
   **
   ** All processes that use the same *component_name* need to provide a
   ** unique *component_index*. The indices do not need to be
   ** consecutive. Consider using the MPI rank or UPC THREAD value, or
   ** a Unix PID. Negative index values are reserved for future
   ** extension.
   **
   ** Both *workflow_name* and *component_name* can be NULL, in which case the
   ** environment variables @ref MSTRO_ENV_WORKFLOW_NAME and @ref
   ** MSTRO_ENV_COMPONENT_NAME will be consulted.
   **
   ** @param workflow_name     The workflow ID.
   **
   ** @param component_name    The component ID.
   **
   ** @param component_index   The unique index among all processes
   **                          using the same component_name.
   **
   ** @return A status value, @ref ::MSTRO_OK on success.
   **
   ** A return value of @ref ::MSTRO_NOT_TWICE is returned if a
   ** process is trying to use the same triple of
   ** (wokflow_name/component_name/component_index) that another
   ** process is using against the same Pool Manager at this time.
   **
   **/
  mstro_status
  mstro_init(const char *workflow_name,
             const char *component_name,
             int64_t component_index);

  /**
   ** @brief De-initialize the maestro library
   **
   ** @return A status value, @ref MSTRO_OK on success.
   **/
  mstro_status
  mstro_finalize(void);

  /**
   ** @brief Obtain a human-readable version string describing the
   ** library version
   **
   ** @return An unmodifiable, unchanging value.
   **/

  const char *
  mstro_version(void);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MAESTRO_H_ */

/* We abuse this file to store the main page documentation file for now */
/** @mainpage Overview

    @section Intro

    Maestro is the Middleware for Memory- and Data-awareness in Workflows.


    This project has received funding from the European Union’s
    Horizon 2020 research and innovation program through grant
    agreement 801101.

    https://www.maestro-data.eu/

    @section Entrypoints

    The core API (@ref MSTRO_Core) provides an interface to the Core Data Object
    abstraction (@ref MSTRO_CDO), which depends on Attributes (@ref
    MSTRO_Attr) like Scope (@ref MSTRO_Scope, post-D3.2) and Memory System Model
    (@ref MSTRO_MSM, post-D3.2), as well as Transformations (@ref
    MSTRO_Transformations, post-D3.2) and movement (@ref MSTRO_Pool).

    The pool manager protocol is based on the @ref MSTRO_Pool_Protocol
    protobuf (https://developers.google.com/protocol-buffers)
    specification which should make it possible to implement
    components for maestro in other languages.

    TODO
	Pool Manager (@ref MSTRO_I_OFI, @ref MSTRO_I_PM_Registry, @ref MSTRO_Protocols, etc.)
	@ref MSTRO_Transport (./transport)
    Attributes remastered



    Maestro Core Internal API documentation: @ref MSTRO_Internal

*/
