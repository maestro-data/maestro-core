Showroom
========

"What can we use Maestro Core for?"


Overview
--------

Workflow applications using Maestro Core offer and demand CDOs to the
:ref:`Maestro Pool<cdo_management>`, a conceptual entity that represents the
set of resources contributed to Maestro, in particular the set of offered CDOs.
One typical use of Maestro Core is bypassing persistent storage for application
coupling, by way of the Maestro Pool.  The :ref:`Pool Manager<pm>` is a Maestro
Core-produced application that handles networking, transport scheduling and
propagates pool events for inspection.

:ref:`CDOs<cdo>` are basically Maestro Core currency, they contain data and
metadata, including user-defined metadata plus data semantics such as data
layout information and other information on data usage the transfer scheduler
may take advantage of.

.. image:: img/maestro_core_components.png
   :alt: Maestro core components overview

:ref:`Events<events>` allow for higher-level control of the Maestro-enabled
workflow, and interfacing with many useful components. In particular, they
allow the implementation of data-driven Workflow Managers or more generally
Execution Frameworks, meaning they can schedule jobs based on CDO availability
and location which is indeed notified by events. 
As an example of another useful workflow components, staging and archiving CDOs
of choice may be implemented by users via dedicated components, archiving
relying especially on events to be notified of the availability of
(possibly :ref:`cherry-picked<story_cherry>`) CDOs for archiving purposes.

In addition to :ref:`data management<cdo_management>`, Maestro Core facilitates
:ref:`memory management<mamba>`, allowing to abstract away memory layer
consideration as well as providing features such as tiling.
CDO location is expressed as a so-called Memory Object,
which can mean a certain number of layers (DRAM, GDRAM, NVRAM, PFS, etc.) and
associated access methods (pointer, path, object ID, etc.), which is then used
by Maestro Core to handle the transport between layers and nodes.

CDOs may convey a wide range of data and memory semantics.  In particular,
:ref:`data layout<layout>` information lets Maestro make the transformations
between a given offered CDO in a given data layout L1 (say row-major) to the
demanded CDO on the consumer side that may have a layout L2 (say column-major).
This allows user to abstract away data layout, especially if tools or
source-to-source substitution fills the data layout attribute (semi)
automatically. Data layouts may be distributed and Maestro Core handle the
redistribution complying with producer and consumer layout and distribution
scheme requirements.  


Demos
-----

A few demonstrators are shipped with Maestro Core, which are compiled and run
through

.. code-block:: shell

  make check 

- Local multi-threaded setup ``demo_mvp_d3_2``: with a few parameters such as number of producer, consumer and archiver threads.

- Multi-application setup ``check_pm_interlock``: The interlock demo is a minimal workflow with Pool Manager and two applications, exchanging a CDO under the Pool Manager supervision, using filesystem and object store transport, RDMA is used if any transport unavailabilities.

- Logger sample application ``simple_telemetry_listener``

- Archiver sample application ``simple_archiver``



