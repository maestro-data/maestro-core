# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# let RTD know it must run doxygen first, for breathe/Sphinx to pick it up
import subprocess, os

read_the_docs_build = os.environ.get('READTHEDOCS', None) == 'True'

if read_the_docs_build:
    print('Running on read the docs')
    subprocess.call('cd ../../; autoreconf -ifv; ./configure; make -Cdocs/doxygen', shell=True)


# -- Project information -----------------------------------------------------

project = 'Maestro Core'
#copyright = '2021, Christopher Haine, Ali Mohammed, Utz-Uwe Haus'
author = 'Christopher Haine, Ali Mohammed, Utz-Uwe Haus'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

# diagrams are nice but readthedocs parser chokes on them
# extensions = ['breathe', 'sphinxcontrib.seqdiag']
extensions = ['breathe']

# Breathe Configuration
breathe_default_project = "MaestroCore"

breathe_projects = { "MaestroCore": "../doxygen/xml" }

breathe_domain_by_extension = {
        "h" : "c",
        }

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_sidebars = {
   '**': ['globaltoc.html', 'searchbox.html'],
}


