.. Maestro Core documentation master file, created by
   sphinx-quickstart on Mon Aug  2 17:33:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Maestro Core documentation
==========================

Maestro Core is a C library that does multi-threaded cross-application data
transfer and inspection over high-speed interconnect, using lightweight data
wrappers named CDOs that include user metadata.

A producer application using Maestro Core may offer data and metadata

.. code-block:: c

   mstro_init("my_workflow", "component_A", ...);
   mstro_cdo src_handle;
   mstro_cdo_declare(src_handle, "my_cdo_name");
   mstro_cdo_attribute_set(src_handle, 
                           MSTRO_ATTR_CORE_CDO_RAW_PTR,
                           src_data, ...);
   mstro_cdo_attribute_set(src_handle, 
                           MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                           &size, ...);
   mstro_cdo_attribute_set(src_handle, 
                           ".maestro.my-namespace.my-key",
                           &value, ...);
   mstro_cdo_offer(src_handle);

Which a consumer application may demand

.. code-block:: c

   mstro_init("my_workflow", "component_B", ...);
   mstro_cdo dst_handle; 
   mstro_cdo_declare(dst_handle, "my_cdo_name");
   mstro_cdo_require(dst_handle);
   mstro_cdo_demand(dst_handle);

When ``mstro_cdo_demand()`` returns, the data and metadata has
been transferred by Maestro Core from the producer and is locally available for
the consumer.

CDOs are central to the Maestro Core design.

Maestro Core is part of the `Maestro <https://www.maestro-data.eu/>`__
framework, which aims at data-driven workflow orchestration.

That's it for the introduction.

.. toctree::
   :maxdepth: 2

   showroom 
   started
   essentials
   services
   tuning
   stories
   dev

