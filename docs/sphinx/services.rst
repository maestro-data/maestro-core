Services
========

Ready-to-use Maestro-enabled convenience apps 

Librarian
---------

The Librarian is a multi-threaded MPI application allowing staging and
archiving CDOs. It is built during the :ref:`standard build process<install>`
and can be found at ``$(MAESTRO_PATH)/examples/librarian``. It minimally
requires as argument a memory ``layer`` and an ``access`` method, typically PFS
(Parallel File System) and a path, and so goes staging:

.. code-block:: shell

 mpirun ./librarian --stage 
                    --layer PFS 
                    --access <path>

With an extra ``pattern`` argument for archiving for describing the CDOs of interest, with a regex-like syntax akin the one used for :ref:`selectors<events>`.

.. code-block:: shell

 mpirun -n 2 ./librarian --unstage 
                         --pattern <regexp-like selector>
                         --layer PFS 
                         --access <path>

The full list of options can be printed with

.. code-block:: shell

 ./librarian --help
