##################################################################################
#Helper functions to read raw input files and identify best combinations         #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################


import glob
import re
import numpy as np
import csv
import math
import matplotlib as mpl
from matplotlib import rc
from matplotlib import pyplot as plt
import os



class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


#application times regex dictionary
regexTime = {}
regexTime["declare"] = "Throughput \(declare/offer\): "
regexTime["withdraw"] = "Throughput \(withdraw/dispose\): "
regexTime["require"] = "Throughput \(declare/require\): "
regexTime["demand"] = "Throughput \(demand/dispose\): "


systemName = {}
systemName["sage"] = "Sage prototype"
systemName["ht-sage"] = "Sage prototype (HT)"


units = {}
units["us"] = 1
units["ms"] = 1000
units["s"]  = 1000000

unitText = {}
unitText["us"] = "$\mu$s"
unitText["ms"] = "$ms$"
unitText["s"]  = "$s$"



floatregex = "[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?"

def findMaxListOfLists(data):
    maxlist = []
    for row in data:
        for sublist in row:
            if isinstance(sublist[0], list):
                for subsublist in sublist:
                    maxlist.append(max(subsublist))
            else:
                maxlist.append(max(sublist))

    return max(maxlist)

def scaleDatabyFactor(data, factor):
    sRow = []
    for row in data:
        sSublist = []
        for sublist in row:
            if isinstance(sublist[0], list):
                sSubsublist = []
                for subsublist in sublist:
                    sSubsublist.append([float(x) / factor for x in subsublist])
                sSublist.append(sSubsublist)
            else:
                sSublist.append([float(x) / factor for x in sublist])
        sRow.append(sSublist)
    return sRow


def scaleDataMagnitude(data):


    magnitude = ""
    factor = 1

    maxvalue = findMaxListOfLists(data)

    #print(maxvalue)

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        magnitude = r' $(\times 10^3)$'
        factor *= 1000

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        magnitude = r' $(\times 10^6)$'
        factor *= 1000

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        magnitude = r' $(\times 10^9)$'
        factor *= 1000

    if maxvalue >= 1000:
        maxvalue = maxvalue/1000
        magnitude = r' $(\times 10^{12})$'
        factor *= 1000

    scaledData = scaleDatabyFactor(data, factor)
    return scaledData, magnitude


def getValueFromFile(filename, pattern, multiplier):

    filename_post = '_[0-9]*'

    filename+=filename_post
    filenames = glob.glob(filename) #get all file names of the repetitions


    value = regexTime[pattern]
    if len(filenames) == 0:
        print(bcolors.FAIL +"Cannot find files with pattern: " + filename + bcolors.ENDC)
    else:
        print(bcolors.OKGREEN +"Reading files: " + filename + bcolors.ENDC)

    temp = []
    for f in filenames:
        for line in open(f):
            match = re.search(value + r"(?P<time>"+ floatregex + ") us", line)
            if match:
                temp.append(float(match.group("time"))/float(multiplier))
            #else:
                #print(line)
    return np.mean(temp)

def getDeclareTime(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, unit, nConsumers='0', transport=0, sizeCDOs=0):
    if nConsumers != '0':
        return getConsumerTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, "declare", units[unit], nConsumers, transport, sizeCDOs)
    else:
        return getTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, "declare", units[unit])


def getWithdrawTime(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, unit, nConsumers='0', transport=0, sizeCDOs=0):
    if nConsumers != '0':
        return getConsumerTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, "withdraw", units[unit], nConsumers, transport, sizeCDOs)
    else:
        return  getTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, "withdraw", units[unit])


def getRequireTime(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, unit, nConsumers, transport, sizeCDOs):
    return  getConsumerTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, "require", units[unit], nConsumers, transport, sizeCDOs)

def getDemandTime(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, unit, nConsumers, transport, sizeCDOs):
    return  getConsumerTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, "demand", units[unit], nConsumers, transport, sizeCDOs)

def getTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, operation, multiplier):

    times = []

    filename = ""
    for t in nThreads:
        temp_a = []
        for a in nAttributes:
            temp_sa = []
            if a == 0: # handle when a == 0 ... no need for inner loop
                sizes = [0]
            else:
                sizes = sizeAttributes

            for sa in sizes:
                filename = path + "/" + str(fabrics)+"_"+ str(t)+"_" + str(a) +"_" + str(sa)+ "_" + str(nCDOThreads)
                temp_sa.append(getValueFromFile(filename, operation, multiplier))
            temp_a.append(temp_sa)
        times.append(temp_a)
    return times


def getConsumerTimes(path, nThreads, nAttributes, sizeAttributes, nCDOThreads, fabrics, operation, multiplier, nConsumers, transport, sizeCDOs):

    times = []

    filename = ""
    for t in nThreads:
        temp_a = []
        for a in nAttributes:
            temp_sa = []
            if a == 0: # handle when a == 0 ... no need for inner loop
                sizes = [0]
            else:
                sizes = sizeAttributes

            for sa in sizes:
                temp_cs = []
                for cSize in sizeCDOs:
                    filename = path + "/" + str(fabrics)+"_"+ str(t)+"_" + str(a) +"_" + str(sa)+ "_" + str(nCDOThreads) + "_" + str(nConsumers) + "_"+ str(cSize) + "_" + str(transport)
                    temp_cs.append(getValueFromFile(filename, operation, multiplier))
                temp_sa.append(temp_cs)
            temp_a.append(temp_sa)
        times.append(temp_a)
    return times

def getScalingData(data, nAttribute, sizeAttribute, cdoSizeIndex=-1):
    output = []

    if cdoSizeIndex == -1:
        for d in data:
            output.append(d[nAttribute][sizeAttribute])
    else:
        for d in data:
            output.append(d[nAttribute][sizeAttribute][cdoSizeIndex])

    return output


def calculateThroughput(data, nThreads, nCDOThreads, unit, sizeCDOs=[0]):

    Throughput_data = []
    Throughput_dataB = [] #for bytes

    i = 0
    for thread in data:
        j = 0
        numAttribs = []
        numAttribsB = [] #for bytes
        for numAttrib in thread:
            k = 0
            sizes = []
            sizesB = [] #for bytes
            for size in numAttrib:
                if sizeCDOs==[0]:
                    sizes.append((1.0) / (units[unit]*data[i][j][k])*1000000) #CDOs = nThreads[i] * nCDOThreads but time is normalized already by the benchmark
                else:
                    cSizes = []
                    cSizesB = [] #for bytes
                    l = 0
                    for cSize in sizeCDOs:
                        cSizes.append((1.0) / (units[unit]*data[i][j][k][l])*1000000) # #CDOs = nThreads[i] * nCDOThreads but time is normalized already by the benchmark
                        cSizesB.append((1.0 * cSize) / (units[unit]*data[i][j][k][l])*1000000)
                        l += 1
                    sizes.append(cSizes)
                    sizesB.append(cSizesB)
                k += 1
            numAttribs.append(sizes)
            numAttribsB.append(sizesB)
            j += 1
        Throughput_data.append(numAttribs)
        Throughput_dataB.append(numAttribsB)
        i += 1

    return Throughput_data, Throughput_dataB


def calculateThroughput_PM(data, nRanks, unit, sizeCDOs=[0]):

    Throughput_data = []
    Throughput_dataB = [] #for bytes

    i = 0
    for thread in data:
        j = 0
        numAttribs = []
        numAttribsB = [] #for bytes
        for numAttrib in thread:
            k = 0
            sizes = []
            sizesB = [] #for bytes
            for size in numAttrib:
                if sizeCDOs==[0]:
                    sizes.append((nRanks[i] * 1.0) / (units[unit]*data[i][j][k])*1000000) #CDOs = nThreads[i] * nCDOThreads but time is normalized already by the benchmark
                else:
                    cSizes = []
                    cSizesB = [] #for bytes
                    l = 0
                    for cSize in sizeCDOs:
                        cSizes.append((nRanks[i] * 1.0) / (units[unit]*data[i][j][k][l])*1000000) # #CDOs = nThreads[i] * nCDOThreads but time is normalized already by the benchmark
                        cSizesB.append((nRanks[i] * 1.0 * cSize) / (units[unit]*data[i][j][k][l])*1000000)
                        l += 1
                    sizes.append(cSizes)
                    sizesB.append(cSizesB)
                k += 1
            numAttribs.append(sizes)
            numAttribsB.append(sizesB)
            j += 1
        Throughput_data.append(numAttribs)
        Throughput_dataB.append(numAttribsB)
        i += 1

    return Throughput_data, Throughput_dataB



def formatValue(value):
    magnitude = 1024

    label = ""
    if value >= magnitude:
        value = value/magnitude
        label = "k"

    if value >= magnitude:
        value = value/magnitude
        label = "M"

    if value >= magnitude:
        value = value/magnitude
        label = "G"

    return str(int(value)) + label



def plotProducerLines(ax, data, nAttributes, sizeAttributes, cs, ms):
    i = 0
    for attrribute in nAttributes:
        j = 0
        if attrribute == 0:
            sizes = [0]
        else:
            sizes = sizeAttributes
        for size in sizes:
            cLabel = "A: " + str(attrribute) + " S: " + formatValue(size)
            ax.plot(getScalingData(data, i, j), color = cs[i], linestyle=':', marker=ms[0], label = cLabel)
            j += 1
        i +=1

def plotConsumerLines(ax, data, sizeCDOs, cs, ms):
    i = 0
    for size in sizeCDOs:
        cLabel = " C: " + formatValue(size)
        ax.plot(getScalingData(data, 0, 0, i), color = cs[i], linestyle=':', marker=ms[0], label = cLabel)
        i += 1

def plotData(data, nAttributes, sizeAttributes, nThreads, path, type, nCDOThreads, sys, fiProvider, exp, unit, yrange=-1, transport="", CDOs=[0], labelEXP="", log=False, xaxis="threads"):


    mpl.rcParams['axes.titlesize'] = 20
    mpl.rcParams['axes.labelsize'] = 20
    mpl.rcParams['xtick.labelsize'] = 16
    mpl.rcParams['ytick.labelsize'] = 16
    mpl.rcParams['legend.fontsize'] = 16
    txtHieght = 0.9
    props = dict(boxstyle='round', facecolor='wheat',edgecolor='wheat' , alpha=0.2)

    cs = ['#f032e6', '#3cb44b', '#4363d8', '#f58231', '#ffe119', '#911eb4', '#46f0f0','#bcf60c',  '#9a6324', '#9a6324', '#9a6324', '#9a6324', '#9a6324',  '#808000', '#808000', '#ffd8b1', '#000075', '#fabebe', '#008080','#808080', '#D4D4D4', '#B4B4B4', '#909090', '#636363', '#494848', '#000000']
#
    ms = ['.','s', '+','*', 'x', 'D', 'v', '^', '<', '>', 'h', 'd', 'p',  'o',  '1', 'P',  'H', 'X', '2',         '3',      '4',   '8', 'p']

    lines = []

    fig1, ax1 = plt.subplots()

    if CDOs == [0]:
        plotProducerLines(ax1, data, nAttributes, sizeAttributes, cs, ms)
        transport_M = ""
    else:
        plotConsumerLines(ax1, data, CDOs, cs, ms)
        transport_M = " | " + transport


    if  yrange != -1:
        yrange = [float(x) for x in yrange.split(',')]
        ax1.set_ylim(yrange)


    ind = np.arange(len(nThreads))
    ax1.set_xticks(ind)
    ax1.set_xticklabels(nThreads)
    if xaxis=="nodes":
        plt.xlabel('Number of nodes')
    elif xaxis == "threads":
        plt.xlabel('Number of threads')


    if ("CDO_throughput" in type):
        plt.ylabel(r'CDOs/s' +labelEXP)
    elif ("data_throughput" in type):
        plt.ylabel(r'bytes/s' + labelEXP)
    else:
        plt.ylabel(r'Execution time ('+unitText[unit]+')')

    if log:
        ax1.set_yscale('log')
        exp += "log_"
        txtHieght = 0.5

    ax1.legend(prop={'size': 12}, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
            ncol=3, mode="expand", borderaxespad=0.)


    ax1.text(0.5*(len(nThreads)-1),txtHieght*ax1.get_ylim()[1], systemName[sys]+" | " + fiProvider + transport_M, fontsize=16,va='bottom', ha='center', bbox=props)

    if not os.path.exists(path+'//figures'):
            os.makedirs(path+'//figures')

    plt.savefig(os.path.normpath(path+'//figures//'+exp+type+"_"+ str(nCDOThreads)+"_"+ fiProvider+'_'+transport+'.pdf'), bbox_inches='tight')



#convert dict to list
def dict2List(dict):

    dictlist = []

    for key in dict.keys():

        dictlist.append(dict[key])

    return dictlist
