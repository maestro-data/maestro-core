##################################################################################
#Helper file for arrayJobGen, which function and configuration definitions       #
##################################################################################
#                                                                                #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################



import math
import glob
import numpy as np

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

system = {}
system["sage"] = 8
system["ht-sage"] = 16


fiProviders = {}
fiProviders["sage"] = ["sockets","verbs"]

#MSTRO_ENV_TRANSPORT_DEFAULT=
transport = {}
transport["sage"]  = ["GFS", "MIO", "OFI"]


consumerModes = {}
consumerModes["sinkall"] = "MSTRO_CONSUMER_SINK_ALL"
consumerModes["one2one"] = "MSTRO_CONSUMER_ONE2ONE"
consumerModes["one2ten"] = "MSTRO_CONSUMER_ONE2TEN"
consumerModes["all2all"] = "MSTRO_CONSUMER_ALL2ALL"


sizeCDOs = [1024, 1048576, 10485760, 104857600] # 1k, 1M, 10M, 100M bytes


nAttributes = [0,1,2,4,8,16,32]


sizeAttributes = [8, 16,32, 64, 8192, 16384] #8, 16, 32, 64, 8k, 16k bytes

def getMaxThreads(sys, ht = 0):

    if ht == 1: #append ht- to system name for the lookup
        sys += "ht-"+sys
    #get num threads on that system
    maxThreads = system[sys] - 2 # one for pc and other
    return maxThreads



def calculatenThreads(sys, ht = 0):
    nThreads = []


    maxThreads = getMaxThreads(sys, ht)

    i = 1
    while(i < maxThreads):
        nThreads.append(i)
        i = i*2

    #add also the maximum num of threads
    nThreads.append(maxThreads)

    return nThreads






def createCommandFile(filename, nAttributes, sizeAttributes, nThreads, appPath, repeats, fabrics, nConsumers, sizeCDOs, transport, nCDOThreads=10000, filemode="w"):

    filename = filename+'.cmd'
    nExperiments = 0

    with open(filename, filemode) as text_file:
        for r in repeats: #repeating each experiment
            for media in transport:
                for CDO_size in sizeCDOs:
                    for fi in fabrics:
                        for t in nThreads:
                            for a in nAttributes: # handle when a == 0 ... no need for inner loop
                                if a == 0:
                                    sizes = [0]
                                else:
                                    sizes = sizeAttributes
                                for sa in sizes:
                                    print('srun {} {} {} {} {} {}'.format(appPath,a,sa,nCDOThreads,nConsumers, CDO_size), file=text_file)
                                    nExperiments += 1

    return nExperiments



def createSlurmBatchJob(preFilename, name, nNodes, nRanks, nExperiments, system, mstroPath, consumerMode, ht=0, jobLimit=18, time="1:30:00", filemode="w"):


    filename = preFilename+'.sbatch'

    nThreads = getMaxThreads(system, ht)

    with open(filename, filemode) as text_file:
        print('#!/bin/sh -l', file=text_file)
        print('#SBATCH --nodes={}'.format(nNodes), file=text_file)
        print('#SBATCH --ntasks={}'.format(nRanks), file=text_file)
        print('#SBATCH --time={}'.format(time), file=text_file)
        print('#SBATCH --job-name={}'.format(name), file=text_file)
        print('#SBATCH --mem=0', file=text_file)


        if system =="sage":
            print('#SBATCH --partition=normal', file=text_file)


        print('#SBATCH --ntasks-per-node={}'.format(int(nRanks/nNodes)), file=text_file)
        print('#SBATCH --ntasks-per-socket={}'.format(1), file=text_file)
        print('#SBATCH --cpus-per-task={}'.format(nThreads), file=text_file)
        print('#SBATCH --array=1-{}%{}'.format(nExperiments,jobLimit), file=text_file)

        if ht !=1:
            print('#SBATCH --hint=nomultithread', file=text_file)
        else:
            print('#SBATCH --hint=multithread', file=text_file)


        if system =="sage":
            print('module use $CLIENT_MOD_PATH', file=text_file)
            print('module load GCC', file=text_file)
            print('module load OpenMPI/4.0.3', file=text_file)


        #common exports
        print('export MPICH_MAX_THREAD_SAFETY=multiple', file=text_file)
        print('export MSTRO_OFI_PM_PC_NUM_THREADS=2', file=text_file) #for multithreaded pool only
        print('export MSTRO_CONSUMER_MODE={}'.format(consumerModes[consumerMode]), file=text_file)
        print('export OMP_PLACES=cores', file=text_file) #OpenMP thread pinning
        print('export OMP_PROC_BIND=close', file=text_file) #spread or close??

        print('export LD_LIBRARY_PATH={}:$LD_LIBRARY_PATH'.format(mstroPath), file=text_file)

        print('$(head -$SLURM_ARRAY_TASK_ID {}.export | tail -1)'.format(preFilename), file=text_file)            #export OMP schedule
        print('exec 1>$(head -$SLURM_ARRAY_TASK_ID {}.filenames | tail -1)'.format(preFilename), file=text_file)  #redirect STDOUT to output file
        print('$(head -$SLURM_ARRAY_TASK_ID {}.cmd | tail -1)'.format(preFilename), file=text_file)               #execute the application


def createExportFile(filename, nAttributes, sizeAttributes, nProducers, repeats, fabrics, transport, sizeCDOs, filemode="w"):
    filename = filename+'.export'


    nExperiments = 0 # number of experiments

    with open(filename, filemode) as text_file:
        for r in repeats: #repeating each experiment
            for media in transport:
                for CDO_size in sizeCDOs:
                    for fi in fabrics:
                        for t in nProducers:
                            for a in nAttributes: # handle when a == 0 ... no need for inner loop
                                if a == 0:
                                    sizes = [0]
                                else:
                                    sizes = sizeAttributes

                                for sa in sizes:
                                    print('export MSTRO_TRANSPORT_DEFAULT={} export OMP_NUM_THREADS={} export FI_PROVIDER={}'.format(media, t, fi), file=text_file)
                                    nExperiments += 1

    return nExperiments



def createOutputfilenamesFile(filename, nAttributes, sizeAttributes, nThreads, repeats, outPath, nCDOThreads, fabrics, nConsumers, sizeCDOs, transport, filemode="w"):

    filename = filename+'.filenames'


    nExperiments = 0 # number of experiments

    with open(filename, filemode) as text_file:
        for r in repeats: #repeating each experiment
            for media in transport:
                for CDO_size in sizeCDOs:
                    for fi in fabrics:
                        for t in nThreads:
                            for a in nAttributes: # handle when a == 0 ... no need for inner loop
                                if a == 0:
                                    sizes = [0]
                                else:
                                    sizes = sizeAttributes
                                for sa in sizes:
                                    print('{}/{}_{}_{}_{}_{}_{}_{}_{}_{}'.format(outPath,fi,t,a,sa,nCDOThreads,nConsumers, CDO_size,media,r), file=text_file)
                                    nExperiments += 1


    return nExperiments
