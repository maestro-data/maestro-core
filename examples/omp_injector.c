/* -*- mode:c -*- */
/** @file
 ** @brief openMP injector component
 **/

/*
 * Copyright (C) 2018-2020 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#include "omp_injector.h"



void create_name(char *dst, size_t idx) {
      snprintf(dst, CDO_NAME_MAX, "Test CDO %zu\n", idx);
}


mstro_status inject_CDOs(int injector_id, mstro_cdo *cdos, size_t num_CDOs, size_t num_attributes, size_t size_attributes, int64_t size_CDO, char **CDO_data) {

  mstro_status s = MSTRO_OK; // global status

  size_t cdoidx;

 int num = atoi(getenv("OMP_NUM_THREADS"));  
#pragma omp parallel for firstprivate(num) schedule(static,1)
  for(int i =0; i<num;i++)
  {
           INFO("Running on CPU %d \n", sched_getcpu());

  }

  /* declare CDOs loop */
  #pragma omp parallel for private(cdoidx) firstprivate(num_attributes, size_CDO,num_CDOs) reduction(| :s)
  for(size_t i=0; i < num_CDOs; i++) {
    mstro_status s1,s2, s3;
    cdoidx = injector_id *num_CDOs + i; /* CDO id = injector_id * thread_id * num_CDOs */
    char name[CDO_NAME_MAX];
    create_name(name, cdoidx);

    s3 = MSTRO_OK; /* initialize its value */
    s1 = mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, & (cdos[i]));


    /* create attributes ...numbers and sizes */
    for(size_t j=0; j < num_attributes ; j++) {
      char attrib_name[ATTRIB_NAME_MAX];
      char * data = malloc(size_attributes*sizeof(char));
      if (data == NULL){
	      ERR("Could not allocate data for attribute %d of cdo %d \n", j, i);
	      s1 = MSTRO_FAIL;
      }
      snprintf(attrib_name, ATTRIB_NAME_MAX, ".maestro.benchmark.attrib_%lu", j+1);
      fill_char_array(data,size_attributes);
      DEBUG("Attribue %d: %s \n", j, data);
      s3 |= mstro_cdo_attribute_set(cdos[i], attrib_name, data, false, true);
      DEBUG("%d: %s \n", s3, mstro_status_description(s3));
    }


    /* attach data */
    if (size_CDO != 0) {
      
      DEBUG("injecting %s: %" PRId64 " \n", name, size_CDO);
      s3 |= mstro_cdo_attribute_set(cdos[i],
                                  MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                  CDO_data[i], false, true);
      s3 |= mstro_cdo_attribute_set(cdos[i],
                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                   &size_CDO, true, false);
    }


    s3 |= mstro_cdo_declaration_seal(cdos[i]);

    s2= mstro_cdo_offer(cdos[i]);
    DEBUG("[injector] offering %s \n", mstro_cdo_name(cdos[i]));

    s = s | s1 | s2 | s3;
  }

  return s;

}

void free_cdo_attributes(mstro_cdo cdo, size_t n_attributes){
	char attrib_name[ATTRIB_NAME_MAX];
	size_t i;
	char *data;
	for (i = 0; i < n_attributes; i++){
		/*create attribute name*/
		 snprintf(attrib_name, ATTRIB_NAME_MAX, ".maestro.benchmark.attrib_%lu", i+1);
		/*get attribute*/
		mstro_cdo_attribute_get(cdo, attrib_name, NULL,(const void**) &data);
		/*free attribute*/
		free(data);
	}
}


mstro_status dispose_CDOs(mstro_cdo *cdos, size_t num_CDOs, size_t num_attributes) {

  mstro_status s = MSTRO_OK;

  #pragma omp parallel for reduction(| :s)
  for(size_t i=0; i < num_CDOs; i++) {
    mstro_status s1;

    free_cdo_attributes(cdos[i], num_attributes);
    s1= mstro_cdo_dispose(cdos[i]);
    s = s | s1;
  }
  return s;

}

void fill_char_array(char *array, size_t size) {
  
  memset(array, 0, size);
  
}

mstro_status withdraw_CDOs(mstro_cdo *cdos, size_t num_CDOs) {

  mstro_status s = MSTRO_OK; 

  #pragma omp parallel for reduction(| :s)
  for(size_t i=0; i < num_CDOs; i++) {
    mstro_status s1;

    s1= mstro_cdo_withdraw(cdos[i]);
    s = s | s1 ;
  }
  return s;

}


mstro_status start_injector(int injector_id, size_t num_attributes, size_t size_attributes, size_t CDOs_per_thread) {

  double declare_time,withdraw_time, before, after;
  size_t num_threads = 1;

  /* Read environment variables */
  if (getenv("OMP_NUM_THREADS") != NULL) {
    num_threads = atoi(getenv("OMP_NUM_THREADS"));
  }

  mstro_status s = MSTRO_OK;


  size_t num_CDOs = CDOs_per_thread * num_threads; 

  s = mstro_init("Tests","Injector",injector_id);

  assert(MSTRO_OK == s);



  mstro_cdo cdos[num_CDOs];

  before = omp_get_wtime();
  /* declare CDOs loop */
  s = inject_CDOs(injector_id, cdos, num_CDOs, num_attributes, size_attributes, 0, NULL);

  assert(MSTRO_OK == s);

  after = omp_get_wtime();
  /*calculate time in microseconds */
  declare_time = (after - before) * 1000.0*1000.0;

  /*Restart timer*/
  before = after; 

  /* withdraw CDOs loop */
  s = withdraw_CDOs(cdos, num_CDOs);

  assert(MSTRO_OK == s);

  after = omp_get_wtime();
  
  /*Calculate time in microseconds */
  withdraw_time = (after - before) * 1000.0*1000.0; 
  fprintf(stdout, "#CDOs: %zu, #Threads: %zu, #Attributes: %zu, Size of attributes: %zu \n", num_CDOs, num_threads, num_attributes, size_attributes);
  fprintf(stdout, "Throughput (declare/offer): %.5f us\n", declare_time/(double) num_CDOs);
  fprintf(stdout, "Throughput (withdraw/dispose): %.5f us\n", withdraw_time/(double) num_CDOs);

  s |= mstro_finalize();

  return s;
}
