/* -*- mode:c -*- */
/** @file
 ** @brief benchmark core CDO operations, i.e., declare, offer, require, demand, withdraw, and dispose.
  * Using multiple producers and consumers.
  * This benchmark demonstrates also how to write an MPI+OpenMP application with Maestro.
 **/

/*
 * Copyright (C) 2018-2020 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro.h"
#include <assert.h>
#include "maestro/logging.h"
#include <string.h>
#include "omp.h"
#include "omp_injector.h"
#include "omp_consumer.h"
#include "mpi.h"


#define BENCHMARK_ATTRIBUTES_YAML "./benchmark_attributes.yaml"
#define MAX_NUM_ATTRIBUTES 35



/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)

int main(int argc, char *argv[]) {

  int injector_id = 1; // could come from MPI_COMM_rank
  size_t num_attributes = 0;
  size_t size_attributes = 0;
  size_t CDOs_per_thread = 10000; // 10k CDOs/thread by default
  size_t nConsumers = 0; // number of consumers
  int64_t size_CDO = 0; // size of CDOs
  mstro_status status = MSTRO_OK; // global status
  char *info = NULL; //maestro info handle

  int rank, size;

  /* read arguments */
  if (argc > 1) {
    num_attributes =atoi(argv[1]);
    if (num_attributes > MAX_NUM_ATTRIBUTES) {
      printf("Maximum number of supported attributes in this benchmark is %d \n", MAX_NUM_ATTRIBUTES);
      num_attributes = MAX_NUM_ATTRIBUTES;
    }
  }
  if (argc > 2) {
    size_attributes =atoi(argv[2]);
  }
  if (argc > 3) {
    CDOs_per_thread =atoi(argv[3]);
  }
  if (argc > 4) {
    nConsumers =atoi(argv[4]);
  }
  if (argc > 5) {
     char c;
     sscanf(argv[5], "%" SCNd64 "%c", &size_CDO, &c);
  }

  /* save print time: */
   setenv("MSTRO_LOG_LEVEL","0",1);


  /* export the path to the custom scehma */
  setenv("MSTRO_SCHEMA_LIST",BENCHMARK_ATTRIBUTES_YAML,1);

  /* read num of threads */
  size_t num_threads = 1;
  /* Read environment variables */
  if (getenv("OMP_NUM_THREADS") != NULL) {
    num_threads = atoi(getenv("OMP_NUM_THREADS"));
  }

  /*start MPI ... if number of ranks is 1, then start only injector component
  that will work with local pool manager*/
  int provided;
  MPI_Init_thread(NULL,NULL,MPI_THREAD_MULTIPLE, &provided);
  if (provided < MPI_THREAD_MULTIPLE) {
     printf("MPI does not support MPI_THREAD_MULTIPLE \n");
  }
  else if (provided < MPI_THREAD_SERIALIZED) {
     printf("MPI does not support MPI_THREAD_SERIALIZED \n");
  }
 else if  (provided < MPI_THREAD_FUNNELED) {
    printf("MPI does not support MPI_THREAD_FUNNELED, \n");
  }
  else if (provided < MPI_THREAD_SINGLE) {
      printf("MPI does not support MPI_THREAD_SINGLE \n");
  }

  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  if (size == 1) {
    /* start producer */
    status = start_injector(rank, num_attributes, size_attributes, CDOs_per_thread);
  }
  else {
    /*If number of ranks is greater than 1,
     * 1. start a pool manager component
     * 2. broadcast PM info
     * 3. look for the num of producers and consumers and start them
     */
    if (rank == 0) {
      /*start pool manager - workflow, component,  id */
      status = mstro_init("Tests", "Pool_Manager", rank);
      if(status!=MSTRO_OK) {
         ERR("Maestro Pool Manager: Failed to initialize Maestro: %d (%s)\n",
            status, mstro_status_description(status));
          /* panic and fail */
          MPI_Abort(MPI_COMM_WORLD, -1);
      }
      status = mstro_pm_start();
      if(status!=MSTRO_OK) {
        ERR("Simple Maestro Pool Manager: Failed to start pool: %d (%s)\n",
              status, mstro_status_description(status));

        /* panic and fail */
        MPI_Abort(MPI_COMM_WORLD, -1);
      }

     
      status = mstro_pm_getinfo(&info);

      if(status!=MSTRO_OK) {
        ERR("Simple Maestro Pool Manager: Failed to obtain pool contact info: %d (%s)\n",
              status, mstro_status_description(status));

        /*  panic and fail */
        MPI_Abort(MPI_COMM_WORLD, -1);
        }

        assert(status == MSTRO_OK);

        /* get size of PM info and broadcast it */
        int info_size = strlen(info) + 1;
        MPI_Bcast(&info_size,1,MPI_INT, 0, MPI_COMM_WORLD);
        /* broadcast the PM info */
        MPI_Bcast(info,info_size,MPI_CHAR, 0, MPI_COMM_WORLD);

        /* sync producers and conumers --otherwise producers may widthraw CDOs before required */
        MPI_Barrier(MPI_COMM_WORLD);

        /* wait for transfers */
        MPI_Barrier(MPI_COMM_WORLD);

    }
    else if ((rank > 0) && (rank <= (int) nConsumers)) {
      double require_time, demand_time, before, after;
      /* get PM info */
      int info_size;
      MPI_Bcast(&info_size,1,MPI_INT, 0, MPI_COMM_WORLD);
      /* get the PM info */
      info = (char *) malloc(info_size*sizeof(char));
      if (info == NULL){
	     ERR("Could not allocate memory for Maestro info object \n");
	     /* panic and fail */
       MPI_Abort(MPI_COMM_WORLD, -1); 
      }
      MPI_Bcast(info,info_size,MPI_CHAR, 0, MPI_COMM_WORLD);

      /* put it in the environment */
      setenv("MSTRO_POOL_MANAGER_INFO",info,1);


      /* get consumer mode */
      int consumer_mode = get_consumer_mode();
      printf("[Consumer %d] consumer mode = %d \n",rank, consumer_mode);
      /* get the number of producers assigned to me */
      int num_producers = get_num_producers(size, nConsumers, consumer_mode);
      printf("[Consumer %d] num_producers = %d \n",rank, num_producers );
      /* allocate producers list */
      int *producers_ids = (int *) malloc(sizeof(int)*num_producers);
      if (producers_ids == NULL){
	      ERR("Could not allocate memory for producers IDs \n");
        /* panic and fail */
        MPI_Abort(MPI_COMM_WORLD, -1);
      }
      /* calculate my assigned producers */
      get_producers(rank, nConsumers, producers_ids, num_producers, consumer_mode);


      for (int i = 0; i < num_producers; i++) {
        printf("producer: %d \n", producers_ids[i]);
      }

      /* create CDOs array */
      size_t num_CDOs = CDOs_per_thread * num_threads*num_producers; /* CDOs per producer * (num_thread) * num_producers */
      printf("[Consumer %d] num_CDOs = %zu \n",rank, num_CDOs );

      mstro_cdo cdos[num_CDOs];

      /* Allocate memory for cdos, allocate memory per thread ...recycle this allocation for various cdos consumed by a single thread */
      char **CDO_data = malloc(sizeof(char *) * num_threads);
      if (CDO_data == NULL){
	      ERR("Could not allocate memory for CDO data handles array \n");
        /* panic and fail */
        MPI_Abort(MPI_COMM_WORLD, -1);
      }
      int ret = -1;
#pragma omp parallel for private(ret)
      for(size_t i = 0; i < num_threads; i++) {
	      ret = posix_memalign((void**) &CDO_data[i], (size_t) sysconf(_SC_PAGESIZE), sizeof(char)*size_CDO);
	      if (ret != 0){
		      ERR("Could not allocate aligned memory for cdo %d of required size %" PRId64 " \n", i, size_CDO);
          /* panic and fail */
          MPI_Abort(MPI_COMM_WORLD, -1);
	      }
	      memset(CDO_data[i], 0, size_CDO); //init memory to warmup
      }


      /* start consumers */
      status = mstro_init("Tests","Consumer",rank);
      assert(MSTRO_OK == status);

      /**Handshaking cdos, to ensure the partner discovery is already done before the benchmark starts*/
      mstro_cdo handshake_cdos[num_producers];
      for (int i = 0; i < num_producers; i++) {
        char name[CDO_NAME_MAX];
        snprintf(name, CDO_NAME_MAX, "Warm up %d\n", producers_ids[i]);
        status |= mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, & (handshake_cdos[i]));
        status |= mstro_cdo_require(handshake_cdos[i]);
        DEBUG("Required CDO %s\n", name);
        status |= mstro_cdo_demand(handshake_cdos[i]);
        DEBUG("Demanded CDO %s\n", name);
        status |= mstro_cdo_dispose(handshake_cdos[i]);
      }
      assert(MSTRO_OK == status);

      /* Require CDOs */
      before = omp_get_wtime();
      double consumer_start = before;

      status = require_CDOs(cdos, num_CDOs, producers_ids, num_producers, CDO_data, size_CDO);
      assert(MSTRO_OK == status);
      after = omp_get_wtime();
      require_time = (after - before) * 1000.0*1000.0; //time in us seconds
      
      fprintf(stdout, "Throughput (declare/require): %.5lf us\n", require_time/(double) num_CDOs);
      fprintf(stdout, "[Consumer %d] declare-require time: %.5lf s\n", rank, after-before);

      /*sync producers and consumers -- avoid CDOs been withdrawn before require */
      MPI_Barrier(MPI_COMM_WORLD);

      /* Demand CDOs */
      before = omp_get_wtime(); /* restart timer here */
      status = demand_CDOs(cdos, num_CDOs);
      assert(MSTRO_OK == status);
      after = omp_get_wtime();
      double consumer_end = after;
      demand_time = (after - before) * 1000.0*1000.0; //time in us seconds
      fprintf(stdout, "Throughput (demand/dispose): %.5lf us\n", demand_time/(double) num_CDOs);
      fprintf(stdout, "[Consumer %d] demand-dispose time: %.5lf s\n", rank,after-before);
      fprintf(stdout, "Bandwidth: %.5f MB/s\n", (double) (num_CDOs*size_CDO)/(demand_time/1000000)/1024/1024); //bandwidth = bytes/s 
      fprintf(stdout, "[Consumer %d] declare-dispose time: %.5lf s\n", rank,consumer_end-consumer_start);
      
      /* wait for transfers */
      MPI_Barrier(MPI_COMM_WORLD);
      status = dispose_CDOs(cdos, num_CDOs, num_attributes);

      /* finalize Maestro */
      status = mstro_finalize();
      assert(status == MSTRO_OK);

      /*clean up*/
      if (producers_ids != NULL){
          free(producers_ids);
      }

      if (CDO_data != NULL){
	      for(size_t i = 0; i < num_threads; i++){
		      free(CDO_data[i]);
	      }
	      free(CDO_data);
      }



    } else{ /* Producers */

      double declare_time,withdraw_time, before, after;

      /* get PM info */
      int info_size;
      MPI_Bcast(&info_size,1,MPI_INT, 0, MPI_COMM_WORLD);
      /* get the PM info */
      info = (char *) malloc(info_size*sizeof(char));
      if (info == NULL){
	       ERR("Could not allocate for maestro info object \n");
               /* panic and fail */
               MPI_Abort(MPI_COMM_WORLD, -1);
      }
      MPI_Bcast(info,info_size,MPI_CHAR, 0, MPI_COMM_WORLD);

      /* put it in the environment */
      setenv("MSTRO_POOL_MANAGER_INFO",info,1);

      size_t num_CDOs = CDOs_per_thread * num_threads;

      status = mstro_init("Tests","Injector",rank);
      assert(MSTRO_OK == status);

      /**Handshaking CDOs */
      mstro_cdo handshaking_cdo;
      char name[CDO_NAME_MAX];
      /**make the cdo with a big enough size to make a real data transfer, e.g. avoid inline tickets or 0 cdo sizes, to warmup all the links*/
      int64_t handshaking_data_size = 1024000; 
      char *handshaking_data;
      int r = posix_memalign((void**) &handshaking_data, (size_t) sysconf(_SC_PAGESIZE), sizeof(char)*handshaking_data_size);
      assert(r == 0);

      snprintf(name, CDO_NAME_MAX, "Warm up %d\n", rank);
      status |= mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, & (handshaking_cdo));
      //posix_memalign((void**) &CDO_data, (size_t) sysconf(_SC_PAGESIZE), sizeof(char)*cdo_data_size);
      status |= mstro_cdo_attribute_set( handshaking_cdo, MSTRO_ATTR_CORE_CDO_RAW_PTR, handshaking_data, false, true);

      status |= mstro_cdo_attribute_set(handshaking_cdo,
                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                   &handshaking_data_size, true, false);
      status |= mstro_cdo_offer(handshaking_cdo);
      DEBUG("offered cdo %s\n", name); 
      assert(MSTRO_OK == status);


      /* create CDOs array */
      mstro_cdo cdos[num_CDOs];
      int ret = -1;
      /* create CDOs data */
      char *CDO_data[num_CDOs];
      if (size_CDO != 0) {
        #pragma omp parallel for private(ret)
        for (size_t i = 0; i < num_CDOs; i++) {
          /* allocate data */
          ret = posix_memalign((void**) &CDO_data[i], (size_t) sysconf(_SC_PAGESIZE), sizeof(char)*size_CDO);
          if (ret != 0 ){
                ERR("Could not allocate aligned memory for cdo %d of required size %" PRId64 " \n", i, size_CDO);
               /* panic and fail */
               MPI_Abort(MPI_COMM_WORLD, -1);
	        }
	        /* fill data */
          fill_char_array(CDO_data[i], size_CDO);
        }

      }

      before = omp_get_wtime();
      double producer_start = before;

      /* declare CDOs loop */
      status = inject_CDOs(rank, cdos, num_CDOs, num_attributes, size_attributes, size_CDO, CDO_data);

      assert(MSTRO_OK == status);

      after = omp_get_wtime();

      /*report time in microseconds */
      declare_time = (after - before) * 1000.0*1000.0;

      fprintf(stdout, "#CDOs: %zu, CDO size: %"PRId64", #Threads: %zu, #Attributes: %zu, Size of attributes: %zu \n", num_CDOs, size_CDO, num_threads, num_attributes, size_attributes);
      fprintf(stdout, "Throughput (declare/offer): %.5lf us\n", declare_time/(double) num_CDOs);
      fprintf(stdout, "[Producer %d] declare-offer time: %.5lf s\n", rank, after-before);
      
      /* sync producers and consumers -- avoid CDOs been withdrawn before require */
      MPI_Barrier(MPI_COMM_WORLD);

      status |= mstro_cdo_withdraw(handshaking_cdo);
      status |= mstro_cdo_dispose(handshaking_cdo);
      assert(MSTRO_OK == status);
      /**free handshaking cdo data*/
      free(handshaking_data);
      
      /* wait for transfers */
      MPI_Barrier(MPI_COMM_WORLD);

      /* restart timer here */
      before = omp_get_wtime();
      /* withdraw CDOs loop */
      status = withdraw_CDOs(cdos, num_CDOs);

      assert(MSTRO_OK == status);

      /* free attributes and dispose CDOs loop */
      status = dispose_CDOs(cdos, num_CDOs, num_attributes);

      assert(MSTRO_OK == status);

      after = omp_get_wtime();
      double producer_end = after;
      withdraw_time = (after - before) * 1000.0*1000.0; //time in us seconds
      fprintf(stdout, "Throughput (withdraw/dispose): %.5lf us\n", withdraw_time/(double) num_CDOs);
      fprintf(stdout, "[Producer %d] withdraw-dispose time: %.5lf s\n", rank, after-before);
      fprintf(stdout, "[Producer %d] declare-dispose time: %.5lf s\n", rank, producer_end- producer_start);

      status = mstro_finalize();

      /* free allocated data*/
      if (size_CDO != 0) {
	      for(size_t i = 0; i < num_CDOs; i++){
		      free(CDO_data[i]);
	      }
      }
      assert(status == MSTRO_OK);
    }

  }


  /*  wait for termination otherwise PM will exit before producers and consumers */
  MPI_Barrier(MPI_COMM_WORLD);

  /* terminate pool manager */
  if ((rank == 0) && (size > 1)) {

    status = mstro_pm_terminate();
    
    if(status!=MSTRO_OK) {
      ERR("Simple Maestro Pool Manager: Failed to shut down pool: %d (%s)\n",
        status, mstro_status_description(status));

    
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
   
    status = mstro_finalize();
   
  }


  if (info != NULL){
	  free(info);
  }

  MPI_Finalize();

  return 0;
}
