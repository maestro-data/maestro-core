#!/usr/bin/env bash
#
# Run a librarians-only workflow
#

# Copyright (C) 2020 Cray Computer GmbH
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# aggressive error tracing
set -euo pipefail

# set a workflow name
MSTRO_WORKFLOW_NAME="libr_test_`date +%s`"
export MSTRO_WORKFLOW_NAME

# ensure error and warnings get colors
MSTRO_LOG_COLOR_ERRORS=1
export MSTRO_LOG_COLOR_ERRORS

MSTRO_LOG_LEVEL="Info"
export MSTRO_LOG_LEVEL

#MSTRO_LOG_MODULES="all,^attr"
MSTRO_LOG_MODULES="all"
export MSTRO_LOG_MODULES

# start pool manager as named pipe
PM_CMD="../tests/simple_pool_manager"

start_pm () {
  # start pool manager, connect its output to fd 3:
  # (we need to run in a subshell to start a new process group)
  exec 3< <(env MSTRO_COMPONENT_NAME="Pool_Manager_`date +%s`" ${PM_CMD})
  PM_PID=$!
  
  
  # ensure USR2 is not caught by this shell
  trap '' USR2
  
  # trap error exit of script: close pipe to pool manager and return error
  trap "terminate ${PM_PID} 99" err
  
  # read pm info: It comes as a CSV of MSTRO_POOL_INFO; base64 text;
  # Potentially we might (later) have the pool manager report changed
  # info over time which we could read with the same read command.
  # 
  read -d ';' -u 3 pm_info_varname
  read -d ';' -u 3 pm_info
  echo "PM info: $pm_info"
  
  if test x"${pm_info_varname}" != xMSTRO_POOL_MANAGER_INFO; then
      return 99;
  fi
  
  MSTRO_POOL_MANAGER_INFO="$pm_info"
  export MSTRO_POOL_MANAGER_INFO
echo $MSTRO_POOL_MANAGER_INFO
  return 0;
}

start_archiver () {
  (env MSTRO_COMPONENT_NAME="Archiver_`date +%s`" MSTRO_LOG_COLOR_ERRORS=1 MSTRO_LOG_COLOR="BLUE" ${ARCHIVER_CMD}) || return 99  &
  return 0;
}

start_stager () {
  (env MSTRO_COMPONENT_NAME="Stager_`date +%s`" MSTRO_LOG_COLOR_ERRORS=1 MSTRO_LOG_COLOR="CYAN" ${STAGER_CMD}) || return 99  &
  return 0;
}

sync () {
  RET=0
  wait %1 || $RET=99
  wait %2 || $RET=99
  
  # trap normal script termination: close pipe to pool manager
#  terminate ${PM_PID} 0 || $RET=99

  return $RET
}

terminate () {
    #$1 is PM_PID, $2 is desired exit code Note that killing PM_PID is
    # not enough, the actual PM will likely be a child of this one,
    # and if we kill it it will be reparented to init(1).
    log "exit, stopping pool manager"
    # close stdout of the pool manager; its heartbeat output on stdout
    # will make it receive a SIGPIPE eventually, which initiates
    # termination.
    exec 3<&-
    log "waiting pm to terminate"
    wait 
    return ${2:-99}	       
}


#
# Find a MPI runner
#
if command -v mpirun &> /dev/null 
then
  LAUNCHER="mpirun"
else
  echo "Error: not meant to run with a batch scheduler."
  exit 99
fi


N_RANKS_MAX=4
N_CDOS_MAX=1
CDO_SIZE=20000001

#
# Preparing random data files for the test
#
INPUT_FILE="../examples/librarian_pioneer_in"
OUTPUT_FILE="../examples/librarian_pioneer_out"
for (( nranks=1; nranks<=$N_RANKS_MAX; nranks++ ))
do 
  TEST_FILE="$INPUT_FILE""_""$nranks"
#  if [ ! -f "$TEST_FILE" ]; then
    head -c $CDO_SIZE </dev/random >$TEST_FILE
#  fi
done

# 
# Preparing Librarian command-line parameters
#
# FIXME have a option to the script to choose slurm execution
TIMEOUT=5
SLEEP_TIME=2
ATIMEOUT=$(( $TIMEOUT+$SLEEP_TIME ))
LIBRARIAN_CMD="./librarian "
STAGER_TIMEOUT="--max-time $TIMEOUT "
ARCHIVER_TIMEOUT="--max-time $ATIMEOUT "

STAGE_ORDER="--stage --layer PFS --access "
ARCHIVE_ORDER="--unstage "
LAYERS=("PFS" "DRAM")
LAYER_ORDERS=("--layer PFS --access " "--layer DRAM ")

LOG_FILE="$0.log"
rm -f $LOG_FILE
log () { echo "$@" >> $LOG_FILE; }
echo "============================================================================"
echo "Running \`Librarian\` regression tests"
echo "============================================================================"
echo "< #ranks [1,$N_RANKS_MAX], stager|archiver, PFS|DRAM, #cdos [1,$N_CDOS_MAX], distributed|single >"
echo "Expected $(( ${#LAYERS[@]} * $N_RANKS_MAX * $ATIMEOUT ))s total execution time"

#
#
# Here we go, this is the non-regression test loop.
# Will run tests combination of the following parameters: 
# - Stager or Archiver 
# - single or distributed CDO
# - number of CDOs
# - number of ranks     
# - layer type
#

N_TEST_CURR=1
N_TEST_MAX=$(( ${#LAYERS[@]} * $N_RANKS_MAX ))

start_pm >> $LOG_FILE 2>&1 || { UNHAPPY="$UNHAPPY: PM unhappy";}

for (( nlayers=0; nlayers<${#LAYERS[@]}; nlayers++ ))
do
  for (( nranks=1; nranks<=$N_RANKS_MAX; nranks++ ))
  do 
    STAGER_CMD=
    ARCHIVER_CMD=
    DIST=
   
  #
  # Preparing Orders
  #  
    INPUT_FILE_X="$INPUT_FILE""_""$nranks"
  # FIXME    PATTERN="(.maestro.core.cdo.name = \"$INPUT_FILE_X\")" 
    if [ "${LAYERS[$nlayers]}" = "PFS" ]; then
      PATTERN="--pattern $INPUT_FILE_X"
    else
      PATTERN="--all-cdos"
    fi 
    OUTPUT_FILE_X="$OUTPUT_FILE""_""$nranks"
    STAGER_CMD="$STAGER_CMD        $STAGE_ORDER $INPUT_FILE_X"
    ARCHIVER_CMD="$ARCHIVER_CMD        $ARCHIVE_ORDER$PATTERN ${LAYER_ORDERS[$nlayers]}"
    if [ "${LAYERS[$nlayers]}" = "PFS" ]; then
      ARCHIVER_CMD="$ARCHIVER_CMD $OUTPUT_FILE_X"
    fi

  #
  # Alternate single and distributed CDO workflows 
  # 
    ASYMMETRY=0
    MAX_DRAM_SINGLE="--max-dram $(( $CDO_SIZE + 1000000)) "
    MAX_DRAM_DIST="--max-dram $(( ($CDO_SIZE + 1000000) / ($nranks)  + 1)) "
  
    if [ $(( $nranks%2 )) -eq 0 ]; then
      MAX_DRAM=$MAX_DRAM_DIST
    else
      MAX_DRAM=$MAX_DRAM_SINGLE
    fi
    RUN_STAGER="$LAUNCHER -n $((nranks + ASYMMETRY)) "
    RUN_ARCHIVER="$LAUNCHER -n $nranks "
  
    STAGER_CMD="$RUN_STAGER$LIBRARIAN_CMD $STAGER_TIMEOUT$STAGER_CMD        $MAX_DRAM "
    ARCHIVER_CMD="$RUN_ARCHIVER$LIBRARIAN_CMD $ARCHIVER_TIMEOUT$ARCHIVER_CMD        $MAX_DRAM "
  
    log " "
    log "#######################"
    log " "
    log "       $STAGER_CMD"
    log "       $ARCHIVER_CMD"
    log "       size/CDO: $CDO_SIZE"
    log " "
    log "#######################"
    log " "
  
    echo -n "Test $N_TEST_CURR/$N_TEST_MAX... "
    N_TEST_CURR=$(($N_TEST_CURR+1))
  
  #
  # Launch the experiment
  #
    UNHAPPY=
    start_stager >> $LOG_FILE 2>&1 || { UNHAPPY="$UNHAPPY - Stager unhappy"; }
    start_archiver >>$LOG_FILE 2>&1  || { UNHAPPY="$UNHAPPY - Archiver unhappy"; }
    sync >> $LOG_FILE 2>&1 || { UNHAPPY="$UNHAPPY - Failed component sync"; } 
  
  #
  # Sanity check
  #
    if [ "${LAYERS[$nlayers]}" = "PFS" ]; then
      cmp -s  "$INPUT_FILE""_""$nranks" "$OUTPUT_FILE""_""$nranks" || { UNHAPPY="$UNHAPPY - Corrupted CDO #$nranks"; }
      rm -f "$OUTPUT_FILE""_""$nranks"
    fi
    ASSERT="Assertion failed"
    DRC="Failed to drc_access credential"
    grep "$ASSERT" $LOG_FILE && { UNHAPPY="$UNHAPPY - $ASSERT";  }
    grep "$DRC" $LOG_FILE && { UNHAPPY="$UNHAPPY - $DRC"; }
    if [ ! -z "$UNHAPPY" ]; then
      echo "failed$UNHAPPY (logs in $LOG_FILE)"  
      exit -1
    else
      echo "passed"
    fi

  done
done

echo "============================================================================"


#
# Cleanup
#
for (( nranks=1; nranks<=$N_RANKS_MAX; nranks++ ))
do 
  rm -f "$INPUT_FILE""_""$nranks"
done


terminate ${PM_PID} 0 






# 
# Example:
#
# STAGER_CMD="mpirun ./librarian \
#               --max-time 10  \
#               --stage --layer PFS --access ../examples/librarian_pioneer_in"
# ARCHIVER_CMD="mpirun -n 2 ./librarian \
#               --max-dram 8 --max-time 10 \
#               --unstage --all-cdos --layer PFS --access ../examples/librarian_pioneer_out"


