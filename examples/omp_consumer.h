/* -*- mode:c -*- */
/** @file
 ** @brief openMP consumer component
 **/

/*
 * Copyright (C) 2018-2020 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


 #ifndef mstro_consumer
 #define mstro_consumer 1


 #define MSTRO_CONSUMER_SINK_ALL 0
 #define MSTRO_CONSUMER_ONE2ONE  1
 #define MSTRO_CONSUMER_ONE2TEN  2
 #define MSTRO_CONSUMER_ALL2ALL  3

 #include "maestro.h"
 #include "maestro/logging.h"
 #include <string.h>
 #include <unistd.h>
 #include <inttypes.h>

 /* simplify logging */
 #define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
 #define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
 #define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
 #define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)



mstro_status require_CDOs(mstro_cdo *cdos, size_t num_CDOs, int *injector_ids, int num_injectors, char **CDO_data, int64_t cdo_data_size);
mstro_status demand_CDOs(mstro_cdo *cdos, size_t num_CDOs);
void get_producers(int rank,int nConsumers, int *producers_ids, int num_producers, int consumer_mode);
int convert_consumer_mode(const char * consumer_mode_env);
int get_num_producers(int size, int nConsumers, int consumer_mode);
int get_consumer_mode(void);

#endif
