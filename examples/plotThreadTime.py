
##################################################################################
# Collect and plot performance data from raw input files                         # 
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################

import math
import numpy as np
import glob
import re
from matplotlib.font_manager import FontProperties
import plotHelper as helper
import arrayJobHelper as jobHelper


import sys
from optparse import OptionParser


import matplotlib


"""
Examples
#Sage

python3 plotThreadTime.py -f ../results/sage/local_PM/ --sys sage --fiProviders sockets --yrange 0,100 --nCDOThreads 1000
python3 plotThreadTime.py -f ../results/sage/local_PM/ --sys sage --fiProviders verbs --yrange 0,100 --nCDOThreads 1000

python3 plotThreadTime.py -f ../results/sage/PM_producers/ --sys sage --fiProviders sockets --multiNode 1,2,4,7 --nCDOThreads 25 --unit "s"
python3 plotThreadTime.py -f ../results/sage/PM_producers/ --sys sage --fiProviders verbs --multiNode 1,2,4,7  --nCDOThreads 25 --unit "s"

python3 plotThreadTime.py --nConsumers 1 --transport 0 -f ../results/PM_producers_consumers/ --sys sage --fiProviders verbs --multiNode 1,2,4,7 --nCDOThreads 15 --unit "ms"

"""


parser = OptionParser()

parser.add_option("-f", "--path", dest="target_path",
                  help="data file path", default=".",  metavar="FILE")


parser.add_option("--yrange", dest="yrange",
                  help="Range for Y-axis, e.g. 0,1000", default=-1)

parser.add_option("--ycut", dest="ycut",
                  help="Break Y-axis at these points, e.g. 100,200", default=-1)

parser.add_option("--sys", dest="sys",
                  help="System name", default="xeon")

parser.add_option("--ht", dest="ht",
                  help="Hyperthreading on/off", default=0)

parser.add_option("--inset", dest="inset",
                  help="inset axis left,bottom,width,height", default="-1")


parser.add_option("--fiProviders", dest="fiProvider",
                  help=" Fabric interface provider", default="sockets")

parser.add_option("--multiNode", dest="multiNode",
                  help="list of number of nodes, e.g. 1,2,4,8 ", default="-1")

parser.add_option("--nCDOThreads", dest="nCDOThreads",
                  help="Number of CDOs per thread", default="10000",  metavar="nCDOThreads")

parser.add_option("--nConsumers", dest="nConsumers",
                  help="Number of consumers", default="0",  metavar="nConsumers")

parser.add_option("--transport", dest="transport",
                  help="Which transport method used", default="0",  metavar="transport")

parser.add_option("--unit", dest="unit",
                  help="y-axis unit", default="us",  metavar="unit")


(options, args) = parser.parse_args()


if options.nConsumers == '0':
    #we are testing with different number and sizes of attrributes
    nAttributes = jobHelper.nAttributes
    sizeAttributes = jobHelper.sizeAttributes
    #we are not testing transport
    transport = jobHelper.transport[options.sys][int(options.transport)] #default
    sizeCDOs = [0]

else:
    #we are testing transport
    transport = jobHelper.transport[options.sys][int(options.transport)]
    sizeCDOs = jobHelper.sizeCDOs
    #number and size of attributes are not critical in this experiment
    nAttributes = [0]
    sizeAttributes = [0]


nThreads = jobHelper.calculatenThreads(options.sys, int(options.ht))

exp = "local_pool_"

if options.multiNode != "-1":
      exp = "multinode_PM_"
      nodes =  [int(x) for x in options.multiNode.split(',')]
      temp = []
      nThreads = [jobHelper.getMaxThreads(options.sys, ht=int(options.ht))]
      for t in nThreads:
            for n in nodes:
                  temp.append(t*n)
      nThreads = temp

print(nThreads)

if options.nConsumers != "0":
    exp = "PM_producers_consumers_"

#read all data
declare_data = helper.getDeclareTime(options.target_path, nThreads, nAttributes, sizeAttributes, int(options.nCDOThreads),options.fiProvider, options.unit, options.nConsumers, transport, sizeCDOs)


withdraw_data = helper.getWithdrawTime(options.target_path, nThreads, nAttributes, sizeAttributes, int(options.nCDOThreads), options.fiProvider, options.unit, options.nConsumers, transport, sizeCDOs)

if options.nConsumers != "0":
    demand_data = helper.getDemandTime(options.target_path, nThreads, nAttributes, sizeAttributes, int(options.nCDOThreads), options.fiProvider, options.unit, options.nConsumers, transport, sizeCDOs)
    demand_throughput, demand_data_throughput = helper.calculateThroughput(demand_data, nThreads, int(options.nCDOThreads), options.unit, sizeCDOs)


declare_throughput, temp = helper.calculateThroughput(declare_data, nThreads, int(options.nCDOThreads), options.unit, sizeCDOs)

declare_throughput_PM, temp = helper.calculateThroughput_PM(declare_data, nodes, options.unit, sizeCDOs)
#print(declare_throughput)





#############################################################################################
#                                      Plotting data                                        #
#############################################################################################

#helper.scaleDataMagnitude(declare_throughput)
if options.nConsumers != "0":
    #plot demand time
    helper.plotData(demand_data, nAttributes, sizeAttributes, nodes, options.target_path, "demand", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs, xaxis="nodes")
    #plot CDO demand throughput -- get the correct exponent for the data and scale the numbers accordingly
    scaled_demand, label = helper.scaleDataMagnitude(demand_throughput)
    helper.plotData(scaled_demand, nAttributes, sizeAttributes, nodes, options.target_path, "demand_CDO_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs, labelEXP=label,xaxis="nodes")
    helper.plotData(scaled_demand, nAttributes, sizeAttributes, nodes, options.target_path, "demand_CDO_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs, labelEXP=label, xaxis="nodes", log=True)


    #plot demand data bandwidth  -- get the correct exponent for the data and scale the numbers accordingly
    scaled_data_consumption, label = helper.scaleDataMagnitude(demand_data_throughput)
    helper.plotData(scaled_data_consumption, nAttributes, sizeAttributes, nodes, options.target_path, "data_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs, labelEXP=label, xaxis="nodes")
    helper.plotData(scaled_data_consumption, nAttributes, sizeAttributes, nodes, options.target_path, "data_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs, labelEXP=label, xaxis="nodes", log=True)
    print(scaled_data_consumption)

    scaledData, label = helper.scaleDataMagnitude(declare_throughput_PM)
    helper.plotData(scaledData, nAttributes, sizeAttributes, nodes, options.target_path, "PM_declare_CDO_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, -1, transport=transport, CDOs=sizeCDOs, labelEXP=label, xaxis="nodes")

else:
    #plot CDO declare times
    helper.plotData(declare_data, nAttributes, sizeAttributes, nThreads, options.target_path, "declare", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs)
    helper.plotData(declare_data, nAttributes, sizeAttributes, nodes, options.target_path, "declare_nodes", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, options.yrange, transport=transport, CDOs=sizeCDOs, xaxis="nodes")
    #print(declare_data)

    #plot throughput
    #plot CDO declare throughput -- get the correct exponent for the data and scale the numbers accordingly
    scaledData, label = helper.scaleDataMagnitude(declare_throughput)
    helper.plotData(scaledData, nAttributes, sizeAttributes, nThreads, options.target_path, "declare_CDO_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, -1, transport=transport, CDOs=sizeCDOs, labelEXP=label)

    scaledData, label = helper.scaleDataMagnitude(declare_throughput_PM)
    helper.plotData(scaledData, nAttributes, sizeAttributes, nodes, options.target_path, "PM_declare_CDO_throughput", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, -1, transport=transport, CDOs=sizeCDOs, labelEXP=label, xaxis="nodes")


    helper.plotData(withdraw_data, nAttributes, sizeAttributes, nThreads, options.target_path, "withdraw", int(options.nCDOThreads), options.sys, options.fiProvider, exp, options.unit, "0,10", transport=transport, CDOs=sizeCDOs)
