##################################################################################
#Run core benchmark experiment campaign as slurm array jobs                      #
##################################################################################
#                                                                                #
##################################################################################
#                                                                                #
# Copyright (C) 2018-2020 Cray Computer GmbH                                     #
# Copyright (C) 2021 HPE Switzerland GmbH                                        #
#                                                                                #
# Redistribution and use in source and binary forms, with or without             #
# modification, are permitted provided that the following conditions are         #
# met:                                                                           #
#                                                                                #
# 1. Redistributions of source code must retain the above copyright              #
#    notice, this list of conditions and the following disclaimer.               #
#                                                                                #
# 2. Redistributions in binary form must reproduce the above copyright           #
#    notice, this list of conditions and the following disclaimer in the         #
#    documentation and/or other materials provided with the distribution.        #
#                                                                                #
# 3. Neither the name of the copyright holder nor the names of its               #
#    contributors may be used to endorse or promote products derived from        #
#    this software without specific prior written permission.                    #
#                                                                                #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS        #
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED          #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT             #
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,         #
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT               #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,          #
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY          #
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT            #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE          #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           #
#                                                                                #
#                                                                                #
##################################################################################




"""
consumerModes:
 "sinkall" : a consumer rank will consume (require and demand) all cdos declared by producer ranks 
 "one2one" : #consumer should be equal to #producers. 
 "one2ten" : every consumer will consume cdos from 10 producers. #producers should be 10 * #consumers
 "all2all" : All consumers will consume all cdos declared from all producers. similar to sinkall mode with multiple consumers. 

nCDOThreads: number of cdos declared per producer thread. 

-f path to core benchmark executable

--app job script file name and job name in slurm.

-N number of nodes

--ranks number of mpi ranks (#producers + #consumers + pm)

-o output path 

--sys system name, puts some system specific configurations in the scripts. Current options: sage 

--ht enable hyperthreading

-- mstroPath path to the installed maestro library.

Example:

python3 arrayJobGen.py --nConsumers 1 --consumerMode sinkall --nCDOThreads 10  -f /maestro-core/examples/core_bench --app benchmark -N 10  --ranks 10 -o ../results/PM_producers_consumers/  --sys pinot --ht 0 --nJobs 1 --mstroPath path_to_maestro_lib

"""

#nConsumers and consumerMode are important inputs for transport tests

import os
from optparse import OptionParser
import arrayJobHelper as helper


parser = OptionParser()
parser.add_option("-f", "--file", dest="exPath",
                  help="data file path", default=".",  metavar="FILE")

parser.add_option("-N", dest="nodes",
                  help="number of nodes", default=1,  metavar="N")

parser.add_option("--app", dest="app",
                  help="app name", default=8,  metavar="APP")

parser.add_option("--ranks", dest="ranks",
                  help="Number of RANK", default=1,  metavar="NUMBEROFRANKS")

parser.add_option("-t", dest="threads",
                  help="threads per rank", default=20,  metavar="THREAD")

parser.add_option("-o", dest="outPath",
                  help="Output folder path", default=".",  metavar="OUTPUT")


parser.add_option("--ht", dest="ht",
                  help="enables/disbales hyperthreading", default="0",  metavar="HT")

parser.add_option("--sys", dest="sys",
                  help="system name", default="sage",  metavar="Partition")

parser.add_option("--nJobs", dest="nJobs",
                  help="Number of concurrent jobs", default="100",  metavar="JOBLIMIT")

parser.add_option("--nCDOThreads", dest="nCDOThreads",
                  help="Number of CDOs per thread", default=25,  metavar="nCDOThreads")


parser.add_option("--mstroPath", dest="mstroPath",
                  help="Path to Maestro install directory", default=".",  metavar="mstroPath")

parser.add_option("--nConsumers", dest="nConsumers",
                  help="Number of consumer ranks", default="0",  metavar="nConsumers")

parser.add_option("--consumerMode", dest="consumerMode",
                  help="sinkall, one2one, one2ten, or all2all", default="sinkall",  metavar="consumerMode")

parser.add_option("--jobTime", dest="jobTime",
                  help="Time limit for a single job, please increase in Piz Daint", default="1:30:00",  metavar="jobtime")


(options, args) = parser.parse_args()

repeats = [0]


nExperiments = 0

#base file name
filename = 'job_'+options.app


#if using more than one node, only use the maximum num of threads ...otherwise, scan through all possible thread counts within node.
if int(options.nodes) > 1:
      nThreads = [helper.getMaxThreads(options.sys, ht=int(options.ht))]

else:
      nThreads = helper.calculatenThreads(options.sys, ht=int(options.ht))
#
fabrics = helper.fiProviders[options.sys]

if int(options.nConsumers) == 0:
    transport = [helper.transport[options.sys][0]] #default
    nAttributes = helper.nAttributes
    sizeAttributes = helper.sizeAttributes
    sizeCDOs = [0]
else:
    transport = helper.transport[options.sys]
    nAttributes = [0]
    sizeAttributes = [0]
    sizeCDOs = helper.sizeCDOs

nProducers = 1
# if we are using more than one node, then
if int(options.nodes) > 1:
      nProducers = int(options.nodes) - 1 - int(options.nConsumers)


#number of producer threads
nProducers = [i * nProducers for i in nThreads]

# commands file to run the application with the correct parameters
#maybe loop over the transport methods from here
nExperiments += helper.createCommandFile(filename, nAttributes, sizeAttributes, nThreads, options.exPath, repeats, fabrics, int(options.nConsumers), sizeCDOs, transport, int(options.nCDOThreads))



print("#experiments = " + str(nExperiments))

#output file names to redirect the output to the correct file
nExperiments = helper.createOutputfilenamesFile(filename, nAttributes, sizeAttributes, nProducers, repeats, options.outPath, int(options.nCDOThreads), fabrics, int(options.nConsumers), sizeCDOs, transport)


#export file to export env ?? needed for the FI_PROVIDER=sockets or others and transport layer
nExperiments = helper.createExportFile(filename, nAttributes, sizeAttributes, nThreads, repeats, fabrics, transport, sizeCDOs, filemode="w")

# batch array job file
#please be aware to increase the job time on slower systems
helper.createSlurmBatchJob(filename, options.app, int(options.nodes), int(options.ranks), nExperiments, options.sys, options.mstroPath, options.consumerMode, ht=int(options.ht), jobLimit=int(options.nJobs), time=options.jobTime, filemode="w")



#sbatch filename
# os.system("sbatch "+filename)
