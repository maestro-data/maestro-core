/* Stage and unstage CDOs, using command-line attributes
*/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro.h"
#include "ketopt.h"
#include "utlist.h"
#include "maestro/logging.h"

#include <mamba.h>
#include <mmb_memory.h>

#include "mpi.h"

#include <string.h> 
#include <strings.h> 
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sched.h>
#include <inttypes.h>

/* simplify logging */
#define NOISE(...)   LOG_NOISE(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)

MPI_Comm g_comm;

const mstro_nanosec_t  DEFAULT_MAX_WAIT = ((mstro_nanosec_t)15)*NSEC_PER_SEC; /* 15s */
mstro_nanosec_t g_last_order_time = 0;
mstro_nanosec_t g_startup_time = 0;

/** Configurable settings */

char *g_conf_workflow_name = NULL;
char *g_conf_component_name = NULL;
char* g_pm_info = NULL;

/** component name after whose BYE event we should terminate (default: none) */
char *g_conf_terminate_after = NULL;
/** the appid for this component, as observed during its WELCOME */
mstro_app_id g_conf_terminate_after_appid = 0;
/** timeout after which to terminate, if no events are observed anymore (default: DEFAULT_MAX_WAIT) */
/* subscription to monitor potential leave-induced termination */
mstro_subscription g_leave_subscription=NULL;

#define CDO_READY_NAME "archiver_ready"
mstro_cdo g_cdo_ready=NULL;

int g_am_master = 0;
int g_mpi_rank;
int g_mpi_size;

mstro_nanosec_t g_conf_max_idle_wait = -1;
mstro_nanosec_t g_conf_max_wait = -1;

int g_all_to_all = 0;

uint64_t g_max_dram = (uint64_t)8000*(uint64_t)1000000;
#define MSTRO_LIBRARIAN_MAX_PARTICIPANTS 1024 // We need a limit, and the librarian is not supposed to be large
mmbMemInterface* g_mempool_interface = NULL; 

int g_return_code = EXIT_SUCCESS;

#define LOGDST_STDOUT 1
#define LOGDST_STDERR 2
#define LOGDST_MAESTRO 3
#define LOGDST_SYSLOG 4
/** logging destination */
int g_conf_logdst = LOGDST_STDOUT;
/** verbose operation */
bool g_verbose = true;

typedef enum {
  LIBRARIAN_ORDER_LAYER_INVALID,
  LIBRARIAN_ORDER_LAYER_PFS,
  LIBRARIAN_ORDER_LAYER_DRAM
} order_stage;

typedef enum {
  LIBRARIAN_ARCHIVE_PHASE_INVALID,
  LIBRARIAN_ARCHIVE_PHASE_REQUIRE,
  LIBRARIAN_ARCHIVE_PHASE_DEMAND
} order_phase;

enum {
  LIBRARIAN_ORDER_INVALID,
  LIBRARIAN_ORDER_STAGE,
  LIBRARIAN_ORDER_ARCHIVE
} order_layer;


/* Raw order as given by the user */
struct order {
  int stage; 
  char* pattern; // selector string pattern
  int layer; 
  union {
    char* path; 
  };
  // XXX could have a "rank" field here, for specific staging?
  int reoffer;
  mstro_subscription subscription_seal; // in case of archiving
  mstro_subscription subscription_offer; // in case of archiving
  struct order* next;
};
struct order* g_order_list = NULL;
pthread_mutex_t g_order_list_mutex = PTHREAD_MUTEX_INITIALIZER; 

/* to signal comm_thread it's time to cleanup */
int g_end_communication = 0;

struct cdo_required;
/* Order communicated by the dispatcher (master rank) */
struct approved_order {
  char* cdo_name;
  int stage; // 0:undef 1:stage 2:archive 
  int phase; // [archive stage] 0: require 1:demand
  int is_distributed; 
  int n_participants;
  uint64_t total_size; // useful in the distributed case
  mmbLayout* layout;
  int layer; // 0:undef 1:PFS 2:DRAM
  union {
    char* path; 
  };
  int reoffer;
  mstro_subscription subscription_seal; // in case of archiving
  mstro_subscription subscription_offer; // in case of archiving
  int worker; // which rank is in charge
  struct cdo_required* req;

  struct approved_order* next;
};
struct approved_order* g_approved_orders = NULL;
pthread_mutex_t g_approved_orders_mutex = PTHREAD_MUTEX_INITIALIZER; 


#define WITH_LOCKED_LIST(body, mutex) \
  do {                                                                  \
  int wlr_stat=pthread_mutex_lock(&mutex);                              \
  if(wlr_stat!=0) {                                                     \
    ERR("Failed to lock orders list: %d (%s)\n",                        \
        wlr_stat, strerror(wlr_stat));                                  \
    abort();                                                            \
  }                                                                     \
                                                                        \
  do {                                                                  \
    body;                                                               \
  } while(0);                                                           \
                                                                        \
  wlr_stat=pthread_mutex_unlock(&mutex);                                \
  if(wlr_stat!=0) {                                                     \
    ERR("Failed to unlock orders list: %d (%s)\n",                      \
        wlr_stat, strerror(wlr_stat));                                  \
    abort();                                                            \
  }                                                                     \
  } while(0)

#define DEFAULT_AORDER_BUF_SIZE 1024 
#define DEFAULT_SELECTOR_SIZE   1024


/* Info on CDO we plan to archive and management. We listen to SEAL events to
 * get info of distribution as well, save it here in that list, and then
 * REQUIRE/REVOKE the distributed CDO. At OFFER time, work will be dispatched
 * among archiver processes */
struct cdo_required {
  int offered; // if one cdo offer was already received (distributed CDO implies several offers)
  mstro_cdo cdo;
  char* cdo_name; // we don't have the handle if another rank handles it alone, but the name will do
  mmbAllocation* mal; // for cleanup, can't set it to *cdo* because unaligned
  void* aligned_ptr; /* mmbAllocation is attached to *cdo* but unaligned */
  int is_distributed;
  uint64_t total_size;
  mmbLayout* layout;
  int n_senders;

/* need to keep track of individual SEALs to accomodate
multiple CDO occurences */
  int count_seals;
  int* sender_seals;   
  int count_offers;
  int* sender_offers;

  struct order* order;

/* Who is assigned on it */
  int worker;
  int n_participants;
/* Needed by master to forward ACK to PM*/
  MPI_Request mpir[MSTRO_LIBRARIAN_MAX_PARTICIPANTS];
  int needs_ack[MSTRO_LIBRARIAN_MAX_PARTICIPANTS];
  int tested;
  mstro_pool_event e[MSTRO_LIBRARIAN_MAX_PARTICIPANTS]; // events to be ack_single'd
  int ecount; // *e* is going to be filled up in no particular order
  struct cdo_required* next;
};
struct cdo_required* g_required_cdos = NULL;
pthread_mutex_t g_required_cdos_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t g_required_cdos_cond = PTHREAD_COND_INITIALIZER;

/* List of CDOs held by the librarian, according to reoffered flag specified
 * on command line. Archiving in DRAM layer automatically implies reoffer */
struct cdo_cont {
  mstro_cdo cdo;
  struct cdo_required* req;
  struct cdo_cont* next;
};
struct cdo_cont* g_reoffered_cdos = NULL;
pthread_mutex_t g_reoffered_cdos_mutex = PTHREAD_MUTEX_INITIALIZER;

void
mstro_librarian_exit(int err)
{
  ERR("Attempting a fast and clean exit\n");
  /* artificially end both thread when appropriate */
  g_end_communication = 1;
  g_conf_max_wait = 0;
  g_return_code = err;
}

#define MSTRO_SAFE_CALL(call)                                           \
  {                                                                     \
    mstro_status MSCs = call;                                           \
    if (MSCs!=MSTRO_OK) {                                               \
      ERR("Maestro returned with error `%s` at %s:%d\n",                \
             mstro_status_description(MSCs), __FILE__, __LINE__);       \
      mstro_librarian_exit(MSCs);                                       \
    }                                                                   \
  }

#define MMB_SAFE_CALL(call)                                             \
  {                                                                     \
    mmbError MMBs = call;                                               \
    if (MMBs!=MMB_OK) {                                                 \
      ERR("Mamba returned with error `%s` at %s:%d\n",                  \
             mmb_error_description(MMBs), __FILE__, __LINE__);          \
      mstro_librarian_exit(MMBs);                                       \
    }                                                                   \
  }

#define MPI_SAFE_CALL(call)                                             \
  {                                                                     \
    int MPIs = call;                                                    \
    int resultlen;                                                      \
    char err_buffer[MPI_MAX_ERROR_STRING];                              \
    if (MPIs!=MPI_SUCCESS) {                                            \
      MPI_Error_string(ierr,err_buffer,&resultlen);                     \
      ERR("MPI returned with error `%s` at %s:%d\n",                    \
             err_buffer, __FILE__, __LINE__);                           \
      mstro_librarian_exit(MPIs);                                       \
    }                                                                   \
  }



/* checking consistency of command-line orders */
int
is_order_empty(struct order* tmp)
{
  int empty = 0;
  if (  tmp->stage <= 0 
     || tmp->pattern == 0 
     || tmp->layer <= 0
     || (tmp->layer == LIBRARIAN_ORDER_LAYER_PFS && tmp->path == 0) ) {
      empty = 1;
  }
  return empty;
}

int
is_order_complete(struct order* tmp)
{
DEBUG("Order complete? stage=%d \tlayer=%d \tpattern=%s \tpath=%s\n", tmp->stage, tmp->layer, tmp->pattern, tmp->path);
  int complete = 0;
  int given_access = 0;
  if (tmp == NULL) { 
    ERR("order struct is null, can't determine if complete\n");
    return 0;
  }
  if (  tmp->stage > 0 
     && tmp->layer > 0) {
     if (tmp->stage == LIBRARIAN_ORDER_ARCHIVE && tmp->pattern == NULL) {
       ERR("Unspecified pattern\n");
       exit(EXIT_FAILURE);
     }
     switch(tmp->layer) {
       case LIBRARIAN_ORDER_LAYER_PFS:
         if (tmp->path != NULL) {
           given_access = 1;
           if (tmp->stage == LIBRARIAN_ORDER_STAGE) {
             if(access(tmp->path, F_OK|R_OK)) {
              ERR("Failed to access given path `%s` planned to be staged\n", tmp->path);
              mstro_librarian_exit(EXIT_FAILURE);
             }
           }
         }
         break;
       case LIBRARIAN_ORDER_LAYER_DRAM:
          // no access to give
         if (tmp->path != NULL) {
           ERR("DRAM layer is not yet accepting specific access method\n");
           exit(EXIT_FAILURE);
         }
         given_access = 1;
         break;
       default:
         ERR("Unknown layer\n");
         exit(EXIT_FAILURE);
     }
     if (given_access)
       complete = 1;
  }  
  return complete;
}

void
print_usage(const char *argv0)
{
  fprintf(stderr, "Usage: %s [OPTIONS] [ORDER] ...\n", argv0);
  fprintf(stderr, "Note: pm-info, workflow name, and component name MUST all be specified, either via env (has precedence) or command line (see below)\n");
  fprintf(stderr, " Atomic ORDER: [-s|-u] [-a|-p PATTERN] -l LAYER -m ACCESS\n");
  fprintf(stderr, "  ORDER:\n");
  fprintf(stderr,
          "  --stage, -s                This order will stage CDOs\n"
          "  --unstage, -u              This order will unstage CDOs\n"
          "  --pattern, -p PATTERN      Selector pattern (query) string, inapplicable to staging\n"
          "  --all-cdos, -a             Pattern will be \"*\", inapplicable to staging \n"
          "  --layer, -l LAYER          [PFS|DRAM] for now\n"
          "  --access, -m METHOD        Access method for the given layer. A path STRING for PFS access\n"
          "  --reoffer, -r              Reoffer archived CDOs\n"
          "  --all2all, -d         Use all archiver ranks for each transfer\n");

  fprintf(stderr, "  Note: layer parameter must be specified before access parameter.\n");
  fprintf(stderr, "  Atomic ORDERs can be chained.\n");
  fprintf(stderr, " Initialization OPTIONS:\n");
  fprintf(stderr, " (environment variables that are used as default in {curly brackets})\n");
  fprintf(stderr,
          " --workflow NAME, -w NAME         Maestro Workflow name to attach to {MSTRO_WORKFLOW_NAME}\n"
          " --component NAME, -c NAME        Maestro component name for this program {MSTRO_COMPONENT_NAME}\n"
          " --terminate-after NAME, -q NAME  Terminate after observing LEAVE of component NAME\n"
          " --max-idle SECONDS, -i SECONDS   Terminate after no events for SECONDS (default forever)\n"
          " --max-time SECONDS, -t SECONDS   Terminate after SECONDS (default forever)\n"
          " --max-dram BYTES, -b BYTES       Max per process single CDO size allowed (bytes)\n"
          " --pm-info PMINFO, -P PMINFO      Pool Manager OOB info {MSTRO_POOL_MANAGER_INFO}\n"
          " --help, -h                       This help\n"
          " --verbose, -v                    Verbose output\n"
          " --version, -V                    Version information\n");
}

/* Rule: env overrides cmd line */
void solve_conflict_env_cmd(char** cmd, const char* env, const char* desc) {
  char* tmp = getenv(env);
  if (*cmd == NULL) {
    if (tmp == NULL) {
      fprintf(stderr, "Unspecified %s\n", desc);
      print_usage(NULL);
      exit(EXIT_FAILURE);
    } else {
//      fprintf(stderr, "Using env (%s=%s)\n", env, tmp);
      *cmd = tmp;
    }
  } else {
    if (tmp != NULL) {
      free(*cmd);
      *cmd = tmp;
    }
  }
}

mstro_status
parse_arguments(int argc, char **argv)
{
  static ko_longopt_t longopts[] = {
    { "workflow",        ko_required_argument,       'w' },
    { "component",       ko_required_argument,       'c' },
    { "stage",           ko_no_argument,             's' },
    { "unstage",         ko_no_argument,             'u' },
    { "archive",         ko_no_argument,             'u' },
    { "all-cdos",        ko_no_argument,             'a' },
    { "pattern",         ko_required_argument,       'p' },
    { "selector",        ko_required_argument,       'p' },
    { "layer",           ko_required_argument,       'l' },
    { "access",          ko_required_argument,       'm' },
    { "reoffer",         ko_no_argument,             'r' },
    { "all2all",         ko_no_argument,             'd' },

    { "terminate-after", ko_required_argument,       'q' },
    { "max-idle",        ko_required_argument,       'i' },
    { "max-time",        ko_required_argument,       't' },
    { "max-dram",        ko_required_argument,       'b' },
    { "pm-info",         ko_required_argument,       'P' },
    { "pm_info",         ko_required_argument,       'P' },
    { "help",            ko_no_argument,             'h' },
    { "verbose",         ko_no_argument,             'v' },
    { "version",         ko_no_argument,             'V' },
    { NULL, 0, 0 }
  };
  ketopt_t opt = KETOPT_INIT;
  int c;

  struct order* tmp;
  tmp = calloc(1,sizeof(struct order));  
  while ((c = ketopt(&opt, argc, argv, 1, "w:c:suap:l:m:rd"
                                          "q:i:t:b:P:hvV", longopts)) >= 0) {
    switch(c) {
      case 's':
        if (tmp->stage > 0) {
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        tmp->stage = 1;
        goto ADD_ORDER;
      case 'u':
        if (tmp->stage > 0) {
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        tmp->stage = 2;
        goto ADD_ORDER;
      case 'p':
        if (tmp->pattern != 0)  {
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
// FIXME either ketopt or I don't understand single quote interpretation, can't do a full pattern at the moment, will say it's just a name
        tmp->pattern = calloc(1,sizeof(char)*DEFAULT_SELECTOR_SIZE);
        sprintf(tmp->pattern, "\(.maestro.core.cdo.name = \"%s\")",opt.arg);
        goto ADD_ORDER;
      case 'a':
        if (tmp->pattern != 0)  {
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        tmp->pattern = "(has .maestro.core.cdo.name)" ;
        goto ADD_ORDER;
      case 'l':
        if (tmp->layer > 0)  {
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        if (strcasecmp("PFS", opt.arg) && strcasecmp("DRAM", opt.arg)) {
          fprintf(stderr, "Unknown layer: %s\n", opt.arg);
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        if (!strcasecmp("PFS", opt.arg))
          tmp->layer = LIBRARIAN_ORDER_LAYER_PFS;
        if (!strcasecmp("DRAM", opt.arg))
          tmp->layer = LIBRARIAN_ORDER_LAYER_DRAM;
        goto ADD_ORDER;
      case 'm':
        if (tmp->layer <= 0)  {
          fprintf(stderr, "Attention: layer must be specified before access parameter\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        switch(tmp->layer) {
          case LIBRARIAN_ORDER_LAYER_PFS:
            if (tmp->path != 0) {
              print_usage(argv[0]);
              exit(EXIT_FAILURE);
            }
                
            tmp->path = opt.arg;
            goto ADD_ORDER;
          case LIBRARIAN_ORDER_LAYER_DRAM:
           // do nothing
            break;
          default:
            fprintf(stderr, "Unknown layer\n");
            exit(EXIT_FAILURE);
        }
        break;

ADD_ORDER:
        if (is_order_complete(tmp)) {
DEBUG("Adding order\n");
          LL_APPEND(g_order_list, tmp);
          tmp = calloc(1,sizeof(struct order));  
          if (tmp == NULL) {
            fprintf(stderr, "No memory available\n");
            exit(EXIT_FAILURE);
          } 
        }
        break;

      case 'w':
        if(!opt.arg) {
          fprintf(stderr, "--workflow option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_workflow_name = opt.arg;
        break;
      case 'c':
        if(!opt.arg) {
          fprintf(stderr, "--component option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_component_name = opt.arg;
        break;
      case 'r':
        tmp->reoffer = 1;
        break;
      case 'd':
        g_all_to_all = 1;
        break;
      case 'q':
        if(!opt.arg) {
          fprintf(stderr, "--terminate-after option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_terminate_after = opt.arg;
        break;
      case 'i':
        if(!opt.arg) {
          fprintf(stderr, "--max-idle option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_max_idle_wait = atoi(opt.arg)* NSEC_PER_SEC;
        break;
      case 't':
        if(!opt.arg) {
          fprintf(stderr, "--max-time option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_max_wait = atoi(opt.arg)* NSEC_PER_SEC;
        break;
      case 'b':
        if(!opt.arg) {
          fprintf(stderr, "--max-dram option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        char* sdram = opt.arg;
        size_t val = atoi(opt.arg); 
        char* fac[3] = {"K", "M", "G"};
        size_t factmp = 1;
        int i;
        for (i=0; i<3; i++) {
          factmp*=1000;
          char* pos = strcasestr(sdram,fac[i]);
          if (pos == 0x0) continue;
          size_t len = pos - sdram;  
          if (len > 0) {
            char* stmp = malloc(sizeof(char)*(len+1));
            strncpy(stmp, sdram, len); 
	    stmp[len] = '\0'; /*make sure stmp is null terminated*/
            val = atoi(stmp) * factmp; 
            free(stmp);
          }
        }
        g_max_dram = val;
        break;
      case 'P': 
        if(!opt.arg) {
          fprintf(stderr, "--pm-info (also -P or --pm_info) option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_pm_info = opt.arg;
        break;
      case 'v':
        g_verbose = true;
        break;
      case 'h':
        print_usage(argv[0]);
        exit(EXIT_SUCCESS);
      case 'V':
        fprintf(stdout, "%s, part of %s\n", argv[0], mstro_version());
        exit(EXIT_SUCCESS);
        break;
      default:
        fprintf(stderr, "Unrecognized option: %c\n", opt.opt? opt.opt : ' ');
        print_usage(argv[0]);
        return MSTRO_FAIL;
    }
  }

  if (!is_order_empty(tmp) && !is_order_complete(tmp)) {
    fprintf(stderr, "Latest order is incomplete\n");
    print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  solve_conflict_env_cmd(&g_conf_workflow_name, "MSTRO_WORKFLOW_NAME", "workflow name");
  solve_conflict_env_cmd(&g_conf_component_name, "MSTRO_COMPONENT_NAME", "component name");
  solve_conflict_env_cmd(&g_pm_info, "MSTRO_POOL_MANAGER_INFO", "pm-info");

  if(opt.ind<argc) {
    fprintf(stderr, "Unexpected non-option arguments: %s (...)\n", argv[opt.ind]);
    print_usage(argv[0]);
    return MSTRO_FAIL;
  }

  /** try to reduce logging, unless user set something */
  if(getenv(MSTRO_ENV_LOG_LEVEL)==NULL) {
      setenv(MSTRO_ENV_LOG_LEVEL, "err", 1);
  } 
  else {
      ;/* well, then try without reduced logging */ 
  }

  if(g_verbose) {
    fprintf(stderr, "Configuration: %s/%s/%" PRInanosec "/%d\n",
            g_conf_workflow_name, g_conf_component_name,
            g_conf_max_wait, g_conf_logdst);
  }
  return MSTRO_OK;
}

mstro_status
mstro_librarian_check_archiver_ready()
{
  MSTRO_SAFE_CALL(mstro_cdo_declare(CDO_READY_NAME, MSTRO_ATTR_DEFAULT, &g_cdo_ready));
  MSTRO_SAFE_CALL(mstro_cdo_require(g_cdo_ready));
  MSTRO_SAFE_CALL(mstro_cdo_demand(g_cdo_ready));
  MSTRO_SAFE_CALL(mstro_cdo_dispose(g_cdo_ready));
  g_cdo_ready=NULL;

  return MSTRO_OK;
}

mstro_status
mstro_librarian_notify_archiver_ready()
{
  MSTRO_SAFE_CALL(mstro_cdo_declare(CDO_READY_NAME, MSTRO_ATTR_DEFAULT, &g_cdo_ready));
  MSTRO_SAFE_CALL(mstro_cdo_offer(g_cdo_ready));

  return MSTRO_OK;
}

/* Ensure the archiver ready to receive work */
mstro_status
mstro_librarian_workflow_sync(void)
{
  int stage_only = 0;
  int unstage_only = 0;
  struct order* tmp;
  LL_FOREACH(g_order_list, tmp) {
    if (tmp->stage == LIBRARIAN_ORDER_ARCHIVE) {
      unstage_only = 1;
    } else {
      stage_only = 1;
    }
  }

  if (stage_only && unstage_only) {
    WARN("Unimplemented sync for mixed stage and unstage orders\n");
  } else if (unstage_only) {
    if (g_am_master) {
      MSTRO_SAFE_CALL(mstro_librarian_notify_archiver_ready());
    } else {
      MSTRO_SAFE_CALL(mstro_librarian_check_archiver_ready());
    }
  } else if (stage_only) {
    MSTRO_SAFE_CALL(mstro_librarian_check_archiver_ready());
  } else ; // nothing to do anyway

  MPI_Barrier(g_comm); // Make sure other ranks are ready to communicate

  return MSTRO_OK;
}

mstro_status
batch_subscribe(void)
{
  mstro_status s;
/* subscribe to LEAVE events if we are waiting for someone */
  if (g_conf_terminate_after != NULL)
    MSTRO_SAFE_CALL(mstro_subscribe(NULL, MSTRO_POOL_EVENT_APP_LEAVE, MSTRO_SUBSCRIPTION_OPTS_DEFAULT,
                            &g_leave_subscription));

/* Preparing for archiving orders */
  struct order* tmp;
  LL_FOREACH(g_order_list, tmp) {
    if (tmp->stage == LIBRARIAN_ORDER_ARCHIVE) {
      DEBUG("Subscribing to SEAL and OFFERs matching `%s`\n", tmp->pattern);
      mstro_cdo_selector tmp_selector=NULL; 
      MSTRO_SAFE_CALL(mstro_cdo_selector_create(NULL, NULL, tmp->pattern, &tmp_selector));
      MSTRO_SAFE_CALL(mstro_subscribe(tmp_selector, MSTRO_POOL_EVENT_SEAL, 
                           MSTRO_SUBSCRIPTION_OPTS_REQUIRE_ACK|
                           MSTRO_SUBSCRIPTION_OPTS_NO_LOCAL_EVENTS,
                           &(tmp->subscription_seal)));
      MSTRO_SAFE_CALL(mstro_subscribe(tmp_selector, MSTRO_POOL_EVENT_OFFER,
                           MSTRO_SUBSCRIPTION_OPTS_SIGNAL_BEFORE|
                           MSTRO_SUBSCRIPTION_OPTS_NO_LOCAL_EVENTS, 
                           &(tmp->subscription_offer)));
    }
  }

  mstro_librarian_workflow_sync();
 
  return MSTRO_OK;
}

mstro_status
subscriptions_cleanup()
{
  mstro_status s = MSTRO_OK;
  struct order* tmp;
  LL_FOREACH(g_order_list, tmp) {
    if (tmp->stage == LIBRARIAN_ORDER_ARCHIVE) { // archiving order
      MSTRO_SAFE_CALL(mstro_subscription_dispose(tmp->subscription_seal));
      MSTRO_SAFE_CALL(mstro_subscription_dispose(tmp->subscription_offer));
    }
  }
 
  return MSTRO_OK;
}

void
orders_cleanup(void)
{
struct order* tmp, *foo;
  WITH_LOCKED_LIST(
    LL_FOREACH_SAFE(g_order_list, tmp, foo) {
      LL_DELETE(g_order_list, tmp);
      free(tmp);
    }
  , g_order_list_mutex);
}

void
ao_cleanup(struct approved_order* tmp)
{
  if (tmp == NULL)
    return;
  free(tmp->cdo_name);
  switch(tmp->layer) {
    case LIBRARIAN_ORDER_LAYER_PFS: 
      free(tmp->path);
      break;
    case LIBRARIAN_ORDER_LAYER_DRAM:
      // nothing todo 
      break;
    default:
      fprintf(stderr, "Unknown layer\n");
      exit(EXIT_FAILURE);
  }

 free(tmp);
}

int 
is_time_to_go(void) 
{
  int go = 0;

  if (g_conf_terminate_after != NULL) {
  /* Wait for component and/or we reached Timeout */
      mstro_pool_event e=NULL;
      MSTRO_SAFE_CALL(mstro_subscription_poll(
                    g_leave_subscription, &e));
      mstro_pool_event tmp=e;
      while(tmp) {
        switch(tmp->kind) {
          case MSTRO_POOL_EVENT_APP_LEAVE:
            if (!strcmp(tmp->payload.leave.component_name,g_conf_terminate_after)) {
              go = 1;
              g_last_order_time = mstro_clock(); // no watch watch, already terminating
              ERR("Terminating because `%s` left.\n", tmp->payload.leave.component_name);
            }
            break;
          default:
            ERR("Unexpected event %d\n", tmp->kind);
        }
        tmp=tmp->next;
      }
      MSTRO_SAFE_CALL(mstro_pool_event_dispose(e));
  } else if ((mstro_clock() - g_startup_time) > g_conf_max_wait) {
        if (g_conf_max_wait == -1) {
          go = 0;
        } else {
          INFO("Terminating because timeout (waited > %llus) or failure\n", g_conf_max_wait/NSEC_PER_SEC);
          go = 1;
        }
  } else if (mstro_clock() - g_last_order_time > g_conf_max_idle_wait) {
      if (g_conf_max_idle_wait == -1) {
          go = 0;
      } else {
        INFO("Terminating because idle timeout (waited > %llus)\n", g_conf_max_idle_wait/NSEC_PER_SEC); 
        go = 1;
      }
  } else if (g_order_list == NULL &&
      g_approved_orders == NULL) {
    INFO("Terminating because no more work.\n");
    go = 1;
  } else ;

  if (go)
    g_end_communication = 1;

  return go;
}

void 
compute_fair_lengths_distribution(size_t* lengths, size_t n_participants, uint64_t total_size) {
  assert(n_participants>=1 && n_participants<=g_mpi_size);
  size_t i;
  for (i=0; i<n_participants; i++) {
    lengths[i] = total_size / n_participants;
  }
  lengths[i-1] += total_size % n_participants;

}


mstro_status
transport_unstage(mstro_cdo cdo, struct approved_order* ao)
{
  if (cdo == NULL || ao == NULL)
    return MSTRO_INVARG;
INFO("Archiving CDO `%s` into %s\n", mstro_cdo_name(cdo), ao->path);

  int64_t sz;
  void* data;
  MSTRO_SAFE_CALL(mstro_cdo_access_ptr(cdo, &data, &sz));


  switch (ao->layer) { 
    case LIBRARIAN_ORDER_LAYER_PFS:
      if (ao->is_distributed == 0) {
        FILE *f = NULL;
        f = fopen(ao->path,"w");
    	if (f == NULL) {
  		ERR("Failed to open %s for GFS transport (errno: %d -- %s)\n",
                      ao->path, errno, strerror(errno));
  	    return MSTRO_FAIL;
  	}
         
        RETRY_GFS_TRANSPORT_WRITE: ;
        size_t bytes_written = fwrite(data, sizeof(char), sz, f);
  	if (bytes_written != sz) {
            if (errno == EAGAIN)
  	    goto RETRY_GFS_TRANSPORT_WRITE;
  	  ERR("Partial write on %s (buf: %p) for GFS transport (errno: %d -- %s)\n",
                      ao->path, data, errno, strerror(errno));
  	  return MSTRO_FAIL;
  	}
  	if (fclose(f) != 0) {
            ERR("Failed to close %s for GFS transport (errno: %d -- %s)\n",
              ao->path, errno, strerror(errno));
  	  return MSTRO_FAIL;
  	}
      } else {
         MPI_File fh;
         int err;
         MPI_Group world_group;
         MPI_Comm_group(g_comm, &world_group);
         int* ranks = malloc(sizeof(int) * ao->n_participants);
         for (int i=0; i<ao->n_participants; i++)
           ranks[i] = i;
         MPI_Group group;
         MPI_Group_incl(world_group, ao->n_participants, ranks, &group);
         MPI_Comm group_comm;
         MPI_Comm_create_group(g_comm, group, 0, &group_comm);
         err = MPI_File_open(group_comm, ao->path, MPI_MODE_CREATE|MPI_MODE_WRONLY, 
                                           MPI_INFO_NULL, &fh);
         if (err != MPI_SUCCESS) {
            char serr[MPI_MAX_ERROR_STRING];
            int slen;
            MPI_Error_string(err, serr, &slen);
            ERR("Failed open file `%s`: %s\n", ao->path, serr); 
            return MSTRO_FAIL;
         }
         uint64_t bsz = 0;
         for (size_t i=0; i<g_mpi_rank; i++)
           bsz += ao->layout->irregular.lengths[i];      
         err = MPI_File_write_at(fh, bsz, data, sz, MPI_BYTE, MPI_STATUS_IGNORE);
//         err = MPI_File_write_ordered(fh, data, sz, MPI_BYTE, MPI_STATUS_IGNORE);

         if (err != MPI_SUCCESS) {
            char serr[MPI_MAX_ERROR_STRING];
            int slen;
            MPI_Error_string(err, serr, &slen);
            ERR("Failed write file `%s`: %s\n", ao->path, serr); 
            return MSTRO_FAIL;
         }
/*
MPI_File_sync( fh ) ; 
MPI_Barrier( comm ) ; 
MPI_File_sync( fh ) ; 
*/
         err = MPI_File_close(&fh);
         if (err != MPI_SUCCESS) {
            char serr[MPI_MAX_ERROR_STRING];
            int slen;
            MPI_Error_string(err, serr, &slen);
            ERR("Failed close file `%s`: %s\n", ao->path, serr); 
            return MSTRO_FAIL;
         }
      }
      break;
    case LIBRARIAN_ORDER_LAYER_DRAM:
      /* nothing todo */
      break;
    default:
      ERR("Unknown layer\n");
  }

  return MSTRO_OK;
}

mstro_status
transport_stage(mstro_cdo cdo, struct approved_order* ao)
{
  if (cdo == NULL || ao == NULL)
    return MSTRO_INVARG;
  int64_t sz;
  if (!ao->is_distributed) {
    sz = ao->total_size;
  } else {
    size_t* lengths;
    size_t n_participants = ao->n_participants;
    lengths = calloc(n_participants, sizeof(size_t));
    compute_fair_lengths_distribution(lengths, ao->n_participants, ao->total_size);
    sz = lengths[g_mpi_rank];
    free(lengths);
  }
  char* data;  /* FIXME free() that at cleanup */
INFO("Staging CDO `%s` from %s local size %zu n_participants %d\n", mstro_cdo_name(cdo), ao->path, sz, ao->n_participants); 
  switch (ao->layer) { 
    case LIBRARIAN_ORDER_LAYER_PFS:
      if (ao->is_distributed == 0) {
        FILE* f = NULL;
        f = fopen(ao->path,"rb");
        if (f == NULL) {
          ERR("Failed to open %s for GFS transport: %d (%s)\n",
              ao->path, errno, strerror(errno));
          return MSTRO_FAIL;
        }
        sz = ao->total_size;
        int ret = posix_memalign((void**)&data, (size_t)sysconf(_SC_PAGESIZE), sz);
        if (ret) {
           ERR("Troubles allocating %"PRId64" bytes\n", sz);
           return MSTRO_NOMEM;
        }
        RETRY_GFS_TRANSPORT_READ: ;
        int bytes_read = fread(data, sizeof(char), (int64_t)sz, f);
        if (bytes_read != sz) {
          if (errno == EAGAIN)
      	  goto RETRY_GFS_TRANSPORT_READ;
          ERR("Partial read (%d out of %d bytes) on %s for GFS transport (errno: %d -- %s)\n",
              bytes_read, (int)sz, ao->path, errno, strerror(errno));
          return MSTRO_FAIL;
        }
      
        if (fclose(f) != 0) {
          ERR("Failed to close %s for GFS transport (errno: %d -- %s)\n",
              ao->path, errno, strerror(errno));
          return MSTRO_FAIL;
        }
      } else {
         MPI_File fh;
         int err;
         MPI_Group world_group;
         MPI_Comm_group(g_comm, &world_group);
         int* ranks = malloc(sizeof(int) * ao->n_participants);
         for (int i=0; i<ao->n_participants; i++)
           ranks[i] = i;
         MPI_Group group;
         MPI_Group_incl(world_group, ao->n_participants, ranks, &group);
         MPI_Comm group_comm;
         MPI_Comm_create_group(g_comm, group, 0, &group_comm);

         err = MPI_File_open(group_comm, ao->path, MPI_MODE_RDONLY, 
                                           MPI_INFO_NULL, &fh);
         if (err != MPI_SUCCESS) {
            char serr[MPI_MAX_ERROR_STRING];
            int slen;
            MPI_Error_string(err, serr, &slen);
            ERR("Failed open file `%s`: %s\n", ao->path, serr); 
            return MSTRO_FAIL;
         }

         posix_memalign((void**)&data, (size_t)sysconf(_SC_PAGESIZE), sz);
         uint64_t bsz = 0;
         for (size_t i=0; i<g_mpi_rank; i++)
           bsz += ao->layout->irregular.lengths[i];      
         err = MPI_File_read_at(fh, bsz, data, sz, MPI_BYTE, MPI_STATUS_IGNORE);
         if (err != MPI_SUCCESS) {
            char serr[MPI_MAX_ERROR_STRING];
            int slen;
            MPI_Error_string(err, serr, &slen);
            ERR("Failed write file `%s`: %s\n", ao->path, serr); 
            return MSTRO_FAIL;
         }
         err = MPI_File_close(&fh);
         if (err != MPI_SUCCESS) {
            char serr[MPI_MAX_ERROR_STRING];
            int slen;
            MPI_Error_string(err, serr, &slen);
            ERR("Failed close file `%s`: %s\n", ao->path, serr); 
            return MSTRO_FAIL;
         }
      }
      break;
    case LIBRARIAN_ORDER_LAYER_DRAM:
      ERR("Trying to order staging from DRAM is a bit weird\n");
      break;
    default:
      ERR("Unknown layer\n");
  }

  MSTRO_SAFE_CALL(mstro_cdo_attribute_set(cdo, MSTRO_ATTR_CORE_CDO_RAW_PTR, data, false, true));
  MSTRO_SAFE_CALL(mstro_cdo_attribute_set(cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, (void**)&sz, true, false));

  return MSTRO_OK;
}

unsigned char
order_checksum(const char* name, void* rawptr, uint64_t size)
{
  unsigned char x;
  uint64_t i;

  for (i=0,x=0;i<size; i++)
    x ^= ((unsigned char*)rawptr)[i];

//  INFO("Checksum for cdo \"%s\": %d\n", name, x);
  return x;
}

/* pushes *tmp* at the end of the serialized string */ 
uint32_t
string_serialize(char* tmp, const char* buf)
{
  uint32_t buf_len = strlen(buf) + 1;
  memcpy(tmp, &buf_len, sizeof(uint32_t));
  tmp += sizeof(uint32_t);
  memcpy(tmp, buf, buf_len);
  return buf_len;
}

/* *buf* must be allocated prior to call */
mstro_status
order_serialize(struct approved_order* ao, char* buf)
{
  if (ao == NULL || buf == NULL) {
    ERR("NULL arg\n");
    return MSTRO_INVARG;
  }

  char* tmp = buf;
  tmp += sizeof(uint32_t) + string_serialize(tmp, ao->cdo_name);
  memcpy(tmp, &(ao->stage), sizeof(int));
  tmp += sizeof(int);
  memcpy(tmp, &(ao->phase), sizeof(int));
  tmp += sizeof(int);

  memcpy(tmp, &(ao->is_distributed), sizeof(int));
  tmp += sizeof(int);
  memcpy(tmp, &(ao->n_participants), sizeof(int));
  tmp += sizeof(int);
  memcpy(tmp, &(ao->total_size), sizeof(uint64_t));
  tmp += sizeof(uint64_t);
  memcpy(tmp, &(ao->layer), sizeof(int));
  tmp += sizeof(int);

  switch(ao->layer) {
    case LIBRARIAN_ORDER_LAYER_PFS:
      if (ao->path == NULL) {
        ERR("No path\n");
        return MSTRO_INVARG; 
      }
      string_serialize(tmp, ao->path);
      break;
    case LIBRARIAN_ORDER_LAYER_DRAM:
      // nothing to do 
      break;
    default:
      ERR("Unknown layer\n");
      return MSTRO_INVARG;
  }

  unsigned char chk = order_checksum(ao->cdo_name, buf, tmp-buf);
  memcpy(tmp, &chk, sizeof(unsigned char));

  return MSTRO_OK;
}

uint32_t
string_deserialize(char** str, char* tmp)
{
  if (tmp == NULL) {
    ERR("NULL buffer\n");
    mstro_librarian_exit(EXIT_FAILURE);
  }
  uint32_t sz;
  memcpy(&sz, tmp, sizeof(uint32_t)); 
DEBUG("Deserializing str of size %" PRIu32 "\n", sz);
  tmp += sizeof(uint32_t);
  *str = malloc(sizeof(char)*sz);
  if (*str == NULL) {
    ERR("Failed to allocate\n");
    mstro_librarian_exit(MSTRO_NOMEM);
  }
  memcpy(*str, tmp, sz);
  DEBUG("Unpacked str: %s\n", *str);
  return sz;
}

mstro_status
order_deserialize(struct approved_order* ao, char* buf)
{
  if (ao == NULL || buf == NULL) {
    ERR("NULL buffer\n");
    return MSTRO_INVARG;
  }

  char* tmp = buf;
  tmp += sizeof(uint32_t) + string_deserialize(&(ao->cdo_name), tmp);
  memcpy(&(ao->stage), tmp, sizeof(int));
  tmp += sizeof(int);
  memcpy(&(ao->phase), tmp, sizeof(int));
  tmp += sizeof(int);

  memcpy(&(ao->is_distributed), tmp, sizeof(int));
  tmp += sizeof(int);
  memcpy(&(ao->n_participants), tmp, sizeof(int));
  tmp += sizeof(int);
  memcpy(&(ao->total_size), tmp, sizeof(uint64_t));
  tmp += sizeof(uint64_t);
  memcpy(&(ao->layer), tmp, sizeof(int));
  tmp += sizeof(int);

  switch(ao->layer) {
    case LIBRARIAN_ORDER_LAYER_PFS:
      string_deserialize(&(ao->path), tmp);
       break;
    case LIBRARIAN_ORDER_LAYER_DRAM:
      // nothing to do 
      break;
    default:
      ERR("Unknown layer\n");
      return MSTRO_INVARG;
  }

  unsigned char schk;
  memcpy(&schk, tmp, sizeof(unsigned char));
  unsigned char chk = order_checksum(ao->cdo_name, buf, tmp-buf);
  if (schk != chk) {
    ERR("Checksum says failed transport\n");
    return MSTRO_FAIL;
  }

 
  return MSTRO_OK;
}

int 
is_distributed(struct approved_order* ao)
{
  return (ao->total_size > g_max_dram && g_mpi_size > 1);
}

size_t
get_n_participants(size_t sz) 
{
   size_t n_participants = sz / g_max_dram;
   if (sz % g_max_dram > 0) 
     n_participants++;
   return n_participants;
}

mstro_status
mstro_librarian_dist_create(struct approved_order* ao, mmbLayout** out_layout)
{
  if (ao == NULL || out_layout == NULL)
    return MSTRO_INVARG;
  mmbLayout* cdo_layout;
  size_t* lengths;
  size_t n_participants = ao->n_participants;
  lengths = calloc(n_participants, sizeof(size_t));
  size_t* offsets; 
  offsets = calloc(n_participants, sizeof(size_t));
  compute_fair_lengths_distribution(lengths, n_participants, ao->total_size);
  for (size_t i=0; i<n_participants; i++) {
    offsets[i] = 0;
    for (size_t j=0; j<i; j++) {
      offsets[i] += lengths[j];
    }
  }
  int index = 0;
  if (is_distributed(ao))
    index = g_mpi_rank;
  DEBUG("index: %d n_participants %d totalsize %zu offset %zu\n", index, n_participants, ao->total_size, offsets[index]);
  mmbError me = mmb_layout_create_dist_irregular_1d(sizeof(char), index, n_participants, offsets, lengths, &cdo_layout);
  if (me != MMB_OK) {
    ERR("Failed to create a distribution\n");
    return MSTRO_FAIL;
  }
  *out_layout = cdo_layout;
  return MSTRO_OK;
}

mstro_status
mstro_librarian_mempool_alloc(struct cdo_required* req, size_t size)
{
  size_t page_size = (size_t)sysconf(_SC_PAGESIZE);
  mmbAllocation* mal;
  MMB_SAFE_CALL(mmb_allocate(size+page_size-1, g_mempool_interface, &mal));
  void* tmp = mal->ptr;
  size_t mod = ((size_t)tmp%page_size);
  if (mod)
    tmp+=(page_size-mod);
DEBUG("unaligned mamba ptr: %p, aligned: %p, pagesize: %zu\n", mal->ptr, tmp, page_size);
  MSTRO_SAFE_CALL(mstro_cdo_attribute_set(req->cdo, MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                    tmp, false, true));
  MSTRO_SAFE_CALL(mstro_cdo_attribute_set(req->cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                    &size, true, false));
  req->aligned_ptr = tmp;
  req->mal = mal;
  return MSTRO_OK;
}

mstro_status
mstro_librarian_mempool_free(struct cdo_required* req)
{
  MMB_SAFE_CALL(mmb_free(req->mal));
  /* raw-ptr is contained in req->mal and thus no need to free again */
  return MSTRO_OK;
}

mstro_status
mstro_librarian_mempool_init(void)
{
  /* `mstro_init` did the `mmb_init` before */

  mmbOptions* MMB_POOL_OPT_DEFAULT;
  size_t poolsize = g_max_dram; 
  MMB_SAFE_CALL(mmb_options_create_default(&MMB_POOL_OPT_DEFAULT));
/* FIXME the following is done BEFORE mmb_init in the tests? */
  MMB_SAFE_CALL(mmb_options_strategy_pooled_set_pool_size(MMB_POOL_OPT_DEFAULT, poolsize));
  mmbMemSpace *space = NULL; 
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = g_max_dram },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };
  MMB_SAFE_CALL(mmb_register_memory(
                  MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &space));
  mmbMemInterfaceConfig interface_config = {
    .provider = MMB_SYSTEM, .strategy = MMB_POOLED, 
    .name = "librarian dram mempool interface"
  };
  MMB_SAFE_CALL(mmb_create_interface(space, &interface_config, &g_mempool_interface));

 //just testing! 
  mmbAllocation* mtest;
  size_t msz = 16;
  MMB_SAFE_CALL(mmb_allocate(msz,g_mempool_interface, &mtest));
  INFO("Simple read don't mind me %d\n", ((int*)(mtest->ptr))[8]);
  MMB_SAFE_CALL(mmb_free(mtest));

  return MSTRO_OK;
}

mstro_status
process_approved_order(struct approved_order* ao) {

INFO("Processing approved order %d on CDO `%s`...\n", ao->stage, ao->cdo_name);

   struct cdo_cont* c = malloc(sizeof(struct cdo_cont));
   struct cdo_required* elt;
   switch (ao->stage) {
     case LIBRARIAN_ORDER_STAGE:
       MSTRO_SAFE_CALL(mstro_cdo_declare(ao->cdo_name, MSTRO_ATTR_DEFAULT, &(c->cdo)));
       mmbLayout* cdo_layout;
//       if (ao->is_distributed) {
         MSTRO_SAFE_CALL(mstro_librarian_dist_create(ao, &cdo_layout));
         MSTRO_SAFE_CALL(mstro_cdo_attribute_set(c->cdo, 
                        MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, cdo_layout, false, true));
//       }
       ao->layout = cdo_layout;
       MSTRO_SAFE_CALL(transport_stage(c->cdo, ao));
       MSTRO_SAFE_CALL(mstro_cdo_seal(c->cdo)); 
       MSTRO_SAFE_CALL(mstro_cdo_offer(c->cdo)); 
       MSTRO_SAFE_CALL(mstro_cdo_withdraw(c->cdo));
       MSTRO_SAFE_CALL(mstro_cdo_dispose(c->cdo));
       if (ao->is_distributed)
         mmb_layout_destroy(cdo_layout);
       break;
     case LIBRARIAN_ORDER_ARCHIVE: 
       elt = ao->req;
       switch(ao->phase) {
         case LIBRARIAN_ARCHIVE_PHASE_REQUIRE : ;
           if (g_am_master) {
             assert(elt != NULL);
           } else {
             assert(elt == NULL);
             elt = malloc(sizeof(struct cdo_required));
             elt->cdo_name = strdup(ao->cdo_name); 
             elt->is_distributed = 0;
             WITH_LOCKED_LIST(
               LL_APPEND(g_required_cdos, elt);
             , g_required_cdos_mutex);

           }
           mstro_cdo dcdo;
           MSTRO_SAFE_CALL(mstro_cdo_declare(ao->cdo_name, MSTRO_ATTR_DEFAULT, &dcdo));
           elt->cdo = dcdo;
           int64_t sz = ao->total_size;
           if (is_distributed(ao)) 
             elt->is_distributed = 1;

//	   if (is_distributed(ao) || elt->n_senders > 1) { // FIXME failing dist_default?
             mmbLayout* cdo_layout;
             MSTRO_SAFE_CALL(mstro_librarian_dist_create(ao, &cdo_layout)); 
             MSTRO_SAFE_CALL(mstro_cdo_attribute_set(dcdo, 
                          MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, cdo_layout, false, true));
             elt->layout = cdo_layout;
             size_t* lengths;
             size_t n_participants = ao->n_participants;
             lengths = calloc(n_participants, sizeof(size_t));
             compute_fair_lengths_distribution(lengths, ao->n_participants, ao->total_size);
             int index = 0;
             if (is_distributed(ao))
               index = g_mpi_rank;
             sz = lengths[index];
             free(lengths);
 //          } 	    
           MSTRO_SAFE_CALL(mstro_librarian_mempool_alloc(elt, sz));
           MSTRO_SAFE_CALL(mstro_cdo_seal(dcdo));
           MSTRO_SAFE_CALL(mstro_cdo_require(dcdo)); // master requires its chunk, the others have to as well
//           if (!g_am_master)
           elt->tested = 0; 
           elt->needs_ack[0] = 1;
           if (g_mpi_rank != 0) {
INFO("Notifying master a REQUIRE has been posted\n");
             MPI_Send((void*)&(elt->needs_ack[0]), sizeof(int), MPI_BYTE, 0, 1, g_comm);
           }
         break;
         case LIBRARIAN_ARCHIVE_PHASE_DEMAND : ;
           c->cdo = NULL;
           LL_FOREACH(g_required_cdos,elt) {
             if (!strcmp(elt->cdo_name,ao->cdo_name) && elt->cdo !=NULL ) {
               c->cdo = elt->cdo;
               break;
             }
           }
           assert(c->cdo != NULL);
           MSTRO_SAFE_CALL(mstro_cdo_demand(c->cdo));
           ao->layout = elt->layout;
           if ( ao->layer == LIBRARIAN_ORDER_LAYER_PFS) {
             DEBUG("Unstaging...\n");
             MSTRO_SAFE_CALL(transport_unstage(c->cdo, ao));
           }
           if ( ao->layer == LIBRARIAN_ORDER_LAYER_DRAM
             && ao->reoffer == 1) {
             DEBUG("Reoffering...\n");
             MSTRO_SAFE_CALL(mstro_cdo_dispose_and_reuse(c->cdo));
             MSTRO_SAFE_CALL(mstro_cdo_offer(c->cdo));
             WITH_LOCKED_LIST(
               LL_APPEND(g_reoffered_cdos, c);
             , g_reoffered_cdos_mutex);
            
           } else {
             MSTRO_SAFE_CALL(mstro_librarian_mempool_free(elt));
             MSTRO_SAFE_CALL(mstro_cdo_dispose(c->cdo));
           }

         break;
         default: 
           ERR("Unknown operation\n");
           mstro_librarian_exit(EXIT_FAILURE);
       }
       break;
     default:
       ERR("Unknown operation\n");
       mstro_librarian_exit(EXIT_FAILURE);

   }
   
   g_last_order_time = mstro_clock();
   return MSTRO_OK;
}

mstro_status
process_approved_orders(void)
{
  mstro_status s = MSTRO_OK;
  struct approved_order* tmp_ao, *foo;
  WITH_LOCKED_LIST(
    LL_FOREACH_SAFE(g_approved_orders, tmp_ao, foo) {
DEBUG("Found an approved order to execute, getting to it...\n");
      process_approved_order(tmp_ao);
      LL_DELETE(g_approved_orders, tmp_ao);
      ao_cleanup(tmp_ao);
    }
  , g_approved_orders_mutex);

  return s;
}

void*
mstro_librarian_run_comm_thread(void *closure) 
{
  int master_does = 0;
  char* buf;
  int ret = posix_memalign((void**)&buf, (size_t)sysconf(_SC_PAGESIZE), 
                                 DEFAULT_AORDER_BUF_SIZE);
  assert(ret==0); 

  int first_time = 1;
  while (!g_end_communication) {
    if (g_mpi_size == 1) {
      process_approved_orders();
    } else if (g_am_master) {
      int count = 0;
      int tag = 0;
      struct approved_order* tmp, *foo;
      WITH_LOCKED_LIST(
        LL_FOREACH_SAFE(g_approved_orders, tmp, foo) {
          if (tmp->is_distributed) {
INFO("Dispatching new order `%p` to %d other rank(s) for multi-rank work on a distributed CDO...\n", tmp, tmp->n_participants-1);
            MPI_Request request = MPI_REQUEST_NULL;
            MSTRO_SAFE_CALL(order_serialize(tmp, buf)); 
            int i;
            for (i=1; i<tmp->n_participants; i++)
              MPI_Send(buf, DEFAULT_AORDER_BUF_SIZE, MPI_BYTE, i, tag, g_comm); 
            process_approved_order(tmp);
          } else {
          /* round robin where master also takes its fair share of work. Balances number of CDOs per rank, not by size which would be a little tricky */
            if (tmp->worker % g_mpi_size == 0) {
              process_approved_order(tmp);
            } else {

INFO("Dispatching new order `%p` to rank %d...\n", tmp, tmp->worker);
                MSTRO_SAFE_CALL(order_serialize(tmp, buf)); 
                MPI_Send(buf, DEFAULT_AORDER_BUF_SIZE, MPI_BYTE, 
                     tmp->worker, tag, g_comm);
            }
          }
          LL_DELETE(g_approved_orders, tmp);
          ao_cleanup(tmp);
        }
      , g_approved_orders_mutex);
    } else {
      int tag = 0;
      MPI_Request request = MPI_REQUEST_NULL;
      MPI_Irecv(buf, DEFAULT_AORDER_BUF_SIZE, MPI_BYTE, 0, tag, g_comm, &request);
      if (first_time) {
        mstro_librarian_workflow_sync();
        first_time = 0;
      }
      MPI_Status status;
      int complete = 0;
      /* Expecting both single recv and bcast (distributed CDO case).
         Nothing else to do than spin-wait (I'm processing orders one by one) */
      while (!complete && !g_end_communication) {
        MPI_Test(&request, &complete, MPI_STATUS_IGNORE);
        if (complete) break;
        sched_yield();
      }
      if (g_end_communication) 
        break;
      struct approved_order* ao = malloc(sizeof(struct approved_order));
DEBUG("Deserializing order...\n");
      MSTRO_SAFE_CALL(order_deserialize(ao, buf)); 
assert(ao->n_participants>=1 && ao->n_participants<=g_mpi_size);
INFO("Recv new order (id:%d out of %d participants): %d on CDO `%s` size %"PRIu64 "...\n", g_mpi_rank, ao->n_participants, ao->stage, ao->cdo_name, ao->total_size);
      ao->req = NULL;
      WITH_LOCKED_LIST(
        LL_APPEND(g_approved_orders, ao);
      , g_approved_orders_mutex);
    }
  }

//  free(buf); 
  return NULL;
}

int 
find_required(char* name)
{
  struct cdo_required* tmp = g_required_cdos;
  if (tmp==NULL)
    return 0;
  while (tmp!=NULL) {
    if (!strcmp(name, tmp->cdo_name))
      return 1;    
    tmp = tmp->next;
  }
  return 0;
}

mstro_status
mstro_librarian_spin_service(void)
{

  /* all ranks need to spin a second thread, so we have:
     - one thread continuously listens to events/orders
     - second thread moves data
  */

  mstro_status s;
  pthread_t comm_thread;
  pthread_create(&comm_thread, NULL, mstro_librarian_run_comm_thread, NULL);

  struct order* tmp_order, *foo;
  int done = 0;
  int rr_count = 1; // not starting round robin with master if we have workers
  g_last_order_time = mstro_clock();

  while (!is_time_to_go()) {
    LL_FOREACH_SAFE(g_order_list, tmp_order, foo) {
    /* Master rank coordinates the orders, other ranks only execute */
      if (!g_am_master) {
        process_approved_orders();
      } else {
        if (tmp_order->stage == LIBRARIAN_ORDER_ARCHIVE) { 
          mstro_pool_event e;
	  s = mstro_subscription_poll(tmp_order->subscription_seal, &e);
          if (s != MSTRO_OK) {
            ERR("Failed poll: %s\n", mstro_status_description(s));
            return s;
          }
          mstro_pool_event tmp=e;
          int send_ack = 0;
          while(tmp != NULL) {
            struct approved_order* ao;
            switch(tmp->kind) {
  /* Doing the sync. at SEAL time, retrieving useful info such as is_distributed, and posting a REQUIRE */
              case MSTRO_POOL_EVENT_SEAL: ;
                if (tmp->payload.seal.appid == mstro_appid()) {
ERR("Ignoring a SEAL event coming from myself on CDO `%s` -> SHOULD NOT HAPPEN WITH `MSTRO_SUBSCRIPTION_OPTS_NO_LOCAL_EVENTS`\n", tmp->payload.seal.cdo_name);
                  tmp=tmp->next;
                  continue;
                } 
                if (tmp->payload.seal.total_size == -1) {
                  if (!strcmp(tmp->payload.seal.cdo_name, CDO_READY_NAME)) {
INFO("Acknowledging someone probing archiver readiness\n");
                    mstro_pool_event ne = tmp->next; // FIXME ack_single?
                    tmp->next = NULL;   
                    MSTRO_SAFE_CALL(mstro_subscription_ack(tmp_order->subscription_seal, tmp));
                    tmp->next = ne;
                  } else {
ERR("Ignoring a SEAL event on CDO `%s` with a size of -1 (unimplemented)\n", tmp->payload.seal.cdo_name);
                  }
                  tmp=tmp->next;
                  continue;
                }
                if (tmp->payload.seal.total_size > g_max_dram * g_mpi_size) {
WARN("Ignoring a SEAL event for a CDO larger (%zu) than I can deal with (%" PRId64 "). Use the --max-dram option to set more space per DRAM, or launch more archiver processes to handle it\n", tmp->payload.seal.total_size, g_max_dram * (uint64_t)g_mpi_size);
                  tmp=tmp->next;
                  continue;
                }
/* */
                struct cdo_required* elt = NULL;
                int cont;
                LL_FOREACH(g_required_cdos, elt) {
                  cont = 0;
                  if (!strcmp(elt->cdo_name,tmp->payload.seal.cdo_name)) {
INFO("SEAL event: %d/%d on elt %p\n", elt->count_seals, elt->n_senders, elt);
                    if (elt->count_seals < elt->n_senders) {
                      for(int i=0; i<elt->count_seals; i++) {
                        if (elt->sender_seals[i] == tmp->payload.seal.appid)
                          cont = 1;
                          break;
                      }
                      if (cont) continue;
                      break;
                    }
                  }
                }
                if (elt != NULL) {
                  elt->sender_seals[elt->count_seals++] = tmp->payload.seal.appid;
                  elt->e[elt->ecount] = tmp;
                  elt->ecount++;
  INFO("Recording a SEAL event for a distributed CDO I am already ready for (`%s`) (from app %"PRIu64")(#%d)(#%d)\n", tmp->payload.seal.cdo_name, tmp->payload.seal.appid, elt->count_seals, elt->ecount);
                  tmp=tmp->next;
                  continue;
                }
                ao = malloc(sizeof(struct approved_order));
                ao->cdo_name = strdup(tmp->payload.seal.cdo_name);
                ao->stage = tmp_order->stage;
                ao->phase = LIBRARIAN_ARCHIVE_PHASE_REQUIRE;
                ao->is_distributed = 0; 
                ao->total_size = tmp->payload.seal.total_size;
                ao->n_participants = get_n_participants(ao->total_size);
assert(ao->n_participants>=1 && ao->n_participants<=g_mpi_size);
INFO("Recording a SEAL event for a new occurence of CDO `%s` from app %"PRIu64", redistribution (%d:%d) \n", tmp->payload.seal.cdo_name, tmp->payload.seal.appid, tmp->payload.seal.n_participants, ao->n_participants);
                ao->layer = tmp_order->layer; 
assert(tmp_order->reoffer == 0 || tmp_order->reoffer == 1);
                ao->reoffer = tmp_order->reoffer;
                ao->subscription_seal = tmp_order->subscription_seal;
                ao->subscription_offer = tmp_order->subscription_offer;
                assert(ao->subscription_seal!=NULL && ao->subscription_offer);
                switch(tmp_order->layer) {
                  case LIBRARIAN_ORDER_LAYER_PFS:
                    if (tmp_order->path != NULL)
                      ao->path = strdup(tmp_order->path);
                    break;
                  case LIBRARIAN_ORDER_LAYER_DRAM:
                    // nothing to do 
                    break;
                  default:
                    ERR("Unknown layer\n");
                    mstro_librarian_exit(EXIT_FAILURE);
                }
                if (is_distributed(ao)) {
                  ao->is_distributed = 1; 
                  ao->worker = -1; // unused in the distributed case
                
                } else {                          
                  if (g_mpi_size == 1)
                    ao->worker = 0;
                  else
                    ao->worker = rr_count++ % g_mpi_size;
                }
           /* abusing required list with an half empty entry (rest is handled by other rank) */
                struct cdo_required* d = calloc(1,sizeof(struct cdo_required));
                d->offered = 0;
                d->order = tmp_order;
                d->e[0] = tmp;
                d->ecount = 1;
                d->worker = ao->worker;
                d->tested = 0;
                d->is_distributed = ao->is_distributed;
                d->total_size = ao->total_size;
                d->n_participants = ao->n_participants;
assert(ao->n_participants>=1 && ao->n_participants<=g_mpi_size);
                d->cdo_name = strdup(ao->cdo_name);
                d->n_senders = tmp->payload.seal.n_participants;
                d->count_seals = 1;
                d->sender_seals = malloc(sizeof(int)*d->n_senders);
                d->sender_seals[0] = tmp->payload.seal.appid;
                d->count_offers = 0;
                d->sender_offers = malloc(sizeof(int)*d->n_senders);
                int mpis = MPI_SUCCESS;
                if (ao->is_distributed) {
                  int i;
                  for (i=1; i<d->n_participants; i++) {
                    mpis = MPI_Irecv((void*)&(d->needs_ack[i]), sizeof(int), MPI_BYTE, i, 1, g_comm, &(d->mpir[i-1]));
                  }
                } else if (d->worker > 0) { 
DEBUG("Posting Irecv, for worker %d\n", d->worker);
                   d->needs_ack[0] = 1; // FIXME faking it
                    mpis = MPI_Irecv((void*)&(d->needs_ack[1]), sizeof(int), MPI_BYTE, d->worker, 1, g_comm, &(d->mpir[0]));
                } else 
  //                  d->needs_ack[0] = 1; // local, non distributed: master posts the require
                     ;
                if (mpis != MPI_SUCCESS) {
                  ERR("Something went wrong with Irecv...\n");
                  mstro_librarian_exit(mpis);
                }
		WITH_LOCKED_LIST(
                  LL_APPEND(g_required_cdos, d); // all this info will be useful for the decoupled demand
       		, g_required_cdos_mutex);

       		ao->req = d;
		WITH_LOCKED_LIST(
                  LL_APPEND(g_approved_orders, ao);
		, g_approved_orders_mutex);
                break;
              default:  
                ERR("Unexpected event %d\n", tmp->kind);
            }
            tmp=tmp->next;
          }

          struct cdo_required* elt, *foo;
          int complete;
          LL_FOREACH_SAFE(g_required_cdos, elt, foo) {
            if (elt->tested == 0 && elt->needs_ack[0] == 1 && elt->ecount == elt->n_senders) {
              if (elt->worker != 0) {
                complete = 0;
                int shift = 0;
                if (elt->is_distributed)
                  shift = 1;
                // Master does not need a message from itself, we just test everyone else
		MPI_Status smpi [MSTRO_LIBRARIAN_MAX_PARTICIPANTS];
		NOISE("Testall on CDO `%s` with %d participants (worker %d), mpir %p, smpi %p\n", elt->cdo_name, elt->n_participants-shift, elt->worker, elt->mpir, smpi);
		assert(elt->n_participants-shift > 0 && elt->n_participants <= g_mpi_size && elt->mpir != NULL);
                int mpis = MPI_Testall(elt->n_participants-shift, elt->mpir, &complete, smpi);
		NOISE("OK Testall on CDO `%s` with %d participants (worker %d), mpir %p, smpi %p\n", elt->cdo_name, elt->n_participants-shift, elt->worker, elt->mpir, smpi);
                if (mpis != MPI_SUCCESS)
                  mstro_librarian_exit(mpis);
                if (!complete) 
                  continue; 
                int i;
                // Sanity check
                for (i=0; i<elt->n_participants; i++) {
                  if (elt->needs_ack[i] != 1) {
                    if (i==0) 
                      ERR("Somehow my local needs_ack is messed up (%d) \n", elt->needs_ack[i]);
                    else
                      ERR("Corrupted message received from rank %d (%d) \n", i, elt->needs_ack[i]);
                    mstro_librarian_exit(EXIT_FAILURE);
                  } 
                }
                INFO("Recv'd pings from workers: all the REQUIREs (%d)(%d ev) are posted, we can ACK the PM\n", elt->n_participants, elt->ecount); 
              }
INFO("Batch subscription ACK (got %d out of %d at this point)\n", elt->ecount, elt->n_senders);
              int i;
              for (i=0; i<elt->ecount; i++) {
                mstro_pool_event ne = elt->e[i]->next;
                elt->e[i]->next = NULL; //FIXME ack_single?
                MSTRO_SAFE_CALL(mstro_subscription_ack(elt->order->subscription_seal, elt->e[i]));
                elt->e[i]->next = ne;
                assert(elt->e[i] != NULL);
                MSTRO_SAFE_CALL(mstro_pool_event_dispose_single(elt->e[i]));
              }
              bzero(elt->needs_ack, MSTRO_LIBRARIAN_MAX_PARTICIPANTS*sizeof(int));
              elt->tested = 1;
            }

          }


/* Although we need to know on master when workers post their REQUIREs for ACK
 * and DEMAND purposes, we are not listening to our own REQUIREs with Maestro,
 * we use only MPI to communicate between our librarian ranks. This might change
 * when: - We fully replace MPI with Maestro for inner librarian comms _and_ -
 * local events are implemented
 */

          e=NULL;
          MSTRO_SAFE_CALL(mstro_subscription_poll(
                    tmp_order->subscription_offer, &e));
          tmp=e;
          while(tmp) {
            struct approved_order* ao;
            switch(tmp->kind) {
              case MSTRO_POOL_EVENT_OFFER: ;
                if (tmp->payload.offer.appid == mstro_appid()) {
ERR("Ignoring OFFER event coming from myself on CDO ``%s`, but SHOULD NOT HAPPEN\n", tmp->payload.offer.cdo_name);
                  continue;
                }   

                struct cdo_required* elt = NULL;
                int cont;
                LL_FOREACH(g_required_cdos, elt) {
                  cont = 0;
                  if (!strcmp(elt->cdo_name,tmp->payload.offer.cdo_name)) {
INFO("OFFER event: %d/%d on elt %p\n", elt->count_offers, elt->n_senders, elt);
                    if (elt->count_offers < elt->n_senders) {
                      for(int i=0; i<elt->count_offers; i++) {
                        if (elt->sender_offers[i] == tmp->payload.offer.appid) {
                          cont = 1;
                          break;
                        }
                      }
                      if (cont) continue;
                      break;
                    }
                  }
                }
                if (elt == NULL) {
ERR("Recvd an OFFER for CDO `%s` not yet registered here\n", tmp->payload.offer.cdo_name);
                 //FIXME fallback
                  mstro_librarian_exit(MSTRO_FAIL);
                }
 
                if (elt != NULL && elt->offered) { 
// FIXME still assuming the PM waits for all the pieces
INFO("Ignoring an OFFER event for a CDO I am already ready for (`%s`) from app %"PRIu64"...\n", tmp->payload.offer.cdo_name, tmp->payload.offer.appid);
                  elt->sender_offers[elt->count_offers++] = tmp->payload.offer.appid; 
                  tmp=tmp->next;
                  continue;
                } 

INFO("Processing OFFER event `%p` on CDO `%s` from app %"PRIu64"...\n", tmp, tmp->payload.offer.cdo_name, tmp->payload.offer.appid);
                ao = malloc(sizeof(struct approved_order));
                ao->cdo_name = strdup(tmp->payload.offer.cdo_name);
                ao->stage = tmp_order->stage;
                ao->phase = LIBRARIAN_ARCHIVE_PHASE_DEMAND;
                ao->layer = tmp_order->layer; 
                ao->reoffer = tmp_order->reoffer;
                ao->subscription_seal = tmp_order->subscription_seal;
                ao->subscription_offer = tmp_order->subscription_offer;
                assert(ao->subscription_seal!=NULL && ao->subscription_offer);
                switch(tmp_order->layer) {
                  case LIBRARIAN_ORDER_LAYER_PFS:
                    if (tmp_order->path != NULL)
                      ao->path = strdup(tmp_order->path);
                    break;
                  case LIBRARIAN_ORDER_LAYER_DRAM:
                    // nothing to do 
                    break;
                  default:
                    ERR("Unknown layer\n");
                    mstro_librarian_exit(EXIT_FAILURE);
                }
               assert (elt != NULL);
                if (!strcmp(elt->cdo_name,tmp->payload.offer.cdo_name)) {
                  ao->worker = elt->worker;
                  ao->is_distributed = elt->is_distributed;
                  ao->n_participants = elt->n_participants;
assert(ao->n_participants>=1 && ao->n_participants<=g_mpi_size);
                  ao->total_size = elt->total_size;
	          WITH_LOCKED_LIST(
                    LL_APPEND(g_approved_orders, ao);
		  , g_approved_orders_mutex);
                  elt->offered = 1;
                  break;
                }

                break;
              default:  
                ERR("Unexpected event %d\n", tmp->kind);
            }
            tmp=tmp->next;
          }
          if (e != NULL)
            MSTRO_SAFE_CALL(mstro_pool_event_dispose(e));
        } else if (tmp_order->stage == LIBRARIAN_ORDER_STAGE) {
        /* All ranks including master handles staging independently, unlike archiving which needs more coordination */
            struct approved_order* ao = malloc(sizeof(struct approved_order));
            ao->stage = tmp_order->stage;
            
            ao->layer = tmp_order->layer; 
            ao->path = strdup(tmp_order->path); 
            ao->subscription_seal = NULL;
            ao->subscription_offer = NULL;
            ao->phase = LIBRARIAN_ARCHIVE_PHASE_INVALID; // N/A for staging
            ao->reoffer = 0; // N/A for staging
            switch(tmp_order->layer) {
              case LIBRARIAN_ORDER_LAYER_PFS:
                ao->cdo_name = strdup(ao->path);
                struct stat st;
                stat(ao->path, &st);
                ao->total_size = st.st_size; 
                if (ao->total_size > g_max_dram * g_mpi_size) {
    ERR("Discarding stage order for a CDO larger (%d) than I can deal with (%" PRId64 "). Use the --max-dram option to set more space per DRAM, or launch more archiver processes to handle it\n", ao->total_size, g_max_dram * (uint64_t)g_mpi_size);
                  LL_DELETE(g_order_list, tmp_order);
                  continue;
                }
                break;
              default:
                ERR("Invalid layer\n");
                mstro_librarian_exit(EXIT_FAILURE);
            }

            ao->n_participants = get_n_participants(ao->total_size); 
            ao->is_distributed = (ao->n_participants > 1); 
            if (ao->is_distributed) {
              ao->worker = -1; // unused in the distributed case
            } else {                          
              if (g_mpi_size == 1)
                ao->worker = 0;
              else
                ao->worker = rr_count++ % g_mpi_size;
            }
            WITH_LOCKED_LIST(
              LL_APPEND(g_approved_orders, ao);
	    , g_approved_orders_mutex);
            LL_DELETE(g_order_list, tmp_order);
        } else {
            ERR("Invalid order that is neither archiving nor staging\n");
            mstro_librarian_exit(EXIT_FAILURE);
        }
      }
    }
  }

DEBUG("Cleaning up before we go...\n");

  void *res;
  pthread_join(comm_thread, &res);

DEBUG("All threads cleanly joined...\n");

  if (g_cdo_ready != NULL) {
    MSTRO_SAFE_CALL(mstro_cdo_withdraw(g_cdo_ready));
    MSTRO_SAFE_CALL(mstro_cdo_dispose(g_cdo_ready));
  }

  struct cdo_required* tmp_req, *tmp_bar;
  LL_FOREACH_SAFE(g_required_cdos, tmp_req, tmp_bar) {
/* If the required CDOs are still not demanded at the point, there was a failure before 	  
    if (!tmp_req->offered) {
      MSTRO_SAFE_CALL(mstro_cdo_retract(tmp_req->cdo));
      MSTRO_SAFE_CALL(mstro_librarian_mempool_free(tmp_req));
      MSTRO_SAFE_CALL(mstro_cdo_dispose(tmp_req->cdo));
    }
*/
    WITH_LOCKED_LIST(
      LL_DELETE(g_required_cdos, tmp_req);
    , g_required_cdos_mutex);

    free(tmp_req->cdo_name);
  }

  struct cdo_cont* tmp_cont, *tmp_foo;
  LL_FOREACH_SAFE(g_reoffered_cdos, tmp_cont, tmp_foo) {
    MSTRO_SAFE_CALL(mstro_cdo_withdraw(tmp_cont->cdo));
    MSTRO_SAFE_CALL(mstro_cdo_dispose(tmp_cont->cdo));
    WITH_LOCKED_LIST(
      LL_DELETE(g_reoffered_cdos, tmp_cont);
    , g_reoffered_cdos_mutex);
  }

  if (g_leave_subscription != NULL)
    MSTRO_SAFE_CALL(mstro_subscription_dispose(g_leave_subscription));

  return MSTRO_OK;
}



int
main(int argc, char ** argv)
{
  mstro_status s;
  int mpi_status;
  int provided;

  g_startup_time = mstro_clock();

  mpi_status = MPI_Init_thread(NULL,NULL,MPI_THREAD_MULTIPLE, &provided);
  if (mpi_status != MPI_SUCCESS) {
    fprintf(stderr, "Failed to init MPI: %d\n", mpi_status);
    return EXIT_FAILURE;
  }
  if (provided < MPI_THREAD_SERIALIZED) {
     fprintf(stderr, "MPI does not support MPI_THREAD_SERIALIZED \n");
     exit(EXIT_FAILURE);
  }

#ifdef MPMD

  	int rank;

	//create a new subcommunicator
    	MPI_Group world_group;
    	MPI_Comm_group(MPI_COMM_WORLD, &world_group);
 
	int nproducers = atoi(getenv("NPRODUCERS")),
	    nconsumers = atoi(getenv("NCONSUMERS"));
    	int *ranks = (int*) malloc(sizeof(int)*(nconsumers));
	for(int i=0;i<nconsumers;i++)
		ranks[i] = i+nproducers;

    	MPI_Group new_group;
    	MPI_Group_incl(world_group, nconsumers, ranks, &new_group);
 
    	// Create the new communicator from that group of processes.
    	MPI_Comm_create(MPI_COMM_WORLD, new_group, &g_comm);

	if(g_comm == MPI_COMM_NULL)
                MPI_Finalize();

        MPI_Comm_rank(g_comm, &rank);

	// check if pm.info file is present
	char *buffer;
        int length;
	if(rank==0)
	{
        	FILE * f = NULL;

        	while(f == NULL)
        	{
                	f = fopen ("pm.info", "r");
                	sleep(1);
                	printf("waiting for pm info file ...\n");
		}
        	printf("done.\n");

        	fseek (f, 0, SEEK_END);
        	length = ftell (f);
        	fseek (f, 0, SEEK_SET);
        	buffer = malloc (length);
        	if (buffer)
                	fread (buffer, 1, length, f);

		for(int i=24;i<length;i++) // shift by 24 characters
			buffer[i-24] = buffer[i];
		length -= 24;
		buffer[length-1] = '\0'; //overwrite the last character
		fclose (f);

        	//printf("buffer=%s\n",buffer);
	}
	MPI_Bcast(&length, 1, MPI_INT, 0, g_comm);
	if(rank != 0)
		buffer = malloc (length);
	MPI_Bcast(buffer, length, MPI_CHAR, 0, g_comm);

	int err = setenv("MSTRO_POOL_MANAGER_INFO", buffer, 1);

  const char *name = "MSTRO_POOL_MANAGER_INFO";
        char *value;
        value = getenv(name);
        printf("MSTRO_POOL_MANAGER_INFO: %s\n",value);

#else
	g_comm = MPI_COMM_WORLD;

#endif


  MPI_Comm_rank (g_comm, &g_mpi_rank);
  if (g_mpi_rank == 0) 
    g_am_master = 1;
  MPI_Comm_size (g_comm, &g_mpi_size);
  if (g_mpi_size > MSTRO_LIBRARIAN_MAX_PARTICIPANTS) {
    fprintf(stderr, "FIXME: Not supporting number of ranks (%d) > %d\n", g_mpi_size, MSTRO_LIBRARIAN_MAX_PARTICIPANTS);
    return EXIT_FAILURE;
  }

  MSTRO_SAFE_CALL(mstro_init(g_conf_workflow_name, g_conf_component_name,g_mpi_rank));

  DEBUG("Parsing arguments...\n");
  MSTRO_SAFE_CALL(parse_arguments(argc, argv));

  MSTRO_SAFE_CALL(mstro_librarian_mempool_init());

  /* Review orders, subscribe to everything there is to subscribe to, according
 * to archiving orders */
  DEBUG("Subscribing...\n");
  if (g_mpi_rank == 0)
    MSTRO_SAFE_CALL(batch_subscribe());

  /* Master rank will review orders, listen, dispatch (round robin or
 * distributed). Listening eternally by default, see --help for timeouts and
 * other clean exit methods. Staging orders individually may take some time to
 * execute, so we will execute parameter orders one by one in the right order,
 * in order to allow some work time on archiving orders as well */ 
  DEBUG("Starting service...\n");
  MSTRO_SAFE_CALL(mstro_librarian_spin_service());

  if (g_am_master) {
    INFO("Cleaning up subscriptions...\n");
    MSTRO_SAFE_CALL(subscriptions_cleanup());
    INFO("Cleaning up remaining orders (the eternal ones)...\n");
    orders_cleanup();
  }
  
  INFO("Finalizing...\n");
  MSTRO_SAFE_CALL(mstro_finalize());

  mpi_status = MPI_Finalize();
  if (mpi_status != MPI_SUCCESS) {
    fprintf(stderr, "Failed to finalize MPI: %d\n", mpi_status);
    return EXIT_FAILURE;
  }

  fprintf(stderr, "Successfuly exiting\n");
  return g_return_code;
}
