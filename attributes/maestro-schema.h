/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Schema parsing
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_ATTRIBUTE_META_SCHEMA_H_
#define MAESTRO_I_ATTRIBUTE_META_SCHEMA_H_ 1

#include <stdbool.h>
#include "maestro/status.h"
#include "deps/mamba/common/mmb_layout.h"
#include "protocols/mstro_pool.pb-c.h"

/** An (abstract) schema handle */
struct mstro_schema_;
typedef struct mstro_schema_ *mstro_schema;

/** An (abstract) schema type declaration handle */
struct mstro_schema_type_;
typedef struct mstro_schema_type_ *mstro_schema_type;

/** An (abstract) schema attribute declaration handle */
struct mstro_schema_attribute_;
typedef struct mstro_schema_attribute_ *mstro_schema_attribute;

/** An (abstract) schema attribute value entry handle */
struct mstro_attribute_entry;
typedef struct mstro_attribute_entry_ *mstro_attribute_entry;

/** parse a maestro schema using the cyaml-defined meta-schema from an octet array */
mstro_status
mstro_schema_parse(const uint8_t *yaml_data, size_t data_len,
                   mstro_schema *result);

/** parse a maestro schema using the cyaml-defined meta-schema from a file */
mstro_status
mstro_schema_parse_from_file(const char *fname, mstro_schema *result);

/** destroy a schema handle */
mstro_status
mstro_schema_free(mstro_schema sch);

/** Return the name of a schema. */
const char *
mstro_schema_name(mstro_schema sch);

/** Return the version of a schema. */
uint64_t
mstro_schema_version(mstro_schema sch);


/** Merge two schemata
 **
 ** The @arg main schema absorbs the @arg consumed second one; the
 ** second one can still be used, but may not be de-allocated
 ** explicitly, as its data structures become shared with the shares
 ** data structures of the @arg main schema. Deallocating that will
 ** also clean up the @arg consumed one.
 **
 ** The schema list is linear. All entries in @arg main dominate all
 ** those in @arg consumed, i.e., consumed is "appended" to the end of
 ** @arg main.
 **
 ** Typical usage: A user schema is used as @arg main, with the
 ** maestro-core schema being @arg consumed. As a result entries in
 ** @arg main shadow entries in @arg consumed.
 **/
mstro_status
mstro_schema_merge(mstro_schema main,
                   mstro_schema consumed);

/** Lookup type name in schema
 **
 **/
mstro_status
mstro_schema_lookup_type(mstro_schema schema,
                         const char *typename,
                         mstro_schema_type *result);

/** Lookup attribute name in schema
 **
 **/
mstro_status
mstro_schema_lookup_attribute(mstro_schema schema,
                              const char *attributename,
                              mstro_schema_attribute *result);


//struct mstro_attribute_dict_;
typedef struct mstro_attribute_dict_ * mstro_attribute_dict;

/** Parse attribute declaration
 **
 ** This is the core function used to parse textual attributes
 ** specified for a CDO. The input will be parsed as though it were
 ** normalized to a depth-1 sequence of 'key: value' entries. Keys
 ** will be looked up as attribute keys in the @arg schema, and values
 ** will be parsed accordingly.
 **
 ** If *result == NULL a fresh dictionary will be allocated.
 ** If *result != NULL the parsed values will be stored in the dictionary, replacing
 ** any previously set values for the same keys.
 **
 ** Example:
 ** maestro.core.cdo:
 **    persist: "on"
 **    scope:
 **     size: 1024
 **
 ** will be turned into the equivalent of
 **  maestro.core.cdo.persist.cdo.persist: "on"
 **  maestro.core.cdo.scope.size: 1024
 **
 ** @arg yaml_fragment may not be NULL.
 **
 ** @arg default_namespace_prefix may be NULL
 **/

mstro_status
mstro_attributes_parse(mstro_schema schema,
                       const char *yaml_fragment,
                       mstro_attribute_dict *result,
                       const char *default_namespace_prefix);

/** Parse attribute declaration
 **
 ** Parsing from file version
 **
 **/

mstro_status
mstro_attributes_parse_from_file(mstro_schema schema,
                                 const char *fname,
                                 mstro_attribute_dict *result,
                                 const char *default_namespace_prefix);



/** Inject default values from schema into attribute dictionary.
 **
 ** If *result==NULL, allocate a fresh, default-filled, attribute
 ** dictionary. Otherwise just (re-)set the default values from the
 ** schema.
 **
 ** When @arg override is true, overrides existing entries, otherwise only fills in missing ones.
 **/
mstro_status
mstro_attribute_dict_set_defaults(mstro_schema schema,
                                  bool override,
                                  mstro_attribute_dict *result);

/** Deallocate a dictionary.
 *
 */
mstro_status
mstro_attribute_dict_dispose(mstro_attribute_dict dict);

/** Clear all entries in dictionary */
mstro_status
mstro_attribute_dict_clear(mstro_attribute_dict dict);


mstro_status
mstro_attribute_dict_get_schema(mstro_attribute_dict dict, mstro_schema *schema_p);


/** Look up maestro attribute in dictionary.
 *
 * On successful lookup return MSTRO_OK, *valtype set appropriately,
 * and *val_p (and *entry_p) set to a (read-only) reference of the
 * value that can be cast to the type documented for the respective
 * kind of @ref mstro_cdo_attr_value_type.
 *
 * The reference stays alive at least until the dictionary is
 * deallocated, or until the value is replaced by a call to @ref
 * mstro_attribute_dict_set().
 *
 * Any of valtype, val_p, and entry_p may be NULL to indicate that the
 * caller is not interested in actually receiving the respective
 * value.
 *
 * If @arg insert_default is true, apply the operation to the current
 * attribute dictionary and fall back to the schema defaults if
 * needed. If it is false, do not refer to the schema defaults.
 *
 * If key is unknown return MSTRO_NOENT and *valtype==MSTRO_CDO_ATTTR_VALUE_NA.
 *
 * If key is known but has no value, return MSTRO_NOENT, and *valtype==MSTRO_CDO_ATTTR_VALUE_INVALID
 *
 * otherwise returns other error code
 */
mstro_status
mstro_attribute_dict_get(mstro_attribute_dict dict,
                         const char *key,
                         enum mstro_cdo_attr_value_type *valtype,
                         const void **val_p,
                         mstro_attribute_entry * const entry_p,
                         bool insert_default);

/** Set an attribute's value in the dictionary.
 *
 * This will blindly accept the user pointer as a value. The pointer
 * needs to be valid at least until the dictionary is deallocated.
 *
 * Very limited type checking can be achieved by specifying @arg type
 * different from MSTRO_CDO_ATTR_VALUE_INVALID. Nonmatching types will
 * result in a MSTRO_INVARG error.
 *
 * val==NULL is handled specially: It will reset the attribute to the default, or 'UNSET' if no default exists.
 * This may be lead to an error being returned, if the attribute is required and has no default.
 *
 * If @arg copy_value is set to ::TRUE, create an internal allocation
 * for the value and copy @arg val into it. Otherwise only store the
 * reference (which must remain valid until the dictionary is
 * disposed, so in particular stack allocated values are forbidden if
 * the dictionary outlives their scope).
 *
 * If @arg user_owned_value is given as ::TRUE, consider the value
 * maestro-owned and deallocate it when the dictionary @arg dict gets
 * destroyed or the value for @arg key is being changed. Otherwise the
 * value is considered user-owned (see @arg copy_value above).
 *
 * NOTE: Specifying both @arg copy_value and @arg user_owned_value as
 * ::TRUE simultaneously is not supported. The common case is to set
 * @arg user_owned_value when not setting @arg copy_value, and vice
 * versa. Setting both to ::FALSE can be used to pass ownership of the
 * @arg val value to maestro.
 */
mstro_status
mstro_attribute_dict_set(mstro_attribute_dict dict, const char *key,
                         enum mstro_cdo_attr_value_type valtype,
                         void *val,
                         bool copy_value, bool user_owned_value);

/* set attribute from protobuf key/value message */
mstro_status
mstro_attribute_dict_set_kventry(mstro_attribute_dict dict,
                                 const Mstro__Pool__KvEntry *entry);


/** Create a pool manager message containing all attributes.
 *
 * References to values in the dictionary will be inserted, so the
 * dictionary must outlive the created message.
 *
 * Care must be taken when destroying the message to not have the
 * protobuf message disposal code free such values; always use
 * mstro_attribute_dict_message_dispose on messages created by this
 * function.
 */

mstro_status
mstro_attribute_dict_to_message(mstro_attribute_dict dict,
                                Mstro__Pool__Attributes **msg_p);


/* Print user set keys and values */
void
mstro_attribute_dict_print(mstro_attribute_dict d);

/** convert MSTRO__POOL__MMBLAYOUT to mmbLayout
  */
mstro_status
mstro_attribute_pool_aval_to_mmbLayout(Mstro__Pool__Mmblayout *aval_layout, mmbLayout **dist_layout);


/** Look for .maestro.core.cdo.dist-layout in Mstro__Pool__Attributes array
  * and convert it to mmbLayout
  *
  * Return MSTRO_OK on success.
**/
mstro_status
mstro_attribute_pool_find_dist_layout(
                                  Mstro__Pool__Attributes *attributes,
                                  mmbLayout **dist_layout);

/** Destroy a pool manager message containing references to the values in dict.
 *
 * This function must be used instead of the generic protobuf deallocators to avoid freeing values still
 * referenced in the dictionary.
 */
mstro_status
mstro_attribute_dict_message_dispose(mstro_attribute_dict dict,
                                     Mstro__Pool__Attributes *msg);

/** cast and copy a size_t array to uint64_t array
 *
 * such coversion is needed for mmbLayout irregular layout offsets and lengths arrays
 */
static inline
mstro_status
mstro_cast_size_t_to_unit64_array(size_t *in, uint64_t *out, size_t len);

/** cast and copy a uint64_t array to size_t array
 *
 * such coversion is needed for mmbLayout irregular layout offsets and lengths arrays
 */
static inline
mstro_status
mstro_cast_unit64_to_size_t_array(uint64_t *in, size_t *out, size_t len);


/** Comparison operations for attribute values
 */
/* to ensure best code generation these should be equal to the MSTRO_CSQ_OP_* constants */
enum mstro_attribute_val_cmp_op {
  MSTRO_ATTR_VAL_CMP_LT = 16, /* MSTRO_CSQ_OP_LT */
  MSTRO_ATTR_VAL_CMP_LE,
  MSTRO_ATTR_VAL_CMP_GT,
  MSTRO_ATTR_VAL_CMP_GE,
  MSTRO_ATTR_VAL_CMP_EQ,
  MSTRO_ATTR_VAL_CMP_NEQ,
  MSTRO_ATTR_VAL_CMP_RMATCH,
  MSTRO_ATTR_VAL_CMP_RMATCH_ICASE
};

/** Compare string @arg VALSTR to @arg AVAL under @arg CMP, interpreting the value according to @arg attr */
mstro_status
mstro_attribute_val_cmp_str(mstro_schema_attribute attr,
                            const char *valstr, enum mstro_attribute_val_cmp_op cmp,
                            const Mstro__Pool__AVal *avalcmp, bool *result);


#endif
