/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Symbol Table implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "symtab.h"
#include <stdbool.h>

#include "maestro/logging.h"


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)




/* these have to stay before the uthash include to get picked up at compile time */
#define HASH_FUNCTION(s,len,hashv)                                      \
  (hashv) = mstro_symtab_key_hash((struct mstro_symtab_entry_key *)s)

#define HASH_KEYCMP(a,b,len)                                            \
  (!mstro_symtab_key_equal((struct mstro_symtab_entry_key *)a,          \
                           (struct mstro_symtab_entry_key *)b))

#include "maestro/i_uthash.h"


/** Symbol table key data type
 **
 ** This is a tricky one.
 **
 ** The obvious part is when len!=0, permanent_address==NULL, and the
 ** structure is size-extended so that fullname contains LEN
 ** (0-terminated) character of the symbol name.
 **
 ** If len==0 and permanent_address!=NULL the string in the permanent_address field.
 ** This is to optimize for the case of symbols that are generated from constant strings.
 **/
struct mstro_symtab_entry_key {
  size_t len;                     /**< the length of the
                                   * symbol-name */
  const void *permanent_address;  /**< set to the address of constants
                                   * that get interned. NULL for 'not
                                   * permanent' */
  char fullname[];                /**< the fully-qualified name;
                                   * auto-extended at mstro_intern
                                   * time */
};

/** Compute a hash value for KEY.
 **
 ** The hash value is always based on the string value, not the
 ** address. That ensuress that user-allocated strings which are
 ** string-equal to a constant string version will hash to the same
 ** value 
 **/
static inline
unsigned
mstro_symtab_key_hash(struct mstro_symtab_entry_key *key)
{
  const char *s;
  size_t len;
  unsigned hashval;

  if(key->permanent_address!=NULL) {
    len = strlen(key->permanent_address);
    s = key->permanent_address;
  } else {
    len = key->len;
    s = key->fullname;
  }

  HASH_JEN(s, len, hashval);

  return hashval;
}


/** Compare two symbol table entry keys for equality
 **
 ** This speeds up comparisons of permanent keys to 2 comparisons;
 ** non-permanent ones compare by memcmp with short-cicuiting to
 ** non-equality if lengths don't match. This incurs an O(n) length
 ** computation if one is a permanent and one is a non-permanent entry
 ** (which could be saved by adding another length field to the
 ** struture */
static inline bool
mstro_symtab_key_equal(struct mstro_symtab_entry_key *k1,
                       struct mstro_symtab_entry_key *k2)
{
  /* DEBUG("Comparing %zu/%p/%s/%s and %zu/%p/%s/%s\n", */
  /*       k1->len, k1->permanent_address, k1->permanent_address, k1->len==0 ? "" : k1->fullname, */
  /*       k2->len, k2->permanent_address, k2->permanent_address, k2->len==0 ? "" : k2->fullname); */
  if(k1->permanent_address == k2->permanent_address) {
    if(k1->permanent_address != NULL) {
      /* both permanent, and equal, this is the quick case */
      return true;
    } else {
      /* both non-permanent */
      if(k1->len != k2->len) {
        /* different length */
        return false;
      } else {
        /* compare equal-length strings. FIXME: consider comparing from the end */
        return (0==uthash_memcmp(k1->fullname, k2->fullname, k1->len)) ? true : false;
      }
    }
  } else {
    /* different addresses; need to check whether one is NULL */
    const char *s1,*s2;
    size_t l1,l2;
    if(k1->permanent_address==NULL) {
      s1 = k1->fullname;
      l1 = k1->len;
    } else {
      s1 = k1->permanent_address;
      l1 = strlen(s1); /* O(n) */
    }
    if(k2->permanent_address==NULL) {
      s2 = k2->fullname;
      l2 = k2->len;
    } else {
      s2 = k2->permanent_address;
      l2 = strlen(s2); /* O(n) */
    }
    if(l1!=l2) {
      return false;
    } else {
      return (0==uthash_memcmp(s1,s2,l1)) ? true : false;
    }
  }
}


/** A structure whose allocation address serves as atribute key in */
/* FIXME: key has a VLA at the end, hence strictly speaking we cannot embed it into another structure:
   symtab.c:130:33: warning: 'key' may not be nested in a struct due to flexible array member [-Wflexible-array-extensions]
   (C11 (ISO/IEC 9899:2011), §6.7.2.1.3 / §6.7.2.1.18)
   We should fix that, but both gcc and clang are handling it fine for now (they only warn at -pedantic and above)

   Note that simply using key[1] does not work either. Likely the best
   option is to have the same declaration #defined and use it here
   inline and in mstro_symtab_entry_key to make the two regions
   assignment-compatible.
*/
struct mstro_symtab_entry {
  UT_hash_handle hh; /**< hashable using specialized functions
                      * mstro_symtab_key_hash and
                      * mstro_symtab_key_equal */
  struct mstro_symtab_entry_key key; /**< the address key and string
                                      * data */
};


mstro_status
mstro_symtab_create(mstro_symtab *result)
{
  *result = NULL;
  return MSTRO_OK;
}

mstro_status
mstro_symtab_destroy(mstro_symtab *tab)
{
  if(tab==NULL) {
    return MSTRO_INVARG;
  }

  /* DEBUG("symbol table with %zu entries\n", */
  /*       HASH_COUNT(*tab)); */
  
  struct mstro_symtab_entry *entry, *tmp;
  HASH_ITER(hh, *tab, entry, tmp) {
    /* DEBUG("Removing entry %p, key %p (len %zu, perm addr %p, fullname %s) from symtab\n", */
    /*       entry, */
    /*       entry->key, entry->key.len, entry->key.permanent_address, entry->key.fullname); */
    HASH_DEL(*tab, entry);
    free(entry);
  }
  return MSTRO_OK;
}

static inline
mstro_status
mstro_symtab_intern__helper(mstro_symtab *tab, const char *string,
                            bool permanent, mstro_symbol *interned_symbol)
{
  if(string==NULL)
    return MSTRO_INVARG;

  struct mstro_symtab_entry *e;
  size_t len;

  if(permanent) {
    /* no copy of string stored */
    len = 0;
  } else {
    len = strlen(string);
  }
  
  e = malloc(sizeof(struct mstro_symtab_entry)
             +len+1);
  if(e==NULL)
    return MSTRO_NOMEM;
  
  e->key.len = len;

  if(permanent)
    e->key.permanent_address = string;
  else {
    e->key.permanent_address = NULL;
    /* we avoid strncpy here because some compilers warn that key.len is
     * computed from strlen of string */
    memcpy(&e->key.fullname[0], string, e->key.len);
    e->key.fullname[e->key.len] = '\0';
  }

  /* DEBUG("Checking whether |%s| (permanent? %d) is already present\n", string, permanent); */
  mstro_symbol sym;
  HASH_FIND(hh, *tab, &e->key, sizeof(struct mstro_symtab_entry_key), sym);

  if(sym==NULL) {
    /* add it */
    /* DEBUG("Not found, adding it at %p\n", e); */
    HASH_ADD(hh, *tab, key, sizeof(struct mstro_symtab_entry_key), e);
    sym = e;
  } else {
    /* DEBUG("already have a candiate. we: permanent? %d, string addr %p, candidate: %zu/%p/%s/%s\n", */
    /*       permanent, string, */
    /*       sym->key.len, sym->key.permanent_address, */
    /*       sym->key.permanent_address, sym->key.len==0 ? "" : sym->key.fullname); */
    if(permanent && sym->key.permanent_address==NULL) {
      /* already have a non-permanent version of it, this is an error */
      ERR("Trying to intern permanent symbol |%s|, but non-permanent one already exists (%p)\n",
          string, sym);
      free(e);
      return MSTRO_INVARG;
    }

    if(permanent && sym->key.permanent_address != string) {
      /* two different addresses for same permanent symbol, this is an error */
      ERR("Trying to intern permanent symbol |%s| (addr %p), but permanent one already exists (%p, addr %p)\n",
          string, string, sym, sym->key.permanent_address);
      free(e);
      return MSTRO_INVARG;
    }
    
    /* DEBUG("found, re-using %p\n", sym); */
    free(e);
  }

  if(interned_symbol!=NULL)
    *interned_symbol = sym;
  
  return MSTRO_OK;
}

mstro_status
mstro_symtab_intern_permanent(mstro_symtab *tab, const char *string, mstro_symbol *interned_symbol)
{
  return mstro_symtab_intern__helper(tab, string, true, interned_symbol);
}

mstro_status
mstro_symtab_intern(mstro_symtab *tab, const char *string, mstro_symbol *interned_symbol)
{
  return mstro_symtab_intern__helper(tab, string, false, interned_symbol);
}

/** Check whether STRING is present in TAB. On success: returns the
 * symbol for STRING in *result, otherwise NULL */
mstro_status
mstro_symtab_lookup(mstro_symtab tab, const char *string, mstro_symbol *result)
{
  if(tab==NULL ||string==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;

  /* lookup; it would seem nice to do alloca() here, but it's not
   * really safe as we don't want to limit the size of strings
   * permitted.  Instead, we avoid an allocation and copy by relying
   * on the key hash and key equal function using the address field
   * before looking into the fullname field. WATCH OUT.*/
  size_t len = strlen(string);
  struct mstro_symtab_entry_key x = {
    .len = len,
    .permanent_address = string,
  };
  HASH_FIND(hh, tab, &x, sizeof(struct mstro_symtab_entry_key), *result);
  return MSTRO_OK;
}

mstro_status
mstro_symtab_unintern(mstro_symtab *tab, mstro_symbol interned_symbol)
{
  if(tab==NULL ||interned_symbol==NULL)
    return MSTRO_INVARG;
  /* remove */
  HASH_DELETE(hh,*tab,interned_symbol);
  free(interned_symbol);
  return MSTRO_OK;
}


const char *
mstro_symbol_name(mstro_symbol sym)
{
  if(sym==NULL) {
    return "(null symbol)";
  } else {
    if(sym->key.permanent_address==NULL) 
      return sym->key.fullname;
    else
      return (const char*)sym->key.permanent_address;
  }
}
