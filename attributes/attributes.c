/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Attribute dictionary implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro/attributes.h"
#include "maestro/logging.h"
#include "maestro/i_khash.h"

#include <assert.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)



struct mstro_attribute_dict_item_ {
  char *key;
  void *val;
  void (*val_destructor)(void*);
  bool val_owned;
  bool copy_val_on_use;
};
typedef struct mstro_attribute_dict_item_ * mstro_attribute_dict_item;

inline mstro_status
mstro_attribute_dict_item_destroy(mstro_attribute_dict_item item)
{
  assert(item!=NULL);

  if(item->val_owned) {
    if(item->val_destructor) {
      item->val_destructor(item->val);
    }
  }
  free(item->key);
  free(item);
  return MSTRO_OK;
}


#define dict_item_hash_func(item) (item==NULL                           \
                                   ? (assert(0),0)                      \
                                   : kh_str_hash_func(item->key))
                                   
#define dict_item_hash_equal(item1, item2) ((item1==NULL || item2==NULL) \
                                            ? (assert(0),0)             \
                                            : kh_str_hash_equal(item1->key, item2->key))

KHASH_INIT(dict_item_set, mstro_attribute_dict_item, /* dummy */ char, 0, \
           dict_item_hash_func, dict_item_hash_equal);


  
struct mstro_attribute_dict_ {
  khash_t(dict_item_set) *ht;
};


mstro_status
mstro_cdo_attributes_create(void *reserved_attributes,
                            mstro_cdo_attributes* result)
{
  if(reserved_attributes!=NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;
  
  *result = malloc(sizeof(struct mstro_attribute_dict_));

  if(result==NULL)
    return MSTRO_NOMEM;

  /* initialize HT */
  (*result)->ht = kh_init(dict_item_set);

  if((*result)->ht==NULL) {
    free(*result);
    *result=NULL;
    return MSTRO_NOMEM;
  }

  return MSTRO_OK;
}

 
mstro_status
mstro_cdo_attributes_add(mstro_cdo_attributes attr,
                         const char *key,
                         void *val,
                         bool val_owned,
                         void (*val_destructor)(void*),
                         bool copy_val_on_use)
{
  if(attr==NULL)
    return MSTRO_INVARG;
  if(key==NULL)
    return MSTRO_INVARG;

  struct mstro_attribute_dict_item_ *item = malloc(sizeof(struct mstro_attribute_dict_item_));
  if(item==NULL) {
    return MSTRO_NOMEM;
  }

  item->key = strdup(key);
  if(item->key==NULL) {
    free(item);
    return MSTRO_NOMEM;
  }
  
  item->val = val;
  item->val_owned = val_owned;
  item->val_destructor = val_destructor;
  item->copy_val_on_use = copy_val_on_use;

  /* insert */
  khint_t pos;
  int absent;
  pos = kh_put(dict_item_set, attr->ht, item, &absent);
  if (!absent) {
    /* table was not modified, so we can abort without disruption */
    ERR("Key %s already present at slot %ld in attribute table at %p, can't add again\n",
        key, (long)pos, attr);
    free(item->key);
    free(item);
    return MSTRO_NOT_TWICE;
  }

  return MSTRO_OK;
}

/** Destroy an attribute dictionary */
mstro_status
mstro_cdo_attributes_destroy(mstro_cdo_attributes attr)
{
  if(attr==NULL)
    return MSTRO_INVARG;

  khint_t k;
  khash_t(dict_item_set) *h = attr->ht;
  
  for (k = 0; k < kh_end(h); ++k)
    if (kh_exist(h, k)) {
      mstro_attribute_dict_item item = kh_key(h,k);
      mstro_attribute_dict_item_destroy(item);
    }

  kh_destroy(dict_item_set, attr->ht);
  
  free(attr);
  
  return MSTRO_OK;
}
