/* -*- mode:c -*- */
/** @file
 ** @brief implementing the Maestro schema-schema in cyaml

 * This should always be kept in sync (manually, unfortunately) with
 * attributes/maestro-schema-schema.yaml
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include <pthread.h>
#include <string.h>

#include <cyaml/cyaml.h>

#include "yaml.h"

#include "maestro.h"
#include "maestro/logging.h"
#include "maestro/attributes.h"
#include "maestro-schema.h"
#include "schema_type_parse.h"
#include "symtab.h"
#include "maestro/i_utlist.h"

#include "maestro/i_uthash.h"
#include "maestro/i_utstack.h"
#include "maestro/i_base64.h"

#include <stdbool.h>

#include <regex.h>
#include <stdatomic.h>
#include <assert.h>

#include "protocols/mstro_pool.pb-c.h"

#include <ctype.h>

/* posix file and mmap */
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#ifndef MAX
#define MAX(x,y) ((x)<(y) ? (y) : (x))
#endif

/* hard-coded versions of the schemata, so that we do not have to parse a file at startup if we don't want to */
#include "maestro-core-yaml.h"
#include "ecmwf-yaml.h"


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)



#define NUM_BUILTIN_SCHEMATA 2
const struct {
  unsigned char *schema;
  uint64_t       schema_size;
} builtin_schema_texts[NUM_BUILTIN_SCHEMATA] =
{ /* be sure to keep the maestro core one first */
  [0] = { .schema = maestro_core_yaml, .schema_size = maestro_core_yaml_len},
  [1] = { .schema = ecmwf_yaml, .schema_size = ecmwf_yaml_len},
};

const unsigned char *MSTRO_SCHEMA_BUILTIN_YAML_CORE      = maestro_core_yaml;
const size_t         MSTRO_SCHEMA_BUILTIN_YAML_CORE_LEN  = maestro_core_yaml_len;

const unsigned char *MSTRO_SCHEMA_BUILTIN_YAML_ECMWF     = ecmwf_yaml;
const size_t         MSTRO_SCHEMA_BUILTIN_YAML_ECMWF_LEN = ecmwf_yaml_len;


/* a placeholder variable that can be used to indicate an invalid pointer */
static char g_unique_ptr;
/* a valid pointer that corresponds to the 'unset default value' so
 * that we can distinguish and intentional NULL default value from an
 * unset value */
static void * MSTRO_SCHEMA_DEFAULT_VAL_UNSET = &g_unique_ptr;

/** The C data structures to hold a parsed schema
 **/

/** user-defined type value specification */
struct mstro_schema_type_val {
  char *val;
};

/** mapping definition for user-defined schema type */
/* static const cyaml_schema_field_t */
/* csf_schema_type_val_mapping[] = { */
/*   CYAML_FIELD_STRING_PTR("val", */
/*                          CYAML_FLAG_DEFAULT, */
/*                          struct mstro_schema_type_val, val, 0, CYAML_UNLIMITED), */
/*   CYAML_FIELD_END */
/* }; */

/** schema type  definition for schema-schema.yaml(for sequences of these entries later) */
static const cyaml_schema_value_t
csv_schema_type_val_entry = {
  CYAML_VALUE_STRING(CYAML_FLAG_POINTER,
                     char, 0, CYAML_UNLIMITED)/* MAPPING(CYAML_FLAG_POINTER, */
                      /* struct mstro_schema_type_val, */
                      /* csf_schema_type_val_mapping),                       */
};


/** user-defined schema values */
struct mstro_schema_type_ {
  UT_hash_handle hh;         /**< hashable by type_symbol */
  char *typename;            /**< "name: str()" */
#define TYPENAME_PATTERN ".*"
#define TYPENAME_PATTERN_CFLAGS (REG_EXTENDED|REG_ICASE)
#define TYPENAME_PATTERN_EFLAGS 0
  mstro_symbol type_symbol;  /**< symbol created for this typename; key when hashing */
  char *unparsed_typespec;   /**< "typespec: include('maestro-user-typespec')", unparsed */
  char *documentation;       /**< "documentation: str()" */

  struct mstro_stp_val *parsed_type; /**< parsed type handle */

};

/** mapping definition for user-defined schema type */
static const cyaml_schema_field_t
csf_schema_type_mapping[] = {
  CYAML_FIELD_STRING_PTR("typename",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_type_, typename, 0, CYAML_UNLIMITED),
  CYAML_FIELD_STRING_PTR("typespec",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_type_, unparsed_typespec, 0, CYAML_UNLIMITED),
  CYAML_FIELD_STRING_PTR("documentation",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_type_, documentation, 0, CYAML_UNLIMITED),
  CYAML_FIELD_END
};

/** schema type  definition for schema-schema.yaml(for sequences of these entries later) */
static const cyaml_schema_value_t
csv_schema_type_entry = {
  CYAML_VALUE_MAPPING(CYAML_FLAG_DEFAULT,
                      struct mstro_schema_type_,
                      csf_schema_type_mapping),
};

struct mstro_schema_attribute_type_parse_closure {
  struct mstro_stp_val *info;
};

/** attribute entry data structure */
struct mstro_schema_attribute_ {
  /* not set by CYAML parse: */
  UT_hash_handle hh; /**< hashable by key_symbol */
  void *defaultval; /**< parsed defaultval_string */
  size_t defaultval_size; /**< size of defaultval */
  mstro_symbol key_symbol; /**< symbol for key; key in the hash table */
  struct mstro_schema_attribute_type_parse_closure type_parse_closure; /**< information for parsing values of this type */
  /* these slots get filled by CYAML parse */
  char *key; /**<   key: regex('(^(\.[^\s\.]+)+$)|(^[^\s\.]+(\.[^\s\.]+)*$)',
                required=False,
                none=False, multiline=False,
                name='valid attribute namestring') */
#define ATTRIBUTE_KEY_PATTERN "(^(\\.[^[:space:].]+)+$)|(^[^[:space:].]+(\\.[^[:space:].]+)*$)"
#define ATTRIBUTE_KEY_PATTERN_CFLAGS (REG_EXTENDED|REG_ICASE)
#define ATTRIBUTE_KEY_PATTERN_EFLAGS 0
  char *typespec;   /**< type: include('maestro-user-typespec') */
  bool required; /**<  required: bool() */

  /* struct mstro_schema_type_val *defaultval; /\**< Any(include('maestro-builtin-typeval'), */
  /*                                              include('maestro-user-typeval'), */
  /*                                              required=False, none=False) *\/ */
  char * defaultval_string; /**< string form of default value */
  char *documentation; /**<  documentation: str(multiline=True) */
};

static const cyaml_schema_field_t
csf_schema_attribute_mapping[] = {
  CYAML_FIELD_STRING_PTR("key",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_attribute_, key, 0, CYAML_UNLIMITED),
  CYAML_FIELD_STRING_PTR("type",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_attribute_, typespec, 0, CYAML_UNLIMITED),
  CYAML_FIELD_BOOL("required",
                   CYAML_FLAG_DEFAULT,
                   struct mstro_schema_attribute_, required),
  /* because we're lazy we store it as a string currently and parse it at instantiation time
     It's actually easier that way, since complicated user-defined types' values can't be parsed by cyaml */
  CYAML_FIELD_STRING_PTR("default",
                         CYAML_FLAG_DEFAULT|CYAML_FLAG_OPTIONAL,
                         struct mstro_schema_attribute_, defaultval_string, 0, CYAML_UNLIMITED),
  CYAML_FIELD_STRING_PTR("documentation",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_attribute_, documentation, 0, CYAML_UNLIMITED),
  CYAML_FIELD_END
};

static const cyaml_schema_value_t
csv_schema_attributes_entry = {
  CYAML_VALUE_MAPPING(CYAML_FLAG_DEFAULT,
                      struct mstro_schema_attribute_,
                      csf_schema_attribute_mapping),
};



/** externally visible: a full schema */
struct mstro_schema_ {
  char    *schema_name;      /**< "schema-name: str()" */

  uint64_t schema_version;   /**< "schema-version: int(min=0) */

  char    *schema_namespace; /**< "schema-namespace: regex('(^$)|(^\.$)|(^(\.[^\s]+)+\.$)', ... */
#define NAMESPACE_PATTERN "(^$)|(^\\.$)|(^(\\.[^[:space:]]+)+\\.$)"
#define NAMESPACE_PATTERN_CFLAGS (REG_EXTENDED|REG_ICASE)
#define NAMESPACE_PATTERN_EFLAGS 0

  struct mstro_schema_type_ *schema_types;             /**< list(include('maestro-user-type-def'), required=False) */
  size_t                     schema_types_count;       /**< number of entries in schema_types array */

  struct mstro_schema_type_val  *schema_type_vals;    /**< list(include('maestro-user-typespec'), required=False) */
  size_t                     schema_type_vals_count;   /**< number of entries in schema_type_vals array */

  struct mstro_schema_attribute_ *schema_attributes;   /**< list(include('maestro-attribute-def'), required=False) */
  size_t                     schema_attributes_count;  /**< number of entries in schema_attributes array */

  /* components filled at instantiation time */
  mstro_symtab              symtab;                   /**< symbol table for all legal attribute names in the schema */
  struct mstro_schema_type_ *type_table; /**< hash table of type
                                          * definitions */
  struct mstro_schema_attribute_ *attribute_table; /**< hash table of
                                                   * attribute
                                                   * definitions */

  struct mstro_schema_ *next; /** is also a linked list: schema
                               * merging works by pushing down the
                               * merged schema here. */
  pthread_rwlock_t lock; /**< lock for schema: Typically there is
                          * little read/write competition, and reads
                          * are more frequent */
};

/** Execute body with read-locked schema */
#define WITH_LOCKED_SCHEMA_READ(schemaptr,...)                          \
  do {                                                                  \
    int _wlsr_err = pthread_rwlock_rdlock(&(schemaptr)->lock);          \
    if(_wlsr_err!=0) {                                                  \
      ERR("Failed to read-lock schema %p: %d (%s)\n", (schemaptr),      \
          _wlsr_err, strerror(_wlsr_err));                              \
    } else {                                                            \
      do { __VA_ARGS__; } while(0);                                     \
      _wlsr_err = pthread_rwlock_unlock(&(schemaptr)->lock);            \
      if(_wlsr_err!=0) {                                                \
        ERR("Failed to read-unlock schema %p: %d (%s)\n", (schemaptr),  \
            _wlsr_err, strerror(_wlsr_err));                            \
      }                                                                 \
    }                                                                   \
  } while(0)

/** Execute body with write-locked schema */
#define WITH_LOCKED_SCHEMA_WRITE(schemaptr,...)                         \
  do {                                                                  \
    int _wlsw_err = pthread_rwlock_wrlock(&(schemaptr)->lock);          \
    if(_wlsw_err!=0) {                                                  \
      ERR("Failed to write-lock schema %p: %d (%s)\n", (schemaptr),     \
          _wlsw_err, strerror(_wlsw_err));                              \
    } else {                                                            \
      do { __VA_ARGS__; } while(0);                                     \
      _wlsw_err = pthread_rwlock_unlock(&(schemaptr)->lock);            \
      if(_wlsw_err!=0) {                                                \
        ERR("Failed to write-unlock schema %p: %d (%s)\n", (schemaptr), \
            _wlsw_err, strerror(_wlsw_err));                            \
      }                                                                 \
    }                                                                   \
  } while(0)

/** Execute body with write-locked schema, upgrading from exsiting read-lock */
#define WITH_LOCKED_SCHEMA_WRITE_UPGRADE(schemaptr,...)                 \
  do {                                                                  \
  int _wlsu_err = pthread_rwlock_unlock(&(schemaptr)->lock);            \
  if(_wlsu_err!=0) {                                                    \
    ERR("Failed to unlock for upgrading to write-lock schema %p: %d (%s)\n", \
        (schemaptr), _wlsu_err, strerror(_wlsu_err));                   \
  } else {                                                              \
    WITH_LOCKED_SCHEMA_WRITE(schemaptr,__VA_ARGS__);                    \
    _wlsu_err = pthread_rwlock_rdlock(&(schemaptr)->lock);              \
    if(_wlsu_err!=0) {                                                  \
      ERR("Failed re-aquire read-lock schema %p: %d (%s)\n",            \
          (schemaptr), _wlsu_err, strerror(_wlsu_err));                 \
    }                                                                   \
  }                                                                     \
  } while(0)


static inline
mstro_status
mstro_schema_type__dispose(struct mstro_schema_type_ *t)
{
  if(t==NULL)
    return MSTRO_INVARG;

  if(t->typename)
    free(t->typename);
  if(t->unparsed_typespec)
    free(t->unparsed_typespec);
  if(t->documentation)
    free(t->documentation);
  if(t->parsed_type)
    mstro_stp_val_dispose(t->parsed_type);
  free(t);
  return MSTRO_OK;
}

mstro_status
mstro_schema_free(mstro_schema sch)
{
  if(sch==NULL)
    return MSTRO_INVARG;

  DEBUG("deallocating schema at %p (%s)\n", sch, sch->schema_name);

  mstro_schema next=sch->next; /* save for tail recursion */

  if(sch->schema_name) {
    free(sch->schema_name);
  }

  if(sch->schema_namespace) {
    free(sch->schema_namespace);
  }

  if(sch->schema_types) {
    for(size_t i=0; i<sch->schema_types_count; i++) {
      struct mstro_schema_type_ *s = &sch->schema_types[i];
      free(s->typename);
      mstro_symtab_unintern(&sch->symtab, s->type_symbol);
      free(s->unparsed_typespec);
      if(s->documentation)
        free(s->documentation);
      if(s->parsed_type)
        mstro_stp_val_dispose(s->parsed_type);
    }
    free(sch->schema_types);
  }

  if(sch->schema_type_vals) {
    for(size_t i=0; i<sch->schema_type_vals_count; i++) {
      struct mstro_schema_type_val *stv = &sch->schema_type_vals[i];
      if(stv->val)
        free(stv->val);
      free(stv);
    }
    free(sch->schema_type_vals);
  }

  if(sch->attribute_table) {
    struct mstro_schema_attribute_ *el,*tmp;
    HASH_ITER(hh, sch->attribute_table, el, tmp) {
      DEBUG("Deleting attribute %s from schema %p attribute table\n",
            el->key, sch);
      HASH_DELETE(hh, sch->attribute_table, el);
    }
  }
  if(sch->schema_attributes) {
    for(size_t i=0; i<sch->schema_attributes_count; i++) {
      struct mstro_schema_attribute_ * sa = &sch->schema_attributes[i];
      /* DEBUG("Schema attribute idx %zu/%zu, att %p\n", i, sch->schema_attributes_count, sa); */
      /* FIXME: key_symbol and ensuring no hash table still refers to sa */
      if(sa->defaultval!=NULL && sa->defaultval!=MSTRO_SCHEMA_DEFAULT_VAL_UNSET) {
        free(sa->defaultval);
      }
      if(sa->type_parse_closure.info)
        mstro_stp_val_dispose(sa->type_parse_closure.info);
      if(sa->key)
        free(sa->key);
      if(sa->typespec)
        free(sa->typespec);
      if(sa->defaultval_string)
        free(sa->defaultval_string);
      if(sa->documentation)
        free(sa->documentation);
      /* the individual sa pointers are not to be freed, since all are
       * allocated in one chunk by cyaml */
    }
    free(sch->schema_attributes);
  }

  {
    struct mstro_schema_type_ *el,*tmp;
    HASH_ITER(hh, sch->type_table, el, tmp) {
      DEBUG("Deleting schema type %s from schema %p type table\n",
            el->typename, sch);
      HASH_DELETE(hh, sch->type_table, el);
      mstro_schema_type__dispose(el);
    }
  }
  mstro_symtab_destroy(&sch->symtab);

  int err= pthread_rwlock_destroy(&sch->lock);
  if(err!=0) {
    ERR("Failed to destroy schema lock: %d (%s)\n", err, strerror(err));
    return MSTRO_FAIL;
  }

  free(sch);

  if(next)
    return mstro_schema_free(next);
  else
    return MSTRO_OK;
}

/** Return the name of a schema. */
const char *
mstro_schema_name(mstro_schema sch)
{
  if(sch==NULL)
    return "(null)";
  else {
    const char *name=NULL;
    WITH_LOCKED_SCHEMA_READ(sch, {
        name = sch->schema_name;
      });
    return name;
  }
}

/** Return the version of a schema. */
uint64_t
mstro_schema_version(mstro_schema sch)
{
  if(sch==NULL)
    return UINT64_MAX;
  else {
    uint64_t version=UINT64_MAX;
    WITH_LOCKED_SCHEMA_READ(sch, {
      version = sch->schema_version;
      });

    return version;
  }
}




/** The toplevel CYAML schema for maestro-schema-schema */
static const cyaml_schema_field_t
csf_top_mapping_schema[] = {
  CYAML_FIELD_STRING_PTR("schema-name",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_, schema_name, 0, CYAML_UNLIMITED),
  CYAML_FIELD_UINT(      "schema-version",
                         CYAML_FLAG_DEFAULT,
                         struct mstro_schema_, schema_version),
  CYAML_FIELD_STRING_PTR("schema-namespace",
                         CYAML_FLAG_OPTIONAL,
                         struct mstro_schema_, schema_namespace, 0, CYAML_UNLIMITED),
  CYAML_FIELD_SEQUENCE(  "schema-types",
                         CYAML_FLAG_POINTER|CYAML_FLAG_OPTIONAL,
                         struct mstro_schema_, schema_types, &csv_schema_type_entry, 0, CYAML_UNLIMITED),
  CYAML_FIELD_SEQUENCE(  "schema-type-values",
                         CYAML_FLAG_POINTER|CYAML_FLAG_OPTIONAL,
                         struct mstro_schema_, schema_type_vals, &csv_schema_type_val_entry, 0, CYAML_UNLIMITED),
  CYAML_FIELD_SEQUENCE(  "maestro-attributes",
                         CYAML_FLAG_POINTER|CYAML_FLAG_OPTIONAL,
                         struct mstro_schema_, schema_attributes, &csv_schema_attributes_entry, 0, CYAML_UNLIMITED),
  CYAML_FIELD_END
};


static const
cyaml_schema_value_t
maestro_schema_schema = {
  CYAML_VALUE_MAPPING(CYAML_FLAG_POINTER,
                      struct mstro_schema_,  csf_top_mapping_schema),
};


/* Validators for the values */

/** check a string against a regex. Returns MSTRO_NOMATCH if not matching. */
static inline
mstro_status
mstro_schema__check_regex(const char *string,
                          regex_t *regex,
                          int eflags)
{
  int s = regexec(regex, string, 0, NULL, eflags);
  if(s==0) {
    return MSTRO_OK;
  } else if(s==REG_NOMATCH) {
    return MSTRO_NOMATCH;
  } else {
    char errbuf[1024];
    regerror(s,regex,errbuf,1024);
    ERR("Failure in regexec: %d (%s)\n",
        s, errbuf);
    return MSTRO_FAIL;
  }
}
static inline
mstro_status
mstro_schema__ensure_regex(const char *pattern,
                           int cflags,
                           _Atomic(regex_t*) *dst)
{
  if(pattern==NULL || dst==NULL)
    return MSTRO_INVARG;

  if(atomic_load(dst)==NULL) {
    /* compile regex and store. */
    regex_t *re = malloc(sizeof(regex_t));
    if(re==NULL) {
      return MSTRO_NOMEM;
    }
    int s = regcomp(re, pattern, cflags);
    if(s!=0) {
      char errbuf[1024];
      regerror(s, re, errbuf, 1024);
      ERR("Failed to compile regex: %d (%s)\n",
          s, errbuf);
      return MSTRO_FAIL;
    }

    regex_t *tmp = NULL;
    if(false==atomic_compare_exchange_strong(dst, &tmp, re)) {
      /* someone else added one: let them win, kill ours */
      DEBUG("Concurrent regex compilation wins against us\n");
      regfree(re);
      free(re);
    }
  }
  assert(atomic_load(dst)!=NULL);
  return MSTRO_OK;
}


/** the regular expression (compiled) for namespace
 * checks. Initialized on first use, FIXME: leaked until we have a
 * schema_infrastructure cleanup function */
static
_Atomic(regex_t*) namespace_regex = NULL;

static inline
mstro_status
mstro_schema__validate_namespace(const char *string)
{
  mstro_status s;
  s = mstro_schema__ensure_regex(NAMESPACE_PATTERN,
                                 NAMESPACE_PATTERN_CFLAGS,
                                 &namespace_regex);
  if(s!=MSTRO_OK) {
    ERR("Failed to ensure namespace regex: %d (%s)\n",
        s, mstro_status_description(s));
    return s;
  }
  //bool namespace_ok;
  s = mstro_schema__check_regex(string,
                                namespace_regex,
                                NAMESPACE_PATTERN_EFLAGS);
  if(s!=MSTRO_OK) {
    ERR("Failed to validate namespace\n");
    if(s==MSTRO_NOMATCH) {
      DEBUG("string |%s| failed to match |%s|\n",
            string, NAMESPACE_PATTERN);
    }
    return s;
  } else {
    DEBUG("Namespace validated\n");
  }
  return MSTRO_OK;
}


/** the regular expression (compiled) for typename
 * checks. Initialized on first use, FIXME: leaked until we have a
 * schema_infrastructure cleanup function */
static
_Atomic(regex_t*) typename_regex = NULL;

static inline
mstro_status
mstro_schema__validate_typename(const char *string)
{
  mstro_status s;
  s = mstro_schema__ensure_regex(TYPENAME_PATTERN,
                                 TYPENAME_PATTERN_CFLAGS,
                                 &typename_regex);
  if(s!=MSTRO_OK) {
    ERR("Failed to ensure typename regex: %d (%s)\n",
        s, mstro_status_description(s));
    return s;
  }
  s = mstro_schema__check_regex(string,
                                typename_regex,
                                TYPENAME_PATTERN_EFLAGS);
  if(s!=MSTRO_OK) {
    ERR("Failed to validate typename\n");
    if(s==MSTRO_NOMATCH) {
      DEBUG("string |%s| failed to match |%s|\n",
            string, TYPENAME_PATTERN);
    }
    return s;
  }
  return MSTRO_OK;
}


/** the regular expression (compiled) for attribute key
 * checks. Initialized on first use, FIXME: leaked until we have a
 * schema_infrastructure cleanup function */
static
_Atomic(regex_t*) attribute_key_regex = NULL;

static inline
mstro_status
mstro_schema__validate_attribute_key(const char *string)
{
  mstro_status s;
  s = mstro_schema__ensure_regex(ATTRIBUTE_KEY_PATTERN,
                                 ATTRIBUTE_KEY_PATTERN_CFLAGS,
                                 &attribute_key_regex);
  if(s!=MSTRO_OK) {
    ERR("Failed to ensure attribute key regex: %d (%s)\n",
        s, mstro_status_description(s));
    return s;
  }
  //bool attribute_key_ok;
  s = mstro_schema__check_regex(string,
                                attribute_key_regex,
                                ATTRIBUTE_KEY_PATTERN_EFLAGS);
  if(s!=MSTRO_OK) {
    ERR("Failed to validate attribute key\n");
    if(s==MSTRO_NOMATCH) {
      DEBUG("string |%s| failed to match |%s|\n",
            string, ATTRIBUTE_KEY_PATTERN);
    }
    return s;
  }
  return MSTRO_OK;
}




/** cyaml parser configuration */
/* FIXME: replace log functions */
static const cyaml_config_t
mstro_cyaml_config = {
  //.log_level = CYAML_LOG_DEBUG, /* Logging errors and warnings only. */
  .log_level = CYAML_LOG_ERROR, /* Logging errors and warnings only. */
  .log_fn = cyaml_log,            /* Use the default logging function. */
  .mem_fn = cyaml_mem,            /* Use the default memory allocator. */
};





/* look at the typespec string and parse it; fill in information that
 * helps us parse values of this type whenever needed later */
static inline
mstro_status
mstro_schema_attribute__install_type_parser(struct mstro_schema_attribute_ *attr)
{
  mstro_status s = mstro_schema_type_parse(attr->typespec, &attr->type_parse_closure.info);
  if(s!=MSTRO_OK) {
    if(attr->type_parse_closure.info && attr->type_parse_closure.info->kind == MSTRO_STP_ERROR) {
      ERR("Error parsing type |%s|, error message %s\n",
          attr->typespec, attr->type_parse_closure.info->errmsg);
    } else {
      ERR("error parsing built-in type: %d (no parser error message available)\n",
          s);
    }
  }
  return s;
}

/* compute the space needed to store the value of STP_VAL arising from
* parsing STRING or inspecting VAL.  Only one of the two may be
* non-NULL. */
static inline
mstro_status
mstro_attribute_val__compute_size(enum mstro_stp_val_kind kind,
                                  const char *string,
                                  void *val,
                                  size_t *val_size)
{
  if(kind==MSTRO_STP_ERROR) {
    ERR("Invalid stp val\n");
    return MSTRO_INVARG;
  }
  if((string==NULL && val==NULL) && (kind == MSTRO_STP_BLOB)) {
    ERR("not both src args may be NULL when kind is MSTRO_STP_BLOB \n");
    return MSTRO_INVARG;
  }
  if(string!=NULL && val!=NULL) {
    ERR("not both src args may be non-NULL\n");
    return MSTRO_INVARG;
  }
  if(val_size==NULL) {
    ERR("Invalid output destination\n");
    return MSTRO_INVOUT;
  }
  assert((string==NULL && val!=NULL) || (string!=NULL && val==NULL) || (kind !=MSTRO_STP_BLOB));

  mstro_status status = MSTRO_OK;

  switch(kind) {
    case MSTRO_STP_BOOL:
      *val_size = sizeof(bool); break;
    case MSTRO_STP_UINT:
      *val_size = sizeof(uint64_t); break;
    case MSTRO_STP_INT:
      *val_size = sizeof(int64_t); break;
    case MSTRO_STP_FLOAT:
      *val_size = sizeof(float); break;
    case MSTRO_STP_DOUBLE:
      *val_size = sizeof(double); break;
    case MSTRO_STP_STR:
    case MSTRO_STP_REGEX:
      if(string!=NULL)
        *val_size = strlen(string)+1;
      else {
	assert(val!=NULL);
        *val_size = strlen((char*)val)+1;
      }
      break;
    case MSTRO_STP_TIMESTAMP:
      *val_size = sizeof(mstro_timestamp);
      break;
    case MSTRO_STP_BLOB:
      if(string!=NULL) {
        /* expect base64 encoded string */
        mstro_blob *b;
        size_t l=strlen(string);
        if(l==0) {
          *val_size = 0; /* base64decode("")== 'empty buffer' */
        } else  {
          unsigned char *r = base64_decode((const unsigned char*)string, strlen(string),
                                           &l);
          if(r==NULL) {
            ERR("Failed to decode the BLOB default value %s", string);
            status = MSTRO_FAIL;
          } else {
            free(r);
            *val_size = l;
            status = MSTRO_OK;
          }
        }
      } else  {
        *val_size = ((mstro_blob*)val)->len;
      }
      break;
    case MSTRO_STP_MMBLAYOUT:
      *val_size = sizeof(mmbLayout);
      break;
    case MSTRO_STP_POINTER:
      *val_size = sizeof(void*);
      break;
    default:
      ERR("Unexpected parsed type %d\n", kind);
      status=MSTRO_UNIMPL;
  }
  return status;
}

#define POINTER_PATTERN ("NIL|NULL|((0x)?[0-9]+)")
/* parse a string according to type specification of the attribute.
 * Store freshly-allocated value in **val_p, and the size in *val_size.
 */
static inline
mstro_status
mstro_attribute_val_parse(const struct mstro_stp_val *parsed_type, const char *string,
                          void **val_p, size_t *val_size)
{
  if(parsed_type==NULL ||string==NULL) {
    ERR("Invalid input args\n");
    return MSTRO_INVARG;
  }
  if(val_p==NULL||val_size==NULL) {
    ERR("Invalid output args\n");
    return MSTRO_INVOUT;
  }

  mstro_status s=MSTRO_OK;


  s =mstro_attribute_val__compute_size(parsed_type->kind, string, NULL, val_size);
  if(s!=MSTRO_OK) {
    ERR("Failed to compute value size\n");
    return s;
  }

  /* all built-ins recognized by built-in regexps for now */
  size_t num_re;
  if(parsed_type->kind == MSTRO_STP_REGEX) {
    num_re = parsed_type->regex_numpatterns;
  } else {
    num_re = 1;
  }
  regex_t regex[num_re];
  int err=0;
  bool need_regmatch = true; /* except for BLOB we need it */
  size_t val_len = strlen(string);
  size_t minlen =0, maxlen=val_len; /* might be changed for strings */

  switch(parsed_type->kind) {
    case MSTRO_STP_BOOL: {
      err = regcomp(&(regex[0]), "1|on|yes|true|0|off|no|false",REG_ICASE|REG_NOSUB|REG_EXTENDED);
      break;
    }
    case MSTRO_STP_UINT: {
      err = regcomp(&(regex[0]), "[+]?[[:blank:]]*[0-9]+",REG_EXTENDED|REG_NOSUB);
      DEBUG("Not checking numeric bounds on types\n");
      break;
    }
    case MSTRO_STP_INT: {
      err = regcomp(&(regex[0]), "[-+]?[[:blank:]]*[0-9]+",REG_EXTENDED|REG_NOSUB);
      DEBUG("Not checking numeric bounds on types\n");
      break;
    }
    case MSTRO_STP_FLOAT: {
      err = regcomp(&(regex[0]), "[-+]?[[:blank:]]*[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?",REG_EXTENDED);
      DEBUG("Not checking numeric bounds on types\n");
      break;
    }
    case MSTRO_STP_DOUBLE: {
      err = regcomp(&(regex[0]), "[-+]?[[:blank:]]*[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?",REG_EXTENDED);
      DEBUG("Not checking numeric bounds on types\n");
      break;
    }
    case MSTRO_STP_STR: {
      minlen = parsed_type->str_minlen;
      maxlen = parsed_type->str_maxlen;
      char *re;
      if(parsed_type->str_excludedchars) {
        size_t l = strlen(parsed_type->str_excludedchars)+strlen("[^]*") + 1;
        re=malloc(sizeof(char)*l);
        if(re) {
          re[0]='['; re[1]='^';
          strcpy(re+2,parsed_type->str_excludedchars);
          re[l-3] = ']'; re[l-2]='*'; re[l-1] = '\0';
        }
      } else {
        re = strdup(".*");
      }
      if(re==NULL) {
        ERR("Failed to allocate string regex\n");
        s=MSTRO_NOMEM;
        goto BAILOUT;
      }
      err = regcomp(&(regex[0]), re, REG_NOSUB);
      free(re);
      break;
    }
    case MSTRO_STP_REGEX:
      for(size_t i=0; i<num_re; i++) {
        err |= regcomp(&(regex[i]), parsed_type->regex_patterns[i],
                       REG_EXTENDED|REG_NOSUB| (parsed_type->regex_ignorecase ? REG_ICASE : 0));
      }
      break;
    case MSTRO_STP_BLOB:
      need_regmatch = false;
      minlen = parsed_type->blob_minlen;
      maxlen = parsed_type->blob_maxlen;
      break;
    case MSTRO_STP_MMBLAYOUT:
      need_regmatch = false;
      minlen = parsed_type->mmblayout_minlen;
      maxlen = parsed_type->mmblayout_maxlen;
      break;
    case MSTRO_STP_TIMESTAMP:
      err = regcomp(&(regex[0]), RFC3339_PATTERN, REG_EXTENDED);
      break;

    case MSTRO_STP_POINTER:
      err = regcomp(&(regex[0]), POINTER_PATTERN, REG_EXTENDED|REG_ICASE);
      break;

    default:
      ERR("Unexpected parsed type %d\n", parsed_type->kind);
      s=MSTRO_UNIMPL;
  }
  if(err) {
    ERR("Failed to construct regex\n");
    // regerror ...
    goto BAILOUT;
  }

  if(! (minlen<=val_len  && val_len <=maxlen)) {
    ERR("Argument |%s| (strlen %zu) not within length bounds for type: min=%zu, max=%zu\n",
        string, val_len, minlen, maxlen);
    s=MSTRO_FAIL;
    goto BAILOUT;
  }

  s=MSTRO_OK;
  if(need_regmatch) {
    s=MSTRO_NOMATCH;
    for(size_t i=0; i<num_re; i++) {
      s = mstro_schema__check_regex(string, &(regex[i]), 0);
      /* DEBUG("Checked regex against |%s|, result %d (%s)\n", val, s, mstro_status_description(s)); */
      if(s==MSTRO_OK)
        break;
      /* NOMATCH: try further */
      if(s==MSTRO_NOMATCH) {
        continue;
      } else {
        break;
      }
    }
    for(size_t i=0; i<num_re; i++) {
      regfree(&regex[i]);
    }
  }

  *val_p = malloc(*val_size);
  if(*val_p==NULL) {
    ERR("Failed to allocate for attribute value\n");
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }
  DEBUG("VAL-PARSE allocation %p\n", *val_p);

  /* numeric bound checking should happen in this switch. All the info is in the stp_val */
  void *dst=*val_p; /* to make the assignments and casts below less messy wrt. '**' */
  if(s==MSTRO_OK) {
    switch(parsed_type->kind) {
      case MSTRO_STP_BOOL: {
        /* we are alredy checked by regex above */
        switch(tolower(string[0])) {
          case 't': // TRUE
          case '1': // 1
          case 'o': // ON
          case 'y': // YES
            *(bool*)dst = true;
            break;
          default:
            *((bool*)dst) = false;
            break;
        }
        break;
      }
      case MSTRO_STP_UINT:
        *((uint64_t*)dst) = strtoumax((const char*)string, NULL, 10);
        break;

      case MSTRO_STP_INT:
        *((int64_t*)dst)  = strtoimax(string, NULL, 10);
        break;

      case MSTRO_STP_FLOAT:
        *((float*)dst) = strtof(string, NULL);
        break;

      case MSTRO_STP_DOUBLE:
        *((double*)dst) = strtod(string, NULL);
        break;

      case MSTRO_STP_STR:
      case MSTRO_STP_REGEX:
        strcpy((char*)dst, string);
        ((char*)dst)[(*val_size)-1] = '\0';
        break;

      case MSTRO_STP_BLOB:
        memcpy((char*)dst, string, *val_size);
        break;

      case MSTRO_STP_MMBLAYOUT:
        s = mstro_mmbLayout_parse(string, (mmbLayout **) &dst);
        if(s!=MSTRO_OK) {
          ERR("Failed to parse mmbLayout object |%s| \n", string);
        }
        break;

      case MSTRO_STP_TIMESTAMP:
        s=mstro_timestamp_parse(string, *val_size, (mstro_timestamp*)dst);
        if(s!=MSTRO_OK) {
          ERR("Failed to parse timestamp value |%s| as rfc3339-timestamp\n");
        }
        break;
      case MSTRO_STP_POINTER: {
        free(*val_p); /* POINTER does not need extra space, we use the
                       * caller's cell directly */
        uintptr_t val;
        if(strcasecmp(string, "NIL")==0)
          val=0;
        else if(strcasecmp(string, "NULL")==0)
          val=0;
        else
          val= strtoumax((const char*)string, NULL, 0);
        *val_p = (void*)val;
        DEBUG("parsed val %s as pointer %p\n", string, *val_p);
        s=MSTRO_OK;
        break;
      }
      default:
        ERR("Unexpected parsed type %d\n", parsed_type->kind);
        s=MSTRO_UNIMPL;
    }
  }

BAILOUT:
  return s;
}


/* parse the defaultval_string and store it (in a fresh allocation) in the default slot */
static inline
mstro_status
mstro_schema_attribute__parse_defaultval(struct mstro_schema_attribute_ *attr)
{
  assert(attr->type_parse_closure.info!=NULL);
  if(attr->defaultval_string)
    return mstro_attribute_val_parse(
        attr->type_parse_closure.info, attr->defaultval_string,
        &attr->defaultval, &attr->defaultval_size);
  else {
    DEBUG("Attribute |%s| has no default value in schema\n", attr->key);
    attr->defaultval = MSTRO_SCHEMA_DEFAULT_VAL_UNSET;
    attr->defaultval_size = 0;
    return MSTRO_OK;
  }
}


/** update ACC to VAL if that is greater */
#define SAVE_MAX(acc,val) (acc) = ((acc)<(val)? (val) : (acc));

static
mstro_status
mstro_schema_validate_and_instantiate(mstro_schema schema)
{
  mstro_status s;
  char * tmpname=NULL;
  if(schema==NULL)
    return MSTRO_INVARG;
  DEBUG("validating and instantiating schema %s (V%zu)\n",
        schema->schema_name, schema->schema_version);

  /* create symbol table */
  s=mstro_symtab_create(&schema->symtab);
  if(s!=MSTRO_OK) {
    ERR("Failed to create symbol table\n");
    goto BAILOUT;
  }


  /* no check on name */
  DEBUG(" schema name      : |%s| -- ok\n", schema->schema_name);

  /* no check on version */
  DEBUG(" schema version   : |%zu| -- ok\n", schema->schema_version);

  /* check on namespace */
  if(! schema->schema_namespace) {
    /* default: empty */
    schema->schema_namespace = strdup("");
  }

  s=mstro_schema__validate_namespace(schema->schema_namespace);
  if(s!=MSTRO_OK) {
    goto BAILOUT;
  }
  DEBUG(" schema namespace: |%s| -- ok\n", schema->schema_namespace);

  /* the longest possible symbol name (after adding namespace and dot)
   * will me saved in */
  size_t max_symbol_len = 0;

  /* type names */
  for(size_t i=0; i<schema->schema_types_count; i++) {
    const char *tname = schema->schema_types[i].typename;
    s=mstro_schema__validate_typename(tname);
    if(s!=MSTRO_OK) {
      ERR("Failed to validate type name |%s|\n",
          tname);
      goto BAILOUT;
    }
    SAVE_MAX(max_symbol_len, strlen(tname));
  }

  DEBUG(" %zu type names: -- ok\n", schema->schema_types_count);


  /* attribute names */
  for(size_t i=0; i<schema->schema_attributes_count; i++) {
    const char *akey = schema->schema_attributes[i].key;
    s=mstro_schema__validate_attribute_key(akey);
    if(s!=MSTRO_OK) {
      ERR("Failed to validate attribute key |%s|\n",
          akey);
      goto BAILOUT;
    }
    SAVE_MAX(max_symbol_len, strlen(akey));
  }

  DEBUG(" %zu attribute keys: -- ok\n", schema->schema_attributes_count);

  size_t namespace_len = strlen(schema->schema_namespace);
  DEBUG("longest symbol: %d characters (+%d for namespace)\n",
        max_symbol_len, namespace_len);

  max_symbol_len+=namespace_len + 1; /*  one for terminating '\0' */

  /* intern all symbols */

  /* we use a scratch buffer that is pre-populated with the namespace */
  tmpname = malloc(max_symbol_len);
  strcpy(tmpname, schema->schema_namespace);
  size_t tmpname_offset = namespace_len;
  /* namespace -- if nonempty -- already has trailing '.' (checked by validation) */

  /* FIXME: do we need all parts of the dotted-hierarchy in the symtab? For now: no */

  /* intern the names and add their definition records the appropriate hash tables */
  for(size_t i=0; i<schema->schema_types_count; i++) {
    const char *fqstr;
    struct mstro_schema_type_ *current = schema->schema_types+i;
    if(current->typename[0]=='.')
      fqstr = current->typename;
    else {
      strcpy(tmpname+tmpname_offset, current->typename);
      fqstr = tmpname;
    }
    DEBUG("Interning type-name |%s|\n", fqstr);
    s=mstro_symtab_intern(&schema->symtab, fqstr, &current->type_symbol);
    if(s!=MSTRO_OK) {
      ERR("Failed to intern type-name |%s|\n", fqstr);
      goto BAILOUT;
    }
    HASH_ADD(hh, schema->type_table, type_symbol, sizeof(mstro_symbol), current);
  }

  for(size_t i=0; i<schema->schema_attributes_count; i++) {
    const char *fqstr;
    struct mstro_schema_attribute_ *current = schema->schema_attributes+i;
    if(current->key[0]=='.') {
      fqstr = current->key;
    } else {
      strcpy(tmpname+tmpname_offset, current->key);
      fqstr = tmpname;
    }
    DEBUG("Interning attribute key |%s|\n", fqstr);
    s=mstro_symtab_intern(&schema->symtab, fqstr, &current->key_symbol);
    if(s!=MSTRO_OK) {
      ERR("Failed to intern attribute name |%s|\n", fqstr);
      goto BAILOUT;
    }

    s=mstro_schema_attribute__install_type_parser(current);
    if(s!=MSTRO_OK) {
      ERR("Failed to install type parser for %s (type %s)\n",
          mstro_symbol_name(current->key_symbol),
          current->typespec);
      goto BAILOUT;
    }

    HASH_ADD(hh, schema->attribute_table,
             key_symbol, sizeof(mstro_symbol),
             current);

    /* parse default value */
    if(!current->required)
      if(current->defaultval_string==NULL) {
        ERR("Attribute |%s| is optional, but schema provides no default value\n",
            fqstr);
        s=MSTRO_FAIL;
        goto BAILOUT;
      }

    DEBUG("Adding default value for %s\n", fqstr);
    s = mstro_schema_attribute__parse_defaultval(current);
    if(s!=MSTRO_OK) {
      ERR("Failed to parse default value |%s| for attribute |%s|\n",
          current->defaultval_string, fqstr);
      goto BAILOUT;
    }

    /* check that attribute has value type */
    DEBUG("Attribute |%s| type |%s|, required %s, default %s [parsed as %p], doc: %s\n",
          mstro_symbol_name(current->key_symbol),
          current->typespec,
          current->required ? "YES" : "NO",
          //"?"//
          current->defaultval_string ? current->defaultval_string : "(none)",
          (current->defaultval==MSTRO_SCHEMA_DEFAULT_VAL_UNSET
           ? "(unset)" : current->defaultval),
          current->documentation ? current->documentation : "(none)"
          );
  }


BAILOUT:
  if(tmpname!=NULL)
    free(tmpname);

  if(s!=MSTRO_OK) {
    if(schema->symtab) {
      mstro_symtab_destroy(&schema->symtab);
      schema->symtab=NULL;
    }
  }
  return s;
}


/** parse a maestro schema using the cyaml-defined meta-schema */
mstro_status
mstro_schema_parse(const uint8_t *yaml_data, size_t data_len,
                   mstro_schema *result)
{
  if(yaml_data==NULL) {
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    return MSTRO_INVOUT;
  }

  /** allocates the *result data structure on success */
  cyaml_err_t err = cyaml_load_data(yaml_data, data_len,
                                    &mstro_cyaml_config,
                                    &maestro_schema_schema,
                                    (cyaml_data_t**)result, NULL);
  if(err!=CYAML_OK) {
    ERR("Failed to parse YAML data: %s\n", cyaml_strerror(err));
    return MSTRO_FAIL;
  } else {
    (*result)->type_table = NULL;
    (*result)->attribute_table = NULL;
    (*result)->next = NULL;

    int err=pthread_rwlock_init(&((*result)->lock), NULL);
    if(err!=0) {
      ERR("Failed to initialize schema lock: %d (%s)\n",
          err, strerror(err));
      mstro_schema_free(*result);
      *result=NULL;
      return MSTRO_FAIL;
    }

    mstro_status s = mstro_schema_validate_and_instantiate(*result);
    if(s!=MSTRO_OK) {
      ERR("Failed to validate and instantiate schema '%s' (V%zu)\n",
          (*result)->schema_name, (*result)->schema_version);
      mstro_schema_free(*result);
      *result=NULL;
      return MSTRO_FAIL;
    } else {
      INFO("Instantiated schema '%s' (V%zu)\n",
           (*result)->schema_name, (*result)->schema_version);
      return MSTRO_OK;
    }
  }
}

mstro_status
mstro_schema_parse_from_file(const char *fname, mstro_schema *result)
{
  mstro_status s=MSTRO_UNIMPL;

  int fd;
  if((fd = open(fname, O_RDONLY, 0)) == -1) {
    ERR("Failed to open file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
    goto BAILOUT;
  }

  struct stat sb;
  if(-1==fstat(fd, &sb)) {
    ERR("Failed to stat file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
    goto BAILOUT_CLOSE;
  }

  uint8_t *fmap = mmap(NULL, sb.st_size, PROT_READ, MAP_FILE|MAP_PRIVATE, fd, 0);
  if(fmap==MAP_FAILED) {
    ERR("Failed to mmap file %s (%zu bytes): %d (%s)\n",
        fname, sb.st_size, errno, strerror(errno));
    s=MSTRO_FAIL;
    goto BAILOUT_CLOSE;
  }

  s=mstro_schema_parse(fmap, sb.st_size, result);
  if(s!=MSTRO_OK) {
    ERR("Failed to parse schema from %s: %d (%s)\n",
        fname, s, mstro_status_description(s));
  }

  if(-1==munmap(fmap, sb.st_size)) {
    ERR("Failed to unmap file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
  }

BAILOUT_CLOSE:
  if(-1==close(fd)) {
    ERR("Failed to close file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
  }

BAILOUT:
  return s;
}


mstro_status
mstro_schema_merge(mstro_schema main,
                   mstro_schema consumed)
{
  if(main==NULL || consumed==NULL) {
    return MSTRO_INVARG;
  }
  char *tmpname = malloc(strlen(main->schema_name)
                         +strlen(" + ")
                         +strlen(consumed->schema_name)
                         +1);
  if(tmpname==NULL) {
    ERR("Cannot allocate merged schema name\n");
    return MSTRO_NOMEM;
  }
  strcpy(tmpname,main->schema_name);
  strcat(tmpname," + ");
  strcat(tmpname,consumed->schema_name);

  WITH_LOCKED_SCHEMA_WRITE(consumed, {
      WITH_LOCKED_SCHEMA_WRITE(main, {
          free(main->schema_name);

          main->schema_name=tmpname;

          LL_APPEND(main,consumed);
          /* FIXME: we could copy entries from consumed into main, but until
           * someone complains we'll cdr down the next list when performing
           * lookups.
           */
        });
    });

  return MSTRO_OK;
}

/* these are 1:1 corresponding to the user-facing ones from enum mstro_stp_val_kind */
enum mstro_schema_builtin_type {
  MSTRO_SCHEMA_BUILTIN_BOOL,
  MSTRO_SCHEMA_BUILTIN_UINT,
  MSTRO_SCHEMA_BUILTIN_INT,
  MSTRO_SCHEMA_BUILTIN_FLOAT,
  MSTRO_SCHEMA_BUILTIN_DOUBLE,
  MSTRO_SCHEMA_BUILTIN_STRING,
  MSTRO_SCHEMA_BUILTIN_REGEX,
  MSTRO_SCHEMA_BUILTIN_BLOB,
  MSTRO_SCHEMA_BUILTIN_TIMESTAMP,
  MSTRO_SCHEMA_BUILTIN_POINTER,
  MSTRO_SCHEMA_BUILTIN_MMBLAYOUT,
  MSTRO_SCHEMA_BUILTIN_TYPE__MAX
};

static struct {
  enum mstro_schema_builtin_type type;
  const char *basename;
  enum mstro_stp_val_kind stp_kind;
} builtin_types[MSTRO_SCHEMA_BUILTIN_TYPE__MAX] = {
  [MSTRO_SCHEMA_BUILTIN_BOOL]   = { .type = MSTRO_SCHEMA_BUILTIN_BOOL,
                                    .basename = "bool",
                                    .stp_kind = MSTRO_STP_BOOL},
  [MSTRO_SCHEMA_BUILTIN_UINT]   = { .type = MSTRO_SCHEMA_BUILTIN_UINT,
                                    .basename = "uint",
                                    .stp_kind = MSTRO_STP_UINT},
  [MSTRO_SCHEMA_BUILTIN_INT]    = { .type = MSTRO_SCHEMA_BUILTIN_INT,
                                    .basename = "int",
                                    .stp_kind = MSTRO_STP_INT},
  [MSTRO_SCHEMA_BUILTIN_FLOAT]  = { .type = MSTRO_SCHEMA_BUILTIN_FLOAT,
                                    .basename = "float",
                                    .stp_kind = MSTRO_STP_FLOAT},
  [MSTRO_SCHEMA_BUILTIN_DOUBLE] = { .type = MSTRO_SCHEMA_BUILTIN_DOUBLE,
                                    .basename = "double",
                                    .stp_kind = MSTRO_STP_DOUBLE},
  [MSTRO_SCHEMA_BUILTIN_STRING] = { .type = MSTRO_SCHEMA_BUILTIN_STRING,
                                    .basename = "str",
                                    .stp_kind = MSTRO_STP_STR},
  [MSTRO_SCHEMA_BUILTIN_REGEX]  = { .type = MSTRO_SCHEMA_BUILTIN_REGEX,
                                    .basename = "regex",
                                    .stp_kind = MSTRO_STP_REGEX},
  [MSTRO_SCHEMA_BUILTIN_BLOB]   = { .type = MSTRO_SCHEMA_BUILTIN_BLOB,
                                    .basename = "blob",
                                    .stp_kind = MSTRO_STP_BLOB},
  [MSTRO_SCHEMA_BUILTIN_TIMESTAMP] = { .type = MSTRO_SCHEMA_BUILTIN_TIMESTAMP,
                                       .basename = "timestamp",
                                       .stp_kind = MSTRO_STP_TIMESTAMP},
  [MSTRO_SCHEMA_BUILTIN_POINTER] = { .type = MSTRO_SCHEMA_BUILTIN_POINTER,
                                     .basename = "pointer",
                                     .stp_kind = MSTRO_STP_POINTER},
  [MSTRO_SCHEMA_BUILTIN_MMBLAYOUT] = { .type = MSTRO_SCHEMA_BUILTIN_MMBLAYOUT,
                                     .basename = "mmblayout",
                                     .stp_kind = MSTRO_STP_MMBLAYOUT},
};

/** lookup or create builtin type */
static inline
mstro_status
mstro_schema_lookup_type__builtins(
    mstro_schema schema,
    const char *typename,
    mstro_schema_type *result)
{
  size_t i;
  mstro_status s = MSTRO_NOENT;

  for(i=0; i<MSTRO_SCHEMA_BUILTIN_TYPE__MAX; i++) {
    /* hand-coded to avoid 3 strlen ops */
    size_t j;
    const char *target = builtin_types[i].basename;
    for(j=0; typename[j]!='\0' && target[j]!='\0'; j++) {
      if(typename[j]!=target[j])
        goto next_entry;
    }
    if(typename[j]!='\0' && target[j]=='\0') {
      if(! (isspace(typename[j]) || typename[j]=='('))
        goto next_entry;
    }
    break; /* found at 'i' */
 next_entry:
    ;
  }
  if(i<MSTRO_SCHEMA_BUILTIN_TYPE__MAX) {
    /* found something matching basename */
    mstro_symbol sym;
    mstro_status status;
    status = mstro_symtab_lookup(schema->symtab, typename, &sym);
    if(status!=MSTRO_OK) {
      ERR("Failed to lookup builtin type in schema symtab: %d\n");
      goto BAILOUT;
    }
    if(sym==NULL) {
      DEBUG("Symbol for built-in type |%s| not yet in symtab, adding\n", typename);
      WITH_LOCKED_SCHEMA_WRITE_UPGRADE(schema, {
          s=mstro_symtab_intern(&schema->symtab, typename, &sym);
        });
      if(s!=MSTRO_OK) {
        ERR("Failed to intern built-in type-name |%s|\n", typename);
        goto BAILOUT;
      }
    }
    /* now symbol exists */
    HASH_FIND(hh, schema->type_table, &sym, sizeof(mstro_symbol), *result);
    if(*result!=NULL) {
      DEBUG("Found existing entry for built-in type |%s|\n", typename);
      s=MSTRO_OK;
      goto BAILOUT;
    } else {
      DEBUG("Failed to find builtin type |%s| in type table, instantiating\n",
            typename);
      WITH_LOCKED_SCHEMA_WRITE_UPGRADE(schema, {
          /* try to parse */
          struct mstro_stp_val *parsed_type;
          mstro_status s = mstro_schema_type_parse(typename, &parsed_type);
          if(s!=MSTRO_OK) {
            if(parsed_type && parsed_type->kind == MSTRO_STP_ERROR) {
              ERR("error parsing built-in type, error message %s\n",
                  parsed_type->errmsg);
            } else {
              ERR("error parsing built-in type: %d (no parser error message available)\n",
                  s);
            }
          } else {
            *result = malloc(sizeof(struct mstro_schema_type_));
            if(*result) {
              (*result)->typename = strdup(typename);
              (*result)->type_symbol = sym;
              (*result)->unparsed_typespec = strdup(typename);
              (*result)->documentation = strdup("builtin type");
              (*result)->parsed_type = parsed_type;
              HASH_ADD(hh, schema->type_table,
                       type_symbol, sizeof(mstro_symbol), *result);
              s = MSTRO_OK;
            } else {
              s= MSTRO_NOMEM;
            }
          }
        });
    }
  }
BAILOUT:
  return s;
}


mstro_status
mstro_schema_lookup_type(mstro_schema schema,
                         const char *typename,
                         mstro_schema_type *result)
{
  mstro_symbol sym;
  mstro_status status=MSTRO_FAIL;

  WITH_LOCKED_SCHEMA_READ(schema, {
      /* built-in types first. This will create them and intern them
       * on the fly, with restrictions if needed. */
      status = mstro_schema_lookup_type__builtins(schema, typename, result);
      if(status==MSTRO_OK) {
        DEBUG("type |%s| recognized as built-in\n", typename);
        goto BAILOUT;
      } else {
        DEBUG("|%s| is not a builtin type\n", typename);
      }

      status = mstro_symtab_lookup(schema->symtab, typename, &sym);
      if(status==MSTRO_OK) {
        HASH_FIND(hh, schema->type_table, &sym, sizeof(mstro_symbol), *result);
        if(*result==NULL) {
          DEBUG("Failed to find |%s| in type table\n", typename);
          status= MSTRO_NOENT;
        } else {
          status = MSTRO_OK;
        }
      } else {
        ERR("Failed to find symbol for |%s| in %s (V%zu)\n",
            typename, mstro_schema_name(schema),
            mstro_schema_version(schema));
        status = MSTRO_NOENT;
      }
BAILOUT:
      ; /* target of jump */
    });
  return status;
}

/** Find FQKEY in schema (chasing absorbed schemata if needed) */
static inline
mstro_status
mstro_schema_lookup_symbol(mstro_schema schema, const char *fqkey,
                           mstro_symbol *sym)
{
  if(schema==NULL ||fqkey==NULL)
    return MSTRO_INVARG;
  if(sym==NULL)
    return MSTRO_INVOUT;

  mstro_status status = MSTRO_OK;
  for(mstro_schema schem = schema;
      schem!=NULL;
      schem=schem->next) {
    WITH_LOCKED_SCHEMA_READ(schem, {
        //DEBUG("Trying schema %s\n", mstro_schema_name(schem));
        status = mstro_symtab_lookup(schem->symtab, fqkey, sym);
      });
    if(status!=MSTRO_OK) {
      return status;
    }
    if(*sym!=NULL) {
      DEBUG("Found %s in %s\n", fqkey, mstro_schema_name(schem));
      break;
    }
  }
  return status;
}



/* recursing on linked schemata */
mstro_status
mstro_schema_lookup_attribute(mstro_schema schema,
                              const char *attributename,
                              mstro_schema_attribute *result)
{
  mstro_symbol sym;
  mstro_status status=MSTRO_FAIL;
  if(schema==NULL) {
    ERR("NULL schema\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("NULL result destination\n");
    return MSTRO_INVOUT;
  }

  *result = NULL;
  status = mstro_schema_lookup_symbol(schema, attributename, &sym);
  if(status==MSTRO_OK) {
    WITH_LOCKED_SCHEMA_READ(schema, {
        for(mstro_schema s=schema; s!=NULL; s=s->next) {
          HASH_FIND(hh, s->attribute_table,
                    &sym, sizeof(mstro_symbol), *result);
          if(*result!=NULL) {
            /* found */
            break;
          }
        }
      });

    if(*result==NULL) {
      DEBUG("Failed to find |%s| in type table\n", attributename);
      status= MSTRO_NOENT;
    } else {
      status = MSTRO_OK;
    }
  } else {
    DEBUG("Failed to find symbol for |%s| in %s (V%zu)\n",
          attributename, mstro_schema_name(schema),
          mstro_schema_version(schema));
    status = MSTRO_NOENT;
  }
  return status;
}



/* libyaml-based parsing using our own schema */

/** attribute entry in dictionary (hashable by key) */
struct mstro_attribute_entry_ {
  UT_hash_handle hh;   /**< hashable by key */
  mstro_symbol  key;   /**< the key (interned attribute name) */
  void *val;           /**< a value, to be interpreted by looking up the
                        * expected type of the attribute in the
                        * appropriate schema */
  size_t valsize;      /**< allocated space for val */
  enum mstro_stp_val_kind kind; /**< schema type parser type of the value (saves lookups in original type declaration)*/
  bool user_owned_val; /**< whether the val allocation is owned by the
                        * user (if not we must free it eventually) */
  /* FIXME: this is the place where serialized versions of the entry
   * should be cached if needed */
  /* these may be unset -- this can be checked by comparing the string
   * with NULL and the AVal val_case with
   * MSTRO__POOL__AVAL__VAL__NOT_SET */
  char *serialized_yaml; /** string-version of the attribute value */
  Mstro__Pool__AVal serialized_pb;   /** protobuf-converted version of
                                      * the attribute value */
};

/** attribute dictionary */
struct mstro_attribute_dict_ {
  mstro_schema schema;
  struct mstro_attribute_entry_ *dict;
};

/** a stack-like data structure keeping partial key state and
 * instatiating the full key name at the current level */
struct partial_key {
  struct partial_key *next;
  size_t keylen; /** size needed for the full length key up to and
                  * including this entry */
  char *fqkey; /** fully qualified key on this level */
};


/* set valsize slot according to type */
static inline
mstro_status
mstro_attribute_entry__set_size(struct mstro_attribute_entry_ *entry,
                                enum mstro_stp_val_kind kind)
{
  return mstro_attribute_val__compute_size(
      kind, NULL, entry->val, &entry->valsize);
}

static inline
mstro_status
mstro_attribute_entry_create(mstro_symbol sym,
                             struct mstro_attribute_entry_ **result)
{
  if(sym==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;
  *result = malloc(sizeof(struct mstro_attribute_entry_));
  if(*result==NULL) {
    ERR("Failed to allocate attribute entry\n");
    return MSTRO_NOMEM;
  }
  (*result)->key = sym;
  (*result)->val = NULL;
  (*result)->valsize = 0;
  (*result)->kind = MSTRO_STP_ERROR;
  (*result)->user_owned_val = false;
  (*result)->serialized_yaml = NULL;
  mstro__pool__aval__init(& ((*result)->serialized_pb));
  return MSTRO_OK;
}

static inline
mstro_status
mstro_attribute_entry_dispose(struct mstro_attribute_entry_ *entry)
{
  if(entry==NULL)
    return MSTRO_INVARG;
  mstro_status status=MSTRO_OK;

  DEBUG("Disposing entry for %s, user-owned? %s, val %p valsize %d\n",
        mstro_symbol_name(entry->key), entry->user_owned_val ? "yes" : "no",
        entry->val, entry->valsize);
  if(! entry->user_owned_val) {
    if(entry->val!=NULL)
    {
      /**special case mmblayout -- need to call its destructor*/
      if (entry->kind == MSTRO_STP_MMBLAYOUT)
      {
        mmb_layout_destroy(entry->val);
      }
      else if (entry->kind == MSTRO_STP_BLOB)
      {
	      mstro_blob_dispose(entry->val);
      }
      else
      {
        free(entry->val);
      }
      entry->val = NULL;
    }
  }
  if(entry->serialized_yaml)
    free(entry->serialized_yaml);

  switch(entry->serialized_pb.val_case) {
    case MSTRO__POOL__AVAL__VAL_STRING:
      if(entry->serialized_pb.string)
        free(entry->serialized_pb.string);
      break;
    case MSTRO__POOL__AVAL__VAL_BYTES:
      if(entry->serialized_pb.bytes.len)
        free(entry->serialized_pb.bytes.data);
      break;
    case MSTRO__POOL__AVAL__VAL_TIMESTAMP:
      mstro__pool__timestamp__free_unpacked(
          entry->serialized_pb.timestamp, NULL);
      break;
    case MSTRO__POOL__AVAL__VAL__NOT_SET:
    case MSTRO__POOL__AVAL__VAL_BOOL:
    case MSTRO__POOL__AVAL__VAL_INT32:
    case MSTRO__POOL__AVAL__VAL_INT64:
    case MSTRO__POOL__AVAL__VAL_UINT32:
    case MSTRO__POOL__AVAL__VAL_UINT64:
    case MSTRO__POOL__AVAL__VAL_FLOAT:
    case MSTRO__POOL__AVAL__VAL_DOUBLE:
      /* These are inlined, no need to free them */
      break;
    default:
      ERR("Unhandled protobuf AVAL type: %d\n",
          entry->serialized_pb.val_case);
      status=MSTRO_FAIL;
  }
  free(entry);

  return status;
}


static inline
mstro_status
mstro_attributes__parse_helper(yaml_parser_t parser,
                               struct mstro_schema_ *schema,
                               mstro_attribute_dict *result,
                               const char *default_namespace_prefix)
{
  mstro_status status = MSTRO_UNIMPL;
  bool result_allocated_here=false;
  struct partial_key *keystack =NULL;
  if(schema==NULL) {
    ERR("Can't parse without schema\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("NULL attribute dict\n");
    return MSTRO_INVOUT;
  }
  size_t default_namespace_prefix_len = 0;
  if(default_namespace_prefix!=NULL)
    default_namespace_prefix_len = strlen(default_namespace_prefix);

  if(*result==NULL) {
    *result = malloc(sizeof(struct mstro_attribute_dict_));
    if(*result==NULL) {
      ERR("Failed to allocate attribute dict\n");
      return MSTRO_NOMEM;
    }
    (*result)->dict=NULL;
    (*result)->schema=schema;
    result_allocated_here = true;
  }

  yaml_event_t  event;

  do {
    if (!yaml_parser_parse(&parser, &event)) {
      ERR("YAML parser error %d\n", parser.error);
      status = MSTRO_FAIL;
      goto BAILOUT;
    }

    switch(event.type) {
      case YAML_NO_EVENT: DEBUG("YAML: No event!\n"); break;

        /* Stream start/end */
      case YAML_STREAM_START_EVENT: DEBUG("YAML: STREAM START\n"); break;
      case YAML_STREAM_END_EVENT:   DEBUG("YAML: STREAM END\n");   break;

        /* Block delimeters */
      case YAML_DOCUMENT_START_EVENT: DEBUG("YAML: Start Document\n"); break;
      case YAML_DOCUMENT_END_EVENT:   DEBUG("YAML: End Document\n");   break;

      case YAML_SEQUENCE_START_EVENT: DEBUG("YAML: Start Sequence\n"); break;
      case YAML_SEQUENCE_END_EVENT:   DEBUG("YAML: End Sequence\n");   break;

      case YAML_MAPPING_START_EVENT: {
        DEBUG("Start Mapping\n");
        if(keystack!=NULL && keystack->fqkey==NULL) {
          ERR("Nested mapping without label\n");
          status=MSTRO_FAIL;
          goto BAILOUT;
        }
        struct partial_key *pk = malloc(sizeof(struct partial_key));
        if(pk==NULL) {
          ERR("Failed to allocate for key stack\n");
          status = MSTRO_NOMEM;
          goto BAILOUT;
        }
        pk->fqkey = NULL; /* set on next scalar event */
        STACK_PUSH(keystack, pk);
        DEBUG("pushed keystack element\n");
        break;
      }

      case YAML_MAPPING_END_EVENT: {
        DEBUG("End Mapping (%s)\n", keystack->fqkey);
        struct partial_key *pk;
        if(STACK_EMPTY(keystack)) {
          ERR("YAML mapping stack empty!?\n");
          status=MSTRO_FAIL;
          goto BAILOUT;
        }
        STACK_POP(keystack,pk);
        if(pk->fqkey)
          free(pk->fqkey);
        free(pk);
        /* restore invariant: in a mapping we have an empty entry on top of stack */
        if(STACK_EMPTY(keystack)) {
          /* top-level mapping ending */
          DEBUG("key stack now empty\n");
        } else {
          if(keystack->fqkey) {
            free(keystack->fqkey);
            keystack->fqkey=NULL;
          }
        }
        break;
      }

        /* Data */
      case YAML_ALIAS_EVENT:  printf("Got alias (anchor %s)\n", event.data.alias.anchor); break;
      case YAML_SCALAR_EVENT: {
        unsigned char *val = event.data.scalar.value;
        size_t val_len = event.data.scalar.length;
        DEBUG("Got scalar (value %s, len %zu)\n", val, val_len);

        if(keystack==NULL) {
          ERR("YAML scalar at toplevel\n");
          status=MSTRO_FAIL;
          goto BAILOUT;
        }

        if(keystack->fqkey==NULL) {
          DEBUG("No key on top of stack, this must be the mapping key\n");
          size_t next_len;
          if(keystack->next!=NULL) {
            next_len = keystack->next->keylen + 1 /* need a '.' */;
          } else if (default_namespace_prefix!=NULL) {
            next_len = default_namespace_prefix_len;
            /* maybe will need to append a '.' */
            if(default_namespace_prefix[next_len-1]!='.')
              next_len++;
          } else {
            next_len=0;
          }
          keystack->keylen = (val_len
                              + next_len);
          keystack->fqkey = malloc(keystack->keylen+1);
          if(keystack->fqkey==NULL) {
            ERR("Failed to allocate key\n");
            status=MSTRO_NOMEM;
            goto BAILOUT;
          }

          size_t offset;
          if(keystack->next!=NULL) {
            DEBUG("keystack provides prefix %s\n", keystack->next->fqkey);
            strcpy(keystack->fqkey, keystack->next->fqkey);
            keystack->fqkey[next_len-1] = '.';
            keystack->fqkey[next_len] = '\0';
            offset = next_len;
          } else {
            DEBUG("no next elt on keystack\n");
            if(val[0]=='.') {
              /* fully qualified key */
              DEBUG("Key is fully qualified\n");
              offset=0;
            } else if (default_namespace_prefix!=NULL) {
              /* prepend default namespace */
              strcpy(keystack->fqkey, default_namespace_prefix);
              keystack->fqkey[next_len-1] = '.';
              keystack->fqkey[next_len] = '\0';
              offset = next_len;
              DEBUG("qualifying |%s| using default namespace |%s|\n",
                    val, default_namespace_prefix);
            } else {
              ERR("Toplevel key needs to start with '.', or a default namespace needs to be set\n");
              status=MSTRO_FAIL;
              goto BAILOUT;
            }
          }

          strcpy(keystack->fqkey+offset, (const char*)val);
          DEBUG("Stored fqkey %s\n", keystack->fqkey);
        } else {
          DEBUG("Already have key |%s|, this must be the value for it\n",
                keystack->fqkey);
          /* lookup key in schema */
          struct mstro_schema_attribute_ *decl;
          status = mstro_schema_lookup_attribute(schema, keystack->fqkey,
                                                 &decl);
          if(status!=MSTRO_OK) {
            if(status==MSTRO_NOENT) {
              WARN("Ignoring invalid attribute |%s| (not in schema %s V%zu)\n",
                  keystack->fqkey, mstro_schema_name(schema), mstro_schema_version(schema));
              goto PREP_NEXT_EVENT;
            } else {
              ERR("attribute lookup in schema failed\n");
              goto BAILOUT;
            }
          }
          /* it's a valid attribute */
          mstro_attribute_entry entry=NULL;
          bool new_entry=false;

          DEBUG("looking up sym %p in dict\n", decl->key_symbol);
          HASH_FIND(hh, (*result)->dict,
                    &(decl->key_symbol), sizeof(mstro_symbol), entry);
          if(entry) {
            DEBUG("Replacing value of previously set attribute |%s|\n",
                  keystack->fqkey);
            if(! entry->user_owned_val) {
              free(entry->val);
              entry->val=NULL;
            }
          } else {
            new_entry=true;
            status = mstro_attribute_entry_create(decl->key_symbol, &entry);
            if(status!=MSTRO_OK)
              goto BAILOUT;
          }

          status = mstro_attribute_val_parse(decl->type_parse_closure.info,
                                             (char*)val,
                                             &entry->val, &entry->valsize);
          if(status!=MSTRO_OK) {
            ERR("Failed to parse |%s| as value for attribute |%s|\n",
                val, mstro_symbol_name(decl->key_symbol));
            if(new_entry)
              mstro_attribute_entry_dispose(entry);
            goto BAILOUT;
          } else {
            DEBUG("Parsed |%s| as valid value for attribute |%s|\n",
                  val, mstro_symbol_name(decl->key_symbol));
            /* FIXME: describe_entry function call here */
          }
          entry->kind = decl->type_parse_closure.info->kind;

          assert(entry->kind!=MSTRO_STP_ERROR);

          if(new_entry)
            HASH_ADD(hh, (*result)->dict, key, sizeof(mstro_symbol), entry);

          DEBUG("Handled entry for %s, cleaning keystack\n", keystack->fqkey);
PREP_NEXT_EVENT:
          free(keystack->fqkey);
          keystack->fqkey = NULL;
        }

        break;
      }

      default:
        ERR("Unhandled YAML event: %d\n", event.type);
        status=MSTRO_FAIL;
        goto BAILOUT;
    }
    if(event.type != YAML_STREAM_END_EVENT)
      yaml_event_delete(&event);
  } while(event.type != YAML_STREAM_END_EVENT);
  yaml_event_delete(&event);

  (*result)->schema = schema;
  status = MSTRO_OK;


BAILOUT:
  if(status!=MSTRO_OK && result_allocated_here) {
    free(*result);
  }

  return status;
}

/* user-facing entrypoint */
mstro_status
mstro_attributes_parse(mstro_schema schema,
                       const char *yaml_fragment,
                       mstro_attribute_dict *result,
                       const char *default_namespace_prefix)
{
  mstro_status s;
  if(schema==NULL) {
    s=MSTRO_INVARG;
    goto BAILOUT;
  }
  if(result==NULL) {
    s=MSTRO_INVOUT;
    goto BAILOUT;
  }
  if(yaml_fragment==NULL) {
    s=MSTRO_INVARG;
    goto BAILOUT;
  }

  /* Initialize parser */
  yaml_parser_t parser;
  if(!yaml_parser_initialize(&parser)) {
    ERR("Failed to initialize YAML parser\n");
    s= MSTRO_FAIL;
    goto BAILOUT;
  }

  /* Set input file */
  yaml_parser_set_input_string(&parser,
                               (const unsigned char*)yaml_fragment,
                               strlen(yaml_fragment));

  s=mstro_attributes__parse_helper(parser, schema,
                                   result, default_namespace_prefix);
  yaml_parser_delete(&parser);

  if(s!=MSTRO_OK) {
    ERR("Failed to parse attribute yaml string |%s|\n", yaml_fragment);
  } else {
    DEBUG("Parsed yaml attribute string, now %zu entries in dictionary\n",
         HASH_COUNT((*result)->dict));
    (*result)->schema = schema;
  }

BAILOUT:
  return s;
}

mstro_status
mstro_attributes_parse_from_file(mstro_schema schema,
                                 const char *fname,
                                 mstro_attribute_dict *result,
                                 const char *default_namespace_prefix)
{
  mstro_status s=MSTRO_UNIMPL;

  int fd;
  if((fd = open(fname, O_RDONLY, 0)) == -1) {
    ERR("Failed to open file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
    goto BAILOUT;
  }

  struct stat sb;
  if(-1==fstat(fd, &sb)) {
    ERR("Failed to stat file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
    goto BAILOUT_CLOSE;
  }

  uint8_t *fmap = mmap(NULL, sb.st_size, PROT_READ, MAP_FILE|MAP_PRIVATE, fd, 0);
  if(fmap==MAP_FAILED) {
    ERR("Failed to mmap file %s (%zu bytes): %d (%s)\n",
        fname, sb.st_size, errno, strerror(errno));
    s=MSTRO_FAIL;
    goto BAILOUT_CLOSE;
  }

  s=mstro_attributes_parse(schema, (const char*)fmap, result, default_namespace_prefix);
  if(s!=MSTRO_OK) {
    ERR("Failed to parse attributes from %s: %d (%s)\n",
        fname, s, mstro_status_description(s));
  }

  if(-1==munmap(fmap, sb.st_size)) {
    ERR("Failed to unmap file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
  }

BAILOUT_CLOSE:
  if(-1==close(fd)) {
    ERR("Failed to close file %s: %d (%s)\n",
        fname, errno, strerror(errno));
    s=MSTRO_FAIL;
  }

BAILOUT:
  return s;
}


mstro_status
mstro_attribute_dict_get_schema(mstro_attribute_dict dict,
                                mstro_schema *schema_p)
{
  if(dict==NULL)
    return MSTRO_INVARG;
  if(schema_p==NULL)
    return MSTRO_INVOUT;
  *schema_p=dict->schema;
  return MSTRO_OK;
}

mstro_status
mstro_attribute_dict_set_defaults(mstro_schema schema,
                                  bool override,
                                  mstro_attribute_dict *result)
{
  override=override; /* avoid unused arg warning */
  if(schema==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;
  if(*result==NULL) {
    *result = malloc(sizeof(struct mstro_attribute_dict_));
    if(*result==NULL)
      return MSTRO_NOMEM;
    (*result)->schema = schema;
    (*result)->dict = NULL;
  }

  DEBUG("Default values will be filled in on demand\n");
  return MSTRO_OK;
}



mstro_status
mstro_attribute_dict_dispose(mstro_attribute_dict dict)
{
  if(dict==NULL)
    return MSTRO_INVARG;
  /* schema will be refcounted one day ...*/
  mstro_status status = MSTRO_OK;

  struct mstro_attribute_entry_ *el,*tmp;
  HASH_ITER(hh,dict->dict,el,tmp) {
    DEBUG("Deleting attribute %s from dict %p\n",
          mstro_symbol_name(el->key), dict->dict);
    HASH_DELETE(hh,dict->dict,el);
    status = status | mstro_attribute_entry_dispose(el);
  }

  free(dict);
  return status;
}

mstro_status
mstro_attribute_dict_clear(mstro_attribute_dict dict)
{
  if(dict==NULL)
    return MSTRO_INVARG;
  /* schema will be refcounted one day ...*/
  mstro_status status = MSTRO_OK;

  struct mstro_attribute_entry_ *el,*tmp;
  HASH_ITER(hh,dict->dict,el,tmp) {
    DEBUG("Deleting attribute %s from dict %p\n",
          mstro_symbol_name(el->key), dict->dict);
    HASH_DELETE(hh,dict->dict,el);
    status = status | mstro_attribute_entry_dispose(el);
  }

  return status;
}

/* find default for KEY in DICT's schema(ta) and create a fresh entry
 * for it. If entry already exist, replaces the current value by the
 * default. */
static inline
mstro_status
mstro_attribute_dict__insert_default(mstro_attribute_dict dict,
                                     mstro_symbol key)
{
  struct mstro_attribute_entry_ *entry=NULL;
  bool must_insert=false;
  mstro_status status = MSTRO_OK;

  DEBUG("schema dict insert default for key %s\n", mstro_symbol_name(key));
  mstro_schema_attribute attr=NULL;

  status = mstro_schema_lookup_attribute(dict->schema,
                                         mstro_symbol_name(key),
                                         &attr);
  if(status!=MSTRO_OK) {
    ERR("Cannot find attribute in schema !? (should not happen here)\n");
    goto BAILOUT;
  }

  if(attr->defaultval==MSTRO_SCHEMA_DEFAULT_VAL_UNSET) {
    /* schema instantiation should have parsed the defaultval_string for us */
    DEBUG("No default for |%s| in schema\n", mstro_symbol_name(key));
    return MSTRO_NOENT;
  }

  assert(attr->defaultval!=MSTRO_SCHEMA_DEFAULT_VAL_UNSET); /* NULL is possible */

  mstro_schema_type tdecl;
  status = mstro_schema_lookup_type(dict->schema, attr->typespec, &tdecl);
  if(status!=MSTRO_OK) {
    ERR("Failed to look up type declaration for type |%s| (attribute |%s|)\n",
        attr->typespec, mstro_symbol_name(key));
    goto BAILOUT;
  }

  HASH_FIND(hh, dict->dict, &key, sizeof(mstro_symbol), entry);
  if(!entry) {
    DEBUG("No entry for |%s|, allocating new one\n", mstro_symbol_name(key));
    status = mstro_attribute_entry_create(key, &entry);
    if(status!=MSTRO_OK) {
      ERR("Cannot allocate default entry\n");
      goto BAILOUT;
    }

    entry->kind=tdecl->parsed_type->kind;
    entry->val = NULL;
    must_insert=true;
  }

  assert(entry->kind == tdecl->parsed_type->kind);

  /* duplicate; if user-supplied: allocate freshly */
  if(entry->user_owned_val)
    entry->val=NULL;

  /* must use the long form since we need to set entry val size from
   * attr defaultval */
  assert(attr->defaultval!=MSTRO_SCHEMA_DEFAULT_VAL_UNSET);
  if(attr->defaultval==NULL) {
    entry->valsize = 0;
  } else {
    status = mstro_attribute_val__compute_size(entry->kind, NULL,
                                               attr->defaultval, &entry->valsize);
    if(status!=MSTRO_OK) {
      ERR("Cannot set entry value size\n");
      goto BAILOUT;
    }
  }

  if(entry->val==NULL && entry->valsize>0) {
    /* either looking at a fresh entry record, or we killed user value
     * in this slot */
    entry->val =  malloc(entry->valsize);
    if(entry->val==NULL) {
      status=MSTRO_NOMEM;
      goto BAILOUT;
    }
    entry->user_owned_val=false;
  }

  switch(entry->kind) {
    case MSTRO_STP_BOOL:
    case MSTRO_STP_UINT:
    case MSTRO_STP_INT:
    case MSTRO_STP_FLOAT:
    case MSTRO_STP_DOUBLE:
    case MSTRO_STP_STR:
    case MSTRO_STP_REGEX:
    case MSTRO_STP_BLOB:
    case MSTRO_STP_TIMESTAMP:
    case MSTRO_STP_POINTER:
    case MSTRO_STP_MMBLAYOUT:
      memcpy(entry->val, attr->defaultval, entry->valsize);
      break;
    default:
      assert(entry->valsize>0);
      free(entry->val);
      entry->val = NULL;
      ERR("Unhandled STP kind %d\n", entry->kind);
      status=MSTRO_UNIMPL;
      goto BAILOUT;
  }

  if(must_insert) {
    HASH_ADD(hh, dict->dict, key, sizeof(mstro_symbol), entry);
  }
BAILOUT:
  if(status!=MSTRO_OK)
    if(must_insert)
      mstro_attribute_entry_dispose(entry);

  DEBUG("Insert default returns %d (%s)\n",
        status, mstro_status_description(status));

  return status;
}

static inline
mstro_status
mstro_attribute__stp_to_val_type(enum mstro_stp_val_kind kind,
                                 enum mstro_cdo_attr_value_type *valtype)
{
  assert(valtype!=NULL);
  mstro_status status = MSTRO_OK;
  
  switch(kind) {
    case MSTRO_STP_BOOL:
      *valtype = MSTRO_CDO_ATTR_VALUE_bool;
      break;
    case MSTRO_STP_UINT:
      *valtype = MSTRO_CDO_ATTR_VALUE_uint64;
      break;
    case MSTRO_STP_INT:
      *valtype = MSTRO_CDO_ATTR_VALUE_int64;
      break;
    case MSTRO_STP_FLOAT:
      *valtype = MSTRO_CDO_ATTR_VALUE_float;
      break;
    case MSTRO_STP_DOUBLE:
      *valtype = MSTRO_CDO_ATTR_VALUE_double;
      break;
    case MSTRO_STP_STR:
    case MSTRO_STP_REGEX:
      *valtype = MSTRO_CDO_ATTR_VALUE_cstring;
      break;
    case MSTRO_STP_BLOB:
      *valtype = MSTRO_CDO_ATTR_VALUE_blob;
      break;
    case MSTRO_STP_TIMESTAMP:
      *valtype = MSTRO_CDO_ATTR_VALUE_timestamp;
      break;
    case MSTRO_STP_POINTER:
      *valtype = MSTRO_CDO_ATTR_VALUE_pointer;
      break;
    case MSTRO_STP_MMBLAYOUT:
      *valtype = MSTRO_CDO_ATTR_VALUE_mmblayout;
      break;
    default:
      ERR("Unhandled MSTRO_STP attribute type %d\n", kind);
      status=MSTRO_UNIMPL;
      break;
  }
  return status;
}

mstro_status
mstro_attribute_dict_get(mstro_attribute_dict dict,
                         const char *key,
                         enum mstro_cdo_attr_value_type *valtype,
                         const void **val_p,
                         mstro_attribute_entry * const entry_p,
                         bool insert_default)
{
  mstro_symbol sym;
  mstro_status status;
  if(dict==NULL||key==NULL) {
    ERR("Invalid input arguments\n");
    return MSTRO_INVARG;
  }

  if(valtype==NULL) {
    /* ok, do not return type info */
    ;
  }
  if(val_p==NULL) {
    /* ok, do not return value */
    ;
  }
  if(entry_p==NULL) {
    /* ok, do not return raw entry */
    ;
  }

  /* It's not clear if this function should should do the full
   * namespace qualification, as the default namespace is a property
   * of the CDO; but then the dictionary is likely not shared among
   * multiple CDOs (while the schema is). OTOH, allocation of a
   * temporary qualifying prefix string buffer would better be handled
   * further up for efficiency ... */

  const char *fqkey=NULL; /* a const ref to the fully qualified version */
  char *tmpfqkey=NULL;    /* a locally allocated fq key if needed */
  if(key[0]!='.') {
    /* not fully qualified */
    WARN("key |%s| not fully qualified; dictionary layer does not qualify it for\n", key);
    /* FIXME: allocate tmpfqkey, generate fully qualified one, ... */
  }
  fqkey = key;

  status = mstro_schema_lookup_symbol(dict->schema, fqkey, &sym);
  if(sym==NULL) {
    DEBUG("Key |%s| not found in schema %s (V%zu)\n",
          fqkey,
          mstro_schema_name(dict->schema),
          mstro_schema_version(dict->schema));
    if(valtype)
      *valtype=MSTRO_CDO_ATTR_VALUE_NA;
    status = MSTRO_NOENT;
    goto BAILOUT;
  }

  struct mstro_attribute_entry_ *entry = NULL;

  HASH_FIND(hh, dict->dict, &sym, sizeof(mstro_symbol), entry);
  if(entry==NULL) {
    DEBUG("Key |%s| found, but no value in attribute dictionary at %p, %sfetching default\n",
          fqkey, dict, insert_default ? "" : "not ");
    if(!insert_default) {
      if(valtype) {
        *valtype=MSTRO_CDO_ATTR_VALUE_NONE;
      }
      status=MSTRO_NOENT;
      goto BAILOUT;
    }
    /* otherwise insert default */

    status = mstro_attribute_dict__insert_default(dict, sym);
    if(status==MSTRO_NOENT) {
      DEBUG("Key |%s| has no default value in schema\n", fqkey);
      goto BAILOUT;
    }
    if(status!=MSTRO_OK) {
      ERR("Failed to insert default value for attribute %s\n", fqkey);
      goto BAILOUT;
    }
    HASH_FIND(hh, dict->dict, &sym, sizeof(mstro_symbol), entry);
    assert(entry!=NULL);
  }

  /* return value */
  if(entry_p)
    *entry_p = entry;
  if(val_p) {
    *val_p = entry->val;
    DEBUG("returning val %p\n", entry->val);
  }
  status=MSTRO_OK;
  if(valtype) {
    status = mstro_attribute__stp_to_val_type(entry->kind, valtype);
  }

BAILOUT:
  if(tmpfqkey)
    free(tmpfqkey);

  return status;
}


/* check if KEY is known in SCHEMA, and if so, return its value type though *VAL_TYPE */
mstro_status
mstro_attribute_lookup_type(mstro_schema schema,
                            const char *key,
                            bool *knownp,
                            enum mstro_cdo_attr_value_type *val_type)
{
  /* find the type for it */
  struct mstro_schema_attribute_ *decl;
  
  mstro_status status = mstro_schema_lookup_attribute(schema, key, &decl);
  if(status!=MSTRO_OK) {
    if(status==MSTRO_NOENT) {
      DEBUG("Invalid attribute |%s| (not in schema %s V%zu)\n",
            key, mstro_schema_name(schema),
            mstro_schema_version(schema));
      *knownp = false;
      return MSTRO_OK;
    } else {
      ERR("attribute lookup in schema failed\n");
      return status;
    }
  }
  *knownp = true;
  
  mstro_schema_type tdecl;
  status = mstro_schema_lookup_type(schema, decl->typespec,
                                    &tdecl);
  if(status!=MSTRO_OK) {
    ERR("Failed to find type declaration for type |%s| (attribute |%s|)\n",
        decl->typespec, key);
    return MSTRO_INVARG;
  } else {
    DEBUG("Found type declaration for type |%s| (attribute |%s|)\n",
          decl->typespec, key);
  }
  /* FIXME: this could be handled better by pre-allocated parsers
   * (needs to be reentrant!) built with a schema-typeval.peg */
  if(!tdecl->parsed_type) {
    ERR("Parsed type not available\n");
    return MSTRO_UNIMPL;
  }

  status = mstro_attribute__stp_to_val_type(tdecl->parsed_type->kind,
                                            val_type);
  if(status!=MSTRO_OK) {
    ERR("Unsupported parsed schema type: %d for type decl %s; is this a legitimate user-facing type?\n", tdecl->parsed_type->kind, decl->typespec);
  }
  return status;
}

mstro_status
mstro_attribute_dict_set(mstro_attribute_dict dict, const char *key,
                         enum mstro_cdo_attr_value_type valtype,
                         void *val, bool copy_value, bool user_owned_value)
{
  mstro_symbol sym;
  mstro_status status;
  if(dict==NULL||key==NULL) {
    ERR("Invalid input arguments\n");
    return MSTRO_INVARG;
  }

  const char *fqkey=NULL; /* a const ref to the fully qualified version */
  char *tmpfqkey=NULL;    /* a locally allocated fq key if needed */
  if(key[0]!='.') {
    /* not fully qualified */
    WARN("key |%s| not fully qualified; dictionary layer does not qualify it for\n", key);
    /* FIXME: allocate tmpfqkey, generate fully qualified one, ... */
  }
  fqkey = key;

  DEBUG("Setting value for |%s|\n", fqkey);

  status = mstro_schema_lookup_symbol(dict->schema, fqkey, &sym);
  if(sym==NULL) {
    DEBUG("Key |%s| not found in schema %s (V%zu)\n",
          fqkey,
          mstro_schema_name(dict->schema),
          mstro_schema_version(dict->schema));
    status = MSTRO_NOENT;
    goto BAILOUT;
  }

  if(copy_value == true && user_owned_value==true) {
    ERR("Attempt to set key |%s| in dictionary with both copy-value and user-owned flag 'true'. This is unsupported\n");
    status = MSTRO_INVARG;
    goto BAILOUT;
  }

  struct mstro_attribute_entry_ *entry = NULL;

  HASH_FIND(hh, dict->dict, &sym, sizeof(mstro_symbol), entry);
  if(entry==NULL) {
    DEBUG("Key |%s| found, but no value in attribute dictionary at %p, adding it\n", fqkey, dict);

    /* lookup key in schema */

    struct mstro_schema_attribute_ *decl;
    status = mstro_schema_lookup_attribute(dict->schema, fqkey, &decl);
    if(status!=MSTRO_OK) {
      if(status==MSTRO_NOENT) {
        ERR("Invalid attribute |%s| (not in schema %s V%zu)\n",
            fqkey, mstro_schema_name(dict->schema),
            mstro_schema_version(dict->schema));
        goto BAILOUT;
      } else {
        ERR("attribute lookup in schema failed\n");
        goto BAILOUT;
      }
    }
    mstro_schema_type tdecl;
    mstro_status s = mstro_schema_lookup_type(dict->schema, decl->typespec,
                                              &tdecl);

    if(s!=MSTRO_OK) {
      ERR("Failed to find type declaration for type |%s| (attribute |%s|)\n",
          decl->typespec, fqkey);
      return MSTRO_INVARG;
    } else {
      DEBUG("Found type declaration for type |%s| (attribute |%s|)\n",
            decl->typespec, fqkey);
    }
    /* FIXME: this could be handled better by pre-allocated parsers
     * (needs to be reentrant!) built with a schema-typeval.peg */
    if(!tdecl->parsed_type) {
      ERR("Parsed type not available\n");
      return MSTRO_UNIMPL;
    }

    /* create fresh entry */
    status = mstro_attribute_entry_create(decl->key_symbol, &entry);
    if(status!=MSTRO_OK) {
      ERR("Failed to create new entry for |%s|\n", fqkey);
      goto BAILOUT;
    }

    entry->kind = tdecl->parsed_type->kind;

    HASH_ADD(hh, dict->dict, key, sizeof(mstro_symbol), entry);
    DEBUG("Added fresh entry for |%s|\n", fqkey);
  } else {
    DEBUG("Found existing entry for |%s|, updating\n", fqkey);
  }

  /* some lax type checking */
  if(valtype!=MSTRO_CDO_ATTR_VALUE_INVALID) {
    /* try some sanity checking */
    status = MSTRO_OK;
    switch(entry->kind) {
      case MSTRO_STP_BOOL:
        if(valtype!=MSTRO_CDO_ATTR_VALUE_bool)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_UINT:
        if(valtype!=MSTRO_CDO_ATTR_VALUE_uint64)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_INT:
        if(valtype != MSTRO_CDO_ATTR_VALUE_int64)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_FLOAT:
        if(valtype != MSTRO_CDO_ATTR_VALUE_float)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_DOUBLE:
        if(valtype != MSTRO_CDO_ATTR_VALUE_double)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_STR:
      case MSTRO_STP_REGEX:
        if(valtype != MSTRO_CDO_ATTR_VALUE_cstring)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_BLOB:
        if(valtype != MSTRO_CDO_ATTR_VALUE_blob)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_TIMESTAMP:
        if(valtype != MSTRO_CDO_ATTR_VALUE_timestamp)
          status = MSTRO_INVARG;
        break;
      case MSTRO_STP_POINTER:
	if(valtype != MSTRO_CDO_ATTR_VALUE_pointer)
		status = MSTRO_INVARG;
        break;
      case MSTRO_STP_MMBLAYOUT:
        if(valtype != MSTRO_CDO_ATTR_VALUE_mmblayout)
            status = MSTRO_INVARG;
        break;
      default:
        ERR("Unhandled MSTRO_STP attribute type %d\n", entry->kind);
        status=MSTRO_UNIMPL;
    }
    if(status!=MSTRO_OK) {
      ERR("Type mismatch between provided value and type\n");
      goto BAILOUT;
    }
  }

  /* modify value */
  if(val==NULL) {
    WARN("value is NULL, setting value of |%s| back to default not implemented\n", fqkey);
    /* something like */
    /*     status = mstro_attribute_dict__insert_default(dict, sym); */
    /* if(status!=MSTRO_OK) { */
    /* if status==MSTRO_NOENT : no default available */
    /*    otherwise */
    /*   ERR("Failed to create new entry with default value for %s: %d (%s)\n", */
    /*       fqkey, status, mstro_status_description(status)); */
    /*   goto BAILOUT; */
    /* } */
  }
  DEBUG("Not checking type restrictions\n");

  /* ensure we get rid of previous value cell */
  if(entry->user_owned_val)
    entry->val=NULL;
  else {
    if(entry->val!=NULL) {
      /** special case mmblayout -- need to call its destructor */
      switch(entry->kind) {
        case MSTRO_STP_MMBLAYOUT:
          mmb_layout_destroy(entry->val);
          break;
        case MSTRO_STP_BLOB:
          mstro_blob_dispose(entry->val);
          break;
        default:
          free(entry->val);
          break;
      }
      entry->val = NULL;
    }
  }

  if(copy_value) {
    status = mstro_attribute_val__compute_size(entry->kind, NULL,
                                               val, &entry->valsize);
    if(status!=MSTRO_OK) {
      ERR("Cannot compute value size\n");
      goto BAILOUT;
    }

    /* create a copy of mmbLayout object */
    switch(entry->kind) {
      case MSTRO_STP_MMBLAYOUT: {
        mmbError stat = mmb_layout_create_copy(val ,(mmbLayout **) &entry->val);
        if (stat != MMB_OK) {
          ERR("Cannot copy mmbLayout \n");
          goto BAILOUT;
        }
        break;
      }
      case  MSTRO_STP_BLOB: {
        void *tmp = malloc(entry->valsize*sizeof(uint8_t));
        if(tmp==NULL) {
          ERR("Failed to allocate BLOB copy\n");
          status = MSTRO_NOMEM;
          goto BAILOUT;
        }
        memcpy(tmp, ((mstro_blob *) val)->data, entry->valsize);
        status = mstro_blob_create(((mstro_blob*)val)->len, tmp,(mstro_blob **) &entry->val);
        if(status!=MSTRO_OK) {
          abort();
        }
        break;
      }
      default:
        entry->val = malloc(entry->valsize);
        memcpy(entry->val, val, entry->valsize);
        break;
    }
    entry->user_owned_val = false;
  } else {
    entry->val = val;
    entry->user_owned_val = true;
  }

  /* this is subtle: It caters to the case where the user asked to
   * 'don't copy value' but also 'I don't want it, maestro should free
   * it eventually'. This is useful for ownership passing of the
   * content of raw-pointer values (entry->val = val here, and
   * 'free(entry->val)' later) */
  assert(! (copy_value==true && user_owned_value==true)); /* (checked at start) */
  entry->user_owned_val = user_owned_value; 

  status = mstro_attribute_entry__set_size(entry, entry->kind);
  if(status!=MSTRO_OK) {
    ERR("Failed to set entry size for |%s|\n", fqkey);
    goto BAILOUT;
  }

  DEBUG("Set value on entry |%s| (val: %p, user-owned? %s, copied? %s)\n",
        fqkey, entry->val, entry->user_owned_val? "yes" : "no",
        copy_value ? "yes" : "no");

  switch(entry->kind) {
    case MSTRO_STP_STR:
    case MSTRO_STP_REGEX:
      /* adjust value size */
      entry->valsize = strlen(entry->val);
      break;
    case MSTRO_STP_BLOB:
      entry->valsize = ((mstro_blob*)entry->val)->len;
      break;
    default:
      break;
  }

  status=MSTRO_OK;

BAILOUT:
  if(tmpfqkey)
    free(tmpfqkey);

  return status;
}

mstro_status
mstro_attribute_dict_set_kventry(mstro_attribute_dict dict,
                                 const Mstro__Pool__KvEntry *entry)
{
  if(dict==NULL || entry==NULL)
    return MSTRO_INVARG;

  const char *key = entry->key;
  const Mstro__Pool__AVal * aval = entry->val;
  if(aval==NULL)
    return MSTRO_INVARG;

  /* we copy to tmp location and then set-with-copy to get around the
   * 'const' an to ensure values are owned by the dict */
  switch(aval->val_case) {
    case MSTRO__POOL__AVAL__VAL_BOOL: {
      bool tmp = aval->bool_;
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_bool, &tmp, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_INT64: {
      int64_t tmp = aval->int64;
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_int64, &tmp, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_UINT64: {
      uint64_t tmp = aval->uint64;
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_uint64, &tmp, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_FLOAT: {
      float tmp = aval->float_;
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_float, &tmp, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_DOUBLE: {
      double tmp = aval->double_;
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_double, &tmp, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_STRING: {
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_cstring, aval->string, true, false);
    }
    case  MSTRO__POOL__AVAL__VAL_BYTES: {
      /* We allocate here and set copy to true, 
       * so that maestro is the owner and can free the object cleanly*/
      mstro_blob *b;
      void *tmp = malloc(entry->val->bytes.len);
      if(tmp==NULL) {
        ERR("Failed to allocate BLOB copy\n");
        return MSTRO_NOMEM;
      }
      memcpy(tmp, entry->val->bytes.data, entry->val->bytes.len);
      mstro_status s = mstro_blob_create(entry->val->bytes.len, tmp, &b);
      if(s!=MSTRO_OK) {
        ERR("Failed to construct blob object\n");
        return s;
      }
      s =  mstro_attribute_dict_set(dict, key,
                                    MSTRO_CDO_ATTR_VALUE_blob, b, true, false);
      mstro_blob_dispose(b);
      return s;
    }
    case MSTRO__POOL__AVAL__VAL_TIMESTAMP: {
      mstro_timestamp ts = { .sec = aval->timestamp->sec,
                             .nsec = aval->timestamp->nsec,
                             .offset = aval->timestamp->offset };
      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_timestamp, &ts, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_MMB_LAYOUT: {
      /**FIXME support other mmbLayout types */
      if(aval->mmblayout->type != MSTRO__POOL__MMBLAYOUT__MMB_LAYOUT_TYPE__MMB_IRREGULAR){
        ERR("Unsupported mmbLayout type \n");
        return MSTRO_UNIMPL;
      }
      mmbLayout *dist_layout;
      mstro_attribute_pool_aval_to_mmbLayout(aval->mmblayout, &dist_layout);

      return mstro_attribute_dict_set(dict, key,
                                      MSTRO_CDO_ATTR_VALUE_mmblayout, dist_layout, true, false);
    }
    case MSTRO__POOL__AVAL__VAL_INT32:
      /* fall-thru */
    case MSTRO__POOL__AVAL__VAL_UINT32:
      /* fall-thru */
    case MSTRO__POOL__AVAL__VAL__NOT_SET:
      /* fall-thru */
    default:
      ERR("Invalid aval kind %d\n", aval->val_case);
      return MSTRO_INVMSG;
  }

  /* not reached */
  return MSTRO_UNIMPL;
}

mstro_status
mstro_attribute_pool_find_dist_layout(Mstro__Pool__Attributes *attributes, mmbLayout **dist_layout)
{
  mstro_status status = MSTRO_OK;
  if (attributes == NULL)
    return MSTRO_OK;

  size_t n = attributes->kv_map->n_map;

  DEBUG("looking for distributed layout among %d attributes \n", n);
  const char *key;
  for(size_t i=0; i<n; i++) {
     key = attributes->kv_map->map[i]->key;
     if(strcmp(key,".maestro.core.cdo.dist-layout")==0) {
       DEBUG("DIST Layout found ... \n");
       status = mstro_attribute_pool_aval_to_mmbLayout(attributes->kv_map->map[i]->val->mmblayout, dist_layout);
      break;
     }
   }

  return status;
}


mstro_status
mstro_attribute_pool_aval_to_mmbLayout(Mstro__Pool__Mmblayout *aval_layout, mmbLayout **dist_layout){
  mstro_status s = MSTRO_UNIMPL;

  /**FIXME support other mmbLayout types */
  DEBUG("layout type %d \n ", aval_layout->type);
  if( (aval_layout->type != MSTRO__POOL__MMBLAYOUT__MMB_LAYOUT_TYPE__MMB_IRREGULAR) || (aval_layout->n_dims != 1)){
       ERR("Unsupported mmbLayout type \n");
     return MSTRO_UNIMPL;
   }
   size_t n_blocks = (size_t) aval_layout->irregular->n_blocks;
   DEBUG("layout index %"PRIu64" \n ", aval_layout->index);
   DEBUG("nblocks %zu \n", n_blocks);
   for (size_t k = 0; k < n_blocks; k++) {
     DEBUG("offsets[%zu] = %"PRIu64", lengths[%zu] = %"PRIu64" \n", k,
                                                    aval_layout->irregular->offsets[k],
                                                    k,
                                                    aval_layout->irregular->lengths[k]);
   }


   /** copy and cast offsets and lengths to size_t * type */
   size_t *offsets = malloc(sizeof(size_t)*n_blocks);
   if(offsets == NULL){
     ERR("Can not allocate memory for mmbLayout attribute \n");
     return MSTRO_NOMEM;
   }
   size_t *lengths = malloc(sizeof(size_t)*n_blocks);
   if(lengths == NULL){
     ERR("Can not allocate memory for mmbLayout attribute \n");
     free(offsets);
     return MSTRO_NOMEM;
   }
   mstro_status s1 =
     mstro_cast_unit64_to_size_t_array(aval_layout->irregular->offsets,
                              offsets,
                              n_blocks);
   assert(s1 == MSTRO_OK);
   mstro_status s2 =
     mstro_cast_unit64_to_size_t_array(aval_layout->irregular->lengths,
                              lengths,
                              n_blocks);
   assert(s2 == MSTRO_OK);

   mmbError mmb_s;
   mmb_s = mmb_layout_create_dist_irregular_1d(aval_layout->element_size_bytes,
                                aval_layout->index,
                                n_blocks,
                                offsets,
                                lengths,
                                dist_layout);
  assert(mmb_s == MMB_OK);
  free(offsets);
  free(lengths);
  s = (mmb_s == MMB_OK) ? MSTRO_OK:MSTRO_FAIL;
  return s;
}

static inline
mstro_status
mstro_attribute_entry_to_mapentry(const struct mstro_attribute_entry_ *entry,
                                  Mstro__Pool__KvEntry **result_p)
{
  mstro_status s = MSTRO_UNIMPL;

  Mstro__Pool__KvEntry *res = malloc(sizeof(Mstro__Pool__KvEntry));
  if(res==NULL) {
    ERR("Failed to allocate KV entry\n");
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }
  mstro__pool__kv_entry__init(res);

  res->key = strdup(mstro_symbol_name(entry->key)); /* FIXME: strdup is wasteful but symbol_name is const */
  if(res->key==NULL) {
    free(res);
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }

  res->val = malloc(sizeof(Mstro__Pool__AVal));
  if(res->val==NULL) {
    ERR("Failed to allocat value box for KV entry\n");
    free(res);
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }
  mstro__pool__aval__init(res->val);

  s=MSTRO_OK;

  switch(entry->kind) {
    case MSTRO_STP_BOOL:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_BOOL;
      res->val->bool_ = *((bool*)entry->val) ? true : false;
      break;
    case MSTRO_STP_UINT:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_UINT64;
      res->val->uint64 = *((uint64_t*)entry->val);
      break;
    case MSTRO_STP_INT:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_INT64;
      res->val->uint64 = *((int64_t*)entry->val);
      break;
    case MSTRO_STP_FLOAT:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_FLOAT;
      res->val->float_ = *((float*)entry->val);
      break;
    case MSTRO_STP_DOUBLE:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_DOUBLE;
      res->val->double_ = *((double*)entry->val);
      break;
    case MSTRO_STP_STR:
    case MSTRO_STP_REGEX:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_STRING;
      /* we need to duplicate strings, since the deallocation of
       * protobuf messages descends into char* members, and so the
       * entry in the dictionary where the entry is will be invalid
       * when the message is deallocated (or vice versa) */
      res->val->string = strdup((char *)entry->val);
       if(res->val->string==NULL) {
         ERR("Failed to allocate string value for KV entry\n");
         free(res->val);
         free(res);
         s=MSTRO_NOMEM;
         goto BAILOUT;
       }
      break;
    case MSTRO_STP_BLOB:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_BYTES;
      mstro_blob *b = (mstro_blob*)entry->val;
      res->val->bytes.len = b->len;
      res->val->bytes.data = malloc(b->len*sizeof(uint8_t));
      memcpy(res->val->bytes.data,b->data,b->len); /* seperate attribute dict from attribute msg structure
     avoid confusion when allocating and deallocating pool messages and attribute dict + avoid leaks */
      //      DEBUG("BLOB of length %zu wrapped as AVal\n", b->len);
      s= MSTRO_OK;
      break;
    case  MSTRO_STP_POINTER:
      res->val->val_case = MSTRO__POOL__AVAL__VAL_BYTES;
      DEBUG("POINTER type not serialized\n");
      s=MSTRO_NOENT;
      break;
    case MSTRO_STP_TIMESTAMP: {
      res->val->val_case = MSTRO__POOL__AVAL__VAL_TIMESTAMP;
      res->val->timestamp = malloc(sizeof(Mstro__Pool__Timestamp));
      if(res->val->timestamp==NULL) {
        ERR("Failed to allocate timestamp AVal\n");
        s=MSTRO_NOMEM;
      } else {
        mstro__pool__timestamp__init(res->val->timestamp);
        const mstro_timestamp *src = (mstro_timestamp*)entry->val;
        res->val->timestamp->sec = src->sec;
        res->val->timestamp->nsec = src->nsec;
        res->val->timestamp->offset = src->offset;
        s=MSTRO_OK;
      }
      break;
    }
    case MSTRO_STP_MMBLAYOUT: {
      res->val->val_case = MSTRO__POOL__AVAL__VAL_MMB_LAYOUT;
      res->val->mmblayout = malloc(sizeof(Mstro__Pool__Mmblayout));
      if(res->val->mmblayout==NULL) {
        ERR("Failed to allocate mmblayout AVal\n");
        s=MSTRO_NOMEM;
        free(res->val);
        free(res);
        goto BAILOUT;
      }
      mstro__pool__mmblayout__init(res->val->mmblayout);
      const mmbLayout *src = (mmbLayout *)entry->val;
      if (src->type != MMB_IRREGULAR){
        ERR("Unsupported mmbLayout type %d \n", src->type);
        s=MSTRO_INVARG;
        free(res->val->mmblayout);
        free(res->val);
        free(res);
        goto BAILOUT;
      }
      res->val->mmblayout->irregular = malloc(sizeof(Mstro__Pool__MmbLayoutIrregular));
      if(res->val->mmblayout->irregular == NULL){
        s=MSTRO_NOMEM;
        free(res->val->mmblayout);
        free(res->val);
        free(res);
        goto BAILOUT;
      }
      DEBUG("mmbLayout type %d \n", src->type);
      DEBUG("mmbLayout n_dims  %zd \n", src->n_dims);
      DEBUG("mmbLayout index %zd \n", src->index);
      DEBUG("mmbLayout element size %zd \n", src->element.size_bytes);
      DEBUG("mmbLayout n_blocks %zd \n", src->irregular.n_blocks.d[0]);
      mstro__pool__mmb_layout_irregular__init(res->val->mmblayout->irregular);
      res->val->mmblayout->type = MSTRO__POOL__MMBLAYOUT__MMB_LAYOUT_TYPE__MMB_IRREGULAR;
      res->val->mmblayout->n_dims = (uint64_t) src->n_dims;
      res->val->mmblayout->index = (uint64_t) src->index;
      res->val->mmblayout->element_size_bytes = (uint64_t) src->element.size_bytes;
      res->val->mmblayout->layout_case = MSTRO__POOL__MMBLAYOUT__LAYOUT_IRREGULAR;
      res->val->mmblayout->irregular->n_blocks = (uint64_t) src->irregular.n_blocks.d[0];
      /**make a copy of offsets and pass it to be serialized */
      uint64_t *offsets = (uint64_t *) malloc(sizeof(uint64_t)*src->irregular.n_blocks.d[0]);
      uint64_t *lengths = (uint64_t *) malloc(sizeof(uint64_t)*src->irregular.n_blocks.d[0]);
      if ((offsets == NULL) || (lengths == NULL)){
        ERR("Failed to allocate mmblayout offsets or lengths \n");
        s=MSTRO_NOMEM;
      }
      /** copy and cast data from src->irregular.offsets (size_t) to uint64_t */
      mstro_status s1 = mstro_cast_size_t_to_unit64_array(src->irregular.offsets, offsets, src->irregular.n_blocks.d[0]);
      mstro_status s2 = mstro_cast_size_t_to_unit64_array(src->irregular.lengths, lengths, src->irregular.n_blocks.d[0]);
      /* something wrong happened, cleanup and fail */
      if ((s != MSTRO_OK) || (s1 != MSTRO_OK) || (s2 != MSTRO_OK)) {
        if(offsets) {
          free(offsets);
        }
        if(lengths) {
          free(lengths);
        }
        free(res->val->mmblayout->irregular);
        free(res->val->mmblayout);
        free(res->val);
        free(res);
        goto BAILOUT;
      }
      res->val->mmblayout->irregular->offsets = offsets;
      res->val->mmblayout->irregular->lengths = lengths;
      for (size_t i = 0; i < src->irregular.n_blocks.d[0]; i++) {
        DEBUG("%zu offsets %"PRIu64" lengths %"PRIu64" \n", i, offsets[i], lengths[i]);
      }
      res->val->mmblayout->irregular->n_offsets = src->irregular.n_blocks.d[0];
      res->val->mmblayout->irregular->n_lengths = src->irregular.n_blocks.d[0];
      s=MSTRO_OK;
      }
      break;
    default:
      ERR("Unsupported attribute type for |%s|, can not serialize: %d\n",
          res->key, entry->kind);
      s=MSTRO_FAIL;
      break;
  }
  if(s!=MSTRO_OK) {
    free(res->key);
    free(res->val);
    free(res);
    res=NULL;
  }

BAILOUT:
  *result_p = res;
  return s;
}
static inline
mstro_status
mstro_cast_size_t_to_unit64_array(size_t *in, uint64_t *out, size_t len){
  for (size_t i = 0; i < len; i++) {
    /*check if the value does not fit in uint64_t */
    if(in[i] >= UINT64_MAX){
      ERR("can not convert from size_t to uint64_t, values are too large");
      return MSTRO_FAIL;
    }
    else {
      out[i] = (uint64_t) in[i];
    }

  }
  return MSTRO_OK;
}


static inline
mstro_status
mstro_cast_unit64_to_size_t_array(uint64_t *in, size_t *out, size_t len){
  for (size_t i = 0; i < len; i++) {
    /*check if the value does not fit in uint64_t */
    if(in[i] >= SIZE_MAX){
      ERR("can not convert from uint64_t to size_t, values are too large");
      return MSTRO_FAIL;
    }
    else {
      out[i] = (size_t) in[i];
    }
  }
  return MSTRO_OK;
}

static inline
void
mstro_attribute_map__mapentry_destroy(Mstro__Pool__KvEntry *entry)
{
  /* this function's idea of object ownership needs to match mstro_attribute_entry_to_mapentry() */
  /* assumes key is not shared with symbol-name */
  switch(entry->val->val_case) {
    case MSTRO__POOL__AVAL__VAL__NOT_SET:
    case MSTRO__POOL__AVAL__VAL_BOOL:
    case MSTRO__POOL__AVAL__VAL_INT32:
    case MSTRO__POOL__AVAL__VAL_INT64:
    case MSTRO__POOL__AVAL__VAL_UINT32:
    case MSTRO__POOL__AVAL__VAL_UINT64:
    case MSTRO__POOL__AVAL__VAL_FLOAT:
    case MSTRO__POOL__AVAL__VAL_DOUBLE:
      /* immediate values */
      break;
    case MSTRO__POOL__AVAL__VAL_STRING:
      /* NOT shared with dict */
      free(entry->val->string);
      entry->val->string = NULL;
      break;
    case MSTRO__POOL__AVAL__VAL_BYTES:
      /* NOT shared with dict */
      entry->val->bytes.len = 0;
      free(entry->val->bytes.data);
      entry->val->bytes.data = NULL;
      break;
    case MSTRO__POOL__AVAL__VAL_TIMESTAMP:
      /* NOT shared with dict */
      free(entry->val->timestamp);
      entry->val->timestamp = NULL;
      break;
    case MSTRO__POOL__AVAL__VAL_MMB_LAYOUT:
      /** nothing is shared with dict ... rely on default deallocator */
      break;
    default:
      ERR("Unexpected mapentry type: %d\n", entry->val->val_case);
  }
  mstro__pool__kv_entry__free_unpacked(entry, NULL);

  return;
}



static inline
mstro_status
mstro_attribute_dict_to_kvmap(mstro_attribute_dict dict,
                              Mstro__Pool__Attributes__Map **map)
{
  mstro_status s=MSTRO_UNIMPL;

  Mstro__Pool__Attributes__Map *res
      = malloc(sizeof(Mstro__Pool__Attributes__Map));
  if(res==NULL) {
    ERR("Cannot allocate k-v-map\n");
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }
  mstro__pool__attributes__map__init(res);

  res->n_map = HASH_COUNT(dict->dict);
  if(res->n_map==0) {
    res->map = NULL;
    s=MSTRO_OK;
    goto BAILOUT;
  }
  DEBUG("Dict has %zu entries\n", res->n_map);

  res->map = malloc(res->n_map * sizeof(Mstro__Pool__KvEntry*));
  if(res->map==NULL) {
    ERR("Failed to allocate k-v-map array\n");
    free(res);
    res=NULL;
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }

  struct mstro_attribute_entry_ *el, *tmp;
  size_t i=0;
  HASH_ITER(hh, dict->dict, el, tmp) {
    DEBUG("serializing attribute entry %s\n", mstro_symbol_name(el->key));

    s = mstro_attribute_entry_to_mapentry(el, &(res->map[i]));
    if(s!=MSTRO_OK) {
      if(s==MSTRO_NOENT) {
        DEBUG("Skipped dictionary entry %s\n", mstro_symbol_name(el->key));
      } else {
        ERR("Failed to serialize dictionary entry %s\n",
            mstro_symbol_name(el->key));
        res->n_map = 0;
        free(res->map);
        free(res);
        res=NULL;
        goto BAILOUT;
      }
    } else {
      DEBUG("Serialized |%s| successfully at index %d \n",  mstro_symbol_name(el->key), i);
      i++;
    }
  }
  DEBUG("Total non-serialized entries: %zu\n", res->n_map - i);
  res->n_map = i; /* we could have skipped some */
  s=MSTRO_OK;

BAILOUT:
  *map = res;

  return s;
}


mstro_status
mstro_attribute_dict_to_message(mstro_attribute_dict dict,
                                Mstro__Pool__Attributes **msg_p)
{
  if(dict==NULL) {
    ERR("Invalid dictionary\n");
    return MSTRO_INVARG;
  }
  if(msg_p==NULL) {
    ERR("Invalid attribiutes destination\n");
    return MSTRO_INVOUT;
  }

  Mstro__Pool__Attributes__Map *map=NULL;
  mstro_status s= mstro_attribute_dict_to_kvmap(dict, &map);
  if(s!=MSTRO_OK) {
    ERR("Failed to construct k-v-map\n");
    return s;
  }
  assert(map!=NULL);

  Mstro__Pool__Attributes *res = malloc(sizeof(Mstro__Pool__Attributes));
  if(res==NULL) {
    ERR("Cannot allocate attributes message\n");
    /* fixme: leaking inside */
    free(map);
    return MSTRO_NOMEM;
  }
  mstro__pool__attributes__init(res);
  res->val_case = MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP;
  res->kv_map = map;
  /* All attribute names are fully qualified, so the we do need a
   * default namespace. FIXME: we could save on message size by
   * picking the 'most-used' prefix and set that, compressing names */
  res->default_namespace = NULL;
  *msg_p = res;
  return MSTRO_OK;
}

mstro_status
mstro_attribute_dict_message_dispose(mstro_attribute_dict dict,
                                     Mstro__Pool__Attributes *msg)
{
  if(dict==NULL)
    return MSTRO_INVARG;
  if(msg==NULL)
    return MSTRO_INVARG;

  /* It's nontrivial to free the message, as some of its content
   * overlaps with dictionary entries */
  if(msg) {
    /* certain types share pointers with dictionary entries. We'll
     * kill them here, then let protobuf clean up around us */
    assert(msg->val_case == MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP);
    for(size_t i=0; i<msg->kv_map->n_map; i++) {
      mstro_attribute_map__mapentry_destroy(msg->kv_map->map[i]);
    }
    msg->kv_map->n_map = 0;
    free(msg->kv_map->map);
    msg->kv_map->map=NULL;

    mstro__pool__attributes__free_unpacked(msg, NULL);
  }

  return MSTRO_OK;
}



static inline
mstro_status
mstro_attribute_val_cmp(const struct mstro_stp_val *attrtype,
                        const void *lhsval, size_t lhsvalsize,
                        enum mstro_attribute_val_cmp_op cmp,
                        const void *rhsval, size_t rhsvalsize,
                        bool *result)
{
  if(attrtype==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;

  bool non_negated = true;
  switch(cmp) {
    case MSTRO_ATTR_VAL_CMP_NEQ:
    non_negated=false; /* fallthrough */
    case MSTRO_ATTR_VAL_CMP_EQ: {
      switch(attrtype->kind) {
        case MSTRO_STP_BOOL:   /* fallthrough */
        case MSTRO_STP_UINT:   /* fallthrough */
        case MSTRO_STP_INT:    /* fallthrough */
        case MSTRO_STP_FLOAT:  /* fallthrough */
        case MSTRO_STP_DOUBLE: /* fallthrough */
        case MSTRO_STP_STR:    /* fallthrough */
        case MSTRO_STP_REGEX:  /* fallthrough */
        case MSTRO_STP_BLOB:   /* fallthrough */
        case MSTRO_STP_POINTER:/* fallthrough */
          *result = (memcmp(lhsval, rhsval, lhsvalsize)==0) ? non_negated : !(non_negated);
          return MSTRO_OK;
        case MSTRO_STP_TIMESTAMP: {
          const mstro_timestamp *l = (const mstro_timestamp*) lhsval;
          const mstro_timestamp *r = (const mstro_timestamp*) rhsval;
          int x = mstro_timestamp_compare(l,r);
          if(x==0)
            *result = non_negated;
          else
            *result = !non_negated;
          return MSTRO_OK;
        }
        default:
          ERR("Unsupported type %d for EQ/NEQ comparison\n", attrtype->kind);
          return MSTRO_FAIL;
      }
      break;
    }

    case MSTRO_ATTR_VAL_CMP_LT:
      switch(attrtype->kind) {
        case MSTRO_STP_UINT: {
          const uint64_t *l = (const uint64_t*)lhsval;
          const uint64_t *r = (const uint64_t*)rhsval;
          *result = (*l) < (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_INT: {
          const int64_t *l = (const int64_t*)lhsval;
          const int64_t *r = (const int64_t*)rhsval;
          *result = (*l) < (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_FLOAT: {
          const float *l = (const float *)lhsval;
          const float *r = (const float *)rhsval;
          *result = (*l) < (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_DOUBLE: {
          const double *l = (const double *)lhsval;
          const double *r = (const double *)rhsval;
          *result = (*l) < (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_TIMESTAMP: {
          const mstro_timestamp *l = (const mstro_timestamp*) lhsval;
          const mstro_timestamp *r = (const mstro_timestamp*) rhsval;
          int x = mstro_timestamp_compare(l,r);
          if(x<0)
            *result = true;
          else
            *result = false;
          return MSTRO_OK;
        }
        default:
          ERR("Comparison LT not supported for type %d\n", attrtype->kind);
          return MSTRO_FAIL;
      }
      break;
    case MSTRO_ATTR_VAL_CMP_GE: {
      /* GE turned into !LT */
      mstro_status s=mstro_attribute_val_cmp(attrtype,
                                             lhsval, lhsvalsize,
                                             MSTRO_ATTR_VAL_CMP_LT,
                                             rhsval, rhsvalsize,
                                             result);
      if(s==MSTRO_OK) {
        *result = !result;
      }
      return s;
    }

    case MSTRO_ATTR_VAL_CMP_LE:
      switch(attrtype->kind) {
        case MSTRO_STP_UINT: {
          uint64_t *l = (uint64_t*)lhsval;
          uint64_t *r = (uint64_t*)rhsval;
          *result = (*l) <= (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_INT: {
          int64_t *l = (int64_t*)lhsval;
          int64_t *r = (int64_t*)rhsval;
          *result = (*l) <= (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_FLOAT: {
          float *l = (float *)lhsval;
          float *r = (float *)rhsval;
          *result = (*l) <= (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_DOUBLE: {
          double *l = (double *)lhsval;
          double *r = (double *)rhsval;
          *result = (*l) <= (*r) ? true : false;
          return MSTRO_OK;
        }
        case MSTRO_STP_TIMESTAMP: {
          const mstro_timestamp *l = (const mstro_timestamp*) lhsval;
          const mstro_timestamp *r = (const mstro_timestamp*) rhsval;
          int x = mstro_timestamp_compare(l,r);
          if(x<=0)
            *result = true;
          else
            *result = false;
          return MSTRO_OK;
        }
        default:
          ERR("Comparison LE not supported for type %d\n", attrtype->kind);
          return MSTRO_FAIL;
      }
    case MSTRO_ATTR_VAL_CMP_GT: {
      /* GT turned into !LE */
      mstro_status s=mstro_attribute_val_cmp(attrtype,
                                             lhsval, lhsvalsize,
                                             MSTRO_ATTR_VAL_CMP_LE,
                                             rhsval, rhsvalsize,
                                             result);
      if(s==MSTRO_OK) {
        *result = !result;
      }
      return s;
    }

    case MSTRO_ATTR_VAL_CMP_RMATCH: /* fallthrough */
    case MSTRO_ATTR_VAL_CMP_RMATCH_ICASE:
      ERR("Unimplemented RMATCH comparison\n");
      return MSTRO_UNIMPL;

    default:
      ERR("Unknown comparison op: %d\n");
      return MSTRO_INVARG;
  }

  /* unreached */
  *result=true;
  return MSTRO_FAIL;
}

mstro_status
mstro_attribute_val_cmp_str(mstro_schema_attribute attr,
                            const char *valstr, enum mstro_attribute_val_cmp_op cmp,
                            const Mstro__Pool__AVal *aval, bool *result)
{
  if(attr==NULL||valstr==NULL||aval==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;

  const struct mstro_stp_val *attrtype = attr->type_parse_closure.info;
  void *rhsval=NULL;
  const void *lhsval=NULL;
  size_t rhsvalsize = 0, lhsvalsize = 0;
  /* scratch space */
  uint8_t scratch[MAX(sizeof(mstro_timestamp),1)];

  /* parse the string value ("rhs") */
  mstro_status s = mstro_attribute_val_parse(attrtype, valstr, &rhsval, &rhsvalsize);
  if(s!=MSTRO_OK) {
    ERR("Failed to parse value for comparison op\n");
    return MSTRO_FAIL;
  }

  /* in most cases this is what we want (asserts below let us do
   * this); exceptions will overwrite (BLOB for instance, and
   * string): */
  lhsvalsize = rhsvalsize;

  /* un-type attribute value ("lhs") */
  switch(aval->val_case) {
    case MSTRO__POOL__AVAL__VAL_INT32:
      ERR("Unimplemented wire type int32\n");
      return MSTRO_UNIMPL;

    case MSTRO__POOL__AVAL__VAL_BOOL:
      assert(attrtype->kind == MSTRO_STP_BOOL);
      lhsval = &aval->bool_;
      break;

    case MSTRO__POOL__AVAL__VAL_INT64:
      assert(attrtype->kind == MSTRO_STP_INT);
      lhsval = &aval->int64;
      break;

    case MSTRO__POOL__AVAL__VAL_FLOAT:
      assert(attrtype->kind == MSTRO_STP_FLOAT);
      lhsval = &aval->float_;
      break;

    case MSTRO__POOL__AVAL__VAL_DOUBLE:
      assert(attrtype->kind == MSTRO_STP_DOUBLE);
      lhsval = &aval->double_;
      break;

    case MSTRO__POOL__AVAL__VAL_STRING:
      assert(attrtype->kind == MSTRO_STP_STR || attrtype->kind == MSTRO_STP_REGEX);
      lhsval = aval->string;
      lhsvalsize = strlen(aval->string);
      break;

    case MSTRO__POOL__AVAL__VAL_BYTES:
      assert(attrtype->kind == MSTRO_STP_BLOB);
      lhsval = aval->bytes.data;
      lhsvalsize = aval->bytes.len;
      break;

    case MSTRO__POOL__AVAL__VAL_TIMESTAMP:
       /* FIXME: aren't timestamps converted to strings on the wire?
        * If not the timestamp kind should be part of the public enum
        * definition */
      assert(attrtype->kind == MSTRO_STP_TIMESTAMP);
      mstro_timestamp *tmp = (mstro_timestamp*)scratch;
      tmp->sec = aval->timestamp->sec;
      tmp->nsec = aval->timestamp->nsec;
      tmp->offset = aval->timestamp->offset;
      lhsval = tmp;
      break;

    default:
      ERR("Unexpected attribute value kind: %d\n", aval->val_case);
      return MSTRO_FAIL;
  }

  s = mstro_attribute_val_cmp(attrtype,
                              lhsval, lhsvalsize,
                              cmp,
                              rhsval, rhsvalsize,
                              result);
  assert(rhsval!=NULL);
  free(rhsval); // was allocated at val_parse time and scope is ending

  return s;
}

void
mstro_attribute_dict_print(mstro_attribute_dict attr)
{
  mstro_attribute_entry tmpd;
  enum mstro_cdo_attr_value_type type;
  const void* val;
  mstro_status s;
  mstro_schema schema = attr->schema;
  char* str = malloc(sizeof(char)*8092);
  str[0] = '\0';
 
  do {
    const char* namespace = schema->schema_namespace;
    schema = schema->next;
    sprintf(str+strlen(str), "%s {", namespace);
    int c = 0;
    for (tmpd = attr->dict; tmpd != NULL; tmpd = tmpd->hh.next) {
      size_t len = strlen(namespace);
      if (strncmp(mstro_symbol_name(tmpd->key),namespace, len))
        continue;
      s = mstro_attribute_dict_get(attr, mstro_symbol_name(tmpd->key),
                                &type, &val, NULL, 0);
      if (c == 1) sprintf(str+strlen(str), ", ");
      if (s == MSTRO_OK) {
        c = 1;
        switch(type) {
        case MSTRO_CDO_ATTR_VALUE_cstring: 
          sprintf(str+strlen(str), "%s = %s", mstro_symbol_name(tmpd->key)+len, (char*)val);
          break;
        case MSTRO_CDO_ATTR_VALUE_int64: 
          sprintf(str+strlen(str), "%s = %"PRId64"", mstro_symbol_name(tmpd->key)+len, *(int64_t*)val);
          break;
        case MSTRO_CDO_ATTR_VALUE_uint64: 
          sprintf(str+strlen(str), "%s = %"PRIu64"", mstro_symbol_name(tmpd->key)+len, *(uint64_t*)val);
          break;
        default: 
          sprintf(str+strlen(str), "%s = N/A", mstro_symbol_name(tmpd->key)+len);
        }
      }
    }
    sprintf(str+strlen(str), "} ");
  } while (schema);
  sprintf(str+strlen(str), "\n");
  DEBUG("%s", str);
  free(str);
}

