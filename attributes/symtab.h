/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Symbol Table implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_SYMTAB_H_
#define MAESTRO_SYMTAB_H_ 1

#include "maestro/status.h"

/**@defgroup MSTRO_Symtab Maestro Symbol table
 ** @{
 ** This is a simple symbol table implementation: String keys interned in a hash table.
 **
 ** The interesting feature is that symbols can be interned with a
 ** 'permanent' flag: This declares that the address of the string
 ** used to intern the symbol will never change during the course of
 ** program, so that if this address is passed to the lookup function
 ** no string comparison is necessary. This is used to intern
 ** hard-coded constants (#define or static const char *) for the
 ** built-in symbols and make their lookup much more efficient.
 **
 ** Furthermore, since strings are likely to share large initial
 ** substrings (due to namespace prefixes being the same) the final
 ** string equality check is done from right-to-left. FIXME: This
 ** needs benchmarking.
 **/

/** A symbol table entry */
struct mstro_symtab_entry;

/* A symbol table */
typedef struct mstro_symtab_entry *mstro_symtab;

/* An interned symbol */
typedef struct mstro_symtab_entry *mstro_symbol;

/** Create a new symbol table */
mstro_status
mstro_symtab_create(mstro_symtab *result);

/** Destroy a symbol table */
mstro_status
mstro_symtab_destroy(mstro_symtab *tab);
    
/** Ensure a symbol table entry for STRING.
 **
 ** If a matching entry already exists no new one is created.
 **
 ** If INTERNED_SYMBOL is non-NULL, returns the symbol through *INTERNED_SYMBOL.
 **/
mstro_status
mstro_symtab_intern(mstro_symtab *tab, const char *string, mstro_symbol *interned_symbol);

/** Ensure a symbol table entry for STRING, when the address of STRING is guaranteed to not change.
 **
 ** If a matching entry already exists no new one is created.
 **
 ** It is an error to call this function with a string argument that
 ** compares equal to any already existing one, permanent or not. In
 ** these cases MSTRO_INVARG is returned.
 **
 ** If INTERNED_SYMBOL is non-NULL, returns the symbol through *INTERNED_SYMBOL.
 **/
mstro_status
mstro_symtab_intern_permanent(mstro_symtab *tab, const char *string, mstro_symbol *interned_symbol);

/** Remove a symbol from the symbol table */
mstro_status
mstro_symtab_unintern(mstro_symtab *tab, mstro_symbol interned_symbol);


/** Check whether STRING is present in TAB. On success: returns the
 * symbol for STRING in *result, otherwise NULL */
mstro_status
mstro_symtab_lookup(mstro_symtab tab, const char *string, mstro_symbol *result);


/** Return a printable name for the symbol */
const char *
mstro_symbol_name(mstro_symbol sym);


/**@} (end of group MSTRO_Symtab) */

#endif
