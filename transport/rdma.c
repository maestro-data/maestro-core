/* -*- mode:c -*- */
/** @file
 ** @brief OFI RDMA Transport implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro/logging.h"
#include "maestro/i_ofi.h"
#include "maestro/i_ofi_threads.h"
#include "maestro/i_memlock.h"
#include "maestro/i_memory.h"
#include "maestro/i_globals.h"
#include "maestro/i_pool_manager_protocol.h"
#include "maestro/i_pool_manager_registry.h"

#include "transport.h"
#include "transport_rdma.h"

#include "attributes/maestro-schema.h"

#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>
#include <string.h>

#ifndef MIN
#define MIN(x,y) ((x)<(y) ? (x) : (y))
#endif

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)

#define UNLOCK_RDMA_PENDING_TABLE do{\
	        int  ret=pthread_mutex_unlock(&g_mstro_transport_rdma_pending_table_mtx);\
                if(ret!=0) {\
                      ERR("Failed to unlock pending RDMA op table\n");\
                      return MSTRO_FAIL;\
                }\
              }while(0)

/** hashing of memory registrations is done on an aggregate key: CDO
 * ID, OFI domain and maybe OFI endpoint.
 *
 * Be sure to make padding 0, and set EP to NULL if FI_MR_ENDPOINT is
 * not set in the domain attributes */
struct mstro_transport_mreg_table_key {
  struct mstro_cdo_id id;      /**< CDO ID for which this registration is done */
  struct fid_domain *domain;   /**< domain in which this registration is valid */
  struct fid_ep     *ep;       /**< endpoint (if FI_MR_ENDPOINT is set) */
};

/*  https://ofiwg.github.io/libfabric/v1.10.1/man/fi_mr.3.html */
struct mstro_transport_mreg_table_entry {
  UT_hash_handle hh;                         /**< hash on CDO id/domain/ep */
  struct mstro_transport_mreg_table_key key; /**< hash key */
  
  uint64_t refcount;                         /**< number of tickets
                                              * issued for this ID */
  void *addr;                                /**< the raw-ptr address
                                              * registered */
  uint64_t len;                              /**< length of the memory
                                              * region */
  struct fid_mr *mr_reg;                     /**< the OFI memory
                                              * registration for this
                                              * CDO's raw-ptr */
};


/** table of memory registrations for transport purposes. Protected by @ref g_mstro_transport_mreg_table_lock */
struct mstro_transport_mreg_table_entry *g_mstro_transport_mreg_table = NULL;
/** Lock protecting @ref g_mstro_transport_mreg_table */
pthread_mutex_t g_mstro_transport_mreg_table_lock = PTHREAD_MUTEX_INITIALIZER;
/** condition var to signal change on @ref g_mstro_transport_mreg_table */
pthread_cond_t g_mstro_transport_mreg_table_cond = PTHREAD_COND_INITIALIZER;

/** table of memory registrations for incoming cdo transfers. Protected by @ref g_mstro_transport_rdma_pending_table_mtx */
struct mstro_transport_mreg_table_entry *g_mstro_transport_rdma_pending_table = NULL;
/** Lock protecting @ref g_mstro_transport_rdma_pending_table */
pthread_mutex_t g_mstro_transport_rdma_pending_table_mtx = PTHREAD_MUTEX_INITIALIZER;

/** find the registry entry for endpoint,cdoid, assuming lock on table is already held by caller */
static mstro_status
mstro_transport_rdma__mreg_entry_find__locked(const struct mstro_endpoint *ep,
                                              const struct mstro_cdo_id *cdoid,
					      struct mstro_transport_mreg_table_entry *reg_table,
                                              struct mstro_transport_mreg_table_entry **regentry_p)
{
  assert(ep!=NULL);
  
  struct mstro_transport_mreg_table_key key;
  memset(&key,0,sizeof(struct mstro_transport_mreg_table_key));
  
  key.id = *cdoid;
  key.domain = ep->domain;
  key.ep = ep->fi->domain_attr->mr_mode & FI_MR_ENDPOINT ? ep->ep : NULL;

  HASH_FIND(hh, reg_table,
            &key, sizeof(struct mstro_transport_mreg_table_key),
	    *regentry_p);
  if (!*regentry_p) {
    return MSTRO_NOENT;
  } else {
    return MSTRO_OK;
  }
}

/** find the registry entry for (APPID,CDOID) */
static mstro_status
mstro_transport_rdma__mreg_entry_find(mstro_app_id appid,
                                      const struct mstro_cdo_id *cdoid,
				      struct mstro_transport_mreg_table_entry *reg_table,
				      pthread_mutex_t *table_lock,
                                      struct mstro_transport_mreg_table_entry **regentry_p)
{
  assert(appid!=MSTRO_APP_ID_INVALID);
  assert(cdoid!=NULL);
  if(regentry_p==NULL)
    return MSTRO_INVOUT;
  
  struct mstro_pm_app_registry_entry *e;
  mstro_status status = mstro_pm_app_lookup(appid, &e);
  if(e==NULL) {
    ERR("App %" PRIappid " not in app table\n", appid);
    return MSTRO_FAIL;
  } 

  int err = pthread_mutex_lock(table_lock);
  if (err) {
    ERR("Couldn't lock mutex on mreg table\n");
	return MSTRO_FAIL;
  }
  status = mstro_transport_rdma__mreg_entry_find__locked(e->ep, cdoid, reg_table, regentry_p);
  
  err = pthread_mutex_unlock(table_lock);
  if (err) {
    ERR("Couldn't unlock mutex on mreg table\n");
    status=MSTRO_FAIL;
  }
  return status;
}


mstro_status  
mstro_transport_rdma_src_execute(mstro_cdo src, Mstro__Pool__TransferTicket* ticket)
{
  if (src == NULL || ticket == NULL)
    return MSTRO_INVARG;

/* XXX we will be ignoring Mamba for now */

  if ((uintptr_t)src->raw_ptr % (size_t) sysconf(_SC_PAGESIZE) != 0) {
    WARN ("CDO data is not pagesize aligned, fall-back on other transport\n");
    return MSTRO_INVARG;
  }
  mstro_status status = MSTRO_OK;
  struct mstro_transport_mreg_table_entry* regentry = NULL;
  struct mstro_cdo_id* cdo_id_p = &(src->gid);

  struct mstro_pm_app_registry_entry *e;
  status = mstro_pm_app_lookup(ticket->ofi->dstid->id, &e);
  if(e==NULL) {
    ERR("Target %zu not in app table\n", ticket->ofi->dstid);
    return MSTRO_FAIL;
  } 

  int err = pthread_mutex_lock(&g_mstro_transport_mreg_table_lock);
  if (err) {
    ERR("Couldn't lock mutex on mreg table\n");
	return MSTRO_FAIL;
  }

  status = mstro_transport_rdma__mreg_entry_find__locked(e->ep, cdo_id_p, g_mstro_transport_mreg_table, &regentry);

  if (regentry) {
    NOISE("another listener on existing mreg\n");
    regentry->refcount++;
    goto FILL_TICKET_FROM_REGISTRATION;
  }
  
FRESH_REGISTRATION:
  /* make a fresh entry */
  regentry = calloc(1, sizeof(struct mstro_transport_mreg_table_entry)); /* to make sure we have 0-initialized key padding */
  if(regentry==NULL) {
    ERR("Cannot allocate new mreg entry\n");
    status=MSTRO_NOMEM;
    goto BAILOUT_UNLOCK;
  }

  memset(&regentry->key, 0, sizeof(struct mstro_transport_mreg_table_key));
  
  regentry->key.id = *cdo_id_p;
  regentry->key.domain = e->ep->domain;
  regentry->key.ep = e->ep->fi->domain_attr->mr_mode & FI_MR_ENDPOINT ? e->ep->ep : NULL;

  regentry->refcount=1;

  {
    struct mstro_transport_datalen dl;
    status = mstro_transport__src_datalen_get(src, ticket, &dl);
    if (status != MSTRO_OK) {
      ERR("Couldn't get CDO data and size for RDMA transport (status: %s)\n", mstro_status_description(status));
      goto BAILOUT_UNLOCK;
    }
    /*we always register the whole cdo memory for transfers, even for distributed cdos tickets*/
    regentry->addr=(void *) ((char *) dl.data); 
    regentry->len=dl.len;

    if (mstro_memlock(dl.data, dl.len) != MSTRO_OK) {
      ERR("Couldn't lock raw-ptr for RDMA transport of CDO (%s)\n", src->name);
      status=MSTRO_FAIL;
      goto BAILOUT_UNLOCK;
    }

    /*  register this address for OFI */
    struct fid_mr *mr;
    void * local_buf_mr_desc = NULL;

    status = mstro_ofi__mreg_attr(FI_HMEM_SYSTEM, /* system memory FIXME check CDO memory layer */
                  0, /*device id -- not used when using system memory FIXME check device id*/
                  e->ep->fi->domain_attr, e->ep->domain, /*domain attr and domain */
                  FI_REMOTE_READ,regentry->addr, dl.len, /*access, buf, len*/
                  &mr,  &local_buf_mr_desc); /*output mr and desc */

    if(status != MSTRO_OK) {
      ERR("Couldn't register memory region for RDMA transport (err: %d)\n", err);
      status=MSTRO_FAIL;
      goto BAILOUT_UNLOCK;
    }
    regentry->mr_reg = mr;

    status = mstro_ofi__maybe_bind_and_enable_mreg(regentry->key.ep,
                                                   e->ep->fi->domain_attr->mr_mode,
                                                   mr);
    if(status!=MSTRO_OK) {
      ERR("Failed to bind peer buffer mreg to endpoint\n");
      int ret = fi_close((struct fid *)mr);
      if(ret<0) {
        ERR("Failed to close peer buffer mreg: %d (%s)\n", ret, strerror(-ret));
        status=MSTRO_OFI_FAIL;
      }
      goto BAILOUT_UNLOCK;
    }


    WITH_CDO_ID_STR(
	idstr, &(regentry->key.id),
	DEBUG("Adding CDO %s (id %s) to mreg table\n",
	      src->name, idstr););

    HASH_ADD(hh, g_mstro_transport_mreg_table,
	     key, sizeof(struct mstro_transport_mreg_table_key),
	     regentry);
  }

FILL_TICKET_FROM_REGISTRATION:
  {
    uint8_t* mr_key=NULL;
    size_t keysize=0;
    uint64_t mr_addr=(uint64_t)regentry->addr;
    status = mstro_mr_key_get(e->ep->fi, regentry->mr_reg, &mr_key, &keysize, &mr_addr);
    if(status!=MSTRO_OK) {
      ERR("Failed to obtain MR key: %d (%s)\n", 
	  status, mstro_status_description(status));
      goto BAILOUT_UNLOCK;
    }

    ticket->ofi->h->mr_key.len = keysize;
    ticket->ofi->h->mr_key.data = mr_key;
    ticket->ofi->h->single = mr_addr;
    //DEBUG("Ticket: key-len %zu, data %p, addr %p\n", ticket->ofi->h->mr_key.len, ticket->ofi->h->mr_key.len, ticket->ofi->h->single );
  }

BAILOUT_UNLOCK:
  {
    int err = pthread_mutex_unlock(&g_mstro_transport_mreg_table_lock);
    if (err) {
      ERR("Couldn't unlock mutex on mreg table\n");
      status=MSTRO_FAIL;
    }
  }

  return status;
}

/* "Bottom half" execution handler: called by mstro_pc__handle_transfer_completed */
mstro_status
mstro_transport_rdma_src_execute_bh(Mstro__Pool__TransferCompleted *tc)
{
  if (tc == NULL) {
    ERR ("Transfer completed msg is null\n");
    return MSTRO_INVARG;
  }
  mstro_status status = MSTRO_OK;

  struct mstro_transport_mreg_table_entry* regentry = NULL;
  struct mstro_cdo_id cdoid = { .qw[0] = tc->srccdoid->qw0,
                                .qw[1] = tc->srccdoid->qw1,
                                .local_id = tc->srccdoid->local_id };

  struct mstro_pm_app_registry_entry *e;
  status = mstro_pm_app_lookup(tc->dstid->id, &e);
  if(e==NULL) {
    ERR("Target app %" PRIappid " not in app table\n", tc->dstid->id);
    return MSTRO_FAIL;
  } 


  int err = pthread_mutex_lock(&g_mstro_transport_mreg_table_lock);
  if (err) {
    ERR("Couldn't lock mutex on mreg table\n");
	return MSTRO_FAIL;
  }

  status = mstro_transport_rdma__mreg_entry_find__locked(e->ep, &cdoid, g_mstro_transport_mreg_table, &regentry);

  if (!regentry) {
    DEBUG("RDMA transfer completion message does not require memory registration cleanup on source, likely 0-length\n");
    status=MSTRO_NOENT;
    goto BAILOUT_UNLOCK;
  }

  regentry->refcount--;
  DEBUG ("A client is done using memory region (start: %p) used for RDMA\n", regentry->addr);

  if (regentry->refcount > 0) 
    goto BAILOUT_UNLOCK; 

  DEBUG ("Unregistering memory region (start: %p) used for RDMA\n", regentry->addr);

  err = fi_close((struct fid*)regentry->mr_reg);
  if(err) {
    ERR("Couldn't unregister memory region for RDMA transport (err: %d)\n", err);
    status=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }

  status = mstro_memunlock(regentry->addr, regentry->len);
  if (status != MSTRO_OK) {
    ERR("Couldn't unlock raw-ptr for RDMA transport\n");
    goto BAILOUT_UNLOCK;
  }

  HASH_DEL(g_mstro_transport_mreg_table, regentry);
  free(regentry);
  err = pthread_cond_signal(&g_mstro_transport_mreg_table_cond);
  if (err) {
      ERR("Couldn't signal mreg entry change\n");
      status = MSTRO_FAIL;
  }

BAILOUT_UNLOCK:
  err = pthread_mutex_unlock(&g_mstro_transport_mreg_table_lock);
  if (err) {
    ERR("Couldn't unlock mutex on mreg table\n");
    status=MSTRO_FAIL;
  }
  
  return status;
}

#define NUM_READ_RETRIES 3
static
mstro_status
mstro_transport_rdma__read(const struct mstro_pm_app_registry_entry *app_entry,
                           mstro_cdo cdo_dst,
                           void *mr_desc,
                           uint64_t mr_addr, uint64_t mr_key,
                           size_t offset, size_t len, size_t src_offset, size_t dst_offset, 
                           struct mstro_transport_rdma_cb_args *closure)
{
  mstro_status status;
  mstro_event event = NULL;

  DEBUG("Doing event creation\n");
  status = mstro_event_create(g_transport_rdma_edom, mstro_transport_rdma_cb,
                              (void*)closure, mstro_transport_rdma_dtor, false, &event);
  if (status != MSTRO_OK) {
    ERR("Couldn't create event (%s)\n", mstro_status_description(status));
    goto BAILOUT;
  } else {
    mstro_event_id eid;
    status = mstro_event_id_get(event, &eid);
    if (status != MSTRO_OK) {
      DEBUG("Failed to query event id (event @%p)\n", event);
      goto BAILOUT;
    }
    DEBUG("Created event id %" PRIx64 " to monitor RDMA read completion of at offset %zu, len %zu.\n",
          eid, offset, len);
  }

  status = mstro_ofi_threads_enqueue_rdma_read(app_entry->addr, (uint8_t*)cdo_dst->raw_ptr+offset+dst_offset, len, 
                                    mr_desc, mr_addr+offset+src_offset, mr_key, 
                                    app_entry->ep, event, NULL);

BAILOUT:
  return status;
}
static inline
int64_t
mstro_transport_get_cdo_size(mstro_cdo cdo)
{
	mstro_status s = MSTRO_OK;
	const void *sizep=NULL;
	enum mstro_cdo_attr_value_type type;
        int64_t cdo_size = -1;
	mmbLayout *cdo_layout = NULL;
	/*local size wins ... it should reflect the amount of memory allocated for this cdo*/
	s = mstro_attribute_dict_get(cdo->attributes,
                                            MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                            &type, &sizep, NULL, false);
	if(s==MSTRO_OK) {
		cdo_size =*(int64_t*)sizep;
	} else { /* we may calculate the cdo size from distribution for dist cdos */
		s = mstro_attribute_pool_find_dist_layout(cdo->attributes_msg, &cdo_layout); 
		assert(s == MSTRO_OK);
		if(cdo_layout == NULL)
		{ 
			cdo_size = -1; /*no distribution info, we may depend on ticket */
		}
		else
		{ /*calculate size for distribution -- FIXME assuming irregular layout*/
			assert(cdo_layout->type == MMB_IRREGULAR);
			cdo_size = cdo_layout->irregular.lengths[cdo_layout->index] * cdo_layout->element.size_bytes;
			mmb_layout_destroy(cdo_layout);
		}


	}
	DEBUG("calculated cdo size is %"PRId64" \n", cdo_size);
	return cdo_size;
}

mstro_status
mstro_transport_rdma_dst_execute(mstro_cdo cdo_dst, Mstro__Pool__TransferTicket* ticket)
{
  if (cdo_dst == NULL || ticket == NULL) {
    ERR ("cdo or ticket is null\n");
    return MSTRO_INVARG;
  }

  mstro_status status = MSTRO_OK;
  int64_t reg_len, transfer_len; 
  int fresh_alloc_route=false;
  struct mstro_transport_rdma_cb_args* closure=NULL;
  struct mstro_pm_app_registry_entry *app_entry = NULL;
  void* local_buf_mr_desc=NULL;
  int ret;
  struct mstro_transport_mreg_table_entry* regentry = NULL;

  /*app entry */
  status = mstro_pm_app_lookup(ticket->srcid->id, &app_entry);
  if(app_entry==NULL) {
	  ERR("Target %zu not in app table\n", ticket->srcid);
	  return MSTRO_FAIL;
  } else {
	  DEBUG("Target found in app table\n");
  }

  /*getting key from ticket */
  /* FIXME: this kind of code should be factored out */
  size_t keysize = ticket->ofi->h->mr_key.len;
  uint8_t* tmp = (uint8_t*)(ticket->ofi->h->mr_key.data);
  uint64_t mr_addr = ticket->ofi->h->single;
  uint64_t mr_key;
  struct fid_mr *mr=NULL;

  switch(keysize) {
	  case 1: { uint8_t  x; memcpy(&x,tmp, sizeof(x)); mr_key = x; break; }
	  case 2: { uint16_t x; memcpy(&x,tmp, sizeof(x)); mr_key = x; break; }
	  case 4: { uint32_t x; memcpy(&x,tmp, sizeof(x)); mr_key = x; break; }
	  case 8: {             memcpy(&mr_key,tmp, sizeof(mr_key));   break; }
	  default: {
			   ERR("Unhandled MR key length -- likely needs OFI based translation\n");
			   return MSTRO_UNIMPL;
		   }
  }


  /*check registrations hash table first */
  ret=pthread_mutex_lock(&g_mstro_transport_rdma_pending_table_mtx);
  if(ret!=0) {
    ERR("Failed to lock pending RDMA op table\n");
    status=MSTRO_FAIL;
    return status;
  }

  status = mstro_transport_rdma__mreg_entry_find__locked(app_entry->ep, &(cdo_dst->gid), g_mstro_transport_rdma_pending_table, &regentry);
  

  if (!regentry) {
          DEBUG("Registering CDO memory for RDMA transfer on destination\n");
	  reg_len = mstro_transport_get_cdo_size(cdo_dst);
	  if(reg_len == -1) { /*fall back to ticket size */
		  reg_len = ticket->data_size;
	  }
	  assert(reg_len > 0); /* this should have been checked before */
	  /* FIXME: XXX we will be ignoring Mamba for now */
	  if (cdo_dst->raw_ptr != NULL) {
		  if ((uintptr_t)cdo_dst->raw_ptr % (size_t) sysconf(_SC_PAGESIZE) != 0) {
			  ERR ("Can't memlock an unaligned raw_ptr\n");
			  UNLOCK_RDMA_PENDING_TABLE;
			  return MSTRO_INVARG;
		  }
		  if (mstro_memlock(cdo_dst->raw_ptr, reg_len) != MSTRO_OK) {
			  ERR("Couldn't lock raw-ptr for RDMA transport (read) of CDO (%s)\n", cdo_dst->name);
			  UNLOCK_RDMA_PENDING_TABLE;
			  return MSTRO_FAIL;
		  }
	  } else {
		  DEBUG("Allocating lockable region; FIXME: should use recyclable pre-mapped RDMA windows\n");
		  cdo_dst->raw_ptr = mstro_alloc_lockable(reg_len, true); /* locking immediately */
		  fresh_alloc_route=1;
		  cdo_dst->core_owned_raw_ptr = true;
		  assert (cdo_dst->raw_ptr!=NULL);
	  }
	   /* register this address for OFI */
	  DEBUG("Doing memory registration\n");
    status = mstro_ofi__maybe_register_local_buffer(
                FI_HMEM_SYSTEM, /*iface system memory layer  TODO check cdo memory layer here and pass the correct type
		            types are https://ofiwg.github.io/libfabric/v1.15.0/man/fi_mr.3.html
		            FI_HMEM_SYSTEM
		            FI_HMEM_CUDA
		            FI_HMEM_ROCR
		            FI_HMEM_ZE
		            FI_HMEM_NEURON*/
	              0, /*device TODO pass device id here, e.g. get_cuda_device*/	
		            app_entry->ep->fi->domain_attr, app_entry->ep->ep,
                app_entry->ep->domain, (uint8_t*) cdo_dst->raw_ptr,reg_len,
                &mr, &local_buf_mr_desc);
    if(status != MSTRO_OK) {
	    ERR("Failed to register or bind cdo memory for RDMA transport\n");
	    if (mr != NULL) 
	    {
		    int ret = fi_close((struct fid *)mr);
		    if(ret<0) {
			    ERR("Failed to close dst cdo mreg: %d (%s)\n", ret, strerror(-ret));
		    }
	    }
      UNLOCK_RDMA_PENDING_TABLE;
	    return status;
    }

	  /* make a fresh entry (for completion) */
	  regentry = calloc(1, sizeof(struct mstro_transport_mreg_table_entry)); 
	  if(regentry==NULL) {
		  ERR("Cannot allocate new transport completion entry\n");
		  UNLOCK_RDMA_PENDING_TABLE;
		  return MSTRO_NOMEM;
	  }
	  memset(&regentry->key, 0, sizeof(struct mstro_transport_mreg_table_key));

	  regentry->key.id = cdo_dst->gid;
	  regentry->key.domain = app_entry->ep->domain;
	  regentry->key.ep = app_entry->ep->fi->domain_attr->mr_mode & FI_MR_ENDPOINT ? app_entry->ep->ep : NULL;
	  regentry->refcount=1;
    regentry->addr= cdo_dst->raw_ptr; 
	  regentry->len= reg_len;
	  regentry->mr_reg = mr;

	
	  // entry on which we'll wait on for completion
	  HASH_ADD(hh, g_mstro_transport_rdma_pending_table,
			  key, sizeof(struct mstro_transport_mreg_table_key), regentry);

  }
  else{
    assert(regentry != NULL);
	  DEBUG("CDO memory already registered for RDMA transfer on destination - increasing refcount\n");
	  regentry->refcount++;
	  mr = regentry->mr_reg;
    /* mr might be NULL, indicating that fabric provider does not require registering host local buffers*/
    if(mr != NULL)
    {
      local_buf_mr_desc = fi_mr_desc(mr);
    }
    else
    {
      local_buf_mr_desc = NULL;
    }
  } 

  UNLOCK_RDMA_PENDING_TABLE;

  DEBUG("Doing closure creation\n");
  closure = malloc(sizeof(struct mstro_transport_rdma_cb_args));
  if(closure==NULL) {
    ERR("Failed to alloc RDMA transport closure\n");
    return MSTRO_NOMEM;
  }
  closure->num_fragments = 1;
  closure->mr = mr;
  closure->fresh_alloc_route = fresh_alloc_route;
  closure->raw_ptr = cdo_dst->raw_ptr;
  closure->len = regentry->len; /*registration length*/
  closure->name = strdup(cdo_dst->name);
  if( closure->name == NULL) {
	  ERR("Failed to allocate RDMA transport closure\n");
	  free(closure);
	  return MSTRO_NOMEM;
  }
  closure->srccdoid.qw[0]    = ticket->srccdoid->qw0;
  closure->srccdoid.qw[1]    = ticket->srccdoid->qw1;
  closure->srccdoid.local_id = ticket->srccdoid->local_id;
  closure->dstcdoid = cdo_dst->gid;
  closure->appid = ticket->srcid->id;

  
  transfer_len = ticket->data_size; /*correct transfer size */
  size_t fragment_size = app_entry->ep->fi->ep_attr->max_msg_size;

  /* mostly for debugging purposes to select an random small fragment size */
  //#define OVERRIDE_MAX_MSG_SIZE 1
#ifdef OVERRIDE_MAX_MSG_SIZE
  fragment_size = MIN(fragment_size, OVERRIDE_MAX_MSG_SIZE);
#endif

  closure->num_fragments = 1 + transfer_len / fragment_size;

  if(closure->num_fragments>1) {
    DEBUG("Message too large for endpoint, splitting into %zu fragments\n",
          closure->num_fragments);
  }

  /* read full-fragment blocks */
  size_t offset=0; 
  size_t num_retries;
  for(size_t i=closure->num_fragments;
      i-->1; ) {
    status = mstro_transport_rdma__read(app_entry, cdo_dst,
                                        local_buf_mr_desc, mr_addr, mr_key,
                                        offset, fragment_size, ticket->src_offset, ticket->dst_offset, 
                                        closure); //add dst_offset for dist_cdos
    if(status!=MSTRO_OK) {
      ERR("Failed to read fragment %zu\n", closure->num_fragments - i);
      return status;
    }
    offset+=fragment_size;
  }
  
  /* final (partial) read */
  status = mstro_transport_rdma__read(app_entry, cdo_dst,
                                      local_buf_mr_desc, mr_addr, mr_key,
                                      offset, transfer_len-offset, ticket->src_offset, ticket->dst_offset,
                                      closure); 
  if(status!=MSTRO_OK) {
    ERR("Failed to read last fragment\n");
    return status;
  }

  return status;
}

/** callback triggered when RDMA completes on receiving side of CDO transfer */ 
void
mstro_transport_rdma_cb(mstro_event ev, void* closure)
{
  mstro_event foo=ev; // avoid unused warning
  struct mstro_transport_rdma_cb_args* args = closure;
  WITH_CDO_ID_STR(
      srcidstr, &(args->srccdoid),
      WITH_CDO_ID_STR(
          dstidstr, &(args->dstcdoid),
          DEBUG("Callback is finishing completion msg of CDO `%s`-> `%s`, %zu fragments left\n",
                srcidstr, dstidstr, args->num_fragments);););

  mstro_status status;
  args->status = MSTRO_OK;

  size_t num_fragments_left = atomic_fetch_sub(&(args->num_fragments), 1);
  assert(num_fragments_left>0);
  if(num_fragments_left>1) {
    DEBUG("%zu more fragments in flight, delaying rdma cleanup\n",
          num_fragments_left-1);
    /* closure is shared across all contexts/events, so don't free it */
    args->status=MSTRO_OK;
    return;
  }

  /*check the refcount of the mreg table */
  struct mstro_transport_mreg_table_entry* regentry=NULL;

  struct mstro_pm_app_registry_entry *e=NULL;
  status = mstro_pm_app_lookup(args->appid, &e);
  if(e==NULL) {
    ERR("Target app %" PRIappid " not in app table\n", args->appid);
    args->status=MSTRO_FAIL;
    return;
  }


  int ret=pthread_mutex_lock(&g_mstro_transport_rdma_pending_table_mtx);
  if(ret!=0) {
    ERR("Failed to lock pending RDMA op table\n");
    args->status=MSTRO_FAIL;
    return;
  }

  status = mstro_transport_rdma__mreg_entry_find__locked(e->ep,&(args->dstcdoid), g_mstro_transport_rdma_pending_table, &regentry);
  if (!regentry) {
	  ERR("RDMA transfer completion message does not map to a documented transfer on destination\n");
	  args->status=MSTRO_FAIL;
	  ret=pthread_mutex_unlock(&g_mstro_transport_rdma_pending_table_mtx);
	  if(ret!=0) {
		  ERR("Failed to unlock pending RDMA op table\n");
	  }
	  return;
  }

  regentry->refcount--;
  DEBUG ("A client is done using memory region (start: %p) used for RDMA\n", regentry->addr);

  if (regentry->refcount <= 0) { /*unregister */
    /* Check if it was registered at the first place */
    if (args->mr != NULL)
    {
	    int err = fi_close((struct fid*)args->mr);
	    if (err) {
		    ERR("Couldn't unregister memory region for RDMA transport (err: %d)\n", err);
		    args->status = MSTRO_FAIL;
	    }
    }
    else {
      DEBUG("Local memory register was not registered for RDMA (not required) \n");
    }
	  // unlock
	  DEBUG("fresh_alloc_route = %d\n", args->fresh_alloc_route);
	  if (!args->fresh_alloc_route) {
		  status = mstro_memunlock(args->raw_ptr, args->len);
		  if (status != MSTRO_OK) {
			  ERR("Couldn't unlock raw-ptr for RDMA transport of CDO `%s`\n", args->name);
			  args->status = status;
		  }
	  } 
	  else
	  {
		  mstro_unlock_lockable (args->raw_ptr, args->len);
	  }
	  /*delete entry */
	  HASH_DEL(g_mstro_transport_rdma_pending_table, regentry);
	  free(regentry);
  }
  else{
	  /*keep memory locked*/
  }
  
  ret=pthread_mutex_unlock(&g_mstro_transport_rdma_pending_table_mtx);
  if(ret!=0) {
    ERR("Failed to unlock pending RDMA op table\n");
    args->status=MSTRO_FAIL;
    return;
  }

  // send completion to PM and sender
  WITH_CDO_ID_STR(
      idstr, &(args->srccdoid),
      DEBUG("Sending transfer completion msg of CDO `%s` to PM and sender\n",
           idstr););
  status = mstro_pc__transport_send_completion(args->appid,
                                               &args->srccdoid,
                                               &args->dstcdoid,
                                               1); // RDMA src always wants completion
  if (status != MSTRO_OK) {
    ERR("Couldn't send completion for RDMA transport of CDO `%s`\n", args->name);
    args->status = status;
  }


  // mark readiness for (the transport thread and then) the local pool
  // there is only one waiter (the transport thread) for one CDO transfer completion


  /* indicate that CDO is filled */
  mstro_cdo cdo;
  mstro_cdo__find_cdo(&(args->dstcdoid), &cdo);
  args->status = mstro_cdo__mark_transfer_complete(cdo);
  if(args->status!=MSTRO_OK) {
    ERR("Failed to indicate completion of incoming CDO: %d (%s)\n",
        args->status, mstro_status_description(args->status));
    return;
  }
  args->status=MSTRO_OK;

  DEBUG("RDMA transport for CDO `%s` successful\n", args->name);

}

/** cleanup function for RDMA callback arguments of completion events */
void
mstro_transport_rdma_dtor(void* closure)
{
  struct mstro_transport_rdma_cb_args* args = closure;

  
  //struct mstro_pm_app_registry_entry *e;
  /* closure is re-used for multiple events if we fragment, so
       * clean up only if appropriate: */
   /**cleanup*/ 
  if(args->num_fragments >0) {
	 DEBUG("%zu more fragments in flight, delaying transport closure cleanup\n",
          args->num_fragments);
    return;
  }
  else 
  {
    free(args->name);
    free(args);
  }
}


mstro_status
mstro_transport_cleanup_or_wait__rdma(mstro_cdo cdo)
{
  if (cdo == NULL)
    return MSTRO_INVARG;

  mstro_status status = MSTRO_OK;
  int err = pthread_mutex_lock(&g_mstro_transport_mreg_table_lock);
  if (err) {
    ERR("Couldn't lock mutex on mreg table: %d (%s)\n",
        err, strerror(err));
    return MSTRO_FAIL;
  }
  
  struct mstro_transport_mreg_table_entry* regentry, *tmp;
  regentry = NULL;
  const void *needle = cdo->raw_ptr;
  /* FIXME: since the mreg table is not searchable by ADDR (and we
   * don't know domain and EP on this code path) we can't HASH_FIND it
   * here but need to do a linear search. */
RETRY:
  HASH_ITER(hh, g_mstro_transport_mreg_table, regentry, tmp) {
    if (needle == regentry->addr) {
      while (regentry->refcount > 0) {
        err = pthread_cond_wait(&g_mstro_transport_mreg_table_cond,
                                &g_mstro_transport_mreg_table_lock);
        if (err) {
          ERR("Couldn't wait for mreg table refcount decrement: %d (%s)\n",
              err, strerror(err));
          status = MSTRO_FAIL;
        }
        /* when we return we hold the lock, but we cannot rely on
         * regentry still being valid -- if could have been killed by
         * whoever brought the refcount to 0. Thus we have to look for
         * it before retrying */
        goto RETRY;
      }
      break;
    }
  }

  err = pthread_mutex_unlock(&g_mstro_transport_mreg_table_lock);
  if (err) {
    ERR("Couldn't unlock mutex on mreg table: %d (%s)\n",
        err, strerror(err));
    status=MSTRO_FAIL;
  }
  
  return status;
}



mstro_status
mstro_transport_rdma_finalize(void)
{
  mstro_status s;
  if(g_mstro_transport_mreg_table==NULL) {
    return MSTRO_OK;
  }

  mstro_cleanup_waitup(s,
                       g_mstro_transport_mreg_table,
                       &g_mstro_transport_mreg_table_lock,
                       &g_mstro_transport_mreg_table_cond,
                       "RDMA transport memory registration");
  if(s!=MSTRO_OK) {
    struct mstro_transport_mreg_table_entry *entry, *tmp;
    HASH_ITER(hh, g_mstro_transport_mreg_table, entry, tmp) {
      WITH_CDO_ID_STR(idstr, &entry->key.id, {
		      WARN("Still have mreg entry %p: CDO %s\n", entry, idstr);
                      });
    }
  }
  /* may leak, but let us continue */
  g_mstro_transport_mreg_table = NULL;
  return s;
}
