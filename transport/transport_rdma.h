/* -*- mode:c -*- */
/** @file
 ** @brief OFI RDMA transport interface
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_TRANSPORT_RDMA_H_
#define MAESTRO_TRANSPORT_RDMA_H_ 1

#include "maestro/cdo.h"
#include "maestro/status.h"
#include "mstro_pool.pb-c.h"


/* Simple hash to keep track of pending dst rdma transports, linking the
 * completion callback to the main code track that must wait for proper
 * completion from OFI CQ before changing CDO state and notifying the pool
 * entry */
extern struct mstro_transport_mreg_table_entry *g_mstro_transport_rdma_pending_table;
extern pthread_mutex_t g_mstro_transport_rdma_pending_table_mtx;

extern struct mstro_transport_mreg_table_entry *g_mstro_transport_mreg_table;
extern pthread_mutex_t g_mstro_transport_mreg_table_lock;
extern pthread_cond_t g_mstro_transport_mreg_table_cond;

extern mstro_event_domain g_transport_rdma_edom;
#define RDMA_EDOM_NAME "RDMA transport completion handling (as dst side)"
extern const char* mstro_transport_rdma_edom_name;

#include "maestro/i_ofi.h"

struct mstro_transport_rdma_cb_args {
  struct fid_mr *mr;
  int fresh_alloc_route;
  void* raw_ptr;
  size_t len;
  char* name;
  _Atomic(size_t) num_fragments; /**< the number of operations in flight (if transfer had to be chunked) */
  struct mstro_cdo_id srccdoid;
  struct mstro_cdo_id dstcdoid;
  mstro_app_id appid;
  mstro_status status;
};

mstro_status  
mstro_transport_rdma_src_execute(mstro_cdo src, Mstro__Pool__TransferTicket* ticket);
mstro_status
mstro_transport_rdma_src_execute_bh(Mstro__Pool__TransferCompleted *tc);

mstro_status
mstro_transport_rdma_dst_execute(mstro_cdo dst, Mstro__Pool__TransferTicket* ticket);

/** @brief callback for rdma transport completion 
 **/
void
mstro_transport_rdma_cb(mstro_event ev, void* closure);

/** @brief event closure deallocator
 **/
void
mstro_transport_rdma_dtor(void* closure);

/** @brief ensure RDMA window(s) for @arg cdo are closed */
mstro_status
mstro_transport_cleanup_or_wait__rdma(mstro_cdo cdo);

/**@brief Finalize RDMA transport layer */
mstro_status
mstro_transport_rdma_finalize(void);

#endif /* MAESTRO_TRANSPORT_RDMA_H_ */
