/* -*- mode:c -*- */
/** @file
 ** @brief Inline Transport implementation
 **/
/*
 * Copyright (C) 2023 Hewlett Package Development LP
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/core.h"
#include "maestro/logging.h"
#include "transport.h"
#include <inttypes.h>
#include "attributes/maestro-schema.h"

#include <unistd.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)



mstro_status
mstro_transport_inline_src_execute(mstro_cdo src,
                                   Mstro__Pool__TransferTicket* ticket)
{
  INFO("Executing inline transport src side for CDO %s\n", src->name);
  if (src == NULL || ticket == NULL) {
    ERR("mstro_transport_inle_src_execute() called with invalid arguments\n");
    return MSTRO_INVARG;
  }
  
  mstro_status s;
  struct mstro_transport_datalen dl;
  s = mstro_transport__src_datalen_get(src, ticket, &dl);
  if (s != MSTRO_OK) {
    ERR("Couldn't get CDO data and size for inline transport (status: %s)\n",
        mstro_status_description(s));
    return MSTRO_FAIL;
  }

  /* FIXME: use mamba array-to-1d-View process */
  ticket->inline_->data.data = (uint8_t *)dl.data + ticket->src_offset; // need to only pack slice
  ticket->inline_->data.len = ticket->data_size;             // ....

  DEBUG("CDO data at %p of %zu bytes inserted inline in ticket\n", dl.data,
        dl.len);

  return MSTRO_OK;
}

mstro_status
mstro_transport_inline_dst_execute(mstro_cdo dst,
                                   Mstro__Pool__TransferTicket* ticket)
{
  if (dst == NULL
      || ticket == NULL
      || ticket->ticket_case!=MSTRO__POOL__TRANSFER_TICKET__TICKET_INLINE) {
    ERR("mstro_transport_inline_dst_execute() called with invalid arguments (%p/%p/%d)\n",
        dst, ticket, ticket? (int)ticket->ticket_case : -1);
    return MSTRO_INVARG;
  }
  INFO("Executing inline transport dst side for CDO %s\n", dst->name);
  
  mstro_status status;
  size_t len = (size_t)ticket->data_size;
  char* data;

  DEBUG("Incoming CDO, size %zu\n", len);

  status = mstro_transport_get_dst_buffer(dst, (void*)&data);
  if(status!=MSTRO_OK) {
      ERR("Failed to allocate space in CDO at transport time\n");
      return MSTRO_FAIL;
  }
  assert(data!=NULL);

  /* data is the raw-ptr or freshly allocated, CDO attribute is correct */
  if(ticket->inline_->data.len!=len) {
    ERR("Inline data length (%zu) differs from CDO size in the ticket (%zu)\n",
        ticket->inline_->data.len, len);
    return MSTRO_FAIL;
  }
  memcpy(data+ticket->dst_offset,
         ticket->inline_->data.data,
         sizeof(char)*len);
  
  return MSTRO_OK;
}

