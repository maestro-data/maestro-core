/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Transport implementation API
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MAESTRO_TRANSPORT_H_
#define MAESTRO_TRANSPORT_H_ 1

/**@addtogroup MSTRO_Core
 **@{
 **/

/**@defgroup MSTRO_Transport Transport layer
 **@{
 ** This is the Transport API, as developed for D5.{3,5}
 **
 **
 **/


#include "maestro/status.h"
#include "maestro/i_cdo.h"
#include "maestro/i_event.h"
#include "../protocols/mstro_pool.pb-c.h"

/* FIXME more rigorous choice of path (can also come from src, src-side maestro core, or PM requirement) */
#define MSTRO_TRANSPORT_GFS_PATH	"./CDOs/"
#define DEBUG_PATH					"./CDOs/test"
#define MSTRO_MAX_PATH_LEN			256
#define MSTRO_MIO_SEM_STR_MAXLEN 	128

struct
mstro_transport_descriptor {
  /** flag  whether  to  notify  source  application  of  completed  transfer  */
  bool notify_src;
  /** the  function  to  call  on  the  source  side  of  the  transfer  */
  mstro_status  (* src_func)(mstro_cdo src, Mstro__Pool__TransferTicket* ticket);
  /** the  function  to  call  on  the  destination  side  of  the  transfer  */
  mstro_status  (* dst_func)(mstro_cdo dst , Mstro__Pool__TransferTicket* ticket);
  char name [16];
};

extern const struct mstro_transport_descriptor g_transport_registry[];

struct mstro_transport_datalen{
	void* data;
	size_t len;
};


/** @brief retrieve data pointer and data size before transfer */
mstro_status
mstro_transport__src_datalen_get(mstro_cdo src,
                                 const Mstro__Pool__TransferTicket* ticket,
                                 struct mstro_transport_datalen* dl);

/** @brief retrieve data pointer to receive transfer */
mstro_status
mstro_transport_get_dst_buffer(mstro_cdo dst, void** data);

/** @brief initialize individual transports */
mstro_status
mstro_transport_init(void);

/** @brief finalize individual transports */
mstro_status
mstro_transport_finalize(void);

/** @brief ensure clean state before relinquishing CDO */
mstro_status
mstro_transport_cleanup_or_wait(mstro_cdo cdo);

/** @brief execute the transport operation specified in the ticket
 **
 ** Executing a transport operation is triggered by invoking the
 ** following function with the process - local CDO handle and a
 ** transport ticket. The implementation will be able to recognize
 ** whether it is on the sending or receiving end of the transfer.
 **
 ** The function sets up the transport operation, and returns as soon
 ** as the transport can proceed (in the background). In particular,
 ** it does not block in a rendezvous situation, waiting for the otehr
 ** side to start executing the matching transport function.
 **/
mstro_status
mstro_transport_execute(
    mstro_cdo  cdo,
    Mstro__Pool__TransferTicket* ticket, 
    int64_t first);

/** A  transport  implementation  needs  to  provide  a source - and a
**  destination -side  function  to  perform  the  data  transfer; NULL
**  indicates  no  operation  is  needed  (e.g., when  transfer  is a
** one -sided  action), and  fill  in a  dispatch  table  with  its  specific
**  functions.
**/


/* TODO check on dst_execute functions that cdo_dst attributes match the ticket
 * attributes, for instance to return an error/warning if a consumer specifies a
 * size which is different than the one given by the producer */

/**@} (end of group MSTRO_Transport) */
/**@} (end of addtogroup MSTRO_Core) */

#endif
