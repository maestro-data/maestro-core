/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Transport implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro/logging.h"
#include "maestro/env.h"
#include "transport.h"
#include "stdlib.h"
#include "stdio.h"
#include "time.h"
#include "maestro/i_globals.h"
#include "maestro/i_cdo.h"
#include "maestro/i_memlock.h"
#include "maestro/i_misc.h"
#include "maestro/i_event.h" // waitup
#include <inttypes.h>

#include <sys/stat.h>

#include "transport/transport_gfs.h"
#include "transport/transport_rdma.h"
#include "transport/transport_inline.h"

#ifdef HAVE_MIO
#include "transport/transport_mio.h"
#include "mio.h"
#endif

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)


#define MSTRO_TRANSPORT_DEBUG_PATH	"./CDOs/test"

/** The transport registry */
const struct mstro_transport_descriptor
g_transport_registry[] = {
  [MSTRO__POOL__TRANSPORT_KIND__INVALID_TRANSPORT_KIND] = {
    .notify_src = false, .src_func = NULL, .dst_func = NULL, .name = "INVALID"
  },
  [MSTRO__POOL__TRANSPORT_KIND__GFS] = {
    .notify_src = false,
    .src_func = mstro_transport_gfs_src_execute,
    .dst_func = mstro_transport_gfs_dst_execute,
    .name = "GFS"
  },
  [MSTRO__POOL__TRANSPORT_KIND__UDJ] = {
    .notify_src = false,
    .src_func = NULL,
    .dst_func = NULL,
    .name = "UDJ"
  },
  [MSTRO__POOL__TRANSPORT_KIND__MIO] = {
    .notify_src = false,
#ifdef HAVE_MIO
    .src_func = mstro_transport_mio_src_execute,
    .dst_func = mstro_transport_mio_dst_execute,
#else /* no MIO */
    .src_func = NULL,
    .dst_func = NULL,
#endif
    .name = "MIO"
  },
  [MSTRO__POOL__TRANSPORT_KIND__OFI] = {
    .notify_src = true,
    .src_func = mstro_transport_rdma_src_execute,
    .dst_func = mstro_transport_rdma_dst_execute,
    .name = "RDMA"
  },
  [MSTRO__POOL__TRANSPORT_KIND__INLINE] = {
    .notify_src = true,
    .src_func = mstro_transport_inline_src_execute,
    .dst_func = mstro_transport_inline_dst_execute,
    .name = "INLINE"
  }

};

mstro_status
mstro_transport_get_dst_buffer(mstro_cdo dst, void** ptr)
{
  if (dst == NULL && ptr != NULL)
    return MSTRO_INVARG;

  mstro_status status;

  if (dst->mamba_array == NULL) {
    DEBUG ("No mamba array present, let's make one\n");
    status = mstro_cdo_allocate_data(dst);
    if(status!=MSTRO_OK) {
	  // already printed an error
      return MSTRO_FAIL;
    }
  }
  *ptr = dst->mamba_array->allocation->ptr;

  return MSTRO_OK;
}

mstro_status
mstro_transport__src_datalen_get(mstro_cdo src,
                                 const Mstro__Pool__TransferTicket* ticket,
                                 struct mstro_transport_datalen* dl)
{
  enum mstro_cdo_attr_value_type type;
  const int64_t* val;
  bool is_distributed = ticket->distributed_cdo;

  const void *sizep=NULL;
  int64_t src_size;
  mstro_status s = mstro_attribute_dict_get(src->attributes,
                                            MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                            &type, &sizep, NULL, false);
  if(s==MSTRO_OK) {
    src_size=*(int64_t*)sizep;
  } else if(s==MSTRO_NOENT && type==MSTRO_CDO_ATTR_VALUE_INVALID) {
    src_size=-1;
  } else {
    ERR("Failed to look up %s (%d: %s)\n",
        MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, s, mstro_status_description(s));
    return MSTRO_FAIL;
  }

  if( (src->mamba_array!=NULL)) { 
    /* mamba array wins */
    mmbArray* ma = src->mamba_array;
    dl->data = (char*)ma->allocation->ptr;
    dl->len = ma->allocation->n_bytes;
    DEBUG("CDO size is %zu (from mamba, attributes say %" PRIi64 ", ticket %" PRIi64"), mamba array at %p\n",
          dl->len, src_size, ticket->data_size, ma);
    }   
    else if(src->raw_ptr != NULL) {
    assert(src_size==ticket->data_size);
    dl->len = ticket->data_size;
    dl->data = src->raw_ptr;
    DEBUG("CDO size is %zu, raw data at %p\n",
          dl->len, dl->data);
    } else {
    /* both NULL */
    assert(ticket->data_size==0);
    dl->len = ticket->data_size;
    dl->data = src->raw_ptr;
    DEBUG("CDO size is %zu (from ticket, attributes say %" PRIi64 "), raw data at %p\n",
          dl->len, src_size, dl->data);
    }

  NOISE("CDO size: %zu\n", dl->len);

  return MSTRO_OK;
}

mstro_event_domain g_transport_rdma_edom = NULL;

const char* mstro_transport_rdma_edom_name = RDMA_EDOM_NAME;

mstro_status
mstro_transport_init(void)
{
  srand(time(NULL)); /* We'll need that to generate random IDs */

  mstro_status status = mstro_event_domain_create(RDMA_EDOM_NAME,
                                   &g_transport_rdma_edom);
  if(status!=MSTRO_OK) {
    ERR("Failed to create RDMA transport completion domain\n");
    return status;
  }

  /* ensure GFS dir is set up */
  mstro_status s = mkdirhier(g_mstro_transport_gfs_dir);
  if(s!=MSTRO_OK) {
    ERR("Failed to initialize GFS transport. Check your setting of %s\n",
        MSTRO_ENV_TRANSPORT_GFS_DIR);
    return s;
  }

#ifdef HAVE_MIO
  /* MIO */
  if(g_mio_available) {
    DEBUG("MIO already initialized\n");
  } else {
    const char* env_mio_config = getenv(MSTRO_ENV_MIO_CONFIG);
    if (env_mio_config == NULL
        || (strlen(env_mio_config) < 1) ) {
      ERR("MIO available, but no configuration file in %s, DISABLING MIO\n",
          MSTRO_ENV_MIO_CONFIG);
    } else {
      DEBUG("Initializing MIO with configuration file %s\n", env_mio_config);
      int status = mio_init(env_mio_config);
      if (status != 0) {
        ERR("Failed to initialize MIO (errno: %d\n)", status);
        return MSTRO_FAIL;
      }
      /* since we can't do that at the right time, at least put it into the termination sequence */
      atexit(mio_fini);
      g_mio_available = true;
    }
  }
#endif

BAILOUT:
  return status;
}

mstro_status
mstro_transport_cleanup_or_wait(mstro_cdo cdo)
{
  if (cdo == NULL)
    return MSTRO_INVARG;
  mstro_status status = MSTRO_OK;
  
  status = mstro_transport_cleanup_or_wait__rdma(cdo);

  return status;
}


mstro_status
mstro_transport_finalize(void)
{
  mstro_status s;

  /* check if there is no pending transport (src side)
     dst (mstro event route) usually is absorbed by demands, but cq
     notification might come late. Could also be if transport is PM initiative
  */

  s=mstro_transport_rdma_finalize();
  if (s != MSTRO_OK)
    return s;

  s = mstro_event_domain_destroy(g_transport_rdma_edom);
  // may leak on error, but allows us to do a clean re-init later
  g_transport_rdma_edom = NULL; 
  if(s!=MSTRO_OK) {
    ERR("Failed to destroy RDMA transport completion domain\n");
    return s;
  }

#ifdef HAVE_MIO
  if(g_mio_available) {
    DEBUG("Skipping MIO finalize -- otherwise we could not-reinit maestro\n");
    // FIXME: This really needs to be fixed in MIO
    //mio_fini(); /* returns void */
    //g_mio_enabled = false;
  }
#endif
  return MSTRO_OK;
}

mstro_status
mstro_transport_execute(
    mstro_cdo  cdo,
    Mstro__Pool__TransferTicket* ticket, 
    int64_t first) //marking this is the first ticket for this transfer ... for M:1 dist cdos
{
  if(cdo == NULL || ticket == NULL) {
    ERR("mstro_transport_execute() called with invalid arguments");
    return MSTRO_INVARG;
  }
  mstro_status s = MSTRO_OK;
  /* Is this a ticket for a distributed cdo? */
  bool is_distributed = ticket->distributed_cdo;
  
  if (cdo->state & MSTRO_CDO_STATE_OFFERED) {
    /* we are called on the source side of transport */
    {
      /* let's check ticket size matches local CDO size */
      enum mstro_cdo_attr_value_type type;
      const void *sizep=NULL;
      int64_t src_size;
      mstro_status s = mstro_attribute_dict_get(cdo->attributes,
	                                          MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
	                                          &type, &sizep, NULL, false);
      if(s==MSTRO_OK) {
	      src_size=*(int64_t*)sizep;
      } else if(s==MSTRO_NOENT && type==MSTRO_CDO_ATTR_VALUE_INVALID) {
	      ERR("Attribute not found or value invalid\n");
	      src_size=-1;
      } else {
	      ERR("Failed to look up %s (%d: %s)\n",
	      MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, s, mstro_status_description(s));
	      return MSTRO_FAIL;
      }
      
      if (ticket->data_size == 0) {
	      DEBUG("0-transport\n");
      } else if ((src_size != ticket->data_size) && (!is_distributed)) {
	      ERR("SRC cdo has a size (%" PRIi64 ") that doesn't match ticket-specified size (%" PRIi64 ")\n",
	      src_size, ticket->data_size);
        return MSTRO_FAIL;
      } else if ((src_size < ticket->data_size) && (is_distributed)) {
        ERR("SRC cdo size (%" PRIi64 ") is less than ticket-specified intersection length (%" PRIi64 ")\n",
	      src_size, ticket->data_size);
        return MSTRO_FAIL;
      }
    }

    mstro_status (*f)(mstro_cdo src, Mstro__Pool__TransferTicket *t)
      = g_transport_registry[ticket->method].src_func;
    if(f==NULL) {
      ERR("No SRC-side transport function for transport type %d\n",
	  ticket->method);
      return MSTRO_FAIL;
    }
    if (ticket->data_size > 0) {
      s=f(cdo, ticket);
    } else {
      DEBUG("Skipping (SRC-side) 0-length transfer\n");
    }

  } else if (cdo->state
      & (MSTRO_CDO_STATE_DEMANDED|MSTRO_CDO_STATE_REQUIRED)) {
    /* we are called on the destination side of transport */

    /* FIXME: we should have a flag in the transport registry
     * indicating whether the transport can/wants to deal with the DST
     * CDO space allocation. Then we could skip the adjust here for
     * transports that want to be smarter than 'just a mamba array' */

    int policy = MSTRO_ALLOC_POLICY_NONE;
    if (ticket->method == MSTRO__POOL__TRANSPORT_KIND__OFI)
      policy = MSTRO_ALLOC_POLICY_LOCKED | MSTRO_ALLOC_POLICY_ALIGNED;

    /* FIXME: we should also decide the most suitable memory layer
     * here for our space adjustment */

    /* if it is a distributed cdo, then cdo is should be calculated from the distribution
     * not the size recieved in the ticket, which could be only one piece of the required data*/

    if(is_distributed) {
      /*calculate the cdo size from distribution */
      int64_t cdo_size = 0;
      mmbLayout *dst_layout = NULL;
      mmbLayout *src_layout = NULL;
      mmbError stat;
      s = mstro_attribute_pool_find_dist_layout(cdo->attributes_msg, &dst_layout);
      assert(s == MSTRO_OK);
    
      if(dst_layout == NULL) {
      /* cook a default layout for dst from src */
      s = mstro_attribute_pool_find_dist_layout(ticket->attributes, &src_layout);
      assert(s == MSTRO_OK);
      stat = mmb_layout_dist_create_default_layout(src_layout, &dst_layout);
      assert(stat == MMB_OK);
      }
      stat = mmb_layout_dist_find_piece_size(dst_layout, &cdo_size);
      assert(stat == MMB_OK);
      DEBUG("Size of my piece as calculated from my distribution layout is %"PRId64" \n", cdo_size);

      /* free up allocated layout objects*/
      if(src_layout) {
        mmb_layout_destroy(src_layout);
      }
      if(dst_layout) {
        mmb_layout_destroy(dst_layout);
      }

      s = mstro_cdo__adjust_space(cdo, cdo_size, policy);
    }
    else {
      s = mstro_cdo__adjust_space(cdo, ticket->data_size, policy);
    }
    
    if(s!=MSTRO_OK) {
      ERR("Failed to adjust space of CDO destination handle\n");
      return s;
    }

    mstro_status (*f)(mstro_cdo src, Mstro__Pool__TransferTicket *t)
        = g_transport_registry[ticket->method].dst_func;
    if(f==NULL) {
      ERR("No DST-side transport function for transport type %d\n",
	  ticket->method);
      return MSTRO_FAIL;
    }

    if(! (cdo->state & MSTRO_CDO_STATE_IN_TRANSPORT)) {
      WITH_CDO_ID_STR(idstr, &cdo->gid, {
          WARN("REQUIRED/DEMANDED CDO %p (%s, %s) incoming, but not marked IN_TRANSPORT here\n",
               cdo, cdo->name, idstr);});
    }

    /* FIXME: this assumption is likely wrong at this time. It's
     * nontrivial to fix also, because the transformation interface
     * requires SRC and DST cdo handles, and for transport we really
     * only have DST and a transport-specific alternative to SRC. That
     * means we can't reuse the existing transformation table. */
    DEBUG("Assuming underlying transport execution has performed any necessary transformations and layout attribute adjustments\n");

    /* layout-related attributes and a few more are always precious, as
     * transformations will have handled them */
    #define NUM_PRECIOUS 8
    const size_t num_precious_attr = NUM_PRECIOUS;
    const char *precious_attributes[NUM_PRECIOUS] = {
      MSTRO_ATTR_CORE_CDO_RAW_PTR,
      MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY,
      MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
      MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE,
      MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS,
      MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER,
      MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE,
      MSTRO_ATTR_CORE_CDO_DIST_LAYOUT
    };
    
    if(first == 0) //only the first ticket updates the attributes
    {
    	s = mstro_cdo_attributes_update_incoming(cdo, ticket->attributes,
		num_precious_attr,
		(const char**)precious_attributes);
    	if(s!=MSTRO_OK) {
      	WITH_CDO_ID_STR(idstr, &cdo->gid,
	  	ERR("Failed to update CDO %s attributes from incoming data: %d (%s)\n",
	   	 s, mstro_status_description(s)););
      	goto BAILOUT;
    	}
    }

    if (ticket->data_size > 0) {
      s=f(cdo, ticket);
      if(s!=MSTRO_OK) {
	      ERR("Incoming transfer failed\n");
	      goto BAILOUT;
      }
    } else {
      DEBUG("Skipping (DST-side) 0-length transfer\n");
    }
    
    // OFI has a callback to handle CDO state updates and SRC/PM completion notification:
    if ( ticket->method == MSTRO__POOL__TRANSPORT_KIND__OFI
      && ticket->data_size > 0) {
; /* nothing to do */
    } else {
      // 0-size transport or non-OFI transport need help from us:
      WITH_CDO_ID_STR(
	  idstr, &cdo->gid,
	  DEBUG("Sending transfer completion msg of CDO `%s` to PM and sender\n",
                idstr););
      struct mstro_cdo_id srccdoid = {.qw[0] = ticket->srccdoid->qw0,
                                      .qw[1] = ticket->srccdoid->qw1,
                                      .local_id = ticket->srccdoid->local_id };

      s = mstro_pc__transport_send_completion(ticket->srcid->id,
                                              &srccdoid,&cdo->gid,
                                              ticket->want_completion);
      if (s!= MSTRO_OK) {
	      ERR("Couldn't send completion for RDMA transport of CDO `%s`\n", cdo->name);
	      goto BAILOUT;
      }

      /* indicate that CDO is filled */
      s = mstro_cdo__mark_transfer_complete(cdo);
      if(s!=MSTRO_OK) {
        ERR("Failed to indicate completion of incoming CDO: %d (%s)\n",
            s, mstro_status_description(s));
	}
    }
  } else {
    ERR("Unexpected CDO state %s: %d -- unsolicted incoming transfer?\n",
	cdo->name, cdo->state);
    return MSTRO_FAIL;
  }

BAILOUT:
  return s;
}
