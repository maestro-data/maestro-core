/* -*- mode:c -*- */
/** @file
 ** @brief GFS Transport implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/core.h"
#include "maestro/logging.h"
#include "transport.h"
#include <inttypes.h>
#include "attributes/maestro-schema.h"
#include "transport_gfs.h"
#include <unistd.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)



mstro_status
mstro_transport_gfs_src_execute(mstro_cdo src, Mstro__Pool__TransferTicket* ticket)
{
	INFO("Executing gfs transport src side for CDO %s\n", src->name);
	if (src == NULL || ticket == NULL) {
		ERR("mstro_transport_gfs_src_execute() called with invalid arguments\n");
		return MSTRO_INVARG;
	}

  mstro_status s;
  struct mstro_transport_datalen dl;
  s = mstro_transport__src_datalen_get(src, ticket, &dl);
  if (s != MSTRO_OK) {
  	ERR("Couldn't get CDO data and size for gfs transport (status: %s)\n", mstro_status_description(s));
  	return MSTRO_FAIL;
  }

  return mstro_transport_gfs_write(dl.data, ticket->data_size, ticket->src_offset, ticket->gfs->path);
}

mstro_status
mstro_transport_gfs_write(const char* buf, size_t size, size_t offset, const char* path)
{
  if (buf == NULL || path == NULL) {
  	ERR("Invalid arguments\n");
  	return MSTRO_INVARG;
  }

	FILE *f = NULL;
	DEBUG("Path src app will use for transport: %s\n", path);
	if (path == NULL) {
		ERR("Ticket for GFS transport does not contain a path\n");
		return MSTRO_INVARG;
	}

  	f = fopen(path,"w");
  	if (f == NULL) {
		ERR("Failed to open %s for GFS transport (errno: %d -- %s)\n",
                    path, errno, strerror(errno));
	    return MSTRO_FAIL;
	}
       
/* add src offset to read from the correct place for dist_cdos */
void * src_ptr = (void *) ((char *) buf + offset); 
RETRY_GFS_TRANSPORT_WRITE: ;
    
    size_t bytes_written = fwrite(src_ptr, sizeof(char), size, f); 
	if (bytes_written != size) {
      if (errno == EAGAIN)
	    goto RETRY_GFS_TRANSPORT_WRITE;
	  ERR("Partial write on %s (buf: %p) for GFS transport (errno: %d -- %s)\n",
                    path, buf, errno, strerror(errno));
	  return MSTRO_FAIL;
	}

	if (fclose(f) != 0) {
		ERR("Failed to close %s for GFS transport (errno: %d -- %s)\n",
                    path, errno, strerror(errno));
		return MSTRO_FAIL;
	}

	return MSTRO_OK;
}

mstro_status
mstro_transport_gfs_dst_execute(mstro_cdo dst,
                                Mstro__Pool__TransferTicket* ticket)
{
  if (dst == NULL
      || ticket == NULL
      || ticket->ticket_case!=MSTRO__POOL__TRANSFER_TICKET__TICKET_GFS) {
    ERR("mstro_transport_gfs_dst_execute() called with invalid arguments (%p/%p/%d)\n",
        dst, ticket, ticket? (int)ticket->ticket_case : -1);
    return MSTRO_INVARG;
  }
  INFO("Executing gfs transport dst side for CDO %s\n", dst->name);
  
  mstro_status status;
  size_t len = (size_t)ticket->data_size;
  char* data;

  DEBUG("Incoming CDO, size %zu\n", len);

  status = mstro_transport_get_dst_buffer(dst, (void*)&data);
  if(status!=MSTRO_OK) {
      ERR("Failed to allocate space in CDO at transport time\n");
      return MSTRO_FAIL;
  }
  assert(data!=NULL);
  //data = dst->raw_ptr;

  /* data is the raw-ptr or freshly allocated, CDO attribute is correct */
  FILE *f = NULL;
  char* path = ticket->gfs->path;
  if (path == NULL) {
    ERR("Ticket for GFS transport does not contain a path\n");
    return MSTRO_INVARG;
  }
  DEBUG("Path dst app will use for transport: %s\n", path);
  f = fopen(path,"rb");
  if (f == NULL) {
    ERR("Failed to open %s for GFS transport: %d (%s)\n",
        path, errno, strerror(errno));
    return MSTRO_FAIL;
  }
RETRY_GFS_TRANSPORT_READ: ;
  size_t bytes_read = fread(data+ticket->dst_offset, sizeof(char), len, f);
  if (bytes_read != (size_t)len) {
    if (errno == EAGAIN)
	  goto RETRY_GFS_TRANSPORT_READ;
    ERR("Partial read (%d out of %d bytes) on %s for GFS transport (errno: %d -- %s)\n",
        bytes_read, len, path, errno, strerror(errno));
    return MSTRO_FAIL;
  }

  if (fclose(f) != 0) {
    ERR("Failed to close %s for GFS transport (errno: %d -- %s)\n",
        path, errno, strerror(errno));
    return MSTRO_FAIL;
  }
      
  /* const int64_t *v1=NULL; */
  /* const int64_t *v2=NULL; */
  /* if (!(MSTRO_OK == mstro_cdo_attribute_get( */
  /*         dst, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, NULL, &v1))) { */
  /*   ERR("Failed to get %s attribute of CDO %s for transport\n", */
  /*       MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, dst->name); */
  /*   return MSTRO_FAIL; */
  /* } */
  /* if (!(MSTRO_OK == mstro_cdo_attribute_get( */
  /*         dst, ".maestro.core.cdo.scope.local-size", NULL, &v2))) { */
  /*   ERR("Failed to get local-size attribute of CDO %s for transport\n", */
  /*       MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, dst->name); */
  /*   return MSTRO_FAIL; */
  /* } */
  /* if(v1!=v2) { */
  /*   ERR("different key string pointers yield different value pointers\n"); */
  /*   abort(); */
  /* } */
  /* if(*v1!=*v2) { */
  /*   ERR("different key string pointers yield different values\n"); */
  /*   abort(); */
  /* }     */
  
  DEBUG("dst app successfully executed transport CDO %s\n", dst->name);

  if (! (ticket->gfs->keep_file)) {
    int s = unlink(ticket->gfs->path);
    if(s!=0) {
      WARN("Couldn't unlink file used for gfs transfer (%s): %d (%s)\n",
           ticket->gfs->path, s, strerror(s));
    } else {
      DEBUG("Removed the transfer file\n");
    }
  }

  return MSTRO_OK;
}

