/* -*- mode:c -*- */
/** @file
 ** @brief MIO Transport implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro/logging.h"

#include "transport.h"
#include "transport_mio.h"

#include "attributes/maestro-schema.h"

#include <mio.h> /* the MIO one */

#include <limits.h>
#include <inttypes.h>
#include <sched.h> 
#include <unistd.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_TRANSP,__VA_ARGS__)



#define MSTRO_MIO_PAGESIZE		4096

struct obj_io_cb_args {
	struct mio_obj_id* semid;
	struct mio_obj* obj;
};

inline
int64_t
mstro_mio_roundup_size(int64_t raw)
{
  assert(raw>=0);
  size_t remainder = raw % MSTRO_MIO_PAGESIZE;
  if (remainder == 0)
    return raw;
  size_t missing = MSTRO_MIO_PAGESIZE - remainder;
  int64_t ret = raw + missing;
  
  /* XXX MIO reads and writes multiples of page size, therefore `missing`
   * bytes of junk will be read/written */
  
  return ret;
}

int mio_cmd_wait_on_op(struct mio_op *op)
{
	struct mio_pollop pop;

	memset(&pop, 0, sizeof pop);
	pop.mp_op = op;
	mio_op_poll(&pop, 1, MIO_TIME_NEVER);
	return op->mop_rc;
}


int obj_open(struct mio_obj_id *oid, struct mio_obj *obj)
{
  int rc;
  struct mio_op op;

  memcpy(obj->mo_id.moi_bytes,
         oid->moi_bytes, MIO_OBJ_ID_LEN);

  memset(&op, 0, sizeof op);

  DEBUG("mio_obj_open...\n");
  rc = mio_obj_open(oid, obj, &op);
  if (rc != 0)
    return rc;

  DEBUG("mio_obj_open worked, wait on op...\n");

  /* If the object doesn't exist, -ENOENT will be returned? */
  rc = mio_cmd_wait_on_op(&op);
  
  return rc;
}

int obj_open_busywait(struct mio_obj_id *oid, struct mio_obj *obj)
{
  int rc = 0;
  struct mio_op op;
  
  memcpy(obj->mo_id.moi_bytes,
         oid->moi_bytes, MIO_OBJ_ID_LEN);
  
  do {
    memset(&op, 0, sizeof op);
    rc = mio_obj_open(oid, obj, &op);
    DEBUG("rc after open: %d\n", rc);
    if (rc != 0 && rc != -ENOENT)
      return rc;
	
    rc = mio_cmd_wait_on_op(&op);
    DEBUG("rc after wait: %d\n", rc);
    if (rc != -ENOENT) {
      goto BAILOUT;
    } else {
      DEBUG("Yielding, waiting for obj to appear\n");
      sleep(5);
      //			sched_yield();
    }
  } while (rc == -ENOENT);
BAILOUT:
  return rc;
}

int obj_create(struct mio_obj_id *oid, struct mio_obj *obj)
{
	int rc;
	struct mio_op op;
WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)(oid),
			INFO("C Sanity check object ID from ticket: %s\n",
                	        idstr););

	memset(&op, 0, sizeof op);
DEBUG("Trying to open...\n");
	rc = obj_open(oid, obj);
DEBUG("Open returned, let's check for errors...\n");
	if (rc == 0) {
		fprintf(stderr, "Object exists!\n");
		return -EEXIST;
	} else if (rc == -ENOENT)
		goto create;
	else
		return rc;
DEBUG("Let's create...\n");

create:
	memset(&op, 0, sizeof op);
	rc = mio_obj_create(oid, NULL, obj, &op);
DEBUG("Create returned, let's check for errors...\n");

	if (rc != 0)
		return rc;
DEBUG("Waiting on op...\n");
	rc = mio_cmd_wait_on_op(&op);
DEBUG("Done...\n");
	return rc;
}

mstro_status
mstro_mio_obj_unlink(struct mio_obj_id *oid)
{
	int rc;
	struct mio_op op;

	memset(&op, 0, sizeof op);
	mio_op_init(&op);

	 WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
		DEBUG("MIO tries to unlink obj %s...\n",
                        idstr););

	rc = mio_obj_delete(oid, &op);
	if (rc != 0)	
		goto BAILOUT;
		
	rc = mio_cmd_wait_on_op(&op); 
	mio_op_fini(&op);
BAILOUT:
	if (rc != 0) {
	  WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
		WARN("MIO failed to remove obj %s for transfer with error code %d\n",
                        idstr, rc););
	  return MSTRO_FAIL;
	}
	return MSTRO_OK;
}

int obj_sync(struct mio_obj *obj)
{
	int rc;
	struct mio_op op;

	memset(&op, 0, sizeof op);
	rc = mio_obj_sync(obj, &op);
	if (rc != 0)
		return rc;

	rc = mio_cmd_wait_on_op(&op);
	return rc;
}

int obj_write_sync(struct mio_obj *obj, uint32_t num_iov, struct mio_iovec *iov)
{
	int rc;
	struct mio_op op;

	memset(&op, 0, sizeof op);
	rc = mio_obj_writev(obj, iov, num_iov, &op);
	if (rc != 0)
		return rc;

	rc = mio_cmd_wait_on_op(&op);
	return rc;
}

void obj_create_fail_cb(struct mio_op* op)
{
	INFO("\033[1;34m Callback level 2: failed to create semaphore \033[0m\n");
}

void obj_create_success_cb(struct mio_op* op)
{
	INFO("\033[1;34m Callback level 2: semaphore ready. Safe to read. \033[0m\n");
}

void obj_write_fail_cb(struct mio_op *op)
{
	INFO("\033[1;34m Async write fail callback called \033[0m\n");

	int i;
	struct obj_io_cb_args *args;
	args = (struct obj_io_cb_args *)op->mop_app_cbs.moc_cb_data;
	
	WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)(args->semid),
		ERR("callback says MIO failed to write obj %s for transfer with error code %d\n",
                        idstr, op->mop_rc););

	/* XXX MIO does not allow user to fini/free op */ 
}

/* for the semaphore */
int obj_create_async(struct mio_obj_id *semid, struct mio_obj *sobj)
{
	int rc;
//	struct mio_op op;
	struct obj_io_cb_args *args;

INFO ("Not checking semaphore preexistence\n");	

	struct mio_op* sop;

	args = malloc(sizeof *args);
	if (args == NULL)
		return -ENOMEM;

	sop = mio_op_alloc_init();
	if (sop == NULL) {
		free(args);
		return -ENOMEM;
	}

	args->semid = semid;
	mio_op_callbacks_set(	sop, 
				obj_create_success_cb,
			       	obj_create_fail_cb,  	/* function called upon failure */
				args);
	rc = mio_obj_create(semid, NULL, sobj, sop);
	if (rc < 0) {
		mio_op_fini_free(sop);
		free(args);
	} 

	return rc;
}

void obj_write_success_cb(struct mio_op *op)
{
    	INFO("\033[1;34m Async write success callback called \033[0m\n");

	struct obj_io_cb_args *args;
	args = (struct obj_io_cb_args *)op->mop_app_cbs.moc_cb_data;
	struct mio_obj sobj;

	if (! (0 == obj_create_async(args->semid, &sobj))) {
		WARN("\033[1;34m Callback level 1: Failed to create a semaphore objet with ID (errno: %d)  \033[0m\n ", op->mop_rc);
	} else {
		//free(args->iovec->miov_base);
		    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)args->semid,
			INFO("\033[1;34m Callback level 1: Asynchronously created a semaphore object with ID %s  with success.  \033[0m\n", idstr););
	}
//	free(args->semid);
}

int obj_read_sync(struct mio_obj *obj, uint32_t num_iov, struct mio_iovec *iov)
{
	int rc;
	struct mio_op op;

	memset(&op, 0, sizeof op);
	mio_op_init(&op);

	rc = mio_obj_readv(obj, iov, 1, &op);
	if (rc != 0)
		return rc;

	rc = mio_cmd_wait_on_op(&op);
	mio_op_fini(&op);

	return rc;
}


static int 
obj_write_async(struct mio_obj *obj, uint32_t num_iov, struct mio_iovec *iov, struct mio_obj_id* semid)
{
	int rc;
	long op_idx;
	struct mio_op *op;
	struct obj_io_cb_args *args;
	
	args = malloc(sizeof *args);
	if (args == NULL)
		return -ENOMEM;

	op = mio_op_alloc_init();
	if (op == NULL) {
		free(args);
		return -ENOMEM;
	}

	args->semid = malloc(sizeof(struct mio_obj_id));
	args->obj = obj;
	memcpy(args->semid, semid, sizeof(struct mio_obj_id));
	mio_op_callbacks_set(	op, 
				obj_write_success_cb,
			       	obj_write_fail_cb,  	/* function called upon failure */
				args);
	rc = mio_obj_writev(obj, iov, num_iov, op);
	if (rc < 0) {
		mio_op_fini_free(op);
		free(args);
	} 

	return rc;
}

mstro_status
mstro_mio_obj_write_async(mstro_cdo src,
                          Mstro__Pool__TransferTicket* ticket,
                          struct mio_obj_id* oid,
                          struct mio_obj_id* semid)
{
  if (src == NULL || oid == NULL || semid == NULL)
    return MSTRO_INVARG;

  mstro_status s;
  
  /* retrieve data pointer and size */
  struct mstro_transport_datalen dl;
  s = mstro_transport__src_datalen_get(src, ticket, &dl);
  if (s != MSTRO_OK) {
    ERR("Couldn't get CDO data and size for mio transport (status: %s)\n", mstro_status_description(s));
    return MSTRO_FAIL;
  }
  void* data = dl.data;
  size_t len = dl.len;
  mstro_status status;

  /* retrieve specific data layout attributes */
  enum mstro_cdo_attr_value_type type;
  const int64_t* val;
  
	struct mio_iovec mvec;
	mvec.miov_base = (char*)data + ticket->src_offset; // cast to MIO's type +  read offset for dist cdos
        /* FIXME: should use constant value sym for attribute name */
	if (!(MSTRO_OK == mstro_cdo_attribute_get(
                src, ".maestro.core.cdo.layout.pre-pad", &type, (const void**)&val))) {
          ERR("Failed to get size attribute pre-padding of CDO %s for transport\n", src->name);
          return MSTRO_FAIL;
	}
	mvec.miov_off = *val;
	mvec.miov_len = len;
	struct mio_obj obj;
	memset(&obj, 0, sizeof obj);
DEBUG("Creating a new object to write in\n");
	int rc = obj_create(oid, &obj);
	if (rc < 0) {
		if (rc == -EEXIST) { // FIXME for debug
			WARN("Object already exists, we are overriding carelessly, since MIO can't unlink properly\n");
/*			DEBUG("MIO tries to remove preexisting object from failed last run...\n");
			if (! (MSTRO_OK == mstro_mio_obj_unlink((struct mio_obj_id*)oid))) {
			    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
				ERR("MIO failed to remove obj %s\n",
	                        idstr););
			    return MSTRO_FAIL;

			}
			DEBUG("... done removing junk obj\n");
*/		} else {
		    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
			ERR("MIO failed to create obj %s for transfer with error code %d\n",
                	        idstr, rc););
			return MSTRO_FAIL;
		}
	}
	WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(obj.mo_id),
		DEBUG("Async write (ID: %s)\n",
                        idstr););
//	rc = obj_write_async(&obj, 1, &mvec, semid);
	rc = obj_write_sync(&obj, 1, &mvec);
	if (rc < 0) {
	    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(oid),
		ERR("MIO failed to write obj %s for transfer with error code %d\n",
                        idstr, rc););
		return MSTRO_FAIL;
	}
	//mio_obj_close(&obj);
DEBUG ("Async write went ok (see callback for further report)\n");	

	/* The success callback will write the obj semaphore. Our
	 * job here is done. */

	return MSTRO_OK;
}

mstro_status
mstro_mio_obj_read_sync(mstro_cdo dst, 
                        Mstro__Pool__TransferTicket* ticket,
                        struct mio_obj_id* oid,
                        struct mio_obj_id* semid)
{
  if (dst == NULL || oid == NULL || semid == NULL)
    return MSTRO_INVARG;
  
  int64_t len = ticket->data_size;
  void* data = NULL;
  mstro_status s;
  if (! (MSTRO_OK == mstro_transport_get_dst_buffer(dst, (void*)&data))) {
    /* Already printed an error */
    return MSTRO_FAIL;
  }
  
  DEBUG("Sync read for now\n");
  /* retrieve specific data layout attributes FIXME redundancy */
  enum mstro_cdo_attr_value_type type;
  const int64_t* val;
  struct mio_iovec mvec;
  /* write data with the correct dst offset for distributed cdos*/
  data =(void *) ((char *) data + ticket->dst_offset); 	
  mvec.miov_base = (char*)data ; // cast to MIO's type 
  if (!(MSTRO_OK == mstro_cdo_attribute_get(
          dst, ".maestro.core.cdo.layout.pre-pad", &type, (const void**)&val))) {
    ERR("Failed to get size attribute pre-padding of CDO %s for transport\n", dst->name);
    return MSTRO_FAIL;
  }
  mvec.miov_off = *val;
  mvec.miov_len = len;
  
  int rc = 0;
  struct mio_obj obj;
  struct mio_obj sobj;
  memset(&obj, 0, sizeof obj);
  memset(&sobj, 0, sizeof sobj);
  
  /* Busy wait on open */
/*	
    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)semid,
	INFO("Trying to open semaphore  %s to know if I can read\n",
                        idstr););
	rc = obj_open_busywait(semid, &sobj);
	if (rc < 0) {
	    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)semid,
		ERR("MIO failed to open semaphore obj %s for transfer with error code %d\n",
                        idstr, rc););
		return MSTRO_FAIL;
	}
	WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
		INFO("Semaphore is there, let's read the CDO %s\n",
                        idstr););
*/
//	mio_obj_close(&sobj);
	
  WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
                   INFO("Open obj %s\n",
                        idstr););
  
  /* Open and read the actual CDO from Mero object */
  rc = obj_open(oid, &obj);
  if (rc < 0) {
    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
                     ERR("MIO failed to open obj %s for transfer with error code %d\n",
                         idstr, rc););
    return MSTRO_FAIL;
  }
  
  DEBUG("Reading len:%zu off:%d vec:%p\n", mvec.miov_len, mvec.miov_off, mvec.miov_base);	
  bzero(mvec.miov_base, mvec.miov_len);
  DEBUG("no doubt pointer is safe\n");
  rc = obj_read_sync(&obj, 1, &mvec);
  if (rc < 0) {
    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)oid,
                     ERR("MIO failed to read obj %s for transfer with error code %d\n",
                         idstr, rc););
    return MSTRO_FAIL;
  }
  
  data = mvec.miov_base; 
  
  DEBUG("NOT removing semaphore\n");
#if 0
  /* Remove semaphore object */
  if (! (MSTRO_OK == mstro_mio_obj_unlink(semid))) {
    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)semid,
                     ERR("MIO failed to remove semaphore obj %s\n",
                         idstr););
  } else {
    WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)semid,
                     DEBUG("MIO removed obj semaphore obj %s successfully\n",
                           idstr););
  }
#endif
  
  DEBUG ("Sync read went ok\n");	
  
  return MSTRO_OK;
}

mstro_status
mstro_transport_mio_src_execute(mstro_cdo src, Mstro__Pool__TransferTicket* ticket)
{
	if (src == NULL || ticket == NULL) {
		ERR("mstro_transport_mio_src_execute() called with invalid arguments\n");
		return MSTRO_INVARG;
	}
	if (sizeof(struct mio_obj_id) != ticket->mio->objid.len) {
		ERR(	"Ticketed objid for MIO transport has an invalid size\n"
			"Ticket uses:\t%zu\n"
			"MIO uses:\t%zu\n",
			 ticket->mio->objid.len,
			 sizeof(struct mio_obj_id));
		return MSTRO_INVARG;
	}

	INFO("Executing mio transport src side for CDO %s\n", src->name);

	
	if (! (ticket->mio->semid.len == sizeof(struct mio_obj_id))) {
		ERR("TicketMIO contains a semid with a len != struct mio_obj_id\n"
			"len: %zu"
			"sizeof (struct mio_obj_id): %zu",
			ticket->mio->semid.len,
			sizeof(struct mio_obj_id)
				);
		return MSTRO_FAIL;
	}

//	struct mio_thread thread;
//	mio_thread_init(&thread);

	struct mio_obj_id oid;
	struct mio_obj_id semid;

	assert(sizeof(struct mstro_cdo_id)==sizeof(struct mio_obj_id));
	memcpy(&semid, ticket->mio->semid.data, ticket->mio->semid.len);
	memcpy(&oid, ticket->mio->objid.data, ticket->mio->objid.len);

	WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(semid),
			INFO("Sanity check semaphore ID from ticket: %s\n",
                	        idstr););
	WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(oid),
			INFO("Sanity check object ID from ticket: %s\n",
                	        idstr););

        /* the async op will copy oid/semid content, so it's safe to use stack-allocated structs */
	if (! (MSTRO_OK == mstro_mio_obj_write_async(src, ticket,
                                                     &oid,
                                                     &semid))) {
		/* Already printed an error */
		return MSTRO_FAIL;
	}

//	mio_thread_fini(&thread);

//	INFO("src app successfully executed transport CDO %s\n", src->name);

	return MSTRO_OK;
}

mstro_status
mstro_transport_mio_dst_execute(mstro_cdo dst,
                                Mstro__Pool__TransferTicket* ticket)
{
  if (dst == NULL
      || ticket == NULL
      || ticket->ticket_case!=MSTRO__POOL__TRANSFER_TICKET__TICKET_MIO) {
    ERR("mstro_transport_mio_dst_execute() called with invalid arguments (%p/%p/%d)\n",
        dst, ticket, ticket? (int)ticket->ticket_case : -1);
    return MSTRO_INVARG;
  }
  INFO("Executing mio transport dst side for CDO %s\n", dst->name);

  
  DEBUG("receiving %zu bytes\n", len);
  
  struct mstro_cdo_id oid;
  struct mstro_cdo_id semid;
  memcpy(&semid, ticket->mio->semid.data, ticket->mio->semid.len);
  memcpy(&oid, ticket->mio->objid.data, ticket->mio->objid.len);

  WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(semid),
                   INFO("Sanity check semaphore ID from ticket: %s\n",
                        idstr););
  WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(oid),
                   INFO("Sanity check object ID from ticket: %s\n",
                        idstr););
  
  if (! (MSTRO_OK == mstro_mio_obj_read_sync(dst, ticket, (struct mio_obj_id*)&oid, (struct mio_obj_id*)&semid))) {
    /* Already printed an error */
    return MSTRO_FAIL;
  }
  
  INFO("dst app successfully executed transport CDO %s\n", dst->name);
  
#if 0
  if (! (ticket->mio->keep_obj)) {
    /* Remove transport object itself */
    if (! (MSTRO_OK == mstro_mio_obj_unlink((struct mio_obj_id*)&oid))) {
      WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(oid),
                       ERR("MIO failed to remove transport obj %s\n",
                           idstr););
    } else {
      WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)&(oid),
                       INFO("MIO removed transport obj %s successfully\n",
                            idstr););
    }
  }
#endif
  
  //	mio_thread_fini(&thread);
  
  return MSTRO_OK;
}
