/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool -- Common parts
 **/
/*
 * Copyright (C) 2019-2020 Cray Computer GmbH
 *           (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_pool.h"
#include "maestro/logging.h"
#include "maestro/i_cdo.h"
#include "maestro/i_globals.h"
#include "maestro/env.h"
#include "maestro/i_pool_manager_protocol.h"
#include "maestro/i_utlist.h"
#include "maestro/i_misc.h"
#include "maestro/i_statistics.h"

#include "i_subscription_registry.h"
#include "transformation/transformation.h"
#include "transport/transport.h"

#include "i_thread_team.h"

#include <pthread.h>
#include <assert.h>
#include <inttypes.h>


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PC,__VA_ARGS__)


#define NUM_CDOS_PER_POOL_ENTRY 4
/** The local pool is made up of these entries.
 **
 ** It maps (globally valid) CDO IDs to the set of all local handles.
 **
 ** Waiting on changes of an entry permits to detect availabilty of
 ** matching src/dst pairs of CDOs.
 **
 **
 **/
struct mstro_pool_entry {
  UT_hash_handle hh;           /**< make structure hashable */
  struct mstro_cdo_id   cdoid; /**< primary key, local-id of should be MSTRO_CDO_LOCAL_ID_NONE */
  pthread_mutex_t mtx;         /**< lock for (the handles of) this entry */
  pthread_cond_t  cvar;        /**< condition variable to wait for entry changes */
  size_t          num_waiters; /**< number of processes currently blocked on this cvar */
  size_t         num_cdo_handles; /**< number of CDO handles for this ID */
  size_t         num_free_slots;  /**< number of free cdo_handles slots */
  mstro_cdo      *cdo_handles;    /**< the CDO handles */
};

/** utility to lock a critical section for a pool entry */

#define WITH_LOCKED_POOL_ENTRY(entry, errorlabel, body)                 \
  do {                                                                  \
    if(pthread_mutex_lock(&e->mtx)!=0) {                                \
      WITH_CDO_ID_STR(                                                  \
          idstr, & ((e)->cdoid), {                                      \
            ERR("Failed to lock mutex for pool entry %p (id %s)\n",     \
                e, idstr);                                              \
          });                                                           \
      goto errorlabel;                                                  \
    }                                                                   \
    do { body } while(0);                                               \
    if(pthread_mutex_unlock(&e->mtx)!=0) {                              \
      WITH_CDO_ID_STR(                                                  \
          idstr, & ((e)->cdoid), {                                      \
            ERR("Failed to unlock mutex for pool entry %p (id %s)\n",   \
                e, idstr);                                              \
          });                                                           \
      goto errorlabel;                                                  \
    }                                                                   \
  } while(0)

/** Allocate pool entry */
static inline
mstro_status
mstro_pool_entry__alloc(struct mstro_pool_entry **res)
{
  if(res==NULL) {
    ERR("NULL pool entry handle destination\n");
    return MSTRO_INVARG;
  }

  mstro_status status;

  int s1,s2;
  *res = calloc(1,sizeof(struct mstro_pool_entry));
  if(*res==NULL) {
    ERR("Cannot allocate pool entry\n");
    return MSTRO_NOMEM;
  } else {
    s1 = pthread_mutex_init(&((*res)->mtx), NULL);
    s2 = pthread_cond_init(&((*res)->cvar), NULL);
    (*res)->num_free_slots = 0;
    (*res)->num_cdo_handles = 0;
    (*res)->cdo_handles = NULL;
    if(s1!=0 || s2!=0) {
      ERR("pthread mutex or cond init failed\n");
      status = MSTRO_FAIL;
      goto BAILOUT;
    }
  }
  status = MSTRO_OK;
BAILOUT:
  if(status!=MSTRO_OK) {
    if(*res!=NULL) {
      if(s1==0) {
        pthread_mutex_destroy(&((*res)->mtx));
      }
      if(s2==0) {
        pthread_cond_destroy(&((*res)->cvar));
      }
      if((*res)->cdo_handles!=NULL) {
        free((*res)->cdo_handles);
      }
      free(*res);
      *res=NULL;
    }
  }
  return status;
}

/** Destroy a pool entry */
static inline
mstro_status
mstro_pool_entry__destroy(struct mstro_pool_entry *e)
{
  mstro_status status = MSTRO_OK;
  if(e==NULL)
    return MSTRO_INVARG;
  int s;
  s=pthread_mutex_destroy(&e->mtx);
  if(s!=0) {
    ERR("Failed to destroy mutex\n");
    status = MSTRO_FAIL;
  }
  s=pthread_cond_destroy(&e->cvar);
  if(s!=0) {
    ERR("Failed to destroy cvar\n");
    status = MSTRO_FAIL;
  }

  free(e->cdo_handles);
  free(e);

  return status;
}


/** Add CDO to pool entry. Entry must already be locked */
static inline
mstro_status
mstro_pool_entry__add(struct mstro_pool_entry *e, mstro_cdo cdo)
{
  mstro_status status = MSTRO_OK;

  if(e==NULL || cdo==NULL)
    return MSTRO_INVARG;

  if(! mstro_cdo_id__equal_uuid_only(&e->cdoid, &cdo->gid)) {
    ERR("Trying to add CDO to wrong pool entry (or gid not ready)\n");
    status = MSTRO_INVARG;
  } else {
    if(e->num_free_slots==0) {
      mstro_cdo * tmp = realloc(e->cdo_handles,
                                sizeof(mstro_cdo *)
                                *(e->num_cdo_handles
                                  + NUM_CDOS_PER_POOL_ENTRY));
      if(tmp==NULL) {
        ERR("Failed to re-allocate handles in pool entry\n");
        status = MSTRO_NOMEM;
      } else {
        e->cdo_handles = tmp;
        e->num_free_slots += NUM_CDOS_PER_POOL_ENTRY;
      }
    }
    if(status==MSTRO_OK) {
      e->cdo_handles[e->num_cdo_handles] = cdo;
      e->num_cdo_handles++;
      e->num_free_slots--;
      WITH_CDO_ID_STR(
          idstr, & e->cdoid,
          DEBUG("Added CDO handle %p; pool entry %p for id %s now has %zu handles, (%zu free slots)\n",
                cdo, e, idstr,
                e->num_cdo_handles, e->num_free_slots);
                      );
    }
  }

BAILOUT:
  return status;
}

/* Find the pool entry for cdo->gid but with local_id set to MSTRO_CDO_LOCAL_ID_NONE (= the master entry).
 *
 * If none exists, creates it (but does not add CDO itself to it).
 *
 * Returns the entry in locked state if return value is MSTRO_OK.
 *
 * Must be called while holding g_mstro_pool_local_mtx.
 */
static inline
mstro_status
mstro_pool__find_entry_and_lock(mstro_cdo cdo, struct mstro_pool_entry **res)
{
  mstro_status status = MSTRO_OK;
  int s;

  struct mstro_cdo_id tmp = cdo->gid;
  tmp.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  HASH_FIND(hh, g_mstro_pool_local,
            &tmp, sizeof(struct mstro_cdo_id),
            *res);
  if(! (*res)) {
    DEBUG("no entry found, creating new one\n");
    /* create new (empty) entry, add to table */
    status = mstro_pool_entry__alloc(res);
    if(status!=MSTRO_OK) {
      ERR("Failed to allocate pool entry\n");
      goto BAILOUT;
    }
    (*res)->cdoid = tmp;
    DEBUG("adding new entry %p to pool\n", *res);
    HASH_ADD(hh, g_mstro_pool_local,
             cdoid, sizeof(struct mstro_cdo_id),
             *res);
  }

  s = pthread_mutex_lock(& (*res)->mtx);
  if(s!=0) {
    ERR("Failed to lock pool entry\n");
    status = MSTRO_FAIL;
  } else {
    status = MSTRO_OK;
  }
BAILOUT:
  return status;
}

mstro_status
mstro_pool__add(mstro_cdo cdo, mstro_cdo_state new_state)
{
  mstro_status status = MSTRO_OK;
  struct mstro_pool_entry *e=NULL;
  int s;

  if(cdo==NULL) {
    return MSTRO_INVARG;
  }

  if(mstro_cdo_id__equal(&MSTRO_CDO_ID_NULL, &cdo->gid)) {
    ERR("CDO has no gid\n");
    return MSTRO_INVARG;
  }

  assert(new_state==MSTRO_CDO_STATE_OFFERED_LOCALLY
         || new_state==MSTRO_CDO_STATE_REQUIRED_LOCALLY);

  /* find ID in pool */
  s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock the pool mutex\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  DEBUG("finding entry in pool\n");
  e=NULL;

  status = mstro_pool__find_entry_and_lock(cdo, &e); /* creates an empty one if needed */

  /* can release table lock */
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* check whether entry is fine (and thus already locked) */
  if(status!=MSTRO_OK) {
    ERR("Failed to obtain pool entry for CDO\n");
    goto BAILOUT;
  }

  status = mstro_pool_entry__add(e, cdo);

  if(status!=MSTRO_OK) {
    ERR("Failed to add CDO to pool entry\n");
  } else {
    mstro_cdo_state_set(cdo, new_state);

    /* DEBUG("added CDO to pool entry; could inject into scheduler knowledge\n"); */

    /* WARN("NOT signaling pool manager that entry is in pool now\n"); */

    /* tell all who want to know */
    if(e->num_waiters>0) {
      s=pthread_cond_broadcast(& e->cvar);
      if(s!=0) {
        ERR("Failed to signal pool entry cvar\n");
        status=MSTRO_FAIL;
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change, state now %s\n",
		    e->num_waiters,
		    mstro_cdo_state_describe(
			    mstro_cdo_state_get(cdo))););
      }
    } else {
      DEBUG("No waiters on this entry\n");
    }

    /* FIXME: (or not?)
     *
     * Currently we handle things lazily and let the WITHDRAW and
     * DEMAND ops block using pool wait ops. For a smarter async
     * scheduler we could do more here if we had that knowledge.
     */
  }

  s = pthread_mutex_unlock(&e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  status = MSTRO_OK;

BAILOUT:
  return status;
}


/** drop CDO from entry. Must be called with the entry already locked. */
static inline
mstro_status
mstro_pool_entry__drop(struct mstro_pool_entry *e, const mstro_cdo cdo)
{
  mstro_status status = MSTRO_UNIMPL;

  assert(mstro_cdo_id__equal_uuid_only(&e->cdoid, &cdo->gid));

  /* find it among handles */
  size_t i;
  for(i=0; i<e->num_cdo_handles; i++) {
    if(e->cdo_handles[i]==cdo)
      break;
  }
  if(i==e->num_cdo_handles) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("CDO handle %p for CDO %s (id %s) not present in pool entry\n",
            cdo->name, idstr););
    status = MSTRO_FAIL;
  } else {
    /* got it at index i */
    e->num_cdo_handles--;
    e->num_free_slots++;

    if(e->num_cdo_handles>0) {
      /* swap last one into the place 'i' */
      e->cdo_handles[i] = e->cdo_handles[e->num_cdo_handles];
    }
    status = MSTRO_OK;
  }
  return status;
}

static inline
void
mstro_pool_entry__compute_states(const struct mstro_pool_entry *e,
                                 size_t *num_provides,
                                 size_t *num_requires,
                                 size_t *num_demanded)
{
  /* check if our CDO is the last one in OFFER state and there are
   * open REQUIRES */
  *num_provides=0;
  *num_requires=0;
  *num_demanded=0;
  for(size_t i=0; i<e->num_cdo_handles; i++) {
    if(mstro_cdo_state_check(e->cdo_handles[i],
                             MSTRO_CDO_STATE_OFFERED
                             | MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY)) {
      (*num_provides)++;
    } else if(mstro_cdo_state_check(e->cdo_handles[i],
                                    MSTRO_CDO_STATE_REQUIRED)) {
      (*num_requires)++;
    } else if(mstro_cdo_state_check(e->cdo_handles[i],
                                    MSTRO_CDO_STATE_DEMANDED)) {
      (*num_demanded)++;
    }
  }
}


mstro_status
mstro_pool__remove_async(mstro_cdo cdo, mstro_cdo_state new_state)
{
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  assert(new_state == MSTRO_CDO_STATE_WITHDRAWN
         || new_state == MSTRO_CDO_STATE_RETRACTED
         || new_state == MSTRO_CDO_STATE_DEMANDED);

  /* find entry */
  mstro_status status = MSTRO_UNIMPL;

  /* ask pool manager */
  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = cdo->gid.qw[0];
  cdoid.qw1 = cdo->gid.qw[1];
  cdoid.local_id = cdo->gid.local_id;

  Mstro__Pool__Withdraw withdraw = MSTRO__POOL__WITHDRAW__INIT;
  Mstro__Pool__Retract retract = MSTRO__POOL__RETRACT__INIT;
  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;

  switch(new_state) {
    case MSTRO_CDO_STATE_WITHDRAWN: {
      withdraw.cdoid = &cdoid;
      status = mstro_pmp_package(&msg, (ProtobufCMessage*)&withdraw);
      if(status!=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            withdraw.base.descriptor->name);
        return status;
      }
      break;
    }
    case MSTRO_CDO_STATE_RETRACTED: {
      retract.cdoid = &cdoid;
      status = mstro_pmp_package(&msg, (ProtobufCMessage*)&retract);
      if(status!=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            retract.base.descriptor->name);
        return status;
      }
      break;
    }
    case MSTRO_CDO_STATE_DEMANDED: {
      /* remove calls us too, but we don't need to signal pool manager */
      //      msg = NULL;
      WARN("CDO already in DEMANDED state when withdrawing -- no valid msg for PM\n");
      break;
    }
    default:
      ERR("illegal new_state: %d\n");
      return MSTRO_FAIL;
  }
  /* send out request. The CDO state will be changed to
   * WITHDRAWN_GLOBALLY or RETRACTED_GLOBALL by the incoming ACK (or
   * here if no PM exists), and we'll thus pass the state change check
   * blocking location below when it's all done. */
  if(new_state!=MSTRO_CDO_STATE_DEMANDED) {
    status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
    switch(status) {
      case MSTRO_OK:
        break; /* ok */
      case MSTRO_NO_PM:
        DEBUG("WITHDRAW/RETRACT/DEMAND local only, no PM\n");
        switch(new_state) {
          case MSTRO_CDO_STATE_WITHDRAWN:
            mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY);
            status = MSTRO_OK;
            break;
          case MSTRO_CDO_STATE_RETRACTED:
            mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_RETRACTED_GLOBALLY);
            status = MSTRO_OK;
            break;
          default:
            ERR("illegale new_state %d\n");
            return MSTRO_FAIL;
        }
        break;
      default:
        ERR("Failed to send WITHDRAW/RETRACT/DEMAND message to pool manager: %d (%s)\n",
            status, mstro_status_description(status));
        return MSTRO_FAIL;
    }
    //increase waiters
    status = mstro_pool_cdo_waiters(cdo, 1);
  }
  return status;
}

/** test for local waiters on a cdo that is being withdrawn or retracted
and tries to set its state to the target state */
bool
mstro_cdo_test_local_requires(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);

  mstro_cdo cdo = request->cdo.cdo;
  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = cdo->gid.qw[0];
  cdoid.qw1 = cdo->gid.qw[1];
  cdoid.local_id = cdo->gid.local_id;
  bool success = false;
  mstro_status status;

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return success;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
            ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
                cdo->name, idstr););
    success = false;
    goto BAILOUT_UNLOCK_POOL;
  }

  /* we already have the entry locked; can release table lock */
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    success = false;
  } else {
    size_t num_provides=0;
    size_t num_requires=0;
    size_t num_demanded=0;

    if(request->cdo.orig_state==MSTRO_CDO_STATE_OFFERED) {
        mstro_pool_entry__compute_states(e,
                                         &num_provides, &num_requires,
                                         &num_demanded);
        WITH_CDO_ID_STR(
            cdoid, & cdo->gid,
            DEBUG("CDO `%s' (id %s): %zu providing, %zu requiring, %zu demanding handles\n",
                  cdo->name, cdoid, num_provides, num_requires, num_demanded););

        /* This will not happen if the pool manager is enabled, as it
         * would not grant the WITHDRAW until all of these outstanding
         * REQUIREs are done. But if no pool manager exists we need to
         * handle it locally */
        if(num_requires>0 && num_provides==1)
        {
          WITH_CDO_ID_STR(
              cdoid, & cdo->gid,
              DEBUG("This is the last instance of CDO `%s' (id %s) "
                    "that can satisfy the %zu outstanding REQUIREs, waiting\n",
                    cdo->name, cdoid, num_requires););
          success = false;

        } else {
          DEBUG("No current REQUIREs (or have alternative OFFERs), can remove\n");
          success = true;
        }
    } else if(request->cdo.orig_state == MSTRO_CDO_STATE_REQUIRED) {
      /* FIXME: if we had eager REQUIRE-fulfilment we'd like to be
       * sure to not kill a require that can serve as OFFERer
       * without extra considerations */
      DEBUG("REQUIREd CDO removal (likely for RETRACT) without extra considerations\n");
      /* Losing a require could make processes that could not
       * WITHDRAW because of use continue */
      /* tell all who want to know */
      if(e->num_waiters>0) {
        /* give them a chance, just in case (FIXME: maybe unnecessary) */
        s=pthread_cond_broadcast(& e->cvar);
        if(s!=0) {
          ERR("Failed to signal pool entry cvar\n");
          success = false;
        } else {
          WITH_CDO_ID_STR(
              idstr, & e->cdoid,
              DEBUG("Signaled %zu waiters about pool entry change\n",
                    e->num_waiters););
        }
      } else {
        DEBUG("No waiters on this entry\n");
        success = true;
      }
    }

    status = mstro_pool_entry__drop(e,cdo);
    if(status==MSTRO_OK) {
      mstro_cdo_state_set(cdo, request->cdo.target_state);
      success = true;
    }
  }
  s= pthread_mutex_unlock(& e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry \n");
    success = false;
  }


BAILOUT:
  return success;

BAILOUT_UNLOCK_POOL:
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    success = false;
  }
  goto BAILOUT;

}

/** wait for local waiters on a cdo that is being withdrawn or retracted */
void
mstro_cdo_wait_local_requires(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);
  mstro_cdo cdo = request->cdo.cdo;
  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = cdo->gid.qw[0];
  cdoid.qw1 = cdo->gid.qw[1];
  cdoid.local_id = cdo->gid.local_id;
  mstro_status status;

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return ;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
            ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
                cdo->name, idstr););
    status = MSTRO_FAIL;
    goto BAILOUT_UNLOCK_POOL;
  }

  /* we already have the entry locked; can release table lock */
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    status = MSTRO_FAIL;
  } else {
    size_t num_provides=0;
    size_t num_requires=0;
    size_t num_demanded=0;

    if(request->cdo.orig_state==MSTRO_CDO_STATE_OFFERED) {
      do {
        mstro_pool_entry__compute_states(e,
                                         &num_provides, &num_requires,
                                         &num_demanded);
        WITH_CDO_ID_STR(
            cdoid, & cdo->gid,
            DEBUG("CDO `%s' (id %s): %zu providing, %zu requiring, %zu demanding handles\n",
                  cdo->name, cdoid, num_provides, num_requires, num_demanded););

        /* This will not happen if the pool manager is enabled, as it
         * would not grant the WITHDRAW until all of these outstanding
         * REQUIREs are done. But if no pool manager exists we need to
         * handle it locally */
        if(num_requires>0 && num_provides==1) {
          WITH_CDO_ID_STR(
              cdoid, & cdo->gid,
              DEBUG("This is the last instance of CDO `%s' (id %s) "
                    "that can satisfy the %zu outstanding REQUIREs, waiting\n",
                    cdo->name, cdoid, num_requires););
          e->num_waiters++;

          mstro__abort_if_deadlock();

          s = pthread_cond_wait(& e->cvar, & e->mtx);
          /*raise the wait flag on this request */
          request->cdo.wait_flag=1;

          if(s!=0) {
            ERR("Failed to wait on pool entry %p\n", e);
            status = MSTRO_FAIL;
            break; /* from DO-loop */
          }
          e->num_waiters--;
          DEBUG("Woke up, will try removing again\n");
        } else {
          DEBUG("No current REQUIREs (or have alternative OFFERs), can remove\n");
          break; /* from DO loop */
        }
      } while(1);
    } else if(request->cdo.orig_state == MSTRO_CDO_STATE_REQUIRED) {
      /* FIXME: if we had eager REQUIRE-fulfilment we'd like to be
       * sure to not kill a require that can serve as OFFERer
       * without extra considerations */
      DEBUG("REQUIREd CDO removal (likely for RETRACT) without extra considerations\n");
      /* Losing a require could make processes that could not
       * WITHDRAW because of use continue */
      /* tell all who want to know */
      if(e->num_waiters>0) {
        /* give them a chance, just in case (FIXME: maybe unnecessary) */
        s=pthread_cond_broadcast(& e->cvar);
        if(s!=0) {
          ERR("Failed to signal pool entry cvar\n");
          status=MSTRO_FAIL;
        } else {
          WITH_CDO_ID_STR(
              idstr, & e->cdoid,
              DEBUG("Signaled %zu waiters about pool entry change\n",
                    e->num_waiters););
        }
      } else {
        DEBUG("No waiters on this entry\n");
      }
    }

    status = mstro_pool_entry__drop(e,cdo);
    if(status==MSTRO_OK) {
      mstro_cdo_state_set(cdo, request->cdo.target_state);
    }
  }
  s= pthread_mutex_unlock(& e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry \n");
    status = MSTRO_FAIL;
  }


BAILOUT:
  return ;

BAILOUT_UNLOCK_POOL:
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    status = MSTRO_FAIL;
  }
  goto BAILOUT;

}


mstro_status
mstro_pool__remove(mstro_cdo cdo, mstro_cdo_state new_state)
{
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  assert(new_state == MSTRO_CDO_STATE_WITHDRAWN
         || new_state == MSTRO_CDO_STATE_RETRACTED
         || new_state == MSTRO_CDO_STATE_DEMANDED);

  /* find entry */
  mstro_status status = MSTRO_UNIMPL;

  mstro_cdo_state orig_state = mstro_cdo_state_get(cdo); 
  if(orig_state != MSTRO_CDO_STATE_OFFERED
   &&orig_state != MSTRO_CDO_STATE_REQUIRED
   &&orig_state != MSTRO_CDO_STATE_REQUIRED_LOCALLY) {
    ERR("Illegal CDO state: %d\n", orig_state);
    return MSTRO_FAIL;
  }
  WITH_CDO_ID_STR(
      idstr, &cdo->gid,
      DEBUG("CDO %s orig state is %d (%s)\n",
            idstr, orig_state, mstro_cdo_state_describe(orig_state));
                  );

  /* ask pool manager */
  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = cdo->gid.qw[0];
  cdoid.qw1 = cdo->gid.qw[1];
  cdoid.local_id = cdo->gid.local_id;

  Mstro__Pool__Withdraw withdraw = MSTRO__POOL__WITHDRAW__INIT;
  Mstro__Pool__Retract retract = MSTRO__POOL__RETRACT__INIT;
  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;

  switch(new_state) {
    case MSTRO_CDO_STATE_WITHDRAWN: {
      withdraw.cdoid = &cdoid;
      status = mstro_pmp_package(&msg, (ProtobufCMessage*)&withdraw);
      if(status!=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            withdraw.base.descriptor->name);
        return status;
      }
      break;
    }
    case MSTRO_CDO_STATE_RETRACTED: {
      retract.cdoid = &cdoid;
      status = mstro_pmp_package(&msg, (ProtobufCMessage*)&retract);
      if(status!=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            retract.base.descriptor->name);
        return status;
      }
      break;
    }
    case MSTRO_CDO_STATE_DEMANDED: {
      /* remove calls us too, but we don't need to signal pool manager */
      //      msg = NULL;
      WARN("CDO already in DEMANDED state when withdrawing -- no valid msg for PM\n");
      break;
    }
    default:
      ERR("illegal new_state: %d\n", new_state);
      return MSTRO_FAIL;
  }
  /* send out request. The CDO state will be changed to
   * WITHDRAWN_GLOBALLY or RETRACTED_GLOBALL by the incoming ACK (or
   * here if no PM exists), and we'll thus pass the state change check
   * blocking location below when it's all done. */
  if(new_state!=MSTRO_CDO_STATE_DEMANDED) {
    status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
    switch(status) {
      case MSTRO_OK:
        break; /* ok */
      case MSTRO_NO_PM:
        DEBUG("WITHDRAW/RETRACT/DEMAND local only, no PM\n");
        switch(new_state) {
          case MSTRO_CDO_STATE_WITHDRAWN:
            mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY);
            break;
          case MSTRO_CDO_STATE_RETRACTED:
            mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_RETRACTED_GLOBALLY);
            break;
          default:
            ERR("illegal new_state %d\n", new_state);
            return MSTRO_FAIL;
        }
        break;
      default:
        ERR("Failed to send WITHDRAW/RETRACT/DEMAND message to pool manager: %d (%s)\n",
            status, mstro_status_description(status));
        return MSTRO_FAIL;
    }

    /* block waiting for CDO state change to be granted -- we'll
     * notice because withdraw reply changes state of cdo */
    mstro_cdo_block_until(cdo,  MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY
                                 |MSTRO_CDO_STATE_RETRACTED_GLOBALLY, "GLOBALLY UNPOOLED");

    DEBUG("CDO withdrawn/retracted from global pool\n");

    /* Sync with the transport layer to ensure windows are closed before
     * relinquishing control to the user. 
     * Rationale: If PM is faster to recv and handle transfer completion, it
     * might give us the go ahead even before we had time to finish cleanup */
     status = mstro_transport_cleanup_or_wait(cdo);
     if (status != MSTRO_OK) {
       WITH_CDO_ID_STR(
           idstr, &cdo->gid,
           ERR("Failed to check/wait for transport completion of cdo %s\n",
               idstr););
       return status;
     }	
  } 

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
            ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
                cdo->name, idstr););
    status = MSTRO_FAIL;
    goto BAILOUT_UNLOCK_POOL;
  }

  /* we already have the entry locked; can release table lock */
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    status = MSTRO_FAIL;
  } else {
    size_t num_provides=0;
    size_t num_requires=0;
    size_t num_demanded=0;

    if(orig_state==MSTRO_CDO_STATE_OFFERED) {
      do {
        mstro_pool_entry__compute_states(e,
                                         &num_provides, &num_requires,
                                         &num_demanded);
        WITH_CDO_ID_STR(
            cdoid, & cdo->gid,
            DEBUG("CDO `%s' (id %s): %zu providing, %zu requiring, %zu demanding handles\n",
                  cdo->name, cdoid, num_provides, num_requires, num_demanded););

        /* This will not happen if the pool manager is enabled, as it
         * would not grant the WITHDRAW until all of these outstanding
         * REQUIREs are done. But if no pool manager exists we need to
         * handle it locally */
        if(num_requires>0 && num_provides==1) {
          WITH_CDO_ID_STR(
              cdoid, & cdo->gid,
              DEBUG("This is the last instance of CDO `%s' (id %s) "
                    "that can satisfy the %zu outstanding REQUIREs, waiting\n",
                    cdo->name, cdoid, num_requires););
          e->num_waiters++;

          mstro__abort_if_deadlock();

          s = pthread_cond_wait(& e->cvar, & e->mtx);
          if(s!=0) {
            ERR("Failed to wait on pool entry %p\n", e);
            status = MSTRO_FAIL;
            break; /* from DO-loop */
          }
          e->num_waiters--;
          DEBUG("Woke up, will try removing again\n");
        } else {
          DEBUG("No current REQUIREs (or have alternative OFFERs), can remove\n");
          break; /* from DO loop */
        }
      } while(1);
    } else if(orig_state == MSTRO_CDO_STATE_REQUIRED) {
      /* FIXME: if we had eager REQUIRE-fulfilment we'd like to be
       * sure to not kill a require that can serve as OFFERer
       * without extra considerations */
      DEBUG("REQUIREd CDO removal (likely for RETRACT) without extra considerations\n");
      /* Losing a require could make processes that could not
       * WITHDRAW because of use continue */
      /* tell all who want to know */
      if(e->num_waiters>0) {
        /* give them a chance, just in case (FIXME: maybe unnecessary) */
        s=pthread_cond_broadcast(& e->cvar);
        if(s!=0) {
          ERR("Failed to signal pool entry cvar\n");
          status=MSTRO_FAIL;
        } else {
          WITH_CDO_ID_STR(
              idstr, & e->cdoid,
              DEBUG("Signaled %zu waiters about pool entry change\n",
                    e->num_waiters););
        }
      } else {
        DEBUG("No waiters on this entry\n");
      }
    }

    status = mstro_pool_entry__drop(e,cdo);
    if(status==MSTRO_OK) {
      mstro_cdo_state_set(cdo, new_state);
    }
  }
  s= pthread_mutex_unlock(& e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry \n");
    status = MSTRO_FAIL;
  }


BAILOUT:
  return status;

BAILOUT_UNLOCK_POOL:
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    status = MSTRO_FAIL;
  }
  goto BAILOUT;

}

static inline
mstro_status
mstro_pool__connect_to_pm(const char *remote_pm_info)
{
  /* We do mstro_ofi_init which enumerates local interfaces. Then we
   * try to connect from one of them to one of the remote_pm_info
   * ones. First match wins */
  return mstro_pm_attach(remote_pm_info);
}


mstro_status
mstro_pool_init(void)
{
  mstro_status s=MSTRO_UNIMPL;

  NOISE("Initializing pool\n");

  const char* remote_pm_info = getenv(MSTRO_ENV_POOL_INFO);

  s=mstro_pmp_init();
  if(s!=MSTRO_OK) {
    goto BAILOUT;
  }
  
  if(remote_pm_info!=NULL) {
    DEBUG("Pool manager info provided: %s\n", remote_pm_info);
    if(getenv(MSTRO_ENV_START_POOL_MANAGER)!=NULL) {
      ERR("Both pool manager info and START_POOL_MANAGER flag given -- unimplemented\n");
      s=MSTRO_UNIMPL;
      goto BAILOUT;
    }

    s = mstro_pool__connect_to_pm(remote_pm_info);
    if(s!=MSTRO_OK) {
      ERR("Failed to connect to pool manager\n");
      goto BAILOUT;
    } else {
      atomic_store(&g_mstro_pm_attached, true);

    }

    goto BAILOUT;

  } else {
    DEBUG("No pool manager info provided, not connecting to pool manager\n");
    s=MSTRO_OK;
  }
BAILOUT:
  return s;
}

mstro_status
mstro_pool_finalize(void)
{
  NOISE("Cleaning up pool\n");
  size_t num_entries=0;
  mstro_status status = MSTRO_OK;
  bool  nonempty_entry_errors = false;

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  num_entries = HASH_COUNT(g_mstro_pool_local);
  DEBUG("Pool has %zu entries\n", num_entries);

  if(atomic_load(&g_mstro_pm_attached)) {
    /* FIXME: this needs to go away when the ofi.c refactoring is done */
    mstro_status mstro_pc_terminate(void);

    s = mstro_pc_terminate();
    if(s!=MSTRO_OK) {
      ERR("Failed to tear down pool manager connection\n");
    }
  }
  /* hopefully reduced to 0 by now */
  num_entries = HASH_COUNT(g_mstro_pool_local);
  if(num_entries>0) {
    struct mstro_pool_entry *e,*tmp;
    HASH_ITER(hh, g_mstro_pool_local, e, tmp) {
      HASH_DEL(g_mstro_pool_local, e);
      if(e->num_cdo_handles>0) {
        WITH_CDO_ID_STR(id,&e->cdoid,{
            ERR("Pool entry for id %s has %d CDOs, this is illegal\n",
                id, e->num_cdo_handles);});
        nonempty_entry_errors=true;
      }
      if(e->num_waiters>0) {
        WITH_CDO_ID_STR(id,&e->cdoid,{
            ERR("Pool entry for id %s has %zu waiters blocked on it, this is illegal\n",
                id, e->num_waiters);});
        nonempty_entry_errors=true;
      }


      if(nonempty_entry_errors) {
      	ERR("Pool had live CDOs at termination\n");
      }
      status = mstro_pool_entry__destroy(e);
    }
  }
  g_mstro_pool_local = NULL;
  s = pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0)
    status = (status==MSTRO_OK) ? MSTRO_FAIL : status;
  if(nonempty_entry_errors)
    status = (status==MSTRO_OK) ? MSTRO_FAIL : status;

  mstro_status s1=mstro_pmp_finalize();
  status = (status==MSTRO_OK) ? s1 : status;

BAILOUT:
  return status;
}


static inline
mstro_status
mstro_pool__demand_no_pm(mstro_cdo cdo, mstro_cdo_state new_state)
{
  char uuid_str[MSTRO_CDO_ID_STR_LEN];
  char *uuid = &uuid_str[0];

  mstro_status status = mstro_cdo_id__str(&cdo->gid, &uuid);
  if(status!=MSTRO_OK) {
    ERR("Cannot obtain id string of CDO\n");
    goto BAILOUT;
  }

  status = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status !=MSTRO_OK) {
    ERR("Failed to find CDO entry: %d\n");
  }

  /* quickly unlock table mutex */
  status=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* check if we have entry */
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
            cdo->name, idstr););
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* have locked entry.
   * FIXME this code should be refactored to use mstro_pool__find_source_cdo or similar 
   * Since we're PM-less we need to find a local * source or go to
   * sleep until one comes in */
  size_t src=0;
  do {
    /* find source among handles */
    for(src=0; src<e->num_cdo_handles; src++) {
      if(mstro_cdo_state_check(e->cdo_handles[src],
                               MSTRO_CDO_STATE_OFFERED
                               | MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY)) {
        break;
      }
    }
    if(src<e->num_cdo_handles) {
      DEBUG("Found CDO at index %zu that can provide data\n", src);
      /* FIXME: it would be good to check whether someone else is
       * waiting at DEMAND for this CDO, and could copy it before we
       * pull it from the pool. However, e->num_waiters>=1 is not
       * sufficient to test this, since other (non-DEMAND) waiters
       * could exist. */
      goto HAVE_SOURCE;
    }
    /* wait for something to happen */
    e->num_waiters++;
    DEBUG("No suitable provider; wait on pool entry %p,"
          " CDO `%s' (id %s), %zu total waiters\n",
          e, cdo->name, uuid, e->num_waiters);
    mstro__abort_if_deadlock();
    int s = pthread_cond_wait(&e->cvar, & e->mtx);
    if(s!=0) {
      ERR("Failed in cond_wait for entry\n");
      status = MSTRO_FAIL;
      goto BAILOUT_UNLOCK_ENTRY;
    }
    e->num_waiters--;
    DEBUG("Woke up on entry %p,"
          " CDO `%s' (id %s), %zu total waiters (plus me), %zu handles\n",
          e, cdo->name, uuid,
          e->num_waiters, e->num_cdo_handles-e->num_waiters);
  } while(1);

HAVE_SOURCE:
  /* have provider at src */
  assert(e->cdo_handles[src]!=cdo); /* can't happen in no-PM case */

  status = mstro_cdo__satisfy(e->cdo_handles[src], cdo);
  if(status!=MSTRO_OK) {
    ERR("Failed to satisfy DEMAND on %p from %p\n",
        cdo, e->cdo_handles[src]);
    goto BAILOUT_UNLOCK_ENTRY;
  }

  /* if applicable, transform incoming cdo so it matches demanded cdo requirements */
/*
  status = mstro_transform_layout(e->cdo_handles[src], cdo,
                                  e->cdo_handles[src]->attributes, cdo->attributes);
  if(status==MSTRO_NOMATCH) {
    DEBUG("No transformation matched\n");
  } else if(status!=MSTRO_OK) {
    ERR("Failed to transform %p to %p\n",
        e->cdo_handles[src], cdo);
    goto BAILOUT_UNLOCK_ENTRY;
  }
*/
  /* drop our own entry */
  status = mstro_pool_entry__drop(e, cdo);
  if(status==MSTRO_OK) {
    mstro_cdo_state_set(cdo, new_state);

    /* tell all who want to know. Note that this is only relevant for
     * non-DEMAND waiters, like WITHDRAWs that could not proceed, and
     * might now that we are gone */
    if(e->num_waiters>0) {
      status=pthread_cond_broadcast(& e->cvar);
      if(status!=0) {
        ERR("Failed to signal pool entry cvar\n");
        status=MSTRO_FAIL;
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change\n",
                  e->num_waiters););
      }
    } else {
      DEBUG("No waiters on this entry\n");
    }
  }

BAILOUT_UNLOCK_ENTRY:
  status = pthread_mutex_unlock(&e->mtx);
  if(status!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
BAILOUT:
  return status;
}

mstro_status mstro_pool_cdo_waiters(mstro_cdo cdo, int change)
{

  mstro_status status = MSTRO_UNIMPL;

  status = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status !=MSTRO_OK) {
    ERR("Failed to find CDO entry: %d\n");
    goto BAILOUT;
  }

  /* quickly unlock table mutex */
  status=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
    goto BAILOUT_UNLOCK_ENTRY;
  }

  /* add/remove waiters */
  DEBUG("Change the number of waiter on cdo by %d \n", change);
  e->num_waiters+= change;
  status = MSTRO_OK;

BAILOUT_UNLOCK_ENTRY:
  status = pthread_mutex_unlock(&e->mtx);
  if(status!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
BAILOUT:
  return status;
}

/** Increase the number of waiters*/
static inline
mstro_status
mstro_pool__demand_async_no_pm(mstro_cdo cdo, mstro_cdo_state new_state)
{

  assert(MSTRO_CDO_STATE_DEMANDED == new_state);
  // increase waiters by one
  return mstro_pool_cdo_waiters(cdo, 1);
}

bool mstro_test_local_demand(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);
  mstro_cdo cdo = request->cdo.cdo;
  bool result=false;
  char uuid_str[MSTRO_CDO_ID_STR_LEN];
  char *uuid = &uuid_str[0];

  mstro_status status = mstro_cdo_id__str(&cdo->gid, &uuid);
  if(status!=MSTRO_OK) {
    ERR("Cannot obtain id string of CDO\n");
    goto BAILOUT;
  }

  status = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to lock pool\n");
    goto BAILOUT;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status !=MSTRO_OK) {
    ERR("Failed to find CDO entry: %d\n");
  }

  /* quickly unlock table mutex */
  status=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to unlock pool mutex\n");
    goto BAILOUT;
  }

  /* check if we have entry */
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
            cdo->name, idstr););
    goto BAILOUT;
  }

  /* have locked entry.
   *
   * Since we're PM-less we need to find a local * source or go to
   * sleep until one comes in */
  size_t src=0;
    /* find source among handles */
    for(src=0; src<e->num_cdo_handles; src++) {
      if(mstro_cdo_state_check(e->cdo_handles[src],
                               MSTRO_CDO_STATE_OFFERED
                               | MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY)) {
        break;
      }
    }
    if(src<e->num_cdo_handles) {
      DEBUG("Found CDO at index %zu that can provide data\n", src);
      /* FIXME: it would be good to check whether someone else is
       * waiting at DEMAND for this CDO, and could copy it before we
       * pull it from the pool. However, e->num_waiters>=1 is not
       * sufficient to test this, since other (non-DEMAND) waiters
       * could exist. */
      goto HAVE_SOURCE;
    }


HAVE_SOURCE:
  /* have provider at src */
  assert(e->cdo_handles[src]!=cdo); /* can't happen in no-PM case */

  status = mstro_cdo__satisfy(e->cdo_handles[src], cdo);
  if(status!=MSTRO_OK) {
    ERR("Failed to satisfy DEMAND on %p from %p\n",
        cdo, e->cdo_handles[src]);
    goto BAILOUT_UNLOCK_ENTRY;
  }

  /* if applicable, transform incoming cdo so it matches demanded cdo requirements */
/*
  status = mstro_transform_layout(e->cdo_handles[src], cdo,
                                  e->cdo_handles[src]->attributes, cdo->attributes);
  if(status==MSTRO_NOMATCH) {
    DEBUG("No transformation matched\n");
  } else if(status!=MSTRO_OK) {
    ERR("Failed to transform %p to %p\n",
        e->cdo_handles[src], cdo);
    goto BAILOUT_UNLOCK_ENTRY;
  }
*/
  /* drop our own entry */
  status = mstro_pool_entry__drop(e, cdo);
  if(status==MSTRO_OK) {
    mstro_cdo_state_set(cdo, request->cdo.target_state);

    result = true;

    /* tell all who want to know. Note that this is only relevant for
     * non-DEMAND waiters, like WITHDRAWs that could not proceed, and
     * might now that we are gone */
    if(e->num_waiters>0) {
      status=pthread_cond_broadcast(& e->cvar);
      if(status!=0) {
        ERR("Failed to signal pool entry cvar\n");
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change\n",
                  e->num_waiters););
      }
    } else {
      DEBUG("No waiters on this entry\n");
    }
  }

BAILOUT_UNLOCK_ENTRY:
  status = pthread_mutex_unlock(&e->mtx);
  if(status!=0) {
    ERR("Failed to unlock pool entry\n");
    goto BAILOUT;
  }
BAILOUT:
  return result;
}


void mstro_wait_local_demand(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);
  mstro_cdo cdo = request->cdo.cdo;

  char uuid_str[MSTRO_CDO_ID_STR_LEN];
  char *uuid = &uuid_str[0];

  mstro_status status = mstro_cdo_id__str(&cdo->gid, &uuid);
  if(status!=MSTRO_OK) {
    ERR("Cannot obtain id string of CDO\n");
    goto BAILOUT;
  }

  status = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to lock pool\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo,&e);
  if(status !=MSTRO_OK) {
    ERR("Failed to find CDO entry: %d\n");
  }

  /** Decrease waiters, to equalize the added waiter in mstro_pool__demand_async_no_pm*/
  e->num_waiters--;
  /* quickly unlock table mutex */
  status=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(status!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* check if we have entry */
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
            cdo->name, idstr););
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* have locked entry.
   *
   * Since we're PM-less we need to find a local * source or go to
   * sleep until one comes in */
  size_t src=0;
  do {
    /* find source among handles */
    for(src=0; src<e->num_cdo_handles; src++) {
      if(mstro_cdo_state_check(e->cdo_handles[src],
                               MSTRO_CDO_STATE_OFFERED
                               | MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY)) {
        break;
      }
    }
    if(src<e->num_cdo_handles) {
      DEBUG("Found CDO at index %zu that can provide data\n", src);
      /* FIXME: it would be good to check whether someone else is
       * waiting at DEMAND for this CDO, and could copy it before we
       * pull it from the pool. However, e->num_waiters>=1 is not
       * sufficient to test this, since other (non-DEMAND) waiters
       * could exist. */
      goto HAVE_SOURCE;
    }
    /* wait for something to happen */
    e->num_waiters++;
    DEBUG("No suitable provider; wait on pool entry %p,"
          " CDO `%s' (id %s), %zu total waiters\n",
          e, cdo->name, uuid, e->num_waiters);
    mstro__abort_if_deadlock();
    int s = pthread_cond_wait(&e->cvar, & e->mtx);
    if(s!=0) {
      ERR("Failed in cond_wait for entry\n");
      status = MSTRO_FAIL;
      goto BAILOUT_UNLOCK_ENTRY;
    }
    /*raise the flag that we had to wait for the operation to complete */
    request->cdo.wait_flag = 1;
    e->num_waiters--;
    DEBUG("Woke up on entry %p,"
          " CDO `%s' (id %s), %zu total waiters (plus me), %zu handles\n",
          e, cdo->name, uuid,
          e->num_waiters, e->num_cdo_handles-e->num_waiters);
  } while(1);

HAVE_SOURCE:
  /* have provider at src */
  assert(e->cdo_handles[src]!=cdo); /* can't happen in no-PM case */

  status = mstro_cdo__satisfy(e->cdo_handles[src], cdo);

  if(status!=MSTRO_OK) {
    ERR("Failed to satisfy DEMAND on %p from %p\n",
        cdo, e->cdo_handles[src]);
    goto BAILOUT_UNLOCK_ENTRY;
  }

  /* if applicable, transform incoming cdo so it matches demanded cdo requirements */
/*
>>>>>>> devel
  status = mstro_transform_layout(e->cdo_handles[src], cdo,
                                  e->cdo_handles[src]->attributes, cdo->attributes);
  if(status==MSTRO_NOMATCH) {
    DEBUG("No transformation matched\n");
  } else if(status!=MSTRO_OK) {
    ERR("Failed to transform %p to %p\n",
        e->cdo_handles[src], cdo);
    goto BAILOUT_UNLOCK_ENTRY;
  }
*/
  /* drop our own entry */
  status = mstro_pool_entry__drop(e, cdo);
  if(status==MSTRO_OK) {
    mstro_cdo_state_set(cdo, request->cdo.target_state);

    /* tell all who want to know. Note that this is only relevant for
     * non-DEMAND waiters, like WITHDRAWs that could not proceed, and
     * might now that we are gone */
    if(e->num_waiters>0) {
      status=pthread_cond_broadcast(& e->cvar);
      if(status!=0) {
        ERR("Failed to signal pool entry cvar\n");
        status=MSTRO_FAIL;
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change\n",
                  e->num_waiters););
      }
    } else {
      DEBUG("No waiters on this entry\n");
    }
  }

BAILOUT_UNLOCK_ENTRY:
  status = pthread_mutex_unlock(&e->mtx);
  if(status!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
BAILOUT:
  return;
}

static inline
mstro_status
mstro_pool__demand_with_pm(mstro_cdo cdo, mstro_cdo_state new_state)
{
  mstro_status status = MSTRO_UNIMPL;

  char uuid_str[MSTRO_CDO_ID_STR_LEN];
  char *uuid = &uuid_str[0];

  status = mstro_cdo_id__str(&cdo->gid, &uuid);
  if(status!=MSTRO_OK) {
    ERR("Cannot obtain id string of CDO\n");
    goto BAILOUT;
  }

  /* tell pool manager it's urgent now */
  /* that will eventually kick off a transport occuring and filling in
   * a CDO OFFER that can be used to satisfy this demand. We'll be
   * notified by a wakeup on the pool entry for the CDO */

  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = cdo->gid.qw[0];
  cdoid.qw1 = cdo->gid.qw[1];
  cdoid.local_id = cdo->gid.local_id;

  Mstro__Pool__Demand demand = MSTRO__POOL__DEMAND__INIT;
  demand.cdoid = &cdoid;

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&demand);
  if(status!=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        demand.base.descriptor->name);
    goto BAILOUT;
  }

  mstro_cdo_state s = mstro_cdo_state_get(cdo);
  assert(s!=MSTRO_CDO_STATE_INVALID);

  mstro_cdo_state_set(cdo, s|MSTRO_CDO_STATE_IN_TRANSPORT);
  DEBUG("flagged cdo %p IN_TRANSPORT\n", cdo);

  status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
  if(status!=MSTRO_OK) {
    if(status==MSTRO_NO_PM) {
      ERR("PM went away unexpectedly\n");
      return MSTRO_FAIL;
    } else {
      ERR("Failed to send DEMAND message to pool manager: %d (%s)\n",
          status, mstro_status_description(status));
      return status;
    }
  }

  /* the pool manager will kick off a transfer if needed; in that case
   * eventually the transport layer will trigger a
   * mstro_cdo_transfer_complete call, setting the CDO state to include
   * OFFERED and SATISIFIED bits and stirring up all waiters on g_cdo_state_change_cvar */
  s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo, &e);

  /* quickly unlock table mutex */
  s=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* check if we have entry */
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
            cdo->name, idstr););
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* have locked entry. */
  do {
    if(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SATISFIED)) {
      DEBUG("Found CDO |%s| (id %s) to have completed incoming transport\n",
            cdo->name, uuid);
      break;
    } else {
      /* otherwise: wait for transport to get things done */
      e->num_waiters++;
      DEBUG("No suitable provider; wait on pool entry %p,"
            " CDO `%s' (id %s), %zu total waiters\n",
            e, cdo->name, uuid, e->num_waiters);
      mstro__abort_if_deadlock();
      int s = pthread_cond_wait(&e->cvar, & e->mtx);
      if(s!=0) {
        ERR("Failed in cond_wait for entry\n");
        status = MSTRO_FAIL;
        goto BAILOUT_UNLOCK_ENTRY;
      }
      e->num_waiters--;
      DEBUG("Woke up on entry %p,"
            " CDO `%s' (id %s), %zu total waiters (plus me), %zu handles\n",
            e, cdo->name, uuid,
            e->num_waiters, e->num_cdo_handles-e->num_waiters);
    }
  } while(1);

  DEBUG("CDO |%s| (id %s) filled by transport, no need to copy\n",
        cdo->name, uuid);

  /* transport does transformation, so we don't */
  DEBUG("Transported CDO will have been transformed by transport layer, so we don't\n");

  /* drop our own entry */
  status = mstro_pool_entry__drop(e,cdo);
  if(status==MSTRO_OK) {
    mstro_cdo_state_set(cdo, new_state);

    /* tell all who want to know. Note that this is only relevant for
     * non-DEMAND waiters, like WITHDRAWs that could not proceed, and
     * might now that we are gone */
    if(e->num_waiters>0) {
      s=pthread_cond_broadcast(& e->cvar);
      if(s!=0) {
        ERR("Failed to signal pool entry cvar\n");
        status=MSTRO_FAIL;
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change\n",
                  e->num_waiters););
      }
    } else {
      DEBUG("No waiters on this entry\n");
    }
  }

BAILOUT_UNLOCK_ENTRY:
  s = pthread_mutex_unlock(&e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
BAILOUT:
  return status;
}

static inline
mstro_status
mstro_pool__demand_async_with_pm(mstro_cdo cdo, mstro_cdo_state new_state)
{
  mstro_status status = MSTRO_UNIMPL;

  assert(MSTRO_CDO_STATE_DEMANDED == new_state);

  /* tell pool manager it's urgent now */
  /* that will eventually kick off a transport occuring and filling in
   * a CDO OFFER that can be used to satisfy this demand. We'll be
   * notified by a wakeup on the pool entry for the CDO */

   Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
   cdoid.qw0 = cdo->gid.qw[0];
   cdoid.qw1 = cdo->gid.qw[1];
   cdoid.local_id = cdo->gid.local_id;

  Mstro__Pool__Demand demand = MSTRO__POOL__DEMAND__INIT;
  demand.cdoid = &cdoid;

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&demand);
  if(status!=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        demand.base.descriptor->name);
    goto BAILOUT;
  }

  mstro_cdo_state s = mstro_cdo_state_get(cdo);
  assert(s!=MSTRO_CDO_STATE_INVALID);

  mstro_cdo_state_set(cdo, s|MSTRO_CDO_STATE_IN_TRANSPORT);
  DEBUG("flagged cdo %p IN_TRANSPORT\n", cdo);

  status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
  if(status!=MSTRO_OK) {
    if(status==MSTRO_NO_PM) {
      ERR("PM went away unexpectedly\n");
      return MSTRO_FAIL;
    } else {
      ERR("Failed to send DEMAND message to pool manager: %d (%s)\n",
          status, mstro_status_description(status));
      return status;
    }
  }

  /** increase the number of waiters for this cdo */
  status = mstro_pool_cdo_waiters(cdo, 1);

  BAILOUT:
  return status;

}

/** Stage 2 of demand_async_with_pm. Does not really wait ...just completes the demand operation*/
void mstro_cdo_wait_demand_async_with_pm(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);
  
  char uuid_str[MSTRO_CDO_ID_STR_LEN];
  char *uuid = &uuid_str[0];

  mstro_cdo cdo = request->cdo.cdo;
  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = cdo->gid.qw[0];
  cdoid.qw1 = cdo->gid.qw[1];
  cdoid.local_id = cdo->gid.local_id;
  mstro_status status;

  status = mstro_cdo_id__str(&cdo->gid, &uuid);
  if(status!=MSTRO_OK) {
    ERR("Cannot obtain id string of CDO\n");
    goto BAILOUT;
  }

  mstro_cdo_state s = mstro_cdo_state_get(cdo);

  DEBUG( "CDO state: %s \n", mstro_cdo_state_describe(s));
  assert(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SATISFIED));
  /* the pool manager will kick off a transfer if needed; in that case
   * eventually the transport layer will trigger a
   * mstro_cdo_transfer_complete call, setting the CDO state to include
   * OFFERED and SATISIFIED bits and stirring up all waiters on g_cdo_state_change_cvar */
  s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  struct mstro_pool_entry *e=NULL;
  status = mstro_pool__find_entry_and_lock(cdo, &e);

  /* quickly unlock table mutex */
  s=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* check if we have entry */
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to obtain pool entry for CDO %s (id %s)\n",
            cdo->name, idstr););
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /** Decrease waiters */
  //e->num_waiters--;

  DEBUG("CDO |%s| (id %s) filled by transport, no need to copy\n",
        cdo->name, uuid);

  /* transport does transformation, so we don't */
  DEBUG("Transported CDO will have been transformed by transport layer, so we don't\n");

  /* drop our own entry */
  status = mstro_pool_entry__drop(e,cdo);
  if(status==MSTRO_OK) {
    mstro_cdo_state_set(cdo, request->cdo.target_state);

    /* tell all who want to know. Note that this is only relevant for
     * non-DEMAND waiters, like WITHDRAWs that could not proceed, and
     * might now that we are gone */
    if(e->num_waiters>0) {
      s=pthread_cond_broadcast(& e->cvar);
      if(s!=0) {
        ERR("Failed to signal pool entry cvar\n");
        status=MSTRO_FAIL;
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change\n",
                  e->num_waiters););
      }
    } else {
      DEBUG("No waiters on this entry\n");
    }
  }

BAILOUT_UNLOCK_ENTRY:
  s = pthread_mutex_unlock(&e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
BAILOUT:
  return ;
}


/** Demand CDO from pool */
mstro_status
mstro_pool__demand(mstro_cdo cdo, mstro_cdo_state new_state)
{
  int s=0;
  struct mstro_pool_entry *e=NULL;
  size_t src=0;

  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  assert(new_state == MSTRO_CDO_STATE_DEMANDED);


  mstro_status status;
  if(atomic_load(&g_mstro_pm_attached)) {
    status = mstro_pool__demand_with_pm(cdo, new_state);
  } else {
    status = mstro_pool__demand_no_pm(cdo, new_state);
  }
  return status;
}


/** Demand CDO from pool async*/
mstro_status
mstro_pool__demand_async(mstro_cdo cdo, mstro_cdo_state new_state)
{
  int s=0;
  struct mstro_pool_entry *e=NULL;
  size_t src=0;

  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  assert(new_state == MSTRO_CDO_STATE_DEMANDED);

  if(atomic_load(&g_mstro_pm_attached)) {
    return mstro_pool__demand_async_with_pm(cdo, new_state);
  } else {
    return mstro_pool__demand_async_no_pm(cdo, new_state);
  }
}


/** Retract CDO  op from local pool */
mstro_status
mstro_pool__retract(mstro_cdo cdo, mstro_cdo_state new_state)
{
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  assert(new_state == MSTRO_CDO_STATE_RETRACTED);

  /* all gory details in the remove function */
  return mstro_pool__remove(cdo, new_state);
}

mstro_status
mstro_pool__find_cdo_with_local_id( const struct mstro_cdo_id *cdoid, mstro_cdo *result) {
  mstro_status status=MSTRO_UNIMPL;

  if(cdoid==NULL) {
    ERR("NULL CDOID\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("NULL output destination\n");
    return MSTRO_INVOUT;
  }

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry* e = NULL;

  struct mstro_cdo_id head = *cdoid;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  HASH_FIND(hh, g_mstro_pool_local,
            &head, sizeof(struct mstro_cdo_id),
            e);

  if(e==NULL) {
    WITH_CDO_ID_STR(idstr, &head,
                    ERR("No entry found in local pool for CDO gid %s\n", idstr););
    status=MSTRO_NOENT;
    goto BAILOUT_UNLOCK;
  }

  size_t i;
  for(i=0; i<e->num_cdo_handles; i++) {
    WITH_CDO_ID_STR(idstr, &e->cdo_handles[i]->gid,
                    DEBUG("Inspecting %s, state %s\n",
                          idstr, mstro_cdo_state_describe(e->cdo_handles[i]->state)););
    if(e->cdo_handles[i]->id.local_id == cdoid->local_id) {              
      break;
    }
  }
  if(i<e->num_cdo_handles) {
    DEBUG("Found CDO at index %zu that can provide data\n", i);
    *result = e->cdo_handles[i];
    status = MSTRO_OK;
  } else {
    WITH_CDO_ID_STR(idstr, cdoid, {
        ERR("CDO %s entry has no source handle available with local-id %zu \n",
            idstr, cdoid->local_id);});
    status = MSTRO_NOENT;
    *result = NULL;
  }

BAILOUT_UNLOCK:
  s=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
  }

  return status;
}


mstro_status
mstro_pool__find_source_cdo(
    const struct mstro_cdo_id *cdoid,
    const Mstro__Pool__Attributes *desired_attributes,
    mstro_cdo *result)
{
  mstro_status status=MSTRO_OK;
  const Mstro__Pool__Attributes *dummy =  desired_attributes; /* silence unused arg warning */
  if(cdoid==NULL) {
    ERR("NULL CDOID\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("NULL output destination\n");
    return MSTRO_INVOUT;
  }

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry* e = NULL;

  struct mstro_cdo_id head = *cdoid;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  HASH_FIND(hh, g_mstro_pool_local,
            &head, sizeof(struct mstro_cdo_id),
            e);

  if(e==NULL) {
    WITH_CDO_ID_STR(idstr, &head,
                    ERR("No entry found in local pool for CDO gid %s\n", idstr););
    status=MSTRO_NOENT;
    goto BAILOUT_UNLOCK;
  }

  size_t i;
  for(i=0; i<e->num_cdo_handles; i++) {
    WITH_CDO_ID_STR(idstr, &e->cdo_handles[i]->gid,
                    DEBUG("Inspecting %s, state %s\n",
                          idstr, mstro_cdo_state_describe(e->cdo_handles[i]->state)););
    if(mstro_cdo_state_check(e->cdo_handles[i],
                            // FIXME: why do we rely on SEALED being acceptable to serve a CDO?
                             MSTRO_CDO_STATE_OFFERED|MSTRO_CDO_STATE_OFFERED_LOCALLY|MSTRO_CDO_STATE_SEALED))
      break;
  }
  if(i<e->num_cdo_handles) {
    DEBUG("Found CDO at index %zu that can provide data\n", i);
    *result = e->cdo_handles[i];
  } else {
    WITH_CDO_ID_STR(idstr, cdoid, {
        ERR("CDO %s entry has no source handle available (in OFFERED state)\n",
            idstr);});
    status = MSTRO_NOENT;
    *result = NULL;
  }

BAILOUT_UNLOCK:
  s=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
  }

  return status;
}

/** Identify a local CDO that can absorb an incoming CDOID */
mstro_status
mstro_pool__find_sink_cdo(
    const struct mstro_cdo_id *cdoid,
    const Mstro__Pool__Attributes *available_attributes,
    mstro_cdo *result)
{
  mstro_status status=MSTRO_OK;
  const Mstro__Pool__Attributes *dummy = available_attributes; /* silence unused arg warning */
  if(cdoid==NULL) {
    ERR("NULL CDOID\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("NULL output destination\n");
    return MSTRO_INVOUT;
  }

  int s = pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  struct mstro_pool_entry* e = NULL;


  struct mstro_cdo_id head = *cdoid;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  HASH_FIND(hh, g_mstro_pool_local,
            &head, sizeof(struct mstro_cdo_id),
            e);

  if(e==NULL) {
    WITH_CDO_ID_STR(idstr, &head,
                    ERR("No entry found in local pool for CDO gid %s\n", idstr););
    status=MSTRO_NOENT;
    goto BAILOUT_UNLOCK;
  }

  size_t i;
  for(i=0; i<e->num_cdo_handles; i++) {
    if(mstro_cdo_id__equal(&e->cdo_handles[i]->gid, cdoid)
       && mstro_cdo_state_check(e->cdo_handles[i],
                                MSTRO_CDO_STATE_REQUIRED
                                |MSTRO_CDO_STATE_DEMANDED))
      break;
  }
  if(i<e->num_cdo_handles) {
    DEBUG("Found CDO at index %zu that can absorb data\n", i);
    *result = e->cdo_handles[i];
  } else {
    ERR ("CDO entry has no sink handle available\n");
    status = MSTRO_NOENT;
    *result = NULL;
  }

BAILOUT_UNLOCK:
  s=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool mutex\n");
    status = MSTRO_FAIL;
  }

  return status;
}


mstro_status
mstro_pool__notify(mstro_cdo cdo)
{
  if(cdo==NULL)
    return MSTRO_INVARG;

  mstro_status status=MSTRO_OK;
  int s;

  struct mstro_pool_entry *e;
  s=pthread_mutex_lock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to lock pool\n");
    return MSTRO_FAIL;
  }

  status = mstro_pool__find_entry_and_lock(cdo, &e);

  s=pthread_mutex_unlock(&g_mstro_pool_local_mtx);
  if(s!=0) {
    ERR("Failed to unlock pool\n");
    abort();
  }

  if(status!=MSTRO_OK) {
    ERR("Failure locating CDO entry\n");
    return status;
  }

  if(e->cdo_handles==NULL) {
    WARN("CDO not in pool, nothing to do\n"); // this should not really happen
    status = MSTRO_OK;
  } else {
    if(e->num_waiters>0) {
      s=pthread_cond_broadcast(& e->cvar);
      if(s!=0) {
        ERR("Failed to signal pool entry cvar\n");
        status=MSTRO_FAIL;
      } else {
        WITH_CDO_ID_STR(
            idstr, & e->cdoid,
            DEBUG("Signaled %zu waiters about pool entry change\n", e->num_waiters););
      }
    } else {
      DEBUG("No waiters on this entry\n");
      status = MSTRO_OK;
    }
  }
  
  s = pthread_mutex_unlock(&e->mtx);
  if(s!=0) {
    ERR("Failed to unlock pool entry\n");
    status = MSTRO_FAIL;
  } else {
    status = MSTRO_OK;
  }

  return status;

}



/* subscription machinery */

/** lookup KEY in SCHEMA, possibly trying namespace-qualified form if
 * needed. Returns attribute object for KEY in *attr. If
 * namespace-qualified form was needed, returns the string for that
 * through *new_key, otherwise that will be NULL */
static inline
mstro_status
mstro__csq_lookup(mstro_schema schema,
                  const char *namespace,
                  const char *key,
                  char **new_key,
                  mstro_schema_attribute *attr)
{
  assert(schema!=NULL); assert(key!=NULL);
  assert(new_key!=NULL); assert(attr!=NULL);

  *new_key = NULL;

  mstro_status s = mstro_schema_lookup_attribute(schema, key, attr);
  if(s==MSTRO_NOENT
     && key[0]!='.'
     && namespace!=NULL) {
    /* try with qualified name */
    size_t nslen = strlen(namespace);
    char *tmpname = malloc(nslen+strlen(key)+2);
    if(tmpname==NULL) {
      ERR("Failed to allocate temp name\n");
      return MSTRO_NOMEM;
    }
    strcpy(tmpname, namespace);
    if(tmpname[nslen]!='.')
      tmpname[nslen++]='.';
    strcpy(tmpname+nslen, key);
    mstro_status s = mstro_schema_lookup_attribute(schema, tmpname, attr);
    if(s==MSTRO_OK) {
      *new_key = tmpname;
    } else {
      free(tmpname);
    }
  }
  return s;
}

static inline
mstro_status
mstro__csq_validate(struct mstro_csq_val *csq,
                    mstro_schema schema,
                    const char *namespace)
{
  /* iterate through csq tree
   *  - replacing identifiers by symbols on the way
   *  - parsing values according into appropriate types
   *
   * Modifies csq so that it can be used for @ref mstro_subscription__csq_eval calls afterwards
   */
  DEBUG("validating csq %p, schema %p, namespace %p\n", csq, schema, namespace);

  if(csq==NULL)
    return MSTRO_OK;

  switch(csq->kind) {
    case MSTRO_CSQ_ERROR:
      ERR("Illegal csq kind: Error token\n");
      return MSTRO_FAIL;

    case MSTRO_CSQ_STRING:
      ERR("Illegal csq kind: String token\n");
      return MSTRO_FAIL;

    case MSTRO_CSQ_OR: {
      struct mstro_csq_val *tmp;
      mstro_status s=MSTRO_OK;
      LL_FOREACH(csq->or.clauses, tmp) {
        DEBUG("Checking OR term %p\n", tmp);
        s |= mstro__csq_validate(tmp, schema, namespace);
        DEBUG("Done with OR term %p\n", tmp);

      }
      DEBUG("Returning %d from OR\n", s);
      return s;
    }

    case MSTRO_CSQ_AND: {
      struct mstro_csq_val *tmp;
      mstro_status s=MSTRO_OK;
      LL_FOREACH(csq->and.terms, tmp) {
        s |= mstro__csq_validate(tmp, schema, namespace);
      }
      return s;
    }

    case MSTRO_CSQ_OP_HAS: {
      /* translate key wrt. schema */
      assert(csq->has.key!=NULL);
      mstro_schema_attribute attr=NULL;
      char *tmp=NULL;
      mstro_status s = mstro__csq_lookup(schema, namespace,
                                         csq->has.key,
                                         &tmp, &attr);
      if(tmp!=NULL) {
        free(csq->has.key);
        csq->has.key=tmp;
      }
      /* now either the plain key, or both key and namespace-qualified key have been tried */
      if(s!=MSTRO_OK) {
        ERR("Attribute name %s: %s (tried namespace %s too)\n",
            csq->has.key, s==MSTRO_NOENT? "not found" : "lookup failed",
            namespace? namespace : "(null)");
        return MSTRO_FAIL;
      }
      assert(attr!=NULL);
      csq->has.kattr = attr;
      return MSTRO_OK;
    }

    case MSTRO_CSQ_OP_LT:  /* fallthrough */
    case MSTRO_CSQ_OP_LE:  /* fallthrough */
    case MSTRO_CSQ_OP_GT:  /* fallthrough */
    case MSTRO_CSQ_OP_GE:  /* fallthrough */
    case MSTRO_CSQ_OP_EQ:  /* fallthrough */
    case MSTRO_CSQ_OP_NEQ: {
      /* translate lhs wrt. schema, rhs wrt. lhs type */
      assert(csq->kv_op.lhs!=NULL);
      assert(csq->kv_op.rhs!=NULL);
      mstro_schema_attribute attr=NULL;
      char *tmp=NULL;
      mstro_status s = mstro__csq_lookup(schema, namespace,
                                         csq->kv_op.lhs,
                                         &tmp, &attr);
      if(tmp!=NULL) {
        free(csq->kv_op.lhs);
        csq->kv_op.lhs=tmp;
      }
      /* now either the plain key, or both key and namespace-qualified key have been tried */
      if(s!=MSTRO_OK) {
        ERR("Attribute name %s: %s (tried namespace %s too)\n",
            csq->kv_op.lhs, s==MSTRO_NOENT? "not found" : "lookup failed",
            namespace? namespace : "(null)");
        return MSTRO_FAIL;
      }
      assert(attr!=NULL);
      csq->kv_op.lhs_attr = attr;

      // comparisons will later use string value of rhs, so enough to leave it as is
      csq->kv_op.rhs = csq->kv_op.rhs;

      return MSTRO_OK;
    }

    case MSTRO_CSQ_OP_RMATCH: /* fallthrough */
    case MSTRO_CSQ_OP_RMATCH_ICASE: {
      /* translate lhs wrt. schema, build regexp from rhs */
      assert(csq->has.key!=NULL);
      mstro_schema_attribute attr=NULL;
      char *tmp=NULL;
      mstro_status s = mstro__csq_lookup(schema, namespace,
                                         csq->kv_op.lhs,
                                         &tmp, &attr);
      if(tmp!=NULL) {
        free(csq->kv_op.lhs);
        csq->kv_op.lhs=tmp;
      }
      /* now either the plain key, or both key and namespace-qualified key have been tried */
      if(s!=MSTRO_OK) {
        ERR("Attribute name %s: %s (tried namespace %s too)\n",
            csq->kv_op.lhs, s==MSTRO_NOENT? "not found" : "lookup failed",
            namespace? namespace : "(null)");
        return MSTRO_FAIL;
      }
      assert(attr!=NULL);
      csq->kv_op.lhs_attr = attr;
      ERR("unimplemented: value pre-parse\n");
      return MSTRO_UNIMPL;
    }
    default:
      ERR("Unexpected csq value kind: %d\n");
      return MSTRO_FAIL;
  }
  return MSTRO_OK;
}

mstro_status
mstro_cdo_selector_create(mstro_schema schema,
                          const char *namespace,
                          const char *query,
                          mstro_cdo_selector *result)
{
  mstro_status status = MSTRO_UNIMPL;

  if(result==NULL) {
    ERR("Invalid result destination\n");
    return MSTRO_INVOUT;
  }

  /* parse query */
  struct mstro_csq_val *csq=NULL;
  status=mstro_selector_parse(query, &csq);
  if(status!=MSTRO_OK) {
    ERR("Failed to parse query\n");
    return MSTRO_FAIL;
  }

  if(schema==NULL) {
    DEBUG("Using default schema for CDO selector\n");
    schema = g_mstro_core_schema_instance;
  }

  status = mstro__csq_validate(csq, schema, namespace);
  if(status!=MSTRO_OK) {
    ERR("Failed to validate query\n");
    mstro_csq_val_dispose(csq);
    return MSTRO_FAIL;
  } else {
    DEBUG("Validated csq\n");
  }

  *result = malloc(sizeof(struct mstro_cdo_selector_));
  if(*result) {
    (*result)->refcount=1;
    (*result)->schema = schema;
    (*result)->query = query ? strdup(query) : strdup("");
    if((*result)->query==NULL) {
      free(*result);
      *result=NULL;
      status=MSTRO_NOMEM;
      goto BAILOUT;
    }
    (*result)->namespace = namespace ? strdup(namespace) : NULL;
    if((*result)->namespace == NULL && namespace!=NULL) {
      free((*result)->query);
      free(*result);
      *result=NULL;
      status=MSTRO_NOMEM;
      goto BAILOUT;
    }

    (*result)->csq = csq;

    status=MSTRO_OK;
  }

BAILOUT:
  return status;
}


mstro_status
mstro_cdo_selector_dispose(mstro_cdo_selector s)
{
  if(s==NULL) {
    ERR("NULL selector\n");
    return MSTRO_INVARG;
  }
  if(s->refcount==0) {
    ERR("refcount already 0\n");
    return MSTRO_INVARG;
  }

  s->refcount--;
  if(s->refcount == 0) {
    if(s->query!=NULL)
      free(s->query);
    if(s->namespace)
      free(s->namespace);
    if(s->csq)
      mstro_csq_val_dispose(s->csq);
    free(s);
  }

  return MSTRO_OK;
}


const char*
mstro_pool_event_description(enum mstro_pool_event_kind event)
{
  if(popcount(event)>1) {
    return "invalid";
  }
  const char *res;
  switch(event) {
    case MSTRO_POOL_EVENT_NONE :      res = "(empty)";     break;
    case MSTRO_POOL_EVENT_DECLARE:    res = "DECLARE";     break;
    case MSTRO_POOL_EVENT_DECLARE_ACK:res = "DECLARE-ACK"; break;
    case MSTRO_POOL_EVENT_SEAL:       res = "SEAL";        break;
    case MSTRO_POOL_EVENT_SEAL_GROUP: res = "SEAL_GROUP";  break;
    case MSTRO_POOL_EVENT_OFFER:      res = "OFFER";       break;
    case MSTRO_POOL_EVENT_REQUIRE:    res = "REQUIRE";     break;
    case MSTRO_POOL_EVENT_WITHDRAW:   res = "WITHDRAW";    break;
    case MSTRO_POOL_EVENT_DEMAND:     res = "DEMAND";      break;
    case MSTRO_POOL_EVENT_RETRACT:    res = "RETRACT";     break;
    case MSTRO_POOL_EVENT_DISPOSE:    res = "DISPOSE";     break;

    case MSTRO_POOL_EVENT_TRANSPORT_INIT:      res="TRANSPORT-INIT";     break;
    case MSTRO_POOL_EVENT_TRANSPORT_TICKET:    res="TRANSPORT-TICKET";   break;
    case MSTRO_POOL_EVENT_TRANSPORT_COMPLETED: res="TRANSPORT-COMPLETED";break;

      /* pool-related */
    case MSTRO_POOL_EVENT_APP_JOIN:        res = "JOIN";       break;
    case MSTRO_POOL_EVENT_APP_WELCOME:     res = "WELCOME";    break;
    case MSTRO_POOL_EVENT_APP_LEAVE:       res = "LEAVE";      break;
    case MSTRO_POOL_EVENT_APP_BYE:         res = "BYE";        break;
    case MSTRO_POOL_EVENT_POOL_CHECKPOINT: res = "CHECKPOINT"; break;
    case MSTRO_POOL_EVENT_SUBSCRIBE:       res = "SUBSCRIBE";  break;
    case MSTRO_POOL_EVENT_UNSUBSCRIBE:     res = "UNSUBSCRIBE"; break;
    default:
      res = "undefined"; break;
  }
  return res;
}

mstro_status
mstro_subscribe(mstro_cdo_selector cdos,
                mstro_pool_event_kinds events,
                enum mstro_subscription_opts flags,
                mstro_subscription *res)
{
  mstro_status status=MSTRO_UNIMPL;

  if(cdos==NULL) {
    DEBUG("NULL CDO selector, using match-all\n");
  }

  if(res==NULL) {
    ERR("result destination invalid\n");
    return MSTRO_INVOUT;
  }

  mstro_subscription s=malloc(sizeof(struct mstro_subscription_));
  if(!s) {
    ERR("Failed to allocate subscription\n");
    return MSTRO_NOMEM;
  }

  int err = pthread_mutex_init(&s->event_mtx, NULL);
  if(err!=0) {
    ERR("Failed to initialize mutex: %d (%s)\n", err, strerror(err));
    free(s);
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

  err = pthread_cond_init(&s->event_cond, NULL);
  if(err!=0) {
    ERR("Failed to initialize cond var: %d (%s)\n", err, strerror(err));
    pthread_mutex_destroy(&s->event_mtx);
    free(s);
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

  s->refcount=1;
  mstro__pool__subscription_handle__init(&s->handle);
  s->cdo_selector = cdos;
  if(cdos)
    s->cdo_selector->refcount++;
  s->event_mask = events;
  s->needs_ack = flags & MSTRO_SUBSCRIPTION_OPTS_REQUIRE_ACK;
  s->no_resolve = flags & MSTRO_SUBSCRIPTION_OPTS_NO_RESOLVE;
  s->signal_before = flags & MSTRO_SUBSCRIPTION_OPTS_SIGNAL_BEFORE;
  s->no_local = flags & MSTRO_SUBSCRIPTION_OPTS_NO_LOCAL_EVENTS;
  s->event_list = NULL;
  s->last_event = NULL;

  status = mstro_subscription_register(s);
  if(status!=MSTRO_OK) {
    ERR("Failed to register subscription\n");
    pthread_cond_destroy(&s->event_cond);
    pthread_mutex_destroy(&s->event_mtx);
    free(s);
    goto BAILOUT;
  }

  *res = s;
  status=MSTRO_OK;

BAILOUT:
  return status;
}


mstro_status
mstro_subscription_wait(mstro_subscription s, mstro_pool_event *event)
{
  mstro_status status = MSTRO_OK;

  if(s==NULL)
    return MSTRO_INVARG;
  if(event==NULL)
    return MSTRO_INVOUT;

  int err = pthread_mutex_lock(&s->event_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription event mutex: %d (%s)\n", err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  while(s->event_list==NULL) {
    /* need to wait */
    mstro__abort_if_deadlock();
    err=pthread_cond_wait(&s->event_cond, &s->event_mtx);
    if(err!=0) {
      ERR("Error on cond wait of subscription: %d (%s)\n", err, strerror(err));
      status = MSTRO_FAIL;
      goto BAILOUT_UNLOCK;
    }
  }

  *event = s->event_list;
  s->event_list = NULL;
  s->last_event = NULL;

BAILOUT_UNLOCK:
  err=pthread_mutex_unlock(&s->event_mtx);
  if(err!=0) {
    ERR("Failed to unlock subscription event mutex: %d (%s)\n", err, strerror(err));
    status=MSTRO_FAIL;
  }

BAILOUT:
  return status;
}

mstro_status
mstro_subscription_timedwait(mstro_subscription s, const struct timespec *abstime, mstro_pool_event *event)
{
  mstro_status status = MSTRO_OK;

  if(s==NULL)
    return MSTRO_INVARG;
  if(event==NULL)
    return MSTRO_INVOUT;

  int err = pthread_mutex_lock(&s->event_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription event mutex: %d (%s)\n", err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  while(s->event_list==NULL) {
    /* need to wait */
    err=pthread_cond_timedwait(&s->event_cond, &s->event_mtx, abstime);
    if(err==ETIMEDOUT) {
      status = MSTRO_TIMEOUT;
      *event=NULL;
      goto BAILOUT_UNLOCK;
    }
    if(err!=0) {
      ERR("Error on cond wait of subscription: %d (%s)\n", err, strerror(err));
      status = MSTRO_FAIL;
      goto BAILOUT_UNLOCK;
    }
  }

  *event = s->event_list;
  s->event_list = NULL;
  s->last_event = NULL;

BAILOUT_UNLOCK:
  err=pthread_mutex_unlock(&s->event_mtx);
  if(err!=0) {
    ERR("Failed to unlock subscription event mutex: %d (%s)\n", err, strerror(err));
    status=MSTRO_FAIL;
  }

BAILOUT:
  return status;
}

mstro_status
mstro_subscription_poll(mstro_subscription s, mstro_pool_event *event)
{
  mstro_status status=MSTRO_OK;

  if(s==NULL) {
    ERR("NULL subscription\n");
    return MSTRO_INVARG;
  }
  if(event==NULL) {
    ERR("NULL event storage destination slot\n");
    return MSTRO_INVOUT;
  }

  int err = pthread_mutex_lock(&s->event_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription event mutex: %d (%s)\n",
        err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  if(s->event_list!=NULL) {
    *event = s->event_list;
    s->event_list = NULL;
    s->last_event = NULL;
  } else {
    *event = NULL;
  }

  size_t ll=0;
  mstro_pool_event tmp;
  LL_COUNT(*event, tmp, ll);
  NOISE("poll on subscription %" PRIu64 " returns %zu events\n",
        s->handle.id, ll);

  err=pthread_mutex_unlock(&s->event_mtx);
  if(err!=0) {
    ERR("Failed to unlock subscription event mutex: %d (%s)\n",
        err, strerror(err));
    status=MSTRO_FAIL;
  }
BAILOUT:
  NOISE("Returning status %d (%s)\n", status, mstro_status_description(status));
  return status;
}


mstro_status
mstro_subscription_ack(mstro_subscription s, mstro_pool_event event)
{
  if(s==NULL || event==NULL)
    return MSTRO_INVARG;
  if(! s->needs_ack) {
    ERR("Subscription %p not set up for ACK\n", s);
    return MSTRO_INVARG;
  }
  /* now send an ack message */
  return mstro_subscription_event_ack(s, event);
}


mstro_status
mstro_pool_event_dispose_single(mstro_pool_event e)
{
  if(e==NULL) {
    ERR("NULL event\n");
    return MSTRO_INVARG;
  }

  switch(e->kind) {
    case MSTRO_POOL_EVENT_NONE:
      break;
      /* cdo-related */
    case MSTRO_POOL_EVENT_DECLARE:
      free(e->payload.declare.cdo_name);
      break;
    case MSTRO_POOL_EVENT_DECLARE_ACK:
      free(e->payload.declare_ack.cdo_name);
      break;
    case MSTRO_POOL_EVENT_SEAL:
      free(e->payload.seal.cdo_name);
      break;
    case MSTRO_POOL_EVENT_SEAL_GROUP:
      break;
    case MSTRO_POOL_EVENT_OFFER:
      free(e->payload.offer.cdo_name);
      break;
    case MSTRO_POOL_EVENT_REQUIRE:
      free(e->payload.require.cdo_name);
      break;
    case MSTRO_POOL_EVENT_WITHDRAW:
      free(e->payload.withdraw.cdo_name);
      break;
    case MSTRO_POOL_EVENT_DEMAND:
      free(e->payload.demand.cdo_name);
      break;
    case MSTRO_POOL_EVENT_RETRACT:
      free(e->payload.retract.cdo_name);
      break;
    case MSTRO_POOL_EVENT_DISPOSE:
      free(e->payload.dispose.cdo_name);
      break;
    case MSTRO_POOL_EVENT_TRANSPORT_INIT:
      free(e->payload.transport_init.cdo_name);
      break;
    case MSTRO_POOL_EVENT_TRANSPORT_TICKET:
      free(e->payload.transport_ticket.cdo_name);
      break;
    case MSTRO_POOL_EVENT_TRANSPORT_COMPLETED:
      free(e->payload.transport_completed.cdo_name);
      break;
      /* pool-related */
    case MSTRO_POOL_EVENT_APP_JOIN:
      free(e->payload.join.component_name);
      break;
    case MSTRO_POOL_EVENT_APP_WELCOME:
      free(e->payload.welcome.component_name);
      break;
    case MSTRO_POOL_EVENT_APP_LEAVE:
      free(e->payload.leave.component_name);
      break;
    case MSTRO_POOL_EVENT_APP_BYE:
      break;
    case MSTRO_POOL_EVENT_POOL_CHECKPOINT:
      free(e->payload.pool_checkpoint.dummy);
      break;
      
    case MSTRO_POOL_EVENT_SUBSCRIBE: /* fallthrough */
    case MSTRO_POOL_EVENT_UNSUBSCRIBE:
    case MSTRO_POOL_EVENT_ALL:
      ERR("Unhandled event kind: %d\n", e->kind);
      break;
  }
  free(e);
  return MSTRO_OK;
}

mstro_status
mstro_pool_event_dispose(mstro_pool_event e)
{
  if(e==NULL) {
    ERR("NULL event\n");
    return MSTRO_INVARG;
  }

  /* capture 'later events' list of list for tail recursion */
  mstro_pool_event tail = e->next;

  mstro_status s = mstro_pool_event_dispose_single(e);
  if (s != MSTRO_OK)
    return s;

  if(tail)
    return mstro_pool_event_dispose(tail);
  else
    return MSTRO_OK;
}

mstro_status
mstro_subscription_dispose(mstro_subscription s)
{
  if(s==NULL)
    return MSTRO_INVARG;

  assert(s->refcount>0);

  mstro_status status=MSTRO_OK;

  WARN("Not signaling PM\n");

  if(atomic_fetch_sub(& s->refcount, 1)==1) {
    /* we were the last, free structure (no locking needed) */
    WARN("Not acknowledging unhandled events (should be done in PM when subscription is canceled)\n");
    struct mstro_pool_event_ *e,*tmp;
    LL_FOREACH_SAFE(s->event_list, e,tmp) {
      status|=mstro_pool_event_dispose(e);
    }
    pthread_mutex_destroy(&s->event_mtx);
    pthread_cond_destroy(&s->event_cond);
    free(s);
  }
  return status;
}


#undef  DO_DEADLOCK_DETECTION

void
mstro__abort_if_deadlock_imminent(const char *func, const char* file, int line)
{
#ifdef DO_DEADLOCK_DETECTION
  extern struct erl_thread_team * g_pm_ofi_team;
  if(erl_thread_team_memberp(g_pm_ofi_team,pthread_self())) {
    ERR("DEADLOCK detected: Trying to wait for CDO state change from a CQ team thread at %s:%s:%d\n",
        func, file, line);
    abort();
  } 
  const char *tid=pthread_getspecific(g_thread_descriptor_key);
  if(tid!=NULL && strcmp(tid,"TI")==0) {
    ERR("DEADLOCK detected: Trying to wait for CDO state change from TI thread: %s:%s:%d\n",
        func, file, line);
    abort();
  }
#endif
}
