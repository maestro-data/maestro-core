/* -*- mode:c -*- */
/** @file
 ** @brief thread team implementation
 **
 ** A thread team is a set of N threads that have an incoming dequeue
 ** each, and run a handler on each queue.
 **/

/*
 * Copyright (C) 2021 HPE Switzerland GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* FIXME: should move to libERL */
#ifndef I_ERL_THREAD_TEAM_H_
#define I_ERL_THREAD_TEAM_H_ 1

#include <stdlib.h>
#include "maestro/i_fifo.h"

/** structure encapsulating message handler closure */
struct erl_thread_team_ctx {
  char *tidprefix;  /**< thread ID prefix */
  int numa_node;    /**< thread NUMA domain target; -1 for don't care */
  size_t id;        /**< unique index */
  mstro_fifo queue; /**< queue to pull messages from */
  /** handler function to execute on messages */
  mstro_status(*item_handler)(void *item);
  /** destructor to call after handler on each processed item */
  void(*item_destructor)(void *item);
};


/** Info for one message handler thread */
struct erl_thread_team_member {
  pthread_t t;
  struct erl_thread_team_ctx ctx;
};

/** user-facing team structure */
struct erl_thread_team {
  char *teamname;
  /** we do round-robin insertion currently */
  _Atomic(size_t) next_target;
  size_t num_members;
  struct erl_thread_team_member members[];
};



/** start @arg num_threads instances of @arg msg_handler in separate
 * mstro_msh_handler_threadfun threads.
 *
 * Returns a threadinfo array via @arg *ti_p
 */
mstro_status
erl_thread_team_create(
    const char *team_name,
    const char *tidprefix,
    size_t num_threads,
    mstro_status(*item_handler)(void *item),
    void(*item_destructor)(void *item),
    struct erl_thread_team **t_p);

/**check thread pinning and numa binding, update thread context with the correct numa node*/
mstro_status
erl_thread_team_bind_and_update_numa(struct erl_thread_team_ctx *ctx);

/**Convenience function, to pass a specific start function to this thread team instead of the default erl_thread_team_threadfun*/
mstro_status
erl_thread_team_create_start_func(
    const char *team_name,
    const char *tidprefix,
    size_t num_threads,
    mstro_status(*item_handler)(void *item),
    void(*item_destructor)(void *item),
    void *(*start_routine)(void *),
    struct erl_thread_team **t_p);

/** stop threads in @arg ti_p, deallocate resources in @arg ti */
mstro_status
erl_thread_team_stop_and_destroy(struct erl_thread_team *team);

/**Convenience function to be called for pm/pc threads, with not fully populated contexts (no fifo queues)*/
mstro_status
erl_thread_team_stop_and_destroy_bl(struct erl_thread_team *t);

/** insert an item into the team's work schedule */
mstro_status
erl_thread_team_enqueue(struct erl_thread_team *team, void *item);

/** insert an item into the team's work schedule - try select a thread that is on the same numa to improve locality */
mstro_status
erl_thread_team_enqueue_on_numa(struct erl_thread_team *team, int target_numa, void *item);

/** check if @arg thread_id is a member if @arg team */
bool
erl_thread_team_memberp(const struct erl_thread_team *team, pthread_t thread_id);


#endif
