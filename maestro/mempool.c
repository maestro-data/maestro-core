/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Memory Pool
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_mempool.h"
#include "maestro/i_utstack.h"
#include "maestro/logging.h"

#include <pthread.h>
#include <inttypes.h>
#include <string.h>
#include <stddef.h>

/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)

struct wrapped_obj {
  void *next;
  uint16_t obj_arena;
  _Alignas(max_align_t) char obj[];
};

struct arena {
  pthread_mutex_t mtx;
  _Atomic size_t num_alloc;   /**< total number of underlying object allocations done in this arena */
  _Atomic size_t num_free;    /**< total number of underlying object de-allocations done in this arena */
  _Atomic size_t num_in_flight; /**< difference between alloc and free counters */
  _Atomic size_t num_reuse;   /**< total number of allocations satisfied by reuses */
  _Atomic size_t num_recycle; /**< total number of objects entered into freelist */
  _Atomic(size_t) size;       /**< number of objects on freelist */
  struct wrapped_obj *freelist;
};

static inline
size_t
mstro_mempool__num_outstanding(const struct arena *a)
{
  assert(a!=NULL);
  /* num_alloc - num_free is the number of items currently created through the mempool
   * num_free is the part of those that we currently have a handle for, the rest is outstanding
   */
#if 0
  ERR("a%zu r%zu flight %zu f%zu r%zu l%zu diff %zd\n",
	a->num_alloc, a->num_reuse,
        a->num_in_flight,
	a->num_free, a->num_recycle,
	a->size, (ssize_t)a->num_in_flight - a->size);
#endif

  /* We want this function to be correct without locking of the arena, despite
   * multi-threaded access to the counters.
   */
#if 0
  if((ssize_t)a->num_in_flight < a->size) {
    ERR("a%zu r%zu diff %zu f%zu r%zu l%zu diff %zd\n",
	a->num_alloc, a->num_reuse,
        a->num_in_flight,
	a->num_free, a->num_recycle,
	a->size, (ssize_t)a->num_in_flight - a->size);
  }
#endif
  assert(a->num_in_flight >= a->size);
  size_t num_outstanding = a->num_in_flight - a->size;
/*
  DEBUG("a%zu r%zu f%zu r%zu l%zu diff %zu\n",
	a->num_alloc, a->num_reuse,
	a->num_free, a->num_recycle,
	a->size, num_outstanding);
*/

  return num_outstanding;
}


struct mstro_mempool_ {
  char *name;
  uint16_t num_arenas;  /**< number of different arenas */
  size_t object_size; /**< size of the objects in this pool */
  size_t decay_ratio;
  bool thread_private;  /**<will only be accessed by one thread, i.e., it should be lockless*/

  struct arena **arenas; /**< one stack per arena */
};

static inline
size_t hash(uintptr_t val)
{
  union {
    uintptr_t i;
    char x[sizeof(uintptr_t)];
  } u;
  u.i = val;
  /* https://www.strchr.com/hash_functions */
  /* /\* bernstein hash *\/ */
  /* #define HASH_INITIAL_VALUE 5381 */
  /* #define HASH_MULT          33 */

  /* /\* kernighan&Ritchie  *\/ */
  /* #define HASH_INITIAL_VALUE 0 */
  /* #define HASH_MULT          31 */

  /* weinberger */
  #define HASH_INITIAL_VALUE 0
  #define HASH_MULT          65599
  

  size_t res = HASH_INITIAL_VALUE;
  
  for(size_t i = 0; i<sizeof(uintptr_t); i++) {
    res = HASH_MULT * res + u.x[i];
  }
  return res;
}


static inline
size_t
mstro_mempool__my_arena(const struct mstro_mempool_ *pool)
{
  /* assumes sizeof(pthread_t)<=sizeof(intptr_t) */
  intptr_t x = (intptr_t)pthread_self();
  /* x may be too regular (like: an address) to simply be used as index */
  size_t idx = hash(x) % pool->num_arenas;
  /* DEBUG("Selecting arena %zu for thread % " PRIuPTR " (hash: %zu %% %zu)\n", */
  /*       idx, x, hash(x), pool->num_arenas); */
  return idx;
}


#define DEFAULT_NUM_ARENAS 5
#define DEFAULT_DECAY_RATIO 1

mstro_mempool 
mstro_mempool_create(const char *name,
                     size_t obj_size,
                     uint16_t num_arenas,
                     size_t decay_ratio,
                     bool thread_private)
{
  struct mstro_mempool_ * result = malloc(sizeof(struct mstro_mempool_));

  if(result) {
    result->name = strdup(name);
    result->object_size = obj_size;
    result->num_arenas = num_arenas==0 ? DEFAULT_NUM_ARENAS : num_arenas;
    result->decay_ratio = decay_ratio==0 ? DEFAULT_DECAY_RATIO : decay_ratio;
    result->thread_private = thread_private;

    result->arenas = malloc(sizeof(struct arena*)*num_arenas);
    if(result->arenas==NULL) {
      if(result->name!=NULL) {
        free(result->name);
      }
      free(result);
      result=NULL;
    } else {
      size_t i;
      for(i=0; i<num_arenas; i++) {
        result->arenas[i] = malloc(sizeof(struct arena));
        int s = 0;
        if(result->arenas[i]) {
          if (!result->thread_private)
          {
            s = pthread_mutex_init(&result->arenas[i]->mtx, NULL);
          }
          result->arenas[i]->num_alloc=0;
          result->arenas[i]->num_free=0;
          result->arenas[i]->num_in_flight=0;
          result->arenas[i]->num_reuse=0;
          result->arenas[i]->num_recycle=0;
          result->arenas[i]->size=0;
          result->arenas[i]->freelist=NULL;
        }
        
        if(s!=0) {
          free(result->arenas[i]); /* to make this case look the same to
                                    * the one above */
          result->arenas[i]=NULL;
        }
        if(result->arenas[i]==NULL)
          break; /* clean up */
      }
      if(i<num_arenas) {
        /* something went wrong. The current i failed */
        for(i=i; i-->0;) {
          if (!result->thread_private)
          {
            pthread_mutex_destroy(&result->arenas[i]->mtx);
          }
          free(result->arenas[i]);
        }
	      free(result->arenas);
        free(result);
        result = NULL;
      }
    }
  }
  return result;
}

void
mstro_mempool_destroy(mstro_mempool pool)
{
  assert(pool!=NULL);
  DEBUG("Destroying mempool %s (%p)\n", pool->name, pool);
  for(size_t i=0; i<pool->num_arenas; i++) {
    if (!pool->thread_private)
    {
      pthread_mutex_lock(&pool->arenas[i]->mtx);
    }
    size_t leaked = mstro_mempool__num_outstanding(pool->arenas[i]);
    /* percentage of reuses vs. total allocation requests */
    double total_requests = ((double)pool->arenas[i]->num_alloc
                             + (double)pool->arenas[i]->num_reuse);
    double reuse_percentage = total_requests>0 
	    ? (double)pool->arenas[i]->num_reuse*100.0/total_requests : 100;

    if(leaked>0) {
      WARN("Mempool %s arena %zu stats: %zu allocs, %zu reuses (%.0f%%), %zu frees, %zu recycl, %zu on freelist, %zu leaked\n",
           pool->name,
           i,
	   pool->arenas[i]->num_alloc, pool->arenas[i]->num_reuse, 
	   reuse_percentage,
	   pool->arenas[i]->num_free, pool->arenas[i]->num_recycle,
	   pool->arenas[i]->size,
	   mstro_mempool__num_outstanding(pool->arenas[i]));
    } else {
      DEBUG("Mempool %s arena %zu stats: %zu allocs, %zu reuses (%.0f%%), %zu frees, %zu recycl, %zu on freelist, %zu leaked\n",
            pool->name,
            i,
	    pool->arenas[i]->num_alloc, pool->arenas[i]->num_reuse, 
	    reuse_percentage,
	    pool->arenas[i]->num_free, pool->arenas[i]->num_recycle,
	    pool->arenas[i]->size,
	    mstro_mempool__num_outstanding(pool->arenas[i]));
    }

    while(!STACK_EMPTY(pool->arenas[i]->freelist)) {
      struct wrapped_obj *x;
      STACK_POP(pool->arenas[i]->freelist,x);
      pool->arenas[i]->size--;
      free(x);
      pool->arenas[i]->num_in_flight--;
      pool->arenas[i]->num_free++;
    }
    assert(pool->arenas[i]->size==0);
    if (!pool->thread_private)
    {
      pthread_mutex_unlock(&pool->arenas[i]->mtx);
      pthread_mutex_destroy(&pool->arenas[i]->mtx);
    }
    free(pool->arenas[i]);
  }
  free(pool->arenas);
  if(pool->name)
    free(pool->name);

  free(pool);  
}

#ifdef DISABLE_MEMPOOL
void *
mstro_mempool_alloc(mstro_mempool pool)
{
  assert(pool->num_arenas>0);
  pool->arenas[0]->num_alloc++;
  pool->arenas[0]->num_in_flight++;
  return malloc(pool->object_size);
}

#else

void *
mstro_mempool_alloc(mstro_mempool pool)
{
  struct wrapped_obj *x;
  assert(pool!=NULL);

  size_t arena_idx = mstro_mempool__my_arena(pool);
  assert(arena_idx<pool->num_arenas);
  
  struct arena * arena = pool->arenas[arena_idx];

  if(arena->size==0) {
    /* need to alloc; of course someone can come by and put something
     * on the freelist, but we prefer to pull in an extra malloc
     * rather than always mutex-lock */
 DO_ACTUAL_ALLOC: {
      size_t struct_size = sizeof(struct wrapped_obj);
      size_t needed_size = struct_size + pool->object_size ;
      x = malloc(needed_size);
      if(x==NULL) {
        ERR("Failed to allocate for arena %zu\n", arena_idx);
        return NULL;
      } else {
        x->obj_arena=arena_idx;
        arena->num_alloc++;
	arena->num_in_flight++;
      }
      return & x->obj;
    }
  } else {
    if (!pool->thread_private)
    {
      pthread_mutex_lock(&arena->mtx);
    }
    if(arena->size>0) {
      STACK_POP(arena->freelist, x);
      arena->size--;
      assert(x->obj_arena == arena_idx);
    } else {
      x=NULL; /* someone else snatched it */
    }
    if (!pool->thread_private)
    {
      pthread_mutex_unlock(&arena->mtx);
    }
    if(x==NULL) {
      /* nothing found on stack, someone else snatched it */
      goto DO_ACTUAL_ALLOC;
    } else {
      arena->num_reuse++;
      return & x->obj;
    }
  }
}
#endif

#ifdef DISABLE_MEMPOOL

void
mstro_mempool_free(mstro_mempool pool, void *obj)
{
  assert(pool->num_arenas>0);
  free(obj);
  pool->arenas[0]->num_free++;
  pool->arenas[0]->num_in_flight--;
}

#else

void 
mstro_mempool_free(mstro_mempool pool, void *obj)
{
  assert(pool!=NULL);
  assert(obj!=NULL);

  /* obj is actually the second half of a struct wrapped_obj */
  struct wrapped_obj * x
      = ((struct wrapped_obj*)
         ((uintptr_t) obj - offsetof(struct wrapped_obj, obj)));

  size_t arena_idx = mstro_mempool__my_arena(pool);
  uint16_t obj_arena_idx = x->obj_arena;
  assert(arena_idx<pool->num_arenas);
  assert(obj_arena_idx<pool->num_arenas);

#if 0
  if(arena_idx!=obj_arena_idx) {
    DEBUG("Cross-arena free: obj arena %zu, my arena %zu\n",
          obj_arena_idx, arena_idx);
  }
#endif
  
  struct arena * arena = pool->arenas[obj_arena_idx];
  

  /* DEBUG("object: %p, wrapped_obj: %p, obj inside: %p\n", */
  /*       obj, x, & x->obj); */
  assert(& x->obj == obj);

  /*
  DEBUG("free %p: arena stats: a%zu/r%zu, f%zu/r%zu, %zu\n", 
        obj,
	arena->num_alloc, arena->num_reuse, 
	arena->num_free, arena->num_recycle, 
	arena->size);
  */

  const size_t num_outstanding = mstro_mempool__num_outstanding(arena);
  const size_t arenasize = arena->size; /* need to fetch a local copy as it
					 * may change under our feet */
  /* explicit free if we have many more 'ready-to-go' than currently in use: */
  if(pool->decay_ratio * arenasize > num_outstanding) {
    free(x);
    arena->num_in_flight--;
    arena->num_free++;
  } else {
    if (!pool->thread_private)
    {
      pthread_mutex_lock(&arena->mtx);
      STACK_PUSH(arena->freelist, x);
      arena->size++;
      pthread_mutex_unlock(&arena->mtx);
    }
    else {
      STACK_PUSH(arena->freelist, x);
      arena->size++;
    }
    arena->num_recycle++;
  }
}
#endif

