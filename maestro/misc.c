#include "maestro/i_misc.h"
#include "maestro/logging.h"

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 1
#endif
#include <limits.h>

#include <assert.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)


/** find next separator in PATH. Return the start of that segment */
static inline
const char *
next_sep (const char *path)
{
  while (*path)
    if (*path == '/' || *path == '\\')
      return path;
    else
      path++;
  return NULL;
}


mstro_status
mkdirhier(const char *dirname)
{
  assert(strlen(dirname)<PATH_MAX);
  
  char buf[PATH_MAX];
  const char *prev = dirname;
  const char *next;
  struct stat sb;

  while(NULL!=(next = next_sep(prev))) {
    strncpy(buf, dirname, next - dirname) ;
    buf[next - dirname] = '\0';
    if(stat(buf,&sb)) {
      mkdir(buf, 0777); /* umask taken into account by system */
      /* we ignore errors, as we'll see an error on the last part */
    }
    prev=next+1;
  }
  /* handle last part */
  if(stat(dirname, &sb)) {
    int s = mkdir(dirname, 0777);
    if(s!=0) {
      ERR("Failed to create GFS transport directory %s: %d (%s)\n",
          dirname, errno, strerror(errno));
      return MSTRO_FAIL;
    }
  }
  return MSTRO_OK;
}
