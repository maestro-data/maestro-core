/* -*- mode:c -*- */
/** @file
 ** @brief Maestro attributes
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/attributes.h"
#include "maestro/i_attributes.h"
#include "maestro/logging.h"
#include "attributes/maestro-schema.h"

#include "c-timestamp/timestamp.h"
#include <ctype.h>
#include <inttypes.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_ATTR,__VA_ARGS__)


/* FIXME: we should have a table of name/type/... and make the const
 * char* refer to the entries in that */
const char *MSTRO_ATTR_CORE_CDO_NAME =
    ".maestro.core.cdo.name";

const char *MSTRO_ATTR_CORE_CDO_ALLOCATE_NOW =
    ".maestro.core.cdo.allocate-now";

const char *MSTRO_ATTR_CORE_CDO_MAESTRO_PROVIDED_STORAGE =
    ".maestro.core.cdo.maestro-provided-storage";

const char *MSTRO_ATTR_CORE_CDO_RAW_PTR =
    ".maestro.core.cdo.raw-ptr";

const char *MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY =
    ".maestro.core.cdo.mamba-array";

const char *MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE =
    ".maestro.core.cdo.scope.local-size";

const char* MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE =
	".maestro.core.cdo.layout.element-size";

const char* MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS =
	".maestro.core.cdo.layout.ndims";

const char* MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER =
	".maestro.core.cdo.layout.order";

const char* MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE =
	".maestro.core.cdo.layout.dims-size";

const char *MSTRO_ATTR_CORE_CDO_ISGROUP =
    ".maestro.core.cdo.isgroup";

const char *MSTRO_ATTR_CORE_CDO_GROUP_MEMBERS =
    ".maestro.core.cdo.group-members";

const char *MSTRO_ATTR_CORE_CDO_ISDISTRIBUTED =
    ".maestro.core.cdo.isdistributed";

const char *MSTRO_ATTR_CORE_CDO_DIST_LAYOUT =
      ".maestro.core.cdo.dist-layout";

/* .... */


mstro_status
mstro_cdo_attr_table__alloc(mstro_cdo_attr_table *result)
{
  *result = calloc(1,sizeof(struct mstro_cdo_attr_table_));
  if (*result == NULL)
    return MSTRO_NOMEM;

  // defaults are set elsewhere in mstro_cdo_attribute_set_default()

  return MSTRO_OK;
}

mstro_status
mstro_cdo_attr_table__destroy(mstro_cdo_attr_table tab)
{
  if (tab == NULL)
    return MSTRO_INVARG;
  if(tab->name)
    free(tab->name);
  if (tab->id)
    free(tab->id);
  if(tab->lifetime)
    free(tab->lifetime);
  if(tab->redundancy)
    free(tab->redundancy);
//  if (tab->scope.layout)
//    free(tab->scope.layout);
  if(tab->scope.distribution)
    free(tab->scope.distribution);

  free(tab);

  return MSTRO_OK;
}

mstro_status
mstro_cdo_attribute_set(mstro_cdo cdo, const char* key, void* val, bool copy_attribute_value, bool user_owned_attribute_value)
{
  if(cdo==NULL || key==NULL)
    return MSTRO_INVARG;
  /* FIXME: this looks like a silly check: */
  if (key[strlen(key)] != '\0') {
    ERR("Invalid key: not a cstring\n");
    return MSTRO_INVARG;
  }

  if(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {
    ERR("cannot set attributes on sealed CDO\n");
    return MSTRO_FAIL;
  }

  char *tmpfqkey=NULL;
  const char *fqkey=NULL;
  if(key[0]!='.') {
    /* qualify it */
    tmpfqkey = malloc(strlen(cdo->current_namespace)+1+strlen(key)+1);
    if(tmpfqkey==NULL) {
      ERR("Cannot allocate for fully-qualified key\n");
      return MSTRO_NOMEM;
    }
    strcpy(tmpfqkey,cdo->current_namespace);
    size_t l = strlen(tmpfqkey);
    if(tmpfqkey[l-1]!='.')  /* add '.' if needed ; we could require current_namespace to always have one ...*/
      tmpfqkey[l++]='.';
    strcpy(tmpfqkey+l,key);
    fqkey = tmpfqkey;
  } else {
    fqkey=key;
  }

  mstro_status status
      = mstro_attribute_dict_set(cdo->attributes, fqkey,
                                 MSTRO_CDO_ATTR_VALUE_INVALID, /* we dont help in checking */
                                 val, copy_attribute_value, user_owned_attribute_value);

  /* Automaticallly set cdo.isdistributed attribute if the user set any of the
     distributed layout attributes */
  if (status == MSTRO_OK) {
    status = mstro_cdo_attribute_set_isdistributed(cdo, key);
  }

  if(tmpfqkey)
    free(tmpfqkey);
  return status;
}

static inline
mstro_status
mstro_cdo_attribute_set_isdistributed(mstro_cdo cdo, const char* key)
{
  mstro_status status = MSTRO_OK;
  bool val = true;

  if ((strcmp(key, MSTRO_ATTR_CORE_CDO_DIST_LAYOUT) == 0)){
    status = mstro_cdo_attribute_set(cdo, MSTRO_ATTR_CORE_CDO_ISDISTRIBUTED, &val, true, false);
  }

  return status;
}

mstro_status
mstro_cdo_attribute_get(mstro_cdo cdo, const char* key,
                        enum mstro_cdo_attr_value_type *type,
                        const void ** val_p)
{
  if (key == NULL || cdo == NULL) {
    ERR("Invalid CDO or key\n");
    return MSTRO_INVARG;
  }
  if(val_p==NULL) {
    ERR("NULL value destination location\n");
    return MSTRO_INVOUT;
  }

  const char *fqkey=key;
  char *tmpkey = NULL;
  if(key[0]!='.') {
    tmpkey = malloc(strlen(cdo->current_namespace)+strlen(key)+1);
    if(tmpkey==NULL) {
      ERR("Cannot allocate fully qualified key name\n");
      return MSTRO_NOMEM;
    }
    strcpy(tmpkey, cdo->current_namespace);
    strcat(tmpkey, key);
    fqkey=tmpkey;
  }

  mstro_status status
      = mstro_attribute_dict_get(cdo->attributes, fqkey,
                                 type, val_p, NULL, true);
  if(tmpkey)
    free(tmpkey);
  return status;
}

mstro_status
mstro_cdo_attribute_set_default(mstro_cdo cdo)
{
  if (cdo == NULL)
    return MSTRO_INVARG;
  /* we can rely on the dict being non-NULL (at least the core schema is loaded) */

  mstro_schema schema;
  mstro_status s= mstro_attribute_dict_get_schema(cdo->attributes, &schema);
  if(s!=MSTRO_OK)
    return s;

  return mstro_attribute_dict_set_defaults(schema,
                                           true,
                                           &(cdo->attributes));
}

mstro_status
mstro_cdo_attribute_set_yaml(mstro_cdo cdo, const char* keyval_in_yaml)
{
  if(cdo==NULL || keyval_in_yaml==NULL)
    return MSTRO_INVARG;

  if(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {
    ERR("Cannot add yaml attributes on SEALED CDO\n");
    return MSTRO_FAIL;
  }

  mstro_schema schema;
  mstro_status s= mstro_attribute_dict_get_schema(cdo->attributes, &schema);
  if(s!=MSTRO_OK)
    return s;

  return mstro_attributes_parse(schema,
                                keyval_in_yaml,
                                &cdo->attributes,
                                cdo->current_namespace);
}

mstro_status
mstro_cdo_attribute_set_yaml_from_file(mstro_cdo cdo, const char* fname)
{
  if(cdo==NULL || fname==NULL)
    return MSTRO_INVARG;

  if(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {
    ERR("Cannot add yaml attributes on SEALED CDO\n");
    return MSTRO_FAIL;
  }

  mstro_schema schema;
  mstro_status s= mstro_attribute_dict_get_schema(cdo->attributes, &schema);
  if(s!=MSTRO_OK)
    return s;

  return mstro_attributes_parse_from_file(schema,
                                fname,
                                &cdo->attributes,
                                cdo->current_namespace);
}

void 
mstro_cdo_attributes_print(mstro_cdo cdo)
{
  mstro_attribute_dict_print(cdo->attributes);
}

mstro_status
mstro_timestamp_parse(const char *str, size_t len, mstro_timestamp *tsp)
{
  if(str==NULL) {
    ERR("NULL string, cannot parse as timestamp\n");
    return MSTRO_INVARG;
  }
  timestamp_t aux;
  timestamp_t *ts;
  if(tsp==NULL) {
    ts=&aux;
  } else {
    ts = (timestamp_t*)tsp;
  }

  int err = timestamp_parse(str, len, ts);
  if(err==0)
    return MSTRO_OK;
  else
    return MSTRO_FAIL;
}

static inline
mstro_status
mstro_parse_number_array(char *str, size_t *num) {
  char * token, *tmp;
  const char *sep = " ";
  size_t i = 0;
  int len = strlen(str)-1;
  /*trim leading spaces and trailing spaces*/
  while(isspace((unsigned char)*str)) str++;
  while(isspace((unsigned char)*(str+len-1))) len--;


  /**assuming the firts the last characters are []*/
  assert(str[0] == '[');
  assert(str[len-1] == ']');

  /* exclude the first character '[' */
  str++;
  /* exclude the last character ']' */
  str[len-2] = 0;

  /*get first number */
  token = strtok_r(str,sep, &tmp);
  while(token != NULL) {
    sscanf(token, "%zu", &num[i]);
    token = strtok_r(NULL,sep, &tmp);
    i++;
  }

  return MSTRO_OK;
}
/* dist_irregular_1D ... mmbLayoutType, n_dims, index, element_size_bytes, n_blocks, offsets, lengths */
/* example "MMB_IRREGULAR, 1, 0,  8, 3, [0 1 2], [1 1 1]"*/
mstro_status
mstro_parse_mmbLayout_irregular_1D(char *str, mmbLayout **dist_layout) {
  const char *sep = ",";
  char * token, *tmp;
  mmbError mmb_status;
  size_t index, temp_int;
  size_t n_blocks;
  size_t *offsets;
  size_t *lengths;
  size_t element_size_bytes;


  token = strtok_r(str, sep, &tmp);
  DEBUG("parsing token %s \n", token);

  /** Reading layout type ... should be MMB_IRREGULAR **/
  if(strcmp(token, "MMB_IRREGULAR") != 0 ) {
    ERR("Cannot parse other mmb_layout types. Only support MMB_IRREGULAR\n");
    return MSTRO_INVARG;
  }

  /*reading n_dims ... should be always 1 */
  token = strtok_r(NULL, sep, &tmp);
  DEBUG("parsing token %s \n", token);
  sscanf(token, "%zu", &temp_int);
  if (temp_int != 1 ) {
    ERR("Can not parse %zu layouts, only support 1D mmbLayout\n", temp_int);
    return MSTRO_INVARG;
  }

  /*reading index ...*/
  token = strtok_r(NULL, sep, &tmp);
  DEBUG("parsing token %s \n", token);
  sscanf(token, "%zu", &index);

  /*reading element_size_bytes ...*/
  token = strtok_r(NULL, sep, &tmp);
  DEBUG("parsing token %s \n", token);
  sscanf(token, "%zu", &element_size_bytes);
  if(element_size_bytes == 0) {
    ERR("mmbLayout element_size_bytes should be greater than 1\n");
    return MSTRO_INVARG;
  }

  /*reading n_blocks ...*/
  token = strtok_r(NULL, sep, &tmp);
  DEBUG("parsing token %s\n", token);
  sscanf(token, "%zu", &n_blocks);

  offsets = malloc(n_blocks*sizeof(size_t));
  lengths = malloc(n_blocks*sizeof(size_t));

  /*reading offsets ...*/
  token = strtok_r(NULL, sep, &tmp);
  DEBUG("parsing token %s \n", token);
  mstro_parse_number_array(token, offsets);

  /*reading lengths ...*/
  token = strtok_r(NULL, sep, &tmp);
  DEBUG("parsing token %s \n", token);
  mstro_parse_number_array(token, lengths);

  DEBUG("element_size_bytes %zd, index %zd, n_blocks %zd, offsets[0] %zd, lengths[0] %zd \n",
               element_size_bytes, index, n_blocks, offsets[0],lengths[0]);
  mmb_status = mmb_layout_create_dist_irregular_1d(element_size_bytes,
                                      index, n_blocks,offsets,lengths,
                                      dist_layout);

  free(offsets);
  free(lengths);

  return (mmb_status == MMB_OK) ? MSTRO_OK:MSTRO_FAIL;
}

mstro_status
mstro_mmbLayout_parse(const char *str, mmbLayout **dist_layout)
{
  DEBUG("parsing %s, with length %d \n", str, strlen(str));
  if(str==NULL) {
    ERR("NULL string, cannot parse as mmbLayout \n");
    return MSTRO_INVARG;
  } else if(strcmp(str, "NIL")==0) {
    DEBUG("NULL mmbLayout ...freeing the allocated memory \n");
    *dist_layout = NULL;
    free(*dist_layout);
    return MSTRO_OK;
  }

  /*read the layout as a string */
  /*mmbLayout: "mmbLayoutType, n_dims, index, mmbLayoutElement, mmbLayoutPadding, mmbLayoutBlock|mmbLayoutIrregular" */
  /* FIXME only support Irregular_1D mmb_layout for distributed CDOs */
  /* mmbLayoutIrregular: "n_blocks, offsets, lengths" */
  DEBUG("I only support now irregular_1D mmbLayout \n");
  /*bypass the const char warning here ... working on the string for parsing */
  char * tmp = strdup(str);
  return mstro_parse_mmbLayout_irregular_1D(tmp, dist_layout);
}

uint64_t
mstro_attribute_msg_get_totalsize(Mstro__Pool__Attributes *attributes)
{
  size_t n = attributes->kv_map->n_map;

  const char *key;
  uint64_t val;
  for(size_t i=0; i<n; i++) {
     key = attributes->kv_map->map[i]->key;
     if(strcmp(key,".maestro.core.cdo.scope.local-size")==0) {
       val = attributes->kv_map->map[i]->val->uint64;
       return val;
     }
   }
  return -1; 
}

uint64_t
mstro_mmbLayout_get_totalsize(mmbLayout* layout)
{
  uint64_t total_size;
  mstro_status s;
  total_size = 0;

  if (layout->type != MMB_IRREGULAR) {
    for (size_t i=0; i<layout->block.dimensions.size; i++) {
      total_size += layout->block.dimensions.d[i];
    }
  } else {
    for (size_t i=0; i<layout->irregular.n_blocks.d[0]; i++) {
      total_size += layout->irregular.lengths[i];
    }
  }
  return total_size;
}

mstro_status
mstro_timestamp_valid(const mstro_timestamp *tsp, bool *valid)
{
  *valid = timestamp_valid((timestamp_t*)tsp);
  return MSTRO_OK;
}

size_t
mstro_timestamp_format(char *dst, size_t len, const mstro_timestamp *tsp)
{
  return timestamp_format(dst, len, (const timestamp_t*)tsp);
}

size_t
mstro_timestamp_format_precision(char *dst, size_t len, const mstro_timestamp *tsp, int precision)
{
  return timestamp_format_precision(dst, len, (const timestamp_t*)tsp, precision);
}

/** Compare timestamps, returning <0, 0, >0 */
int
mstro_timestamp_compare(const mstro_timestamp *tsp1, const mstro_timestamp *tsp2)
{
  /* timstamp_compare does not respect offset ... FIXME there */
  uint64_t lshift = tsp1->offset * 60;
  uint64_t rshift = tsp2->offset * 60;
  if((tsp1->sec +lshift  < tsp2->sec + rshift))
    return -1;
  else if((tsp1->sec +lshift  == tsp2->sec + rshift)
          && tsp1->nsec == tsp2->nsec)
    return 0;
  else
    return +1;
}

/** Convert timestamp to C style (caller-allocated) struct tm, UTC */
mstro_status
mstro_timestamp_to_tm_utc(const mstro_timestamp *tsp, struct tm *tmp)
{
  if(tsp==NULL)
    return MSTRO_INVARG;
  if(tmp==NULL)
    return MSTRO_INVOUT;

  struct tm *res = timestamp_to_tm_utc((const timestamp_t*)tsp, tmp);
  if(res==NULL)
    return MSTRO_FAIL;

  return MSTRO_OK;
}

/** Convert timestamp to C style (caller-allocated) struct tm, local time zone */
mstro_status
mstro_timestamp_to_tm_local(const mstro_timestamp *tsp, struct tm *tmp)
{
  if(tsp==NULL)
    return MSTRO_INVARG;
  if(tmp==NULL)
    return MSTRO_INVOUT;

  struct tm *res = timestamp_to_tm_local((const timestamp_t*)tsp, tmp);
  if(res==NULL)
    return MSTRO_FAIL;

  return MSTRO_OK;
}




mstro_status
mstro_blob_create(size_t len, void *data, mstro_blob **result_p)
{
  if(result_p==NULL)
    return MSTRO_INVOUT;
  *result_p = malloc(sizeof(mstro_blob));
  if(!*result_p)
    return MSTRO_NOMEM;
  (*result_p)->len = len;
  (*result_p)->data = data;
  return MSTRO_OK;
}

/** convenience function to dispose a blob allocated with mstro_blob_create()
 */
mstro_status
mstro_blob_dispose(mstro_blob *b)
{
  if(b==NULL)
    return MSTRO_INVARG;
  if(b->data) 
  {
	  free(b->data);
	  b->data = NULL;
  }
  free(b);
  b = NULL;
  return MSTRO_OK;
}
