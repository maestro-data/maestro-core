/* -*- mode:c -*- */
/** @file
 ** @brief Maestro NUMA abstraction
 **/
/*
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_NUMA_H_
#define MAESTRO_I_NUMA_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_NUMA Maestro NUMA abstraction
 **@{
 **
 **/

#ifdef HAVE_NUMAIF_H
#include <numa.h>
#include <numaif.h>
#endif

#if defined(__APPLE__) && (!defined(__arm64__))
#include <cpuid.h>
#endif

#include "maestro/logging.h"


/** obtain node ID for a given address.
 *
 * Will always succeed, yielding '0' if NUMA is not supported or ID cannot be obtained */
static inline
int
mstro_numa__node_for_addr(void * addr)
{
#ifdef HAVE_NUMA
  if(g_have_numa) {
    int node = -1;
    long s = get_mempolicy(&node,
		    NULL /* node mask */,
		    0 /* max nodes in mask */,
		    addr, MPOL_F_NODE | MPOL_F_ADDR);
    if(s<0) {
      LOG_WARN(MSTRO_LOG_MODULE_CORE,
	       "Failed to obtain NUMA info for addr %p: %ld (%s)\n",
	       addr, s, strerror(-s));
      return 0;
    } else {
      return node;
    }
  } else {
    return 0;
  }
#else
  void *dummy = addr; /* avoid 'unused arg' warning */
  return 0;
#endif
}

/** Return the number of NUMA domains available to this maestro instance */
static inline
int
mstro_numa__num_nodes(void)
{
#ifdef HAVE_NUMA
  /* FIXME: should query resource/config info */
  return numa_max_node()+1;
#else
  return 1;
#endif
}


/*
 * Hints for auto pinning 
 * PM/PC handler threads create demand queue using mstro_pm_demand_queue_insert, 
 * it might be better to keep PM and transport threads on the same numa domain
 * */

/**get a operations thread core number */
static inline
int
mstro_numa__get_OP_core_num(void)
{
	int current_index=atomic_fetch_add(&g_numa_op_bind_next,1);

	/*check if operation core list is already populated */
	if(g_numa_op_bind_cores != NULL)
	{
		/*return the next number in the list*/
		current_index = current_index % g_numa_op_bind_cores_len;
		return g_numa_op_bind_cores[current_index];
	}
	else
	{
		return -1;
		
	}
}

/**get a PM/PC thread core number */
static inline
int
mstro_numa__get_pm_pc_core_num(void)
{
	int current_index=atomic_fetch_add(&g_numa_pm_pc_bind_next,1);

	/*check if PM/PC core list is already populated */
	if(g_numa_pm_pc_bind_cores != NULL)
	{
		/*return the next number in the list*/
		current_index = current_index % g_numa_pm_pc_bind_cores_len;
		return g_numa_pm_pc_bind_cores[current_index];
	}
	else
	{
		return -1;
		
	}
}
/**get a transport thread core number */
static inline
int
mstro_numa__get_transport_core_num(void)
{
	int current_index=atomic_fetch_add(&g_numa_transport_bind_next,1);

	/*check if transport core list is already populated */
	if(g_numa_transport_bind_cores != NULL)
	{
		/*return the next number in the list*/
		current_index = current_index % g_numa_transport_bind_cores_len;
		return g_numa_transport_bind_cores[current_index];
	}
	else
	{
		return -1;
		
	}
}

static inline
mstro_status
mstro_numa_bind_thread(char *tidprefix)
{
        mstro_status status = MSTRO_UNIMPL;
        /**check we have NUMA enabled*/
#ifdef HAVE_NUMA
        int core_num = 0;
        /**get a core number to pin on it depending on thread type (tidprefix)*/
        if ((strcmp(tidprefix, "OP") == 0))
        {
                core_num = mstro_numa__get_OP_core_num();
        }
        /**PM/PC*/
        else if ((strcmp(tidprefix, "PM") == 0)||(strcmp(tidprefix, "PC") == 0)||(strcmp(tidprefix, "PM_OFI") == 0)||(strcmp(tidprefix, "PC_OFI") == 0))
        {
                core_num = mstro_numa__get_pm_pc_core_num();
        }
        /**transport thread*/
        else if (strcmp(tidprefix, "transport") == 0)
        {
                core_num = mstro_numa__get_transport_core_num();
        }
        else
        {
                LOG_ERR(MSTRO_LOG_MODULE_CORE, "Unknown thread type %s\n", tidprefix);
                return MSTRO_FAIL;
        }
        
        if (core_num == -1)
        {
               LOG_INFO(MSTRO_LOG_MODULE_CORE, "User did not specify a core to bind (-1), not binding this thread to any core\n");
               return MSTRO_OK; 
        }
        /*basic error checking */
        if (core_num >= g_numa_configured_cpus)
        {
                LOG_ERR(MSTRO_LOG_MODULE_CORE, "Can not pin a thread to core number %d\n", core_num);
                status = MSTRO_FAIL;
        }
        else /**try to pin the thread*/
        {
                struct bitmask *cpu_mask = numa_allocate_cpumask();
                numa_bitmask_setbit(cpu_mask, core_num);
                int ret = numa_sched_setaffinity(0, cpu_mask);
                if(ret)
                {
                        LOG_ERR(MSTRO_LOG_MODULE_CORE,
                                        "Failed to pin %s thread to core %d, %d (%s), "
                                        "Please check workload manager cpu binding setting,"
                                        " e.g., try using --cpu-bind=v for slurm\n",
                                        tidprefix,core_num,errno, strerror(errno));
                        status=MSTRO_FAIL;
                }
                else {
                        LOG_INFO(MSTRO_LOG_MODULE_CORE, "%s thread is pinned on core %d\n", tidprefix, core_num);
			/*bind the thread to this numa node*/
                        int numa_node = numa_node_of_cpu(core_num);
                        struct bitmask *nodemask = numa_allocate_cpumask();
                        numa_bitmask_setbit(nodemask, numa_node);
                        numa_set_membind(nodemask);
                        LOG_INFO(MSTRO_LOG_MODULE_CORE, "%s thread is bound to memory node %d\n", tidprefix, numa_node);
                        numa_free_cpumask(nodemask);
                        status = MSTRO_OK;
                }
                numa_free_cpumask(cpu_mask);
        }
#else
        LOG_WARN(MSTRO_LOG_MODULE_CORE, "Does not have libnuma, can not pin %s thread\n", tidprefix);
        status = MSTRO_OK;
#endif
        return status;
}

static inline 
int
mstro_numa_get_cpu(void) {
  #if defined(__linux) || defined(__linux__)
  return sched_getcpu();
  #elif defined(__APPLE__) && (!defined(__arm64__))
  #define CPUID(INFO, LEAF, SUBLEAF) __cpuid_count(LEAF, SUBLEAF, INFO[0], INFO[1], INFO[2], INFO[3])
      uint32_t CPUInfo[4];                           
      CPUID(CPUInfo, 1, 0);                          
       /* CPUInfo[1] is EBX, bits 24-31 are APIC ID */ 
      if ( (CPUInfo[3] & (1 << 9)) == 0) {           
          return  -1;  /* no APIC on chip */             
      }                                              
      int CPU = (unsigned)CPUInfo[1] >> 24;                                      
      return (CPU>=0)?CPU:0; 
  #else
  return 0;
  #endif

}

/**@} (end of group MSTRO_I_NUMA) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_NUMA_H_ */

