/* -*- mode:c -*- */
/** @file
 ** @brief Pool manager protocol
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_pool_manager_protocol.h"
#include "maestro/logging.h"
#include "maestro/i_globals.h"

#include "maestro/i_mempool.h"
#include "maestro/i_maestro_numa.h"

#include "maestro/i_ofi.h"
#include "maestro/i_statistics.h"
#include "maestro/i_ofi_threads.h"

#include <assert.h>
#include <stdint.h>
#include <inttypes.h>


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PM,__VA_ARGS__)


/* instantiation of globals */
struct mstro_endpoint *g_pm_endpoint=NULL;
fi_addr_t g_pm_addr=FI_ADDR_NOTAVAIL;

/* message envelope allocation pools. Small messages in @ref g_msg_env_pool_small
 */
mstro_mempool g_msg_env_pool_small = NULL;

mstro_status
mstro_pmp_init(void)
{
  mstro_status stat;

  g_msg_env_pool_small = mstro_mempool_create("Small PMP Envelope Pool",
                                              sizeof(struct mstro_msg_envelope),
                                              mstro_numa__num_nodes(),
                                              2, false);
  if(g_msg_env_pool_small==NULL) {
    ERR("Failed to allocate PMP Envelope Pool\n");
    stat = MSTRO_NOMEM;
  } else {
    stat = MSTRO_OK;
  }

  return stat;
}

mstro_status
mstro_pmp_finalize(void)
{
  if(g_msg_env_pool_small!=NULL) {
    mstro_mempool_destroy(g_msg_env_pool_small);
    g_msg_env_pool_small=NULL;
  }
  return MSTRO_OK;
}


/** create a new message envelope */
/* FIXME: should have a size parameter */
mstro_status
mstro_msg_envelope_allocate(struct mstro_msg_envelope **res)
{
  if(res==NULL)
    return MSTRO_INVOUT;
  
  *res = mstro_mempool_alloc(g_msg_env_pool_small);
  if((*res)==NULL)
    return MSTRO_NOMEM;

  (*res)->size = MSTRO_MSG_SHORT_SIZE;
  (*res)->tag = 0;
  (*res)->descriptor = NULL;
  /* if this fails you need to fix the definition of MSTRO_MSG_SHORT_SIZE */
  assert(offsetof(struct mstro_msg_envelope,data)<=48); 
  NOISE("Allocated envelope at %p\n", *res);

  return MSTRO_OK;
}

/** destroy a message envelope */
mstro_status
mstro_msg_envelope_free(struct mstro_msg_envelope *e)
{
  if(e==NULL)
    return MSTRO_INVARG;
      
  mstro_mempool_free(g_msg_env_pool_small, e);
  NOISE("De-Allocated envelope at %p\n", e);
  return MSTRO_OK;
}

static inline
mstro_status
mstro_pmp__prepare_send(mstro_app_id target, const Mstro__Pool__MstroMsg *msg, struct mstro_msg_envelope **env, struct mstro_pm_app_registry_entry **app_entry)
{
  if ((env == NULL) || (app_entry == NULL))
  {
    ERR("Invalid output locations, envelope %p, app_entry %p\n", env, app_entry);
    return MSTRO_INVOUT;
  }
  
  size_t msgsize = mstro__pool__mstro_msg__get_packed_size(msg);
  assert(msg->msg_case!=MSTRO__POOL__MSTRO_MSG__MSG__NOT_SET);
  /* we rely on the true sub-message name being in any of the union
   * member's base slot, so DECLARE is as good as any */
  DEBUG("handling msg %s to %zu\n",
        msg->declare->base.descriptor->name, target);

  assert(target!=MSTRO_APP_ID_INVALID);

  if(target==MSTRO_APP_ID_MANAGER && !g_mstro_pm_attached) {
    /* check if this is the JOIN message */
    if(msg->msg_case == MSTRO__POOL__MSTRO_MSG__MSG_JOIN) {
      DEBUG("JOIN message, trying to connect to pool manager\n");
    } else {
      DEBUG("No pool manager, no need to send message of type %s to app id %" PRIu64 "\n",
            msg->base.descriptor->name, target);
      return MSTRO_NO_PM;
    }
  } else {
    /* non-PM target or PM attached */
    ;
  }

  /* serialize it */
  size_t len = msgsize;
  if(len>MSTRO_MSG_SHORT_SIZE) {
    INFO("Message %s packed size %zu requires fragmentation\n",
         msg->base.descriptor->name, len);
    ERR("VSMs unimplemented\n");
    if(target==MSTRO_APP_ID_MANAGER && !g_mstro_pm_attached) {
      mstro_stats_add_counter(MSTRO_STATS_CAT_OFI, MSTRO_STATS_L_PC_NUM_VSM_OUT, 1);
    } else {
      mstro_stats_add_counter(MSTRO_STATS_CAT_OFI, MSTRO_STATS_L_PM_NUM_VSM_OUT, 1);
    }
    return MSTRO_UNIMPL;
  }

  mstro_status s = mstro_msg_envelope_allocate(env);
  if(s!=MSTRO_OK) {
    ERR("Cannot allocate for pool message\n");
    return s;
  }
  (*env)->descriptor = msg->base.descriptor;
  (*env)->payload_size = msgsize;
  mstro__pool__mstro_msg__pack(msg, (*env)->data);

  const ProtobufCFieldDescriptor* msg_kind
      = protobuf_c_message_descriptor_get_field(
          &mstro__pool__mstro_msg__descriptor,
          msg->msg_case);
  DEBUG("Packed %s message (kind: %s, space required: %zu) into envelope (size %zu)\n",
        msg->base.descriptor->name,
        msg_kind ? msg_kind->name : "??",
        msgsize, (*env)->size);


  s = mstro_pm_app_lookup(target, app_entry);
  if(*app_entry==NULL) {
    ERR("Target %zu not in app table\n", target);
    return MSTRO_FAIL;
  } else {
    DEBUG("Target found in app table\n");
  }

  return MSTRO_OK;
}

/** Take protobuf-based message and pack it into a message envelope
 * and send it to the app indicated by @ref target. The @arg msg argument can be a
 * local-scope object at the call-site of this function (it will be
 * serialized immediately on entry and not referenced afterwards) */
mstro_status
mstro_pmp_send_nowait(mstro_app_id target, const Mstro__Pool__MstroMsg *msg)
{
  return mstro_pmp_send_w_completion(target,msg, NULL);
}

/**for suitations when we need to add a completion to a send messgae, queued on OFI_thread send queue*/
mstro_status
mstro_pmp_send_w_completion(mstro_app_id target, const Mstro__Pool__MstroMsg *msg, mstro_event completion_event)
{
  
  struct mstro_msg_envelope *env=NULL;
  struct mstro_pm_app_registry_entry *app_entry= NULL;
  mstro_status s = mstro_pmp__prepare_send(target, msg, &env, &app_entry);
  if(s != MSTRO_OK)
  {
    return s;
  }

  env->ep = app_entry->ep; /**ensure the ep field in envlope is properly initialized*/
  s = mstro_ofi_threads_enqueue_for_send(app_entry->addr, env, completion_event); 
  if (s != MSTRO_OK)
  {
    ERR("Failed to enqueue send message\n");
    return s;
  } 
  DEBUG("OFI message submitted\n");
  s=MSTRO_OK;
  return s;
}

/* same as mstro_pmp_send_nowait, but with immediate EP argument, and less case checking regarding target */
mstro_status
mstro_pmp_send_nowait_ep(const struct mstro_endpoint *ep, fi_addr_t addr, const Mstro__Pool__MstroMsg *msg)
{
  size_t msgsize = mstro__pool__mstro_msg__get_packed_size(msg);

  assert(msg->msg_case!=MSTRO__POOL__MSTRO_MSG__MSG__NOT_SET);

  assert(ep!=NULL);

  /* serialize it */
  size_t len = msgsize;
  if(len>MSTRO_MSG_SHORT_SIZE) {
    INFO("Message %s packed size %zu requires fragmentation\n",
         msg->base.descriptor->name, len);
    ERR("VSMs unimplemented\n");
    /* if(target==MSTRO_APP_ID_MANAGER && !g_mstro_pm_attached) { */
    /*   mstro_stats_add_counter(MSTRO_STATS_CAT_OFI, MSTRO_STATS_L_PC_NUM_VSM_OUT, 1); */
    /* } else { */
    /*   mstro_stats_add_counter(MSTRO_STATS_CAT_OFI, MSTRO_STATS_L_PM_NUM_VSM_OUT, 1); */
    /* } */
    return MSTRO_UNIMPL;
  }

  struct mstro_msg_envelope *env=NULL;

  mstro_status s = mstro_msg_envelope_allocate(&env);
  if(s!=MSTRO_OK) {
    ERR("Cannot allocate for pool message\n");
    return s;
  }
  env->descriptor = msg->base.descriptor;
  env->payload_size = msgsize;
  mstro__pool__mstro_msg__pack(msg, env->data);

  const ProtobufCFieldDescriptor* msg_kind
      = protobuf_c_message_descriptor_get_field(
          &mstro__pool__mstro_msg__descriptor,
          msg->msg_case);
  DEBUG("Packed %s message (kind: %s, space required: %zu) into envelope (size %zu)\n",
        msg->base.descriptor->name,
        msg_kind ? msg_kind->name : "??",
        msgsize, env->size);


  /* for non-completion contexts the message handler frees the context */
  env->ep = ep;
  s = mstro_ofi_threads_enqueue_for_send(addr, env, NULL); 
  if(s!=MSTRO_OK) {
    ERR("Failed to submit %s message: %d (%s)\n",
        msg->base.descriptor->name,
        s, mstro_status_description(s));
    goto BAILOUT;
  }
  DEBUG("OFI message submitted\n");

  s=MSTRO_OK;
BAILOUT:
  /* envelope freed in completion handler */
  return s;
}

#if DEPRECATED
/** Take protobuf-based message and pack it into a message envelope
 * and send it to the pool manager. The @arg msg argument can be a
 * local-scope object at the call-site of this function (it will be
 * serialized immediately on entry and not referenced afterwards)
 *
 * Waits for completion of the send operation.
 */
mstro_status
mstro_pmp_send_wait(mstro_app_id target, const Mstro__Pool__MstroMsg *msg)
{
  size_t msgsize = mstro__pool__mstro_msg__get_packed_size(msg);

  /* we rely on the true sub-message name being in any of the union
   * member's base slot, so DECLARE is as good as any */
  DEBUG("handling msg %s to %zu\n", msg->declare->base.descriptor->name, target);

  assert(target!=MSTRO_APP_ID_INVALID);

  if(target==MSTRO_APP_ID_MANAGER && !g_mstro_pm_attached) {
    /* check if this is the JOIN message */
    if(msg->msg_case == MSTRO__POOL__MSTRO_MSG__MSG_JOIN) {
      DEBUG("JOIN message, trying to connect to pool manager\n");
    } else {
      DEBUG("No pool manager, no need to send message of type %s to app id %" PRIu64 "\n",
            msg->base.descriptor->name, target);
      return MSTRO_NO_PM;
    }
  } else {
    /* non-PM target or PM attached */
    ;
  }

  /* serialize it */
  size_t len = msgsize;
  if(len>MSTRO_MSG_SHORT_SIZE) {
    INFO("Message %s packed size %zu requires fragmentation\n",
         msg->base.descriptor->name, len);
    ERR("VSMs unimplemented\n");
    return MSTRO_UNIMPL;
  }

  struct mstro_msg_envelope *env;

  mstro_status s = mstro_msg_envelope_allocate(&env);
  if(s!=MSTRO_OK) {
    ERR("Cannot allocate for pool message\n");
    return s;
  }
  env->descriptor = msg->base.descriptor;
  env->payload_size = msgsize;
  mstro__pool__mstro_msg__pack(msg, env->data);

  DEBUG("Packed %s message (space required: %zu) into envelope (size %zu)\n",
        msg->base.descriptor->name, msgsize, env->size);

  struct mstro_pm_app_registry_entry *app_entry = NULL;
  s = mstro_pm_app_lookup(target, &app_entry);
  if(app_entry==NULL) {
    ERR("Target %zu not in app table\n", target);
    return MSTRO_FAIL;
  } else {
    DEBUG("Target found in app table\n");
  }

  s = mstro_ofi__submit_message_wait(app_entry->ep, app_entry->addr,
                                     env);
  if(s!=MSTRO_OK) {
    ERR("Failed to submit %s message: %d (%s)\n",
        msg->base.descriptor->name,
        s, mstro_status_description(s));
    goto BAILOUT;
  }
  DEBUG("OFI message submitted, waited, and completed\n");

  s=MSTRO_OK;
BAILOUT:
  /* envelope freed in completion handler */
  return s;
}

#endif



mstro_status
mstro_pmp_package(Mstro__Pool__MstroMsg *msg,
                  ProtobufCMessage *inner)
{
  if(msg==NULL || inner==NULL) {
    return MSTRO_INVARG;
  }

  /* check that dst has been initialized somehow */
  assert(msg->base.descriptor == &mstro__pool__mstro_msg__descriptor);

  /* common init: */
  /* we are sharing the same token and appid structure on all outgoing
   * messages.  Care must be taken to distinguish between these
   * reusable tokens and dynamically allocated ones on incoming
   * messages -- the protobuf-created
   * mstro__pool__mstro_msg__free_unpacked() function will recurse ind
   * try to free these */
  msg->token = &g_pool_apptoken; /* populated at WELCOME time for clients, at PM start for pool manager */

  /* security part of token would need to be set from security token
   * at some point */
  /* msg->token = ... */
  /* opts left at NULL means: not set */

  /* no extra data (join sub-message contains all info) */


  /* specific ones */
  /* FIXME: have caller pass in the proper value to put into 'case' so
   * that we can switch instead of all the 'if' cases */
  if(inner->descriptor==&mstro__pool__join__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_JOIN;
    msg->join = (Mstro__Pool__Join*)inner;
  } else if(inner->descriptor == &mstro__pool__declare__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_DECLARE;
    msg->declare = (Mstro__Pool__Declare*)inner;
  } else if(inner->descriptor == &mstro__pool__declare_ack__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_DECLARE_ACK;
    msg->declare_ack = (Mstro__Pool__DeclareAck*)inner;
  } else if(inner->descriptor == &mstro__pool__welcome__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_WELCOME;
    msg->welcome = (Mstro__Pool__Welcome*)inner;
  } else if(inner->descriptor == &mstro__pool__seal__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_SEAL;
    msg->seal = (Mstro__Pool__Seal*)inner;
  } else if(inner->descriptor == &mstro__pool__offer__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_OFFER;
    msg->offer = (Mstro__Pool__Offer*)inner;
  } else if(inner->descriptor == &mstro__pool__require__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_REQUIRE;
    msg->require = (Mstro__Pool__Require*)inner;
  } else if(inner->descriptor == &mstro__pool__withdraw__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_WITHDRAW;
    msg->withdraw = (Mstro__Pool__Withdraw*)inner;
  } else if(inner->descriptor == &mstro__pool__retract__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_RETRACT;
    msg->retract = (Mstro__Pool__Retract*)inner;
  } else if(inner->descriptor == &mstro__pool__demand__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_DEMAND;
    msg->demand = (Mstro__Pool__Demand*)inner;
  } else if(inner->descriptor == &mstro__pool__pool_op_ack__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_ACK;
    msg->ack = (Mstro__Pool__PoolOpAck*)inner;
  } else if(inner->descriptor == &mstro__pool__leave__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_LEAVE;
    msg->leave = (Mstro__Pool__Leave*)inner;
  } else if(inner->descriptor == &mstro__pool__bye__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_BYE;
    msg->bye = (Mstro__Pool__Bye*)inner;
  } else if(inner->descriptor == &mstro__pool__initiate_transfer__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_INITIATE_TRANSFER;
    msg->initiate_transfer = (Mstro__Pool__InitiateTransfer*)inner;
  } else if(inner->descriptor == &mstro__pool__transfer_ticket__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_TICKET;
    msg->transfer_ticket = (Mstro__Pool__TransferTicket*)inner;
  } else if(inner->descriptor == &mstro__pool__transfer_completed__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_COMPLETED;
    msg->transfer_completed = (Mstro__Pool__TransferCompleted*)inner;
  } else if(inner->descriptor == &mstro__pool__dispose__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_DISPOSE;
    msg->dispose = (Mstro__Pool__Dispose*)inner;
  } else if(inner->descriptor == &mstro__pool__subscribe__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE;
    msg->subscribe = (Mstro__Pool__Subscribe*)inner;
  } else if(inner->descriptor == &mstro__pool__subscribe_ack__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE_ACK;
    msg->subscribe_ack = (Mstro__Pool__SubscribeAck*)inner;
  } else if(inner->descriptor == &mstro__pool__unsubscribe__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_UNSUBSCRIBE;
    msg->unsubscribe = (Mstro__Pool__Unsubscribe*)inner;
  } else if(inner->descriptor == &mstro__pool__event__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_EVENT;
    msg->event = (Mstro__Pool__Event*)inner;
  } else if(inner->descriptor == &mstro__pool__event_ack__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_EVENT_ACK;
    msg->event_ack = (Mstro__Pool__EventAck*)inner;
  } else if(inner->descriptor == &mstro__pool__resolve__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE;
    msg->resolve = (Mstro__Pool__Resolve*)inner;
  } else if(inner->descriptor == &mstro__pool__resolve_reply__descriptor) {
    msg->msg_case = MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE_REPLY;
    msg->resolve_reply = (Mstro__Pool__ResolveReply*)inner;
  } else {
      ERR("Unimplemented submessage: %s. Please fix\n",
          inner->descriptor->name);
      return MSTRO_UNIMPL;
  }
  return MSTRO_OK;
}
