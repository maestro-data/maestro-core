/* -*- mode:c -*- */
/** @file
 ** @brief Pool manager registry
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_pool_manager_registry.h"
#include "maestro/logging.h"

#include "maestro/i_uthash.h"
#include "maestro/i_utlist.h"
#include "maestro/i_attributes.h"
#include "maestro/i_ofi.h"
#include "maestro/i_statistics.h"

#include "protocols/mstro_pool.pb-c.h"

#include "maestro/i_pool_manager_protocol.h"
#include "maestro/i_globals.h"

#include "i_subscription_registry.h"

#include <assert.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdatomic.h>


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PM,__VA_ARGS__)


/** the app registry table */
static struct mstro_pm_app_registry_entry *g_mstro_pm_app_registry = NULL;

/** lock for the registry */
pthread_mutex_t g_mstro_pm_app_registry_lock = PTHREAD_MUTEX_INITIALIZER;

/** macro to make locked uthash accesses to the registry. Not super
 * safe, beware. It's meant to only be used around HASH_* functions */
#define WITH_LOCKED_APP_REGISTRY(body) \
  do {                                                                  \
  int wlr_stat=pthread_mutex_lock(&g_mstro_pm_app_registry_lock);       \
  if(wlr_stat!=0) {                                                     \
    ERR("Failed to lock app registry: %d (%s)\n",                       \
        wlr_stat, strerror(wlr_stat));                                  \
    abort();                                                            \
  }                                                                     \
                                                                        \
  do {                                                                  \
    body;                                                               \
  } while(0);                                                           \
                                                                        \
  wlr_stat=pthread_mutex_unlock(&g_mstro_pm_app_registry_lock);         \
  if(wlr_stat!=0) {                                                     \
    ERR("Failed to unlock app registry: %d (%s)\n",                     \
        wlr_stat, strerror(wlr_stat));                                  \
    abort();                                                            \
  }                                                                     \
  } while(0)

/** the global next-available-app-id counter */
static _Atomic mstro_app_id g_next_msg_source_id = MSTRO_APP_ID_MANAGER+1;

static
mstro_app_id
mstro_pm_app__next_id(void)
{
  mstro_app_id next = atomic_fetch_add(&g_next_msg_source_id, 1);
  if(next<UINT64_MAX) {
    return next;
  } else {
    /* slow path: table is quite full apparently */
    ERR("Unimplemented slow-path for >2^64-1 maestro pool participants, FIXME\n");
    abort();
  }
}

static
mstro_status
mstro_pm_app_reg__new_entry(struct mstro_pm_app_registry_entry **entry_p, mstro_app_id id)
{
  assert(entry_p!=NULL);

  struct mstro_pm_app_registry_entry *e
      =malloc(sizeof(struct mstro_pm_app_registry_entry));
  if(e==NULL) {
    return MSTRO_NOMEM;
  }

  e->appid = id;
  e->ep = NULL;
  e->serialized_desc = NULL;
  e->transport_methods = NULL;
  e->component_name = NULL;
  e->component_index = UINT64_MAX;
  e->dead = false;
  e->pending = false;

  *entry_p = e;
  return MSTRO_OK;
}

static
mstro_status
mstro_pm_app_reg__entry_dispose(struct mstro_pm_app_registry_entry *e)
{
  if(e==NULL)
    return MSTRO_INVARG;

  if(e->ep) {
    ; /* owned by creator (?) */
  }

  if(e->serialized_desc) {
    free(e->serialized_desc);
  }

  if(e->transport_methods) {
    ; /* owned by creator */
  }

  if(e->component_name)
    free(e->component_name);

  free(e);
  return MSTRO_OK;
}

mstro_status
mstro_pm_app_register(const struct mstro_endpoint *ep,
                      fi_addr_t addr,
                      char* serialized_desc,
                      const Mstro__Pool__TransportMethods *transport_methods,
                      mstro_app_id *id_p,
                      const char *component_name,
                      uint64_t component_index,
                      struct mstro_pm_app_registry_entry **entry_p)
{
  if(ep==NULL)
    return MSTRO_INVARG;
  if(id_p==NULL && entry_p==NULL)
    return MSTRO_INVOUT;


  struct mstro_pm_app_registry_entry *e=NULL;
  mstro_status status
      = mstro_pm_app_reg__new_entry(&e, mstro_pm_app__next_id());

  if(status!=MSTRO_OK) {
    ERR("Failed to create new regentry\n");
    return status;
  }

  e->ep = (struct mstro_endpoint *) ep;
  e->addr = addr;
  e->serialized_desc = serialized_desc;
  e->transport_methods = (Mstro__Pool__TransportMethods *)transport_methods;
  e->component_name  = strdup(component_name);
  e->component_index = component_index;
  e->pending =false;

  /* check for duplicates */
  WITH_LOCKED_APP_REGISTRY({
      struct mstro_pm_app_registry_entry *elt=NULL;
      struct mstro_pm_app_registry_entry *tmp=NULL;
      HASH_ITER(hh, g_mstro_pm_app_registry, elt, tmp) {
        if(0==strcmp(elt->component_name, e->component_name)) {
          if(elt->component_index == e->component_index) {
	    if(elt->dead) {
	      /* garbage-collect the old one; FIXME: could be smarter and re-use parts of it */
	      DEBUG("Garbage-collecting previous app entry for %s/%zu (app %" PRIappid ")\n",
		    elt->component_name, elt->component_index);
	      HASH_DEL(g_mstro_pm_app_registry, elt);
	    } else {
	      ERR("Duplicate component registration: %s:%" PRIu64         \
                  " has appid %" PRIappid                                \
                  ", now adding same component/index pair as appid %" PRIappid "\n",
                  elt->component_name, elt->component_index, elt->appid, e->appid);
              status = MSTRO_NOT_TWICE;
              goto unlock;
	    }
          }
        }
      }
      /**Everythin is fine*/
      assert(status == MSTRO_OK);
      /** add app to registry*/
      HASH_ADD(hh, g_mstro_pm_app_registry,
               appid, sizeof(mstro_app_id), e);
   unlock:
      ;});

  if(status!=MSTRO_OK) {
    mstro_pm_app_reg__entry_dispose(e);
  }
  else
  {
    DEBUG("Registered app %" PRIappid " for %s:%" PRIu64 " with transport methods %p\n",
           e->appid, e->component_name, e->component_index, e->transport_methods);
    /* return data to caller if interesed */
    if(id_p!=NULL) {
      *id_p = e->appid;
    }
    if(entry_p!=NULL) {
      *entry_p = e;
    }
  }

  return status;
}

mstro_status
mstro_pc_app_register_pending(mstro_app_id id)
{
	struct mstro_pm_app_registry_entry *e=NULL;
  struct mstro_pm_app_registry_entry *elt=NULL;

	mstro_status status = MSTRO_UNIMPL;
	status = mstro_pm_app_reg__new_entry(&e, id);
	if(status==MSTRO_OK) {
		e->pending = true;
		WITH_LOCKED_APP_REGISTRY({
        /**check there is no other entry before*/
        HASH_FIND(hh, g_mstro_pm_app_registry, &id, sizeof(mstro_app_id), elt);
        if((elt!=NULL) && (elt->pending)) { 
          WARN("There is alreading a pending entry for app %"PRIappid", should not overwrite \n", id);
          /*cleanup*/
          status = mstro_pm_app_reg__entry_dispose(e);
          assert(status ==MSTRO_OK);
          status = MSTRO_FAIL;
        }
        else if((elt!=NULL) && (!elt->pending))
        {
          ERR("App %"PRIappid" already exists in regsitry, should not overwrite \n", id);
          /*cleanup*/
          status = mstro_pm_app_reg__entry_dispose(e);
          assert(status ==MSTRO_OK);
          status = MSTRO_FAIL;
        }
        else { // elt == NULL
				  HASH_ADD(hh, g_mstro_pm_app_registry,
						  appid, sizeof(mstro_app_id), e);
          DEBUG("Added a pending entry for app %"PRIappid" to registry \n", id);
          status = MSTRO_OK;
        }
				});
	}
	return status;
}

mstro_status
mstro_pc_app_register(struct mstro_endpoint *ep,
                      fi_addr_t addr,
                      char* serialized_desc,
                      const Mstro__Pool__TransportMethods *transport_methods,
                      mstro_app_id id,
                      struct mstro_pm_app_registry_entry **entry_p)
{
  if(ep==NULL)
    return MSTRO_INVARG;
  if(id==MSTRO_APP_ID_INVALID)
    return MSTRO_INVARG;

  struct mstro_pm_app_registry_entry *elt=NULL;

  mstro_status status = MSTRO_OK; 
  /* FIXME: we should check that there is no previous entry and
   * document whether that's an error or a legal way of overriding
   * things */
  /* remove pending entries */
  WITH_LOCKED_APP_REGISTRY({
        HASH_FIND(hh, g_mstro_pm_app_registry, &id, sizeof(mstro_app_id), elt);
        if((elt!=NULL) && (elt->pending)) {
          /**fill it and drop the pending flag*/
          elt->addr = addr;
          elt->ep = ep;
          elt->serialized_desc = serialized_desc;
          elt->transport_methods = (Mstro__Pool__TransportMethods *) transport_methods;
          elt->pending = false;
          status = MSTRO_OK;
        }
        else if ((elt != NULL) && (!elt->pending))
        {
          //FIXME ...maybe just a warning
          ERR("Trying to register app %"PRIappid" twice, may overwrite and corrupt registry...Aborting\n", id);
          status = MSTRO_FAIL;
        }
        else /*elt == NULL */
        {
          //FIXME ...maybe just a warning
          ERR("Trying to add app %"PRIappid" to registry without having a pending entry, should not happen...Aborting\n", id);
          status = MSTRO_FAIL;
        }

	;});

  /* return data to caller if interesed */
  if(entry_p!=NULL) {
    *entry_p = elt;
  }

  return status;
}

mstro_status
mstro_pm_app_register_manager(struct mstro_endpoint *ep,
                              fi_addr_t addr)
{
  if(ep==NULL)
    return MSTRO_INVARG;

  struct mstro_pm_app_registry_entry *e=NULL;
  mstro_pm_app_lookup(MSTRO_APP_ID_MANAGER, &e);
  if(e!=NULL) {
    WARN("Overwriting existing pool manager registry entry!\n");
    e=NULL;
  }

  mstro_status status = mstro_pm_app_reg__new_entry(&e, MSTRO_APP_ID_MANAGER);
  if(status==MSTRO_OK) {
    e->ep = ep;
    e->addr = addr;

    WITH_LOCKED_APP_REGISTRY({
        HASH_ADD(hh, g_mstro_pm_app_registry,
                 appid, sizeof(mstro_app_id), e);
      });
  }
  return status;
}






mstro_status
mstro_pm_app_lookup(mstro_app_id appid,
                    struct mstro_pm_app_registry_entry **app_entry_p)
{
  assert(app_entry_p!=NULL);
  mstro_status status=MSTRO_OK;

  WITH_LOCKED_APP_REGISTRY({
      struct mstro_pm_app_registry_entry *e=NULL;
      HASH_FIND(hh, g_mstro_pm_app_registry, &appid, sizeof(mstro_app_id), e);
      if((e!=NULL) && (e->pending)) {
	      status = MSTRO_WOULDBLOCK;
	      *app_entry_p = NULL;
      }
      else if( (e!=NULL) &&(!e->pending))
      {
        status = MSTRO_OK;
       	*app_entry_p = e;
      }
      else {
        *app_entry_p = NULL;
        status=MSTRO_FAIL;
      }
    });
  return status;
}



/** pair of CDO name and chosen CDO ID */
struct cdo_name_id_pair {
  struct cdo_name_id_pair *next;  /**< this is a linked list */
  char *cdo_name;                 /**< the CDO name */
  struct mstro_cdo_id cdo_id;     /**< the CDO ID */
};

/** Table of colliding names for the same CDO ID, and their resolution */
struct cdo_id_collision_entry {
  UT_hash_handle hh;                   /**< hash on cdo_id */
  struct mstro_cdo_id cdo_id;          /**< the CDO ID that had a collision */
  struct cdo_name_id_pair *collisions; /**< list of name/ID pairs to resolve the collision */
};

/** Table of collisions encountered */
/* FIXME: currenly not implemented */
static struct cdo_id_collision_entry *g_cdo_id_collision_table = NULL;

/** entry of one CDO's local-id (= handle inside the originating app) to state and attributes */
struct cdo_handle_entry {
  UT_hash_handle hh; /**< hashed on local_id */
  uint64_t local_id; /**< the local-id part of the CDO id */
  mstro_cdo_state cdo_state;            /**< state of this handle */
  Mstro__Pool__Attributes *attributes;  /**< attributes reported from this app */
  int64_t n_segments; /**< number of transmissions required to fill this CDO -- mainly for dist CDOs*/
  mmbLayout *dist_layout; /**< distributed layout handle*/
};

/** Per-Application CDO knowledge structure. */
struct per_app_cdo_entries {
  UT_hash_handle hh;                 /**< hash on app */
  mstro_app_id app;                  /**< application id [currently JOINED] */
  struct cdo_handle_entry *handles;  /**< entries for this app */
  /* we could have a separate mutex for the handles to that we can
   * local the per-app entry independently of the registry table */
};

/** Entry used to keep all CDOs known to the pool manager in sync */
struct mstro_pm_cdo_registry_entry {
  UT_hash_handle hh;                             /**< hash on CDO id */
  struct mstro_cdo_id cdo_id;                    /**< the global cdo id (local-part always MSTRO_CDO_LOCAL_ID_NONE */
  char *cdo_name;                                /**< the CDO printable name */
  struct per_app_cdo_entries *app_to_attributes; /**< app-to-attributes table for the cdo id */
  bool is_distributed;                           /**< flag indicates a distributed CDO */
};


/** the CDO registry */
static struct mstro_pm_cdo_registry_entry *g_mstro_pm_cdo_registry = NULL;

/** lock for the CDO registry */
pthread_mutex_t g_mstro_pm_cdo_registry_lock = PTHREAD_MUTEX_INITIALIZER;

/** macro to make locked uthash accesses to the cdo registry. Not super
 * safe, beware. It's meant to only be used around HASH_* functions */
#define WITH_LOCKED_CDO_REGISTRY(body) \
  do {                                                                  \
  int wlr_stat=pthread_mutex_lock(&g_mstro_pm_cdo_registry_lock);       \
  if(wlr_stat!=0) {                                                     \
    ERR("Failed to lock cdo registry: %d (%s)\n",                       \
        wlr_stat, strerror(wlr_stat));                                  \
    abort();                                                            \
  }                                                                     \
                                                                        \
  do {                                                                  \
    body;                                                               \
  } while(0);                                                           \
                                                                        \
  wlr_stat=pthread_mutex_unlock(&g_mstro_pm_cdo_registry_lock);         \
  if(wlr_stat!=0) {                                                     \
    ERR("Failed to unlock cdo registry: %d (%s)\n",                     \
        wlr_stat, strerror(wlr_stat));                                  \
    abort();                                                            \
  }                                                                     \
  } while(0)


/** Outstanding require and demand queue */

struct mstro_pm_demand_queue_entry {
  struct mstro_pm_demand_queue_entry *next; /**< singly linked list */
  struct mstro_cdo_id cdo_id; /**< the CDO ID */
  mstro_app_id requestor; /**< the requesting app */
  Mstro__Pool__Attributes *req_attributes; /**< the requesting side's CDO attributes */
  mmbLayout *req_dist_layout; /**<the requesting side's CDO distributed layout handle*/
  Mstro__Pool__TransportMethods *req_methods; /**< requesting side's transfer method wishes */
  mstro_app_id provider; /**< source satisfying this request */
};



static inline
mstro_status
mstro_pm_demand_queue_entry__destroy(struct mstro_pm_demand_queue_entry *res)
{
  /* FIXME: could pool for reuse */
  /* the attributes are read-only and belong the the app's entry, we must not free them */
  free(res);
  return MSTRO_OK;
}


/** list of outstanding demand queue entries */
static struct mstro_pm_demand_queue_entry *g_mstro_pm_demand_queue = NULL;
/** Lock for @ref g_mstro_pm_demand_queue */
static pthread_mutex_t g_mstro_pm_demand_queue_mtx = PTHREAD_MUTEX_INITIALIZER;
/** wait object for @ref g_mstro_pm_demand_queue */
pthread_cond_t  g_mstro_pm_demand_queue_cvar = PTHREAD_COND_INITIALIZER;


/** list of outstanding WITHDRAW requests */
/**Widraws are mainly operations ...they are handled in the operation queue as normal operations*/

/** notify whoever needs to be informed about changes on the CDO registry.
 *
 * pokes @ref g_mstro_pm_demand_queue_cvar
 */
static inline
mstro_status
mstro_pm__notify_cdo_registry_change(void)
{
  int s = pthread_mutex_lock(&g_mstro_pm_demand_queue_mtx);
  if(s!=0) {
    ERR("Failed to lock demand queue: %d\n", s);
    return MSTRO_FAIL;
  }
  s=pthread_cond_signal(&g_mstro_pm_demand_queue_cvar); /*signal is enough for a single transport thread*/
  if(s!=0) {
    ERR("Failed to signal demand queue update: %d\n", s);
    pthread_mutex_unlock(&g_mstro_pm_demand_queue_mtx);
    return MSTRO_FAIL;
  }
  s=pthread_mutex_unlock(&g_mstro_pm_demand_queue_mtx);
  if(s!=0) {
    ERR("Failed to lock demand queue: %d\n", s);
    return MSTRO_FAIL;
  }

  return MSTRO_OK;
}

/** find best candidate to serve distributed CDO to app_id
 *
 * Returns best match in *best, or MSTRO_APP_ID_INVALID if none exists.
 *
 * must be called under lock.
 */
static inline
mstro_status
mstro_pm__dist_cdo_find_best_provider(
    struct per_app_cdo_entries *app_to_attributes_table,
    struct mstro_pm_demand_queue_entry *e,
    mstro_pm_candidates **candidates)
{
  struct per_app_cdo_entries *entry, *tmp;
  struct per_app_cdo_entries *best;
  uint64_t best_local_id;
  best = NULL;
  mstro_status status;
  bool default_layout = false;
  mmbLayout *src_layout = NULL;
  mmbError stat;

  /* FIXME: this is just greedy */
  /* FIXME: this also does not return the actual local-id */
  /* Try to find a 1:1 mapping for the required cdo layout */
  status = mstro_pm__find_cdo_with_layout(
                              app_to_attributes_table,
                              e->req_dist_layout,
                              &best,
                              &best_local_id);
  assert(status == MSTRO_OK);
  if(best) {
    /* create candidates out of best with length 1 */
    status = mstro__pool_create_candidates(candidates, 1, true);
    if (status != MSTRO_OK) {
      return status;
    }
    (*candidates)->app[0] = best;
    (*candidates)->local_id[0] = best_local_id;
    DEBUG("Found entry with local-id %zu for % " PRIappid " to provide CDO with the exact layout to % " PRIappid "\n",
          (*candidates)->local_id[0], (*candidates)->app[0], e->requestor);
    return MSTRO_OK;
  }
  else {
    /* try to find a many:1 mapping for the required cdo layout */
    HASH_ITER(hh, app_to_attributes_table, entry, tmp) {
      struct cdo_handle_entry *h,*t;
      HASH_ITER(hh, entry->handles, h, t) {
        if(h->cdo_state==MSTRO_CDO_STATE_OFFERED)  {

          if(!h->dist_layout) {
            WARN("CDO with local-id %zu is flagged as distributed but has no distribution layout ... will create a default one \n", h->local_id);
            stat = mmb_layout_dist_create_default_layout(e->req_dist_layout, &src_layout);
            assert(stat == MMB_OK);
            default_layout = true;
          }
          else {
            src_layout = h->dist_layout;
            default_layout = false;
          }

          /** try to match the distribution */
          mmbLayoutEquivalence diff;
          
          stat = mmb_layout_cmp(src_layout, e->req_dist_layout, &diff);
          assert(MMB_OK == stat);
          assert(diff != MMB_LAYOUT_EQUAL); /* would have been caught above by mstro_pm__find_cdo_with_layout */
          if (diff == MMB_LAYOUT_DIFF_FIELDS){
            DEBUG("Found entry with local-id %zu for % " PRIappid " but with different layout \n",
                h->local_id, entry->app);

            /* FIXME make a hash table of the two layouts, so that we do not compute this intersection many times */
            mmbLayoutIntersection *out_li = NULL;
            stat = mmb_layout_compute_intersection(src_layout, e->req_dist_layout, &out_li);
            assert(MMB_OK == stat);
            DEBUG("checking layouts intersection \n");
            if (out_li) {
              mstro_pm__mmbLayoutIntersecton_to_candidates(out_li, e->req_dist_layout->index, src_layout, app_to_attributes_table, candidates);
              mmb_layout_destroy_mmbLayoutIntersection(out_li);
            }

            /*destroy the default layout we created */
            if (default_layout) {
              mmb_layout_destroy(src_layout);
            }


          }
          else { /*diff == MMB_LAYOUT_DIFF_INDEX OR diff == MMB_LAYOUT_DIFF_TYPES */
            /* do nothing */
            /* we do not care if we found same layout with different index
             * because there will be no intersection
             * AND
             * we do not care about MMB_LAYOUT_DIFF_TYPES because finding intersections
             * is not supported (yet) */

          }
        }

      }
    }
  }
  return MSTRO_OK;
}

static inline
mstro_status
mstro__pool_create_candidates(mstro_pm_candidates **candidates, size_t length, bool distribution) {
  mstro_status status = MSTRO_OK;
  /* Initialize candidates */
  *candidates = (mstro_pm_candidates *) malloc(sizeof(mstro_pm_candidates));
  if (!(*candidates)) {
    ERR("Not enough memory for candidates \n");
    return MSTRO_NOMEM;
  }
  (*candidates)->n_sources = length; /* number of apps */
  (*candidates)->is_distributed = distribution; /* a (re)distribution operation */
  if (!((*candidates)->n_sources)) {
    mstro_pm_candidates_destroy(*candidates);
    ERR("Not enough memory for candidates \n");
    return MSTRO_NOMEM;
  }
  (*candidates)->app = malloc(sizeof(struct per_app_cdo_entries *)*length);
  if (!((*candidates)->app)) {
    mstro_pm_candidates_destroy(*candidates);
    ERR("Not enough memory for candidates \n");
    return MSTRO_NOMEM;
  }
  (*candidates)->local_id = malloc(sizeof(uint64_t)*length);
  if (!((*candidates)->local_id)) {
    mstro_pm_candidates_destroy(*candidates);
    ERR("Not enough memory for candidates \n");
    return MSTRO_NOMEM;
  }


  return status;
}

static inline
mstro_status
mstro_pm__count_intersections(
                        mmbLayoutIntersection *intersection,
                        size_t dst_index,
                        size_t *count) {
  mstro_status status = MSTRO_OK;
  *count = 0;
  size_t n_src_pieces = intersection->n_src_pieces;
  size_t index;
  for (size_t i = 0; i < n_src_pieces; i++) {

    index = dst_index*n_src_pieces + i;

    /* we are only interested if the length of intersection is > 0*/
    if (intersection->overlap[index].length > 0) {
      *count += 1;
    }
  }

  return status;
}

static inline
mstro_status
mstro_pm__mmbLayoutIntersecton_to_candidates(
                            mmbLayoutIntersection *intersection,
                            size_t dst_index,
                            mmbLayout *src_layout,
                            struct per_app_cdo_entries *app_to_attributes_table,
                            mstro_pm_candidates **candidates) {

  mstro_status status = MSTRO_OK;
  size_t n_src_pieces = intersection->n_src_pieces;
  struct per_app_cdo_entries *src;
  uint64_t src_local_id;
  int index;
  mmbError stat;

  /* create a copy of src layout to play with */
  mmbLayout *target_layout = NULL;
  stat = mmb_layout_create_copy(src_layout, &target_layout);
  assert(stat == MMB_OK);
  
  /*get true number of candidates */
  size_t n_candidates = 0;
  status = mstro_pm__count_intersections(intersection, dst_index, &n_candidates);
  DEBUG("number of candidates %zu \n", n_candidates);
  assert(status == MSTRO_OK);
  /* Initialize candidates */
  status = mstro__pool_create_candidates(candidates, n_candidates, true);
  if (status != MSTRO_OK) {
    return status;
  }
  size_t candidates_index = 0;
  /* find all the required pieces for dst_index, aka our CDO, and fill the list of candidates */
  for (size_t i = 0; i < n_src_pieces; i++) {

    index = dst_index*n_src_pieces + i;
    src = NULL; // initialize

    /* we are only interested if the length of intersection is > 0*/
    if (intersection->overlap[index].length > 0) {
      DEBUG("finding piece %zu with src offset %zu and dst offset %zu, length %zu \n",
                              i,
                              intersection->overlap[index].src_offset,
                              intersection->overlap[index].dst_offset,
                              intersection->overlap[index].length);

      /* point to the current src piece in the layout */
      target_layout->index = i;
      /* find the app and cdo id with a layout matching source distribution */
      status = mstro_pm__find_cdo_with_layout(
                                  app_to_attributes_table,
                                  target_layout,
                                  &src,
                                  &src_local_id);
      assert(status == MSTRO_OK);
      if(src == NULL) {
        DEBUG("One or more required pieces of this distribution is missing and we can not use it \n ");
        mstro_pm_candidates_destroy(*candidates);
        *candidates = NULL;
        return status;

      }
      DEBUG("found piece %zu in app %" PRIappid " with local id %zu \n", candidates_index, src, src_local_id);
      (*candidates)->app[candidates_index] = src;
      (*candidates)->local_id[candidates_index] = src_local_id;
      candidates_index++;

    }
    else { /* intersection is zero ...  nothing to do here */

    }
  }

  stat = mmb_layout_destroy(target_layout);
  assert(stat == MMB_OK);

  return status;
}

static inline
mstro_status
mstro_pm__find_cdo_with_layout(
                            struct per_app_cdo_entries *app_to_attributes_table,
                            mmbLayout *s_layout,
                            struct per_app_cdo_entries **app,
                            uint64_t *local_id) {

  mstro_status status = MSTRO_OK;
  mmbError stat;
  mmbLayout *src_layout = NULL;
  bool default_layout = false;
  struct per_app_cdo_entries *entry, *tmp;
  *app = NULL;

  HASH_ITER(hh, app_to_attributes_table, entry, tmp) {
    struct cdo_handle_entry *h,*t;
    HASH_ITER(hh, entry->handles, h, t) {
      if (h->cdo_state==MSTRO_CDO_STATE_OFFERED) {

        if (!h->dist_layout) {
          WARN("CDO with local-id %zu is flagged as distributed but has no distribution layout ... will create a default one \n", h->local_id);
          stat = mmb_layout_dist_create_default_layout(s_layout, &src_layout);
          assert(stat == MMB_OK);
          default_layout = true;
        }
        else {
          src_layout = h->dist_layout;
          default_layout = false;
        }

        /** try to match the distribution */
        mmbLayoutEquivalence diff;
        mmbError mmb_s;
        mmb_s = mmb_layout_cmp(src_layout, s_layout, &diff);
        assert(MMB_OK == mmb_s);
        /** release the default layout we created */
        if (default_layout) {
          mmb_layout_destroy(src_layout);
        }
        if (diff == MMB_LAYOUT_EQUAL){
          DEBUG("Found entry with local-id %zu for % " PRIappid " to provide CDO \n",
                h->local_id, entry->app);
          *app = entry;
          *local_id = h->local_id;
          /* best case */
          return  status;
        }
      }
    }
  }

  return status;
}


static inline
mstro_status
mstro_pm_candidates_destroy(mstro_pm_candidates *candidates){
    mstro_status status = MSTRO_OK;
    DEBUG("Destroying candidate list \n");
    if(candidates)
    {
      if(candidates->app) {
        free(candidates->app);
      }
      if (candidates->local_id) {
        free(candidates->local_id);
      }

      free(candidates);
    }


    return status;
}

/** find best candidate to serve CDO to app_id
 *
 * Returns best match in *best, or MSTRO_APP_ID_INVALID if none exists.
 *
 * must be called under lock.
 */
static inline
mstro_status
mstro_pm__find_best_provider(
    struct per_app_cdo_entries *app_to_attributes_table,
    mstro_app_id app_id, mstro_pm_candidates **candidates)
{
  struct per_app_cdo_entries *entry, *tmp;
  mstro_status status;
  app_id=app_id; /* silence unused arg warning */
  /* FIXME: this is just greedy */
  /* FIXME: this also does not return the actual local-id */
  HASH_ITER(hh, app_to_attributes_table, entry, tmp) {
    struct cdo_handle_entry *h,*t;
    HASH_ITER(hh, entry->handles, h, t) {
      if(h->cdo_state==MSTRO_CDO_STATE_OFFERED) {
        DEBUG("Found entry with local-id %zu for app % " PRIappid " to provide CDO to app % " PRIappid "\n",
              h->local_id, entry->app, app_id);
        status = mstro__pool_create_candidates(candidates, 1, false);
        if (status != MSTRO_OK) {
          return status;
        }
        (*candidates)->app[0] = entry;
        (*candidates)->local_id[0] = h->local_id;
        return MSTRO_OK;
      }
    }
  }
  return MSTRO_OK;
}

/** find a dist_layout for the current cdo
 *
 * Returns a pointer to dist_layout of the current cdo
 *
 * must be called under lock.
 */
static inline
mstro_status
mstro_pm__find_dist_layout(
    struct per_app_cdo_entries *app_to_attributes_table,
    mmbLayout **default_layout)
{
  struct per_app_cdo_entries *entry, *tmp;
  mstro_status status;
  /* FIXME: this is just greedy */
  HASH_ITER(hh, app_to_attributes_table, entry, tmp) {
    struct cdo_handle_entry *h,*t;
    HASH_ITER(hh, entry->handles, h, t) {
      if(h->dist_layout) {
        *default_layout = h->dist_layout;
        return MSTRO_OK;
      }
    }
  }
  /* We should not reach this part ...if is_distributed is set, then at least one has a distribution */
  ERR("Can not find a distribution for this CDO \n");
  return MSTRO_FAIL;
}

/** look up per-app cdo entry. Must be called unter CDO_REGISTRY_LOCK. */
static inline
mstro_status
mstro_pm_cdo_app_lookup(const struct mstro_cdo_id* cdo_id,
                        mstro_app_id app_id,
                        struct per_app_cdo_entries **app_entry, struct cdo_handle_entry **handle_entry)
{
  struct mstro_pm_cdo_registry_entry *regentry=NULL;

  struct mstro_cdo_id head = *cdo_id;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  HASH_FIND(hh, g_mstro_pm_cdo_registry,
            &head, sizeof(struct mstro_cdo_id), regentry);
  if(!regentry) {
    WITH_CDO_ID_STR(idstr, &head,
                    DEBUG("No regentry for cdo %s\n", idstr););
    *app_entry = NULL;
    *handle_entry = NULL;
    return MSTRO_FAIL;
  } else {
    struct per_app_cdo_entry *per_app_entry;
    HASH_FIND(hh, regentry->app_to_attributes,
              &app_id, sizeof(app_id), *app_entry);
    if(*app_entry==NULL) {
      WITH_CDO_ID_STR(idstr, &head,
                      DEBUG("No regentry for app %" PRIappid " for CDO %s\n", app_id, idstr););

      *handle_entry = NULL;
      return MSTRO_FAIL;
    } else {
      HASH_FIND(hh, (*app_entry)->handles,
                &cdo_id->local_id, sizeof(cdo_id->local_id), *handle_entry);
      if(*handle_entry)
        return MSTRO_OK;
      else {
        WITH_CDO_ID_STR(idstr, &head,
                        DEBUG("No handle entry for app %" PRIappid " for CDO %s, local-id %zu\n",
                              app_id, idstr, cdo_id->local_id););
        return MSTRO_FAIL;
      }
    }
  }
}


static inline
mstro_status
mstro_pm__send_transfer_init(struct mstro_pm_demand_queue_entry *e,
                             struct per_app_cdo_entries *candidate,
                             uint64_t candidate_local_id, 
                             size_t n_segments,
                             bool is_distributed) {

  mstro_status status = MSTRO_OK;

  WITH_CDO_ID_STR(dststr, &e->cdo_id, {
      DEBUG("Selected provider for CDO %s, app %" PRIappid
            " using local-id %" PRId64" of app %" PRIappid "\n",
            dststr, e->requestor,
            candidate_local_id, candidate->app);});

  Mstro__Pool__CDOID srccdoid = MSTRO__POOL__CDOID__INIT;
  srccdoid.qw0 = e->cdo_id.qw[0];
  srccdoid.qw1 = e->cdo_id.qw[1];
  srccdoid.local_id = candidate_local_id;

  Mstro__Pool__CDOID dstcdoid = MSTRO__POOL__CDOID__INIT;
  dstcdoid.qw0 = e->cdo_id.qw[0];
  dstcdoid.qw1 = e->cdo_id.qw[1];
  dstcdoid.local_id = e->cdo_id.local_id;


  Mstro__Pool__Appid dst_appid = MSTRO__POOL__APPID__INIT;
  dst_appid.id = e->requestor;
  NOISE("Initiating a transfer for requestor %zu\n",
       dst_appid.id);

  Mstro__Pool__InitiateTransfer it
      = MSTRO__POOL__INITIATE_TRANSFER__INIT;
  it.srccdoid = &srccdoid;
  it.dstcdoid = &dstcdoid;
  it.dst_appid = &dst_appid;
  it.dst_attributes = e->req_attributes;

  /* set the number of tickets required to fill the data for this CDO*/
  it.n_segments = (int64_t) n_segments;
  /* flag this ticket as a (re)distribution or not */
  it.distributed_cdo = is_distributed;

  struct mstro_pm_app_registry_entry *app_entry = NULL;
  mstro_status s;
  s = mstro_pm_app_lookup(dst_appid.id, &app_entry);
  if(s != MSTRO_OK || app_entry==NULL) {
    ERR("Target %zu not in app table\n", e->requestor);
    return MSTRO_FAIL;
  }
  DEBUG("InitiateTransfer msg packed with serialized ep desc %s\n",
       app_entry->serialized_desc);
  it.dst_serialized_endpoint = app_entry->serialized_desc;
  DEBUG("entry has req methods %p\n", e->req_methods);
  it.methods = e->req_methods;
  it.cp = 0; // FIXME When PM starts doing smart stuff. I am
             // here arbitrarily choosing not to keep a
             // local copy on the src after
             // transfer. Perhaps PM wants to communicate to
             // keep the transport file/obj on the dst, in
             // which TODO case add a "keep_file" field in
             // the InitiateTransfer msg

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;

  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&it);
  if(status!=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        it.base.descriptor->name);
    return MSTRO_FAIL;
  }

  status = mstro_pmp_send_nowait(candidate->app, &msg);

  switch(status) {
    case MSTRO_OK:
      WITH_CDO_ID_STR(
          idstr, &e->cdo_id,
          INFO("Sent initiate-transfer for %s to app %zu (for app %zu)\n",
               idstr,
               candidate->app, it.dst_appid->id);
                      );
      break;
    default:
      ERR("Failed to send initiate-transfer message to app %zu: %d (%s)\n",
          candidate->app, status, mstro_status_description(status));
      return MSTRO_FAIL;
  }
  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_TICKETS, 1);

  return status;
}

static inline
mstro_status
mstro_pm__send_transfer_init_to_candidates(
                                      struct mstro_pm_demand_queue_entry *e,
                                      mstro_pm_candidates *candidates) {
  mstro_status status = MSTRO_OK;
  if((!candidates) || (!e)){
    ERR("Invalid list of candidates or required cdo\n");
    return MSTRO_INVARG;
  }
  DEBUG("Number of candidates %zu \n", candidates->n_sources);
  for (size_t i = 0; i < candidates->n_sources ; i++) {
    status = mstro_pm__send_transfer_init(e, candidates->app[i], candidates->local_id[i], candidates->n_sources, candidates->is_distributed);
    if (status != MSTRO_OK) {
      return status;
    }
  }
  /* set the number of required transmissions on cdo handle*/
  DEBUG("Set the number of required transmissions on cdo handle\n");
  struct per_app_cdo_entries *app_entry;
  struct cdo_handle_entry *handle_entry;
  status = mstro_pm_cdo_app_lookup(&e->cdo_id, e->requestor, &app_entry, &handle_entry);
  if(status!=MSTRO_OK) {
    ERR("Failed to find CDO entry \n");
      return status; 
  }
  handle_entry->n_segments = candidates->n_sources;

  DEBUG("Disposing demand queue entry that has been handled\n");
  status = mstro_pm_demand_queue_entry__destroy(e);
  if(status!=MSTRO_OK) {
    ERR("Failed to de-allocate demand queue entry\n");
    return MSTRO_FAIL;
  }
  return status;
}

static inline
mstro_status
mstro_pm__handle_demands(struct mstro_pm_demand_queue_entry *q)
{
  mstro_status status = MSTRO_OK;
  struct mstro_pm_demand_queue_entry *unhandled=NULL;
  size_t len=0;
  struct mstro_pm_demand_queue_entry *e;
  mmbError stat;
  mmbLayout *default_layout = NULL;
  mmbLayout *src_layout;
  LL_COUNT(q,e,len);

  NOISE("Handling %d demand queue entries\n", len);

  while(q!=NULL) {
    e = q;
    q = q->next;
    /* try to handle e */
    WITH_LOCKED_CDO_REGISTRY(
        struct mstro_pm_cdo_registry_entry *regentry=NULL;

        struct mstro_cdo_id head = e->cdo_id;
        head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

        HASH_FIND(hh, g_mstro_pm_cdo_registry,
                  &head, sizeof(struct mstro_cdo_id), regentry);
        if(!regentry) {
          /* should not happen */
          WITH_CDO_ID_STR(idstr, &e->cdo_id,
                          ERR("No CDO registry entry for DEMANDed CDO %s\n",
                              idstr););
        } else {
          mstro_pm_candidates *candidates = NULL;

          if (regentry->is_distributed) {

            if (!e->req_dist_layout) {
              WARN("Required CDO is flagged as distributed but has no distribution layout ... will create a default one \n");
              /* look for a cdo distribution as a source dist */
              status = mstro_pm__find_dist_layout(regentry->app_to_attributes, &src_layout);
              stat = mmb_layout_dist_create_default_layout(src_layout, &default_layout);
              assert(stat == MMB_OK);
              e->req_dist_layout = default_layout;
            }

            /** distributed cdo  -- need to handle the distribution */
            mstro_pm__dist_cdo_find_best_provider(regentry->app_to_attributes,
                                              e,
                                              &candidates);
          }
          else {
            mstro_pm__find_best_provider(regentry->app_to_attributes,
                                         e->requestor,
                                         &candidates);
          }
          if(candidates==NULL) {
            /* no offer for this demand */
            LL_PREPEND(unhandled, e);
          } else { // send transfer init
            status = mstro_pm__send_transfer_init_to_candidates(e, candidates);
            if (status != MSTRO_OK) {
              goto BAILOUT_UNLOCK;
            }
            mstro_pm_candidates_destroy(candidates);
          } // end else
        }
   BAILOUT_UNLOCK:
        ;
        );
  }

  if(unhandled!=NULL) {
    /* append the leftovers to queue for another round later */
    int s=pthread_mutex_lock(&g_mstro_pm_demand_queue_mtx);
    if(s!=0) {
      ERR("Failed to lock demand queue mutex: %d\n", s);
      return MSTRO_FAIL;
    }
    LL_CONCAT(g_mstro_pm_demand_queue, unhandled);
    s=pthread_mutex_unlock(&g_mstro_pm_demand_queue_mtx);
    if(s!=0) {
      ERR("Failed to unlock demand queue mutex: %d\n", s);
      return MSTRO_FAIL;
    }
  }

  return status;
}

static inline
void unlock_demand_queue(void *arg)
{
  arg=arg; /* silence unused arg warning */
  pthread_mutex_unlock(&g_mstro_pm_demand_queue_mtx);
}

/* This function is called by the transport initiator thread to process the demand queue once:
 *  wait on cvar, drain queue, unlock, process drained queue entries, quit */
mstro_status
mstro_pm_handle_demand_queue(void)
{
  mstro_status status;
  struct mstro_pm_demand_queue_entry *q=NULL;
  int s;

  
    s = pthread_mutex_lock(&g_mstro_pm_demand_queue_mtx);
    if(s!=0) {
      ERR("Failed to lock demand queue mutex: %d (%s)\n", s, strerror(s));
      status = MSTRO_FAIL;
      goto BAILOUT;
    }
    /* this function will be canceled by the pool manager thread at the
    * cond_wait */
    pthread_cleanup_push(unlock_demand_queue, NULL);


    while(g_mstro_pm_demand_queue==NULL) {
      /* no entries during last processing step; we hold the lock: go to sleep */
      /* go to sleep until something interesting happens */
      s = pthread_cond_wait(&g_mstro_pm_demand_queue_cvar,
                        &g_mstro_pm_demand_queue_mtx);
      if(s!=0) {
        ERR("Failed to wait on demand queue: %d (%s)\n", s, strerror(s));
        status = MSTRO_FAIL;
        goto BAILOUT;
      }
      DEBUG("Woke up on demand queue change\n");
    } 
          
    /* once we wake up we hold the lock again: Pull off one round of
      * entries */
    /* drain */
    q=g_mstro_pm_demand_queue;
    g_mstro_pm_demand_queue = NULL;
    

    /*unlock the demand queue while we handle the entries */
    pthread_cleanup_pop(1);

    status = mstro_pm__handle_demands(q);
    if(status!=MSTRO_OK) {
      ERR("Failure processing demand queue entries\n");
      goto BAILOUT;
    }

    /* this will jump into cleanup/non-local exit if we were canceled */
    pthread_testcancel();

    /* if someone added new entries we'll loop */
  
  
BAILOUT:
  return status;
}

static
mstro_status
mstro_pm__cdo_handle_entry__create(uint64_t local_id, mstro_cdo_state state,
                                   struct cdo_handle_entry **result)
{
  assert(result!=NULL);
  assert(local_id!=MSTRO_CDO_LOCAL_ID_NONE);
  assert(state!=MSTRO_CDO_STATE_INVALID);

  struct cdo_handle_entry *tmp = malloc(sizeof(struct cdo_handle_entry));
  if(tmp==NULL) {
    ERR("Cannot allocate per-CDO handle entry\n");
    return MSTRO_NOMEM;
  }
  tmp->local_id = local_id;
  tmp->cdo_state = state;
  tmp->attributes = NULL;
  tmp->n_segments = 0;
  tmp->dist_layout = NULL;
  *result = tmp;
  return MSTRO_OK;
}

static inline
mstro_status
mstro_pm__cdo_handle_entry__destroy(struct cdo_handle_entry *h)
{
  mstro_status m_status = MSTRO_OK;
  mmbError stat = MMB_OK;
  if(h==NULL)
    return MSTRO_INVARG;
  if(h->attributes)
    mstro__pool__attributes__free_unpacked(h->attributes, NULL);
  if(h->dist_layout){
    stat = mmb_layout_destroy(h->dist_layout);
    m_status = (stat == MMB_OK)? MSTRO_OK: MSTRO_FAIL;
  }
  free(h);
  return m_status;
}


static
mstro_status
mstro_pm__per_app_entries__create(mstro_app_id app, uint64_t local_id, mstro_cdo_state state,
                                  struct per_app_cdo_entries **result)
{
  assert(result!=NULL);
  mstro_status status;

  *result = malloc(sizeof(struct per_app_cdo_entries));
  if(*result==NULL) {
    ERR("Cannot allocate new per-app CDO entry\n");
    status=MSTRO_NOMEM;
    goto BAILOUT_ERR;
  }
  (*result)->app = app;
  (*result)->handles = NULL;

  struct cdo_handle_entry *tmp;
  status = mstro_pm__cdo_handle_entry__create(local_id, state, &tmp);
  if(status!=MSTRO_OK) {
    ERR("Cannot allocate per-CDO handle entry\n");
    status=MSTRO_NOMEM;
    goto BAILOUT_ERR;
  }
  HASH_ADD(hh, (*result)->handles, local_id, sizeof(uint64_t), tmp);
  status = MSTRO_OK;

BAILOUT:
  return status;

BAILOUT_ERR:
  if(*result)
    free(*result);
  goto BAILOUT;
}


mstro_status
mstro_pm_cdo_registry_send_declare_ack(mstro_pool_operation op)
{
    mstro_status status = MSTRO_UNIMPL;
    /* build DECLARE_ACK */
    Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
    cdoid.qw0 = op->cdoid.qw[0];
    cdoid.qw1 = op->cdoid.qw[1]; 
    cdoid.local_id = op->cdoid.local_id;
  
    Mstro__Pool__DeclareAck declare_ack = MSTRO__POOL__DECLARE_ACK__INIT;
    declare_ack.serial = op->msg->declare->serial;
    declare_ack.cdoid = &cdoid;
    declare_ack.channel = op->appid; /* FIXME: sufficient for now */

    Mstro__Pool__MstroMsg msg_ack = MSTRO__POOL__MSTRO_MSG__INIT;
    status = mstro_pmp_package(&msg_ack, (ProtobufCMessage*)&declare_ack);
    if(status!=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            declare_ack.base.descriptor->name);
        return status;
    }
    status = mstro_pmp_send_nowait(op->appid, &msg_ack);
    if(status!=MSTRO_OK) {
        ERR("Failed to send DECLARE_ACK: %d (%s)\n",
            status, mstro_status_description(status));
    }

    WITH_CDO_ID_STR(idstr, &(op->cdoid),
                  INFO("Sent DECLARE-ACK for app %" PRIappid " CDO (global id: `%s')\n",
                       op->appid, idstr);
                  );
    return status;

}

/**Wrapper for mstro_pm_cdo_registry_declare  */
mstro_status
mstro_pm_cdo_registry_declare_op(mstro_pool_operation op)
{
  op->status =  mstro_pm_cdo_registry_declare(op->appid,
                                      op->msg->declare->cdo_name,
                                      op->msg->declare->serial,
                                      &(op->cdoid));

  if ((op->status != MSTRO_WOULDBLOCK) && (op->status != MSTRO_OK))
  {
    ERR("Failed to add CDO %s declaration to registry\n", op->msg->declare->cdo_name);
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*engine will execute next step, i.e. send ack */
    return MSTRO_OK; /**We should continue to send the ack with error status rather than dropping the operation*/
  }
  else /**MSTRO_OK*/
  {
    WITH_CDO_ID_STR(idstr, &(op->cdoid),
              INFO("app %" PRIappid " declared CDO (global id: `%s')\n",
                       op->appid, idstr);
                  );
    return MSTRO_OK;
  }
  
}

mstro_status
mstro_pm_cdo_registry_declare(mstro_app_id app,
                              const char *cdo_name,
                              uint64_t local_id,
                              struct mstro_cdo_id *cdo_id_p)
{
  struct mstro_pm_app_registry_entry *e;
  assert(app!=MSTRO_APP_ID_INVALID);

  if(cdo_name==NULL)
    return MSTRO_INVARG;
  if(cdo_id_p==NULL)
    return MSTRO_INVOUT;

  /* compute default ID */
  mstro_status status = mstro_cdo_id_from_name(cdo_name, cdo_id_p);
  if(status!=MSTRO_OK) {
    ERR("Failed to compute CDO ID\n");
    goto BAILOUT;
  }
  // cdo_id_p has local-id set to NONE

  struct mstro_cdo_id head = *cdo_id_p;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  // now store proper value
  cdo_id_p->local_id = local_id;

  /* check whether ID has been seen before */
  struct mstro_pm_cdo_registry_entry *regentry=NULL;
  WITH_LOCKED_CDO_REGISTRY(
      HASH_FIND(hh, g_mstro_pm_cdo_registry,
                &head, sizeof(struct mstro_cdo_id), regentry);
      if(!regentry) {
        WITH_CDO_ID_STR(idstr, &head, {
            DEBUG("New CDO %s -> %s\n", cdo_name, idstr);
          });
        regentry = malloc(sizeof(struct mstro_pm_cdo_registry_entry));
        if(regentry==NULL) {
          ERR("Cannot allocate new CDO registry entry\n");
          status=MSTRO_NOMEM;
          goto BAILOUT_UNLOCK;
        }
        regentry->cdo_name = strdup(cdo_name);
        if(regentry->cdo_name==NULL) {
          ERR("Cannot allocate name of new CDO regentry\n");
          free(regentry);
          status=MSTRO_NOMEM;
          goto BAILOUT_UNLOCK;
        }
        memcpy(&(regentry->cdo_id), &head, sizeof(struct mstro_cdo_id));
        regentry->app_to_attributes = NULL;
        regentry->is_distributed = false; /* init value*/
        HASH_ADD(hh, g_mstro_pm_cdo_registry,
                 cdo_id, sizeof(struct mstro_cdo_id), regentry);
      }

      /* We now have a regentry, possibly without app registration */
      if(0!=strcmp(regentry->cdo_name, cdo_name)) {
        WITH_CDO_ID_STR(idstr, &head, {
            ERR("Jackpot! You hit a hash collision on the CDO ID: |%s| and |%s| both map to %s\n",
                regentry->cdo_name, cdo_name, idstr);
            abort();
          });
      } else {
        /* have previous registration for this CDO */
        struct per_app_cdo_entries *per_app_entries;
        HASH_FIND(hh, regentry->app_to_attributes,
                  &app, sizeof(app), per_app_entries);
        if(per_app_entries) {
          /* have entries for this app already */
          struct cdo_handle_entry *tmp;
          status = mstro_pm__cdo_handle_entry__create(local_id, MSTRO_CDO_STATE_DECLARED, &tmp);
          if(status!=MSTRO_OK) {
            ERR("Cannot allocate per-CDO handle entry\n");
            status=MSTRO_NOMEM;
          }
          struct cdo_handle_entry *existing_entry=NULL;
          HASH_FIND(hh, per_app_entries->handles,
                    &local_id, sizeof(local_id), existing_entry);
          if(existing_entry) {
            WITH_CDO_ID_STR(idstr, cdo_id_p,
                            ERR("Duplicate DECLARE for same handle: app %" PRIappid ", id %s, local-id %" PRIx64 "\n",
                                app, idstr, local_id););
          } else {
            HASH_ADD(hh, per_app_entries->handles,
                     local_id, sizeof(local_id), tmp);
          }
        } else {
          /* first entry for thus app */
          DEBUG("Creating first entry for app %" PRIappid "\n", app); 
          status = mstro_pm__per_app_entries__create(app, local_id, MSTRO_CDO_STATE_DECLARED,
                                                     &per_app_entries);
          if(status!=MSTRO_OK) {
            ERR("Cannot allocate new per-app CDO entry\n");
            goto BAILOUT_UNLOCK;
          }
          HASH_ADD(hh, regentry->app_to_attributes,
                   app, sizeof(mstro_app_id), per_app_entries );
        }
      }
      DEBUG("CDO %s now has been registered by %d applications\n",
            cdo_name, HASH_COUNT(regentry->app_to_attributes));
      status = MSTRO_OK;
 BAILOUT_UNLOCK:
      ;
                           );

BAILOUT:
  return status;
}

mstro_status
mstro_pm_cdo_registry_find(mstro_app_id app, const struct mstro_cdo_id *id)
{
  bool found=false;

  struct mstro_pm_cdo_registry_entry *regentry=NULL;

  struct mstro_cdo_id tmp = *id;
  tmp.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  WITH_LOCKED_CDO_REGISTRY({
      HASH_FIND(hh, g_mstro_pm_cdo_registry,
                &tmp, sizeof(struct mstro_cdo_id), regentry);
      if(regentry) {
        struct per_app_cdo_entries *per_app_entries;
        HASH_FIND(hh, regentry->app_to_attributes,
                  &app, sizeof(app), per_app_entries);
        if(per_app_entries) {
          struct cdo_handle_entry *e;
          HASH_FIND(hh, per_app_entries->handles,
                    &(id->local_id), sizeof(id->local_id), e);
          if(e)
            found=true;
        }
      }
    });
  if(found)
    return MSTRO_OK;
  else
    return MSTRO_FAIL;
}

void
mstro_pm_cdo_registry_print_attributes(const struct mstro_cdo_id *cdoid,
                                       const mstro_app_id app_id,
                                       Mstro__Pool__Attributes *attributes)
{
  WITH_CDO_ID_STR(idstr, cdoid,{
          DEBUG("Stored attributes for on %s for app %" PRIappid "\n", idstr, app_id);
        });
  Mstro__Pool__Attributes__Map *kv_map = attributes->kv_map;
  if(kv_map==NULL) {
    DEBUG("No attributes\n");
  } else {
    DEBUG("Found %d entries...\n", kv_map->n_map);
    for(size_t i=0; i<kv_map->n_map; i++) {
      DEBUG("Key: `%s`\n", kv_map->map[i]->key);
    }
  }
}

mstro_status
mstro_pm_cdo_registry_store_attributes(const struct mstro_cdo_id *cdo_id,
                                       mstro_app_id app_id,
                                       Mstro__Pool__Attributes *attributes)
{
  struct mstro_pm_cdo_registry_entry *regentry=NULL;
  bool found=false;
  mstro_status status;
  mmbLayout *dist_layout = NULL;
  struct mstro_cdo_id head = *cdo_id;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  WITH_LOCKED_CDO_REGISTRY(
      HASH_FIND(hh, g_mstro_pm_cdo_registry,
                &head, sizeof(struct mstro_cdo_id), regentry);
      if(regentry) {
        struct per_app_cdo_entries *per_app_entries;
        HASH_FIND(hh, regentry->app_to_attributes,
                  &app_id, sizeof(app_id), per_app_entries);
        if(per_app_entries) {
          struct cdo_handle_entry *e;
          HASH_FIND(hh, per_app_entries->handles,
                    &(cdo_id->local_id), sizeof(cdo_id->local_id), e);
          if(e) {
            if(e->attributes==NULL) {
              e->attributes = attributes;
              found=true;
              mstro_attribute_pool_find_dist_layout(attributes, &dist_layout);
              e->dist_layout = dist_layout;
              /** set is_distributed flag on the global CDO entry*/
              if(dist_layout) {
                regentry->is_distributed = true;
              }
            } else {
              WITH_CDO_ID_STR(idstr, cdo_id, {
                  ERR("Trying to overwrite attributes for %s from app %" PRIappid "\n",
                      idstr, app_id);
                });
            }
          } else {
            WITH_CDO_ID_STR(idstr, cdo_id, {
                ERR("No matching local-id handle for app %" PRIappid " for %s\n", app_id, idstr);
              };);
          }
        } else {
          WITH_CDO_ID_STR(idstr, cdo_id, {
              ERR("No entries at all for app %" PRIappid " for %s\n", app_id, idstr);
            };);
        }
      } else {
        WITH_CDO_ID_STR(headid, &head, {
            WITH_CDO_ID_STR(idstr, cdo_id, {
                ERR("No entries at all for %s (tried on behalf of app %" PRIappid ", CDO %s)\n",
                    headid, app_id, idstr);});};);
      });

  return found? MSTRO_OK : MSTRO_FAIL;
}

/** check whether state transition is valid */
static inline
bool
mstro_pm_cdo_registry__valid_state_transition(const struct mstro_cdo_id *cdoid,
                                              const mstro_app_id appid,
                                              mstro_cdo_state old_state,
                                              mstro_cdo_state new_state)
{
  assert(cdoid!=NULL);
  char *msg=NULL;
  bool unchecked=false;

  switch(new_state) {
    case MSTRO_CDO_STATE_INVALID:
      msg = "Cannot transition to INVALID state on PM";
      break;

    case MSTRO_CDO_STATE_CREATED:
      if(old_state!=MSTRO_CDO_STATE_INVALID)
        msg = "Cannot step to CREATED state from any other valid state";
      break;

    case MSTRO_CDO_STATE_DECLARED:
      if(old_state!=MSTRO_CDO_STATE_CREATED)
        msg = "Cannot DECLARE unless freshly CREATED";
      break;

    case MSTRO_CDO_STATE_SEALED:
      if(old_state!=MSTRO_CDO_STATE_DECLARED)
        msg = "Cannot SEAL unless freshly DECLARED";
      break;

    case MSTRO_CDO_STATE_OFFERED:
      if(old_state != MSTRO_CDO_STATE_DECLARED)
        msg = "Cannot OFFER unless properly DECLARED";
      break;

    case MSTRO_CDO_STATE_WITHDRAWN:
      if(!  (old_state == MSTRO_CDO_STATE_OFFERED))
        msg = "Cannot WITHDRAW unless OFFERED (and no other flags set)";
      break;

    case MSTRO_CDO_STATE_REQUIRED:
      if(!  (old_state == MSTRO_CDO_STATE_DECLARED))
        msg = "Cannot REQUIRE unless DECLARED (and no other flags set)";
      break;

    case MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_IN_TRANSPORT:
      if(!  (old_state == MSTRO_CDO_STATE_REQUIRED))
        msg = "Cannot flag REQUIRED as IN_TRANSPORT unless REQUIRED (and no other flags set)";
      break;

    case MSTRO_CDO_STATE_DEMANDED|MSTRO_CDO_STATE_IN_TRANSPORT:
      if(! (old_state == (MSTRO_CDO_STATE_REQUIRED)))
	msg = "Cannot set DEMANDED+IN-TRANSPORT unless REQUIRED (and no other flags set)";
      break;

    case MSTRO_CDO_STATE_RETRACTED:
      if(! (   (old_state == MSTRO_CDO_STATE_REQUIRED)
            || (old_state == (MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_IN_TRANSPORT))))
        msg = "Cannot set RETRACTED unless REQUIRED or REQUIRED+IN-TRANSPORT (and no other flags set)";
      break;

    default:
      msg="Unchecked state transition";
      unchecked=true;
      break;
  }

  if(msg==NULL) {
    return true;
  } else {
    if(unchecked) {
      WITH_CDO_ID_STR(
          idstr, cdoid,
          WARN("Unchecked state transition for %s, app %zu, %s to %s\n",
               idstr, appid,
               mstro_cdo_state_describe(old_state),
               mstro_cdo_state_describe(new_state),
               msg););
      return true; /* let it pass */
    } else {
      WITH_CDO_ID_STR(
          idstr, cdoid,
          ERR("Illegal state transition for %s, app %zu, %s (%" PRIu32 ") to %s (%" PRIu32 "): %s\n",
              idstr, appid,
              mstro_cdo_state_describe(old_state), old_state,
              mstro_cdo_state_describe(new_state), new_state,
              msg););
      return false;
    }
  }
}

/* must be called under lock */
static inline
mstro_status
mstro_pm_cdo_registry__set_state(const struct mstro_cdo_id *cdoid,
                                 const mstro_app_id appid,
                                 mstro_cdo_state new_state,
                                 struct per_app_cdo_entries *per_app_entries)
{
  assert(cdoid!=NULL);
  assert(per_app_entries!=NULL);
  assert(cdoid->local_id!=MSTRO_CDO_LOCAL_ID_NONE);

  /* find per_cdo_entry */
  struct cdo_handle_entry *e;
  HASH_FIND(hh, per_app_entries->handles,
            &(cdoid->local_id), sizeof(cdoid->local_id), e);
  if(!e) {
    WITH_CDO_ID_STR(idstr, cdoid,
                    ERR("Cannot find handle-entry for app %d, CDO %s\n",
                        appid, idstr););
    return MSTRO_NOENT;
  }

  /* intentionally inside an assert, so that it can be compiled-out */
  assert(mstro_pm_cdo_registry__valid_state_transition(
      cdoid, appid,
      e->cdo_state, new_state));

  WITH_CDO_ID_STR(idstr, cdoid, {
      DEBUG("Stepping CDO state for %s for app %zu from %d (%s) to %d (%s)\n",
            idstr, appid,
            e->cdo_state,
            mstro_cdo_state_describe(e->cdo_state),
            new_state,
            mstro_cdo_state_describe(new_state)
            );
    });
  e->cdo_state = new_state;
  return MSTRO_OK;
}

static inline 
mstro_status
mstro_pm_cdo_registry_update_state(const struct mstro_cdo_id *cdoid,
                                   const mstro_app_id app_id,
                                   mstro_cdo_state new_state)
{
  struct mstro_pm_cdo_registry_entry *regentry=NULL;
  bool found=false;

  struct mstro_cdo_id head = *cdoid;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  WITH_LOCKED_CDO_REGISTRY(
      HASH_FIND(hh, g_mstro_pm_cdo_registry,
                &head, sizeof(struct mstro_cdo_id), regentry);
      if(regentry) {
        struct per_app_cdo_entries *per_app_entries;
        HASH_FIND(hh, regentry->app_to_attributes,
                  &app_id, sizeof(app_id), per_app_entries);
        if(per_app_entries) {
          mstro_status s =
              mstro_pm_cdo_registry__set_state(cdoid, app_id,
                                               new_state, per_app_entries);
          if(s==MSTRO_OK) {
            found=true;
          }
        }
      });

  if(found) {
    mstro_status status = mstro_pm__notify_cdo_registry_change();
    if(status!=MSTRO_OK) {
      ERR("Failed to notify waiters of registry change\n");
      return status;
    }
  }

  return found? MSTRO_OK : MSTRO_FAIL;
}


/*wrapper for mstro_pm_cdo_registry_update_state to work with operations */
mstro_status
mstro_pm_cdo_registry_update_state_op(mstro_pool_operation op)
{
  switch (op->kind)
  {
    case MSTRO_OP_PM_OFFER:
      op->status = mstro_pm_cdo_registry_update_state(&(op->cdoid), op->appid, MSTRO_CDO_STATE_OFFERED);
      // FIXME:
      WARN("If this is a group CDO, members will not be OFFERED at this time\n");
      break;
    case MSTRO_OP_PM_REQUIRE:
      op->status = mstro_pm_cdo_registry_update_state(&(op->cdoid), op->appid, MSTRO_CDO_STATE_REQUIRED);
      WARN("Not scheduling any action at REQUIRE time\n");
      // FIXME:
      WARN("If this is a group CDO, members will not be REQUIRED at this time\n");
      break;
    case MSTRO_OP_PM_RETRACT:
      op->status = mstro_pm_cdo_registry_update_state(&(op->cdoid), op->appid, MSTRO_CDO_STATE_RETRACTED);
      /* for groups, do recursion ? */
      WARN("If this is a group CDO, members will not be RETRACTED at this time\n");
      break;
    case MSTRO_OP_PM_DEMAND:
      op->status =  mstro_pm_cdo_registry_update_state(&(op->cdoid), op->appid, MSTRO_CDO_STATE_DEMANDED|MSTRO_CDO_STATE_IN_TRANSPORT);
      break;
    default:
      ERR("Invalid operation\n");
      return MSTRO_INVARG;
  }
  if (op->status == MSTRO_OK)
  {
    WITH_CDO_ID_STR(idstr, &(op->cdoid),
      INFO("app %" PRIappid " %sED CDO (global id: `%s')\n",
          op->appid, mstro_pool_op_kind_to_string(op->kind),idstr);
           );
  }
  else /**op->status == MSTRO_FAIL */
  {
    WITH_CDO_ID_STR(idstr, &(op->cdoid),
                  ERR("CDO id %s is not known here for app %" PRIappid " \n",
                       idstr, op->appid);
                  );
    /**If the operation failed we should not notify subscribers ... we should propagate it to issuer directly*/
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1;
  }
  return MSTRO_OK; /**operation status will be probagated to operation issuer*/
}


static inline
mstro_status
mstro_pm__per_app_cdo_entry_dispose(struct per_app_cdo_entries *e)
{
  if(e==NULL)
    return MSTRO_INVARG;
  struct cdo_handle_entry *h,*tmp;
  mstro_status s=MSTRO_OK;

  HASH_ITER(hh, e->handles, h, tmp) {
    HASH_DEL(e->handles, h);
    s |= mstro_pm__cdo_handle_entry__destroy(h);
  }
  free(e);

  return s;
}

/** De-Register the application @ref id */
mstro_status
mstro_pm_app_deregister(mstro_pool_operation op)
{
  mstro_status status=MSTRO_OK;
  mstro_app_id key = op->appid;

  if(g_mstro_pm_cdo_registry) {
    DEBUG("CDO registry empty when deregistering for app %" PRIappid "\n", key);
    goto DROP_APP_ENTRY;
  }

  WARN("Not checking whether app %zu holds permanent CDOs that need to be preserved\n", key);
  WARN("Not checking whether app %zu offers any CDOs that will be needed by others\n", key);

  if(key==MSTRO_APP_ID_MANAGER) {
    DEBUG("De-registering pool manager connection\n");
  }

  WITH_LOCKED_CDO_REGISTRY({
      struct mstro_pm_cdo_registry_entry *e=NULL;
      struct mstro_pm_cdo_registry_entry *tmp=NULL;
      DEBUG("Have %zu CDO registry entries\n", HASH_COUNT(g_mstro_pm_cdo_registry));

      HASH_ITER(hh, g_mstro_pm_cdo_registry, e, tmp) {
        struct per_app_cdo_entries *app_record;
        WITH_CDO_ID_STR(idstr, &e->cdo_id,
                        DEBUG("Inspecting CDO registry entry for %s\n", idstr););
        HASH_FIND(hh, e->app_to_attributes,
                  &key, sizeof(mstro_app_id),
                  app_record);
        DEBUG("Record for app %" PRIappid ": %p\n", key, app_record);
        if(app_record==NULL) {
          ; /* this app not on list for this CDO */
        } else {
          WITH_CDO_ID_STR(idstr, &e->cdo_id,
                          DEBUG("Found app %" PRIappid " listed for CDO %s with %zu handles\n",
                                key, idstr, HASH_COUNT(app_record->handles)););
          struct cdo_handle_entry *h;
          struct cdo_handle_entry *tmp;
          HASH_ITER(hh, app_record->handles, h, tmp) {
            switch(h->cdo_state) {
              case MSTRO_CDO_STATE_DECLARED:
                break; /* ok, declared and never sealed */
              case MSTRO_CDO_STATE_SEALED:
                break; /* ok, sealed and never offered */
              case MSTRO_CDO_STATE_OFFERED:
                WITH_CDO_ID_STR(idstr, &e->cdo_id,
                                ERR("LEAVE request from app %" PRIappid " when CDO %s (local-id %" PRIx64 ") is still OFFERED\n",
                                    key, idstr, h->local_id);
                                );
                break;
              case MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY:
              case MSTRO_CDO_STATE_WITHDRAWN:
              case MSTRO_CDO_STATE_RETRACTED_GLOBALLY:
              case MSTRO_CDO_STATE_RETRACTED:
              case MSTRO_CDO_STATE_DEAD:
                break; /* ok, no one could need it */
              default:
                WITH_CDO_ID_STR(idstr, &e->cdo_id,
                                ERR("Unexpected PM cdo registry state for CDO %s, local-id % " PRIx64 \
                                    ", app %" PRIappid ": %d (%s)\n",
                                    idstr, h->local_id, key, h->cdo_state,
                                    mstro_cdo_state_describe(h->cdo_state));
                                );
                break;
            }
          }
        }
        HASH_DEL(e->app_to_attributes, app_record);
        mstro_pm__per_app_cdo_entry_dispose(app_record);
      }
    });
  DEBUG("Dropped all CDO registry entries for app %zu\n", key);

DROP_APP_ENTRY:
  /* We can not drop the reference to the app itself because we could
   * not send the BYE otherwise. Instead we mark the entry 'dead' and can recycle it on re-join */
  WITH_LOCKED_APP_REGISTRY({
      struct mstro_pm_app_registry_entry *e=NULL;
      HASH_FIND(hh, g_mstro_pm_app_registry, &key, sizeof(key), e);
      if(e!=NULL) {
        e->dead = true;
      } else {
        ERR("Registry entry for app %" PRIappid " vanished\n", key);
      }
    });
  DEBUG("Marked registry entry for app %" PRIappid " as dead\n", key);

  op->status = MSTRO_OK;
  if ((op->status != MSTRO_WOULDBLOCK) && (op->status != MSTRO_OK))
  {
    /**Failed operation ... should send ack and close*/
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*engine will execute next step, i.e. send ack */
  }
  return MSTRO_OK;
}


mstro_status
mstro_pm_cdo_registry_cdo_name_lookup(const struct mstro_cdo_id *id,
                                      const char **cdo_name)
{
  mstro_status s;
  struct mstro_pm_cdo_registry_entry *regentry=NULL;

  struct mstro_cdo_id head = *id;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  WITH_LOCKED_CDO_REGISTRY(
      HASH_FIND(hh, g_mstro_pm_cdo_registry,
                &head, sizeof(struct mstro_cdo_id), regentry);
      if(regentry) {
        *cdo_name = regentry->cdo_name;
        s=MSTRO_OK;
      } else {
        s=MSTRO_NOMATCH;
      });
  return s;
}

static inline
mstro_status
mstro_pm_demand_queue_entry__create(const struct mstro_cdo_id *cdo_id,
                                    mstro_app_id app,
                                    struct mstro_pm_demand_queue_entry **res)
{
  mmbLayout *dist_layout = NULL;
  if(res==NULL)
    return MSTRO_INVOUT;
  *res=malloc(sizeof(struct mstro_pm_demand_queue_entry));
  if(NULL==*res) {
    return MSTRO_NOMEM;
  } else {
    mstro_status status=MSTRO_OK;
    struct mstro_pm_app_registry_entry *app_entry;
    status = mstro_pm_app_lookup(app, &app_entry);

    (*res)->cdo_id.qw[0] = cdo_id->qw[0];
    (*res)->cdo_id.qw[1] = cdo_id->qw[1];
    (*res)->cdo_id.local_id = cdo_id->local_id;
    (*res)->provider = MSTRO_APP_ID_INVALID;
    (*res)->req_methods = app_entry->transport_methods;
    (*res)->requestor = app;
    /* (*res)->req_attributes set below inside locked region */


    struct mstro_cdo_id head = *cdo_id;
    head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

    WITH_LOCKED_CDO_REGISTRY({
        struct per_app_cdo_entries *app_entry;
        struct cdo_handle_entry *handle_entry;
        mstro_status status = mstro_pm_cdo_app_lookup(cdo_id, app, &app_entry, &handle_entry);
        if(status!=MSTRO_OK) {
          ERR("Failed to find CDO entry for app %zu\n", app);
        } else {
          if(handle_entry->cdo_state & MSTRO_CDO_STATE_INJECTED) {
            WARN("operation on injected cdo, fasten seat belts (untested)\n");
          } else {
            DEBUG("demand queue entry for cdo in state %d (%s)\n",
                  handle_entry->cdo_state,
                  mstro_cdo_state_describe(handle_entry->cdo_state));
            assert(   handle_entry->cdo_state & MSTRO_CDO_STATE_REQUIRED
                   || handle_entry->cdo_state & MSTRO_CDO_STATE_DEMANDED );
          }
          if(handle_entry->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT) {
            ; /* normal */
          } else {
            WARN("operation on CDO that is not marked 'in-transport'\n");
          }
          (*res)->req_attributes = handle_entry->attributes;
          mstro_attribute_pool_find_dist_layout(handle_entry->attributes, &dist_layout);
          (*res)->req_dist_layout = dist_layout;

        }
      });
    return status;
  }
}

mstro_status
mstro_pm_demand_queue_insert(const struct mstro_cdo_id *cdo_id,
                             mstro_app_id requestor)
{
  if(cdo_id==NULL)
    return MSTRO_INVARG;
  if(requestor==MSTRO_APP_ID_INVALID)
    return MSTRO_INVARG;

  struct mstro_pm_demand_queue_entry *e;
  mstro_status status = mstro_pm_demand_queue_entry__create(cdo_id, requestor, &e);
  if(status!=MSTRO_OK) {
    ERR("Cannot allocate demand queue entry\n");
    return status;
  }

  if(status==MSTRO_OK) {
    /* add to queue */
    int s=pthread_mutex_lock(&g_mstro_pm_demand_queue_mtx);
    if(s!=0) {
      ERR("Failed to lock demand queue mutex: %d (%s)\n",
          s, strerror(s));
      status=MSTRO_FAIL;
    } else {
      LL_PREPEND(g_mstro_pm_demand_queue, e);
      s=pthread_cond_signal(&g_mstro_pm_demand_queue_cvar); /*signal is enough for a single transport thread*/
      if(s!=0) {
        ERR("Failed to signal change in demand queue: %d (%s)\n",
            s, strerror(s));
        status=MSTRO_FAIL;
      }
      s=pthread_mutex_unlock(&g_mstro_pm_demand_queue_mtx);
      if(s!=0) {
        ERR("Failed to unlock demand queue mutex: %d (%s)\n",
            s, strerror(s));
        status=MSTRO_FAIL;
      }
    }
  }
BAILOUT:
  return status;
}


mstro_status
mstro_pm_cdo_registry_transfer_completed(mstro_pool_operation op)
{

  /* There are 3 essentially different cases:
     IN_TRANSPORT set on
     - DEMANDED
     - REQUIRED
     - other 'base state'

     For DEMANDED we must consider the completion of transfer
     notification as the removal from the pool.

     For REQUIRED we set SATISFIED -- telling us that we've
     proactively done a transfer for a required CDO that has not been
     DEMANDED, and that is also considered OFFERED until we see the
     DEMAND.

     Other base states indicate that we have been very proactive, and
     should have INJECTED set. Again, we'll set SATISFIED (to indicate
     availability) and keep the INJECTED for bookkeeping.
  */

  mstro_status status = MSTRO_OK;
  const struct mstro_cdo_id *cdo_id = &(op->cdoid);
  mstro_app_id app_id = op->appid;

  DEBUG("FIXME: this code should use state checker function\n");

  WITH_LOCKED_CDO_REGISTRY({
      struct per_app_cdo_entries *app_entry;
      struct cdo_handle_entry *handle_entry;
      mstro_status status = mstro_pm_cdo_app_lookup(cdo_id, app_id, &app_entry, &handle_entry);
      if(status!=MSTRO_OK) {
        ERR("Failed to find CDO entry for app %" PRIappid "\n", app_id);
        status = MSTRO_INVARG;
      } else {
        /* decrement the number of outstanding transmissions*/
        handle_entry->n_segments--;
        WITH_CDO_ID_STR(idstr, cdo_id,
                            DEBUG("There are %zu outstanding pieces for (probably distributed) CDO  %s\n",
                                  handle_entry->n_segments, idstr););
        if (handle_entry->n_segments == 0) {
          if(! (handle_entry->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT)) {
            if(handle_entry->cdo_state==MSTRO_CDO_STATE_RETRACTED) {
              WITH_CDO_ID_STR(idstr, cdo_id,
                            ERR("RETRACTED and not IN_TRANSPORT state for CDO %s (app %" PRIappid
                                ": %d (%s), assuming retract overlapped with eager transfer.\n",
                                idstr, app_id,
                                handle_entry->cdo_state,
                                mstro_cdo_state_describe(handle_entry->cdo_state)););
            } else {
              WITH_CDO_ID_STR(idstr, cdo_id,
                            ERR("Unexpected PM cdo registry state for CDO %s (app %" PRIappid ": %d (%s)\n",
                                idstr, app_id,
                                handle_entry->cdo_state,
                                mstro_cdo_state_describe(handle_entry->cdo_state)););
              status = MSTRO_INVARG;
            }
          } else {
          /* ok, it was in-transport */
            if(handle_entry->cdo_state & MSTRO_CDO_STATE_DEMANDED) {
              WITH_CDO_ID_STR(idstr, cdo_id,
                            DEBUG("Clearing IN_TRANSPORT state from DEMANDED CDO %s\n",
                                  idstr););
              handle_entry->cdo_state &= ~MSTRO_CDO_STATE_IN_TRANSPORT;
            } else if(handle_entry->cdo_state & MSTRO_CDO_STATE_REQUIRED) {
              WITH_CDO_ID_STR(idstr, cdo_id,
                            DEBUG("Clearing IN_TRANSPORT state from REQUIRED CDO %s\n",
                                  idstr););
              handle_entry->cdo_state &= ~MSTRO_CDO_STATE_IN_TRANSPORT;
              handle_entry->cdo_state |= MSTRO_CDO_STATE_SATISFIED;
            } else if(handle_entry->cdo_state & MSTRO_CDO_STATE_INJECTED) {
              WITH_CDO_ID_STR(idstr, cdo_id,
                            DEBUG("Clearing IN_TRANSPORT state from INJECTED CDO %s\n",
                                  idstr););
              handle_entry->cdo_state &= ~MSTRO_CDO_STATE_IN_TRANSPORT;
              handle_entry->cdo_state |= MSTRO_CDO_STATE_SATISFIED;
            } else {
              WITH_CDO_ID_STR(idstr, cdo_id,
                            ERR("Unhandled CDO state %s for CDO %s\n",
                                mstro_cdo_state_describe(handle_entry->cdo_state), idstr););
              status = MSTRO_UNIMPL;
            }
            /* WITH_CDO_ID_STR(idstr, cdo_id, { */
            /*     DEBUG("New state for CDO %s (app %" PRIappid ": %d (%s)\n", */
            /*           idstr, app_id, */
            /*           handle_entry->cdo_state, */
            /*           mstro_cdo_state_describe(handle_entry->cdo_state));}); */
          }
        }
      }
    });
  op->status = status;
  if ((op->status != MSTRO_WOULDBLOCK) && (op->status != MSTRO_OK))
  {
    /**Failed operation ... should send ack and close*/
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*engine will execute next step, i.e. send ack */
    status = MSTRO_OK;
  }
  return status;
}


mstro_status
mstro_pm_cdo_app_match(mstro_app_id origin, const struct mstro_cdo_id *id,
                       const struct mstro_cdo_selector_ *cdo_selector)
{
  if(origin==g_pool_app_id) {
    ERR("FIXME: unhandled case PM-is-source-of-PM-event\n");
    return MSTRO_UNIMPL;
  }
  if(id==NULL) {
    return MSTRO_INVARG;
  }
  if(cdo_selector==NULL) {
    DEBUG("NULL selector always matches -- hope that's what you meant\n");
    return MSTRO_OK;
  }

   WITH_CDO_ID_STR(str,id, { 
       DEBUG("Trying to match %s origin app %" PRIappid " for selector %p, query |%s|\n", 
             str, origin, cdo_selector, cdo_selector->query); 
     }); 
  mstro_status status = MSTRO_FAIL;
  /* now fetch attributes from per-app-CDO entry, call subscription-module checker and return result */
  WITH_LOCKED_CDO_REGISTRY({
      struct per_app_cdo_entries *app_entry;
      struct cdo_handle_entry *handle_entry;
      status = mstro_pm_cdo_app_lookup(id, origin, &app_entry, &handle_entry);
      if(status!=MSTRO_OK) {
        ERR("Failed to find CDO entry for app %zu\n", origin);
      } else {
        status = mstro_subscription_selector_eval(id, cdo_selector,
                                                  handle_entry->attributes);
      }
    });
  return status;
}

/** evaluate if we can immediately withdraw a distributed cdo .... MUST be called within locked registery block*/
static inline
mstro_status
mstro_pm_dist_cdo_registry_immediate_withdraw(
                               const struct mstro_cdo_id *cdoid,
                               mstro_app_id appid,
                               struct mstro_pm_cdo_registry_entry *regentry,
                               bool *immediate_withdraw) {

    mstro_status status = MSTRO_OK;
    *immediate_withdraw=false;

    struct mstro_cdo_id head = *cdoid;
    head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

    INFO("Withdrawing a piece of a distributed CDO\n");

   /* count OFFERs */
   struct per_app_cdo_entries *entry;
   struct per_app_cdo_entries *tmp;
   size_t num_offers=0;
   size_t num_required=0;
   size_t num_retracted=0;
   size_t num_in_flight=0; /* demanded and in-transport */
   size_t num_req_in_transport=0; /* required and in-transport (eager sends) */
   HASH_ITER(hh, regentry->app_to_attributes, entry, tmp) {
        struct cdo_handle_entry *h;
        struct cdo_handle_entry *t;
        HASH_ITER(hh, entry->handles, h, t) {
        /* WITH_CDO_ID_STR(idstr, &head, { */
        /*     DEBUG("Inspecting %s for app %" PRIappid " local-id %zu: %d (%s)\n", */
        /*           idstr, entry->app, h->local_id, */
        /*           h->cdo_state, */
        /*           mstro_cdo_state_describe(h->cdo_state));}); */
        if(h->cdo_state & MSTRO_CDO_STATE_OFFERED) {
          num_offers++;
        }
        if(h->cdo_state & MSTRO_CDO_STATE_RETRACTED) {
          num_retracted++;
        }
        if(h->cdo_state & MSTRO_CDO_STATE_REQUIRED) {
          num_required++;
          if(h->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT) {
                num_req_in_transport++;
              }
        }
        if((h->cdo_state & MSTRO_CDO_STATE_DEMANDED)
               && (h->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT)) {
                 num_in_flight++;
        }
      }
    }
    assert(num_offers>0); /* because we must have one outstanding OFFER for it from appid */
        /* find our entry */
    HASH_FIND(hh, regentry->app_to_attributes,
                  &appid, sizeof(appid), entry);
    if(entry==NULL) {
          ERR("Failed to find our per-app CDO registry entry (num_offers %zu)\n",
              num_offers);
          status = MSTRO_NOENT;
          return  status;
    }

    /* check whether there is any REQUIRE or DEMAND for it */
    if(num_required==0 && num_in_flight==0) {
      WITH_CDO_ID_STR(idstr, cdoid,
                      DEBUG("No outstanding REQUIRE or in-flight DEMAND for CDO %s, permitting WITHDRAW\n", idstr););
      mstro_pm_cdo_registry__set_state(cdoid, appid,
                                       MSTRO_CDO_STATE_WITHDRAWN,
                                       entry);
      *immediate_withdraw=true;
    } else {
      WARN("FIXME we are blocking all instances of a distributed CDO if there is a required piece\n, we should only block the required piece \n");
      WITH_CDO_ID_STR(idstr, cdoid,
                      DEBUG("Still have %zu REQUIREs and %zu in-flight DEMANDs for CDO %s outstanding\n",
                            num_required, num_in_flight, idstr););
      if(num_req_in_transport< num_required) {
        INFO("Some REQUIREs are not in-transport yet; could check whether this instance is worth copying\n");
      }
      /* can't withdraw right away, need to schedule a wakeup on this CDO */
      *immediate_withdraw=false;
    }

    return status;
}


/** evaluate if we can immediately withdraw .... MUST be called within locked registery block*/
static inline
mstro_status
mstro_pm_cdo_registry_immediate_withdraw(
                               const struct mstro_cdo_id *cdoid,
                               mstro_app_id appid,
                               struct mstro_pm_cdo_registry_entry *regentry,
                               bool *immediate_withdraw) {

    mstro_status status = MSTRO_OK;
    *immediate_withdraw=false;

    struct mstro_cdo_id head = *cdoid;
    head.local_id = MSTRO_CDO_LOCAL_ID_NONE;


   /* count OFFERs */
   struct per_app_cdo_entries *entry;
   struct per_app_cdo_entries *tmp;
   size_t num_offers=0;
   size_t num_required=0;
   size_t num_retracted=0;
   size_t num_in_flight=0; /* demanded and in-transport */
   size_t num_req_in_transport=0; /* required and in-transport (eager sends) */
   HASH_ITER(hh, regentry->app_to_attributes, entry, tmp) {
        struct cdo_handle_entry *h;
        struct cdo_handle_entry *t;
        HASH_ITER(hh, entry->handles, h, t) {
        /* WITH_CDO_ID_STR(idstr, &head, { */
        /*     DEBUG("Inspecting %s for app %" PRIappid " local-id %zu: %d (%s)\n", */
        /*           idstr, entry->app, h->local_id, */
        /*           h->cdo_state, */
        /*           mstro_cdo_state_describe(h->cdo_state));}); */
        if(h->cdo_state & MSTRO_CDO_STATE_OFFERED) {
          num_offers++;
        }
        if(h->cdo_state & MSTRO_CDO_STATE_RETRACTED) {
          num_retracted++;
        }
        if(h->cdo_state & MSTRO_CDO_STATE_REQUIRED) {
          num_required++;
          if(h->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT) {
                num_req_in_transport++;
              }
        }
        if((h->cdo_state & MSTRO_CDO_STATE_DEMANDED)
               && (h->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT)) {
                 num_in_flight++;
        }
      }
    }
    assert(num_offers>0); /* because we must have one outstanding OFFER for it from appid */
        /* find our entry */
    HASH_FIND(hh, regentry->app_to_attributes,
                  &appid, sizeof(appid), entry);
    if(entry==NULL) {
          ERR("Failed to find our per-app CDO registry entry (num_offers %zu)\n",
              num_offers);
          status = MSTRO_NOENT;
          return  status;
    }

    if(num_offers>1) {
        /* easy, someone else has it too */
        WITH_CDO_ID_STR(idstr, cdoid,
                          DEBUG("Multiple OFFERs of CDO %s in pool\n", idstr););
          INFO("FIXME: performing quick WITHDRAW for CDO from app %d;"
               " could check whether this instance was worth copying\n",
               appid);
          assert(entry!=NULL);
          mstro_pm_cdo_registry__set_state(cdoid, appid,
                                           MSTRO_CDO_STATE_WITHDRAWN,
                                           entry);
          *immediate_withdraw=true;
      } else {
          /* only one offer */
          WITH_CDO_ID_STR(idstr, cdoid, DEBUG("Only one OFFER of CDO %s in pool\n", idstr););
          /* check whether there is any REQUIRE or DEMAND for it */
          if(num_required==0 && num_in_flight==0) {
            WITH_CDO_ID_STR(idstr, cdoid,
                            DEBUG("No outstanding REQUIRE or in-flight DEMAND for CDO %s, permitting WITHDRAW\n", idstr););
            mstro_pm_cdo_registry__set_state(cdoid, appid,
                                             MSTRO_CDO_STATE_WITHDRAWN,
                                             entry);
            *immediate_withdraw=true;
          } else {
            WITH_CDO_ID_STR(idstr, cdoid,
                            DEBUG("Still have %zu REQUIREs and %zu in-flight DEMANDs for CDO %s outstanding\n",
                                  num_required, num_in_flight, idstr););
            if(num_req_in_transport< num_required) {
              INFO("Some REQUIREs are not in-transport yet; could check whether this instance is worth copying\n");
            }
            /* can't withdraw right away, need to schedule a wakeup on this CDO */
            *immediate_withdraw=false;
          }
        }


      return status;
}


/** withdraw CDOID for APPID
 *
 * If feasible, will do it immediately, otherwise will either block, or schedule a proactive copy to release APPID.
 * Completion will be signaled by raising CONTINUATION-EVENT.
 * Returns only in error conditions.
 */
/* Algorithm:
 *
 * If the CDO is not a GROUP-CDO:
 *
 *  * if >1 current OFFERs, immediately permit
 *
 *  ** we could be smarter here and keep a copy if the layout or
 *     distribution is attractive for outstanding REQUIREs, will leave
 *     a note for that
 *
 * * if this is the last OFFER, but there are REQUIRE or DEMAND entries, block
 *
 *  ** we should be smarter here and make a copy if it’s a REQUIRE; a
 *     DEMAND will likely be IN-TRANSPORT at this time, so blocking
 *     will be short
 *
 * For GROUP-CDOs it’s a bit more complicated.
 *
 * A GROUP-OFFER is implicitly offering all members, so GROUP-WITHDRAW
 * will need to withdraw all members. I would first wirthdraw the
 * group entry, then cycle through members, but all under one big fat
 * lock. Availability checks need to be done per-member, and for the
 * group entry. If any member is precious, the whole group-withdraw
 * blocks.
 *
 * How about permanent CDOs? Should we have a new flag on the CDO to
 * indicate that a permanent copy has been made? Then we could
 * schedule that at OFFER time, and possibly block at WITHDRAW if it’s
 * not set yet. Who handles the permanence database by the way? We
 * don’t have machinery to inject permanent CDOs at startup of the PM
 * currently. Should we? Maybe just a directory with 2 files per CDO,
 * the serialized attributes and a blob?
 *
 */
mstro_status
mstro_pm_cdo_registry_withdraw(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  WITH_CDO_ID_STR(idstr, &(op->cdoid),
                  DEBUG("Request to withdraw %s for app %"PRIappid"\n",
                        idstr, op->appid););
  /* flag to remember whether we performed the WITHDRAW immediately */
  bool immediate_withdraw = false;

  struct mstro_cdo_id head = op->cdoid;
  head.local_id = MSTRO_CDO_LOCAL_ID_NONE;

  WITH_LOCKED_CDO_REGISTRY({
      struct mstro_pm_cdo_registry_entry *regentry=NULL;
      HASH_FIND(hh, g_mstro_pm_cdo_registry,
                &head, sizeof(struct mstro_cdo_id), regentry);
      if(!regentry) {
        /* should not happen */
        WITH_CDO_ID_STR(idstr, &(op->cdoid),
                        ERR("No CDO registry entry for WITHDRAWN CDO %s\n",
                            idstr););
        status = MSTRO_NOENT;
        goto unlock_fail;
      }
      if(regentry->is_distributed) {
        status = mstro_pm_dist_cdo_registry_immediate_withdraw(&(op->cdoid), op->appid,regentry, &immediate_withdraw);
      }
      else {
        status = mstro_pm_cdo_registry_immediate_withdraw(&(op->cdoid), op->appid,regentry, &immediate_withdraw);
      }

      unlock_fail:
         ;
       });

  if(status == MSTRO_OK) {
    if(immediate_withdraw) {
      /* CDO state changed, so can trigger next step */
      op->status = MSTRO_OK;
    } else { /**Can not withdraw immediately */
      status = MSTRO_WOULDBLOCK;
    }
  } else /**failed*/
  {
    op->status = MSTRO_FAIL;
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*jump to sending ack. engine will execute next step, i.e. send ack */
    status = MSTRO_OK;
    ERR("Failed to perform a WITHDRAW: %d (%s)\n", status, mstro_status_description(status));
  }

  return status;
}


/**
 * @brief remove cdo from pm registry.
 * If the operation failed, mstro_pool_op_engine will drop the operation, i.e. without sending notification of event:after
 * This is fine here because dispose does not require PoolOp Ack
 * 
 * @param op operation handle 
 * @return mstro_status 
 */
mstro_status
mstro_pm_cdo_registry_dispose(mstro_pool_operation op)
{

  mstro_status s;
  const struct mstro_cdo_id *cdo_id = &(op->cdoid);
  mstro_app_id appid = op->appid;

  WITH_LOCKED_CDO_REGISTRY({
      struct per_app_cdo_entries *app_entry=NULL;
      struct cdo_handle_entry *handle_entry=NULL;
      s = mstro_pm_cdo_app_lookup(cdo_id, appid, &app_entry, &handle_entry);
      if(s!=MSTRO_OK) {
        /* should not happen */
        WITH_CDO_ID_STR(idstr, cdo_id,
                        ERR("No CDO registry entry for %s when attempting to DISPOSE it\n",
                            idstr););
        s=MSTRO_NOENT;
        goto unlock;
      }
      /*test if it is in transport */
      if((handle_entry->cdo_state & MSTRO_CDO_STATE_IN_TRANSPORT)) {
      WITH_CDO_ID_STR(idstr, cdo_id,
		      WARN("cdo %s state %d (%s) for app %" PRIappid " cannot be DISPOSEd\n",
			      idstr, handle_entry->cdo_state, mstro_cdo_state_describe(handle_entry->cdo_state), appid););
      s=MSTRO_WOULDBLOCK;
      goto unlock;
      }
      if(!(handle_entry->cdo_state & MSTRO_CDO_STATE_DISPOSABLE)) {
        WITH_CDO_ID_STR(idstr, cdo_id,
                        ERR("cdo %s state %d (%s) for app %" PRIappid " cannot be DISPOSEd\n",
                            idstr, handle_entry->cdo_state,
                            mstro_cdo_state_describe(handle_entry->cdo_state), appid););
        s=MSTRO_FAIL;
        goto unlock;
      }
      /* now delete from (app_to_attributes) table */
      HASH_DEL(app_entry->handles, handle_entry);
      /* and kill entry */
      s = mstro_pm__cdo_handle_entry__destroy(handle_entry);
      /* we don't remove the app entry from the overall CDO registry
       * at this point; this will be done by mstro_pm_app_deregister
       * which will notice we don't have any handles for any CDOs
       * anymore */
   unlock:
      ;
    });
  op->status = s;/**update operation status*/
  if ((op->status != MSTRO_WOULDBLOCK) && (op->status != MSTRO_OK))
  {
    /**Failed operation ... should send ack and close*/
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*engine will execute next step, i.e. send ack */
    s = MSTRO_OK;
  }
  return s;
}


mstro_status
mstro_pm_reg_init(void)
{
  return MSTRO_OK;
}

mstro_status
mstro_pm_reg_finalize(void)
{
  mstro_status s=MSTRO_OK;

  DEBUG("Cleaning PM registry\n");
  WITH_LOCKED_APP_REGISTRY({
      struct mstro_pm_app_registry_entry *elt=NULL;
      struct mstro_pm_app_registry_entry *tmp=NULL;
      HASH_ITER(hh, g_mstro_pm_app_registry, elt, tmp) {
        if(!elt->dead) {
          ERR("Dropping live entry for app %s:%" PRIu64 " appid %" PRIappid "\n",
              elt->component_name, elt->component_index,
              elt->appid);
          s |= MSTRO_FAIL;
        }
        HASH_DEL(g_mstro_pm_app_registry, elt);
        s |= mstro_pm_app_reg__entry_dispose(elt);
      }
      g_mstro_pm_app_registry = NULL;
    });

  return s;
}
