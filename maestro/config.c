/*
 * SPDX-License-Identifier: ISC
 *
 * Copyright (C) 2018 Michael Drake <tlsa@netsurf-browser.org>
 */

#include <stdlib.h>
#include <stdio.h>

#include <cyaml/cyaml.h>

#include "maestro.h"


/******************************************************************************
 * CYAML schema to tell libcyaml about both expected YAML and data structure.
 *
 * (Our CYAML schema is just a bunch of static const data.)
 ******************************************************************************/

static const cyaml_schema_field_t cdo_mapping_schema[] = {
	CYAML_FIELD_STRING_PTR("name", CYAML_FLAG_POINTER,
			struct mstro_config_cdo_, name,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_INT("user-provided-storage", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_cdo_, user_provided_storage),
	CYAML_FIELD_INT("layer", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_cdo_, layer),	
	CYAML_FIELD_INT("size", CYAML_FLAG_DEFAULT,
			struct mstro_config_cdo_, size),
	CYAML_FIELD_END
};

static const cyaml_schema_value_t cdo_entry= {
	CYAML_VALUE_MAPPING(CYAML_FLAG_DEFAULT,
			struct mstro_config_wc_producer_, cdo_mapping_schema),
};

static const cyaml_schema_field_t charybdis_mapping_schema[] = {
	CYAML_FIELD_INT("nranks", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_charybdis_, nranks),
	CYAML_FIELD_STRING_PTR("bin", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_charybdis_, bin,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t spare_mapping_schema[] = {
	CYAML_FIELD_INT("nranks", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_spare_, nranks),
	CYAML_FIELD_STRING_PTR("bin", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_spare_, bin,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t pool_manager_mapping_schema[] = {
	CYAML_FIELD_INT("nranks", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_pool_manager_, nranks),
	CYAML_FIELD_STRING_PTR("bin", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_pool_manager_, bin,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t consumer_mapping_schema[] = {
	CYAML_FIELD_SEQUENCE("cdo_list", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_consumer_, cdo, &cdo_entry,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_INT("nranks", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_consumer_, nranks),
	CYAML_FIELD_STRING_PTR("bin", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_consumer_, bin,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t producer_mapping_schema[] = {
	CYAML_FIELD_SEQUENCE("cdo_list", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_producer_, cdo, &cdo_entry,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_INT("nranks", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_producer_, nranks),
	CYAML_FIELD_STRING_PTR("bin", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_wc_producer_, bin,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t workflow_mapping_schema[] = {
	CYAML_FIELD_MAPPING("producer", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_workflow_, producer, producer_mapping_schema),
	CYAML_FIELD_MAPPING("consumer", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_workflow_, consumer, consumer_mapping_schema),
	CYAML_FIELD_MAPPING("pool-manager", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_workflow_, pool_manager, pool_manager_mapping_schema),
	CYAML_FIELD_MAPPING("spare", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_workflow_, spare, spare_mapping_schema),
	CYAML_FIELD_MAPPING("charybdis", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			struct mstro_config_workflow_, charybdis, charybdis_mapping_schema),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t top_mapping_schema[] = {
	CYAML_FIELD_STRING_PTR("version", CYAML_FLAG_POINTER,
			struct mstro_config_, version,
			0, CYAML_UNLIMITED),
	CYAML_FIELD_INT("token", CYAML_FLAG_DEFAULT,
			struct mstro_config_, token),
       	CYAML_FIELD_MAPPING("workflow", CYAML_FLAG_POINTER,
			struct mstro_config_, workflow, workflow_mapping_schema),
	CYAML_FIELD_END
};

/* CYAML value schema for the top level mapping. */
static const cyaml_schema_value_t top_schema = {
	CYAML_VALUE_MAPPING(CYAML_FLAG_POINTER,
			struct mstro_config_, top_mapping_schema),
};

/******************************************************************************
 * Actual code to load and save YAML doc using libcyaml.
 ******************************************************************************/

/* Our CYAML config.
 *
 * If you want to change it between calls, don't make it const.
 *
 * Here we have a very basic config.
 */
static const cyaml_config_t config = {
	.log_level = CYAML_LOG_WARNING, /* Logging errors and warnings only. */
	.log_fn = cyaml_log,            /* Use the default logging function. */
	.mem_fn = cyaml_mem,            /* Use the default memory allocator. */
};

mstro_status
mstro_config_parse(mstro_config* handle, const char* filename)
{
  if (handle == NULL) return MSTRO_INVARG; 

  cyaml_err_t err;
  err = cyaml_load_file(filename, &config,
  		&top_schema, (cyaml_data_t **)handle, NULL);
  if (err != CYAML_OK) {
    fprintf(stderr, "ERROR: %s\n", cyaml_strerror(err));
    return MSTRO_FAIL;
  }

  return MSTRO_OK;
}


