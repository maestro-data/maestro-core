/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool Manager component -- SMP based implementation
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/status.h"
#include "maestro/pool_manager.h"
#include "maestro/i_state.h"
#include "maestro/logging.h"
#include "maestro/logging.h"

#include <inttypes.h>
#include <unistd.h>

static
mstro_status
mstro_smp_pm_loop(void)
{
  return MSTRO_OK;
}



/* User-side entry point: dedicate current thread to pool manager work */
mstro_status
mstro_pm_start(int64_t pool_manager_size)
{
  mstro_status stat = MSTRO_UNIMPL;
  
  if(!(pool_manager_size>=1)) {
    return MSTRO_INVARG;
  }

  if(pool_manager_size!=1) {
    WARN("DEFICIENCY: Cannot run distributed pool manager yet\n");
    return MSTRO_UNIMPL;
  }

  const struct mstro_core_initdata *d;
  stat = mstro_core_state_get(&d);

  if(stat!=MSTRO_OK) {
    ERR("Failed to get initdata: %d\n", stat);
    goto BAILOUT;
  }
  if(d==NULL) {
    ERR("Trying to start pool manager before mstro_init()\n");
    stat=MSTRO_FAIL;
    goto BAILOUT;
  }
  
  INFO("Starting Pool Manager for %s,"
       "expecting %" PRIi64 " partners\n",
       d->workflow_name, pool_manager_size);

  /* organize quorum for pool_manager_size */
  
  /* open queue */

  /* run pool manager loop. It will return after receiving a TERM message */
  stat = mstro_smp_pm_loop();

  INFO("Pool Manager loop terminated\n");
BAILOUT:
  return stat;
}

mstro_status
mstro_pm_terminate(void)
{
  mstro_status status = MSTRO_UNIMPL;
#if 0
  struct mstro_pm_msg *msg
      = mstro_pm_msg_create(MSTRO_CMSG_TERMINATE_POOL_MANAGER);
  if(!msg) {
    status = MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    /* no payload */
    status = mstro_u2c_submit_wait(msg); /* send and wait for completion; completion handler cleans up msg */
  } 
#endif
  
  return status;
}
