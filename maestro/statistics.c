/* -*- mode:c -*- */
/** @file
 ** @brief Statistics layer implementation of maestro
 **
 **/

/*
 * Copyright (C) 2018-2020 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/i_statistics.h"
#include "maestro/logging.h"
#include "maestro/i_uthash.h"
#include "maestro/i_utlist.h"
/* #include "i_global.h" */

#include <pthread.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <inttypes.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_STATS,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_STATS,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_STATS,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_STATS,__VA_ARGS__)

/* timing stuff */
#if defined(__linux)
#  define HAVE_POSIX_TIMER
#  include <time.h>
#  ifdef CLOCK_MONOTONIC
#     define CLOCKID CLOCK_MONOTONIC
#  else
#     define CLOCKID CLOCK_REALTIME
#  endif
  static struct timespec linux_rate;
#elif defined(__APPLE__)
#  include <time.h>
#  define HAVE_MACH_TIMER
#  ifdef CLOCK_MONOTONIC
#     define CLOCKID CLOCK_MONOTONIC
#  else
#     define CLOCKID CLOCK_REALTIME
#  endif
#  include <mach/mach_time.h>
   static mach_timebase_info_data_t info;
#elif defined(_WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
   static LARGE_INTEGER win_frequency;
#else
#error Unsupported system class
#endif

/** System-specific init to be able to implement mstro_clock() */
static void
mstro_stats__init_clock(void)
{
#if defined(__APPLE__)
  mach_timebase_info(&info);
#elif defined(__linux)
  clock_getres(CLOCKID, &linux_rate);
#elif defined(_WIN32)
  QueryPerformanceFrequency(&win_frequency);
#else
#error Unsupported system class
#endif
}


#include <stdint.h>



/* ----------------------------------------------------------------------- */

/** a lock protecting all statistics data below @ref g_collection */
static pthread_mutex_t
g_statistics_mutex = PTHREAD_MUTEX_INITIALIZER;

/** in a collection there is one hash table per label */
struct collection {
  char *label;                     /**< the collection label */
  UT_hash_handle hh;               /**< structure is hashable, by label */
  enum mstro_stats_collection_type type;  /**< measurement data type */
  /* data: */
  union { /* depending on type */
    struct counter_measurements  *counter;
    struct key_val_measurements  *key_val;
    struct key_time_measurements *key_time;
  };
};

/** the table, by category, of all stats */
static struct collection *
g_collection[MSTRO_STATS_CATEGORY__MAX] = {
  NULL, NULL, NULL, NULL, NULL, NULL, NULL
};

/** Statistics category printable names */
static
const char * const
mstro_stats_category_name[MSTRO_STATS_CATEGORY__MAX] = {
  [MSTRO_STATS_CAT_GENERAL]   = "General",
  [MSTRO_STATS_CAT_POOL]      = "Pool",
  [MSTRO_STATS_CAT_PROTOCOL]  = "Pool-Protocol",
  [MSTRO_STATS_CAT_OBJECTS]   = "Objects",
  [MSTRO_STATS_CAT_MEMORY]    = "Memory",
  [MSTRO_STATS_CAT_TRANSPORT] = "Transport",
  [MSTRO_STATS_CAT_THREADING] = "Threading"
};

/** Statistics collection types printable names */
static
const char * const
mstro_stats_collection_type_name[MSTRO_STATS_COLLECTION__MAX] = {
  [MSTRO_STATS_COLL_COUNTER] = "occurence count",
  [MSTRO_STATS_COLL_KEY_VAL] = "key-value table",
  [MSTRO_STATS_COLL_KEY_TIME] = "key-time table"
};

/* data: lists (pushed onto) of the measurements */
/** single values measurements */
struct counter_measurements {
  uint64_t count;                      /**< a count */
  struct counter_measurements *next;   /**< next in list */
};

/** key/value measurements */
struct key_val_measurements {
  uint64_t key;                        /**< a key */
  void *val;                           /**< an opaque value */
  struct key_val_measurements *next;   /**< next in list */
};

/** key/time value measurements */
struct key_time_measurements {
  uint64_t key;                        /**< a key */
  mstro_nanosec_t time;                /**< a time duration */
  struct key_time_measurements *next;  /**< next in list */
};



/** If DST is NULL use logging, otherwise fprintf to stream */
/* FIXME: make the level configurable by environment variable */
#define PRINT_OR_LOG(dst,...)                   \
  do {                                          \
    if(dst!=NULL)                               \
      fprintf(dst, __VA_ARGS__);                \
    else                                        \
      INFO(__VA_ARGS__);                        \
  } while(0)

/** comparison function for sorting by < */
static inline
int
mstro_stats__cmp_key_lt(uint64_t x, uint64_t y) {
  return x < y;
}


/** report all data in category to DST. Uses PRINT_OR_LOG() */
static mstro_status
mstro_stats_report_category(FILE *dst,
                     const char *category_name,
                     struct collection *coll)
{
  mstro_status ret = MSTRO_OK;
  if(coll) {
    struct collection *el,*tmp;
    PRINT_OR_LOG(dst, " Category %s\n", category_name);
    HASH_ITER(hh,coll,el,tmp) {
      PRINT_OR_LOG(dst, "  %14s: (%s)\n",
                   el->label, mstro_stats_collection_type_name[el->type]);
      switch(el->type) {
        case MSTRO_STATS_COLL_COUNTER: {
          struct counter_measurements *entry;
          LL_FOREACH(el->counter,entry) {
            PRINT_OR_LOG(dst, "  %14c\t%14"PRIu64"\n", ' ', entry->count);
          }

          break;
        }
        case MSTRO_STATS_COLL_KEY_VAL: {
          struct key_val_measurements *entry;
          LL_FOREACH(el->key_val,entry) {
            /* FIXME: allow user print-value function call here */
            PRINT_OR_LOG(dst, "  %14c\t%14"PRIu64"\t%p\n",
                         ' ', entry->key, entry->val);
          }
          break;
        }
        case MSTRO_STATS_COLL_KEY_TIME: {
          struct key_time_measurements *entry;
          /* sort entries by key */
          LL_FOREACH(el->key_time,entry) {
            PRINT_OR_LOG(dst, "  %14c\t%14"PRIu64"\t%14"PRIu64" ns\n",
                         ' ', entry->key, entry->time);
          }
          break;
        }
        default:
          return MSTRO_FAIL;
      }
    }
  }
  return ret;
}

/** linked list entry for reporting values in CSV format */
struct report_data_entry {
  struct report_data_entry *next;     /**< linking pointer */
  const char *category;               /**< statistics category */
  const char *label1;                 /**< first level label */
  char *label2;                       /**< second level label, may contain ':'-separated deeper labels */
  char *value;                        /**< value */
};

/**
 ** @brief  compare data entries for sorting
 **
 ** Compare alphabetically: first by category, then by label1, then by label2
 **
 ** @param[in] e1 first entry
 ** @param[in] e2 second entry
 ** @returns <0, 0, >0 depending on how e1 and e2 compare (like strcmp)
 **/
static inline
int
report_data_entry_cmp(const struct report_data_entry *e1,
                      const struct report_data_entry *e2)
{
  int cmp1 = strcmp(e1->category, e2->category);
  if(0!=cmp1)
    return cmp1;
  else {
    int cmp2 = strcmp(e1->label1, e2->label1);
    if(0!=cmp2)
      return cmp2;
    else {
      /* some keys have a ':' separator... */
      char *colon1 = strchr(e1->label1,':');
      char *colon2 = strchr(e1->label2,':');
      if(!colon1 && !colon2) {
        return strcmp(e1->label2, e2->label2);
      } else if (colon1 && colon2) {
        /* try 2-level on this */
        *colon1='\0';
        *colon2='\0';
        int res1 = strcmp(e1->label2, e2->label2);
        int res2 = strcmp(colon1+1,colon2+1);
        *colon1=':';
        *colon2=':';
        if(res1!=0)
          return res1;
        else
          return res2;
      } else if(colon1) {
        *colon1='\0';
        int res = strcmp(e1->label2, e2->label2);
        *colon1=':';
        if(0!=res)
          return res;
        else
          return +1; /* e1 longer, so bigger */
      } else {
        /* colon2 */
        *colon2='\0';
        int res = strcmp(e1->label2, e2->label2);
        *colon2=':';
        if(0!=res)
          return res;
        else
          return -1; /* e2 longer, so smaller */
      }
    }
  }
}

/** create a measurement data entry for CSV printing */
static inline
struct report_data_entry *
create_report_data_entry(const char * category, const char *label1,
                         char *label2,
                         char *value)
{
  struct report_data_entry *result
      = malloc(sizeof(struct report_data_entry));
  if(NULL!=result) {
    result->category = category; /* shared */
    result->label1 = label1;     /* shared */
    result->label2 = strdup(label2);     /* owned */
    if(NULL==result->label2) {
      free(result);
      result=NULL;
      goto BAILOUT;
    }
    result->value  = strdup(value);      /* owned */
    if(NULL==result->value) {
      free(result->label2);
      free(result);
      result=NULL;
      goto BAILOUT;
    }
  }
BAILOUT:
  return result;
}



/** Field separator for CSV files */
#define MSTRO_STATS_CSV_SEP ";"

/** buffer size increment for allocation of temporary output strings */
#define PRINT_CHUNK_SIZE (4*1024)

/** MAX of x and y */
#define MAX(x,y) ((x)<(y)? (y) : (x))

/** print to *BUF at *BUF_DST, possibly reallocating it. */
mstro_status
sprintf_extending_as_needed(char **buf,
                            char **buf_dst,
                            size_t *buflen,
                            const char *fmtstring, ...)
{
  assert(buf!=NULL);
  assert(buf_dst!=NULL);
  assert(buflen!=NULL);
  if(*buflen>0) {
    assert(*buf!=NULL);
    assert(*buf_dst!=NULL);
    assert(*buf_dst-*buf >= 0);
  }
  size_t offset = *buflen == 0 ? 0 : (*buf_dst - *buf);
  va_list args;
TRY_PRINT: {
    va_start(args,fmtstring);
    size_t space_left = *buflen - offset;
    size_t needed = vsnprintf(*buf_dst, space_left,
                              fmtstring, args);
    if(needed>=space_left) {
      /* need to reallocate and try again */
      size_t new_size = *buflen+MAX(needed,PRINT_CHUNK_SIZE);
      char *tmp = realloc(*buf,new_size);
      if(tmp==NULL) {
        return MSTRO_NOMEM;
      }
      *buf_dst = tmp + offset;
      *buf = tmp;
      *buflen = new_size;
      goto TRY_PRINT;
    } else {
      *buf_dst += needed;
      assert(**buf_dst=='\0');
    }

    va_end(args);
  }
 goto BAILOUT;
BAILOUT:
  return MSTRO_OK;
}

mstro_status
mstro_stats_report_csv(FILE *dst)
{
  /* Format: one header row, one data row.
     Comma separation
     Headers have form category:label
     Header is sorted alphabetically by category, then label
  */
  mstro_status stat= MSTRO_OK;
  char *line=NULL;
  char *line_dst=NULL; /* current pos in line */
  size_t line_len=0;

  struct report_data_entry *data = NULL;
  size_t cat;

  for(cat=0; cat<MSTRO_STATS_CATEGORY__MAX; cat++) {
    /* DEBUG("Category %s: %d labels\n", */
    /*       mstro_stats_category_name[cat], */
    /*       HASH_COUNT(g_collection[cat])); */
    if(g_collection[cat]==NULL)
      continue;
    struct collection *el,*tmp;
    HASH_ITER(hh,g_collection[cat],el,tmp) {
      switch(el->type) {
        case MSTRO_STATS_COLL_COUNTER: {
          struct counter_measurements *entry;
          size_t i=0;
          LL_FOREACH(el->counter,entry) {
            i++;
            char id[11];
            int len = snprintf(id,11,"%zu", i);
            assert(len<11);
            char val[15];
            len = snprintf(val,15,"%"PRIu64"", entry->count);
            assert(len<15);
            struct report_data_entry *node
                = create_report_data_entry(
                    mstro_stats_category_name[cat], el->label, id, val);
            if(NULL==node) {
              stat = MSTRO_NOMEM;
              goto BAILOUT;
            } else {
              LL_PREPEND(data,node);
            }
          }
          /* DEBUG("Category %s, label %s: %d records\n", */
          /*       mstro_stats_category_name[cat], el->label, i); */
          break;
        }
        case MSTRO_STATS_COLL_KEY_TIME: {
          struct key_time_measurements *entry;
          size_t i=0;
          LL_FOREACH(el->key_time,entry) {
            i++;
            char id[16+1+10];
            /** since keys can be duplicated we add a counter to it. Due to
                padding of key sorting still works fine with this */
            int len = snprintf(id,16+1+10,"%"PRIu64":%zu", entry->key,i);
            assert(len<16+1+10);
            char val[15];
            len = snprintf(val,15,"%"PRIu64"", entry->time);
            assert(len<15);
            struct report_data_entry *node
                = create_report_data_entry(
                    mstro_stats_category_name[cat], el->label, id, val);
            if(NULL==node) {
              stat = MSTRO_NOMEM;
              goto BAILOUT;
            } else {
              LL_PREPEND(data,node);
            }
            /* DEBUG("Category %s, label %s: %d records\n", */
            /*       mstro_stats_category_name[cat], el->label, i); */
          }
          break;
        }
        default: {
          ERR("Unsupported collection type %d\n", el->type);
          stat = MSTRO_FAIL;
          goto BAILOUT;
        }
      }
    }
  }
  LL_SORT(data,report_data_entry_cmp);
  /* { */
  /*   size_t count=0; */
  /*   struct report_data_entry *tmp; */
  /*   LL_COUNT(data, tmp, count); */
  /*   DEBUG("Report data has %d nodes\n", count); */
  /* } */

  struct report_data_entry *entry;
  line=malloc(PRINT_CHUNK_SIZE);
  if(line==NULL) {
    stat=MSTRO_NOMEM;
    goto BAILOUT;
  }
  line_len=PRINT_CHUNK_SIZE;
  line_dst=line;

  /* start with run info */
  stat = sprintf_extending_as_needed(&line, &line_dst, &line_len,
                                     /* careful, we are using preproc string concat on the format string here: */
                                     "\"%s\"" MSTRO_STATS_CSV_SEP
                                     "\"%s\"" MSTRO_STATS_CSV_SEP
                                     "\"%s\"" MSTRO_STATS_CSV_SEP
                                     "\"%s\"" MSTRO_STATS_CSV_SEP "\"%s\"",
                                     "workflow-id", "app-id", "hostname", "pid", "thread");
  if(stat!=MSTRO_OK) {
    ERR("Failure printing report header\n");
    goto BAILOUT;
  }

  LL_FOREACH(data,entry) {
    stat = sprintf_extending_as_needed(&line, &line_dst, &line_len,
                                       MSTRO_STATS_CSV_SEP
                                       "\"%s:%s:%s\"",
                                       entry->category,
                                       entry->label1,
                                       entry->label2);
    if(stat!=MSTRO_OK) {
      ERR("Failure printing report header\n");
      goto BAILOUT;
    }
  }

  /** output header */
  PRINT_OR_LOG(dst, "%s\n", line);

  /** reuse buffer for data line */
  line[0]='\0';
  line_dst=line;
  stat = sprintf_extending_as_needed(&line, &line_dst, &line_len,
                                     "\"%s\"" MSTRO_STATS_CSV_SEP
                                     "%zu"    MSTRO_STATS_CSV_SEP
                                     "\"%s\"" MSTRO_STATS_CSV_SEP
                                     /* unfortunately no pid_t print format specifier */
                                     "%" PRIdMAX  MSTRO_STATS_CSV_SEP
                                     "\"%s\"",
                                     mstro_workflow_id(),
                                     mstro_appid(),
                                     mstro_hostname(),
                                     (intmax_t) mstro_pid(),
                                     mstro_threadid()
                                     );
  if(stat!=MSTRO_OK) {
    ERR("Failure printing report node\n");
    goto BAILOUT;
  }

  LL_FOREACH(data,entry) {
    stat = sprintf_extending_as_needed(&line, &line_dst, &line_len,
                                       MSTRO_STATS_CSV_SEP
                                       "%s",
                                       entry->value);
    if(stat!=MSTRO_OK) {
      ERR("Failure printing report node\n");
      goto BAILOUT;
    }
  }
  stat = sprintf_extending_as_needed(&line, &line_dst, &line_len, "\n");
  if(stat!=MSTRO_OK) {
    ERR("Failure printing report node\n");
    goto BAILOUT;
  }

  /** output data */
  PRINT_OR_LOG(dst, "%s\n", line);

BAILOUT:
  if(line)
    free(line);
  {
    struct report_data_entry *el,*tmp;
    LL_FOREACH_SAFE(data,el,tmp) {
      /* collection names and label1 are shared,
         label2 and value needs free */
      free(el->label2);
      free(el->value);
      free(el);
    }
  }

  return stat;
}

mstro_status
mstro_stats_report(FILE *dst)
{
  mstro_status ret = MSTRO_OK;

  PRINT_OR_LOG(dst, "UDJ statistics\n");

  size_t i;
  for(i=0; i<MSTRO_STATS_CATEGORY__MAX; i++) {
    ret=mstro_stats_report_category(dst,
                             mstro_stats_category_name[i],
                             g_collection[i]);
    if(ret!=MSTRO_OK)
      return ret;
  }
  /** keep CSV report at end **/
  PRINT_OR_LOG(dst, ";;; start CSV report\n");
  ret = mstro_stats_report_csv(dst);
  PRINT_OR_LOG(dst, ";;; end CSV report\n");

  return ret;
}

mstro_status
mstro_stats_register_label(enum mstro_stats_category category,
                           const char *label, enum mstro_stats_collection_type type)
{
  int stat;
  if(category>=MSTRO_STATS_CATEGORY__MAX)
    return MSTRO_INVARG;
  if(label==NULL)
    return MSTRO_INVARG;
  if(type>=MSTRO_STATS_COLLECTION__MAX)
    return MSTRO_INVARG;

  struct collection *entry = calloc(1,sizeof(struct collection));
  if(entry==NULL) {
    return MSTRO_NOMEM;
  }
  entry->label = strdup(label);
  if(entry->label==NULL) {
    free(entry);
    return MSTRO_NOMEM;
  }
  entry->type = type;

  stat=pthread_mutex_lock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to lock statistics mutex\n");
    return MSTRO_FAIL;
  }

  /* FIXME: should we check for re-registration of exisiting label? */
  HASH_ADD_STR(g_collection[category],label,entry);

  stat=pthread_mutex_unlock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to unlock statistics mutex\n");
    return MSTRO_FAIL;
  }

  return MSTRO_OK;

}

/** store a 'count' event for LABEL in CATEGORY */
mstro_status
mstro_stats_store_counter(enum mstro_stats_category category,
                          const char *label,
                          uint64_t counter)
{
  if(category>=MSTRO_STATS_CATEGORY__MAX)
    return MSTRO_INVARG;
  if(label==NULL)
    return MSTRO_INVARG;

  int stat;
  mstro_status ret;
  struct counter_measurements *elt=NULL;

  stat = pthread_mutex_lock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to lock statistics mutex:%d\n", stat);
    return MSTRO_FAIL;
  }
  /* check whether we have a declaration for LABEL */
  struct collection *entry;
  HASH_FIND_STR(g_collection[category],label,entry);
  if(!entry) {
    ERR("Attempt to add to undeclared statistics label %s\n",
        label);
    ret=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  if(entry->type!=MSTRO_STATS_COLL_COUNTER) {
    ERR("Attempt to record counter in a collection not supporting this\n");
    ret=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  /* push to list of data */
  elt = malloc(sizeof(struct counter_measurements));
  if(elt==NULL) {
    ERR("Failed to allocate measurement\n");
    ret=MSTRO_NOMEM;
    goto BAILOUT_UNLOCK;
  }
  elt->count = counter;
  LL_PREPEND(entry->counter,elt);
  ret=MSTRO_OK;

BAILOUT_UNLOCK:
  stat = pthread_mutex_unlock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to unlock statistics mutex:%d\n", stat);
    ret=MSTRO_FAIL;
  }
  return ret;
}

/** add a count to the most recently stored counter event for LABEL in
 * CATEGORY */
mstro_status
mstro_stats_add_counter(enum mstro_stats_category category, const char *label,
                        uint64_t increment)
{
  if(category>=MSTRO_STATS_CATEGORY__MAX)
    return MSTRO_INVARG;
  if(label==NULL)
    return MSTRO_INVARG;

  int stat;
  mstro_status ret;
  stat = pthread_mutex_lock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to lock statistics mutex:%d\n", stat);
    return MSTRO_FAIL;
  }
  /* check whether we have a declaration for LABEL */
  struct collection *entry;
  HASH_FIND_STR(g_collection[category],label,entry);
  if(!entry) {
    ERR("Attempt to add to undeclared statistics label %d\n", label);
    ret=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  if(entry->type!=MSTRO_STATS_COLL_COUNTER) {
    ERR("Attempt to record counter in a collection not supporting this\n");
    ret=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  if(entry->counter==NULL) {
    /* new data */
    struct counter_measurements *elt
        = malloc(sizeof(struct counter_measurements));
    if(elt==NULL) {
      ERR("Failed to allocate measurement\n");
      ret=MSTRO_NOMEM;
      goto BAILOUT_UNLOCK;
    }
    elt->count = 0;
    LL_PREPEND(entry->counter,elt);
  }

  /* DEBUG("Stepping counter %s from %zu to %zu\n", */
  /*       label, entry->counter->count,   entry->counter->count+increment); */
  entry->counter->count+=increment;

  ret=MSTRO_OK;

BAILOUT_UNLOCK:
  stat = pthread_mutex_unlock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to unlock statistics mutex:%d\n", stat);
    ret=MSTRO_FAIL;
  }
  return ret;
}


/** store a timing for LABEL in CATEGORY under KEY */
mstro_status
mstro_stats_store_timing(enum mstro_stats_category category, const char *label,
                  uint64_t key, mstro_nanosec_t time)
{
  if(category>=MSTRO_STATS_CATEGORY__MAX)
    return MSTRO_INVARG;
  if(label==NULL)
    return MSTRO_INVARG;

  int stat;
  mstro_status ret;
  struct key_time_measurements *elt = NULL;
  stat = pthread_mutex_lock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to lock statistics mutex:%d\n", stat);
    return MSTRO_FAIL;
  }
  /* check whether we have a declaration for LABEL */
  struct collection *entry;
  HASH_FIND_STR(g_collection[category],label,entry);
  if(!entry) {
    ERR("Attempt to add to undeclared statistics label %d\n", label);
    ret=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  if(entry->type!=MSTRO_STATS_COLL_KEY_TIME) {
    ERR("Attempt to record timing in a collection not supporting this\n");
    ret=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  /* push to list of data */
  elt = malloc(sizeof(struct key_time_measurements));
  if(elt==NULL) {
    ERR("Failed to allocate measurement\n");
    ret=MSTRO_NOMEM;
    goto BAILOUT_UNLOCK;
  }
  elt->key = key;
  elt->time = time;
  LL_PREPEND(entry->key_time,elt);
  ret=MSTRO_OK;

BAILOUT_UNLOCK:
  stat = pthread_mutex_unlock(&g_statistics_mutex);
  if(stat!=0) {
    ERR("Failed to unlock statistics mutex:%d\n", stat);
    ret=MSTRO_FAIL;
  }
  return ret;
}


/** Initialise statistics module */

/** set of predefined ones */
#define NUM_PREDEFINED_LABELS 55
static struct {
  enum mstro_stats_category category;
  const char *label;
  enum mstro_stats_collection_type type;
} predefined_labels[NUM_PREDEFINED_LABELS] = {
  { MSTRO_STATS_CAT_GENERAL,
    MSTRO_STATS_L_COMPONENT_RUNTIME,
    MSTRO_STATS_COLL_KEY_TIME },
  { MSTRO_STATS_CAT_POOL,
    MSTRO_STATS_L_POOL_ASSOCIATION_TIME,
    MSTRO_STATS_COLL_KEY_TIME },
  { MSTRO_STATS_CAT_OBJECTS,
    MSTRO_STATS_L_NUM_CDOS_CREATED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_POOL,
    MSTRO_STATS_L_BYTES_POOLED,
    MSTRO_STATS_COLL_COUNTER},
  { MSTRO_STATS_CAT_TRANSPORT,
    MSTRO_STATS_L_BYTES_MOVED,
    MSTRO_STATS_COLL_COUNTER },
  /* PC */
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_DECLARE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_DECLARE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_DECLARE_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_SEAL,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_SEAL,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_SEAL_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_SEAL_GROUP,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_OFFER,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_OFFER,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_OFFER_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_WITHDRAW,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_WITHDRAW,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_WITHDRAW_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_REQUIRE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_REQUIRE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_REQUIRE_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_RETRACT,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_RETRACT,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_RETRACT_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_DEMAND,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_DEMAND,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_ASYNC_DEMAND_BLOCKED,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_DISPOSE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_SUBSCRIBE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_POOL_EVENTS,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_TICKETS_OUT,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_TICKETS_IN,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PC_NUM_RESOLVE,
    MSTRO_STATS_COLL_COUNTER },
  /* PM */
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_JOIN,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_LEAVE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_DECLARE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_SEAL,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_SEAL_GROUP,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_OFFER,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_WITHDRAW,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_REQUIRE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_RETRACT,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_DEMAND,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_DISPOSE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_SUBSCRIBE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_POOL_EVENTS,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_TICKETS,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_TRANSFER_COMPLETIONS,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_PROTOCOL,
    MSTRO_STATS_L_PM_NUM_RESOLVE,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_POOL,
    MSTRO_STATS_L_PM_NUM_IMM_WITHDRAWS,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_POOL,
    MSTRO_STATS_L_PM_NUM_WITHDRAW_WAKEUPS,
    MSTRO_STATS_COLL_COUNTER },
  /* OFI */
  { MSTRO_STATS_CAT_OFI,
    MSTRO_STATS_L_PC_NUM_VSM_IN,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_OFI,
    MSTRO_STATS_L_PC_NUM_VSM_OUT,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_OFI,
    MSTRO_STATS_L_PM_NUM_VSM_IN,
    MSTRO_STATS_COLL_COUNTER },
  { MSTRO_STATS_CAT_OFI,
    MSTRO_STATS_L_PM_NUM_VSM_OUT,
    MSTRO_STATS_COLL_COUNTER },

};

mstro_status
mstro_stats_init(void)
{
  mstro_status s = MSTRO_OK;
  mstro_stats__init_clock();

  /* declare predefined labels */
  for(size_t i=0; i<NUM_PREDEFINED_LABELS; i++) {
    s = mstro_stats_register_label(predefined_labels[i].category,
                                   predefined_labels[i].label,
                                   predefined_labels[i].type);
    //    DEBUG("registered stats label %s\n", predefined_labels[i].label);
    if(s!=MSTRO_OK)
      break;
  }
BAILOUT:
  return s;
}

/** Finalize statistics module */
mstro_status
mstro_stats_finalize(void)
{
  /* Clean up all of categorized storage */
  size_t cat;
  for(cat=0; cat<MSTRO_STATS_CATEGORY__MAX; cat++) {
    if(g_collection[cat]==NULL)
      continue;
    struct collection *el,*tmp;
    HASH_ITER(hh,g_collection[cat],el,tmp) {
      switch(el->type) {
        case MSTRO_STATS_COLL_COUNTER: {
          struct counter_measurements *entry, *tmpe;
          LL_FOREACH_SAFE(el->counter,entry,tmpe) {
            LL_DELETE(el->counter,entry);
            free(entry);
          }
          break;
        }
        case MSTRO_STATS_COLL_KEY_VAL: {
          struct key_val_measurements *entry,*tmpe;
          LL_FOREACH_SAFE(el->key_val,entry,tmpe) {
            /* FIXME: Confirm if entry->val is user owned or mstro_stats owned
             * if mstro_stats-owned it should be freed here */
            LL_DELETE(el->key_val,entry);
            free(entry);
          }
          break;
        }
        case MSTRO_STATS_COLL_KEY_TIME: {
          struct key_time_measurements *entry,*tmpe;
          /* sort entries by key */
          LL_FOREACH_SAFE(el->key_time,entry,tmpe) {
            LL_DELETE(el->key_time,entry);
            free(entry);
          }
          break;
        }
        default:
          return MSTRO_FAIL;
      }
      HASH_DEL(g_collection[cat],el);
      free(el->label);
      free(el);
    }
  }

  return MSTRO_OK;
}
