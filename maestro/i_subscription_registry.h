
/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Subscription registry implementation
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAESTRO_I_SUBSCRIPTION_REGISTRY_H_
#define MAESTRO_I_SUBSCRIPTION_REGISTRY_H_ 1

/**@addtogroup MSTRO_Internal
 **@{
 **/

/**@defgroup MSTRO_I_SUBSCRIPTION_REGISTRY Maestro Subscription Registry implementation
 **@{
 ** This is the Pool Subscription and event handling interface
 **/


#include "maestro/status.h"
#include "maestro/i_pool_manager_protocol.h"
#include "protocols/mstro_pool.pb-c.h"

#include "cdo_sel_parse.h"

#include <stdbool.h>
#include <stdatomic.h>

/** the CDO selector handle */
struct mstro_cdo_selector_ {
  _Atomic(size_t)       refcount;  /**< reference count */
  mstro_schema          schema;    /**< schema to evaluate query in */
  char                 *namespace; /**< namespace for query evaluation */
  char                 *query;     /**< the query string (unparsed) */
  struct mstro_csq_val *csq;       /**< parsed cdo-selector-query */
};

/** the invalid subscription handle */
#define MSTRO_SUBSCRIPTION_HANDLE_INVALID 0

/** the subscription handle structure */
struct mstro_subscription_ {
  _Atomic(size_t) refcount; /**< reference count */
  /** the subscription handle, assigned by PM, usable directly for
   * messaging. A value of ::MSTRO_SUBSCRIPTION_HANDLE_INVALID in
   * handle.handle indicates 'not set' */
  Mstro__Pool__SubscriptionHandle handle; 

  struct mstro_cdo_selector_ *cdo_selector; /**< the CDO selector for the subscription */
  mstro_pool_event_kinds      event_mask;   /**< the event mask for the subscription */
  bool needs_ack;                           /**< flag for notify-and-wait-for-ack semantic */
  bool no_resolve;                          /**< flag to indicate IDs should not be resolved */
  bool signal_before;                       /**< flag for signal-before-handling event semantic */
  bool no_local;                            /**< flag to avoid local events notifications */
  pthread_mutex_t event_mtx;                /**< a mutex for the event queue */
  pthread_cond_t event_cond;                /**< a condition variable to wait on for new events */
  struct mstro_pool_event_ *event_list;     /**< the list of events
                                             * that were received and
                                             * have not been picked up
                                             * by the user */
  struct mstro_pool_event_ *last_event;          /**< the tail element of
                                             * event_list, for quick
                                             * append */
};

/** Initialize subscription subsystem */
mstro_status
mstro_subscriptions_init(void);

/** De-initialize subscription subsystem */
mstro_status
mstro_subscriptions_finalize(void);



/** The subscription table type */
typedef struct mstro_subscription_table_ *mstro_subscription_table;

/** @brief Add a subscription to local or pool manager subscription table */
mstro_status
mstro_subscription_register(mstro_subscription subscription);

/** @brief Add a pool manager message subscription message to subscription table */
mstro_status
mstro_subscription_message_register(const Mstro__Pool__Subscribe *smsg, mstro_app_id origin,
                                    Mstro__Pool__SubscriptionHandle **result);

/** @brief Handle incoming pool manager message acknowledging subscription */
mstro_status
mstro_subscription_register_bh(const Mstro__Pool__SubscribeAck *ack);


/** @brief Remove a subscription from local or pool manager subscription table */
mstro_status
mstro_subscription_unregister(mstro_subscription subscription);

/** @brief Add a pool manager message subscription message to subscription table */
mstro_status
mstro_subscription_message_unregister(const Mstro__Pool__Unsubscribe *umsg, mstro_app_id origin);

/** @brief Handle incoming pool manager message acknowledging unsubscription */
mstro_status
mstro_subscription_unregister_bh(const Mstro__Pool__SubscriptionHandle *h);

/** @brief Handle acknowledgement of an event
 *
 * If @arg e is a list of events, acknowledge all of them.
 */
mstro_status
mstro_subscription_event_ack(mstro_subscription s, mstro_pool_event e);

/** @brief Handle acknowledgement message for an event */
mstro_status
mstro_subscription_message_event_ack(const Mstro__Pool__EventAck *msg);


/** @brief Let all subscriptions in @arg tab notice that event @arg e occured.
 *
 * If @arg beforep is true, this is a 'BEFORE' notification for the
 * event, otherwise an 'AFTER' notification.
 *
 * If any of the notifications requires an ACK, this function will
 * block until all these ACKs have occured.
 *
 * Local subscriptions will be handled directly, remote subscriptions
 * will receive event messages.
 */
mstro_status
mstro_pool_event_advertise(Mstro__Pool__Event *e,
                           bool beforep,
                           _Atomic(uint64_t) *nr_outstanding_acks);


/** @brief Handle an incoming event notification */
mstro_status
mstro_pool_event_consume(const Mstro__Pool__Event *e);



/** @brief Handle a resolver reply message, caching its data */
mstro_status
mstro_pool_resolve_reply_bh(const Mstro__Pool__ResolveReply *reply);

/** @brief Fetch name of ID from resolver cache, possibly querying the
 * PM to find it.
 *
 * Result @arg *name references a unique string per CDO-ID that
 * remains valid until @ref mstro_finalize()
 **/
mstro_status
mstro_pool_resolve_cdoid(const struct mstro_cdo_id *id,
                         const char **name);


/** Evaluate SEL as predicate over ATTRIBUTES for CDOID.
 *
 * This function is the shared part of local and pool-manager side cdo
 * selector checking. It works on the attributes message for
 * convenience, even though we may have the attribute dictionary for
 * local CDOs, or could construct it for the PM. If it becomes a
 * bottlenect this should be improvable.
 *
 * Return MSTRO_OK for 'matched', MSTRO_NOMATCH if not, or an error
 * code.
 * 
 */
mstro_status
mstro_subscription_selector_eval(const struct mstro_cdo_id *cdoid,
                                 const struct mstro_cdo_selector_ *sel,
                                 const Mstro__Pool__Attributes *attributes);

/**@} (end of group MSTRO_I_SUBSCRIPTION_REGISTRY) */
/**@} (end of group MSTRO_Internal) */

#endif /* MAESTRO_I_SUBSCRIPTION_REGISTRY_H_ */
