#include "maestro/i_memory.h"

#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#include <stdatomic.h>

#include "maestro/logging.h"


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)



_Atomic(uint64_t) mstro_key_counter = MSTRO_MEM_KEY_BASE;

uint64_t
mstro_memory_new_key()
{
  uint64_t retval = atomic_fetch_add(&mstro_key_counter,1);
  if(retval==0) {
    ERR("MR key wraparound, cannot continue\n");
    abort();
  }
  return retval;
}
  

void *
mstro_alloc_lockable(size_t size, bool lock_immediately)
{
  void *ptr=NULL;
  int retval;
  /* 
   * alloc memory aligned to a page, to prevent two mlock() in the
   * same page.
   */
  retval = posix_memalign(&ptr, (size_t) sysconf(_SC_PAGESIZE), size);
  
  /* return NULL on failure */
  if (retval)
    return NULL;
  
  /* lock this buffer into RAM */
  if(lock_immediately)
    if (mlock(ptr, size)) {
      free(ptr);
      return NULL;
    }
  return ptr;
}

void
mstro_unlock_lockable(void *ptr, size_t size)
{
    munlock(ptr, size);
}

mstro_status 
mstro_free_lockable(void *ptr, size_t size, bool needs_unlock)
{
  if(ptr==NULL)
    return MSTRO_INVARG;
  
  if(needs_unlock)
    /* unlock the address range */
    mstro_unlock_lockable(ptr, size);

  /* free the memory */
  free(ptr);
  return MSTRO_OK;
}



