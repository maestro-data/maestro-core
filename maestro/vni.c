/* -*- mode:c -*- */
/** @file
 ** @brief HPE SS11 Virtual Network Identifier interface
 **/

/* Copyright © 2023 Hewlett Packard Enterprise Development LP
 * Copyright (C) 2019 Cray Computer GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_vni.h"
#include "maestro/logging.h"
#include "maestro/status.h"
#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>
#include <string.h>

#define NFREE(x) do { if(x) free(x); } while(0)

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)

/* until CXI hits upstream: */
#define FI_FULL_VERSION(major,minor,revision) (((major) << 16) | ((minor) << 8) | (revision)) 
#if FI_VERSION_LT(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
#define FI_ADDR_CXI  (FI_ADDR_PSMX3 + 1)
#define FI_PROTO_CXI (FI_PROTO_PSMX3 + 1)
#endif

/** Insert vni credentials into a libfabric object */
mstro_status
mstro_vni_insert_ofi(struct fi_info *fi, const mstro_vni_info info)
{
  	fi_vni_auth_key *auth_key = NULL;
  	if(fi==NULL||info==NULL)
    	return MSTRO_INVARG;

	/*CXI and CXI_COMPAT are the same due to the compatibility issue in 1.15.2*/
    #if FI_VERSION_GE(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
	#define FI_ADDR_CXI_COMPAT (FI_ADDR_PSMX3+1)
	assert((fi->addr_format==FI_ADDR_CXI) || (fi->addr_format == FI_ADDR_CXI_COMPAT));
	#else
	assert(fi->addr_format==FI_ADDR_CXI);
	#endif

	if (info->auth_key == NULL)
	{
		ERR("Missing VNI support\n"); /**We can still continue on systems that do not enforce vni yet*/
	    fi->domain_attr->auth_key = NULL;
		return MSTRO_OK;
	}
	   
	#if FI_VERSION_GE(FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION), FI_VERSION(1,5))
  		// auth key is free()ed by fabric.c on teardown, so we need to duplicate it
		/**allocate a new key*/
		auth_key = malloc(sizeof(fi_vni_auth_key));
   		if (auth_key == NULL)
   		{
			ERR("unable to allocate memory for auth_key \n");
			return MSTRO_NOMEM;
   		}
		/**fill key from info*/
  		auth_key->svc_id = info->auth_key->svc_id;
  		auth_key->vni = info->auth_key->vni;
  		fi->domain_attr->auth_key = (uint8_t*) auth_key;
  		fi->domain_attr->auth_key_size = sizeof(info->auth_key);
	#else
  		#warning Open Fabric version too old for auth_key suppport
	#endif
  
  	return MSTRO_OK;
}

static inline
void
mstro_vni_serialize(mstro_vni_info result)
{
	int ret = 0;
	if(result->auth_key == NULL) {
		ret = snprintf(result->serialized_id, MSTRO_VNI_SERIALIZED_LEN,
                     "%" PRIu32",%" PRIu32, 0, 0);	
	}
	else {
  		ret = snprintf(result->serialized_id, MSTRO_VNI_SERIALIZED_LEN,
                     "%" PRIu32",%" PRIu32, result->auth_key->vni, result->auth_key->svc_id);
	}
  	assert(ret<MSTRO_VNI_SERIALIZED_LEN);
}

/** obtain VNI credentials */
mstro_status
mstro_vni_init(mstro_vni_info *result_p)
{
  mstro_status stat = MSTRO_UNIMPL;
  DEBUG("Initializing VNI credentials\n");
  if(result_p==NULL)
  {
	ERR("Invalid output argument\n");
    return MSTRO_INVOUT;
  }
  *result_p = malloc(sizeof(struct mstro_vni_info_));
  if(*result_p) {
    
	mstro_vni_get_auth_key_from_env(&(*result_p)->auth_key, "cxi0"); /* for testing now, then we should take all service ids and devices we find. 
														 When we fill the key, we should be given the device name at this point to pass 
														the correct service id fi->nic->device_attr->name*/
    if((*result_p)->auth_key == NULL)
    {
	      INFO("Missing VNI support\n"); 
    }
    else {
		mstro_vni_serialize(*result_p);
    	DEBUG("Allocated VNI credential %" PRIu32 " (service id: %" PRIu32", serialized %s)\n",
         (*result_p)->auth_key->vni, (*result_p)->auth_key->svc_id,
         (*result_p)->serialized_id);
	}
    stat=MSTRO_OK;
    
  } else {
	ERR("Could not allocate vni info object\n");
    stat=MSTRO_NOMEM;
    goto BAILOUT_FREE;
  }
  goto BAILOUT;
BAILOUT_FREE:
  NFREE(*result_p);
BAILOUT:
  return stat;
}

/** free VNI credentials */
mstro_status
mstro_vni_destroy(mstro_vni_info info)
{
  if(info==NULL)
    return MSTRO_INVARG;
  if(info->auth_key)
  	free(info->auth_key);
  free(info);
  return MSTRO_OK;
}

mstro_status
mstro_vni_init_from_credential(mstro_vni_info *result_p,
                               Mstro__CredCXI *credential)
{
  mstro_status status = MSTRO_UNIMPL;

  int ret;

  if(result_p==NULL)
    return MSTRO_INVOUT;
  
  *result_p = malloc(sizeof(struct mstro_vni_info_));
  if(*result_p) {
	/**check if there if the credential is valid*/
	if(credential == NULL)
	{
		(*result_p)->auth_key = NULL; /**we do not have VNIs enabled*/
		return MSTRO_UNIMPL;
	}
	(*result_p)->auth_key = malloc(sizeof(fi_vni_auth_key));
        
	if ((*result_p)->auth_key == NULL)
	{
		status = MSTRO_NOMEM;
		return status;
	}
	(*result_p)->auth_key->svc_id = credential->svc_id;
	(*result_p)->auth_key->vni = credential->vni;

    mstro_vni_serialize(*result_p);

    DEBUG("Allocated VNI credential %" PRIu32 " (service id: %" PRIu32", serialized %s)\n",
         (*result_p)->auth_key->vni, (*result_p)->auth_key->svc_id,
         (*result_p)->serialized_id);
    status = MSTRO_OK;
  } else {
    status = MSTRO_NOMEM;
  }
  goto BAILOUT;
BAILOUT_FREE:
  NFREE(*result_p);
BAILOUT:
  return status;  
}


mstro_status
mstro_vni_get_auth_key_from_env(fi_vni_auth_key **auth_key, const char *device_name)
{
	uint32_t svc_id;
	uint16_t vni;
	char *token=NULL;
	char *vni_str = NULL; 
	char *svc_ids_str = NULL; 
	char *devices_str = NULL;
        int device_index = 0;
	/* Trying to read launcher allocated vni*/
	/* Launcher will deliver the three new environment variables. 
	 * It is important to note the VNI is per job-step (application), but the Service_ID is per job-step per NIC. 
	 * A process must have chosen which NIC it will use before it can obtain the correct Service ID.
	 */

	DEBUG("Reading VNIs from env\n");
	char *tmp = getenv("SLINGSHOT_VNIS");
	if (tmp != NULL)
	{
		vni_str = strdup(tmp);
	}
	tmp = getenv("SLINGSHOT_SVC_IDS");
	if (tmp != NULL)
	{
		svc_ids_str = strdup(tmp);
	}
	tmp = getenv("SLINGSHOT_DEVICES");
	if (tmp != NULL)
	{
		devices_str = strdup(tmp);
	}
	/* The SLINGSHOT_VNIS  is a comma-separated list of authorized VNIs. 
         * Multiple VNIs for a service are separated by colons. A typical application will use the first VNI supplied. 
         * For coupled applications, another per-job VNI will be supplied. If supplied, it will be the second VNI listed.
	 */
	
	if( (vni_str == NULL) || (svc_ids_str == NULL) || (devices_str == NULL))
      	{
              WARN("Launcher did not put the vni info for us .... returning NULL\n");
	      *auth_key = NULL;
	      return MSTRO_FAIL;
      	}
      	else
      	{
              DEBUG("SLINGSHOT_VNIS: %s, SVC_IDS: %s, SLINGSHOT_DEVICES %s\n", vni_str, svc_ids_str, devices_str);
	      /*Taking the last VNI in the list, to support coupled applications */
	      token = strrchr(vni_str, ',');  /*find the last comma*/
	      if(token != NULL)
	      {
		      DEBUG("Last VNI is %s \n", token+1);
		      vni = (uint16_t) atoi(token+1);
	      }
	      else
	      {
		      DEBUG("Only found one VNI: %s \n", vni_str);
		      vni = (uint16_t) atoi(vni_str);
	      }
	 
	/* The SLINGSHOT_DEVICES is comma-separated list of device names. 
	 * These device names correspond to the individual NIC devices on the local node. e.g., SLINGSHOT_DEVICES="cxi0,cxi1,cxi2,cxi3"
	 * Find the index of our device in the list of SLINGSHOT_DEVICES/
	 */
	      token = NULL;
	      token = strtok(devices_str, ",");
	      while ((token!=NULL) && (strcmp(token, device_name)!=0))
	      {
		      device_index++;
		      token = strtok(NULL,",");
	      }
	      if(token!=NULL)
	      {
		      DEBUG("Found device %s at index %d \n", device_name, device_index);
	      }
	      else
	      {
		      ERR("Could not find device name (%s) in SLINGSHOT_DEVICES list %s\n", device_name, devices_str);
		      *auth_key = NULL;
		      return MSTRO_FAIL;
	      }
	/* The SLINGSHOT_SVC_IDS variable is comma-separated list of CXI service IDs, one SVC_ID for each DEVICE listed. 
	 */

	      /*Looking for serivice id that match device index here*/
	      token = NULL;
	      int count = 0;
	      token = strtok(svc_ids_str, ",");
	      while((token!=NULL) && (count < device_index))
	      {
		      count++;
		      token = strtok(NULL,",");
	      }
	      
	      if(token != NULL)
	      {
		      DEBUG("Found service id %s at index %d \n", token, count);
		      svc_id = (uint32_t) atoi(token);
	      }
	      else
	      {
		      ERR("Could not find a service id at index %d in SLINGSHOT_SVC_IDS list %s\n", device_index, svc_ids_str);
                      *auth_key = NULL;
                      return MSTRO_FAIL;
	      }

	      /*creating and filling the key structure */
	      *auth_key = malloc(sizeof(fi_vni_auth_key));
	      if (*auth_key == NULL)
	      {
		      ERR("Failed to allocate auth_key structure \n");
		      return MSTRO_NOMEM;
	      }
	      (*auth_key)->vni = vni;
	      (*auth_key)->svc_id = svc_id;
              free(vni_str);
	      free(svc_ids_str);
	      free(devices_str);
	      return MSTRO_OK;
	}

}

