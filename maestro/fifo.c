/* -*- mode:c -*- */
/** @file
 ** @brief Maestro FIFO infrastructure
 **/
/*
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "i_fifo.h"

#include "maestro/i_kdq.h"
#include <pthread.h>
#include <stdlib.h>
#include <assert.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)



/** queue container type for void* (converted to uintptr_t since kdq
 * does not support pointer type elements) */
KDQ_INIT(uintptr_t)

struct mstro_fifo_ {
  pthread_mutex_t lock; /**< lock for fifo */
  pthread_cond_t  cvar; /**< condition variable for waiters */
  kdq_t(uintptr_t)  *q; /**< the dequeue */
};

mstro_status
mstro_fifo_init(mstro_fifo* res)
{
  if(res==NULL)
    return MSTRO_INVOUT;
  *res = malloc(sizeof(struct mstro_fifo_));
  if(*res==NULL)
    return MSTRO_NOMEM;

  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
#if 0
  pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
#else
  pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
#endif
  int s = pthread_mutex_init(&(*res)->lock, &attr);
  if(s!=0) {
    ERR("Failed to init mutex: %d (%s)\n", s, strerror(s));
    return MSTRO_FAIL;
  }
  pthread_mutexattr_destroy(&attr);

  s = pthread_cond_init(&(*res)->cvar, NULL);
  if(s!=0) {
    ERR("Failed to init condvar: %d (%s)\n", s, strerror(s));
    return MSTRO_FAIL;
  }

  (*res)->q = kdq_init(uintptr_t);
  if((*res)->q==NULL) {
    ERR("Failed to init dequeue\n");
    return MSTRO_NOMEM;
  }
  return MSTRO_OK;
}

mstro_status
mstro_fifo_finalize(mstro_fifo f)
{
  if(f==NULL) 
    return MSTRO_INVARG;
  int s = pthread_mutex_lock(&f->lock);
  if(s!=0) {
    ERR("Failed to lock fifo %p: %d (%s)\n", f, s, strerror(s));
    return MSTRO_FAIL;
  }
  if(kdq_size(f->q)>0) {
    ERR("FIFO has %zu elements, refusing to destroy it\n", kdq_size(f->q));
    pthread_mutex_unlock(&f->lock);
    return MSTRO_FAIL;
  }

  kdq_destroy(uintptr_t, f->q);
  pthread_cond_destroy(&f->cvar);
  pthread_mutex_unlock(&f->lock);
  pthread_mutex_destroy(&f->lock);

  free(f);
  return MSTRO_OK;
}

void
mstro_fifo__unlock_mutex(void* arg) 
{
  pthread_mutex_t *mtx = (pthread_mutex_t*)arg;
  int s = pthread_mutex_unlock(mtx);
  if(s!=0) {
    ERR("Failed to unlock mutex %p: %d (%s)\n", mtx, s, strerror(s));
  }
  return;
}


void *mstro_fifo_dequeue(mstro_fifo f)
{
  uintptr_t *box;
  uintptr_t val;

  assert(f!=NULL);
  int s = pthread_mutex_lock(&f->lock);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    return NULL;
  }
  pthread_cleanup_push(mstro_fifo__unlock_mutex, &f->lock);

  while(kdq_size(f->q)==0) {
    NOISE("FIFO %p is empty, waiting\n", f);
     
    s=pthread_cond_wait(&f->cvar, &f->lock);
   
    if(s!=0) {
      ERR("Failed to wait on fifo: %d (%s)\n", s, strerror(s));
      abort();
    }
  }
  box = kdq_shift(uintptr_t, f->q);
  val = *box;
  
  pthread_cleanup_pop(1);
  
  return (void*)val;
}

/**immediate dequeue, i.e, nonblocking dequeue function*/
void *mstro_fifo_i_dequeue(mstro_fifo f)
{
  uintptr_t *box;
  uintptr_t val;
  void * result;
  assert(f!=NULL);
  int s = pthread_mutex_lock(&f->lock);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    return NULL;
  }
  pthread_cleanup_push(mstro_fifo__unlock_mutex, &f->lock);

  if(kdq_size(f->q)==0) {
    NOISE("FIFO %p is empty\n", f);
    result = NULL;
  }
  else 
  {
    box = kdq_shift(uintptr_t, f->q);
    val = *box;
    result = (void *) val;
  }
  
  pthread_cleanup_pop(1);
  
  return result;
}

mstro_status
mstro_fifo_enqueue(mstro_fifo f, void* obj)
{
  
  if(f==NULL) {
    return MSTRO_INVARG;
  }
  if(obj==NULL) {
    return MSTRO_INVARG;
  }

  mstro_status stat=MSTRO_OK;
  int s = pthread_mutex_lock(&f->lock);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    return MSTRO_FAIL;
  }

  kdq_push(uintptr_t, f->q, (uintptr_t)obj);

  s= pthread_cond_signal(&f->cvar);
  if(s!=0) {
    ERR("Failed to signal on fifo %p: %d (%s)\n", f, s, strerror(s));
    stat=MSTRO_FAIL;
  }
  s = pthread_mutex_unlock(&f->lock);
  if(s!=0) {
    ERR("Failed to release fifo lock: %d (%s)\n", s, strerror(s));
    stat=MSTRO_FAIL;
  }
  return stat;
}
