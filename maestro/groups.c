#include "i_groups.h"
#include "maestro/i_globals.h"
#include "maestro/i_pool_manager_registry.h"
#include "maestro/i_statistics.h"
#include "protocols/mstro_pool.pb-c.h"

#include <stdatomic.h>

#include "maestro/i_hashing.h"
/* The group table is a 2-level hash table: By proxy CDO ID, then for
 * each of them a hash table by group handle */

/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)

KHASH_INIT(group_handle_set, mstro_group,
          /* dummy: value type      */ char,
          /* not a map, just a set: */ 0,
           kh_voidptr_hash_func, kh_voidptr_hash_equal)

/* Declare group hash type: key is proxy CDOID, value is a
 * khash_t(group_handle_set) table with all the group handles.  */
KHASH_INIT(group, const struct mstro_cdo_id*,
           khash_t(group_handle_set)*, 1,
           kh_cdoidptr_hash_func, kh_cdoidptr_hash_equal)

khash_t(group) *g_mstro_group_table = NULL;

/** a lock for @ref g_group_table */
static pthread_mutex_t g_mstro_group_table_mtx = PTHREAD_MUTEX_INITIALIZER;

/** utility lock macro for group table */
#define WITH_LOCKED_GROUP_TABLE(body) \
  do {                                                                  \
    int wlgt_stat=pthread_mutex_lock(&g_mstro_group_table_mtx);         \
    if(wlgt_stat!=0) {                                                  \
      ERR("Failed to lock group table: %d (%s)\n",                      \
          wlgt_stat, strerror(wlgt_stat));                              \
      abort();                                                          \
    }                                                                   \
                                                                        \
    do {                                                                \
      body;                                                             \
    } while(0);                                                         \
                                                                        \
    wlgt_stat=pthread_mutex_unlock(&g_mstro_group_table_mtx);           \
    if(wlgt_stat!=0) {                                                  \
      ERR("Failed to unlock group table: %d (%s)\n",                    \
          wlgt_stat, strerror(wlgt_stat));                              \
      abort();                                                          \
    }                                                                   \
  } while(0)

static inline
mstro_status
mstro__group_table_insert(mstro_group g)
{
  if(g==NULL) {
    return MSTRO_FAIL;
  }
  /* insertion only permitted very early on */
  assert(g->state&MSTRO_GROUP_STATE_DECLARED);
  
  WITH_LOCKED_GROUP_TABLE({
      if(g_mstro_group_table==NULL) {
	DEBUG("Initilizing group table\n");
        g_mstro_group_table=kh_init(group); /* FIXME: could be moved to group submodule init function */
      }

      khash_t(group_handle_set) *tab=NULL;
      khiter_t entry_for_id = kh_get(group, g_mstro_group_table, &g->id);
      
      if(entry_for_id==kh_end(g_mstro_group_table)) {
        /* need to create a new handle table */
        tab = kh_init(group_handle_set);
        assert(tab!=NULL);
        int ret;
        /* store entry for ID and the empty table */
        entry_for_id = kh_put(group, g_mstro_group_table, &g->id, &ret);
        assert(entry_for_id!=kh_end(g_mstro_group_table));
	assert(ret!=-1);
	assert(ret!=0);
        kh_val(g_mstro_group_table, entry_for_id) = tab;
        DEBUG("First group handle for group %s, creating new handle set %p\n", g->name, tab);
      } else {
        /* table exists */
        tab = kh_val(g_mstro_group_table, entry_for_id);
        DEBUG("Existing group handle for group %s, obtained handle set %p\n", g->name, tab);
      }
      int ret;
      /* store handle for this group in the table */
      kh_put(group_handle_set,tab,g,&ret);
      assert(ret!=-1);
      assert(ret!=0);
      DEBUG("Stored group handle %p for group %s in tab %p, entry %p of group table\n",
		      g, g->name, tab, entry_for_id);
    });
  return MSTRO_OK;
}

static inline
mstro_status
mstro__group_table_remove(mstro_group g)
{
  if(g==NULL) {
    return MSTRO_FAIL;
  }
  /* removal only permitted very early on */
  assert(g->state&MSTRO_GROUP_STATE_DISPOSABLE);

  mstro_status s=MSTRO_UNIMPL;
  
  WITH_LOCKED_GROUP_TABLE({
      if(g_mstro_group_table==NULL) {
        WITH_CDO_ID_STR(tname, &g->id, {
            ERR("Attempt to remove group |%s| (%p) with id |%s| from empty group table\n",
                g->name, g, tname);});
        s=MSTRO_FAIL;
      } else {
        khiter_t handles_for_id = kh_get(group, g_mstro_group_table, &g->id);
      
        if(handles_for_id==kh_end(g_mstro_group_table)) {
          WITH_CDO_ID_STR(tname, &g->id, {
              ERR("Attempt to remove group |%s| (%p) with id |%s| from group table, but ID has no handle table\n",
                  g->name, g, tname);});
          s=MSTRO_FAIL;
        } else {
	  khash_t(group_handle_set) *tab=kh_val(g_mstro_group_table, handles_for_id);
          if(tab==NULL ) {
            WITH_CDO_ID_STR(tname, &g->id, {
                ERR("Attempt to remove group |%s| (%p) with id |%s| from group table, but handle set for group is empty\n",
                    g->name, g, tname);});
            s=MSTRO_FAIL;
	  } else {
	    khiter_t entry = kh_get(group_handle_set, tab, g);
	    if(entry==kh_end(tab)) {
	      WITH_CDO_ID_STR(tname, &g->id, {
                ERR("Attempt to remove group |%s| (%p) with id |%s| from group table, but it's not present\n",
                    g->name, g, tname);});
	      s=MSTRO_FAIL;
	    } else {
              kh_del(group_handle_set, tab, entry);
	      s=MSTRO_OK;
	    }
	  }
        }
      }
    });
  return s;
}

static inline mstro_status
mstro_group__allocate(
    const char *name,
    struct mstro_group_ **result)
{
  if(name==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;

  mstro_status s=MSTRO_NOMEM;
  
  *result = malloc(sizeof(struct mstro_group_));
  if(*result) {
    (*result)->name = strdup(name);
    if(!(*result)->name)
      goto BAILOUT;
    (*result)->undeclared_cdos = kh_init(group_member_name);
    if((*result)->undeclared_cdos==NULL) {
      free((*result)->name);
      goto BAILOUT;
    }
    (*result)->group_cdo = NULL;
    /* hh and idhh do not need init */
    (*result)->self = *result;
    struct mstro_cdo_id tmp = MSTRO_CDO_ID_INVALID;
    (*result)->id = tmp;
    (*result)->state = MSTRO_GROUP_STATE_CREATED;
    (*result)->members = NULL;
    (*result)->post_demand_members = NULL;
    (*result)->member_iter_idx = 0;
    s=MSTRO_OK;
  }
 BAILOUT:
  return s;
}



/* public API */
mstro_status
mstro_group_declare(const char *name, mstro_group* group)
{
  mstro_status s = mstro_group__allocate(name, group);
  if(s==MSTRO_OK) {
    DEBUG("Declaring group-proxy CDO...\n");
    s=mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &((*group)->group_cdo));
    if(s==MSTRO_OK) {
      (*group)->id = (*group)->group_cdo->id;
      WITH_CDO_ID_STR(idstr, &(*group)->id, {
          DEBUG("Assigned proxy CDOID %s to group %s\n", idstr, name);
        });
      bool trueval = true;
      s = mstro_cdo_attribute_set((*group)->group_cdo,
                                  MSTRO_ATTR_CORE_CDO_ISGROUP, &trueval,
                                  true, false);
      if(s==MSTRO_OK) {
        DEBUG("Declared CDO group |%s|\n", name);
        (*group)->state = MSTRO_GROUP_STATE_DECLARED;
        s = mstro__group_table_insert(*group);
        goto DONE;
      }
    }
  } else {
    ERR("Failed to declare CDO group |%s|: %d (%s)\n",
        name, s, mstro_status_description(s));
  }
DONE:
  return s;
}


mstro_status
mstro_group_add(mstro_group g, mstro_cdo cdo)
{
  mstro_status s=MSTRO_UNIMPL;
  struct mstro_group_cdo_member *m=NULL;
  
  if(g==NULL || cdo==NULL) {
    s=MSTRO_INVARG; goto BAILOUT;
  }
  
  m = malloc(sizeof(struct mstro_group_cdo_member));
  if(m==NULL) {
    s=MSTRO_NOMEM; goto BAILOUT;
  }

  /* It is enough to only ensure a declared state at this stage, auto-seal
   * would be done by group_seal anyway */
  if(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_CREATED))
    mstro_cdo_block_until(cdo, MSTRO_CDO_STATE_DECLARED, "DECLARE");
  
  m->entry = cdo;
  m->user_owned = true;
  m->user_pooled = false;
  
  HASH_ADD_PTR(g->members,entry,m);
  
  DEBUG("Added CDO handle %p for |%s| to group |%s|, now have %zu members\n",
        cdo, cdo->name, g->name, (size_t)HASH_COUNT(g->members));
  s=MSTRO_OK;
BAILOUT:
  if(s!=MSTRO_OK) {
    if(m) {
      free(m);
    }
  }
  return s;
}

mstro_status
mstro_group_add_by_name(mstro_group g, const char *name)
{
  mstro_status s=MSTRO_UNIMPL;
  char *n=NULL;

  if(g==NULL || name==NULL) {
    return MSTRO_INVARG;
  }
  n = strdup(name);
  if(n==NULL) {
    return MSTRO_NOMEM;
  }

  int ret;
  kh_put(group_member_name, g->undeclared_cdos, n, &ret);
  if(ret==0) {
    ERR("Failed to insert CDO name |%s| into undeclared members list: already present\n",
        n);
    free(n);
    return MSTRO_FAIL;
  }
  DEBUG("Added undeclared CDO name |%s| to group |%s|, now have %zu names\n",
        name, g->name, (size_t)kh_size(g->undeclared_cdos));
  return MSTRO_OK;
}

mstro_status
mstro_group_seal(mstro_group g)
{
  if(g==NULL || !(g->state & MSTRO_GROUP_STATE_DECLARED))
    return MSTRO_INVARG;

  mstro_status s=MSTRO_UNIMPL;

  /* add all names to a serializable attribute entry */
  Mstro__Pool__GroupMembers *m = malloc(sizeof(Mstro__Pool__GroupMembers));
  if(!m) {
    s=MSTRO_NOMEM; goto BAILOUT;
  }
  mstro__pool__group_members__init(m);

  
  m->n_undeclared_members = (size_t)kh_size(g->undeclared_cdos);
  m->n_declared_members   = (size_t)HASH_COUNT(g->members);
  m->undeclared_members   = malloc(sizeof(char*)*m->n_undeclared_members);
  m->declared_members     = malloc(sizeof(Mstro__Pool__CDOID*)*m->n_declared_members);
  if(m->declared_members==NULL || m->undeclared_members==NULL) {
    goto BAILOUT_FREE;
  }

  /* FIXME: this could be done more efficiently with a big chunk of memory */
  {
    size_t i=0;
    struct mstro_group_cdo_member *el,*tmp;
    HASH_ITER(hh, g->members, el, tmp) {
      m->declared_members[i] = malloc(sizeof(Mstro__Pool__CDOID));
      /* need to ensure the GID is ready */
      if(mstro_cdo_state_check(el->entry, MSTRO_CDO_STATE_DECLARED)) {
        mstro_status s = mstro_cdo_seal(el->entry);
        if(s!=MSTRO_OK) {
          WITH_CDO_ID_STR(idstr, &el->entry->id, {
              ERR("Failed to seal declared group member %s: %d\n", idstr, s);
            });
          goto declare_fixup_failure;
        }
      }
      /* CDO now either auto-sealed (when it was only delared so far), or was already past that.
         Let's double-check: */
      if(!mstro_cdo_state_check(el->entry,
                                (MSTRO_CDO_STATE_SEALED
                                 | MSTRO_CDO_STATE_OFFERED
                                 | MSTRO_CDO_STATE_REQUIRED))) {
        WITH_CDO_ID_STR(gname, &g->id, {
            WITH_CDO_ID_STR(idstr, &el->entry->gid, {
                ERR("Invalid CDO state for group member |%s| in group |%s|: %" PRIu32 "\n",
                    idstr, gname, mstro_cdo_state_get(el->entry));
              });
          };);
        s=MSTRO_INVARG;
        goto declare_fixup_failure;
      } else {
        s=MSTRO_OK;
        mstro__pool__cdoid__init(m->declared_members[i]);
        m->declared_members[i]->qw0 = el->entry->gid.qw[0];
        m->declared_members[i]->qw1 = el->entry->gid.qw[1];
        m->declared_members[i]->local_id = el->entry->gid.local_id;
        i++;
        continue;
      }
   declare_fixup_failure:
      if(s!=MSTRO_OK || m->declared_members[i]==NULL) {
        for(size_t j=i; j>0; j--) {
          free(m->declared_members[j-1]);
        };
        free(m->declared_members);
        m->declared_members=NULL;
        goto BAILOUT_FREE;
      }
    }
  }

  {
    size_t i=0;
    struct mstro_group_cdo_name *el,*tmp;
    const char *name;
    if(g->undeclared_cdos) {
      kh_foreach_key(g->undeclared_cdos, name, {
          /* no need to copy, as after serialization we don't hold a
           * pointer anymore */
          m->undeclared_members[i++] = (char*)name;
        });
    }
  }

  /* FIXME: support m->sel CDO selector here */
  m->sel = NULL;
  
  size_t size = mstro__pool__group_members__get_packed_size(m);
  uint8_t *buf = malloc(size*sizeof(uint8_t));
  if(!buf) {
    ERR("Failed to allocate a group_members buffer\n");
    s=MSTRO_NOMEM;
    goto BAILOUT_FREE;
  }

  size_t written_size = mstro__pool__group_members__pack(m, buf);
  assert(written_size == size);

  DEBUG("Serialized %zu + %zu (decl/undecl) group members data for group |%s|: %zu bytes\n",
        m->n_declared_members, m->n_undeclared_members, g->name, written_size);

  /* now have data to seal the CDO */

  mstro_blob *blob;
  s = mstro_blob_create(size, buf, &blob);
  if(s!=MSTRO_OK) {
    ERR("Failed to wrap group members into blob\n");
    goto BAILOUT_FREE;
  }
  
  s = mstro_cdo_attribute_set(g->group_cdo,
                              MSTRO_ATTR_CORE_CDO_GROUP_MEMBERS,
                              blob, true, false); /* make maestro owner, so that it can be cleaned properly */
  if(s!=MSTRO_OK) 
    goto BAILOUT_FREE;
  
  s = mstro_blob_dispose(blob); /*clean our copy here*/
  if (s!= MSTRO_OK)
	  goto BAILOUT_FREE;

  s = mstro_cdo_seal(g->group_cdo);
  if(s!=MSTRO_OK)
    goto BAILOUT_FREE;

  g->state = MSTRO_GROUP_STATE_SEALED;
  s=MSTRO_OK;

  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_SEAL_GROUP, 1);
    
BAILOUT_FREE:
  if(m) {
    if(m->declared_members) {
      for(size_t i=0; i<m->n_declared_members; i++) {
        free(m->declared_members[i]);
      }
      free(m->declared_members);
    }
    if(m->undeclared_members)
      free(m->undeclared_members);
    free(m);
  }
  
BAILOUT:
  return s;
}

mstro_status
mstro_group_offer(mstro_group g)
{
  if(g==NULL)
    return MSTRO_INVARG;

  DEBUG("%zu members, %zu names\n",
        (size_t)HASH_COUNT(g->members),
        (size_t)kh_size(g->undeclared_cdos));
  
  mstro_status s;
  if(g->state & MSTRO_GROUP_STATE_DECLARED) {
    DEBUG("Auto-sealing group |%s|\n", g->name);
    s=mstro_group_seal(g);
    if(s!=MSTRO_OK) {
      return s;
    }
  }
  assert(g->state & MSTRO_GROUP_STATE_SEALED);

  /* OFFER all handles */
  struct mstro_group_cdo_member *elt, *tmp;
  HASH_ITER(hh, g->members, elt, tmp) {
    if(!mstro_cdo_state_check(elt->entry, MSTRO_CDO_STATE_POOLED)) {
      mstro_cdo_offer(elt->entry);
      elt->user_pooled=false;
    } else {
      elt->user_pooled=true;
    }
  }
  
  /* OFFER group, including batch-declare of name-only members on the PM */
  s = mstro_cdo_offer(g->group_cdo);
  if(s==MSTRO_OK) {
    g->state = MSTRO_GROUP_STATE_OFFERED;
    DEBUG("Group |%s| now offered\n", g->name);
  }
  return s;
}

mstro_status
mstro_group_require(mstro_group g)
{
  if(g==NULL)
    return MSTRO_INVARG;

  mstro_status s;
  if(g->state & MSTRO_GROUP_STATE_DECLARED) {
    DEBUG("Auto-sealing group |%s|\n", g->name);
    s=mstro_group_seal(g);
    if(s!=MSTRO_OK) {
      return s;
    }
  }
  assert(g->state & MSTRO_GROUP_STATE_SEALED);

  /* REQUIRE all handles */
  struct mstro_group_cdo_member *elt, *tmp;
  HASH_ITER(hh, g->members, elt, tmp) {
    if(!mstro_cdo_state_check(elt->entry, MSTRO_CDO_STATE_POOLED)) {
      mstro_cdo_require(elt->entry);
      elt->user_pooled=false;
    } else {
      elt->user_pooled=true;
    }
  }

  /* REQUIRE group, including batch-declare of name-only members on the PM */
  s = mstro_cdo_require(g->group_cdo);
  if(s==MSTRO_OK) {
    g->state = MSTRO_GROUP_STATE_REQUIRED;
    DEBUG("Group |%s| now required\n", g->name);
  }
  return s;
}

mstro_status
mstro_group_retract(mstro_group g)
{
  if(g==NULL) {
    return MSTRO_INVARG;
  }
  assert(g->state & MSTRO_GROUP_STATE_OFFERED || g->state & MSTRO_GROUP_STATE_REQUIRED);

  return MSTRO_UNIMPL;
}

mstro_status
mstro_group_demand(mstro_group g)
{
  if(g==NULL) {
    return MSTRO_INVARG;
  }
  assert(g->state & MSTRO_GROUP_STATE_REQUIRED);

  Mstro__Pool__GroupMembers *members = NULL;

  mstro_status s = mstro_cdo_demand(g->group_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to DEMAND proxy CDO for group %s\n", g->name);
    goto BAILOUT;
  }

  DEBUG("Group |%s| DEMAND: proxy CDO successfully demanded\n", g->name);

  /* fill in information about the members */
  const void *blob;
  enum mstro_cdo_attr_value_type type;
  s = mstro_cdo_attribute_get(g->group_cdo,
                              MSTRO_ATTR_CORE_CDO_GROUP_MEMBERS,
                              &type,
                              &blob);
  assert(type==MSTRO_CDO_ATTR_VALUE_blob);

  const mstro_blob *member_blob = (const mstro_blob*)blob;
  members = mstro__pool__group_members__unpack(
		  NULL, member_blob->len, member_blob->data);

  if(members==NULL) {
    ERR("NULL members structure in group %s\n", g->name);
    s=MSTRO_FAIL;
    goto BAILOUT;
  }
  
  /* incoming member structure can only reference declared members */
  if(members->n_undeclared_members!=0) {
    ERR("Cannot handle %d undeclared members in group %s\n",
        members->n_undeclared_members, g->name);
    s=MSTRO_UNIMPL;
    goto BAILOUT;
  }

  g->post_demand_members = members;
  g->member_iter_idx = 0;
  members=NULL; /* consumed */

  g->state = MSTRO_GROUP_STATE_DEMANDED;

  DEBUG("DEMAND-GROUP for group |%s| (@%p) completed, %zu members\n",
        g->name, g, g->post_demand_members->n_declared_members);

  /* handles that were auto-required need to be auto-demanded */
  struct mstro_group_cdo_member *elt, *tmp;
  HASH_ITER(hh, g->members, elt, tmp) {
    assert(mstro_cdo_state_check(elt->entry, MSTRO_CDO_STATE_POOLED));
    if(! elt->user_pooled) {
      WITH_CDO_ID_STR(idstr, &elt->entry->gid,{
          DEBUG("Auto-demanding CDO %s (@%p)\n",
                idstr, elt->entry);});
      mstro_cdo_demand(elt->entry);
    }
  }

  s=MSTRO_OK;
    
 BAILOUT:
  if(members) {
    mstro__pool__group_members__free_unpacked(members, NULL);
  }
  return s;
}

mstro_status
mstro_group_withdraw(mstro_group g)
{
  if(g==NULL)
    return MSTRO_INVARG;
  if(!(g->state&MSTRO_GROUP_STATE_WITHDRAWABLE)) {
    ERR("Group |%s| (@%p) not withdrawable, state %d\n",
        g->name, g, g->state);
    return MSTRO_INVARG;
  }

  mstro_status s=mstro_cdo_withdraw(g->group_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to withdraw group-proxy CDO: %d (%s)\n",
        s, mstro_status_description(s));
  } else {
    g->state = MSTRO_GROUP_STATE_WITHDRAWN;
  }

  /* handles that we auto-offered need to be disposed */
  struct mstro_group_cdo_member *elt, *tmp;
  HASH_ITER(hh, g->members, elt, tmp) {
    assert(mstro_cdo_state_check(elt->entry, MSTRO_CDO_STATE_POOLED));
    if(! elt->user_pooled) {
      WITH_CDO_ID_STR(idstr, &elt->entry->gid,{
          DEBUG("Auto-withdrawing CDO %s (@%p)\n",
                idstr, elt->entry);});
      mstro_cdo_withdraw(elt->entry);
    }
  }
  
  DEBUG("GROUP-WITHDRAW of |%s| (@%p) completed\n",
        g->name, g);
  
  return s;
}

mstro_status
mstro_group_dispose(mstro_group g)
{
  if(g==NULL)
    return MSTRO_INVARG;

  if(! (g->state & MSTRO_GROUP_STATE_DISPOSABLE)) {
    ERR("Group %p (%s) not disposable, state %d\n",
        g, g->name, g->state);
    return MSTRO_INVARG;
  }
  mstro_status s = mstro__group_table_remove(g);
  if(s!=MSTRO_OK) {
    ERR("Failed to remove group from group table\n");
  }

  mstro_cdo_dispose(g->group_cdo);

  /* these must have been cleared at SEAL time */
  if(g->undeclared_cdos) {
    const char *name;
    kh_foreach_key(g->undeclared_cdos, name,
                   { free((char *)name); });
    kh_destroy(group_member_name, g->undeclared_cdos);
  }

  struct mstro_group_cdo_member *elt, *tmp;
  
  HASH_ITER(hh, g->members, elt, tmp) {
    if(!elt->user_owned) {
      mstro_cdo_dispose(elt->entry);
    } /* else: the handle was passed in by the users, so we they will
       * dispose it */
    HASH_DEL(g->members, elt);
    free(elt);
  }

  assert(g->name!=NULL);
  free(g->name);
  if(g->post_demand_members) 
    mstro__pool__group_members__free_unpacked(g->post_demand_members, NULL);
  free(g);

  return s;
}

mstro_status
mstro_group_size_get(mstro_group g, size_t* num_cdos)
{
  if(g==NULL)
    return MSTRO_INVARG;
  if(num_cdos==NULL)
    return MSTRO_INVOUT;

  if(! (g->state & (MSTRO_GROUP_STATE_OFFERED|MSTRO_GROUP_STATE_REQUIRED|MSTRO_GROUP_STATE_DEMANDED))) {
    ERR("Group not in OFFERED, REQUIRED or DEMANDED state\n");
    return MSTRO_INVARG;
  }

  mstro_status s = MSTRO_UNIMPL;

  if(g->state&MSTRO_GROUP_STATE_DEMANDED) {
    /* simple: look into demand results */
    *num_cdos = g->post_demand_members->n_declared_members;
    s=MSTRO_OK;
  } else {
    /* look into CDO attributes */
    WARN("FIXME: unimplemented for non-DEMANDed groups\n");
    s=MSTRO_UNIMPL;
  }
  return s;
}


mstro_status
mstro_group_next(mstro_group g, mstro_cdo* next_cdo_p)
{
  if(g==NULL)
    return MSTRO_INVARG;
  if(next_cdo_p==NULL)
    return MSTRO_INVOUT;

  if(! (g->state&MSTRO_GROUP_STATE_DEMANDED)) {
    ERR("Group not in DEMANDED state\n");
    return MSTRO_INVARG;
  }
  mstro_status s = MSTRO_UNIMPL;

  if(g->member_iter_idx == g->post_demand_members->n_declared_members) {
    *next_cdo_p = NULL; /* indicate end of iteration */
    g->member_iter_idx=0;
    s=MSTRO_OK;
  } else {
    /* mimic the declare/seal/require/demand sequence, except that we
     * can't declare without a name. But we have the ID, which is
     * enough to SEAL. Since we can rely on group-demand to only have
     * succeeded with all members known to the PM we're fine to leave
     * out the normal declare */
    size_t idx = g->member_iter_idx ++;
    struct mstro_cdo_id id = { .qw[0] = g->post_demand_members->declared_members[idx]->qw0,
                               .qw[1] = g->post_demand_members->declared_members[idx]->qw1};
    WITH_CDO_ID_STR(idstr, &id,{
        DEBUG("Will return group member CDO %s (%zu/%zu)\n",
              idstr, idx, g->post_demand_members->n_declared_members);});
    s = mstro_cdo__demand_by_id(&id, next_cdo_p);
  }

  return s;
}



mstro_status
mstro_group_union(mstro_group* out, mstro_group a, mstro_group b)
{
  mstro_group *dummy = out; /* avoid Wunused */
  dummy = &a; /* avoid Wunused */
  dummy = &b; /* avoid Wunused */

  return MSTRO_UNIMPL;
}

mstro_status
mstro_group_inter(mstro_group* out, mstro_group a, mstro_group b)
{
  mstro_group *dummy = out; /* avoid Wunused */
  dummy = &a; /* avoid Wunused */
  dummy = &b; /* avoid Wunused */

  return MSTRO_UNIMPL;
}
