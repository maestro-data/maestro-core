/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool Operations implementation
 **
 **/
/* Copyright © 2023 Hewlett Packard Enterprise Development LP
 * Copyright (C) 2022  Hewlett-Packard Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro/i_pool_operations.h"
#include "i_fifo.h"
#include "maestro/i_globals.h"
#include "maestro/logging.h"
#include "i_maestro_numa.h"
#include <assert.h>



/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PM,__VA_ARGS__)



/** allocate new pool operation */
mstro_status
mstro_pool_op__allocate(mstro_pool_operation *op)
{
    mstro_status status=MSTRO_UNIMPL;
    *op = malloc(sizeof(struct mstro_pool_operation_));
    if (*op == NULL)
    {
        ERR("Can not allocate a new pool operation structure\n");
        status = MSTRO_NOMEM;
    }
    else 
    {
        DEBUG("Allocated a new pool operation structure\n");
        (*op)->kind = MSTRO_OP_INVALID;
        (*op)->step = MSTRO_OP_ST_INVALID;
        (*op)->handler_steps = NULL;
        (*op)->nr_outstanding_acks=0;
        (*op)->msg= NULL;
        (*op)->cdoid.local_id = 0;
        (*op)->cdoid.qw[0] =0;
        (*op)->cdoid.qw[1] =0;
        (*op)->appid = 0;
        (*op)->status = MSTRO_UNIMPL;
        status = MSTRO_OK;
    }
    return status;

}

/** deallocate pool operation */
mstro_status
mstro_pool_op__free(mstro_pool_operation op)
{
    mstro_status status = MSTRO_UNIMPL;
    assert(op != NULL);
    mstro_pm__msg_free(op->msg);
    free(op);
    
    status = MSTRO_OK;
    return status;
}


/** Convenience function to print operation type for debugging */
char *
mstro_pool_op_kind_to_string(enum mstro_pool_operation_kind operation_type)
{
    char *type = NULL;
    switch (operation_type)
    {
        case MSTRO_OP_PM_DECLARE:
            type = "DECLARE";
            break;
        case MSTRO_OP_PM_SEAL:
            type = "SEAL";
            break;
        case MSTRO_OP_PM_OFFER:
            type = "OFFER";
            break;
        case MSTRO_OP_PM_REQUIRE:
            type = "REQUIRE";
            break;
        case MSTRO_OP_PM_RETRACT:
            type = "RETRACT";
            break;
        case MSTRO_OP_PM_DEMAND:
            type = "DEMAND";
            break;
        case MSTRO_OP_PM_WITHDRAW:
            type = "WITHDRAW";
            break;
        case MSTRO_OP_PM_DISPOSE:
            type = "DISPOSE";
            break;
        case MSTRO_OP_PM_LEAVE:
            type = "LEAVE";
            break;
        case MSTRO_OP_PM_JOIN:
            type = "JOIN";
            break;
        case MSTRO_OP_PM_TRANSFER_COMPLETE:
            type = "TRANSFER_COMPLETED";
            break;
        case MSTRO_OP_PM_SUBSCRIBE:
            type = "SUBSCRIBE";
            break;
        case MSTRO_OP_PM_UNSUBSCRIBE:
            type = "UNSUBSCRIBE";
            break;
        case MSTRO_OP_PM_EVENT_ACK:
            type = "EVENT_ACK";
            break;
        case MSTRO_OP_PM_MSG_RESOLVE:
            type = "MSG_RESOLVE";
            break;
        case MSTRO_OP_PC_INIT_TRANSFER:
            type = "INIT TRANSFER";
            break;
        case MSTRO_OP_PC_TRANSFER:
            type = "TRANSFER TICKET";
            break;
        case MSTRO_OP_PC_DECLARE_ACK:
            type = "DECLARE ACK";
            break;
        case MSTRO_OP_PC_POOL_ACK:
            type = "POOL OP ACK";
            break;                 
        case MSTRO_OP_PC_SUBSCRIBE_ACK:
            type = "SUBSCRIBE ACK";
            break;           
        case MSTRO_OP_PC_BYE:
            type = "BYE";
            break;                      
        case MSTRO_OP_PC_EVENT:
            type = "EVENT";
            break;                    
        case MSTRO_OP_PC_RESOLVE_REPLY:
            type = "RESOLVE REPLY";
            break;             
        case MSTRO_OP_PC_TRANSFER_COMPLETED:
            type = "TRANSFER COMPLETED";
            break; 
        default:
            ERR("Unknown operation type\n");
            type = "UNKNOWN";
            break;
    }
    return type;
}


static inline
void
mstro_pool_op__print_state(mstro_pool_operation op, enum mstro_pool_operation_state state)
{
    char *cdo_id_str = alloca(MSTRO_CDO_ID_STR_LEN);
    char op_step_str[24];
    char *cdo_name = NULL;
    snprintf(op_step_str, 20, " step %d of", op->step);

    switch (op->kind) {
      /** PM CDO operations */
      case MSTRO_OP_PM_DECLARE:
        cdo_name = op->msg->declare->cdo_name;
	/* fallthrough */
      case MSTRO_OP_PM_SEAL:
      case MSTRO_OP_PM_OFFER:
      case MSTRO_OP_PM_REQUIRE:
      case MSTRO_OP_PM_RETRACT:
      case MSTRO_OP_PM_DEMAND:
      case MSTRO_OP_PM_WITHDRAW:
      case MSTRO_OP_PM_DISPOSE:
      case MSTRO_OP_PM_TRANSFER_COMPLETE:
        mstro_cdo_id__str(&(op->cdoid), &cdo_id_str);
        if (state == MSTRO_OP_STATE_FAILED) {
          ERR("Failed to execute step %d of operation %s on CDO %s (from app %" PRIappid ")\n",
              op->step,
              mstro_pool_op_kind_to_string(op->kind),
              (cdo_name != NULL)                    ? cdo_name  : cdo_id_str,
              op->appid);
        } else {
          DEBUG("%s%s operation %s on CDO %s (from app %"PRIappid")\n",
                (state == MSTRO_OP_STATE_BLOCKED)   ? "Blocked" : "Completed",
                (state == MSTRO_OP_STATE_COMPLETED) ? ""        : op_step_str,
                mstro_pool_op_kind_to_string(op->kind),
                (cdo_name != NULL)                  ? cdo_name  : cdo_id_str,
                op->appid);
        }
        break;
        /** PM other msg processing */
      case MSTRO_OP_PM_LEAVE:
	/* fallthrough */
      case MSTRO_OP_PM_JOIN:
      case MSTRO_OP_PM_SUBSCRIBE:
      case MSTRO_OP_PM_UNSUBSCRIBE:
      case MSTRO_OP_PM_EVENT_ACK:
      case MSTRO_OP_PM_MSG_RESOLVE:
        if (state == MSTRO_OP_STATE_FAILED) {
          ERR("Failed to execute step %d of operation %s (from app %"PRIappid")\n",
              op->step,
              mstro_pool_op_kind_to_string(op->kind),
              op->appid);
        } else {
          DEBUG("%s%s operation %s (from app %" PRIappid ")\n",
                (state == MSTRO_OP_STATE_BLOCKED)   ? "Blocked" : "Completed",
                (state == MSTRO_OP_STATE_COMPLETED) ? ""        : op_step_str,
                mstro_pool_op_kind_to_string(op->kind),
                op->appid);
        }
        break;
        /*----------------------------------PC Operations---------------------------------------------*/
      case MSTRO_OP_PC_INIT_TRANSFER:
	/* fallthrough */
      case MSTRO_OP_PC_TRANSFER:
      case MSTRO_OP_PC_DECLARE_ACK:
      case MSTRO_OP_PC_POOL_ACK:
      case MSTRO_OP_PC_SUBSCRIBE_ACK:
      case MSTRO_OP_PC_BYE:
      case MSTRO_OP_PC_EVENT:
      case MSTRO_OP_PC_RESOLVE_REPLY:
      case MSTRO_OP_PC_TRANSFER_COMPLETED:
        if (state == MSTRO_OP_STATE_FAILED) {
          ERR("Failed to execute step %d of operation %s\n",
              op->step,
              mstro_pool_op_kind_to_string(op->kind));
        } else {
          DEBUG("%s%s operation %s\n",
                (state == MSTRO_OP_STATE_BLOCKED)   ? "Blocked" : "Completed",
                (state == MSTRO_OP_STATE_COMPLETED) ? ""        : op_step_str,
                mstro_pool_op_kind_to_string(op->kind));
        }
        break;
      default:
        ERR("Unknown operation %d\n", op->kind);
        break;
    }
}


/* Function to execute an operation */
mstro_status
mstro_pool_op_engine(void *operation)
{
    mstro_status status = MSTRO_UNIMPL;
    mstro_pool_operation op = (mstro_pool_operation) operation;
    /**Check input */
    if(op != NULL)
    {
        status = MSTRO_OK;
    }
    else
    {
        status = MSTRO_FAIL;
        ERR("Invalid operation (NULL)\n");
    }
    
    while(status == MSTRO_OK)
    {
        /* execute operation step */
        if (op->handler_steps[op->step] == NULL)
        {
            NOISE("We hit a no-op ... continue\n");
        }
        else
        {
            status = op->handler_steps[op->step](op);
        }

        /**check status*/
        if(status == MSTRO_WOULDBLOCK)
        {
            //Propagate back WOULDBLOCK status to put the op back in the queue;
            mstro_pool_op__print_state(op, MSTRO_OP_STATE_BLOCKED);
            break;
        }
        else if(status == MSTRO_OK)
        {
            if (op->step >= (MSTRO_OP_ST_MAX-1))
            {
                mstro_pool_op__print_state(op, MSTRO_OP_STATE_COMPLETED);
                /**clean up */
                status = mstro_pool_op__free(op);
                break;
            }
            else
            {
                mstro_pool_op__print_state(op, MSTRO_OP_STATE_STEP_COMPLETED);
            }
            op->step++; // make progress
        }
        else // probably status = MSTRO_FAIL or other error codes, we can not continue
        {
            mstro_pool_op__print_state(op, MSTRO_OP_STATE_FAILED);
            /**clean up */
            status = mstro_pool_op__free(op);
            break;
        }
    }
    return status;
}
