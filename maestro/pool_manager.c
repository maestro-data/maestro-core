/* -*- mode:c -*- */
/** @file
 ** @brief Pool Manager implementation 
 **
 **/
/*
 * Copyright (C) 2019-2020 Cray Computer GmbH
 * Copyright (C) 2021-     Hewlett Packard Development LP
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "i_fifo.h"
#include "maestro/i_globals.h"
#include "maestro/logging.h"
#include "i_subscription_registry.h"
#include "i_maestro_numa.h"
#include "maestro/i_statistics.h"
#include "maestro/i_pool_manager.h"
#include "maestro/i_ofi.h"
#include "i_thread_team.h"


#include <assert.h>
#include <inttypes.h>
#include "maestro/i_hashing.h"
#include "maestro/i_pool_operations.h"


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PM,__VA_ARGS__)

/**
 * Function stacks to handle various pm operations 
 * steps are coming from @ref mstro_pool_operation_step in i_pool_operations.h 
 * steps are 
 * MSTRO_OP_ST_INVALID = 0,           invalid pool operation kind
 * MSTRO_OP_ST_ANNOUNCE,              announce before operation  
 * MSTRO_OP_ST_CHECK_ACK,             check acks before          
 * MSTRO_OP_ST_EXE,                   execute operation         
 * MSTRO_OP_ST_AFTR,                  announce after operation   
 * MSTRO_OP_ST_CHECK_AFTR_ACK,        check acks after          
 * MSTRO_OP_ST_SEND_POOLOP_ACK,       PM ack operation completion
 * MSTRO_OP_ST_UPDATE_STATS,          Update stats
 * MSTRO_OP_ST_MAX   
 */

/**function stack to handle pm dispose cdo msg*/
const mstro_pool_op_st_handler mstro_pm__dispose_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_cdo_registry_dispose,          /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      NULL,                                   /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };

/**function stack to handle pm declare cdo msg*/
const mstro_pool_op_st_handler mstro_pm__declare_steps[] = {
                                                      NULL,                                    
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_cdo_registry_declare_op,       /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm_cdo_registry_send_declare_ack, /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };


/**function stack to handle pm seal cdo msg*/
const mstro_pool_op_st_handler mstro_pm__seal_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm__cdo_seal,                     /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_ack_op,                  /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };

/**function stack to handle pm offer cdo msg*/
const mstro_pool_op_st_handler mstro_pm__offer_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_cdo_registry_update_state_op,  /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_ack_op,                  /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };                                                      

/**function stack to handle pm require cdo msg*/
const mstro_pool_op_st_handler mstro_pm__require_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_cdo_registry_update_state_op,  /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_ack_op,                  /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };

/**function stack to handle pm retract cdo msg*/
const mstro_pool_op_st_handler mstro_pm__retract_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_cdo_registry_update_state_op,  /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_ack_op,                  /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };

 /**function stack to handle pm demand cdo msg*/
const mstro_pool_op_st_handler mstro_pm__demand_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm__cdo_demand,                   /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      NULL,                                   /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };

/**function stack to handle pm withdraw cdo msg*/
const mstro_pool_op_st_handler mstro_pm__withdraw_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_cdo_registry_withdraw,         /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_ack_op,                  /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };

/**function stack to handle pm transfer completed msg*/
const mstro_pool_op_st_handler mstro_pm__transfer_completed_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,           /**<announce before operation  */
                                                      mstro_pm__check_acks,                    /**<check acks before          */
                                                      mstro_pm_cdo_registry_transfer_completed,/**<execute operation          */
                                                      mstro_pm__event_notify_after,            /**<announce after operation   */
                                                      mstro_pm__check_acks,                    /**<check acks after           */
                                                      NULL,                                    /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                   /**Update stats                */
                                                      };

/**function stack to handle app leave msg*/
const mstro_pool_op_st_handler mstro_pm__leave_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm_app_deregister,                /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_bye,                     /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };   

/**function stack to handle app leave msg*/
const mstro_pool_op_st_handler mstro_pm__join_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm__app_join,                     /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_welcome,                 /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };  

/**function stack to handle app subscribe msg*/
const mstro_pool_op_st_handler mstro_pm__subscribe_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm__subscribe,                    /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_subscribe_ack,           /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                  /**Update stats                */
                                                      };    

/**function stack to handle unsubscribe msg*/
const mstro_pool_op_st_handler mstro_pm__unsubscribe_steps[] = {
                                                      NULL,
                                                      mstro_pm__event_notify_before,          /**<announce before operation  */
                                                      mstro_pm__check_acks,                   /**<check acks before          */
                                                      mstro_pm__unsubscribe,                  /**<execute operation          */
                                                      mstro_pm__event_notify_after,           /**<announce after operation   */
                                                      mstro_pm__check_acks,                   /**<check acks after           */
                                                      mstro_pm__send_ack_op,                  /**<PM ack operation completion*/
                                                      NULL                                    /**Update stats                */
                                                      }; 

/**function stack to handle event ack msg*/
const mstro_pool_op_st_handler mstro_pm__ack_steps[] = {
                                                      NULL,
                                                      NULL,                                    /**<announce before operation  */
                                                      NULL,                                    /**<check acks before          */
                                                      mstro_pm__event_ack,                     /**<execute operation          */
                                                      NULL,                                    /**<announce after operation   */
                                                      NULL,                                    /**<check acks after           */
                                                      NULL,                                    /**<PM ack operation completion*/
                                                      NULL                                     /**Update stats                */
                                                      }; 
                                                                                                                                                          
/**function stack to handle msg resolve msg*/
const mstro_pool_op_st_handler mstro_pm__resolve_steps[] = {
                                                      NULL,
                                                      NULL,                                   /**<announce before operation  */
                                                      NULL,                                   /**<check acks before          */
                                                      mstro_pm__msg_resolve,                  /**<execute operation          */
                                                      NULL,                                   /**<announce after operation   */
                                                      NULL,                                   /**<check acks after           */
                                                      NULL,                                   /**<PM ack operation completion*/
                                                      mstro_pm__update_stats                   /**Update stats                */
                                                      }; 


static inline
mstro_status
mstro_pm__fill_pool_event(mstro_pool_operation op, Mstro__Pool__Event *ev)
{
    mstro_status status = MSTRO_UNIMPL;
    
    ev->origin_id = op->msg->token->appid;
    switch (op->kind)
    {
        case MSTRO_OP_PM_DECLARE:
            ev->kind = MSTRO__POOL__EVENT_KIND__DECLARE;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_DECLARE;
            ev->declare = op->msg->declare;
            ev->cdo_name = op->msg->declare->cdo_name;
            status = MSTRO_OK;
            break;
        case MSTRO_OP_PM_SEAL:
            ev->kind = MSTRO__POOL__EVENT_KIND__SEAL;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_SEAL;
            ev->seal = op->msg->seal;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_OFFER:
            ev->kind = MSTRO__POOL__EVENT_KIND__OFFER;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_OFFER;
            ev->offer = op->msg->offer;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_REQUIRE:
            ev->kind = MSTRO__POOL__EVENT_KIND__REQUIRE;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_REQUIRE;
            ev->require = op->msg->require;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_RETRACT:
            ev->kind = MSTRO__POOL__EVENT_KIND__RETRACT;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_RETRACT;
            ev->retract = op->msg->retract;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_DEMAND:
            ev->kind = MSTRO__POOL__EVENT_KIND__DEMAND;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_DEMAND;
            ev->demand = op->msg->demand;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_WITHDRAW:
            ev->kind = MSTRO__POOL__EVENT_KIND__WITHDRAW;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_WITHDRAW;
            ev->withdraw = op->msg->withdraw;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_DISPOSE:
            ev->kind = MSTRO__POOL__EVENT_KIND__DISPOSE;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_DISPOSE;
            ev->dispose = op->msg->dispose;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_TRANSFER_COMPLETE:
            ev->kind = MSTRO__POOL__EVENT_KIND__TRANSFER_COMPLETED;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_TRANSFER_COMPLETED;
            ev->transfer_completed = op->msg->transfer_completed;
            status = mstro_pm__possibly_fill_event_cdoname(&(op->cdoid), op->appid, ev);
            if(status!=MSTRO_OK) {
                status = MSTRO_FAIL;
            }
            else {
                status = MSTRO_OK;
            }
            break;
        case MSTRO_OP_PM_LEAVE:
            ev->kind = MSTRO__POOL__EVENT_KIND__APP_LEAVE;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_LEAVE;
            ev->leave = op->msg->leave;
            status = MSTRO_OK;
            break;
        case MSTRO_OP_PM_JOIN:
            ev->kind = MSTRO__POOL__EVENT_KIND__APP_JOIN;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_JOIN;
            ev->join = op->msg->join;
            ev->origin_id = NULL; // we will put in the correct appid in after events in mstro_pool_op__event_notify
            status = MSTRO_OK;
            break;
        case MSTRO_OP_PM_SUBSCRIBE: 
            ev->kind = MSTRO__POOL__EVENT_KIND__SUBSCRIBE;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_SUBSCRIBE;
            ev->subscribe = op->msg->subscribe;
            status = MSTRO_OK;
            break;
        case MSTRO_OP_PM_UNSUBSCRIBE:
            ev->kind = MSTRO__POOL__EVENT_KIND__UNSUBSCRIBE;
            ev->payload_case = MSTRO__POOL__EVENT__PAYLOAD_UNSUBSCRIBE;
            ev->unsubscribe = op->msg->unsubscribe;
            status = MSTRO_OK;
            break;
        /**FIXME add more operations */
        default:
            ERR("Undefined operation \n");
            status = MSTRO_FAIL;
            break;
    }

    return status;
}

static inline
mstro_status
mstro_pm__event_notify(mstro_pool_operation op, bool before)
{
    mstro_status status = MSTRO_UNIMPL;
    /** create an event msg*/
    Mstro__Pool__Event pool_event_msg = MSTRO__POOL__EVENT__INIT;
    Mstro__Pool__Appid aid = MSTRO__POOL__APPID__INIT;

    status = mstro_pm__fill_pool_event(op, &pool_event_msg);
    if(status != MSTRO_OK) 
    {
        ERR("Failed to fill pool event from pool operation structure\n");
        return MSTRO_FAIL;
    }

    /**Add time stamp */
    Mstro__Pool__Timestamp ctime = MSTRO__POOL__TIMESTAMP__INIT;
    ctime.offset = 0;
    mstro_nanosec_t tick = mstro_clock();
    ctime.sec = tick/NSEC_PER_SEC;
    ctime.nsec = tick-ctime.sec;
    pool_event_msg.ctime = &ctime;

    if ((op->kind == MSTRO_OP_PM_JOIN) && (!before))
    {
        aid.id = op->appid;
        pool_event_msg.origin_id = &aid;
    }
    

    DEBUG("Advertising event kind %d (%s:%s)\n",
        pool_event_msg.kind,
        protobuf_c_enum_descriptor_get_value(&mstro__pool__event_kind__descriptor,
                                             pool_event_msg.kind)->name, before ? "before" : "after" );

    status = mstro_pool_event_advertise(&pool_event_msg, before, &(op->nr_outstanding_acks));
    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_POOL_EVENTS, 1);
                                                                 
    if(status!=MSTRO_OK) {
    ERR("Failed to advertise event: %d (%s)\n",
        status, mstro_status_description(status));
    }

    return status;

}

/**Create mstro_pool_operations from incoming msgs
 * pushes the created operations to the *queue*
*/
mstro_status
mstro_pm__op_maker(Mstro__Pool__MstroMsg *msg, const struct mstro_endpoint *ep)
{
    mstro_status status=MSTRO_OK;
    mstro_pool_operation op;

    /*Create an operation*/
    status = mstro_pool_op__allocate(&op);
    if (status != MSTRO_OK)
    {
        mstro_pm__msg_free(msg);
        return status;
    }

    /**handle msg */
    switch(msg->msg_case) {
    /* 'good' messages: */
    case MSTRO__POOL__MSTRO_MSG__MSG_DECLARE:
        /*fill declare specific parts */
        op->kind = MSTRO_OP_PM_DECLARE;
        op->handler_steps = mstro_pm__declare_steps;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_SEAL:
        /**fill seal parts*/
        op->kind = MSTRO_OP_PM_SEAL;
        op->handler_steps = mstro_pm__seal_steps;
        op->cdoid.qw[0] = msg->seal->cdoid->qw0; 
        op->cdoid.qw[1] = msg->seal->cdoid->qw1;
        op->cdoid.local_id = msg->seal->cdoid->local_id;
        op->send_attribute_update = false;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_OFFER:
        /**fill offer parts*/
        op->kind = MSTRO_OP_PM_OFFER;
        op->handler_steps = mstro_pm__offer_steps;
        op->cdoid.qw[0] = msg->offer->cdoid->qw0; 
        op->cdoid.qw[1] = msg->offer->cdoid->qw1;
        op->cdoid.local_id = msg->offer->cdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_REQUIRE:
        /**fill require parts*/
        op->kind = MSTRO_OP_PM_REQUIRE;
        op->handler_steps = mstro_pm__require_steps;
        op->cdoid.qw[0] = msg->require->cdoid->qw0; 
        op->cdoid.qw[1] = msg->require->cdoid->qw1;
        op->cdoid.local_id = msg->require->cdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_RETRACT:
        /**fill retract parts*/
        op->kind = MSTRO_OP_PM_RETRACT;
        op->handler_steps = mstro_pm__retract_steps;
        op->cdoid.qw[0] = msg->retract->cdoid->qw0; 
        op->cdoid.qw[1] = msg->retract->cdoid->qw1;
        op->cdoid.local_id = msg->retract->cdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND:
        /**fill demand parts*/
        op->kind = MSTRO_OP_PM_DEMAND;
        op->handler_steps = mstro_pm__demand_steps;
        op->cdoid.qw[0] = msg->demand->cdoid->qw0; 
        op->cdoid.qw[1] = msg->demand->cdoid->qw1;
        op->cdoid.local_id = msg->demand->cdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_WITHDRAW:
        /**fill withdraw parts*/
        op->kind = MSTRO_OP_PM_WITHDRAW;
        op->handler_steps = mstro_pm__withdraw_steps;
        op->cdoid.qw[0] = msg->withdraw->cdoid->qw0; 
        op->cdoid.qw[1] = msg->withdraw->cdoid->qw1;
        op->cdoid.local_id = msg->withdraw->cdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_DISPOSE:
        /*fill dispose specific parts */
        op->kind = MSTRO_OP_PM_DISPOSE;
        op->handler_steps = mstro_pm__dispose_steps; // cdo pm dispose stack
        op->cdoid.qw[0] = msg->dispose->cdoid->qw0; 
        op->cdoid.qw[1] = msg->dispose->cdoid->qw1;
        op->cdoid.local_id = msg->dispose->cdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_COMPLETED:
        /*fill transfer complete specific parts */
        op->kind = MSTRO_OP_PM_TRANSFER_COMPLETE;
        op->handler_steps = mstro_pm__transfer_completed_steps; 
        op->cdoid.qw[0] = msg->transfer_completed->dstcdoid->qw0; /*the operation concerns the dst cdo*/
        op->cdoid.qw[1] = msg->transfer_completed->dstcdoid->qw1;
        op->cdoid.local_id = msg->transfer_completed->dstcdoid->local_id;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_LEAVE:
        op->kind = MSTRO_OP_PM_LEAVE;
        op->handler_steps = mstro_pm__leave_steps;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_JOIN:
        op->kind = MSTRO_OP_PM_JOIN;
        op->handler_steps = mstro_pm__join_steps;
        op->join.ep = ep;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE:
        op->kind = MSTRO_OP_PM_SUBSCRIBE;
        op->handler_steps = mstro_pm__subscribe_steps;
        op->subscription_handle = NULL;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_UNSUBSCRIBE:
        op->kind = MSTRO_OP_PM_UNSUBSCRIBE;
        op->handler_steps = mstro_pm__unsubscribe_steps;
        op->subscription_handle = msg->unsubscribe->subscription_handle;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_EVENT_ACK:
        op->kind = MSTRO_OP_PM_EVENT_ACK;
        op->handler_steps = mstro_pm__ack_steps;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE:
        op->kind = MSTRO_OP_PM_MSG_RESOLVE;
        op->handler_steps = mstro_pm__resolve_steps;
        break;
    default:
        WARN("%s message received, dropping it, not even sending ACK\n",
              msg->base.descriptor->name);
        /*cleanup*/
        mstro_pool_op__free(op);
        mstro_pm__msg_free(msg);
        status = MSTRO_UNIMPL;
        break;
    }
    /**get our numa node*/
    int numa_node = 0;
    #ifdef HAVE_NUMA
    /**update numa node*/
    numa_node = numa_node_of_cpu(mstro_numa_get_cpu());
    #endif
    if (status == MSTRO_OK)
    {
        /* continue to fill general operation fields from msg and push to queue */
        op->step = MSTRO_OP_ST_ANNOUNCE; // first step is to announce
        op->msg = msg;
        if (op->kind != MSTRO_OP_PM_JOIN)
        {
            op->appid = msg->token->appid->id;
        }
        DEBUG("Filled operation structure from msg\n");
        status = erl_thread_team_enqueue_on_numa(g_pool_operations_team, numa_node, op);
        assert(status == MSTRO_OK);
    }
    
    return status;
}


mstro_status 
mstro_pm__check_acks(mstro_pool_operation op)
{
    mstro_status status = MSTRO_UNIMPL;

    uint64_t nr_left = atomic_load_explicit(&(op->nr_outstanding_acks), memory_order_acquire);
    if(nr_left==0) {
        DEBUG("No outstanding acks\n");
        status = MSTRO_OK;
    }
    else
    {
        DEBUG("Remaining %" PRIu64" acks\n", nr_left);
        status = MSTRO_WOULDBLOCK;
    }
    return status;
}


mstro_status
mstro_pm__event_notify_before(mstro_pool_operation op)
{
    mstro_status status = MSTRO_UNIMPL;
    status = mstro_pm__event_notify(op, true);
    return status;
}

mstro_status
mstro_pm__event_notify_after(mstro_pool_operation op)
{
    mstro_status status = MSTRO_UNIMPL;
    status = mstro_pm__event_notify(op, false);
    return status;
}



/** Group registry */

/** A group record, containing the list of members for a group.

    This is created at SEAL time when the group is mentioned by some
    app for the first time.
*/
struct mstro_pm__group_record {
  struct mstro_cdo_id id;          /**< the proxy CDO ID */
  _Atomic(size_t) num_refs;        /**< number of live group handles for this record */
  size_t num_cdos;                 /**< the number of members */
  struct mstro_cdo_id *member_ids; /**< array of ::NUM_CDOS many CDO
                                    * IDs. FIXME: add hash for this */
  char **member_names;             /**< array of ::NUM_CDOS many CDO
                                      IDs.  FIXME: replace by long
                                      array of NUL-terminated strings
                                      and a hash to do look-ups */
};

/** The group registry is a hash table from proxy CDO-ID to group_record */
KHASH_INIT(group_reg, const struct mstro_cdo_id*,
           struct mstro_pm__group_record *, 1,
           kh_cdoidptr_hash_func, kh_cdoidptr_hash_equal)

    /** the global group registry. Protected by @ref g_pm_group_registry_mtx */
    khash_t(group_reg) g_pm_group_registry; /* we rely on kh_init just
                                             * doing the equivalent of
                                             * calloc, so this is
                                             * fine */
/** mutex protecting @ref g_pm_group_registry */
pthread_mutex_t g_pm_group_registry_mtx = PTHREAD_MUTEX_INITIALIZER;

#define WITH_LOCKED_GROUP_REGISTRY(body) \
  do {                                                                  \
    int wlgr_stat=pthread_mutex_lock(&g_pm_group_registry_mtx);         \
    if(wlgr_stat!=0) {                                                  \
      ERR("Failed to lock group registry: %d (%s)\n",                   \
          wlgr_stat, strerror(wlgr_stat));                              \
      abort();                                                          \
    }                                                                   \
                                                                        \
    do {                                                                \
      body;                                                             \
    } while(0);                                                         \
                                                                        \
    wlgr_stat=pthread_mutex_unlock(&g_pm_group_registry_mtx);           \
    if(wlgr_stat!=0) {                                                  \
      ERR("Failed to unlock group registry: %d (%s)\n",                 \
          wlgr_stat, strerror(wlgr_stat));                              \
      abort();                                                          \
    }                                                                   \
  } while(0)

/** fill in the PoolOpAck structure according to OP. */
static inline
mstro_status
mstro_pmp_fill_pool_op_ack(Mstro__Pool__PoolOpAck *poack,
                           Mstro__Pool__PoolOpAck__PoolOp op,
                           Mstro__Pool__CDOID *cdoid,
                           Mstro__Pool__SubscriptionHandle *h,
                           mstro_status status)
{
  assert(poack!=NULL
         && poack->base.descriptor == &mstro__pool__pool_op_ack__descriptor);
  assert(cdoid!=NULL || h!=NULL);

  poack->op = op;
  switch(op) {
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__SEAL:
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__OFFER:
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__REQUIRE:
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__RETRACT:
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__WITHDRAW:
      poack->operand_case=MSTRO__POOL__POOL_OP_ACK__OPERAND_CDOID;
      poack->cdoid = cdoid;
      break;
      
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__UNSUBSCRIBE:
      poack->operand_case=MSTRO__POOL__POOL_OP_ACK__OPERAND_HANDLE;
      poack->handle = h;
      break;

    default:
      ERR("unsupported PoolOp\n");
      return MSTRO_FAIL;
  }
  if(status==MSTRO_OK)
    poack->status = MSTRO__POOL__POOL_OP_ACK__POOL_OP_STATUS__OK;
  else
    poack->status = MSTRO__POOL__POOL_OP_ACK__POOL_OP_STATUS__FAIL;

  return MSTRO_OK;
}

static inline
mstro_status
mstro_pm__send_ack(mstro_app_id app_id,
                   Mstro__Pool__PoolOpAck__PoolOp op,
                   Mstro__Pool__CDOID *cdoid,
                   Mstro__Pool__SubscriptionHandle *h,
                   Mstro__Pool__Attributes *a,
                   mstro_status s)
{
  Mstro__Pool__PoolOpAck poack = MSTRO__POOL__POOL_OP_ACK__INIT;
  mstro_status status = mstro_pmp_fill_pool_op_ack(&poack, op, cdoid, h, s);
  if(a!=NULL) {
    DEBUG("Including attribute update in PoolOpAck (likely a GROUP SEAL)\n");
    poack.payload_case = MSTRO__POOL__POOL_OP_ACK__PAYLOAD_UPDATED_ATTRIBUTES;
    poack.updated_attributes = a;
  }

  if(status!=MSTRO_OK) {
    const char *op_kind
        = (protobuf_c_enum_descriptor_get_value(
            &mstro__pool__pool_op_ack__pool_op__descriptor, op))
        ->name;
    ERR("Failed to construct ACK for %s\n", op_kind);
    return status;
  }
  
  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&poack);
  if(status!=MSTRO_OK) {
    ERR("Failed to package %s-ACK into a pool manager message\n",
        poack.base.descriptor->name);
    return status;
  }
  /* send it off: this function automatically packs the message and sends it. The   */
  status = mstro_pmp_send_nowait(app_id, &msg);
  if(status!=MSTRO_OK) {
    ERR("Failed to send %s-ACK to app %" PRIappid "\n", app_id);
  }
  
  return status; 
}

mstro_status
mstro_pm__update_stats(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  switch (op->kind)
  {
  case MSTRO_OP_PM_DISPOSE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_DISPOSE, 1);
    break;
  case MSTRO_OP_PM_DECLARE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_DECLARE, 1);
    break;
  case MSTRO_OP_PM_SEAL:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_SEAL, 1);
    break;
  case MSTRO_OP_PM_OFFER:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_OFFER, 1);
    break;
  case MSTRO_OP_PM_REQUIRE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_REQUIRE, 1);
    break;
  case MSTRO_OP_PM_RETRACT:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_RETRACT, 1);
    break;
  case MSTRO_OP_PM_DEMAND:
    status =  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_DEMAND, 1);
    break;
  case MSTRO_OP_PM_WITHDRAW:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_POOL, MSTRO_STATS_L_PM_NUM_IMM_WITHDRAWS, 1);
    break;
  case MSTRO_OP_PM_TRANSFER_COMPLETE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_TRANSFER_COMPLETIONS, 1);
    break;
  case MSTRO_OP_PM_LEAVE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_LEAVE, 1);
    break;
  case MSTRO_OP_PM_JOIN:
    status  = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_JOIN, 1);
    break;
  case MSTRO_OP_PM_SUBSCRIBE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_SUBSCRIBE, 1);
    break;
  case MSTRO_OP_PM_MSG_RESOLVE:
    status = mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_RESOLVE, 1);
    break;
  default:
    ERR("Unknown operation kind \n");
    status = MSTRO_FAIL;
    break;
  }
  return status;
}


mstro_status
mstro_pm__send_ack_op(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  switch (op->kind)
  {
  case MSTRO_OP_PM_SEAL:
    status = mstro_pm__send_ack(op->appid,
                                MSTRO__POOL__POOL_OP_ACK__POOL_OP__SEAL,
                                op->msg->seal->cdoid, NULL, op->send_attribute_update?op->msg->seal->attributes:NULL, op->status);
    /*  we consumed the attributes allocation (in the registry), so must
      * ensure freeing the message will not kill it. As long as we can
      * rely on protobuf unpacking into separate allocation we can simply
      * NULL it and things will work. */
    op->msg->seal->attributes = NULL;
  break;
  case MSTRO_OP_PM_OFFER:
    status = mstro_pm__send_ack(op->appid,
                                MSTRO__POOL__POOL_OP_ACK__POOL_OP__OFFER,
                                op->msg->offer->cdoid, NULL, NULL, op->status);
    break;
  case MSTRO_OP_PM_REQUIRE:
    status = mstro_pm__send_ack(op->appid,
                                MSTRO__POOL__POOL_OP_ACK__POOL_OP__REQUIRE,
                                op->msg->require->cdoid, NULL, NULL, op->status);
    break;
  case MSTRO_OP_PM_RETRACT:
    status = mstro_pm__send_ack(op->appid,
                                MSTRO__POOL__POOL_OP_ACK__POOL_OP__RETRACT,
                                op->msg->retract->cdoid, NULL, NULL, op->status);
    break;
  case MSTRO_OP_PM_WITHDRAW:
    status = mstro_pm__send_ack(op->appid,
                                MSTRO__POOL__POOL_OP_ACK__POOL_OP__WITHDRAW,
                                op->msg->withdraw->cdoid, NULL, NULL, op->status);
    break;
  case MSTRO_OP_PM_UNSUBSCRIBE:
    status = mstro_pm__send_ack(op->appid,
                                MSTRO__POOL__POOL_OP_ACK__POOL_OP__UNSUBSCRIBE,
                                NULL, op->subscription_handle, NULL, op->status);  
    break;
  default:
      ERR("Invalid operation\n");
      return MSTRO_INVARG;
  }
  if(status!=MSTRO_OK)
  {
    ERR("Failed to send %s-ACK: %d (%s)\n",
        mstro_pool_op_kind_to_string(op->kind), status, mstro_status_description(status));
  }
  return status;
}

mstro_status
mstro_pm__send_subscribe_ack(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;

  Mstro__Pool__SubscribeAck sack = MSTRO__POOL__SUBSCRIBE_ACK__INIT;
  sack.serial = op->msg->subscribe->local_id;
  sack.handle = op->subscription_handle;

  Mstro__Pool__MstroMsg msg_sack = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg_sack, (ProtobufCMessage*)&sack);
  if(status!=MSTRO_OK) {
    ERR("Failed to package %s-ACK into a pool manager message\n",
        sack.base.descriptor->name);
    return status;
  }
  
  status = mstro_pmp_send_nowait(op->appid, &msg_sack);
  if(status !=MSTRO_OK) {
    ERR("Failed to send %s-ACK to app %" PRIappid "\n",
        sack.base.descriptor->name, op->appid);
    return status;
  }


  return status;
}

void
mstro_pm__msg_free(Mstro__Pool__MstroMsg *msg)
{
  if(msg!=NULL) {
    assert(msg->token!=&g_pool_apptoken);
    mstro__pool__mstro_msg__free_unpacked(msg, NULL);
  }
}


/** if the application receiving EV needs to be told the name, send
 * it, otherwise set it to NULL in the EV structure */
mstro_status
mstro_pm__possibly_fill_event_cdoname(
    const struct mstro_cdo_id *cdoid,
    mstro_app_id recipient,
    Mstro__Pool__Event *ev)
{
  recipient = recipient; /* avoid unused warning */
  
  /** FIXME: for the moment we just always send the name, could be improved */
  mstro_status s=
      mstro_pm_cdo_registry_cdo_name_lookup(cdoid,
                                            (const char **)&ev->cdo_name);
  if(s!=MSTRO_OK) {
    WITH_CDO_ID_STR(idstr, cdoid, {
        ERR("Cannot find CDO registry entry for |%s|\n",
            idstr);});
    return MSTRO_NOENT;
  }
  return MSTRO_OK;  
}

/*** DECLARE ***/
/** DECLARE is handled by pool operations */

/*** GROUP SEAL ***/

/* Create a new group record for CDOID with MEMBERS on APP_ID.
   Undeclared CDO members are declared on the PM CDO registry and
   moved from the undeclared to the declared list of MEMBERS
   destructively.
*/
static inline
mstro_status
mstro_pm__group_record_create(const struct mstro_cdo_id *cdoid,
                              mstro_app_id app_id,
                              Mstro__Pool__GroupMembers *members,
                              struct mstro_pm__group_record **result)
{
  if(cdoid==NULL || members==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_NOMEM;
  mstro_status s=MSTRO_UNIMPL;
  struct mstro_pm__group_record *entry =NULL;

  if(members->sel!=NULL) {
    ERR("FIXME: No support for CDO selectors in groups\n");
    s=MSTRO_UNIMPL;
    goto BAILOUT;
  }
  
  /* build group-registry entry */
  entry = calloc(1,sizeof(struct mstro_pm__group_record));
  if(!entry) {
    s=MSTRO_NOMEM; goto BAILOUT;
  }

  memcpy(&entry->id, cdoid, sizeof(struct mstro_cdo_id));
  entry->num_refs = 1;
  entry->num_cdos = members->n_declared_members + members->n_undeclared_members;

  entry->member_ids   = malloc(sizeof(struct mstro_cdo_id) * entry->num_cdos);
  entry->member_names = calloc(entry->num_cdos, sizeof(char*));
  if(entry->member_ids==NULL || entry->member_names==NULL) {
    s=MSTRO_NOMEM;
    goto BAILOUT;
  }

  for(size_t i=0; i<members->n_declared_members; i++) {
    struct mstro_cdo_id c = { .qw[0] = members->declared_members[i]->qw0,
                              .qw[1] = members->declared_members[i]->qw1,
                              .local_id = members->declared_members[i]->local_id };
    memcpy(&entry->member_ids[i], &c, sizeof(struct mstro_cdo_id));
    /* lookup and store name also */
  }

  /* add handles to per-group CDO handle table */
  s=MSTRO_OK;
  size_t offset = members->n_declared_members;
  for(size_t i=0; i<members->n_undeclared_members; i++) {
    char *name = members->undeclared_members[i];
    struct mstro_cdo_id c;
    s = mstro_pm_cdo_registry_declare(app_id, name, MSTRO_CDO_LOCAL_ID_UNDCL, &c);
    if(s!=MSTRO_OK) {
      ERR("Failed to batch-declare CDO |%s|: %d (%s)\n", name, s, mstro_status_description(s));
      break;
    }
    WITH_CDO_ID_STR(cidstr, &c, {
        WITH_CDO_ID_STR(gidstr, cdoid, {
            DEBUG("Batch-declared CDO |%s| (%s) for group %s from app %" PRIappid "\n",
                  name, cidstr, gidstr, app_id);});};);
    
    memcpy(&entry->member_ids[i+offset], &c, sizeof(struct mstro_cdo_id));
    entry->member_names[i+offset]=strdup(members->undeclared_members[i]);
    if(entry->member_names[i+offset]==NULL) {
      s=MSTRO_NOMEM;
      goto BAILOUT;
    }
  }
  /* now that all CDOs are declared we need to fix up the members
   * structure so that DEMANDind clients will not see undeclared CDOs
   * in the member attribute */
  /* NOTE: This relies on the protobuf allocator being system malloc/realloc/free */
  Mstro__Pool__CDOID **tmp = realloc(members->declared_members,
                                    sizeof(Mstro__Pool__CDOID*)*entry->num_cdos);
  if(tmp==NULL) {
    ERR("Failed to reallocate group member attribute\n");
    s=MSTRO_NOMEM;
  } else {
    members->declared_members = tmp;
    for(size_t i=0; i<members->n_undeclared_members; i++) {
      members->declared_members[i+offset] = malloc(sizeof(Mstro__Pool__CDOID));
      if(members->declared_members[i+offset]==NULL) {
        ERR("Failed to allocate member UUID slot\n");
        /* FIXME: leaking here */
        s=MSTRO_NOMEM;
        goto BAILOUT;
        break;
      }
      mstro__pool__cdoid__init(members->declared_members[i+offset]);
      members->declared_members[i+offset]->qw0 = entry->member_ids[i+offset].qw[0]; 
      members->declared_members[i+offset]->qw1 = entry->member_ids[i+offset].qw[1];
      members->declared_members[i+offset]->local_id = entry->member_ids[i+offset].local_id;
      free(members->undeclared_members[i]);
    }
    free(members->undeclared_members);
    members->undeclared_members=NULL;
    members->n_undeclared_members = 0;
    members->n_declared_members = entry->num_cdos;
  }                                 
  
BAILOUT:
  if(s!=MSTRO_OK) {
    if(entry) {
      if(entry->member_ids)
        free(entry->member_ids);
      if(entry->member_names) {
        for(size_t i=0; i<entry->num_cdos; i++) {
          if(entry->member_names[i]!=NULL) {
            free(entry->member_names[i]);
          } else {
            break;
          }
        }
        free(entry->member_names);
      }
    }
    free(entry);
  } else {
    *result = entry;
  }
  return s;
}

static inline
mstro_status
mstro_pm__group_registry_entry_dispose(struct mstro_pm__group_record *r)
{
  if(r==NULL)
    return MSTRO_INVARG;
  if(--(r->num_refs)>0) {
    /* no locking needed: num_refs is atomic, and when we reach 0 no
     * one else will step on our feet, or see a 0 */
    free(r->member_ids);
    for(size_t i=0; i<r->num_cdos; i++) {
      free(r->member_names[i]);
    }
    free(r->member_names);
    free(r);
  }
  return MSTRO_OK;
}

static inline
mstro_status
mstro_pm__group_registry_entry_check_consistency(struct mstro_pm__group_record *record,
                                                 const Mstro__Pool__GroupMembers *members)
{
  if(record==NULL)
    return MSTRO_INVARG;
  if(members==NULL)
    return MSTRO_OK;
  if(members->sel!=NULL) {
    ERR("FIXME: unimplemented case of group with selector and an exisiting record\n");
    return MSTRO_UNIMPL;
  }
  for(size_t i=0;i<members->n_declared_members;i++) {
    struct mstro_cdo_id c = { .qw[0] = members->declared_members[i]->qw0,
                              .qw[1] = members->declared_members[i]->qw1,
                              .local_id = members->declared_members[i]->local_id };
    bool found = false;
    /* FIXME: avoid linear search here */
    for(size_t j=0; j<record->num_cdos; j++) {
      if(mstro_cdo_id__equal(&record->member_ids[j], &c)) {
        found = true;
        break;
      }
    }
    if(!found) {
      WITH_CDO_ID_STR(idstr, &c, {
          ERR("Group member %s not present in existing group record\n", idstr);});
      return MSTRO_NOMATCH;
    }
  }
  
  for(size_t i=0;i<members->n_undeclared_members;i++) {
    const char *c = members->undeclared_members[i];
    bool found = false;
    /* FIXME: avoid linear search here */
    for(size_t j=0; j<record->num_cdos; j++) {
      if(strcmp(record->member_names[j],c)==0) {
        found = true;
        break;
      }
    }
    if(!found) {
      ERR("Group member %s not present in existing group record\n", c);
    }
  }
  
  return MSTRO_OK;
}



static inline
mstro_status 
mstro_pm__group_seal(const struct mstro_cdo_id *cdoid,
                     mstro_app_id app_id,
                     Mstro__Pool__Attributes *attributes)
{
  mstro_status s = MSTRO_UNIMPL;
  size_t i;
  const Mstro__Pool__KvEntry *e=NULL;

  if(cdoid==NULL || attributes==NULL)
    return MSTRO_INVARG;
  if(attributes->val_case!=MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP)
    return MSTRO_UNIMPL;
  
  for(i=0; i<attributes->kv_map->n_map; i++) {
    e = attributes->kv_map->map[i];
    if(strcmp(e->key,MSTRO_ATTR_CORE_CDO_GROUP_MEMBERS)==0) {
      break;
    }
  }
  if(i==attributes->kv_map->n_map) {
    ERR("Group has no members attribute\n");
    return MSTRO_INVARG;
  }

  assert(e->val->val_case == MSTRO__POOL__AVAL__VAL_BYTES);
  
  Mstro__Pool__GroupMembers *members 
      = mstro__pool__group_members__unpack(NULL, e->val->bytes.len, e->val->bytes.data);
  DEBUG("Group has %zu undeclared and %zu declared members. Selector is %p\n",
        members->n_undeclared_members, members->n_declared_members, members->sel);
  if(members->sel!=NULL) {
    ERR("GROUPS with CDO selectors unimplemented.\n");
    s= MSTRO_UNIMPL;
    goto BAILOUT;
  }


  /* register in group table */
  WITH_LOCKED_GROUP_REGISTRY({
      int ret;
      khiter_t slot = kh_put(group_reg, &g_pm_group_registry, cdoid, &ret);
      switch(ret) {
        case -1:
          ERR("Failed to register group\n");
          s=MSTRO_FAIL;
          break;
        case 0: {
          /* group already present, fetch handle and verify */
          WITH_CDO_ID_STR(idstr, cdoid, {
              DEBUG("Group %s record key already present, checking consistency\n", idstr);});
          slot = kh_get(group_reg, &g_pm_group_registry, cdoid);
          struct mstro_pm__group_record *rec = kh_val(&g_pm_group_registry, slot);
          s=mstro_pm__group_registry_entry_check_consistency(rec, members);
          break;
        }
        default: {
          /* inserted key, also insert entry */
          struct mstro_pm__group_record *entry = NULL;
          s = mstro_pm__group_record_create(cdoid, app_id, members, &entry);
          if(s!=MSTRO_OK) {
            ERR("Failed to construct new group registry entry\n");
          } else {          
            kh_val(&g_pm_group_registry,slot) = entry;
            WITH_CDO_ID_STR(idstr, cdoid, {
                DEBUG("Group %s record created, %zu members\n",
                      idstr, entry->num_cdos);});
            s=MSTRO_OK;
          }
          break;
        }
      }
    });
  
  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PM_NUM_SEAL_GROUP, 1);

BAILOUT:
  if(members!=NULL) {
    /* need to re-serialize and replace member attribute on CDO, then deallocate it */
    size_t size = mstro__pool__group_members__get_packed_size(members);
    uint8_t *buf = malloc(size*sizeof(uint8_t));
    if(!buf) {
      ERR("Failed to allocate replacement group_members buffer\n");
      s=MSTRO_NOMEM;
    } else {
      size_t written_size = mstro__pool__group_members__pack(members, buf);
      assert(written_size == size);

      /* Stuff into existing protobuf BYTES entry where we found the
       * serialized form of the original members data */
      e->val->bytes.len = size;
      free(e->val->bytes.data);
      e->val->bytes.data = buf;
      WITH_CDO_ID_STR(idstr, cdoid, {
          DEBUG("Updated group %s member attribute: Now %zu + %zu (decl/undecl) members: %zu bytes\n",
                idstr, members->n_declared_members, members->n_undeclared_members, written_size);});
    }
    mstro__pool__group_members__free_unpacked(members, NULL);
  }
  
  return s;
}

/*** SEAL ***/
/**Handled by operations*/

mstro_status
mstro_pm__cdo_seal(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;

  /* sanity check that it's been declared already */
  status = mstro_pm_cdo_registry_find(op->appid, &(op->cdoid));
  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(idstr, &(op->cdoid),
                    {ERR("CDO %s id not known here for app\n", idstr);});
    return MSTRO_INVMSG;
  }

  Mstro__Pool__Seal *seal = op->msg->seal;

  if(seal->attributes!=NULL) {
    DEBUG("Seal attributes: default-namespace |%s|, kind %s\n",
          seal->attributes->default_namespace == NULL
          ? seal->attributes->default_namespace : "(empty)",
          seal->attributes->val_case==MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP
          ? "kv-map" : (
              seal->attributes->val_case
              == MSTRO__POOL__ATTRIBUTES__VAL_YAML_STRING
              ? "yaml" : "(other)"));
    if(seal->attributes->val_case==MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP) {
      DEBUG("Seal attributes: %zu entries\n",
            seal->attributes->kv_map->n_map);
    }

    /* if this is a GROUP CDO perform extra work: Perform PM-side
     * group registration, declare undeclared CDOs in one go, etc. */
    if(seal->attributes->val_case!=MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP) {
      DEBUG("If this SEAL is for a group CDO it will fail (FIXME)\n");
    } else if(seal->attributes->kv_map!=NULL) {
      size_t i;
      for(i=0; i<seal->attributes->kv_map->n_map; i++) {
        const Mstro__Pool__KvEntry *e = seal->attributes->kv_map->map[i];
        if(strcmp(e->key,MSTRO_ATTR_CORE_CDO_ISGROUP)==0) {
          WITH_CDO_ID_STR(idstr, &(op->cdoid),{
              DEBUG("Recognized %s as GROUP-CDO\n", idstr);
            });
          status = mstro_pm__group_seal(&(op->cdoid), op->appid, seal->attributes);
          if(status!=MSTRO_OK) {
            WITH_CDO_ID_STR(idstr, &(op->cdoid),{
                ERR("Failed to perform GROUP SEAL operation on %s: %d (%s)\n",
                    idstr, status, mstro_status_description(status));});
            return status;
          } else {
            op->send_attribute_update = true;
            break; /* no need to look further */
          }
        }
      }
    }

    /* FIXME: would be nice to be able to print out the actual attributes added here */
    op->status = mstro_pm_cdo_registry_store_attributes(&(op->cdoid), op->appid,
                                                    seal->attributes);

    if ((op->status != MSTRO_WOULDBLOCK) && (op->status != MSTRO_OK))
    {
      /**Failed operation ... should send ack and close*/
      op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*engine will execute next step, i.e. send ack */
      status = MSTRO_OK; /**we want the operation engine to continue either to notify subscribers or sending ack */
    } 

    if(op->status==MSTRO_OK) {
      WITH_CDO_ID_STR(idstr, &(op->cdoid),
                  INFO("app %" PRIappid " sealed CDO (global id: `%s')\n",
                       op->appid, idstr);
                  );
    } else {
      WITH_CDO_ID_STR(idstr, &(op->cdoid),{
          ERR("Failed to set attributes on %s for app %" PRIappid "\n", idstr, op->appid);
        });
    }
  }

  return status;
}

/*** OFFER ***/
// handled by operations

/*** REQUIRE ***/
// handled by operations

/*** RETRACT ***/
// handled by operations

/*** DEMAND ***/
mstro_status
mstro_pm__cdo_demand(mstro_pool_operation op)
{
  mstro_status status  = MSTRO_UNIMPL;
  status = mstro_pm_cdo_registry_update_state_op(op);
  if(status!=MSTRO_OK) {
    ERR("Failed to update CDO registry for app %" PRIappid "\n", op->appid);
    return status;
  }

  /* trigger transport process */
  op->status =mstro_pm_demand_queue_insert(&(op->cdoid), op->appid);
  if(op->status!=MSTRO_OK) {
    WITH_CDO_ID_STR(idstr, &(op->cdoid),{
        ERR("Failed to trigger transport process for %s to %"PRIappid": %d\n",
            idstr, op->appid, op->status);
      });
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /*engine will execute next step, i.e. send ack */
    status = MSTRO_OK;
  }
  return status;
}

/*** WITHDRAW ***/
/**handled by operations*/

/*** DISPOSE ***/
/** DISPOSE is handled by pool operations */

void
mstro_pm__remove_app_address(mstro_event event, void *context)
{
  struct mstro_pm_app_registry_entry *app_entry = context;
  if(app_entry==NULL) {
    ERR("Target %zu not in app table\n", app_entry->appid);
    return ;
  } 
  int ret = fi_av_remove(app_entry->ep->av, &app_entry->addr,1,0);
  if(ret != 0)
  {
    ERR("Failed to remove app %"PRIappid" address from av %d (%s) \n", app_entry->appid , ret, fi_strerror(-ret));
  }
  else
  {
     DEBUG("Removed app %"PRIappid" from fi_av\n", app_entry->appid);
  } 
}

extern mstro_event_domain g_ofi_rdma_edom;

/*** LEAVE ***/
mstro_status
mstro_pm__send_bye(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  INFO("Granting LEAVE request from app %" PRIappid "\n", op->appid);
  
  Mstro__Pool__Bye bye = MSTRO__POOL__BYE__INIT;

  Mstro__Pool__MstroMsg msg_bye = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg_bye, (ProtobufCMessage*)&bye);
  if(status!=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        bye.base.descriptor->name);
    return status;
  }

  struct mstro_pm_app_registry_entry *app_entry = NULL;
  status = mstro_pm_app_lookup(op->appid, &app_entry);
  if(app_entry==NULL) {
    ERR("Target %zu not in app table\n", op->appid);
    return MSTRO_FAIL;
  } 

  /**remove app from av - if we see the app again it can cause a problem, inserting duplicates in av results in undefined behavior 
   * see notes on https://ofiwg.github.io/libfabric/v1.18.0/man/fi_av.3.html
   * "An AV should only store a single instance of an address. 
   * Attempting to insert a duplicate copy of the same address into an AV may result in undefined behavior, 
   * depending on the provider implementation. "
   */
  mstro_event event = NULL;
  status=mstro_event_create(
      g_ofi_rdma_edom,
      mstro_pm__remove_app_address,
      app_entry,
      NULL, false, &event);
  if(status!=MSTRO_OK) {
    ERR("Failed to create completion event\n");
    return status;
  }

  status = mstro_pmp_send_w_completion(op->appid, &msg_bye, event);
  if(status!=MSTRO_OK) {
    ERR("Cannot send BYE reply to app %" PRIappid ": %d (%s)\n",
        op->appid, status, mstro_status_description(status));
    return status;
  }
  return status;
}


/*** JOIN ***/
mstro_status
mstro_pm__send_welcome(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;

  Mstro__Pool__Appid id = MSTRO__POOL__APPID__INIT;
  id.id = op->appid;
  
  Mstro__Pool__Apptoken token = MSTRO__POOL__APPTOKEN__INIT;
  token.appid = &id;
  
  Mstro__Pool__Welcome welcome = MSTRO__POOL__WELCOME__INIT;
  welcome.token = &token;
  
  
  if (op->status!= MSTRO_OK)
  {
    /*Send NACK*/
    welcome.token = NULL;
    welcome.nack = true;
    INFO("Sending negative acknolwedge for a join request \n");

    Mstro__Pool__MstroMsg msg_welcome = MSTRO__POOL__MSTRO_MSG__INIT;
    status = mstro_pmp_package(&msg_welcome,
                                          (ProtobufCMessage*)&welcome);
    if(status!=MSTRO_OK) {
      ERR("Failed to package %s into a pool manager message\n",
        welcome.base.descriptor->name);
      return status;
    }

    status = mstro_pmp_send_nowait_ep(op->join.ep, op->join.translated_addr, &msg_welcome);
    if(status!=MSTRO_OK) {
      ERR("Cannot send WELCOME-NACK reply: %d (%s)\n",
           status, mstro_status_description(status));
      return status;
    }
  }
  else 
  {
  
    INFO("Sending Welcome message to app %"PRIappid" \n", op->appid);
    Mstro__Pool__MstroMsg msg_welcome = MSTRO__POOL__MSTRO_MSG__INIT;
    status = mstro_pmp_package(&msg_welcome,
                                          (ProtobufCMessage*)&welcome);
    if(status!=MSTRO_OK) {
      ERR("Failed to package %s into a pool manager message\n",
        welcome.base.descriptor->name);
      return status;
    }
  
    status = mstro_pmp_send_nowait(op->appid,
                                 &msg_welcome);
    if(status!=MSTRO_OK) {
      ERR("Cannot send WELCOME reply to %zu: %d (%s)\n",
          op->appid,
          status, mstro_status_description(status));
      return status;
    }
  }
return status;
}

static inline
const char *
mstro_pm__transport_method_name(Mstro__Pool__TransportKind tk)
{
  return protobuf_c_enum_descriptor_get_value
      (&mstro__pool__transport_kind__descriptor, tk)
      ->name;
}

mstro_status
mstro_pm__app_join(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;

  DEBUG("New component %s:%d (PM proto version %06x) advertises endpoint %s\n",
        op->msg->join->component_name, op->msg->join->component_index,
        op->msg->join->protocol_version, op->msg->join->serialized_endpoint);

  if(op->msg->join->transport_methods==NULL
     || (op->msg->join->transport_methods->n_supported==0)) {
    ERR("No transport methods advertised\n");
    op->status = MSTRO_INVMSG;
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1; /**Report error to app*/
    return MSTRO_OK;
  }

  char transports[200];
  Mstro__Pool__TransportMethods *tm = op->msg->join->transport_methods;
  
  transports[0] = '\0'; //initialize
  for (size_t i = 0; i < tm->n_supported; i++)
  {
    strcat(transports, " ");
    strcat(transports, mstro_pm__transport_method_name(tm->supported[i]));
  }

  struct mstro_pm_app_registry_entry *regentry = NULL;
  op->status = mstro_pm__register_app(op,&regentry);
  if(op->status!=MSTRO_OK) {
    if(op->status == MSTRO_NOT_TWICE) {
      ERR("Failed to register caller: already have another entity registered with same workflow/component/component-id triple\n");
      op->step = MSTRO_OP_ST_SEND_POOLOP_ACK-1;
      status=MSTRO_OK;
      return status;
    } else {
      ERR("Failed to register caller as new app\n");
      op->step = MSTRO_OP_ST_SEND_POOLOP_ACK-1; 
      status=MSTRO_OK;
      return status;
    }
  }
  status = MSTRO_OK;
  DEBUG("Registered app at id %"PRIappid", Transport: %s \n",
       regentry->appid, transports);

  /**save app id to operation */
  op->appid = regentry->appid;

  INFO("JOIN message received. Caller %s:%d is now known as app #%" PRIu64 "\n", 
       op->msg->join->component_name, op->msg->join->component_index, op->appid);

  return status;
}


/*** SUBSCRIBE ***/
mstro_status
mstro_pm__subscribe(mstro_pool_operation op)
{
  DEBUG("SUBSCRIBE from app %" PRIappid "\n", op->appid);
  Mstro__Pool__Subscribe *subscribe = op->msg->subscribe;
  
  assert(subscribe!=NULL); 
  assert(op->appid!=MSTRO_APP_ID_INVALID);

  op->status = mstro_subscription_message_register(subscribe, op->appid, &(op->subscription_handle));
  if(op->status!=MSTRO_OK) {
    ERR("Failed to register subscription for app %" PRIu64 " (local id %" PRIu64 ")\n",
        op->appid, subscribe->local_id);
      /*FIXME should go to send ack step and send a NACK but there is no NACK msg for subscribe!!*/
    return op->status; /** Fail hard .. will drop the message*/
  }

  return MSTRO_OK;
}

/*** UNSUBSCRIBE ***/
mstro_status
mstro_pm__unsubscribe(mstro_pool_operation op)
{
  op->status = mstro_subscription_message_unregister(op->msg->unsubscribe,op->appid);
  if (op->status !=MSTRO_OK)
  {
    ERR("Failed to drop subscription %" PRIu64 "\n", op->subscription_handle->id);
    op->step = MSTRO_OP_ST_SEND_POOLOP_ACK -1;
  }
  return MSTRO_OK;
}

/*** EVENT_ACK ***/
mstro_status
mstro_pm__event_ack(mstro_pool_operation op)
{
  /* pass in to subscription engine. It will do the accounting, and in
   * the end trigger continuations for those handlers that sent the
   * notification event. */
  mstro_status status = mstro_subscription_message_event_ack(op->msg->event_ack);
  if(status!=MSTRO_OK) {
    ERR("Failed to process event ack %" PRIu64 ": %d (%s)\n",
        op->msg->event_ack->serial, status, mstro_status_description(status));
  }
  return status;
}

/* FIXME: missing event handling here */
mstro_status
mstro_pm__msg_resolve(mstro_pool_operation op)
{
  Mstro__Pool__Resolve *resolve = op->msg->resolve;
  mstro_app_id app_id = op->appid;;
  assert(resolve!=NULL); assert(app_id!=MSTRO_APP_ID_INVALID);
  DEBUG("RESOLVE from app %" PRIappid "\n", app_id);
  mstro_status s=MSTRO_UNIMPL;

  Mstro__Pool__ResolveReply reply = MSTRO__POOL__RESOLVE_REPLY__INIT;
  reply.query = resolve;

  switch(resolve->query_case) {
    case MSTRO__POOL__RESOLVE__QUERY_CDOID: {
      reply.result_case = MSTRO__POOL__RESOLVE_REPLY__RESULT_NAME;
      struct mstro_cdo_id id = {.qw[0] = resolve->cdoid->qw0,
                                .qw[1] = resolve->cdoid->qw1,
                                .local_id = resolve->cdoid->local_id };
      s= mstro_pm_cdo_registry_cdo_name_lookup(&id, (const char **)&reply.name);
      if(s!=MSTRO_OK) {
        WITH_CDO_ID_STR(idstr, &id, {
            ERR("Cannot find CDO registry entry for |%s|\n",
                idstr);});
        reply.name = NULL;
      } else {
        ; /* set in lookup call above */
      }
      WITH_CDO_ID_STR(idstr, &id, {
          DEBUG("Resolved CDO ID %s to |%s| for app %" PRIappid "\n",
                idstr, reply.name, app_id);});
      s=MSTRO_OK;
      break;
    }
    case MSTRO__POOL__RESOLVE__QUERY_APPID: {
      reply.result_case = MSTRO__POOL__RESOLVE_REPLY__RESULT_NAME;
      mstro_app_id appid = resolve->appid;
      struct mstro_pm_app_registry_entry *entry;
      s = mstro_pm_app_lookup(appid, &entry);
      if(s!=MSTRO_OK) {
        ERR("Cannot find app for ID " PRIi64 "\n", appid);
        reply.name = NULL;
      } else {
        reply.name = entry->component_name;
      }
      DEBUG("Resolved app %" PRIappid " to |%s| for app %" PRIappid "\n",
            appid, reply.name, app_id);
      s=MSTRO_OK;
      break;
    }

    case MSTRO__POOL__RESOLVE__QUERY__NOT_SET: {
      ERR("Invalid resolve query received\n");
      s=MSTRO_INVARG; return s;
      break;
    }
    default: {
      ERR("Unsupported resolve query type: %d\n",
          resolve->query_case);
      s=MSTRO_UNIMPL; return s;
    }
  }
  /* send reply */
  Mstro__Pool__MstroMsg rmsg = MSTRO__POOL__MSTRO_MSG__INIT;
  s = mstro_pmp_package(&rmsg, (ProtobufCMessage*)&reply);
  if(s!=MSTRO_OK) {
    ERR("Failed to package ResolveReply into a pool manager message\n",
        reply.base.descriptor->name);
    return s;
  }
  s = mstro_pmp_send_nowait(app_id, &rmsg);
  if(s!=MSTRO_OK) {
    ERR("Failed to send ResolveReply to app %" PRIappid "\n", app_id);
    return s;
  }

  return s;
}


/** unpack envelope and handle PM-side message*/

/* Pool manager "Handle incoming Message" dispatcher function */
mstro_status
mstro_pm_handle_msg(const struct mstro_msg_envelope *envelope)
{
  
  
  /* for now we serialize things in here. Instead we could push to a
   * queue and have a queue runner in a different thread pick things
   * up */
  mstro_status status=MSTRO_OK;
  DEBUG("PM Handling incoming message size %d\n", envelope->payload_size);

  Mstro__Pool__MstroMsg *msg
      = mstro__pool__mstro_msg__unpack(NULL, envelope->payload_size,
                                       envelope->data);

  const struct mstro_endpoint *ep = envelope->ep;
  
  if(msg==NULL) {
    ERR("Failed to unpack the message\n");
    status = MSTRO_INVMSG;
    goto BAILOUT;
  } else {
    DEBUG("... unpacked msg: type %s (%s), vsm %s\n",
         msg->base.descriptor->name,
         (msg->base.descriptor==&mstro__pool__mstro_msg__descriptor
          ? msg->declare->base.descriptor->name : "no subtype"),
         msg->opts && msg->opts->vsm_data ? "yes" : "no");
  }
  
  if(msg->base.descriptor != &mstro__pool__mstro_msg__descriptor) {
    ERR("Cannot handle messages other than MstroPool messages\n");
    mstro_pm__msg_free(msg);
    status = MSTRO_INVMSG;
    goto BAILOUT;
  }
  
  if(msg->opts && msg->opts->vsm_data!=NULL) {
    mstro_stats_add_counter(MSTRO_STATS_CAT_OFI, MSTRO_STATS_L_PM_NUM_VSM_IN, 1);
    ERR("VSM, Unimplemented\n");
    mstro_pm__msg_free(msg);
    status=MSTRO_UNIMPL;
    goto BAILOUT;
  }

  /* FIXME: even JOIN should have security token */
  if(msg->msg_case!=MSTRO__POOL__MSTRO_MSG__MSG_JOIN) {
    if(!msg->token) {
      ERR("Incoming message without token\n");
      mstro_pm__msg_free(msg);
      status=MSTRO_INVMSG;
      goto BAILOUT;
    }
    if(!msg->token->appid) {
      ERR("Incoming message without App ID\n");
      mstro_pm__msg_free(msg);
      status=MSTRO_INVMSG;
      goto BAILOUT;
    }

    /* FIXME: should check whether the app is JOINED */
    if(msg->token->appid->id==MSTRO_APP_ID_INVALID) {
      ERR("Invalid app id -- are you JOINed?\n");
      mstro_pm__msg_free(msg);
      status=MSTRO_INVMSG;
      goto BAILOUT;
    }
  }
  
  switch(msg->msg_case) {
    /* 'good' messages: */
    case MSTRO__POOL__MSTRO_MSG__MSG_DECLARE:
      //DEBUG("PM MSG-DECLARE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_SEAL:
      //DEBUG("PM MSG-SEAL result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_OFFER:
      //DEBUG("PM MSG-OFFER result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_REQUIRE:
      //DEBUG("PM MSG-REQUIRE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_RETRACT:
      //DEBUG("PM MSG-RETRACT result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND:
      //DEBUG("PM MSG-DEMAND result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_WITHDRAW:
      //DEBUG("PM MSG-WITHDRAW result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_DISPOSE:
      //DEBUG("PM MSG-DISPOSE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_COMPLETED:
      //DEBUG("PM TRANSFER-COMPLETED result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_LEAVE:
      //DEBUG("PM MSG-LEAVE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE:
      //DEBUG("PM MSG-SUBSCRIBE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_UNSUBSCRIBE:
      //DEBUG("PM MSG-UNSUBSCRIBE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_EVENT_ACK:
      //DEBUG("PM MSG-EVENT-ACK result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE:
      //DEBUG("PM MSG-RESOLVE result: %d\n", status);
      status = mstro_pm__op_maker(msg, NULL);
      break;

    case MSTRO__POOL__MSTRO_MSG__MSG_JOIN:
      //DEBUG("PM MSG-JOIN result: %d\n", status);
      status = mstro_pm__op_maker(msg, ep); 
      break;

      /* currently unimplemented: */
    case MSTRO__POOL__MSTRO_MSG__MSG_SEAL_GROUP:

    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND_ATTR:

    case MSTRO__POOL__MSTRO_MSG__MSG_QUERY:

    case MSTRO__POOL__MSTRO_MSG__MSG_LOG:

    case MSTRO__POOL__MSTRO_MSG__MSG_INITIATE_TRANSFER:

    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_TICKET: {
      WARN("%s message received, dropping it, not even sending ACK\n",
           msg->base.descriptor->name);
      mstro_pm__msg_free(msg);
      status = MSTRO_UNIMPL;
      break;
    }

      /* 'illegal' messages: */
    case MSTRO__POOL__MSTRO_MSG__MSG_WELCOME:
    case MSTRO__POOL__MSTRO_MSG__MSG_BYE:
    case MSTRO__POOL__MSTRO_MSG__MSG_DECLARE_ACK:
    case MSTRO__POOL__MSTRO_MSG__MSG_ACK:
    case MSTRO__POOL__MSTRO_MSG__MSG_EVENT:
    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND_ATTR_RES:
    case MSTRO__POOL__MSTRO_MSG__MSG_QUERY_RES:
    case MSTRO__POOL__MSTRO_MSG__MSG__NOT_SET: {
      ERR("Invalid message kind (incoming on PM side): %s\n",
          msg->base.descriptor->name);
      mstro_pm__msg_free(msg);
      status=MSTRO_INVARG;
      goto BAILOUT;
    }

    default:
      WARN("Unknown message kind: %s\n",
           msg->base.descriptor->name);
      mstro_pm__msg_free(msg);
      status = MSTRO_INVARG;
      break;
  }

BAILOUT:
  /* messages get deallocated by sub-handlers, not here */
  //DEBUG("PM handler returning status %d\n", status);
  
  return status;
}




