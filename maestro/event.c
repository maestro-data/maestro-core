/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Event handling infrastructure
 **/
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/i_event.h"
#include "maestro/i_uthash.h"
#include "maestro/logging.h"

#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <assert.h>
#include <inttypes.h>

/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)


/** An event descriptor */
struct mstro_event_ {
  UT_hash_handle hh;           /**< hashable, by @ref mstro_event.id */
  mstro_event_id id;           /**< the ID, unique within an event domain */
  mstro_event_handler handler; /**< the handler function */
  void *closure;               /**< the user-provided closure */
  bool auto_destroy;           /**< whether to deallocate the event
                                * after triggering */
  void(*closure_dtor)(void*);  /**< user-provided closure dtor */
  pthread_mutex_t mtx;         /**< mutex locking this event */
  mstro_event_domain domain;   /**< domain that the event belongs to */
};

#define WITH_LOCKED_EVENT(e,...) do {                                   \
    int _wloe_err = pthread_mutex_lock(&e->mtx);                        \
    if(_wloe_err!=0) {                                                  \
      ERR("Failed to lock event mutex: %d (%s)\n",                      \
          _wloe_err, strerror(_wloe_err));                              \
    } else {                                                            \
      do {__VA_ARGS__} while(0);                                        \
      _wloe_err = pthread_mutex_unlock(&e->mtx);                        \
      if(_wloe_err!=0) {                                                \
        ERR("Failed to unlock event mutex: %d (%s)\n",                  \
            _wloe_err, strerror(_wloe_err));                            \
      }                                                                 \
    }                                                                   \
  } while(0)



#define MSTRO_EVENT_ID_INVALID 0
/** An event domain */
struct mstro_event_domain_ {
  char *name;                /**< A domain name */
  _Atomic(uint64_t) next_id; /**< ID assigned to next event */
  mstro_event events;        /**< table of registered events */
  pthread_mutex_t mtx;       /**< mutex locking the event table */
  pthread_cond_t cond;       /**< associated condition variable */
};

#define WITH_LOCKED_DOMAIN(d,...) do {                                  \
    int _wlod_err = pthread_mutex_lock(&d->mtx);                        \
    if(_wlod_err!=0) {                                                  \
      ERR("Failed to lock event domain %p (%s) mutex: %d (%s)\n",       \
          d, d->name, _wlod_err, strerror(_wlod_err));                  \
    } else {                                                            \
      do {__VA_ARGS__} while(0);                                        \
      _wlod_err = pthread_mutex_unlock(&d->mtx);                        \
      if(_wlod_err!=0) {                                                \
        ERR("Failed to unlock event domain %p (%s) mutex: %d (%s)\n",   \
            d, d->name, _wlod_err, strerror(_wlod_err));                 \
      }                                                                 \
    }                                                                   \
  } while(0)

mstro_status
mstro_event_domain_create(const char *name, mstro_event_domain *result)
{
  if(result==NULL)
    return MSTRO_INVOUT;
  *result = malloc(sizeof(struct mstro_event_domain_));
  if(*result==NULL) {
    return MSTRO_NOMEM;
  }
  
  char anon_name[1024];
  if(name==NULL || name == MSTRO_EVENT_DOMAIN_ANONYMOUS) {
    sprintf(anon_name, "(anonymous@%p)", (void*)*result);
    name=anon_name;
  }
  (*result)->name=strdup(name);
  if((*result)->name==NULL) {
    free(*result);
    return MSTRO_NOMEM;
  }
  (*result)->next_id = 1;
  (*result)->events=NULL;
  int err = pthread_mutex_init(&(*result)->mtx, NULL);
  err += pthread_cond_init(&(*result)->cond, NULL);
  if(err) {
    free((*result)->name);
    free(*result);
    return MSTRO_NOMEM;
  }

  return MSTRO_OK;
}

static inline mstro_status
mstro_event_destroy__internal(mstro_event event);

mstro_status
mstro_event_domain_destroy(mstro_event_domain d)
{
  if(d==NULL)
    return MSTRO_INVARG;

  /* Give a little time for events to cleanly finish */
  mstro_status s;
  mstro_cleanup_waitup(s, d->events, &d->mtx, &d->cond, d->name);
  if (s != MSTRO_OK)
    return s;
 
  mstro_event tmp,el;
  HASH_ITER(hh, d->events, el, tmp) {
    HASH_DEL(d->events, el);
    mstro_event_destroy__internal(el);
  }

  int err = pthread_mutex_destroy(&d->mtx);
  err += pthread_cond_destroy(&d->cond);
  free(d->name);
  free(d);

  if(err)
    return MSTRO_FAIL;

  return MSTRO_OK;
}

static inline
mstro_event
mstro_event_find_by_id(mstro_event_domain d, mstro_event_id id)
{
  assert(d!=NULL); assert(id!=MSTRO_EVENT_ID_INVALID);
  
  mstro_event res=NULL;
  WITH_LOCKED_DOMAIN(d, {
      HASH_FIND(hh,d->events, &id, sizeof(mstro_event_id), res);
    });
  return res;
}

static
mstro_event_id
mstro_event_domain_nextid(mstro_event_domain d)
{
  uint64_t id = atomic_fetch_add(&d->next_id, 1);

  /* of course, only one of the concurrent users will see this, but
     that's enough for all to die. */
  if(id==MSTRO_EVENT_ID_INVALID) {
    ERR("FIXME: Wrap-around of event domain IDs, unimplemented, giving up.\n");
    
    /* to solve the issue we'd need some form of either compaction of
     * exisiting IDs at this point, or a reasonably efficient way to
     * find free IDs among the current live events. This is tricky to
     * get fast (and correct, when the fast path here is fetch-add). */
    
    abort();
  }

  return id;
}
  

mstro_status
mstro_event_create(mstro_event_domain d,
                   mstro_event_handler handler, void *closure,
                   void(*closure_dtor)(void* closure),
                   bool auto_destroy,
                   mstro_event *result)
{
  if(d==NULL) {
    ERR("NULL event domain\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("Invalid event output destination\n");
    return MSTRO_INVOUT;
  }
  
  *result = malloc(sizeof(struct mstro_event_));
  if(*result==NULL) {
    ERR("Failed to allocate event data structure\n");
    return MSTRO_NOMEM;
  }
  (*result)->handler = handler;
  (*result)->closure = closure;
  (*result)->auto_destroy = auto_destroy;
  (*result)->closure_dtor= closure_dtor;
  (*result)->id = mstro_event_domain_nextid(d);
  (*result)->domain = d;

  int err = pthread_mutex_init(&(*result)->mtx, NULL);
  if(err!=0) {
    ERR("Failed to initialize event mutex\n");
    free(*result);
    return MSTRO_FAIL;
  }

  WITH_LOCKED_DOMAIN(d, 
                     HASH_ADD(hh, d->events,
                              id, sizeof(mstro_event_id),
                              *result);
                     );
  return MSTRO_OK;
}

/** Obtain event ID.
 *
 * This ID is sufficient to locate the event in its domain later.
 */
mstro_status
mstro_event_id_get(mstro_event event, mstro_event_id *id)
{
  if(event==NULL)
    return MSTRO_INVARG;
  if(id==NULL)
    return MSTRO_INVOUT;
  *id = event->id;
  return MSTRO_OK;
}

mstro_status
mstro_event_domain_name_get(mstro_event event, char** name)
{
  if(event==NULL)
    return MSTRO_INVARG;
  if(name==NULL)
    return MSTRO_INVARG;
  *name = strdup(event->domain->name);
  return MSTRO_OK;
}

/* belts-off internal deallocator (no locking) */
static inline mstro_status
mstro_event_destroy__internal(mstro_event event)
{
  assert(event!=NULL);
  
  if(event->closure_dtor) {
    event->closure_dtor(event->closure);
    event->closure = NULL;
  }

  int err = pthread_mutex_destroy(&event->mtx);
  if(err!=0) {
    ERR("Failed to destroy event mutex: %d (%s)\n",
        err, strerror(err));
    return MSTRO_FAIL;
  }
  
  free(event);
  return MSTRO_OK;
}

/** Destroy an event.
 *
 * If it had a non-NULL closure_dtor set at creation time, that
 * destructor will be called on the closure argument.
 */
#include <errno.h>
mstro_status
mstro_event_destroy(mstro_event event)
{
  if(event==NULL) {
    ERR("NULL event\n");
    return MSTRO_INVARG;
  }

  mstro_status s=MSTRO_OK;
  mstro_event_domain d = event->domain; /* need to save before killing event */
  WITH_LOCKED_DOMAIN(d,
                     HASH_DEL(d->events, event);
                     s = mstro_event_destroy__internal(event);
                     if (HASH_COUNT(d->events) == 0) {
                       int err = pthread_cond_signal(&d->cond);
                       if (err) {
                         ERR("Couldn't signal event list is empty (errno: %d)\n", err);
                         s = MSTRO_FAIL;
                       }
                     }
                     );

  return s;
}

mstro_status
mstro_event_destroy_by_id(mstro_event_domain d, mstro_event_id id)
{
  if(d==NULL)
    return MSTRO_INVARG;
  if(id==MSTRO_EVENT_ID_INVALID)
    return MSTRO_INVARG;
  
  mstro_event e = mstro_event_find_by_id(d,id);
  if(e==NULL) {
    ERR("Event id %" PRIu64 " not found in domain %s\n", id, d->name);
    return MSTRO_FAIL;
  } else {
    return mstro_event_destroy(e);
  }
}

mstro_status
mstro_event_trigger(mstro_event e,
                    void **result)
{
  if(e==NULL)
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;

  assert(e->handler!=NULL);
  e->handler(e, e->closure);
  if(e->auto_destroy) {
    mstro_event_destroy(e);
    *result = NULL;
  } else {
    *result = e->closure;
  }

  return MSTRO_OK;
}

mstro_status
mstro_event_trigger_by_id(mstro_event_domain d,
                          mstro_event_id id,
                          void **result)
{
  mstro_event e = mstro_event_find_by_id(d,id);
  if(e==NULL) {
    ERR("Event id %" PRIu64 " not found in domain %s\n", d->name);
    return MSTRO_FAIL;
  } else {
    return mstro_event_trigger(e, result);
  }
}


  

