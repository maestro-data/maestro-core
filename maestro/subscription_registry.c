#include "maestro/pool.h"
#include "i_subscription_registry.h"
#include "maestro/logging.h"
#include "maestro/i_utlist.h"
#include <inttypes.h>
#include "maestro/i_globals.h"
#include "maestro/i_misc.h"
#include "maestro/i_event.h"
#include "maestro/i_cdo.h"
#include "maestro/cdo.h"
#include "maestro/i_attributes.h"

#include "maestro/i_pool.h"


#include "attributes/maestro-schema.h"


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_EVENT,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_EVENT,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_EVENT,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_EVENT,__VA_ARGS__)


/** A table storing a set of subscriptions */

struct mstro_subscription_table_entry {
  /** who to notify by pool messages */
  mstro_app_id                          subscriber;
  /** global subscription handle identifier */
  mstro_event                           subscription_event_handle;
  /** the subscription */
  struct mstro_subscription_            *subscription;
  /** (linked list:) next entry */
  struct mstro_subscription_table_entry *next; 
};

struct mstro_subscription_table {
  /* FIXME: this should be more complicated to make matching events to
   * subscriptions cheaper, but for now it is really stupid */

  /** Event domain */
  mstro_event_domain edom;
  /** linked list of entries */
  struct mstro_subscription_table_entry *entries;
};

/** the global subscription table, protected by @ref g_subscription_table_mtx */
static
struct mstro_subscription_table g_subscription_table = {
  .edom = NULL,
  .entries = NULL,
};

/** mutex protecting @ref g_subscription_table */
static pthread_mutex_t g_subscription_table_mtx = PTHREAD_MUTEX_INITIALIZER;

#define WITH_LOCKED_SUBSCRIPTIONS(...) do {                             \
    int _wlos_err = pthread_mutex_lock(&g_subscription_table_mtx);      \
    if(_wlos_err!=0) {                                                  \
      ERR("Failed to lock global subscription mutex: %d (%s)\n",        \
          _wlos_err, strerror(_wlos_err));                              \
    } else {                                                            \
      do {__VA_ARGS__} while(0);                                        \
      _wlos_err = pthread_mutex_unlock(&g_subscription_table_mtx);      \
      if(_wlos_err!=0) {                                                \
        ERR("Failed to unlock global subscription mutex: %d (%s)\n",    \
            _wlos_err, strerror(_wlos_err));                            \
      }                                                                 \
    }                                                                   \
  } while(0)

/** counter of event ids; protected by g_subscription_table_mtx */
static uint64_t g_next_event_id = 1;


/** a counter for outgoing subscription requests to the pool manager */
static _Atomic(uint64_t) g_subscription_request_idx = 1;

/** a data structure containig all subscriptions that are in flight to
 * the pool manager and do not have subscription handles assigned */
struct outstanding_subscription {
  UT_hash_handle hh;   /**< hashable by request_id */
  uint64_t request_id; /**< the local ID of the request in flight */
  struct mstro_subscription_ *subscription; /**< the subscription handle */
};

static struct outstanding_subscription *g_outstanding_subscriptions = NULL;
static pthread_mutex_t g_outstanding_subscriptions_mtx = PTHREAD_MUTEX_INITIALIZER;

#define WITH_LOCKED_OUTSTANDING_SUBSCRIPTIONS(...) do {                 \
    int _wlos_err = pthread_mutex_lock(&g_outstanding_subscriptions_mtx); \
    if(_wlos_err!=0) {                                                  \
      ERR("Failed to lock outstanding subscription mutex: %d (%s)\n",   \
          _wlos_err, strerror(_wlos_err));                              \
    } else {                                                            \
      do {__VA_ARGS__} while(0);                                        \
      _wlos_err = pthread_mutex_unlock(&g_outstanding_subscriptions_mtx); \
      if(_wlos_err!=0) {                                                \
        ERR("Failed to unlock outstanding subscription mutex: %d (%s)\n", \
            _wlos_err, strerror(_wlos_err));                            \
      }                                                                 \
    }                                                                   \
  } while(0)

/* Events from the PM may not include enough payload to provide
 * user-level names directly. Hence we keep a resolver cache for
 * them. This will be populated if we observe DECLARE-ACK, from the
 * local CDO-ID table, or by an explicit RESOLVE call on the first
 * observation of an unknown ID.
 *
 * It would be conceivable to open a backdoor into the global CDO
 * registry (and simply create a CDO object for each CDO observed in
 * an event c ontext) to avoid duplicating CDO id/name data here; on
 * the other hand, separation also has it's advantages.
 */
/** entry in the resolver table for CDOs */
struct cdo_resolver_table_entry {
  /** hash table by ID */
  UT_hash_handle hh;
  /** a CDO ID */
  struct mstro_cdo_id id;
  /** its name */
  char *name;
};
/** the CDO ID->name resolver table */
struct cdo_resolver_table_entry *g_cdo_resolver_table = NULL;

/** a lock for @ref g_cdo_resolver_table */
pthread_mutex_t g_cdo_resolver_table_mtx = PTHREAD_MUTEX_INITIALIZER;

/** evento domain for resolver events */
mstro_event_domain g_resolver_event_domain = NULL;

/** Convenience macro for executing code under resolver table lock */
#define WITH_LOCKED_RESOLVER_TABLE(...) do {                            \
    int _wlrt_err = pthread_mutex_lock(&g_cdo_resolver_table_mtx);      \
    if(_wlrt_err!=0) {                                                  \
      ERR("Failed to lock resolver table mutex: %d (%s)\n",             \
          _wlrt_err, strerror(_wlrt_err));                              \
    } else {                                                            \
      do {__VA_ARGS__} while(0);                                        \
      _wlrt_err = pthread_mutex_unlock(&g_cdo_resolver_table_mtx);      \
      if(_wlrt_err!=0) {                                                \
        ERR("Failed to unlock resolver table mutex: %d (%s)\n",         \
            _wlrt_err, strerror(_wlrt_err));                            \
      }                                                                 \
    }                                                                   \
  } while(0)


/** store a CDO name -> id mapping noticed in an event in the resolver table */
static inline
mstro_status
mstro__subscr_resolver_notice_cdoid(const char *name,
                                    const struct mstro_cdo_id *id)
{
  if(name==NULL || id==NULL)
    return MSTRO_INVARG;
  struct cdo_resolver_table_entry *e
      = malloc(sizeof(struct cdo_resolver_table_entry));
  if(!e) {
    return MSTRO_NOMEM;
  }

  e->id = *id;
  e->name = strdup(name);
  if(e->name==NULL) {
    free(e);
    return MSTRO_NOMEM;
  }

  mstro_status s = MSTRO_OK;

  WITH_LOCKED_RESOLVER_TABLE(
      /* FIXME: check for previous entry with same ID */
      struct cdo_resolver_table_entry *tmp=NULL;
      HASH_FIND(hh, g_cdo_resolver_table,
                &e->id,sizeof(struct mstro_cdo_id), tmp);
      if(tmp) {
        ERR("Already have an entry for CDO id -- should not happen\n");
        s = MSTRO_FAIL;
      } else {
        HASH_ADD(hh, g_cdo_resolver_table, id, sizeof(struct mstro_cdo_id), e);
      }
                             );
  return s;
}

/** drop all entries from resolver table */
static inline
mstro_status
mstro__subscr_resolver_finalize(void)
{
  WITH_LOCKED_RESOLVER_TABLE({
      struct cdo_resolver_table_entry *tmp=NULL, *e=NULL;
      HASH_ITER(hh, g_cdo_resolver_table, e, tmp) {
        HASH_DEL(g_cdo_resolver_table, e);
        free(e->name);
        free(e);
      }
    });
  return MSTRO_OK;
}

/* structure holding a condition variable that will be signaled by
 * @ref event_barrier_handler. Permits implementing a generic @ref
 * event_wait operation using mstro_event data structures */
struct mstro_event__barrier_closure {
  pthread_mutex_t mtx;
  pthread_cond_t cvar;
  bool broadcast; /**< broadcast or signal? */
};

void
mstro_event__barrier_handler(mstro_event e, void *context) {
  const mstro_event ee = e; /* avoid Wunused */
  struct mstro_event__barrier_closure *c
      = (struct mstro_event__barrier_closure*)context;
  pthread_mutex_lock(&c->mtx);
  if(c->broadcast)
    pthread_cond_broadcast(&c->cvar);
  else
    pthread_cond_signal(&c->cvar);
  pthread_mutex_unlock(&c->mtx);
}

/* Bind @arg EVENTNAME to a fresh @ref mstro_event in event domain
 * @ref domain for which @ref event_barrier_handler had been
 * installed, then execute @arg SETUP_CODE. After executing @arg SETUP_CODE, wait
 * for the event to be triggered.
 * Uses cond_broadcast if @arg broadcastp is set, otherwise cond_signal.
 * Setup code that wants to abort can jump to @arg ABORT_LABEL, skipping the wait.
 **/
#define WAIT_FOR_EVENT(eventname, domain, broadcastp, abort_label, setup_code) \
  do {                                                                  \
    mstro_event eventname;                                              \
    bool eventname##triggered = false;                                  \
    struct mstro_event__barrier_closure eventname##closure = {          \
      .mtx = PTHREAD_MUTEX_INITIALIZER,                                 \
      .cvar = PTHREAD_COND_INITIALIZER,                                 \
      .broadcast = broadcastp };                                        \
                                                                        \
    int eventname##s = pthread_mutex_lock(&((eventname##closure).mtx)); \
                                                                        \
    mstro_status eventname##stat =                                      \
        mstro_event_create(domain, mstro_event__barrier_handler,        \
                           &eventname##closure, NULL, true,             \
                           &eventname);                                 \
                                                                        \
    do { setup_code } while(0);                                         \
                                                                        \
    mstro__abort_if_deadlock();                                         \
    pthread_cond_wait(&((eventname##closure).cvar),                     \
                      &((eventname##closure).mtx));                     \
                                                                        \
    eventname##triggered = true;                                        \
 abort_label:                                                           \
    if(!eventname##triggered)                                           \
      mstro_event_destroy(eventname);                                   \
    pthread_mutex_unlock(&((eventname##closure).mtx));                  \
    pthread_cond_destroy(&((eventname##closure).cvar));                 \
    pthread_mutex_destroy(&((eventname##closure).mtx));                 \
  } while(0)


/** Fetch name of ID from resolver cache, possibly querying the PM to
 * find it */
static inline
mstro_status
mstro__subscr_resolver_lookup_cdoid(const struct mstro_cdo_id *id,
                                    const char **name)
{
  if(id==NULL)
    return MSTRO_INVARG;
  if(name==NULL)
    return MSTRO_INVOUT;

  *name = NULL;

  struct cdo_resolver_table_entry *e=NULL;
  WITH_LOCKED_RESOLVER_TABLE(
      struct cdo_resolver_table_entry *e;
      HASH_FIND(hh,g_cdo_resolver_table,
                id,sizeof(struct mstro_cdo_id), e);
      if(e) {
        *name = e->name;
      });

  if(*name)
    goto BAILOUT;

  WITH_CDO_ID_STR(idstr, id, {
      DEBUG("CDO ID %s not found in resolver cache, performing PM lookup\n",
            idstr);
    });

  Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
  cdoid.qw0 = id->qw[0];
  cdoid.qw1 = id->qw[1];
  cdoid.local_id = id->local_id;

  Mstro__Pool__Resolve resolve = MSTRO__POOL__RESOLVE__INIT;
  resolve.query_case = MSTRO__POOL__RESOLVE__QUERY_CDOID;
  resolve.cdoid = &cdoid;

  bool failure=false;
  WAIT_FOR_EVENT(event, g_resolver_event_domain, false, ABORT, {
      mstro_status s = mstro_event_id_get(event, &resolve.serial);
      if(s!=MSTRO_OK) {
        ERR("Failed to obtain event ID\n");
        failure=true;
        goto ABORT;
      }
      Mstro__Pool__MstroMsg msg=MSTRO__POOL__MSTRO_MSG__INIT;
      s = mstro_pmp_package(&msg, (ProtobufCMessage*)&resolve);
      if(s !=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            resolve.base.descriptor->name);
        failure=true;
        goto ABORT;
      } else {
        s = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
        if(s!=MSTRO_OK) {
          ERR("Cannot send resolve request to pool manager: %d (%s)\n",
              s, mstro_status_description(s));
          failure=true;
          goto ABORT;
        }
      }
      DEBUG("Entering wait for resolver lookup reply\n");
      /* setup done, time to wait. ResolveReply will be handled by
       * @ref mstro_pool_resolve_reply_bh will will trigger our
       * event */
    });
  if(failure)
    goto BAILOUT;

  /* retry */
  WITH_LOCKED_RESOLVER_TABLE(
      struct cdo_resolver_table_entry *e;
      HASH_FIND(hh,g_cdo_resolver_table,
                id, sizeof(struct mstro_cdo_id), e);
      if(e) {
        *name = e->name;
      });

BAILOUT:
  if(*name) {
    WITH_CDO_ID_STR(idstr, id, {
        DEBUG("Resolved CDO-ID %s to |%s|\n", idstr, *name);});
    return MSTRO_OK;
  } else {
    WITH_CDO_ID_STR(idstr, id, {
        ERR("Failed to find CDO ID %s, even after attempting resolve on PM\n",
            idstr);});
    return MSTRO_NOMATCH;
  }
}

mstro_status
mstro_pool_resolve_cdoid(const struct mstro_cdo_id *id,
                         const char **name)
{
  return mstro__subscr_resolver_lookup_cdoid(id, name);
}

static
void
mstro__subscr_local_event_handler(mstro_event e, void *ctx)
{
  mstro_status s;
  mstro_subscription sub = (mstro_subscription)ctx;

  int err=pthread_mutex_lock(&sub->event_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription mutex\n");
    s=MSTRO_FAIL;
    goto BAILOUT;
  }
  err=pthread_cond_broadcast(&sub->event_cond);
  if(err!=0) {
    ERR("Failed to broadcast on subscription\n");
    s=MSTRO_FAIL;
    goto BAILOUT_UNLOCK;
  }
  s=mstro_event_destroy(e);

BAILOUT_UNLOCK:
  err=pthread_mutex_unlock(&sub->event_mtx);
  if(err!=0) {
    ERR("Failed to unlock subscription mutex\n");
    s=MSTRO_FAIL;
  }
BAILOUT:
  if(s!=MSTRO_OK) {
    ERR("Failed to complete event handler properly: %d (%s)\n",
        s, mstro_status_description(s));
  }
  return;
}

static inline
mstro_status
mstro_subscription_register__local(mstro_subscription subscription,
                                   mstro_app_id origin)
{
  mstro_status status = MSTRO_UNIMPL;
  
  struct mstro_subscription_table_entry *e
      = malloc(sizeof(struct mstro_subscription_table_entry));
  if(!e) {
    status=MSTRO_NOMEM;
    goto BAILOUT;
  }
  e->subscriber = origin;
             
  e->subscription = subscription;
  subscription->refcount++;
  e->next = NULL;

  int err = pthread_mutex_lock(&g_subscription_table_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription table: %d (%s)\n", err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  assert(g_subscription_table.edom!=NULL);
  
  if(subscription->handle.id==MSTRO_SUBSCRIPTION_HANDLE_INVALID) {
    /* local-only subscriptions: add local handler */
    status = mstro_event_create(g_subscription_table.edom,
                                mstro__subscr_local_event_handler,
                                subscription,
                                NULL, false,
                                &e->subscription_event_handle);
    if(status!=MSTRO_OK) {
      ERR("Failed to create local event for subscription\n");
      goto BAILOUT_UNLOCK;
    }
    mstro_event_id id;
    status = mstro_event_id_get(e->subscription_event_handle, &id);
    if(status!=MSTRO_OK) {
      ERR("Failed to obtain local event id\n");
      goto BAILOUT_UNLOCK;
    }
    subscription->handle.id = id;
    
    INFO("Assigned local unique id %" PRIu64 " (event %p) to subscription %p from app %" PRIappid "\n",
         subscription->handle.id, e->subscription_event_handle, subscription, origin);
  } else {
    /* proxied subscriptions, handle assigned by PM */
    e->subscription_event_handle=NULL;
    DEBUG("Accepted pre-assigned id %" PRIu64 " for subscription %p\n",
          subscription->handle.id, subscription);
  }
  
  LL_PREPEND(g_subscription_table.entries, e);
  if(subscription->handle.id == UINT64_MAX) {
    ERR("FIXME: overflow of subscription table\n");
    status=MSTRO_UNIMPL;
  }


BAILOUT_UNLOCK:
  
  err = pthread_mutex_unlock(&g_subscription_table_mtx);
  if(err!=0) {
    ERR("Failed to unlock subscription table: %d (%s)\n", err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  status=MSTRO_OK;
  
BAILOUT:
  return status;
}


#define MSTRO_POOL_EVENT_VALID_MASK                                     \
  (MSTRO_POOL_EVENT_DECLARE    | MSTRO_POOL_EVENT_DECLARE_ACK           \
   | MSTRO_POOL_EVENT_SEAL     | MSTRO_POOL_EVENT_SEAL_GROUP            \
   | MSTRO_POOL_EVENT_OFFER    | MSTRO_POOL_EVENT_REQUIRE               \
   | MSTRO_POOL_EVENT_DEMAND   | MSTRO_POOL_EVENT_RETRACT               \
   | MSTRO_POOL_EVENT_WITHDRAW  | MSTRO_POOL_EVENT_DISPOSE              \
   | MSTRO_POOL_EVENT_TRANSPORT_INIT | MSTRO_POOL_EVENT_TRANSPORT_TICKET \
   | MSTRO_POOL_EVENT_TRANSPORT_COMPLETED                               \
   | MSTRO_POOL_EVENT_APP_JOIN  | MSTRO_POOL_EVENT_APP_WELCOME          \
   | MSTRO_POOL_EVENT_APP_LEAVE | MSTRO_POOL_EVENT_APP_BYE              \
   | MSTRO_POOL_EVENT_POOL_CHECKPOINT                                   \
   | MSTRO_POOL_EVENT_SUBSCRIBE | MSTRO_POOL_EVENT_UNSUBSCRIBE )

static inline
mstro_status
mstro_pool_event__translate_to_eventmsg(enum mstro_pool_event_kind e,
                                        Mstro__Pool__EventKind *ek)
{
  mstro_status status=MSTRO_OK;
  if(ek==NULL)
    return MSTRO_INVARG;
  switch(e) {
    case MSTRO_POOL_EVENT_DECLARE:
      *ek = MSTRO__POOL__EVENT_KIND__DECLARE;
      break;
    case MSTRO_POOL_EVENT_DECLARE_ACK:
      *ek = MSTRO__POOL__EVENT_KIND__DECLARE_ACK;
      break;
    case MSTRO_POOL_EVENT_SEAL:
      *ek = MSTRO__POOL__EVENT_KIND__SEAL;
      break;
    case MSTRO_POOL_EVENT_SEAL_GROUP:
      *ek = MSTRO__POOL__EVENT_KIND__SEAL_GROUP;
      break;
    case MSTRO_POOL_EVENT_OFFER:
      *ek = MSTRO__POOL__EVENT_KIND__OFFER;
      break;
    case MSTRO_POOL_EVENT_REQUIRE:
      *ek = MSTRO__POOL__EVENT_KIND__REQUIRE;
      break;
    case MSTRO_POOL_EVENT_WITHDRAW:
      *ek = MSTRO__POOL__EVENT_KIND__WITHDRAW;
      break;
    case MSTRO_POOL_EVENT_DEMAND:
      *ek = MSTRO__POOL__EVENT_KIND__DEMAND;
      break;
    case MSTRO_POOL_EVENT_RETRACT:
      *ek = MSTRO__POOL__EVENT_KIND__RETRACT;
      break;
    case MSTRO_POOL_EVENT_DISPOSE:
      *ek = MSTRO__POOL__EVENT_KIND__DISPOSE;
      break;
    case MSTRO_POOL_EVENT_TRANSPORT_INIT:
      *ek = MSTRO__POOL__EVENT_KIND__TRANSPORT_INIT;
      break;
    case MSTRO_POOL_EVENT_TRANSPORT_TICKET:
      *ek = MSTRO__POOL__EVENT_KIND__TRANSPORT_TICKET;
      break;
    case MSTRO_POOL_EVENT_TRANSPORT_COMPLETED:
      *ek = MSTRO__POOL__EVENT_KIND__TRANSFER_COMPLETED;
      break;
      
    case MSTRO_POOL_EVENT_APP_JOIN:
      *ek = MSTRO__POOL__EVENT_KIND__APP_JOIN;
      break;
    case MSTRO_POOL_EVENT_APP_WELCOME:
      *ek = MSTRO__POOL__EVENT_KIND__APP_WELCOME;
      break;
    case MSTRO_POOL_EVENT_APP_LEAVE:
      *ek = MSTRO__POOL__EVENT_KIND__APP_LEAVE;
      break;
    case MSTRO_POOL_EVENT_APP_BYE:
      *ek = MSTRO__POOL__EVENT_KIND__APP_BYE;
      break;
    case MSTRO_POOL_EVENT_SUBSCRIBE:
      *ek = MSTRO__POOL__EVENT_KIND__SUBSCRIBE;
      break;
    case MSTRO_POOL_EVENT_UNSUBSCRIBE:
      *ek = MSTRO__POOL__EVENT_KIND__UNSUBSCRIBE;
      break;
    case MSTRO_POOL_EVENT_POOL_CHECKPOINT:
      *ek = MSTRO__POOL__EVENT_KIND__CHECKPOINT;
      break;
    default:
      ERR("Unexpected event kind %d\n", e);
      status=MSTRO_FAIL;
      break;
  }
  return status;
}

static inline
mstro_status
mstro_subscription_register__with_pm(mstro_subscription subscription)
{
  mstro_status status = MSTRO_UNIMPL;
  
  if(subscription==NULL)
    return MSTRO_INVARG;
  
  unsigned int mask = subscription->event_mask & MSTRO_POOL_EVENT_VALID_MASK;
  size_t numevents = popcount(mask);
  if(numevents==0) {
    ERR("No events in subscription event mask\n");
    return MSTRO_INVARG;
  }

  Mstro__Pool__Subscribe smsg = MSTRO__POOL__SUBSCRIBE__INIT;
  smsg.require_event_ack = subscription->needs_ack;
  smsg.no_resolve = subscription->no_resolve;
  smsg.signal_before = subscription->signal_before;
  smsg.no_local = subscription->no_local;

  smsg.n_events = numevents;
  Mstro__Pool__EventKind events[numevents];
  /** compute events */
  for(size_t i=0; i<numevents; i++) {
    int bit = ffs(mask);
    assert(bit!=0);
    status = mstro_pool_event__translate_to_eventmsg(1<<(bit-1), events+i);
    if(status!=MSTRO_OK) 
      goto BAILOUT;
    mask ^= 1<<(bit-1); /* clear it */
  }
  smsg.events = events;

  Mstro__Pool__CDOSelector selector[1];
  Mstro__Pool__CDOSelector *selectorptr[1] = { &selector[0] };
  mstro__pool__cdoselector__init(&selector[0]);
  selector[0].sel_case = MSTRO__POOL__CDOSELECTOR__SEL_QUERY;
  if(subscription->cdo_selector)
    selector[0].query = subscription->cdo_selector->query;
  else
    selector[0].query = "";
  WARN("Beware, pool subscription ignore schema and namespace settings (FIXME)\n");

  smsg.n_selector = 1;
  smsg.selector = selectorptr;

  /* add to table of outstanding requests */
  struct outstanding_subscription *sub = calloc(1, sizeof(struct outstanding_subscription));
  if(sub==NULL) {
    return MSTRO_NOMEM;
  }
  sub->subscription = subscription;
  WITH_LOCKED_OUTSTANDING_SUBSCRIPTIONS({
      /* find free index */
      struct outstanding_subscription *tmp;
      do {
        sub->request_id = g_subscription_request_idx++;
        HASH_FIND(hh, g_outstanding_subscriptions,
                  &(sub->request_id), sizeof(sub->request_id),
                  tmp);
      } while(tmp!=NULL);
      HASH_ADD(hh, g_outstanding_subscriptions, request_id, sizeof(sub->request_id), sub);
    });

  /* we abuse the event lock to be notified by the incoming message
   * handler -- somehow it's an event, right? */
  int err=pthread_mutex_lock(&subscription->event_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription: %d (%s)\n", err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT_FREE;
  }

  smsg.local_id = sub->request_id;
  
  Mstro__Pool__MstroMsg msg=MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&smsg);
  if(status !=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        smsg.base.descriptor->name);
    goto BAILOUT;
  }

  DEBUG("Packaged subscribe message\n");
  
  status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
  if(status!=MSTRO_OK) {
    ERR("Cannot send subscription request to pool manager: %d (%s)\n",
        status, mstro_status_description(status));
    goto BAILOUT_FREE;
  }

  DEBUG("Waiting for pool manager to assign subscription handle\n");

  /* we still hold the lock on the subscription, so let's go to sleep
   * and wait what happens */
  do {
    mstro__abort_if_deadlock();
    err=pthread_cond_wait(&subscription->event_cond,
                          &subscription->event_mtx);
    if(err!=0) {
      ERR("Failed to sleep on cond var for subscription: %d (%s)\n",
          err, strerror(err));
      status = MSTRO_FAIL;
      goto BAILOUT_FREE;
    }
  } while(subscription->handle.id==MSTRO_SUBSCRIPTION_HANDLE_INVALID);

  DEBUG("Subscription %p has been assigned handle %" PRIu64 "\n",
        subscription, subscription->handle.id);
  
  /* ok, valid handle found */
  /* the outstanding subscription allocation is freed by the incoming
   * message handler */
  err=pthread_mutex_unlock(&subscription->event_mtx);
  if(err!=0) {
    ERR("Failed to release mutex for subscription: %d (%s)\n",
        err, strerror(err));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  status = MSTRO_OK;
  goto BAILOUT;
  
BAILOUT_FREE:
  WITH_LOCKED_OUTSTANDING_SUBSCRIPTIONS({
      if(sub) {
        HASH_DEL(g_outstanding_subscriptions, sub);
        free(sub);
      }});

BAILOUT:
  return status;  
}

static inline
mstro_status
mstro_subscription__destroy(mstro_subscription subscription)
{
  mstro_status s=MSTRO_OK;

  if(subscription==NULL) {
    ERR("NULL subscription\n");
    return MSTRO_INVARG;
  }
  assert(subscription->refcount>0);

  subscription->refcount--;
  if(subscription->refcount==0) {
    DEBUG("Subscription %p has reached refcount 0, killing it\n");
    if(subscription->event_list) {
      WARN("Unhandled events in subscription, dropping them\n");
      s = mstro_subscription_event_ack(subscription,
                                       subscription->event_list);
      if(s!=MSTRO_OK) {
        ERR("Failed to auto-acknowledge outstanding events: %d (%s)\n",
            s, mstro_status_description(s));
      }
      s = mstro_pool_event_dispose(subscription->event_list);
      if(s!=MSTRO_OK) {
        ERR("Failed to auto-dispose outstanding events: %d (%s)\n",
            s, mstro_status_description(s));
      }
    }

    WARN("Destroying subscription, but not terminating waiters, FIXME\n");
    int err = pthread_cond_destroy(&subscription->event_cond);
    if(err!=0) {
      ERR("Failed to destroy condvar: %d (%s)\n", err, strerror(err));
      s=MSTRO_FAIL;
    }
    err = pthread_mutex_destroy(&subscription->event_mtx);
    if(err!=0) {
      ERR("Failed to destroy mutex: %d (%s)\n", err, strerror(err));
      s=MSTRO_FAIL;
    }
    if(subscription->cdo_selector)
      s|= mstro_cdo_selector_dispose(subscription->cdo_selector);
    
    free(subscription);
  }
  return s;
}

static inline
mstro_status
mstro_subscription_unregister__local(mstro_subscription subscription)
{
  mstro_status status = MSTRO_OK;

  /* remove subscription from table */
  WITH_LOCKED_SUBSCRIPTIONS({
      struct mstro_subscription_table_entry *e, *tmp, *last;
      last=NULL;
      LL_FOREACH_SAFE(g_subscription_table.entries, e, tmp) {
        if(e->subscription==subscription) {
          DEBUG("Found subscription\n");
          if(last==NULL) {
            g_subscription_table.entries = e->next;
          } else {
            last->next = e->next;
          }
          e->next = NULL;
          /* disable handler invocations */
          if(e->subscription_event_handle!=NULL) {
            status = mstro_event_destroy(e->subscription_event_handle);
            if(status!=MSTRO_OK) {
              ERR("Failed to destroy event\n");
              break;
            }
          }

          /* Reset handle, then notify one last time. That will wake
           * up anyone who is waiting for the subscription to expire,
           * and let others clean up before we kill the mutex and cond
           * var */
          int err=pthread_mutex_lock(&subscription->event_mtx);
          if(err!=0) {
            ERR("Failed to acquire subscription mutex: %d (%s)\n",
                err, strerror(err));
            status |= MSTRO_FAIL;
            break;
          }
          DEBUG("Invalidating handle on subscription %p (was: %" PRIu64 ")\n",
                subscription, subscription->handle.id);
          subscription->handle.id = MSTRO_SUBSCRIPTION_HANDLE_INVALID;

          err=pthread_cond_broadcast(&subscription->event_cond);
          if(err!=0) {
            ERR("Failed to signal that subscription is to be destroyed: %d (%s)\n",
                err, strerror(err));
            status |= MSTRO_FAIL;
            pthread_mutex_unlock(&subscription->event_mtx);
            break;
          }

          err=pthread_mutex_unlock(&subscription->event_mtx);
          if(err!=0) {
            ERR("Failed to release subscription mutex: %d (%s)\n",
                err, strerror(err));
            status |= MSTRO_FAIL;
            break;
          }
          
          status |= mstro_subscription__destroy(subscription);
          if(status!=MSTRO_OK) {
            ERR("Failed to destroy subscription\n");
            break;
          }
          free(e);
          break; /* out of list traversal */
        } else {
          last = e;
        }
      }
    });
  return status;
}

/** Send deregistration message */
static inline
mstro_status
mstro_subscription_unregister__with_pm(mstro_subscription subscription)
{
  mstro_status status = MSTRO_UNIMPL;

  if(subscription==NULL)
    return MSTRO_INVARG;
  int err=pthread_mutex_lock(&subscription->event_mtx);
  if(err!=0) {
    ERR("Failed to lock subscription: %d (%s)\n", err, strerror(err));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

  assert(subscription->handle.id != MSTRO_SUBSCRIPTION_HANDLE_INVALID);
  Mstro__Pool__Unsubscribe smsg = MSTRO__POOL__UNSUBSCRIBE__INIT;
  smsg.subscription_handle = &subscription->handle;

  /* Send to PM; it will return a PoolOpAck for unsubscribe, which
   * will trigger our mstro_subscription_unregister_bh, which signals
   * an event for the subscription.  */
  
  Mstro__Pool__MstroMsg msg=MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&smsg);
  if(status !=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        smsg.base.descriptor->name);
    goto BAILOUT_UNLOCK;
  }

  DEBUG("Packaged unsubscribe message\n");
  
  status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
  if(status!=MSTRO_OK) {
    ERR("Cannot send subscription request to pool manager: %d (%s)\n",
        status, mstro_status_description(status));
    goto BAILOUT_UNLOCK;
  }
  
  DEBUG("Waiting for pool manager to confirm un-subscription\n");
  
  /* we still hold the lock on the subscription, so let's go to sleep
   * and wait what happens */
  do {
    mstro__abort_if_deadlock();
    err=pthread_cond_wait(&subscription->event_cond,
                          &subscription->event_mtx);
    if(err!=0) {
      ERR("Failed to sleep on cond var for subscription: %d (%s)\n",
          err, strerror(err));
      status = MSTRO_FAIL;
      goto BAILOUT_UNLOCK;
    }
  } while(subscription->handle.id!=MSTRO_SUBSCRIPTION_HANDLE_INVALID);

  DEBUG("Subscription %p has been canceled on PM\n",
        subscription);

BAILOUT_UNLOCK:
  err=pthread_mutex_unlock(&subscription->event_mtx);
  if(err!=0) {
    ERR("Failed to unlock subscription: %d (%s)\n",
        err, strerror(err));
    status = MSTRO_FAIL;
  }
  if(status==MSTRO_OK)
    status = mstro_subscription_unregister__local(subscription);

BAILOUT:
  return status;  
}  

mstro_status
mstro_subscription_unregister(mstro_subscription subscription)
{
  if(subscription==NULL) {
    ERR("NULL subscription\n");
    return MSTRO_INVARG;
  }

  if(atomic_load(&g_mstro_pm_attached)) {
    if(g_pool_app_id==MSTRO_APP_ID_MANAGER) {
      /* local registration on manager: assign a handle directly */
      return mstro_subscription_unregister__local(subscription);
    } else {
      assert(subscription->handle.id != MSTRO_SUBSCRIPTION_HANDLE_INVALID);
      /* remote registration needs to be discussed with PM */
      return mstro_subscription_unregister__with_pm(subscription);
    }
  } else {
    assert(subscription->handle.id != MSTRO_SUBSCRIPTION_HANDLE_INVALID);
    return mstro_subscription_unregister__local(subscription);
  }
}


/** @brief Add a subscription to local or pool manager subscription table */
mstro_status
mstro_subscription_register(mstro_subscription subscription)
{
  mstro_status status=MSTRO_UNIMPL;

  if(atomic_load(&g_mstro_pm_attached)) {
    if(g_pool_app_id==MSTRO_APP_ID_MANAGER) {
      assert(subscription->handle.id == MSTRO_SUBSCRIPTION_HANDLE_INVALID);
      /* local registration on manager: assign a handle directly */
      return mstro_subscription_register__local(subscription, g_pool_app_id);
    } else {
      assert(subscription->handle.id == MSTRO_SUBSCRIPTION_HANDLE_INVALID);
      /* remote registration will get handle assigned and stored in subscription */
      return mstro_subscription_register__with_pm(subscription);
    }
  } else {
    assert(subscription->handle.id == MSTRO_SUBSCRIPTION_HANDLE_INVALID);
    /* local registration without PM: assign handle directly */
    return mstro_subscription_register__local(subscription, g_pool_app_id);
  }
}

mstro_status
mstro_subscription_register_bh(const Mstro__Pool__SubscribeAck *ack)
{
  if(ack==NULL)
    return MSTRO_INVARG;

  mstro_status status=MSTRO_UNIMPL;

  /* find by local ID */
  struct outstanding_subscription s;
  s.request_id = ack->serial;
  struct outstanding_subscription *sub=NULL;
  WITH_LOCKED_OUTSTANDING_SUBSCRIPTIONS({
      HASH_FIND(hh,g_outstanding_subscriptions,
                &(s.request_id), sizeof(s.request_id), sub);
      if(!sub) {
        ERR("Cannot find local id " PRIu64 " among outstanding requests\n",
            ack->serial);
        status=MSTRO_FAIL;
      } else {
        HASH_DEL(g_outstanding_subscriptions, sub);
      }
    });
  if(sub) {
    /* sub is no longer outstanding */
    int err = pthread_mutex_lock(&sub->subscription->event_mtx);
    if(err!=0) {
      ERR("Failed to lock subscription mutex: %d (%s)\n",
          err, strerror(err));
      goto BAILOUT_FREE;
    }
    
    sub->subscription->handle.id = ack->handle->id;
    
    err = pthread_mutex_unlock(&sub->subscription->event_mtx);
    if(err!=0) {
      ERR("Failed to unlock subscription mutex: %d (%s)\n",
          err, strerror(err));
      goto BAILOUT_FREE;
    }
    
    status = mstro_subscription_register__local(sub->subscription, g_pool_app_id);
    if(status!=MSTRO_OK) {
      ERR("Failed to insert subscription into local registry\n");
    } else {
      err = pthread_mutex_lock(&sub->subscription->event_mtx);
      if(err!=0) {
        ERR("Failed to lock subscription mutex: %d (%s)\n",
            err, strerror(err));
        goto BAILOUT_FREE;
      }

      err = pthread_cond_broadcast(&sub->subscription->event_cond);
      if(err!=0) {
        ERR("Failed to signal subscription event: %d (%s)\n",
            err, strerror(err));
        goto BAILOUT_FREE;
      }
      
      err = pthread_mutex_unlock(&sub->subscription->event_mtx);
      if(err!=0) {
        ERR("Failed to unlock subscription mutex: %d (%s)\n",
            err, strerror(err));
        goto BAILOUT_FREE;
      }
    }
    
 BAILOUT_FREE:
    free(sub);
  }

BAILOUT:
  return status;
}


/* translate a list of protobuf event messages to a single event mask */
static inline
mstro_status
mstro_pool_event__eventmsgs_translate(size_t nevents,
                                      const Mstro__Pool__EventKind *events,
                                      mstro_pool_event_kinds *event_mask)
{
  if(event_mask==NULL)
    return MSTRO_INVOUT;
  if(events==NULL && nevents>0)
    return MSTRO_INVARG;

  mstro_pool_event_kinds res = MSTRO_POOL_EVENT_NONE;
  mstro_status s=MSTRO_UNIMPL;
  
  for(size_t i=0; i<nevents; i++) {
    switch(events[i]) {
      case MSTRO__POOL__EVENT_KIND__APP_JOIN:
        res |= MSTRO_POOL_EVENT_APP_JOIN;          break;
      case MSTRO__POOL__EVENT_KIND__APP_WELCOME:
        res |= MSTRO_POOL_EVENT_APP_WELCOME;       break;
      case MSTRO__POOL__EVENT_KIND__APP_LEAVE:
        res |= MSTRO_POOL_EVENT_APP_LEAVE;         break;
      case MSTRO__POOL__EVENT_KIND__APP_BYE:
        res |= MSTRO_POOL_EVENT_APP_BYE;           break;
      case MSTRO__POOL__EVENT_KIND__SUBSCRIBE:
        res |= MSTRO_POOL_EVENT_SUBSCRIBE;         break;
      case MSTRO__POOL__EVENT_KIND__UNSUBSCRIBE:
        res |= MSTRO_POOL_EVENT_UNSUBSCRIBE;       break;
      case MSTRO__POOL__EVENT_KIND__CHECKPOINT:
        res |= MSTRO_POOL_EVENT_POOL_CHECKPOINT;   break;
      case MSTRO__POOL__EVENT_KIND__DECLARE:
        res |= MSTRO_POOL_EVENT_DECLARE;           break;
      case MSTRO__POOL__EVENT_KIND__DECLARE_ACK:
        res |= MSTRO_POOL_EVENT_DECLARE_ACK;         break;
      case MSTRO__POOL__EVENT_KIND__SEAL:
        res |= MSTRO_POOL_EVENT_SEAL;                break;
      case MSTRO__POOL__EVENT_KIND__SEAL_GROUP:
        res |= MSTRO_POOL_EVENT_SEAL_GROUP;          break;
      case MSTRO__POOL__EVENT_KIND__OFFER:
        res |= MSTRO_POOL_EVENT_OFFER;               break;
      case MSTRO__POOL__EVENT_KIND__REQUIRE:
        res |= MSTRO_POOL_EVENT_REQUIRE;             break;
      case MSTRO__POOL__EVENT_KIND__WITHDRAW:
        res |= MSTRO_POOL_EVENT_WITHDRAW;            break;
      case MSTRO__POOL__EVENT_KIND__DEMAND:
        res |= MSTRO_POOL_EVENT_DEMAND;              break;
      case MSTRO__POOL__EVENT_KIND__RETRACT:
        res |= MSTRO_POOL_EVENT_RETRACT;             break;
      case MSTRO__POOL__EVENT_KIND__DISPOSE:
        res |= MSTRO_POOL_EVENT_DISPOSE;             break;
      case MSTRO__POOL__EVENT_KIND__TRANSPORT_INIT:
        res |= MSTRO_POOL_EVENT_TRANSPORT_INIT;      break;
      case MSTRO__POOL__EVENT_KIND__TRANSPORT_TICKET:
        res |= MSTRO_POOL_EVENT_TRANSPORT_TICKET;    break;
      case MSTRO__POOL__EVENT_KIND__TRANSFER_COMPLETED:
        res |= MSTRO_POOL_EVENT_TRANSPORT_COMPLETED; break;
      default:
        ERR("Unexpected pool protocol event kind: %d, ignoring it\n",
            events[i]);
    }
  }
  
  if(res & (~MSTRO_POOL_EVENT_VALID_MASK)) {
    ERR("Invalid event mask constructed, check protocol\n");
    s=MSTRO_INVARG;
  } else {
    s=MSTRO_OK;
    *event_mask = res;
  }
  
BAILOUT:
  return s;
}

/** @brief Add a pool manager message subscription message to subscription table */
mstro_status
mstro_subscription_message_register(const Mstro__Pool__Subscribe *smsg,
                                    mstro_app_id origin,
                                    Mstro__Pool__SubscriptionHandle **new_handle)
{
  if(smsg==NULL || origin==MSTRO_APP_ID_INVALID)
    return MSTRO_INVARG;

  if(new_handle==NULL)
    return MSTRO_INVOUT;

  mstro_status status = MSTRO_OK;
  mstro_subscription s = malloc(sizeof(struct mstro_subscription_));
  if(s==NULL)
    return MSTRO_NOMEM;

  /* FIXME: this has some duplication with mstro_subscribe body */
  int err = pthread_mutex_init(&s->event_mtx, NULL);
  if(err!=0) {
    ERR("Failed to initialize mutex: %d (%s)\n", err, strerror(err));
    free(s);
    status=MSTRO_FAIL;
    goto BAILOUT;
  }  
  err = pthread_cond_init(&s->event_cond, NULL);
  if(err!=0) {
    ERR("Failed to initialize cond var: %d (%s)\n", err, strerror(err));
    pthread_mutex_destroy(&s->event_mtx);
    free(s);
    status=MSTRO_FAIL;
    goto BAILOUT;
  }  
  
  s->refcount=1;
  mstro__pool__subscription_handle__init(&s->handle);

  if(smsg->n_selector!=1) {
    ERR("FIXME: multiple (or no) CDO selectors in one subscription not supported, have %zu\n",
        smsg->n_selector);
    status=MSTRO_UNIMPL;
    goto BAILOUT;
  }
  if(smsg->selector[0]->sel_case != MSTRO__POOL__CDOSELECTOR__SEL_QUERY) {
    ERR("FIXME: can only handle 'query' style cdo selectors in subscriptions\n");
    status=MSTRO_UNIMPL;
    goto BAILOUT;
  }
   
  status = mstro_cdo_selector_create(NULL, NULL,
                                     smsg->selector[0]->query,
                                     &s->cdo_selector);
  s->cdo_selector->refcount++;

  status = mstro_pool_event__eventmsgs_translate(smsg->n_events,
                                                 smsg->events,
                                                 &s->event_mask);
  s->needs_ack = smsg->require_event_ack;
  s->no_resolve = smsg->no_resolve;
  s->signal_before = smsg->signal_before;
  s->no_local = smsg->no_local;
  s->event_list = NULL;
  s->last_event = NULL;

  status = mstro_subscription_register__local(s, origin);
  if(status!=MSTRO_OK) {
    ERR("Failed to register subscription\n");
    goto BAILOUT;
  }
  
  *new_handle = &s->handle;
BAILOUT:
  return status;
}

/** @brief Remove a subscription from local or pool manager subscription table */
mstro_status
mstro_subscription_unregister_bh(const Mstro__Pool__SubscriptionHandle *h)
{
  if(h==NULL || h->id == MSTRO_SUBSCRIPTION_HANDLE_INVALID) {
    ERR("Invalid subscription handle\n");
    return MSTRO_INVMSG;
  }
  DEBUG("Incoming unsubscribe confirmation for subscription %" PRIu64 "\n",
        h->id);

  /* remove from local (client-side) subscription table */
  mstro_subscription sub=NULL;
  WITH_LOCKED_SUBSCRIPTIONS({
      struct mstro_subscription_table_entry *s=NULL;
      LL_FOREACH(g_subscription_table.entries, s) {
        if(s->subscription->handle.id == h->id) {
          break;
        }
      }
      if(s!=NULL) {
        DEBUG("Subscription %" PRIu64 " known, that's good\n",
              h->id);
        sub=s->subscription;
      } else {
        ERR("Unregister ack for unknown subscription %" PRIu64 "\n",
            h->id);
      }
    });
  if(sub!=NULL) {
    return mstro_subscription_unregister__local(sub);
  } else {
    return MSTRO_INVMSG;
  }
}


/** @brief Handle a pool manager message unsubscription message in table */
mstro_status
mstro_subscription_message_unregister(const Mstro__Pool__Unsubscribe *umsg,
                                      mstro_app_id origin)
{
  if(umsg==NULL || origin==MSTRO_APP_ID_INVALID)
    return MSTRO_INVARG;
  if(umsg->subscription_handle==NULL) {
    return MSTRO_INVARG;
  }

  /* find subscription */
  mstro_subscription sub=NULL;

  WITH_LOCKED_SUBSCRIPTIONS({
      struct mstro_subscription_table_entry *s=NULL;
      LL_FOREACH(g_subscription_table.entries, s) {
        if(s->subscription->handle.id == umsg->subscription_handle->id) {
          break;
        }
      }
      if(s!=NULL) {
        DEBUG("Subscription %" PRIu64 " known, that's good\n",
              s->subscription->handle.id);
        /* FIXME: We cannot check whether the sender of this request
         * is actually the subscriber, so currently anyone can
         * unsubscribe anyone else */
        sub=s->subscription;
      } else {
        ERR("Unregister request for unknown subscription %" PRIu64 "\n",
            umsg->subscription_handle->id);
      }
    });
  if(sub!=NULL) {
    return mstro_subscription_unregister__local(sub);
  } else {
    return MSTRO_INVMSG;
  }
}

/** @brief Handle acknowledgement of an event */
mstro_status
mstro_subscription_event_ack(mstro_subscription s, mstro_pool_event e)
{
  if(s==NULL || e==NULL)
    return MSTRO_INVARG;

  if(!s->needs_ack) {
    DEBUG("Subscription %" PRIu64 " does not set up for event acknowledgement, skipping\n",
        s->handle.id);
    return MSTRO_OK;
  }

  mstro_status status=MSTRO_OK;

  if(atomic_load(&g_mstro_pm_attached) && g_pool_app_id!=MSTRO_APP_ID_MANAGER) {
    /* send to PM */
    Mstro__Pool__EventAck ea = MSTRO__POOL__EVENT_ACK__INIT;
    Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
    while(e!=NULL) {
      ea.serial=e->serial;
      status = mstro_pmp_package(&msg, (ProtobufCMessage*)&ea);
      if(status !=MSTRO_OK) {
        ERR("Failed to package %s into a pool manager message\n",
            ea.base.descriptor->name);
        break;
      }
      status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
      if(status!=MSTRO_OK) {
        ERR("Cannot send event ack to PM: %d (%s)\n",
            status, mstro_status_description(status));
        break;
      }
      e=e->next;
    }
    /* send message to PM */
    status = MSTRO_OK;
  } else {
    /* local-only or local on PM */
    ERR("FIXME: local-only or PM-local event ack\n");
    return MSTRO_UNIMPL;
    /* INFO("local ack on subscription %p (id %" PRIu64 ")\n", */
    /*      s, s->handle.handle); */
    /* return mstro_event_trigger_by_id(s->handle.handle); */
  }

  return status;
}

/** @brief Handle acknowledgement message for an event */
mstro_status
mstro_subscription_message_event_ack(const Mstro__Pool__EventAck *msg)
{
  if(msg==NULL)
    return MSTRO_INVARG;

  INFO("Incoming ack on subscription, event id %" PRIu64 "\n",
       msg->serial);

  assert(g_subscription_table.edom!=NULL);
  void *result=NULL;
  return mstro_event_trigger_by_id(g_subscription_table.edom,
                                   msg->serial, &result);
}


static void
event_ack_cb(mstro_event event, void *ctx)
{
  assert(event!=NULL); assert(ctx!=NULL);
  _Atomic(uint64_t) *nr_outstanding_acks = (_Atomic(uint64_t) *) ctx;
  
  uint64_t nr_left = atomic_fetch_sub_explicit(nr_outstanding_acks,
                                               1, 
                                               memory_order_acquire);

  DEBUG("event ack on event %p, ctx %p, (pre-) nr_left=%" PRIu64 "\n",
        event, ctx, nr_left);
}


/** mask of the events that support a CDO selector */
#define MSTRO_POOL_EVENT_CDO_MASK (                      \
    MSTRO_POOL_EVENT_DECLARE                             \
    | MSTRO_POOL_EVENT_DECLARE_ACK                       \
    | MSTRO_POOL_EVENT_SEAL                              \
    | MSTRO_POOL_EVENT_SEAL_GROUP                        \
    | MSTRO_POOL_EVENT_OFFER                             \
    | MSTRO_POOL_EVENT_REQUIRE                           \
    | MSTRO_POOL_EVENT_WITHDRAW                          \
    | MSTRO_POOL_EVENT_DEMAND                            \
    | MSTRO_POOL_EVENT_RETRACT                           \
    | MSTRO_POOL_EVENT_DISPOSE                           \
    | MSTRO_POOL_EVENT_TRANSPORT_INIT                    \
    | MSTRO_POOL_EVENT_TRANSPORT_TICKET                  \
    | MSTRO_POOL_EVENT_TRANSPORT_COMPLETED)              \

/** extract the CDO ID from a CDO-Event */
static inline
mstro_status
mstro__pool_event_cdoid_get(const Mstro__Pool__Event *ev,
                            struct mstro_cdo_id *id)
{
  mstro_status s=MSTRO_OK;
  assert(ev!=NULL); assert(id!=NULL);

  Mstro__Pool__CDOID *cdoid=NULL;
  
  switch(ev->payload_case) {
    case MSTRO__POOL__EVENT__PAYLOAD_DECLARE:
      break;
    case MSTRO__POOL__EVENT__PAYLOAD_DECLARE_ACK:
      cdoid = ev->declare_ack->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_SEAL:
      cdoid = ev->seal->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_SEAL_GROUP:
      cdoid = ev->seal_group->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_OFFER:
      cdoid = ev->offer->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_REQUIRE:
      cdoid = ev->require->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_WITHDRAW:
      cdoid = ev->withdraw->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_DEMAND:
      cdoid = ev->demand->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_RETRACT:
      cdoid = ev->retract->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_DISPOSE:
      cdoid = ev->dispose->cdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_TRANSPORT_INIT:
      cdoid = ev->transport_init->dstcdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_TRANSPORT_TICKET:
      cdoid = ev->transport_ticket->dstcdoid; break;
    case MSTRO__POOL__EVENT__PAYLOAD_TRANSFER_COMPLETED:
      cdoid = ev->transfer_completed->dstcdoid; break;
      
    case MSTRO__POOL__EVENT__PAYLOAD_JOIN: /* fallthrough */
    case MSTRO__POOL__EVENT__PAYLOAD_WELCOME: /* fallthrough */
    case MSTRO__POOL__EVENT__PAYLOAD_LEAVE: /* fallthrough */
    case MSTRO__POOL__EVENT__PAYLOAD_BYE: /* fallthrough */
    case MSTRO__POOL__EVENT__PAYLOAD_SUBSCRIBE: /* fallthrough */
    case MSTRO__POOL__EVENT__PAYLOAD_UNSUBSCRIBE: /* fallthrough */
    case MSTRO__POOL__EVENT__PAYLOAD__NOT_SET:
      ERR("Event is not a CDO event (type %d) -- does not have CDO ID to extract\n",
          ev->payload_case);
      s=MSTRO_INVARG;
      break;
    default:
      ERR("Unexpected event type: %d\n", ev->payload_case);
      s=MSTRO_INVARG;
  }

  if(s==MSTRO_OK) {
    /* DECLARE may leave uuid NULL */
    if(cdoid) {
      id->qw[0] = cdoid->qw0;
      id->qw[1] = cdoid->qw1;
      id->local_id = cdoid->local_id;
    } else {
      *id = MSTRO_CDO_ID_NULL;
    }
  }
  
  return s;
}

static inline
const Mstro__Pool__KvEntry *
mstro_subscription__pool_attr_find(const Mstro__Pool__Attributes__Map *kv_map,
                                   const char *key)
{
  if(kv_map==NULL) {
    DEBUG("Empty kv map when looking for key |%s|\n", key);
    return NULL;
  } else {
    DEBUG("Parsing %d entries...\n", kv_map->n_map);
    for(size_t i=0; i<kv_map->n_map; i++) {
      if(strcmp(key,kv_map->map[i]->key)==0) {
        return kv_map->map[i];
      }
    }
    return NULL;
  }      
}

static inline
mstro_status
mstro_subscription__csq_eval(const struct mstro_csq_val *csq,
                             const Mstro__Pool__Attributes *attributes)
{
  assert(attributes!=NULL);
  if(attributes->val_case!=MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP) {
    ERR("Cannot handle non-KV-MAP attributes\n");
    return MSTRO_FAIL;
  }
  
  if(csq==NULL) {
    return MSTRO_OK;
  } else {
    switch(csq->kind) {
      case MSTRO_CSQ_OR: {
        const struct mstro_csq_val *tmp = csq->or.clauses;
        mstro_status s=MSTRO_NOMATCH; /* empty OR is false */
        while(tmp) {
          s = mstro_subscription__csq_eval(tmp, attributes);
          if(s==MSTRO_OK) {
            DEBUG("OR had a match\n");
            return s;
          } else {
            tmp=tmp->next;
          }
        }
        return s;
      }
        
      case MSTRO_CSQ_AND: {
        const struct mstro_csq_val *tmp = csq->and.terms;
        mstro_status s=MSTRO_OK; /* empty AND is true */
        while(tmp) {
          s = mstro_subscription__csq_eval(tmp, attributes);
          if(s!=MSTRO_OK) {
            DEBUG("AND rejected\n");
            return s;
          } else {
            tmp=tmp->next;
          }
        }
        return s;
      }

      case MSTRO_CSQ_OP_HAS: {
        bool complement = csq->has.not;
        const char *key = csq->has.key;
        const Mstro__Pool__KvEntry *entry=NULL;
        entry = mstro_subscription__pool_attr_find(attributes->kv_map, key);
        if(entry==NULL) {
          if(complement) {
            DEBUG("HAS-NOT matched for |%s|\n", key);
            return MSTRO_OK;
          } else {
            DEBUG("HAS did not match for |%s|\n", key);
            return MSTRO_NOMATCH;
          }
        } else {
          if(complement) {
            DEBUG("HAS-NOT did not match for |%s|\n", key);
            return MSTRO_NOMATCH;
          } else {
            DEBUG("HAS matched for |%s|\n", key);
            return MSTRO_OK;
          }
        }
      } 
        /* binary ops */
      case MSTRO_CSQ_OP_LT:       /* fallthrough */
      case MSTRO_CSQ_OP_LE:       /* fallthrough */
      case MSTRO_CSQ_OP_GT:       /* fallthrough */
      case MSTRO_CSQ_OP_GE:       /* fallthrough */
      case MSTRO_CSQ_OP_EQ:       /* fallthrough */
      case MSTRO_CSQ_OP_NEQ:      /* fallthrough */
      case MSTRO_CSQ_OP_RMATCH:   /* fallthrough */
      case MSTRO_CSQ_OP_RMATCH_ICASE: {
        const char *lhs = csq->kv_op.lhs;
        const char *rhs = csq->kv_op.rhs;
        const Mstro__Pool__KvEntry *entry=NULL;
        entry = mstro_subscription__pool_attr_find(attributes->kv_map, lhs);
        if(entry==NULL) {
          /* not-equal: always ok if no entry; others will always fail */
          if(csq->kind == MSTRO_CSQ_OP_NEQ) {
            DEBUG("empty entry for |%s|, NEQ matches\n", lhs);
            return MSTRO_OK;
          } else {
            DEBUG("empty entry for |%s| in CDO attributes, binop does not match\n", lhs);
            return MSTRO_NOMATCH;
          }
        } else {
          /* key matched. Now need to parse the value in the csq and
           * call the appropriate comparison function */
          const Mstro__Pool__AVal *aval = entry->val;
          void *rhsval=NULL;
          size_t rhsval_size=0;
          enum mstro_attribute_val_cmp_op cmp;
          switch(csq->kind) {
            case MSTRO_CSQ_OP_LT:     cmp = MSTRO_ATTR_VAL_CMP_LT; break;
            case MSTRO_CSQ_OP_LE:     cmp = MSTRO_ATTR_VAL_CMP_LE; break;
            case MSTRO_CSQ_OP_GT:     cmp = MSTRO_ATTR_VAL_CMP_GT; break;
            case MSTRO_CSQ_OP_GE:     cmp = MSTRO_ATTR_VAL_CMP_GE; break;
            case MSTRO_CSQ_OP_EQ:     cmp = MSTRO_ATTR_VAL_CMP_EQ; break;
            case MSTRO_CSQ_OP_NEQ:    cmp = MSTRO_ATTR_VAL_CMP_NEQ; break;
            case MSTRO_CSQ_OP_RMATCH: cmp = MSTRO_ATTR_VAL_CMP_RMATCH; break;
            case MSTRO_CSQ_OP_RMATCH_ICASE: cmp = MSTRO_ATTR_VAL_CMP_RMATCH_ICASE; break;
            default:
              /* not reached */
              ERR("Illegal csq kind\n");
              return MSTRO_FAIL;
          }
          bool result;
          mstro_status s = mstro_attribute_val_cmp_str(csq->kv_op.lhs_attr,
                                                       csq->kv_op.rhs, cmp, aval, &result);
          if(s!=MSTRO_OK) {
            ERR("Failed to parse rhs val according to schema key typespec\n");
            return MSTRO_FAIL;
          }
          if(result==true)
            return MSTRO_OK;
          else
            return MSTRO_NOMATCH;
        }
      }

      /* these should not be present in proper CSQ queries: */
      case MSTRO_CSQ_ERROR:
        ERR("Encountered ERROR csq in query\n");
        return MSTRO_FAIL;
      case MSTRO_CSQ_STRING:
        ERR("Encountered STRING csq in query\n");
        return MSTRO_FAIL;
      default:
        ERR("Unexpected csq kind %d in query\n", csq->kind);
        return MSTRO_FAIL;
    }
  }
  /* unreachable: */
  return MSTRO_FAIL;
}

/** Evaluate SEL as predicate over ATTRIBUTES for CDOID.
 *
 * This function is the shared part of local and pool-manager side cdo
 * selector checking. It works on the attributes message for
 * convenience, even though we may have the attribute dictionary for
 * local CDOs, or could construct it for the PM. If it becomes a
 * bottlenect this should be improvable.
 *
 * Return MSTRO_OK for 'matched', MSTRO_NOMATCH if not, or an error
 * code.
 * 
 */
mstro_status
mstro_subscription_selector_eval(const struct mstro_cdo_id *cdoid,
                                 const struct mstro_cdo_selector_ *sel,
                                 const Mstro__Pool__Attributes *attributes)
{
  if(cdoid==NULL || attributes==NULL)
    return MSTRO_INVARG;
  
  WITH_CDO_ID_STR(idstr, cdoid, {
      DEBUG("Checking selector %p for CDO ID %s\n", sel, idstr);});

  if(sel==NULL) {
    DEBUG("NULL selector: matches\n");
    return MSTRO_OK;
  }

  struct mstro_csq_val *csq = sel->csq;
  if(csq==NULL) {
    /* FIXME: maybe we need to create it on the first use of SEL if
     * we're on the PM? */
    ERR("No parsed query (CSQ) available\n");
    return MSTRO_FAIL;
  }
  return mstro_subscription__csq_eval(csq, attributes);
}
                                
/** check whether EV needs to be reported for the event selector in S.
 *
 * (Does not check the event kind matches).
 *
 *  Returns MSTRO_NOMATCH if not, MSTRO_OK if yes, or an error status
 */
static inline
mstro_status
mstro_subscription_selector_check(struct mstro_subscription_ *s,
                                  Mstro__Pool__Event *ev)
{
  assert(s!=NULL && ev!=NULL);
  assert(s->event_mask & MSTRO_POOL_EVENT_CDO_MASK);
  assert(ev->origin_id!=NULL);

  mstro_status status = MSTRO_NOMATCH;
  
  if((ev->payload_case==MSTRO__POOL__EVENT__PAYLOAD_DECLARE) || (ev->payload_case==MSTRO__POOL__EVENT__PAYLOAD_DISPOSE)){
    /* declare is special: no CDOID known yet, no attribuites, only
     * string name. Efficient subscriptions will subscribe to
     * DECLARE_ACK instead. */
    /**After dispose, we do not have an entry in the app registry for this cdo. */
    WARN("DECLARE and DISPOSE events always matches -- FIXME: only name could be checked (and we don't)\n");
    return MSTRO_OK;
  } else {
    /* we always need the CDO id. Unfortunately it's slightly buried in the EV */
    struct mstro_cdo_id id = MSTRO_CDO_ID_INVALID;
    status = mstro__pool_event_cdoid_get(ev, &id);
    if(status!=MSTRO_OK) {
      ERR("Failed to retrieve CDO id from event\n");
      return status;
    }
    
    /* fetch CDO attributes reference, local or PM */
    if(g_pool_app_id==MSTRO_APP_ID_MANAGER) {
      mstro_app_id origin = ev->origin_id->id;
      status = mstro_pm_cdo_app_match(origin, &id, s->cdo_selector);
    } else {
      
      Mstro__Pool__Attributes *attributes=NULL;
      status = mstro_cdo_attribute_msg_get(&id, &attributes);
      if(status!=MSTRO_OK) {
        ERR("Failed to serialized attribute message for CDO\n");
        return status;
      }
      status = mstro_subscription_selector_eval(&id, s->cdo_selector,
                                                attributes);
    }
  }
  return status;
}

  
/** @brief Let all subscriptions in @arg tab notice that event @arg kind/@arg iev/@arg data occured.
 *
 * If any of the notifications requires an ACK, this function will
 * block until all these ACKs have occured.
 *
 * When the notification phase (with acks, if appropriate) has
 * completed, it will trigger the continuation event @arg event.
 *
 * Local subscriptions will be handled directly, remote subscriptions
 * will receive event messages.
 */
mstro_status
mstro_pool_event_advertise(Mstro__Pool__Event *ev,
                           bool beforep,
                           _Atomic(uint64_t) *nr_outstanding_acks)
{
  mstro_status status = MSTRO_OK;

  if(ev==NULL) {
    ERR("NULL pool event data\n");
    return MSTRO_INVARG;
  }
  /* caller needs to set these */
  assert(ev->kind!=MSTRO__POOL__EVENT_KIND__INVALID_EVENT);
  assert(ev->payload_case!=MSTRO__POOL__EVENT__PAYLOAD__NOT_SET);
      
  assert(g_subscription_table.edom!=NULL);

  enum mstro_pool_event_kind kind;
  status=mstro_pool_event__eventmsgs_translate(1, &ev->kind, &kind);
  if(status!=MSTRO_OK) {
    ERR("Failed to translate event type: incoming %" PRIu64 "\n", ev->kind);
    return status;
  }
  
  WITH_LOCKED_SUBSCRIPTIONS({
      struct mstro_subscription_table_entry *s=NULL;

      LL_FOREACH(g_subscription_table.entries, s) {
        if(  (s->subscription->event_mask & kind)
             && (s->subscription->signal_before == beforep)) {
          assert(s->subscription);
          struct mstro_pm_app_registry_entry *app_entry_a, *app_entry_b;
          mstro_pm_app_lookup(ev->origin_id->id, &app_entry_a);
          mstro_pm_app_lookup(s->subscriber, &app_entry_b);
          if ( s->subscription->no_local 
            && !strcmp(app_entry_a->component_name,
                       app_entry_b->component_name) ) {
            DEBUG("Subscription flag says I'm not sending local (in the sense of the same application) event notification to appid %d (component `%s`) \n", s->subscriber, app_entry_b->component_name);
            continue;
          }

          DEBUG("Subscription %p from %" PRIu64 " matches event %d (beforep is %d)\n",
                s, s->subscriber, kind, beforep);
          if(s->subscription->event_mask & MSTRO_POOL_EVENT_CDO_MASK) {
            DEBUG("This is a CDO event, checking CDO selector\n");
            /* FIXME: Due to ev being a protobuf event structure this
             * is less efficient than it could be. We should instead
             * convert the event content to cdo, attributes, in
             * schema/dict format, and check against those for every
             * candidate subscription, rather than the other way
             * around */
            status = mstro_subscription_selector_check(s->subscription, ev);
            switch(status) {
              case MSTRO_NOMATCH:
                DEBUG("subscription selector rejected this event\n");
                status = MSTRO_OK; /*no problem we should continue with the operation*/
                continue;
              case MSTRO_OK:
                DEBUG("Subscription predicate matches, will notify\n");
                break;
              default:
                ERR("Failed to evaluate subscription predicate: %d (%s)\n",
                    status, mstro_status_description(status));
                goto UNLOCK; /**we should fail fast*/
            }
          }

          ev->subscription_handle = &s->subscription->handle;
          
          if(s->subscriber != MSTRO_APP_ID_MANAGER
             && s->subscriber != MSTRO_APP_ID_INVALID) {
            /* send via PMP */
            
            if(s->subscription->needs_ack) {
              uint64_t num_outstanding
                  = atomic_fetch_add_explicit(nr_outstanding_acks,
                                              1, memory_order_release);
              DEBUG("Now %" PRIu64 " outstanding acks for this event\n",
                    num_outstanding+1);

              mstro_event ack_event;
              status = mstro_event_create(g_subscription_table.edom,
                                          event_ack_cb, nr_outstanding_acks, NULL, true,
                                          &ack_event);
              if(status!=MSTRO_OK) {
                ERR("Failed to create event for subscription ack, skipping subscriber %" PRIu64 "\n",
                    s->subscriber);
                goto ABORT;
              }
              
              status = mstro_event_id_get(ack_event, &ev->serial);
              if(status!=MSTRO_OK) {
                ERR("Failed to obtain event serial for subscriber %" PRIu64 "\n");
                goto ABORT;
              }                            
              DEBUG("Subscriber %" PRIu64 " for subscription %p assigned event ack id %" PRIu64 "\n",
                    s->subscriber, s, ev->serial);
            } else {
              DEBUG("Subscriber %" PRIu64 " for subscription %p needs no ack id\n",
                    s->subscriber, s);
              ev->serial = 0;
            }
            
            Mstro__Pool__MstroMsg msg=MSTRO__POOL__MSTRO_MSG__INIT;
            status = mstro_pmp_package(&msg, (ProtobufCMessage*)ev);
            if(status !=MSTRO_OK) {
              ERR("Failed to package %s into a pool manager message\n",
                  ev->base.descriptor->name);
              goto ABORT;
            }
            status = mstro_pmp_send_nowait(s->subscriber, &msg);
            if(status!=MSTRO_OK) {
              ERR("Cannot send event notification to app %" PRIu64 ":  %d (%s)\n",
                  s->subscriber, status, mstro_status_description(status));
              goto ABORT;
            }
          } else {
            /* local-only or local to PM */
            if(s->subscription->needs_ack) {
              /* FIXME: store something on outstanding-ack table */
              
              ERR("Not waiting for ACK on local subscription %p\n",
                  s);
            }
            status = mstro_pool_event_consume(ev);
            if(status!=MSTRO_OK) {
              ERR("Cannot perform local event notification:  %d (%s)\n",
                  status, mstro_status_description(status));
              continue;
            }
          }
        }
        /* continue FOREACH */
        continue;
        /* abort target for cases where an ack setup failed */
     ABORT:
        atomic_fetch_sub_explicit(nr_outstanding_acks, 1, memory_order_release);  
      }
  UNLOCK:
  ; });

  DEBUG("Done informing subscribers\n");

BAILOUT:
  return status;
}


mstro_status
mstro_pool_event_consume(const Mstro__Pool__Event *eventmsg)
{
  assert(eventmsg!=NULL);

  mstro_status status=MSTRO_OK;

  /* perform as much work as possible outside lock, assuming that
   * typically events are going to match a subscription */
  struct mstro_pool_event_ *ev = malloc(sizeof(struct mstro_pool_event_));
  if(ev==NULL) {
    ERR("Failed to allocate event structure\n");
    return MSTRO_NOMEM;
  }

  status=mstro_pool_event__eventmsgs_translate(1, &eventmsg->kind, &ev->kind);
  if(status!=MSTRO_OK) {
    ERR("Failed to translate event type: incoming %" PRIu64 "\n",
        eventmsg->kind);
    return status;
  }
  DEBUG("Translated EventKind %d to pool_event_kind %d (%s)\n",
        eventmsg->kind, ev->kind, mstro_pool_event_description(ev->kind));
  ev->serial = eventmsg->serial;

  assert(eventmsg->ctime!=NULL);
  
  ev->ctime = (eventmsg->ctime->sec * NSEC_PER_SEC) + eventmsg->ctime->nsec;

  switch(ev->kind) {
    case MSTRO_POOL_EVENT_APP_JOIN:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_JOIN);
      ev->payload.join.component_name = strdup(eventmsg->join->component_name);
      if(ev->payload.join.component_name == NULL) {
        ERR("Failed to allocate event data\n");
        free(ev);
        return MSTRO_NOMEM;
      }
      if(eventmsg->origin_id)
        ev->payload.join.appid = eventmsg->origin_id->id; /* may be MSTRO_APP_ID_INVALID for JOIN:pre */
      else
        ev->payload.join.appid = MSTRO_APP_ID_INVALID;
        
      
      DEBUG("Event: %s JOINed (app %" PRIappid ")\n",
            ev->payload.join.component_name, ev->payload.join.appid);
      break;
      
    case MSTRO_POOL_EVENT_APP_LEAVE:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_LEAVE);
      ev->payload.leave.appid = eventmsg->origin_id->id;
      ev->payload.leave.component_name = strdup(eventmsg->leave->component_name);
      DEBUG("Event: %" PRIu64 " asked to LEAVE\n", ev->payload.leave.appid);
      break;
      
    case MSTRO_POOL_EVENT_DECLARE:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_DECLARE);
      assert(eventmsg->origin_id!=NULL
             && eventmsg->origin_id->id!=MSTRO_APP_ID_INVALID);
      ev->payload.declare.appid = eventmsg->origin_id->id;
      ev->payload.declare.cdo_name = strdup(eventmsg->declare->cdo_name);
      if(ev->payload.declare.cdo_name == NULL) {
        ERR("Failed to allocate event data\n");
        free(ev);
        return MSTRO_NOMEM;
      }
      DEBUG("Event: DECLARE for |%s| from %" PRIu64 "\n",
            ev->payload.declare.cdo_name, ev->payload.declare.appid);
      break;

    case MSTRO_POOL_EVENT_OFFER:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_OFFER);
      assert(eventmsg->origin_id!=NULL
             && eventmsg->origin_id->id!=MSTRO_APP_ID_INVALID);
      assert(eventmsg->offer->cdoid!=NULL);
      ev->payload.offer.appid = eventmsg->origin_id->id;
      /* struct mstro_cdo_id id = { */
      /*   .qw[0] = eventmsg->offer->cdoid->qw0, */
      /*   .qw[1] = eventmsg->offer->cdoid->qw1, */
      /*   .local_id = MSTRO_CDO_LOCAL_ID_NONE */
      /*   // FIXME: Why do we not set the actual local_id? (I don't remember --uuh) */
      /*   //, .local_id = eventmsg->offer->cdoid->local_id; */
      /* }; */
      /* we cannot call the resolver in here, as that would lock up
       * our thread in the PC. But CDO-related events should have a
       * NAME value */
      if(eventmsg->cdo_name==NULL) {
        ERR("OFFER event missing a CDO name\n");
        free(ev);
        return MSTRO_FAIL;
      } else {
        ev->payload.offer.cdo_name = strdup(eventmsg->cdo_name);
        if(ev->payload.declare.cdo_name == NULL) {
          ERR("Failed to allocate event data\n");
          free(ev);
          return MSTRO_NOMEM;
        }
      }
      DEBUG("Event: OFFER for |%s| from %" PRIu64 "\n",
              ev->payload.offer.cdo_name, ev->payload.offer.appid);
      break;
      
    case MSTRO_POOL_EVENT_SEAL:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_SEAL);
      assert(eventmsg->origin_id!=NULL
             && eventmsg->origin_id->id!=MSTRO_APP_ID_INVALID);
      assert(eventmsg->seal->cdoid!=NULL);
      ev->payload.seal.appid = eventmsg->origin_id->id;
      if(eventmsg->cdo_name==NULL) {
        ERR("SEAL event missing a CDO name\n");
        free(ev);
        return MSTRO_FAIL;
      } else {
        ev->payload.seal.cdo_name = strdup(eventmsg->cdo_name);
        if(ev->payload.seal.cdo_name == NULL) {
          ERR("Failed to allocate event data\n");
          free(ev);
          return MSTRO_NOMEM;
        }
      }
      mmbLayout* layout = NULL; 
      mstro_status  s = mstro_attribute_pool_find_dist_layout(eventmsg->seal->attributes, &layout);
      if (s != MSTRO_OK) {
        ERR("Failed to read attributes in SEAL pool message\n");
        return s;
      }
      if (layout == NULL) {
        if (eventmsg->seal->attributes==NULL)
           ev->payload.seal.total_size = -1;
        else   
          ev->payload.seal.total_size = mstro_attribute_msg_get_totalsize(eventmsg->seal->attributes);
        ev->payload.seal.n_participants = 1;
      } else {
        ev->payload.seal.total_size = mstro_mmbLayout_get_totalsize(layout);
        ev->payload.seal.n_participants = layout->irregular.n_blocks.d[0];
      }
      DEBUG("Event: SEAL for |%s| from %" PRIu64 "\n",
            ev->payload.seal.cdo_name, ev->payload.seal.appid);
      break;
      
    case MSTRO_POOL_EVENT_REQUIRE:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_REQUIRE);
      assert(eventmsg->origin_id!=NULL
             && eventmsg->origin_id->id!=MSTRO_APP_ID_INVALID);
      assert(eventmsg->require->cdoid!=NULL);
      ev->payload.require.appid = eventmsg->origin_id->id;
      if(eventmsg->cdo_name==NULL) {
        ERR("REQUIRE event missing a CDO name\n");
        free(ev);
        return MSTRO_FAIL;
      } else {
        ev->payload.require.cdo_name = strdup(eventmsg->cdo_name);
        if(ev->payload.require.cdo_name == NULL) {
          ERR("Failed to allocate event data\n");
          free(ev);
          return MSTRO_NOMEM;
        }
      }
      
      DEBUG("Event: REQUIRE for |%s| from %" PRIu64 "\n",
            ev->payload.require.cdo_name, ev->payload.require.appid);
      break;

    case MSTRO_POOL_EVENT_DEMAND:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_DEMAND);
      assert(eventmsg->origin_id!=NULL
             && eventmsg->origin_id->id!=MSTRO_APP_ID_INVALID);
      assert(eventmsg->demand->cdoid!=NULL);
      ev->payload.demand.appid = eventmsg->origin_id->id;
      if(eventmsg->cdo_name==NULL) {
        ERR("DEMAND event missing a CDO name\n");
        free(ev);
        return MSTRO_FAIL;
      } else {
        ev->payload.demand.cdo_name = strdup(eventmsg->cdo_name);
        if(ev->payload.demand.cdo_name == NULL) {
          ERR("Failed to allocate event data\n");
          free(ev);
          return MSTRO_NOMEM;
        }
      }
      
      DEBUG("Event: DEMAND for |%s| from %" PRIu64 "\n",
            ev->payload.demand.cdo_name, ev->payload.demand.appid);
      break;
    case MSTRO_POOL_EVENT_WITHDRAW:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_WITHDRAW);
      assert(eventmsg->origin_id!=NULL
             && eventmsg->origin_id->id!=MSTRO_APP_ID_INVALID);
      assert(eventmsg->withdraw->cdoid!=NULL);
      ev->payload.withdraw.appid = eventmsg->origin_id->id;

      if(eventmsg->cdo_name==NULL) {
        ERR("WITHDRAW event missing a CDO name\n");
        free(ev);
        return MSTRO_FAIL;
      } else {
        ev->payload.withdraw.cdo_name = strdup(eventmsg->cdo_name);
        if(ev->payload.withdraw.cdo_name == NULL) {
          ERR("Failed to allocate event data\n");
          free(ev);
          return MSTRO_NOMEM;
        }
      }
      
      DEBUG("Event: WITHDRAW for |%s| from %" PRIu64 "\n",
            ev->payload.withdraw.cdo_name, ev->payload.withdraw.appid);
      break;

    case MSTRO_POOL_EVENT_APP_BYE:
      assert(eventmsg->payload_case==MSTRO__POOL__EVENT__PAYLOAD_BYE);
      ev->payload.bye.appid = eventmsg->origin_id->id;
      DEBUG("Event: %" PRIu64 " granted BYE\n", ev->payload.bye.appid);
      break;

    case MSTRO_POOL_EVENT_SEAL_GROUP:
    
    case MSTRO_POOL_EVENT_RETRACT:
    case MSTRO_POOL_EVENT_DISPOSE:
    case MSTRO_POOL_EVENT_TRANSPORT_INIT:
    case MSTRO_POOL_EVENT_TRANSPORT_TICKET:
    case MSTRO_POOL_EVENT_TRANSPORT_COMPLETED:
      
        /* pool-related */
    case MSTRO_POOL_EVENT_POOL_CHECKPOINT:
    case MSTRO_POOL_EVENT_SUBSCRIBE:
    case MSTRO_POOL_EVENT_UNSUBSCRIBE:
      WARN("Unhandled event kind: %d (%s)\n",
           ev->kind, mstro_pool_event_description(ev->kind));
      free(ev);
      return MSTRO_UNIMPL;
    default:
      ERR("Unknown event kind %d\n", ev->kind);
      free(ev);
      return MSTRO_FAIL;
  }
  
        
  /* check that we know about the subscription */
  WITH_LOCKED_SUBSCRIPTIONS({
      struct mstro_subscription_table_entry *s=NULL;
      LL_FOREACH(g_subscription_table.entries, s) {
        if(s->subscription->handle.id == eventmsg->subscription_handle->id) {
          break;
        }
      }
      if(s!=NULL) {
        /* append local event information and notify listeners */
        DEBUG("Subscription %" PRIu64 " known, that's good\n",
              s->subscription->handle.id);
        DEBUG("Not checking event details against subscription (trusting PM)\n");
        int err = pthread_mutex_lock(&s->subscription->event_mtx);
        if(err!=0) {
          ERR("Failed to lock event mutex of subscription: %d (%s)\n",
              err, strerror(err));
          status = MSTRO_FAIL;
        } else {
          ev->next = NULL;
          /* append in O(1) */
          if(!s->subscription->event_list) {
            s->subscription->event_list = ev;
            s->subscription->last_event = ev;
          } else {
            assert(s->subscription->last_event!=NULL);
            s->subscription->last_event->next = ev;
            s->subscription->last_event = ev;
          }
          err = pthread_cond_broadcast(&s->subscription->event_cond);
          if(err!=0) {
            ERR("Failed to broadcast event cond: %d (%s)\n",
                err, strerror(err));
            status = MSTRO_FAIL;
          } else {
            err = pthread_mutex_unlock(&s->subscription->event_mtx);
            if(err!=0) {
              ERR("Failed to unlock event mutex of subscription: %d (%s)\n",
                  err, strerror(err));
              status = MSTRO_FAIL;
            }
            /* ok, we made it */
            DEBUG("Added event to subscription and signaled\n");
            status=MSTRO_OK;
          }
        }
      } else {
        ERR("Event notification does not match a locally known subscription\n");
        mstro_pool_event_dispose(ev);
        status = MSTRO_FAIL;
      }
    });
      
  return status;
}


mstro_status
mstro_subscriptions_init(void)
{
  mstro_status status=MSTRO_OK;
  
  WITH_LOCKED_SUBSCRIPTIONS(
      if(g_subscription_table.edom==NULL) {
        status = mstro_event_domain_create("Subscription Events",
                                           &g_subscription_table.edom);
        if(status!=MSTRO_OK) {
          ERR("Failed to create event domain\n");
        }
      }
                            );
  if(status==MSTRO_OK) {
    status = mstro_event_domain_create("Resolver events",
                                       &g_resolver_event_domain);
  }
  return status;
}

mstro_status
mstro_subscriptions_finalize(void)
{
  mstro_status status=MSTRO_OK;
  
  DEBUG("Not properly de-initializing subscription registry\n");

  WITH_LOCKED_OUTSTANDING_SUBSCRIPTIONS(
      if(HASH_COUNT(g_outstanding_subscriptions)>0) {
        ERR("Outstanding subscriptions at finalization time\n");
      });

  bool done=false;
  while(!done) {
    mstro_status s1;
    /* this is not super efficient, but better safe than sorry. We
     * pick up one, go through the normal unsubscribe machinery, and
     * repeat. */
    mstro_subscription s=NULL;
    WITH_LOCKED_SUBSCRIPTIONS({
        if(g_subscription_table.entries==NULL) {
          done=true;
          s=NULL;
        } else {
          s = g_subscription_table.entries->subscription;
          /* in case user code asynchronously drops the subscription: */
          s->refcount++;
          WARN("Live subscription %p at finalization time\n", s);
        }
      });

    if(s) {
      /* remove from table and refcount down once */
      s1 = mstro_subscription_unregister(s);
      if(s1!=MSTRO_OK) {
        ERR("Failed to unregister subscription %p\n", s);
        status|=s1;
      }
      /* drop our extra refcount from above */
      s1 = mstro_subscription__destroy(s);
      if(s1!=MSTRO_OK) {
        ERR("Failed to dispose subscription %p: %d (%s)\n",
            s, s1, mstro_status_description(s1));
        status|=s1;
      }
    }
    if(status!=MSTRO_OK) {
      break; /* avoid endless retries */
    }
  }

  /* now all subscriptions should be gone */
  status|=mstro_event_domain_destroy(g_subscription_table.edom);
  g_subscription_table.edom=NULL; /* in case we get re-initialized */

  status|= mstro_event_domain_destroy(g_resolver_event_domain);
  g_resolver_event_domain=NULL;

  /* we collected errors by |= , so we need to normalize to MSTRO_FAIL */
  if(status!=MSTRO_OK) {
    ERR("Failed to de-initialize subscriptions subsystem\n");
    status=MSTRO_FAIL;
  }
  return status;
}

/* called on pool client to handle resolver reply */
mstro_status
mstro_pool_resolve_reply_bh(const Mstro__Pool__ResolveReply *reply)
{
  DEBUG("Resolver reply received\n");
  if(reply==NULL ||reply->query==NULL) {
    return MSTRO_INVMSG;
  }
  switch(reply->query->query_case) {
    case MSTRO__POOL__RESOLVE__QUERY_CDOID:
      /* ok */
      break;
    default:
      ERR("Unhandled resolve reply type: %d\n",
          reply->query->query_case);
      return MSTRO_UNIMPL;
  }

  if(reply->result_case!=MSTRO__POOL__RESOLVE_REPLY__RESULT_NAME) {
    ERR("No name in resolver reply\n");
    return MSTRO_INVMSG;
  }
  
  struct mstro_cdo_id id = {.qw[0] = reply->query->cdoid->qw0,
                            .qw[1] = reply->query->cdoid->qw1,
                            //, .local_id = reply->query->cdoid->local_id
  };
  

  mstro_status s= mstro__subscr_resolver_notice_cdoid(reply->name, &id);
  if(s!=MSTRO_OK) {
    ERR("Failed to cache resolver reply\n");
    return s;
  }
#define MSTRO_SERIAL_INVALID ((uint64_t)0)
  if(reply->query->serial!=MSTRO_SERIAL_INVALID) {
    /* trigger event */
    void *result=NULL;
    s = mstro_event_trigger_by_id(g_resolver_event_domain,
                                  reply->query->serial,
                                  &result);
  }
  return s;
}

