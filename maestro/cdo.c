/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO public API implementation
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/pool.h"
#include "maestro/i_cdo.h"
#include "maestro/i_memlock.h"
#include "maestro/i_memory.h"
#include "maestro/i_attributes.h"
#include "maestro/logging.h"

#include "maestro/i_pool.h"
#include "maestro/i_globals.h"
#include "maestro/i_pool_manager_protocol.h"

#include "maestro/i_pool.h"

#include "maestro/status.h"
#include "maestro/i_statistics.h"
#include "maestro/i_uthash.h"
#include "i_maestro_numa.h"

#include "transformation/transformation.h"
#include <mamba.h>

#include <string.h>
#include <assert.h>
#include <inttypes.h>

#include <stdatomic.h>
#include <pthread.h>

#include "i_subscription_registry.h"


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)

#define MAX(x,y) ((x)>(y)? (x) : (y))

/** counter (per-application) that is used to identify CDO
 * declarations and distinguish multiple handles for the same CDO
 * names */
static _Atomic uint64_t g_cdo_serial_counter = (MAX(MSTRO_CDO_LOCAL_ID_NONE, MSTRO_CDO_LOCAL_ID_UNDCL) + 1);

/** a table of oustanding CDO declarations */
/* note: this is a bit too clever maybe -- we use the hh hash slot of
 * the CDO with the serial as key while it's outstanding; later the
 * CDO hash table will use the cdoid of course. */
static mstro_cdo g_outstanding_cdo_decl_table = NULL;

/** lock protecting g_outstanding_cdo_decl_table */
static pthread_mutex_t g_outstanding_cdo_decl_table_mtx = PTHREAD_MUTEX_INITIALIZER;


/* FIXME: This could be avoided if we used a second hash handle in the
 * cdo data structure. That would also save some memory; on the other
 * hand the cdo structure is already big enough to be split into
 * primary/secondary ... */
/** hash table entry to lookup CDOs by ID */
struct cdo_table_entry {
  UT_hash_handle hh; /**< hashable, by cdo ID */
  struct mstro_cdo_id cdoid; /**< key for lookup */
  mstro_cdo cdo; /**< the CDO */
};

/** a table keeping track of all CDOs by global ID assignment until
 * disposal */
struct cdo_table_entry *g_live_cdo_table = NULL;

/** lock protecting g_live_cdo_table */
static pthread_mutex_t g_live_cdo_table_mtx = PTHREAD_MUTEX_INITIALIZER;

/** Sometimes code needs to block until a CDO changes state.
 **
 ** We optimize for the common case here where the number of threads
 ** that would be blocking on state change waits is small (likely it's
 ** always only very few: SEAL is the prime candiate, so only heavily
 ** multithreaded user code with many blocking SEALs would be
 ** different).
 **
 ** A state change will be signaled to all waiters via broadcast on
 ** the condition variable @ref g_cdo_state_change_cvar. The waiter
 ** has a CDO in mind, can quickly check whether it's his (likely,
 ** since few waiters exist), or go back to wait immediately.
 **
 ** This saves a lock&cvar per CDO.
 **/
static pthread_cond_t g_cdo_state_change_cvar = PTHREAD_COND_INITIALIZER;

/** mutex for @ref g_cdo_state_change_cvar */
static pthread_mutex_t g_cdo_state_change_lock = PTHREAD_MUTEX_INITIALIZER;

/** Wait for CDO state change. Spurious returns may happen,
 * caller must check state explicitly.  */
mstro_cdo_state
wait_for_cdo_state_change(mstro_cdo cdo, mstro_cdo_state old_state)
{

  mstro__abort_if_deadlock();
      
  pthread_mutex_lock(&g_cdo_state_change_lock);
  mstro_cdo_state state = atomic_load(&cdo->state);
  if (old_state == state)
	 pthread_cond_wait(&g_cdo_state_change_cvar, &g_cdo_state_change_lock);
  pthread_mutex_unlock(&g_cdo_state_change_lock);
  return atomic_load(&cdo->state);
}

/* FIXME: CDO memory management should use a pool and recycling instead of individual alloc/free */
static inline
mstro_cdo
mstro_cdo__alloc(void)
{
  mstro_cdo res = malloc(sizeof(struct mstro_cdo_));
  if(res==NULL) {
    ERR("Cannot allocate a CDO\n");
    abort();
  }
  /* we need to zero out the hash-table relevant parts. For simplicity we zero everything */
  memset(res, 0, sizeof(struct mstro_cdo_));

  /* default: same region as handle is in */
  res->preferred_numa_node = mstro_numa__node_for_addr(res);

  /* initialize parts that need special handling */
  atomic_init(&(res->state), MSTRO_CDO_STATE_INVALID);

  /* add attribute and default namespace */
  res->current_namespace = strdup(MSTRO_CDO_ATTR_NAMESPACE_DEFAULT);
  if (res->current_namespace == NULL) {
    ERR("Cannot allocate for default CDO namespace\n");
    free(res);
    abort();
  }
  mstro_status s = mstro_attribute_dict_set_defaults(
      g_mstro_core_schema_instance, false, &(res->attributes));

  if(s!=MSTRO_OK) {
    ERR("Cannot set default attribute dict, aborting: %d (%s)\n",
        s, mstro_status_description(s));
    abort();
  }

  return res;
}

static inline
mstro_status
mstro_cdo__free(mstro_cdo *cdoptr)
{
  mstro_status status=MSTRO_OK;

  DEBUG("cdoptr %p, cdo %p, name %s\n", cdoptr, *cdoptr, (*cdoptr)->name);
  assert(cdoptr!=NULL);
  assert(*cdoptr!=NULL);
  if((*cdoptr)->name)
    free((*cdoptr)->name);

  if((*cdoptr)->attributes_msg) {
     mstro__pool__attributes__free_unpacked((*cdoptr)->attributes_msg,NULL);
  }

  status = mstro_attribute_dict_dispose((*cdoptr)->attributes);
  if(status!=MSTRO_OK) {
    ERR("Failed to destroy CDO attribute table\n");
    goto BAILOUT;
  }

  if((*cdoptr)->current_namespace)
    free((*cdoptr)->current_namespace);

  if((*cdoptr)->mamba_array) {
    mmbError stat = mmb_array_destroy((*cdoptr)->mamba_array);
    if(stat != MMB_OK) {
      ERR("Failed to free mamba array: %d (%s)\n",
          stat, mmb_error_description(stat));
      status = MSTRO_FAIL;
      goto BAILOUT;
    }
  }
  /* FIXME: if we had mamba-only CDOs or mamba could be told to own
   * the raw-ptr (created during RECEIVE into a CDO that has no
   * storage allocated) we would not need this: */
  if((*cdoptr)->core_owned_raw_ptr) {
    NOISE("Freeing maestro-provided raw-ptr\n");
    free((*cdoptr)->raw_ptr);
  } else {
    NOISE("Not freeing raw-ptr (user-provided)\n");
  }

  free(*cdoptr);
  *cdoptr=NULL;
BAILOUT:
  return status;
}

/*** CDO state handling and checking ***/

/** check whether CDO has state s. Can be called without locking CDO */
bool
mstro_cdo_state_check(mstro_cdo cdo, mstro_cdo_state s)
{
  mstro_cdo_state state = atomic_load(&cdo->state);
  return (state&s)!=0;
}

/** check whether state transition is valid */
static inline
bool
mstro_pc_cdo__valid_state_transition(const struct mstro_cdo_id *cdoid,
                                              mstro_cdo_state old_state,
                                              mstro_cdo_state new_state)
{
  assert(cdoid!=NULL);
  char *msg=NULL;
  bool unchecked=false;
  
  switch(new_state) {
    case MSTRO_CDO_STATE_INVALID:
      msg = "Cannot transition to INVALID state on PC";
      break;
      
    case MSTRO_CDO_STATE_CREATED:
      if(  old_state != MSTRO_CDO_STATE_INVALID 
        && !(old_state&MSTRO_CDO_STATE_DISPOSABLE))
        msg = "Cannot step to CREATED state from any other valid state, unless in case of dispose-and-reuse";
      break;
      
    case MSTRO_CDO_STATE_DECLARED:
      if(old_state!=MSTRO_CDO_STATE_CREATED)
        msg = "Cannot DECLARE unless freshly CREATED";
      break;
      
    case MSTRO_CDO_STATE_SEALED:
      if(old_state!=MSTRO_CDO_STATE_DECLARED)
        msg = "Cannot SEAL unless freshly DECLARED";
      break;
      
    case MSTRO_CDO_STATE_OFFERED: 
      if(old_state != MSTRO_CDO_STATE_OFFERED_LOCALLY)
        msg = "Cannot OFFER unless OFFERED_LOCALLY";
      break;

    case MSTRO_CDO_STATE_OFFERED_LOCALLY: 
      if(old_state != MSTRO_CDO_STATE_SEALED)
        msg = "Cannot OFFER_LOCALLY unless properly SEALED";
      break;

    case MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY:
      if(old_state != MSTRO_CDO_STATE_OFFERED)
        msg = "Cannot WITHDRAW_GLOBALLY unless OFFERED (and no other flags set)";
      break;

    case MSTRO_CDO_STATE_WITHDRAWN:
      if(old_state != MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY)
        msg = "Cannot WITHDRAW unless WITHDRAWN_GLOBALLY";
      break;

    case MSTRO_CDO_STATE_REQUIRED_LOCALLY:
      if(old_state != MSTRO_CDO_STATE_DECLARED
       &&old_state != MSTRO_CDO_STATE_SEALED)
        msg = "Cannot REQUIRE unless DECLARED/SEALED";
      break;

    case MSTRO_CDO_STATE_REQUIRED:
      if(old_state != MSTRO_CDO_STATE_REQUIRED_LOCALLY)
        msg = "Cannot flag REQUIRED as IN_TRANSPORT unless REQUIRED";
      break;

    case MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_IN_TRANSPORT:
      if(old_state != MSTRO_CDO_STATE_REQUIRED)
        msg = "Cannot flag REQUIRED as IN_TRANSPORT unless REQUIRED";
      break;

    case MSTRO_CDO_STATE_OFFERED|MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_SATISFIED:
      if(! (old_state == (MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_IN_TRANSPORT)))
	msg = "Cannot set OFFERED|REQUIRED|SATISFIED unless REQUIRED|IN_TRANSPORT";
      break;

    case MSTRO_CDO_STATE_DEMANDED:
      if(old_state != (MSTRO_CDO_STATE_REQUIRED)
       &&old_state != (MSTRO_CDO_STATE_OFFERED|MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_SATISFIED))
	msg = "Cannot set DEMANDED+IN-TRANSPORT unless REQUIRED (and no other flags set)";
      break;
    
    case MSTRO_CDO_STATE_DEMANDED|MSTRO_CDO_STATE_IN_TRANSPORT:
      if(! (old_state == (MSTRO_CDO_STATE_REQUIRED)))
	msg = "Cannot set DEMANDED+IN-TRANSPORT unless REQUIRED (and no other flags set)";
      break;

    case MSTRO_CDO_STATE_RETRACTED_GLOBALLY:
      if(! (   (old_state == MSTRO_CDO_STATE_REQUIRED)
            || (old_state == (MSTRO_CDO_STATE_REQUIRED|MSTRO_CDO_STATE_IN_TRANSPORT))))
        msg = "Cannot set RETRACTED unless REQUIRED or REQUIRED+IN-TRANSPORT (and no other flags set)";
      break;
  
    case MSTRO_CDO_STATE_RETRACTED:
      if(old_state != MSTRO_CDO_STATE_RETRACTED_GLOBALLY)
        msg = "Cannot set RETRACTED unless RETRACTED GLOBALLY";
      break;
      
    default:
      msg="Unchecked state transition";
      unchecked=true;
      break;
  }

  if(msg==NULL) {
    return true;
  } else {
    if(unchecked) {
      WITH_CDO_ID_STR(
          idstr, cdoid,
          WARN("Unchecked state transition for %s, %s to %s\n",
               idstr,
               mstro_cdo_state_describe(old_state),
               mstro_cdo_state_describe(new_state),
               msg););
      return false;
    } else {
      WITH_CDO_ID_STR(
          idstr, cdoid,
          ERR("Illegal state transition for %s, %s (%" PRIu32 ") to %s (%" PRIu32 "): %s\n",
              idstr, 
              mstro_cdo_state_describe(old_state), old_state,
              mstro_cdo_state_describe(new_state), new_state,
              msg););
      return false;
    }
  }
}

/** set state. Notify all waiters on state change queue. Does not require CDO to be locked. */
mstro_status
mstro_cdo_state_set(mstro_cdo cdo, mstro_cdo_state s)
{
  assert(g_pool_app_id != MSTRO_APP_ID_MANAGER);
  bool ret = false;
  /* get cdo current state */
  mstro_cdo_state old_state = mstro_cdo_state_get(cdo);
  DEBUG("Attempting to change state from `%s` to `%s` \n", 
         mstro_cdo_state_describe(old_state), 
         mstro_cdo_state_describe(s));
  /*verify operation*/
  if(mstro_pc_cdo__valid_state_transition(&cdo->gid,old_state, s))
  {
    /*transition is allowed ... go ahead*/
    ret = atomic_compare_exchange_strong(&cdo->state,&old_state,s);
    if (ret)
    {
      pthread_mutex_lock(&g_cdo_state_change_lock);
      pthread_cond_broadcast(&g_cdo_state_change_cvar);
      pthread_mutex_unlock(&g_cdo_state_change_lock);
      return MSTRO_OK;
    }
    else
    {
      DEBUG("CDO state has changed in the meanwhile...you may need to retry\n");
      return MSTRO_INVARG;
    }
  }
  else
  {
    ERR("Can not change state from `%s` to `%s` \n", 
         mstro_cdo_state_describe(old_state), 
         mstro_cdo_state_describe(s));
    return MSTRO_FAIL;
  }
  
}

mstro_status
mstro_cdo_state_set_safe_flags(mstro_cdo cdo, mstro_cdo_state s)
{
  assert(g_pool_app_id != MSTRO_APP_ID_MANAGER);
  bool ret = false;
  mstro_cdo_state old_state = mstro_cdo_state_get(cdo);
  mstro_cdo_state state_flags = (old_state & MSTRO_CDO_STATE_FLAGS);
  mstro_cdo_state new_state = s | state_flags;

  DEBUG("Attempting to change state from `%s` to `%s` \n", 
         mstro_cdo_state_describe(old_state), 
         mstro_cdo_state_describe(new_state));

  /*verify operation*/
  if(mstro_pc_cdo__valid_state_transition(&cdo->gid,old_state, new_state))
  {
    ret = atomic_compare_exchange_strong(&cdo->state,&old_state,new_state); // nobody should come in-between
    if (ret)
    {
      pthread_mutex_lock(&g_cdo_state_change_lock);
      pthread_cond_broadcast(&g_cdo_state_change_cvar);
      pthread_mutex_unlock(&g_cdo_state_change_lock);
      return MSTRO_OK;
    }
    else
    {
      DEBUG("CDO state has changed in the meanwhile...you may need to retry\n");
      return MSTRO_INVARG;
    }
  
  }
  else
  {
    ERR("Can not change state from `%s` to `%s` \n", 
         mstro_cdo_state_describe(old_state), 
         mstro_cdo_state_describe(new_state));
    return MSTRO_FAIL;
  }
}

mstro_cdo_state
mstro_cdo_state_get(mstro_cdo cdo)
{
  if(cdo==NULL)
    return MSTRO_CDO_STATE_INVALID;
  else {
    return atomic_load(&cdo->state);
  }
}


void
mstro_cdo_block_until(mstro_cdo cdo, mstro_cdo_state s, const char* debug)
{
  const char *dummy = debug; // avoid unused arg warning

  mstro_cdo_state state = atomic_load(&cdo->state);
  while(0 == (state & s)) {
    /* wait for state change */

    /* BEWARE: Enable this debug code only if absolutely necessary: since it does
     * not lock the g_outstanding_cdo_decl_table_mtx (which is
     * expensive) it is a data race with the completion handler
     * setting the cdo->gid slot */

    /* const struct mstro_cdo_id null_cdoid = MSTRO_CDO_ID_INVALID; */
    /* WITH_CDO_ID_STR( */
    /*     /\* we might be called before GID is assigned *\/ */
    /*     idstr, mstro_cdo_id__equal(&cdo->gid, &null_cdoid) ? &cdo->id : &cdo->gid, */
    /*     DEBUG("cdo %s is not in %s state yet, waiting for state change\n", */
    /*           idstr, debug);); */
    state = wait_for_cdo_state_change(cdo, state);
  }
  WITH_CDO_ID_STR(
      idstr, &cdo->gid,
      DEBUG("cdo %s changed state, now %s\n",
            idstr, mstro_cdo_state_describe(
                mstro_cdo_state_get(cdo))););
}


const char *
mstro_cdo_name(mstro_cdo cdo)
{
  if(cdo==NULL) {
    return NULL;
  } else {
    return cdo->name;
  }
}

static inline
mstro_status
mstro_cdo_declare__propagate(const char *name,
		  mstro_cdo *result)
{
  mstro_cdo_state_set(*result, MSTRO_CDO_STATE_CREATED);

  (*result)->gid = MSTRO_CDO_ID_NULL;

  mstro_status s = mstro_cdo_id_from_name(name, &(*result)->id);

  if(s!=MSTRO_OK) {
    ERR("Failed to compute local ID for CDO");
    mstro_cdo__free(result);
    return s;
  }

  /* assign unique index */
  _Atomic uint64_t serial = atomic_fetch_add(&g_cdo_serial_counter, 1);
  if(serial==UINT64_MAX) {
    ERR("Wrap-around on CDO serial counter, FIXME\n");
    abort();
  }
  (*result)->id.local_id = serial;
  (*result)->serial = serial;
  (*result)->n_segments = 0; /* initial value ... no outstanding communications */


  /*** inform pool manager ***/
  /* build DECLARE msg */
  Mstro__Pool__Declare d = MSTRO__POOL__DECLARE__INIT;
  d.serial = (*result)->id.local_id;
  d.cdo_name = (*result)->name;

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  s = mstro_pmp_package(&msg, (ProtobufCMessage*)&d);
  if(s!=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        d.base.descriptor->name);
    return s;
  }

  /* add this serial to 'outstanding CDO declarations' table */
  pthread_mutex_lock(&g_outstanding_cdo_decl_table_mtx);
  HASH_ADD(hh, g_outstanding_cdo_decl_table,
           serial, sizeof((*result)->serial), (*result));
  pthread_mutex_unlock(&g_outstanding_cdo_decl_table_mtx);

  /* send it off: this function automatically packs the message and sends it. The completion will set the global ID. */
  s = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
  if(s!=MSTRO_OK) {
    switch(s) {
      case MSTRO_NO_PM:
        /* fake it */
        mstro_cdo_declare_completion((*result)->id.local_id, &(*result)->id, 0);
        s=MSTRO_OK;
        break;
      default:
        /* In all of these cases 'outstanding CDO' table will not be
         * cleaned up by the DECLARE_ACK, but if the CDO gets deallocated
         * it will ruin the table */
        pthread_mutex_lock(&g_outstanding_cdo_decl_table_mtx);
        HASH_DEL(g_outstanding_cdo_decl_table, (*result));
        pthread_mutex_unlock(&g_outstanding_cdo_decl_table_mtx);

        ERR("Failed to send DECLARE to pool manager (%d: %s)\n", s, mstro_status_description(s));
        mstro_cdo__free(result);
        return s;
    }
  }

  return MSTRO_OK;
}

mstro_status
mstro_cdo_declare_core(const char *name,
                  mstro_cdo_attributes attributes,
		  mstro_cdo *result)
{
  if(name==NULL) {
    ERR("name cannot be NULL\n");
    return MSTRO_INVARG;
  }
  if(result==NULL) {
    ERR("declaration handle storage cannot be NULL\n");
    return MSTRO_INVOUT;
  }
  if(attributes!=MSTRO_ATTR_DEFAULT) {
    ERR("non-default declaration attributes unsupported, FIXME\n");
    return MSTRO_UNIMPL;
  }

  *result = mstro_cdo__alloc();
  if(*result==NULL) {
    ERR("Cannot allocate for CDO declaration\n");
    return MSTRO_NOMEM;
  }

  (*result)->name = strdup(name);
  if((*result)->name==NULL)  {
    ERR("Cannot allocate for CDO name\n");
    mstro_cdo__free(result);
    return MSTRO_NOMEM;
  }

  mstro_status s;
  s = mstro_cdo_declare__propagate(name, result);
  if (s != MSTRO_OK)
    return s;

 /* mamba array and raw pointer */
  (*result)->mamba_array = NULL;
  (*result)->raw_ptr = NULL;

  /* Ensuire default attributes */
  s = mstro_cdo_attribute_set_default(*result);
  if (s != MSTRO_OK)
    return s;

  mstro_stats_add_counter(MSTRO_STATS_CAT_OBJECTS,
                          MSTRO_STATS_L_NUM_CDOS_CREATED,
                          1);

  return MSTRO_OK;
}

mstro_status
mstro_cdo_declare_async(const char *name,
                        mstro_cdo_attributes attributes,
                        mstro_cdo *result,
                        mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();

   mstro_status status = MSTRO_UNIMPL;
   status = mstro_cdo_declare_core(name, attributes, result);
   /** if successful, fill in the request */
   if(status == MSTRO_OK) {
     /** fill the request structure to be able to check on the operation later **/
     DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
     status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
     if (status!=MSTRO_OK) {
       ERR("Can not allocate memory for request object \n");
       return status;
     }
     (*request)->cdo.cdo = *result;
     (*request)->cdo.target_state = MSTRO_CDO_STATE_DECLARED;
     WARN("FIXME: unclear orig_state here\n");
     (*request)->cdo.orig_state = MSTRO_CDO_STATE_INVALID;
     //add to counter
     mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL,
                             MSTRO_STATS_L_PC_NUM_ASYNC_DECLARE, 1);
   }
   /** always return the output status*/
   return status;
}

mstro_status
mstro_cdo_declare(const char *name,
                  mstro_cdo_attributes attributes,
		  mstro_cdo *result)
{
  MSTRO_API_NEEDS_INIT();

  mstro_status status = MSTRO_UNIMPL;

  status = mstro_cdo_declare_core(name, attributes, result);
  /** if successful, increase statistics counter */
  if(status == MSTRO_OK) {
    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_DECLARE, 1);

    WITH_CDO_ID_STR(idstr, &(*result)->id,
                  INFO("Declared CDO `%s', (local ID: %s)\n",
                       (*result)->name, idstr);
                  );
  }

  /** always return the output status*/
  return status;

}

/* Build the cdo->attributes_msg protobuf structure for all attributes of the CDO
 * If already non-NULL, warn.
 * We do not support updating it -- use mstro_cdo_attributes_merge() for that.
 */
static inline
mstro_status
mstro_cdo_ensure_attribute_msg(mstro_cdo cdo)
{
  if(cdo->attributes_msg!=NULL) {
    WITH_CDO_ID_STR(idstr, &cdo->gid,
                    WARN("CDO %s already has attribute message\n", idstr);
                    );
    return MSTRO_OK;
  }

  return mstro_attribute_dict_to_message(cdo->attributes,
                                         &cdo->attributes_msg);
}

mstro_status
mstro_cdo__sync_rawptr(mstro_cdo cdo)
{
  if (cdo->raw_ptr != NULL) {
    DEBUG("raw-ptr is already occupied\n");
    return MSTRO_OK;
  }

  mstro_status status = MSTRO_OK;
  mmbError me;
  me = mmb_allocation_get_ptr(cdo->mamba_array->allocation, &cdo->raw_ptr);
  if (me != MMB_OK) {
    ERR("Couldn't sync raw-ptr and mamba_array\n");
    return MSTRO_FAIL;
  }

  status = mstro_attribute_dict_set(cdo->attributes,
                                    MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                    MSTRO_CDO_ATTR_VALUE_INVALID,
                                    cdo->raw_ptr, false, true);
  if(status!=MSTRO_OK) {
    ERR("Failed to set %s attribute (after allocation during transport) of CDO %s\n",
        MSTRO_ATTR_CORE_CDO_RAW_PTR, cdo->name);
    return status;
  }

  return status;
}

static inline
mstro_status
mstro_cdo__create_mamba_array(mstro_cdo cdo, int policy)
{
  mstro_status s = MSTRO_OK;
  mmbError me;
  mmbLayout *layout=NULL;
  int64_t* ndims, *patt, *localsz, *elsz;
  size_t* dimsz;
  mmbDimensions dims;

  s = mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, NULL, (const void**)&localsz);

  DEBUG("Sync'ing mamba_array (size: %zu) and attributes (CDO `%s`)\n", (size_t)*localsz, cdo->name);

  /* fetching minimal set of attributes for custom layout */
  s |= mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE,
                               NULL, (const void**)&elsz);
  s |= mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS,
                               NULL, (const void**)&ndims);
  s |= mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER,
                               NULL, (const void**)&patt);
  if (s != MSTRO_OK) {
    ERR("Can't make a mamba array because attribute_get failed\n");
    return MSTRO_FAIL;
  }

  /* dims-size is critical for layout info because it does not have a default value */
  s = mstro_attribute_dict_get(cdo->attributes, MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE,
                               NULL, (const void**)&dimsz, NULL, false);
  if (s == MSTRO_OK) {
    /* custom layout route */
    int i;
    int64_t ndl = 1;
    for (i=0; i<*ndims; i++)
      ndl *= dimsz[i];
    if (ndl* *elsz != *localsz) {
      ERR("Given attributes dims-size (%zu) (element size: %d) and local-size are inconsistent (%d)\n", ndl, *elsz, *localsz);
      return MSTRO_FAIL;
    }
    dims.size = *(const int64_t*)ndims;
    dims.d = dimsz;
    mmbLayoutOrder lo = (patt) ? MMB_ROWMAJOR : MMB_COLMAJOR;
    me = mmb_layout_create_regular_nd(*(const int64_t*)elsz, *(const int64_t*)ndims, lo, MMB_PADDING_NONE, &layout);
    if (me != MMB_OK) {
      ERR("Couldn't create a Mamba layout\n");
      return MSTRO_FAIL;
    }
  } else {
    /* blob route */
    dims.size = 1;
	dims.d = (size_t*)localsz;
    me = mmb_layout_create_regular_1d(sizeof(char), MMB_PADDING_NONE, &layout);
    if(me != MMB_OK) {
      ERR("Failed to create regular 1d layout: %d (%s)\n",
          me, mmb_error_description(me));
      return MSTRO_FAIL;
    }
  }

  if ((policy & MSTRO_ALLOC_POLICY_LOCKED) &&
      (policy & MSTRO_ALLOC_POLICY_ALIGNED)) {
    DEBUG("Requiring a locked and aligned raw-ptr\n");
    cdo->raw_ptr = mstro_alloc_lockable(*localsz, false); /* don't lock immediately, RDMA transport handles that */
    assert (cdo->raw_ptr!=NULL);
    cdo->core_owned_raw_ptr = true;
    mstro_status status = mstro_attribute_dict_set(cdo->attributes,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                   MSTRO_CDO_ATTR_VALUE_INVALID,
                                                   cdo->raw_ptr, false, true);
    if(status!=MSTRO_OK) {
      ERR("Failed to set %s attribute of CDO %s\n",
          MSTRO_ATTR_CORE_CDO_RAW_PTR, cdo->name);
      return status;
    }
  }
  if (cdo->raw_ptr != NULL) {
    DEBUG("Creating raw-ptr mamba wrapper\n");
    me = mmb_array_create_wrapped(
        cdo->raw_ptr,
        &dims,
        layout,
        g_default_cdo_interface,
        MMB_READ_WRITE, &(cdo->mamba_array));
  } else {
    DEBUG("Creating brand new mamba array\n");
    me = mmb_array_create(
        &dims,
        layout,
        g_default_cdo_interface,
        MMB_READ_WRITE, & (cdo->mamba_array));
    s = mstro_cdo__sync_rawptr(cdo);
    if (s != MSTRO_OK) {
      // already printed an error
      return s;
    }
  }
  
  if (me != MMB_OK) {
    ERR("Couldn't create mamba array\n");
    return MSTRO_FAIL;
  }
  /* at this point mamba_array parameters and CDO attributes are in sync */
  
  /* DEBUG("Created mmbArray wrapper for raw-ptr %p at %p, size %"PRIu64", inner raw prt %p\n", */
  /*      cdo->raw_ptr, cdo->mamba_array, size, cdo->mamba_array->allocation->ptr); */
  me = mmb_layout_destroy(layout);
  if(me!=MMB_OK) {
    ERR("Failed to deallocate mamba layout: %d (%s)\n",
        me, mmb_error_description(me));
    return MSTRO_FAIL;
  }
  /* need to bypass public API since CDO may be sealed (if we are called from transport) */
  s = mstro_attribute_dict_set(cdo->attributes,
                               MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY,
                               MSTRO_CDO_ATTR_VALUE_INVALID,
                               cdo->mamba_array,
                               false, true);
  if(s!=MSTRO_OK) {
    ERR("Failed to store mamba-array attribute for CDO %s\n", cdo->name);
    return s;
  }

  return s;
}

mstro_status
mstro_cdo_seal(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status = mstro_cdo_declaration_seal(cdo);
  if (status == MSTRO_OK) {
      mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_SEAL, 1);
  }
  return status;
}


mstro_status
mstro_cdo_seal_async(mstro_cdo cdo, mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status = MSTRO_UNIMPL;
  if(cdo==NULL) {
    ERR("NULL CDO illegal\n");
    return MSTRO_INVARG;
  }
  if(! mstro_cdo_state_check(cdo,
                             //MSTRO_CDO_STATE_CREATED | /** FIXME requires that the cdo is already in declared state. It will return an error otherwise. **/
                               MSTRO_CDO_STATE_DECLARED)) {
    ERR("CDO `%s' has state %d, cannot seal\n", cdo->name, cdo->state);
    return MSTRO_INVARG;
  }

  DEBUG("cdo now in DECLARED state\n");

  void *raw_ptr=NULL;
  mmbArray *mamba_array=NULL;

  mstro_status s_rawptr = mstro_attribute_dict_get(
      cdo->attributes, MSTRO_ATTR_CORE_CDO_RAW_PTR,
      NULL, (const void**)&raw_ptr, NULL, false);
  mstro_status s_marray = mstro_attribute_dict_get(
      cdo->attributes, MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY,
      NULL, (const void**)&mamba_array, NULL, false);

  if(s_rawptr!=MSTRO_OK) {
    if(s_rawptr==MSTRO_NOENT) {
      ; /* maybe ok */
    } else {
      ERR("Cannot retrieve CDO raw-ptr: %d\n", s_rawptr);
      return s_rawptr;
    }
  }
  if(s_marray!=MSTRO_OK) {
    if(s_marray==MSTRO_NOENT) {
      ; /* maybe ok */
    } else {
      ERR("Cannot retrieve CDO mamba-array: %d\n", s_marray);
      return s_marray;
    }
  }

  DEBUG("Raw ptr %p, mamba_array %p\n", raw_ptr, mamba_array);
  if(raw_ptr!=NULL && mamba_array!=NULL) {
    INFO("CDO `%s` has both raw-ptr and existing mamba-array, assuming DISPOSE_AND_REUSE case\n",
        cdo->name);
  }

  /* we will cache these two attributes, as the user can not change
   * them anymore (until potential dispose_and_reuse) and we'd like all mamba
   * core code to be able to avoid dictionary lookups for them */
 // assert(cdo->raw_ptr==NULL && cdo->mamba_array==NULL);

  cdo->raw_ptr = raw_ptr;
  cdo->mamba_array = mamba_array;

  if (cdo->mamba_array == NULL && cdo->raw_ptr == NULL) {
    INFO("Sealing CDO `%s`, but no raw-ptr (type 0)\n", cdo->name);
  } else if (cdo->raw_ptr!=NULL && cdo->mamba_array!=NULL)  { 
     // do nothing, reusing resources 
  } else {
    status = mstro_cdo_allocate_data(cdo); /* sync raw-ptr+mamba_array+attributes */
    if (status != MSTRO_OK) {
      ERR("Couldn't sync mamba+raw array and attributes\n");
      goto BAILOUT;
    }
  }

  /* Ensure a sane name is filled in. The attribute is, in fact, only
   * needed for subscription handling (so users can match on names the
   * same way that they can for other attributes).
   *
   * The user does not typically set the name attribute explicitly,
   * but if they do we need to ensure it's not in disagreement with
   * the name specified at mstro_cdo_declare() time. */
  const void *cdo_name_attrval=NULL;
  enum mstro_cdo_attr_value_type valtype;

  mstro_status s_cdoname = mstro_attribute_dict_get(
      cdo->attributes, MSTRO_ATTR_CORE_CDO_NAME,
      &valtype, &cdo_name_attrval, NULL, false);
  if(s_cdoname==MSTRO_OK || s_cdoname==MSTRO_NOENT) {
    const char *n_cdo = mstro_cdo_name(cdo);
    if(s_cdoname==MSTRO_OK) {
      /* ok, already have a name, check it matches */
      const char *n_attr = (const char *)cdo_name_attrval;

      if(0!=strcmp(n_cdo,n_attr)) {
        ERR("CDO |%s| has name attribute set to |%s|, overriding\n",
            cdo->name, cdo_name_attrval);
      }
    }

    s_cdoname = mstro_attribute_dict_set(cdo->attributes,
                                         MSTRO_ATTR_CORE_CDO_NAME,
                                         MSTRO_CDO_ATTR_VALUE_cstring,
                                         &n_cdo, true, false);
    if(s_cdoname!=MSTRO_OK) {
      ERR("Failed to set CDO name attribute: %d (%s)\n", s_cdoname,
          mstro_status_description(s_cdoname));
      status = s_cdoname;
      goto BAILOUT;
    }
  } else {
    ERR("Failed to inquire about CDO name attribute\n");
    status = s_cdoname;
    goto BAILOUT;
  }

  /* build SEAL message, send and wait for completion */
  {

    Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
    cdoid.qw0 = cdo->gid.qw[0];
    cdoid.qw1 = cdo->gid.qw[1];
    cdoid.local_id = cdo->gid.local_id;

    status = mstro_cdo_ensure_attribute_msg(cdo);
    if(status!=MSTRO_OK) {
      ERR("Failed to serialize attributes\n");
      return status;
    }
    assert(cdo->attributes_msg!=NULL);
    /* we don't transmit the default namespace */
    cdo->attributes_msg->default_namespace=NULL;

    /* attributes message is preserved in cdo, so it's safe to pack into message */
    Mstro__Pool__Seal seal = MSTRO__POOL__SEAL__INIT;
    seal.cdoid = &cdoid;
    seal.attributes = cdo->attributes_msg;

    Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
    status = mstro_pmp_package(&msg, (ProtobufCMessage*)&seal);
    if(status!=MSTRO_OK) {
      ERR("Failed to package %s into a pool manager message\n",
          seal.base.descriptor->name);
      return status;
    }
    /* FIXME: do we need an ACK here? */
    status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
    switch(status) {
      case MSTRO_OK:
        ;
        break;
      case MSTRO_NO_PM:
        DEBUG("seal local only, no PM\n");
        /* need to update the status locally */
        mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_SEALED);
        break;
      default:
        ERR("Failed to send SEAL message to pool manager: %d (%s)\n",
            status, mstro_status_description(status));
        goto BAILOUT;
    }
  }

  /** fill the request structure to be able to check on the operation later **/
  DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
  status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
  if(status!=MSTRO_OK) {
    ERR("Can not allocate memory for request object \n");
    goto BAILOUT;
  }
  (*request)->cdo.cdo = cdo;
  (*request)->cdo.target_state = MSTRO_CDO_STATE_SEALED;

  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_ASYNC_SEAL, 1);
  status = MSTRO_OK;

BAILOUT:
  return status;
}

/** some global states need to be pushed manually */
static inline
void
mstro_cdo__advance_target_state(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);
  switch (request->cdo.target_state) {
    case MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY:
      request->cdo.target_state = MSTRO_CDO_STATE_WITHDRAWN;
      break;
    case MSTRO_CDO_STATE_RETRACTED_GLOBALLY:
      request->cdo.target_state = MSTRO_CDO_STATE_RETRACTED;
      break;
    case MSTRO_CDO_STATE_DECLARED:  /*fallthrough*/
    case MSTRO_CDO_STATE_SEALED:    /*fallthrough*/
    case MSTRO_CDO_STATE_WITHDRAWN: /*fallthrough*/
    case MSTRO_CDO_STATE_RETRACTED: /*fallthrough*/
    case MSTRO_CDO_STATE_OFFERED:   /*fallthrough*/
    case MSTRO_CDO_STATE_DEMANDED:  /*fallthrough*/
    case MSTRO_CDO_STATE_REQUIRED:  /*fallthrough*/
    case MSTRO_CDO_STATE_SATISFIED: /*fallthrough*/
      /* no action needed */
      break;
    default:
      ERR("Unexpected CDO state: %d\n", request->cdo.target_state);
      abort();
  }
}

/** Increment ASYNC operation BLOCKed counter with the request->wait_flag*/
static inline
void
mstro_request__increment_async_blocked_counter(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);

  const char *counter;
  switch (request->cdo.target_state) {
    case MSTRO_CDO_STATE_DECLARED:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_DECLARE_BLOCKED;
      break;
    case MSTRO_CDO_STATE_SEALED:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_SEAL_BLOCKED;
      break;
    case MSTRO_CDO_STATE_OFFERED:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_OFFER_BLOCKED;
      break;
    case MSTRO_CDO_STATE_REQUIRED:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_REQUIRE_BLOCKED;
      break;
    case MSTRO_CDO_STATE_DEMANDED:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_DEMAND_BLOCKED;
      break;
    case MSTRO_CDO_STATE_WITHDRAWN:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_WITHDRAW_BLOCKED;
      break;
    case MSTRO_CDO_STATE_RETRACTED:
      counter = MSTRO_STATS_L_PC_NUM_ASYNC_RETRACT_BLOCKED;
      break;
    default:
      ERR("Unexpected state %d not accounted in statistics\n",
          request->cdo.target_state);
      return;
  }
  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL,
                          counter,
                          request->cdo.wait_flag);
}






static
mstro_status
mstro_request_wait__cdo(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);

  DEBUG("Wait for asynchronous cdo operation (%s) to complete \n",
        mstro_cdo_state_describe(request->cdo.target_state));
  /* initialize the wait flag value for the request */
  request->cdo.wait_flag=0;

  switch (request->cdo.target_state) {
    /* Demand with pm */
    case MSTRO_CDO_STATE_SATISFIED:
      DEBUG("Waiting on a demand with pm async operation \n");
      
      if ((mstro_cdo_state_get(request->cdo.cdo) != MSTRO_CDO_STATE_SATISFIED)) {
        DEBUG("Will have to wait until cdo state is advanced to MSTRO_CDO_STATE_SATISFIED\n");
        request->cdo.wait_flag=1;
        /* wait until the transfer is satisfied by the transport.*/
        mstro_cdo_block_until(request->cdo.cdo, request->cdo.target_state, "");
        DEBUG( "CDO state: %s \n",
               mstro_cdo_state_describe(mstro_cdo_state_get(request->cdo.cdo)));
        /** This function does not wait or block, it just completes the demand operation*/
        request->cdo.target_state = MSTRO_CDO_STATE_DEMANDED;
      }
      mstro_cdo_wait_demand_async_with_pm(request);
      /* reduce number of waiters */
      mstro_pool_cdo_waiters(request->cdo.cdo, -1);
      break;

    case MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY:
    case MSTRO_CDO_STATE_RETRACTED_GLOBALLY:
      if ((mstro_cdo_state_get(request->cdo.cdo)  != request->cdo.target_state)) {
        /* wait until the cdo state is updated .*/
        DEBUG("Waiting until cdo state is %s \n",
              mstro_cdo_state_describe(request->cdo.target_state));
        /* raise the wait flag on this request */
        request->cdo.wait_flag=1;
        mstro_cdo_block_until(request->cdo.cdo, request->cdo.target_state, "");
      }
      mstro_cdo__advance_target_state(request);
      mstro_cdo_wait_local_requires(request);
      /* decrement waiters */
      mstro_pool_cdo_waiters(request->cdo.cdo, -1);
      break;

    case MSTRO_CDO_STATE_DEMANDED:
      /* Local demand operation -- with pm is handled above in MSTRO_CDO_STATE_SATISFIED*/
      DEBUG("call wait local demand to try to make progress on the local pool \n");
      /* call wait local demand to try to make progress on the local pool */
      mstro_wait_local_demand(request);
      break;
  }
  
  if(mstro_cdo_state_get(request->cdo.cdo) == request->cdo.target_state){
    /* nothing to do here */
    DEBUG("CDO is in already in the requested state, i.e, %s\n ",
          mstro_cdo_state_describe(request->cdo.target_state));
  } else {
    /* wait for the operation to complete */
    DEBUG("Wait for the operation (%s) to complete on CDO %s \n",
          mstro_cdo_state_describe(request->cdo.target_state),
          request->cdo.cdo->name);
    request->cdo.wait_flag = 1;
    mstro_cdo_block_until(request->cdo.cdo, request->cdo.target_state, "");
  }
  /* Increment ASYNC operation BLOCKed counter with the request->cdo.wait_flag */
  mstro_request__increment_async_blocked_counter(request);

  /* Free the request */
  return mstro_request__free(request);
}

static
bool
mstro_request_test__generic(mstro_request request);


static
mstro_status
mstro_request_wait__generic(mstro_request req)
{
  assert(req->kind==MSTRO_REQUEST_GENERIC);
  mstro_status retstat = MSTRO_UNIMPL;

  if(mstro_request_test__generic(req)) {
    /* fast path: already completed */
    ;
  } else {
    int s=pthread_mutex_lock(&req->generic.lock);
    if(s!=0) {
      ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
      return MSTRO_FAIL;
    }
    s=pthread_cond_wait(&req->generic.cvar, &req->generic.lock);
    if(s!=0) {
      ERR("Failed to wait for completion: %d (%s)\n", s, strerror(s));
      int s=pthread_mutex_unlock(&req->generic.lock);
      if(s!=0) {
        ERR("Failed to unlock mutex: %d (%s)\n", s, strerror(s));
      }
      return MSTRO_FAIL;
    }

    assert(req->generic.completed); /* we would not have been
                                     * woken up otherwise */
    s=pthread_mutex_unlock(&req->generic.lock);
    if(s!=0) {
      ERR("Failed to unlock mutex: %d (%s)\n", s, strerror(s));
      return MSTRO_FAIL;
    }
  }

  retstat = req->generic.status;
  mstro_request__free(req);

  return retstat;
}
  
/** Wait for cdo asynchronous operation to complete **/
mstro_status
mstro_request_wait(mstro_request request)
{
  if(request==NULL)
    return MSTRO_INVARG;

  if(!mstro_request_test(request))
    mstro__abort_if_deadlock();

  switch(request->kind) {
    case MSTRO_REQUEST_CDO:
      if (!mstro_request_is_valid(request)) {
        ERR("Invalid maestro async operation request handle\n");
        return MSTRO_INVARG;
      }
      return mstro_request_wait__cdo(request);
      break;
    case MSTRO_REQUEST_GENERIC:
      if (!mstro_request_is_valid(request)) {
        ERR("Invalid maestro async operation request handle\n");
        return MSTRO_INVARG;
      }
      return mstro_request_wait__generic(request);
      break;
    default:
      ERR("Unexpected request kind %d\n", request->kind);
      return MSTRO_UNIMPL;
  }
  return MSTRO_FAIL;
}

/* check if mstro request is valid or not */
bool
mstro_request_is_valid(mstro_request request){
  if (request == NULL){
    return false;
  }

  switch(request->kind) {
    case MSTRO_REQUEST_GENERIC:
      return true;
    case MSTRO_REQUEST_CDO: {
      if (request->cdo.cdo == NULL){
        return false;
      }
      /*check request target state -- valid states are*/
      switch (request->cdo.target_state) {
        case MSTRO_CDO_STATE_DECLARED:
        case MSTRO_CDO_STATE_SEALED:
        case MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY:
        case MSTRO_CDO_STATE_WITHDRAWN:
        case MSTRO_CDO_STATE_RETRACTED_GLOBALLY:
        case MSTRO_CDO_STATE_RETRACTED:
        case MSTRO_CDO_STATE_OFFERED:
        case MSTRO_CDO_STATE_DEMANDED:
        case MSTRO_CDO_STATE_REQUIRED:
        case MSTRO_CDO_STATE_SATISFIED:
          return true;
      }
      return false;
    }
      break;
    case MSTRO_REQUEST_INVALID: /*fallthrough*/
    default:
      return false;
  }
  return false;
}

/** check whether CDO asynchronous operation has completed  **/
static
bool
mstro_request_test__generic(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_GENERIC);

  bool state = request->generic.completed;
  return state;
}

static
bool
mstro_request_test__cdo(mstro_request request)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_CDO);

  bool status;
  if (!mstro_request_is_valid(request)) {
    ERR("Invalid maestro async operation request handle\n");
    return false;
  }
  DEBUG("Check if operation (%s) is complete on CDO %s\n",
        mstro_cdo_state_describe(request->cdo.target_state),
        request->cdo.cdo->name);
  status = mstro_cdo_state_check(request->cdo.cdo, request->cdo.target_state);
  /* if the first stage of withdraw is completed */
  if(status) {
    /* update the request to test the next stage*/
    mstro_cdo__advance_target_state(request);
  } else {
    /*CDO is not in target state */
    if (request->cdo.target_state == MSTRO_CDO_STATE_DEMANDED) {
      /* call test to try to make progress on the local pool */
      status = mstro_test_local_demand(request);
      if(status) {
        /* We made it */
        /* reduce number of waiters */
        status = mstro_pool_cdo_waiters(request->cdo.cdo, -1);
        if(status!=MSTRO_OK) {
          ERR("Failed to increment/decrement the waiters on the cdo\n");
          return false;
        }
      } // end successful complettion of demand
    } // end if target state is demanded
  } // end if cdo is in target state

  /* stage 2 of the operations */
  if(  (request->cdo.target_state == MSTRO_CDO_STATE_WITHDRAWN)
    || (request->cdo.target_state == MSTRO_CDO_STATE_RETRACTED)) {
    /** Try to set the state of the cdo */
    status = mstro_cdo_test_local_requires(request);
    if(status) {
      /* reduce number of waiters */
      mstro_pool_cdo_waiters(request->cdo.cdo, -1);
    }
  } else if (request->cdo.target_state == MSTRO_CDO_STATE_SATISFIED) {
    /* stage 2 of demand with pm */
    if ((mstro_cdo_state_get(request->cdo.cdo) != MSTRO_CDO_STATE_SATISFIED)) {
      DEBUG("Will have to wait until cdo state is advanced to MSTRO_CDO_STATE_SATISFIED\n");
      status = false;
    } else {
      /** This function does not wait or block, it just completes the demand operation*/
      DEBUG("Async demand SATISFIED ...advancing target state to DEMANDED \n");
      mstro_cdo_wait_demand_async_with_pm(request);
      request->cdo.target_state = MSTRO_CDO_STATE_DEMANDED;
      /* reduce number of waiters */
      mstro_pool_cdo_waiters(request->cdo.cdo, -1);
      status = true;
    }
  }
  
  return status;
}

bool
mstro_request_test(mstro_request request)
{
  if(request==NULL) {
    ERR("Invalid request\n");
    return false;
  }
  switch(request->kind) {
    case MSTRO_REQUEST_CDO:
      return mstro_request_test__cdo(request);
      break;
    case MSTRO_REQUEST_GENERIC:
      return mstro_request_test__generic(request);
      break;
    default:
      ERR("Unexepected request kind: %d\n", request->kind);
  }
  return false;
}

mstro_status
mstro_request__allocate(enum mstro_request_kind kind,
                        struct mstro_request_ **res)
{
  if(kind==MSTRO_REQUEST_INVALID || kind>=MSTRO_REQUEST__max)
    return MSTRO_INVARG;
  if(res==NULL)
    return MSTRO_INVOUT;

  mstro_status stat=MSTRO_OK;

  *res=malloc(sizeof(struct mstro_request_));
  if(*res==NULL) {
    return MSTRO_NOMEM;
  } else {
    (*res)->kind = kind;
    switch(kind) {
      case MSTRO_REQUEST_GENERIC: {
        int s=pthread_mutex_init(&((*res)->generic.lock), NULL);
        if(s!=0) {
          ERR("Failed to initialize mutex\n");
          stat=MSTRO_FAIL;
          goto BAILOUT;
        }
        s=pthread_cond_init(&((*res)->generic.cvar), NULL);
        if(s!=0) {
          ERR("Failed to initialize cvar\n");
          stat=MSTRO_FAIL;
          goto BAILOUT;
        }
        (*res)->generic.completed = false;
        (*res)->generic.payload=NULL;
        (*res)->generic.status=MSTRO_OK;
        NOISE("Allocated generic operation request object %p\n", *res);

        break;
      }
      case MSTRO_REQUEST_CDO:
        (*res)->cdo.cdo=NULL;
        (*res)->cdo.target_state = MSTRO_CDO_STATE_INVALID;
        (*res)->cdo.orig_state = MSTRO_CDO_STATE_INVALID;
        (*res)->cdo.wait_flag=0;
        NOISE("Allocated cdo operation request object %p\n", *res);
        break;
      default:
        ERR("Unexpected request kind: %d\n", kind);
        stat=MSTRO_UNIMPL;
    }
  }
 BAILOUT:
  return stat;
}


/** Frees a maestro request object  **/
mstro_status
mstro_request__free(mstro_request request)
{
  assert(request!=NULL);
  mstro_status stat=MSTRO_OK;

  NOISE("free async operation request, %p\n", request);
  switch(request->kind) {
    case MSTRO_REQUEST_INVALID:
      break;
    case MSTRO_REQUEST_GENERIC: {
      NOISE("free async generic operation request handle %p\n", request);
      int s=pthread_cond_destroy(&(request->generic.cvar));
      if(s!=0) {
        ERR("Failed to destroy cvar\n");
        stat |= MSTRO_FAIL;
      }
      s = pthread_mutex_destroy(&(request->generic.lock));
      if(s!=0) {
        ERR("Failed to destroy mutex\n");
        stat |= MSTRO_FAIL;
      }
      break;
    }
    case MSTRO_REQUEST_CDO:
      NOISE("free async CDO operation request handle %p\n", request);
      request->cdo.cdo = NULL;
      break;
    default:
      ERR("Unexpected async request handle kind: %d\n", request->kind);
      stat=MSTRO_UNIMPL;
      break;
  }
  free(request);
  return stat;
}

mstro_status
mstro_request_free(mstro_request request)
{
  return mstro_request__free(request);
}

mstro_status
mstro_request__generic__mark_complete(mstro_request request,
                                      mstro_status status,
                                      void *payload)
{
  assert(request!=NULL); assert(request->kind==MSTRO_REQUEST_GENERIC);

  int s=pthread_mutex_lock(&request->generic.lock);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    return MSTRO_FAIL;
  }
  if(request->generic.completed) {
    ERR("request already completed\n");
    abort();
  }
  
  request->generic.status=status;
  request->generic.payload=payload;
  request->generic.completed=true;

  s=pthread_cond_broadcast(&request->generic.cvar);
  if(s!=0) {
    ERR("Failed to broadcast completion: %d (%s)\n", s, strerror(s));
  }
  
  s=pthread_mutex_unlock(&request->generic.lock);
  if(s!=0) {
    ERR("Failed to unlock mutex: %d (%s)\n", s, strerror(s));
    return MSTRO_FAIL;
  }

  return MSTRO_OK;
}

mstro_status
mstro_cdo_declaration_seal(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status = MSTRO_UNIMPL;

  if(cdo==NULL) {
    ERR("NULL CDO illegal\n");
    return MSTRO_INVARG;
  }
  if(! mstro_cdo_state_check(cdo,
                             MSTRO_CDO_STATE_CREATED
                             | MSTRO_CDO_STATE_DECLARED)) {
    ERR("CDO `%s' has state %d, cannot seal\n", cdo->name, cdo->state);
    return MSTRO_INVARG;
  }

  void *raw_ptr=NULL;
  mmbArray *mamba_array=NULL;


  mstro_status s_rawptr = mstro_attribute_dict_get(
      cdo->attributes, MSTRO_ATTR_CORE_CDO_RAW_PTR,
      NULL, (const void**)&raw_ptr, NULL, false);
  mstro_status s_marray = mstro_attribute_dict_get(
      cdo->attributes, MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY,
      NULL, (const void**)&mamba_array, NULL, false);

  if(s_rawptr!=MSTRO_OK) {
    if(s_rawptr==MSTRO_NOENT) {
      ; /* maybe ok */
    } else {
      ERR("Cannot retrieve CDO raw-ptr: %d\n", s_rawptr);
      return s_rawptr;
    }
  }
  if(s_marray!=MSTRO_OK) {
    if(s_marray==MSTRO_NOENT) {
      ; /* maybe ok */
    } else {
      ERR("Cannot retrieve CDO mamba-array: %d\n", s_marray);
      return s_marray;
    }
  }

  /* we will cache these two attributes, as the user can not change
   * them anymore and we'd like all mamba core code to be able to
   * avoid dictionary lookups for them */
//  assert(cdo->raw_ptr==NULL && cdo->mamba_array==NULL);
  cdo->raw_ptr = raw_ptr;
  cdo->mamba_array = mamba_array;

  if (cdo->mamba_array == NULL && cdo->raw_ptr == NULL) {
    DEBUG("Sealing CDO `%s`, but no raw-ptr (type 0)\n", cdo->name);
  } else if(raw_ptr!=NULL && mamba_array!=NULL) { // dispose-and-reuse use-case
    DEBUG("Sealing CDO `%s`, dispose-and-reuse fashion\n", cdo->name);
  } else {
    status = mstro_cdo_allocate_data(cdo); /* sync raw-ptr+mamba_array+attributes */
    if (status != MSTRO_OK) {
      ERR("Couldn't sync mamba+raw array and attributes\n");
      goto BAILOUT;
    }
  }
  
  /* Ensure a sane name is filled in. The attribute is, in fact, only
   * needed for subscription handling (so users can match on names the
   * same way that they can for other attributes).
   *
   * The user does not typically set the name attribute explicitly,
   * but if they do we need to ensure it's not in disagreement with
   * the name specified at mstro_cdo_declare() time. */
  const void *cdo_name_attrval=NULL;
  enum mstro_cdo_attr_value_type valtype;

  mstro_status s_cdoname = mstro_attribute_dict_get(
      cdo->attributes, MSTRO_ATTR_CORE_CDO_NAME,
      &valtype, &cdo_name_attrval, NULL, false);
  if(s_cdoname==MSTRO_OK || s_cdoname==MSTRO_NOENT) {
    const char *n_cdo = mstro_cdo_name(cdo);
    if(s_cdoname==MSTRO_OK) {
      /* ok, already have a name, check it matches */
      const char *n_attr = (const char *)cdo_name_attrval;

      if(0!=strcmp(n_cdo,n_attr)) {
        ERR("CDO |%s| has name attribute set to |%s|, overriding\n",
            cdo->name, cdo_name_attrval);
      }
    }

    s_cdoname = mstro_attribute_dict_set(cdo->attributes,
                                         MSTRO_ATTR_CORE_CDO_NAME,
                                         MSTRO_CDO_ATTR_VALUE_cstring,
                                         (void*)n_cdo, true, false);
    if(s_cdoname!=MSTRO_OK) {
      ERR("Failed to set CDO name attribute: %d (%s)\n", s_cdoname,
          mstro_status_description(s_cdoname));
      status = s_cdoname;
      goto BAILOUT;
    }
  } else {
    ERR("Failed to inquire about CDO name attribute\n");
    status = s_cdoname;
    goto BAILOUT;
  }

  /* we're done, but we need to ensure we've seen the DECLARE_ACK with
   * the global ID by now */
  mstro_cdo_block_until(cdo, MSTRO_CDO_STATE_DECLARED, "DECLARE");

  DEBUG("cdo now in DECLARED state\n");

  /* build SEAL message, send and wait for completion */
  {

    Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
    cdoid.qw0 = cdo->gid.qw[0];
    cdoid.qw1 = cdo->gid.qw[1];
    cdoid.local_id = cdo->gid.local_id;

    status = mstro_cdo_ensure_attribute_msg(cdo);
    if(status!=MSTRO_OK) {
      ERR("Failed to serialize attributes\n");
      return status;
    }
    assert(cdo->attributes_msg!=NULL);
    /* we don't transmit the default namespace */
    cdo->attributes_msg->default_namespace=NULL;

    /* attributes message is preserved in cdo, so it's safe to pack into message */
    Mstro__Pool__Seal seal = MSTRO__POOL__SEAL__INIT;
    seal.cdoid = &cdoid;
    seal.attributes = cdo->attributes_msg;

    Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
    status = mstro_pmp_package(&msg, (ProtobufCMessage*)&seal);
    if(status!=MSTRO_OK) {
      ERR("Failed to package %s into a pool manager message\n",
          seal.base.descriptor->name);
      return status;
    }
    /* FIXME: do we need an ACK here? */
    status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
    switch(status) {
      case MSTRO_OK:
        ;
        break;
      case MSTRO_NO_PM:
        DEBUG("seal local only, no PM\n");
        /* need to update the status locally */
        mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_SEALED);
        break;
      default:
        ERR("Failed to send SEAL message to pool manager: %d (%s)\n",
            status, mstro_status_description(status));
        goto BAILOUT;
    }
  }

  /* wait for ACK to set sealed state */
   mstro_cdo_block_until(cdo, MSTRO_CDO_STATE_SEALED, "SEALED");

   WITH_CDO_ID_STR(
      idstr, &cdo->id,
      WITH_CDO_ID_STR(gidstr, &cdo->gid,
                         INFO("CDO `%s` (local id %s, global id %s) now sealed\n",
                               cdo->name, idstr, gidstr);););


   status = MSTRO_OK;

BAILOUT:
  return status;
}

mstro_status
mstro_cdo_offer_async(mstro_cdo cdo, mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status;
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  if(!mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {

      /** FIXME allow auto-sealing of valid cdos */
      ERR("CDO `%s' state %d illegal for ASYNC OFFER operation\n",
          cdo->name, cdo->state);
      return MSTRO_INVARG;

  }

  /* We have a sealed CDO. Good. */
  WITH_CDO_ID_STR(
      id, &cdo->gid,
      {
        status = mstro_pool__add(cdo, MSTRO_CDO_STATE_OFFERED_LOCALLY);

        if(status!=MSTRO_OK) {
          ERR("Failed to add CDO %s (id %s) to local pool\n", cdo->name, id);
          return status;
        } else {
          INFO("Added CDO `%s' (id %s) OFFER to local pool\n",
               cdo->name, id);
          /* tell pool manager */
          Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
          cdoid.qw0 = cdo->gid.qw[0];
          cdoid.qw1 = cdo->gid.qw[1];
          cdoid.local_id = cdo->gid.local_id;


          Mstro__Pool__Offer offer = MSTRO__POOL__OFFER__INIT;
          offer.cdoid = &cdoid;

          Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
          status = mstro_pmp_package(&msg, (ProtobufCMessage*)&offer);
          if(status!=MSTRO_OK) {
            ERR("Failed to package %s into a pool manager message\n",
                offer.base.descriptor->name);
            return status;
          }

          status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
          switch(status) {
            case MSTRO_OK:
              /* PoolOpAck handler sets OFFERED state for us */
              break;
            case MSTRO_NO_PM:
              DEBUG("OFFER local only, no PM\n");
              mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_OFFERED);
	      status = mstro_pool__notify(cdo);

	      WITH_CDO_ID_STR(idstr, &cdo->gid, {
			      DEBUG("CDO %s now in state %s\n",
					      idstr, mstro_cdo_state_describe(mstro_cdo_state_get(cdo)));
			      });
              break;
            default:
              ERR("Failed to send OFFER message to pool manager: %d (%s)\n",
                  status, mstro_status_description(status));
              return status;
          }

          /** fill the request structure to be able to check on the operation later **/
          DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
          status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
          if(status!=MSTRO_OK) {
            ERR("Can not allocate memory for request object \n");
            return status;
          }
          (*request)->cdo.cdo = cdo;
          (*request)->cdo.target_state = MSTRO_CDO_STATE_OFFERED;


  WITH_CDO_ID_STR(
      idstr, &cdo->id,
      WITH_CDO_ID_STR(gidstr, &cdo->gid,
                      DEBUG("CDO %s (local id %s, global id %s) now sealed\n",
                            cdo->name, idstr, gidstr);););

            {
                const int64_t* val;
                enum mstro_cdo_attr_value_type type;
                status = mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                               NULL, (const void**)&val);
                if(status!=MSTRO_OK) {
                  ERR("CDO has no local-size\n");
                  return MSTRO_FAIL;
            }

            status = mstro_stats_add_counter(MSTRO_STATS_CAT_POOL,
                                             MSTRO_STATS_L_BYTES_POOLED,
                                             /* type 0 CDOs have no size */
                                             *val==-1? 0 : *val);

            mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_ASYNC_OFFER, 1);
          }

          return MSTRO_OK;
        }
      });
  /* not reached */
  return MSTRO_UNIMPL;
}

mstro_status
mstro_cdo_offer(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status;
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  DEBUG("Offer of %s\n", cdo->name);


  if(!mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {
    if(mstro_cdo_state_check(cdo,
                             MSTRO_CDO_STATE_CREATED
                             |MSTRO_CDO_STATE_DECLARED)) {
      DEBUG("Auto-sealing CDO `%s'\n", cdo->name);
      /* NOTE: this assume there is no concurrent seal ongoing --
       * which is ok by convention */
      status = mstro_cdo_declaration_seal(cdo);
      if(status!=MSTRO_OK) {
        return status;
      }
    } else {
      ERR("CDO `%s' state %d illegal for OFFER operation\n",
          cdo->name, cdo->state);
      return MSTRO_INVARG;
    }
  }

  /* We have a sealed CDO. Good. */

  WITH_CDO_ID_STR(lidstr, &cdo->id, {
      WITH_CDO_ID_STR(gidstr, &cdo->gid, {
          DEBUG("Offer of %s (lid: %s, gid: %s)\n",
                cdo->name, lidstr, gidstr);});});

  WITH_CDO_ID_STR(id, &cdo->gid, {

      const int64_t* val;
      int64_t cdo_size;
      enum mstro_cdo_attr_value_type type;
      status = mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                           NULL, (const void**)&val);
      if(status!=MSTRO_OK) {
        ERR("CDO has no local-size\n");
        return MSTRO_FAIL;
      }
      cdo_size = *val;
      if(cdo_size == -1) {
        cdo_size = 0;
        /* -1 is the default cdo size, setting zero to reflect the correct size of data */
        status = mstro_attribute_dict_set(cdo->attributes, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                          MSTRO_CDO_ATTR_VALUE_INVALID, /* we dont help in checking */
                                          &cdo_size, true, false);
        if(status != MSTRO_OK) {
          ERR("Can not set the cdo size to zero \n");
          return MSTRO_FAIL;
        }
      }
      status = mstro_pool__add(cdo, MSTRO_CDO_STATE_OFFERED_LOCALLY);

      if(status!=MSTRO_OK) {
        ERR("Failed to add CDO %s (id %s) to local pool\n", cdo->name, id);
        return status;
      } else {
        INFO("Added CDO `%s' (id %s) OFFER to local pool\n",
             cdo->name, id);
        /* tell pool manager */
        Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
        cdoid.qw0 = cdo->gid.qw[0];
        cdoid.qw1 = cdo->gid.qw[1];
        cdoid.local_id = cdo->gid.local_id;

        Mstro__Pool__Offer offer = MSTRO__POOL__OFFER__INIT;
        offer.cdoid = &cdoid;

        Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
        status = mstro_pmp_package(&msg, (ProtobufCMessage*)&offer);
        if(status!=MSTRO_OK) {
          ERR("Failed to package %s into a pool manager message\n",
              offer.base.descriptor->name);
          return status;
        }

        status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
        switch(status) {
          case MSTRO_OK:
            /* PoolOpAck handler sets OFFERED state for us */
            break;
          case MSTRO_NO_PM:
            DEBUG("OFFER local only, no PM\n");
            mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_OFFERED);
            status = mstro_pool__notify(cdo);

            WITH_CDO_ID_STR(idstr, &cdo->gid, {
                DEBUG("CDO %s now in state %s\n",
                      idstr, mstro_cdo_state_describe(mstro_cdo_state_get(cdo)));
              });
            break;
          default:
            ERR("Failed to send OFFER message to pool manager: %d (%s)\n",
                status, mstro_status_description(status));
            return status;
        }
        mstro_cdo_block_until(cdo, MSTRO_CDO_STATE_OFFERED, "OFFERED");

        {
          
          status = mstro_stats_add_counter(MSTRO_STATS_CAT_POOL,
                                           MSTRO_STATS_L_BYTES_POOLED,
                                           cdo_size);

          mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_OFFER, 1);
        }

        return MSTRO_OK;
      }
    });
  /* not reached */
  return MSTRO_UNIMPL;
}

mstro_status
mstro_cdo_withdraw_async(mstro_cdo cdo, mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  WITH_CDO_ID_STR(idstr, &cdo->gid, {
      DEBUG("asynchronously removing CDO `%s' (id %s) from pool\n",
            cdo->name, idstr);});

  if(!mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_POOLED)) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("CDO `%s' (id %s) not pooled, cannot remove it\n",
            cdo->name, idstr););
    return MSTRO_INVARG;
  }

  /** check its original status */
  mstro_cdo_state orig_state = mstro_cdo_state_get(cdo);

  if ((orig_state != MSTRO_CDO_STATE_REQUIRED) && (orig_state != MSTRO_CDO_STATE_OFFERED)){
    ERR("Illegal CDO state: %d\n");
    return MSTRO_FAIL;
  }
  WITH_CDO_ID_STR(
      idstr, &cdo->gid,
      DEBUG("CDO %s orig state is %d (%s)\n",
            idstr, orig_state, mstro_cdo_state_describe(orig_state));
                  );

  /** async remove of the cdo ...stage 1 only */
  mstro_status status = mstro_pool__remove_async(cdo, MSTRO_CDO_STATE_WITHDRAWN);

  if (status == MSTRO_OK) {
    /** fill up the request handler */
    DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
    status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
    if(status!=MSTRO_OK) {
      ERR("Can not allocate memory for request object \n");
      return status;
    }
    (*request)->cdo.cdo = cdo;
    (*request)->cdo.target_state = MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY;
    (*request)->cdo.orig_state = orig_state;

    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL,
                            MSTRO_STATS_L_PC_NUM_ASYNC_WITHDRAW,
                            1);
  }

  return status;
}

mstro_status
mstro_cdo_withdraw(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  WITH_CDO_ID_STR(idstr, &cdo->gid, {
      DEBUG("Removing CDO `%s' (id %s) from pool\n",
            cdo->name, idstr);});

  if(!mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_POOLED)) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("CDO `%s' (id %s) not pooled, cannot remove it\n",
            cdo->name, idstr););
    return MSTRO_INVARG;
  }

  mstro_status status = mstro_pool__remove(cdo, MSTRO_CDO_STATE_WITHDRAWN);

  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to remove CDO %s (id %s) from pool\n",
            cdo->name, idstr););
    return status;
  } else {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        INFO("Withdrew CDO `%s' (id %s) from pool\n",
             cdo->name, idstr););

    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_WITHDRAW, 1);
    return MSTRO_OK;
  }

  return MSTRO_UNIMPL;
}

mstro_status
mstro_cdo_require_async(mstro_cdo cdo, mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status;
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  /* cdo should be sealed */
  if(!mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {
      ERR("CDO `%s' state %d illegal for REQUIRE operation\n",
          cdo->name, cdo->state);
      return MSTRO_INVARG;
  }

  /** fill up the request handler */
  DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
  status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
  if(status!=MSTRO_OK) {
    ERR("Can not allocate memory for request object \n");
    return status;
  }
  (*request)->cdo.cdo = cdo;
  (*request)->cdo.target_state = MSTRO_CDO_STATE_REQUIRED;
  (*request)->cdo.orig_state = MSTRO_CDO_STATE_SEALED;

  /* We have a sealed CDO. Good. */
  WITH_CDO_ID_STR(
      id, &cdo->gid,
      {
        status = mstro_pool__add(cdo, MSTRO_CDO_STATE_REQUIRED_LOCALLY);

        if(status!=MSTRO_OK) {
          ERR("Failed to add CDO %s (id %s) to local pool\n", cdo->name, id);
          return status;
        } else {
          assert(cdo->gid.qw[0]!=MSTRO_CDO_ID_NULL.qw[0]
               && cdo->gid.qw[1]!=MSTRO_CDO_ID_NULL.qw[1]
               && cdo->gid.local_id!=MSTRO_CDO_ID_NULL.local_id);

          INFO("Added CDO `%s' (id %s) REQUIRE to local pool\n",
               cdo->name, id);

          /* FIXME: move this and other pmp_send ops to pool.c */
          /* tell pool manager */
          Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
          cdoid.qw0 = cdo->gid.qw[0];
          cdoid.qw1 = cdo->gid.qw[1];
          cdoid.local_id = cdo->gid.local_id;

          Mstro__Pool__Require require = MSTRO__POOL__REQUIRE__INIT;
          require.cdoid = &cdoid;

          Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
          status = mstro_pmp_package(&msg, (ProtobufCMessage*)&require);
          if(status!=MSTRO_OK) {
            ERR("Failed to package %s into a pool manager message\n",
                require.base.descriptor->name);
            return status;
          }

          /* FIXME: do we WAit or an ACK here? */
          status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
          if(status!=MSTRO_OK) {
            if(status==MSTRO_NO_PM) {
              DEBUG("require local only, no PM\n");
              mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_REQUIRED);
              status = mstro_pool__notify(cdo);
  
              WITH_CDO_ID_STR(idstr, &cdo->gid, {
                  DEBUG("CDO %s now in state %s\n",
                        idstr, mstro_cdo_state_describe(mstro_cdo_state_get(cdo)));
                });

            } else {
              ERR("Failed to send REQUIRE message to pool manager: %d (%s)\n",
                  status, mstro_status_description(status));
              return status;
            }
          }
          mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_ASYNC_REQUIRE, 1);
          return MSTRO_OK;
        }
      });
  /* not reached */
  return MSTRO_UNIMPL;
}


mstro_status
mstro_cdo_require(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status;
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  NOISE("Requiring CDO `%s' from pool\n", cdo->name);

  /* some code duplication (structurally) with OFFER, but we keep it
   * that way for now */
  if(!mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED)) {
    if(mstro_cdo_state_check(cdo,
                             MSTRO_CDO_STATE_CREATED
                             | MSTRO_CDO_STATE_DECLARED)) {
      DEBUG("Auto-sealing CDO `%s'\n", cdo->name);
      status = mstro_cdo_declaration_seal(cdo);
      if(status!=MSTRO_OK) {
        return status;
      }
    } else {
      WITH_CDO_ID_STR(lidstr, &cdo->id, {
          WITH_CDO_ID_STR(gidstr, &cdo->gid, {
              ERR("CDO `%s' (lid %s, gid %s) state %d (%s) illegal for REQUIRE operation\n",
                  cdo->name, lidstr, gidstr,
                  cdo->state, mstro_cdo_state_describe(cdo->state));});});
      return MSTRO_INVARG;
    }
  }

  /* We have a sealed CDO. Good. */
  WITH_CDO_ID_STR(lidstr, &cdo->id, {
      WITH_CDO_ID_STR(gidstr, &cdo->gid, {
          NOISE("Requiring CDO `%s' (lid %s, gid %s) from pool\n",
               cdo->name, lidstr, gidstr);});});


  WITH_CDO_ID_STR(id, &cdo->gid, {
      status = mstro_pool__add(cdo, MSTRO_CDO_STATE_REQUIRED_LOCALLY);
      if(status!=MSTRO_OK) {
        ERR("Failed to add CDO %s (id %s) to local pool\n", cdo->name, id);
        return status;
      } else {
        assert(cdo->gid.qw[0]!=MSTRO_CDO_ID_NULL.qw[0]
               && cdo->gid.qw[1]!=MSTRO_CDO_ID_NULL.qw[1]
               && cdo->gid.local_id!=MSTRO_CDO_ID_NULL.local_id);
        INFO("Added CDO `%s' (id %s) REQUIRE to local pool\n",
             cdo->name, id);

          /* FIXME: move this and other pmp_send ops to pool.c */
          /* tell pool manager */
          Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
          cdoid.qw0 = cdo->gid.qw[0];
          cdoid.qw1 = cdo->gid.qw[1];
          cdoid.local_id = cdo->gid.local_id;

          Mstro__Pool__Require require = MSTRO__POOL__REQUIRE__INIT;
          require.cdoid = &cdoid;

          Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
          status = mstro_pmp_package(&msg, (ProtobufCMessage*)&require);
          if(status!=MSTRO_OK) {
            ERR("Failed to package %s into a pool manager message\n",
                require.base.descriptor->name);
            return status;
          }

          status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
          if(status!=MSTRO_OK) {
            if(status==MSTRO_NO_PM) {
              DEBUG("require local only, no PM\n");
              mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_REQUIRED);
              status = mstro_pool__notify(cdo);
  
              WITH_CDO_ID_STR(idstr, &cdo->gid, {
                  DEBUG("CDO %s now in state %s\n",
                        idstr, mstro_cdo_state_describe(mstro_cdo_state_get(cdo)));
                });

            } else {
              ERR("Failed to send REQUIRE message to pool manager: %d (%s)\n",
                  status, mstro_status_description(status));
              return status;
            }
          }
          mstro_cdo_block_until(cdo,MSTRO_CDO_STATE_REQUIRED,"REQUIRED");
          mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_REQUIRE, 1);
          return MSTRO_OK;
        }
      });
  /* not reached */
  return MSTRO_UNIMPL;
}

mstro_status
mstro_cdo_demand(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  WITH_CDO_ID_STR(lidstr, &cdo->id, {
      WITH_CDO_ID_STR(gidstr, &cdo->gid, {
          DEBUG("Demanding %s (lid: %s, gid: %s)\n",
                cdo->name, lidstr, gidstr);});});

  mstro_cdo_state tmp = mstro_cdo_state_get(cdo);
  if(tmp != MSTRO_CDO_STATE_REQUIRED) {
    if (tmp == MSTRO_CDO_STATE_REQUIRED_LOCALLY) {
      /* PM-route: wait for ACK to set required state */
      mstro_cdo_block_until(cdo, MSTRO_CDO_STATE_REQUIRED, "REQUIRED");
    } else {
      WITH_CDO_ID_STR(
          idstr, &cdo->gid,
          ERR("CDO `%s' (id %s) not REQUIRED in pool, cannot DEMAND it\n",
              cdo->name, idstr););
      return MSTRO_INVARG;
    } 
  }

  mstro_status status = mstro_pool__demand(cdo, MSTRO_CDO_STATE_DEMANDED);

  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to demand CDO %s (id %s) from pool\n",
            cdo->name, idstr););
    return status;
  } else {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        INFO("Demanded CDO `%s' (id %s) from pool\n",
             cdo->name, idstr););

    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_DEMAND, 1);
    return MSTRO_OK;
  }

  return MSTRO_UNIMPL;
}

mstro_status
mstro_cdo_demand_async(mstro_cdo cdo, mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();
  mstro_status status = MSTRO_UNIMPL;

  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  WITH_CDO_ID_STR(idstr, &cdo->gid, {
      INFO("Demanding CDO `%s' (id %s) from pool\n",
            cdo->name, idstr);});

  mstro_cdo_state tmp = mstro_cdo_state_get(cdo);
  if(tmp != MSTRO_CDO_STATE_REQUIRED) {
      WITH_CDO_ID_STR(
          idstr, &cdo->gid,
          ERR("CDO `%s' (id %s) not REQUIRED in pool, cannot DEMAND it\n",
              cdo->name, idstr););
      return MSTRO_INVARG;
  }

  status =  mstro_pool__demand_async(cdo, MSTRO_CDO_STATE_DEMANDED);

  if (status == MSTRO_OK) {
    /** fill up the request handler */
    DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
    status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
    if(status!=MSTRO_OK) {
      ERR("Can not allocate memory for request object \n");
      return status;
    }
    (*request)->cdo.cdo = cdo;
    (*request)->cdo.orig_state = MSTRO_CDO_STATE_REQUIRED;
    
    if(atomic_load(&g_mstro_pm_attached)) {
      /** stage 1 is that the PM statify the request*/
      (*request)->cdo.target_state = MSTRO_CDO_STATE_SATISFIED;
      DEBUG("request target state is %s \n",
            mstro_cdo_state_describe((*request)->cdo.target_state));
    } else {
      /** need to check the local pool */
      (*request)->cdo.target_state = MSTRO_CDO_STATE_DEMANDED;
    }

    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL,
                            MSTRO_STATS_L_PC_NUM_ASYNC_DEMAND, 1);
  }

  return status;
}


mstro_status
mstro_cdo_retract_async(mstro_cdo cdo, mstro_request *request)
{
  MSTRO_API_NEEDS_INIT();
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  WITH_CDO_ID_STR(idstr, &cdo->gid, {
      DEBUG("Retracting CDO `%s' (id %s) from pool\n",
            cdo->name, idstr);});

  mstro_cdo_state tmp = mstro_cdo_state_get(cdo);
  if(tmp != MSTRO_CDO_STATE_REQUIRED) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("CDO `%s' (id %s) not REQUIRED in pool, cannot RETRACT it\n",
            cdo->name, idstr););
    return MSTRO_INVARG;
  } 
  
  mstro_status status = mstro_pool__remove_async(cdo, MSTRO_CDO_STATE_RETRACTED);

  if (status == MSTRO_OK ){

    /** fill up the request handler */
    DEBUG("Allocate mstro_request struct and save cdo handle and target state to it\n");
    status = mstro_request__allocate(MSTRO_REQUEST_CDO, request);
    if(status!=MSTRO_OK) {
      ERR("Can not allocate memory for request object \n");
      return status;
    }
    (*request)->cdo.cdo = cdo;
    (*request)->cdo.target_state = MSTRO_CDO_STATE_RETRACTED_GLOBALLY;
    (*request)->cdo.orig_state = MSTRO_CDO_STATE_REQUIRED;
    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL,
                            MSTRO_STATS_L_PC_NUM_ASYNC_RETRACT, 1);
  }
  
  return status;
}

mstro_status
mstro_cdo_retract(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  WITH_CDO_ID_STR(idstr, &cdo->gid, {
      DEBUG("Retracting CDO `%s' (id %s) from pool\n",
            cdo->name, idstr);});

  mstro_cdo_state tmp = mstro_cdo_state_get(cdo);
  if(tmp != MSTRO_CDO_STATE_REQUIRED
   &&tmp != MSTRO_CDO_STATE_REQUIRED_LOCALLY) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("CDO `%s' (id %s) not REQUIRED in pool, cannot RETRACT it\n",
            cdo->name, idstr););
    return MSTRO_INVARG;
  }

  mstro_status status = mstro_pool__retract(cdo, MSTRO_CDO_STATE_RETRACTED);

  if(status!=MSTRO_OK) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("Failed to retract CDO `%s` (id %s) from pool\n",
            cdo->name, idstr););
  } else {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        INFO("Retracted CDO `%s' (id %s) from pool\n",
             cdo->name, idstr););
    mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_RETRACT, 1);
  }

  return status;
}

static inline
mstro_status
mstro_cdo_dispose__propagate(mstro_cdo cdo)
{
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }

  if(! mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_DISPOSABLE)) {
    WITH_CDO_ID_STR(
        idstr, &cdo->gid,
        ERR("CDO `%s' (id %s) state %d, not disposable\n",
            cdo->name, idstr, cdo->state););
    return MSTRO_INVARG;
  }
  /* maybe tell PM */
  if(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_SEALED
                           | MSTRO_CDO_STATE_WITHDRAWN
                           | MSTRO_CDO_STATE_DEMANDED
                           | MSTRO_CDO_STATE_RETRACTED)) {
    mstro_status status;
    Mstro__Pool__CDOID cdoid = MSTRO__POOL__CDOID__INIT;
    cdoid.qw0 = cdo->gid.qw[0];
    cdoid.qw1 = cdo->gid.qw[1];
    cdoid.local_id = cdo->gid.local_id;

    Mstro__Pool__Dispose dispose = MSTRO__POOL__DISPOSE__INIT;
    dispose.cdoid = &cdoid;

    Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
    status = mstro_pmp_package(&msg, (ProtobufCMessage*)&dispose);
    if(status!=MSTRO_OK) {
      ERR("Failed to package %s into a pool manager message\n",
          dispose.base.descriptor->name);
      return status;
    }

    status =mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
    if(status!=MSTRO_OK) {
      if(status==MSTRO_NO_PM) {
        DEBUG("dispose local only, no PM\n");
      } else {
        ERR("Failed to send DISPOSE message to pool manager: %d (%s)\n",
            status, mstro_status_description(status));
        return status;
      }
    }
  }

  /* We do not wait for the PM to send an ack --

     We permit DISPOSE after the CDO is (locally) in state

     - DECLARED (no PM involved),

     - SEALED (PM has no offer or require or demand, so cannot have
       dependencies that need waiting)

     - WITHDRAWN (PM granted WITHDRAW, so can’t require more wait)

     - DEMANDED (after DEMAND completed the resources are the local
       app’s, so PM can’t rely on us satisfying any transfer requests)

     - RETRACTED (OFFER withdrawn, so situation like SEALED)

     So a DISPOSE to the PM is mostly for bookkeeping on the PM, and
     should not require an ack. If they’re unhappy with us (still busy
     handling incoming DISPOSEs out of order) they can still keep us
     waiting at LEAVE.
  */
  /* mstro_cdo_block_until(cdo, MSTRO_CDO_STATE_DISPOSED_GLOBALLY, "DISPOSED_GLOBALLY"); */

  /* drop from live CDO table */
  struct cdo_table_entry *res;
  pthread_mutex_lock(&g_live_cdo_table_mtx);
  HASH_FIND(hh, g_live_cdo_table,
            &cdo->gid, sizeof(struct mstro_cdo_id), res);
  if(res) {
    HASH_DEL(g_live_cdo_table, res);
  } else {
    ERR("DISPOSABLE CDO not in live-cdo-table!?\n");
  }
  pthread_mutex_unlock(&g_live_cdo_table_mtx);
  free(res);

  return MSTRO_UNIMPL;
}
  
mstro_status
mstro_cdo_dispose(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  // Bookkeeping
  mstro_status s;
  s = mstro_cdo_dispose__propagate(cdo);
  if (s != MSTRO_OK && s != MSTRO_UNIMPL)
    // already printed an error
    return s;

  // Free resources
  /* Careful! We can only get name, id, etc. *before* the disposal
   * happens.
   * FIXME: This should actually be #ifdef-guarded depending on log
   * level to save overhead for high CDO disposal rate situations. */
  WITH_CDO_ID_STR(
      idstr, &cdo->gid,
      {
        INFO("Disposing CDO `%s' (id %s)\n",
             cdo->name, idstr);

        mstro_status status = mstro_cdo__free(&cdo);
        if(status!=MSTRO_OK) {
          ERR("Failed to deallocate CDO (id %s)\n", idstr);
          return status;
        } 
      });

  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_DISPOSE, 1);

  return MSTRO_OK;
}

// TODO make it more efficient by using one new pool message (UNSEAL?) to avoid
// doing dispose and declare messages. Then *_propagate "bypass" functions can go.
mstro_status
mstro_cdo_dispose_and_reuse(mstro_cdo cdo)
{
  MSTRO_API_NEEDS_INIT();
  if(cdo==NULL) {
    ERR("NULL CDO invalid\n");
    return MSTRO_INVARG;
  }
  mstro_status s;

  s = mstro_cdo_dispose__propagate(cdo); // will not free *cdo*
  if (s != MSTRO_OK && s != MSTRO_UNIMPL)
    // already printed an error
    return s;

  s = mstro_cdo_declare__propagate(cdo->name, &cdo); // will not alloc *cdo*, will not alter user attributes
  if (s != MSTRO_OK)
    // already printed an error
    return s;

  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL,
                          MSTRO_STATS_L_PC_NUM_DISPOSE, 1);

  return MSTRO_OK;
}

static inline
mstro_status
mstro_cdo__mamba_copy(mstro_cdo src, mstro_cdo dst)
{
  mstro_status status;

  if(src==NULL || dst==NULL) {
    ERR("Invalid src/dst arg\n");
    return MSTRO_INVARG;
  }
  if(src->mamba_array==NULL || dst->mamba_array==NULL) {
    ERR("Missing src/dst mamba array\n");
    return MSTRO_INVARG;
  }

  DEBUG("mamba copy. SRC raw ptr %p, mamba raw ptr %p, DST raw ptr %p, mamba raw ptr %p\n",
       src->raw_ptr, src->mamba_array->allocation->ptr,
       dst->raw_ptr, dst->mamba_array->allocation->ptr);
  mmbError stat = mmb_array_copy(
		  dst->mamba_array,
		  src->mamba_array);
  if(stat != MMB_OK) {
    ERR("Failed to copy mamba arrays: %d (%s)\n",
		    stat, mmb_error_description(stat));
    status = MSTRO_FAIL;
    goto BAILOUT;
  }
  status = MSTRO_OK;
BAILOUT:
  DEBUG("Returning from mamba copy: %d\n", status);
  return status;
}

/** adjust CDO storage to 0 if possible. Changes the local-size attribute appropriately */
static inline
mstro_status
mstro_cdo__truncate_space(mstro_cdo cdo)
{
  int64_t zero = 0;
  mstro_status status = MSTRO_FAIL;
  void *null_ptr =  NULL;
  if(cdo->mamba_array==NULL) {
    assert(cdo->raw_ptr==NULL); /* otherwise a mamba_array should exist around it */
    DEBUG("Both CDOs type 0, fine\n");
  } else {
    if(cdo->raw_ptr==NULL) {
      /* mamba array but no raw ptr */
      WARN("FIXME: shrinking mamba array not implemented until mamba 0.1.6\n");
      /* FIXME: use
         mmbDimension d = { 0 }; // or a correct version of this
         s = mmb_array_resize(cdo->mmb_array, d);
      */
      WARN("Freeing mamba allocation on cdo; if it was user-allocated they may be surprised\n");
      mmbError r = mmb_array_destroy(cdo->mamba_array);
      if(r!=MMB_OK) {
        ERR("Failed to deallocate mamba array: %d\n", r);
        return MSTRO_FAIL;
      }
      cdo->mamba_array=NULL;
    } else {
      ERR("Cannot handle incoming type-0 CDO to CDO with preallocated raw-ptr\n");
      return MSTRO_FAIL;
    }
  
    /* set raw ptr attribute to null, in case someone is asking for the raw ptr */
    status = mstro_attribute_dict_set(cdo->attributes,
                                      MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                      MSTRO_CDO_ATTR_VALUE_pointer,
                                      null_ptr, true, false);
    assert(status == MSTRO_OK);
  }
  return mstro_attribute_dict_set(cdo->attributes,
                                  MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                  MSTRO_CDO_ATTR_VALUE_int64,
                                  &zero, true, false);
}

/** adjust CDO storage to SIZE if possible. Changes the local-size attribute appropriately */
mstro_status
mstro_cdo__adjust_space(mstro_cdo cdo, int64_t size, int policy)
{
  DEBUG("Raw ptr %p, mamba_array %p, wanted size %" PRId64 "\n", cdo->raw_ptr, cdo->mamba_array, size);

  if (size < 0)
    ERR("Invalid size (%" PRIx64 ")\n", size);
  if (size == 0) {
    void *null_ptr = NULL;
    NOISE("Receiving 0-size CDO\n");
    /*set the raw ptr attribute of the incoming cdo to NULL*/
    return mstro_attribute_dict_set(cdo->attributes,
                                    MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                    MSTRO_CDO_ATTR_VALUE_pointer,
                                    &null_ptr, true, false);
  }
  
  if(cdo->mamba_array==NULL) {
    if(cdo->raw_ptr!=NULL) {
      ERR("Should not happen: CDO has raw-ptr but no mamba array\n");
      return MSTRO_FAIL;
    }

    /* both mamba array and raw-ptr are NULL */
    mstro_status s = mstro_attribute_dict_set(cdo->attributes,
                                              MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                              MSTRO_CDO_ATTR_VALUE_int64, &size, true, false);
    if(s!=MSTRO_OK) {
      ERR("Failed to set size on cdo\n");
      return s;
    }
    /* need to create space on DST */
    return mstro_cdo__create_mamba_array(cdo, policy);

  } else {
    mstro_status s=MSTRO_UNIMPL;
    const void *currentsizeptr=NULL;
    enum mstro_cdo_attr_value_type vt;
    s = mstro_attribute_dict_get(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                 &vt, &currentsizeptr, NULL, false);
    if(s==MSTRO_NOENT && vt==MSTRO_CDO_ATTR_VALUE_INVALID) {
      ERR("CDO has mamba-array but no local-size\n");
      return MSTRO_FAIL;
    }
    if(s!=MSTRO_OK) {
      ERR("Failed to retrieve local-size on CDO\n");
      return MSTRO_FAIL;
    }

    int64_t avail = *(int64_t*)currentsizeptr;
    if(avail==size) {
      DEBUG("Sizes match, no need to adjust CDO space\n");
      return MSTRO_OK;
    } else {
      if(cdo->raw_ptr==NULL) {
        /* mamba array but no raw ptr */
        WARN("FIXME: shrinking mamba array not implemented until mamba 0.1.6\n");
        /* FIXME: use
           mmbDimension d = { 0 }; // or a correct version of this
           s = mmb_array_resize(cdo->mmb_array, d);
        */
        s=mstro_attribute_dict_set(cdo->attributes,
                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                   MSTRO_CDO_ATTR_VALUE_int64,
                                   &size, true, false);
        if(s!=MSTRO_OK) {
          ERR("Failed to set new local-size for CDO\n");
          return s;
        }

        WARN("Freeing mamba allocation on cdo; if it was user-allocated they may be surprised\n");
        mmbError r = mmb_array_destroy(cdo->mamba_array);
        if(r!=MMB_OK) {
          ERR("Failed to deallocate mamba array: %d\n", r);
          return MSTRO_FAIL;
        }
        return mstro_cdo_allocate_data(cdo);
      } else {
        ERR("Cannot handle incoming CDO size (%" PRId64 ") with size different from preallocated raw-ptr size (%" PRId64 ") \n", size, avail);
        return MSTRO_FAIL;
      }
    }
  }
}

mstro_status
mstro_cdo__satisfy(mstro_cdo src, mstro_cdo dst)
{
  if(src==NULL || dst==NULL) {
    ERR("NULL CDO illegal\n");
    return MSTRO_INVARG;
  }

  if(src==dst) {
    ERR("should not be called on identical CDO handles\n");
    return MSTRO_INVARG;
  }

  if(! mstro_cdo_state_check(src, MSTRO_CDO_STATE_OFFERED
                             |MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY)) {
    ERR("Source CDO not in OFFERED state\n");
    return MSTRO_INVARG;
  }

  if(! mstro_cdo_state_check(dst, MSTRO_CDO_STATE_REQUIRED)) {
    ERR("Destination CDO not in REQUIRED state\n");
    return MSTRO_INVARG;
  }

  if(! mstro_cdo_id__equal_uuid_only(&src->gid, &dst->gid)) {
    ERR("CDOs have different CDO-IDs (despite ignoring local-id)\n");
    return MSTRO_INVARG;
  }

  mstro_status s = MSTRO_UNIMPL;

#define SRC_SIZE_UNKNOWN -2
  int64_t src_size=SRC_SIZE_UNKNOWN; /* = unitialized */

  DEBUG("Ready to copy data between matching CDOs\n");
  if(src->mamba_array==NULL) {
    /* type-0 CDO on SRC */
    src_size=0;
    s=mstro_cdo__truncate_space(dst);
  } else {
    /* SRC has data */
    const void *srclenptr=NULL;
    s = mstro_attribute_dict_get(src->attributes,
                                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                 NULL, &srclenptr, NULL, false);
    if(s!=MSTRO_OK) {
      ERR("SRC has no local-size attribute\n");
      return MSTRO_FAIL;
    }
    src_size = *(int64_t*)srclenptr;
    DEBUG("Incoming size %" PRIi64 "\n", src_size);

    s = mstro_cdo__adjust_space(dst, src_size, MSTRO_ALLOC_POLICY_NONE);
    if(s!=MSTRO_OK) {
      ERR("Failed to adjust space of CDO destination handle\n");
      return s;
    }


    /* FIXME: once mamba_copy takes into account layout properties we need to adjust dst->attributes to avoid doing transformations twice when we do the transform below */

  }

  size_t num_precious_attr = 0;
  const char **precious_attributes = NULL;

  s = mstro_transform_layout(src, dst,
                             src->attributes, dst->attributes);
  /* FIXME: precious attr should be a return value of transform_layout */

  if(s!=MSTRO_OK) {
    if(s==MSTRO_NOMATCH) {
      /* no applicable transformation */
      DEBUG("No transformation applied\n");
      if (src->mamba_array!=NULL) {
        s = mstro_cdo__mamba_copy(src, dst);
        if(s!=MSTRO_OK) {
          ERR("Failed to perform mamba copy\n");
          return s;
        }
      }
    } else {
      ERR("Failure trying to apply a transformation\n");
      return s;
    }
  } else {
    DEBUG("Some transformation applied\n");
  }
  /* FIXME: we need a way to know the precious attributes that cannot
     be overwritten by the attribute copy, because they have been
     taken care of by the transformation.  These should really be the
     union of an explicit list (like raw-ptr, mamba-array), plus some
     returned from transform_layout above */

  WARN("Copying attributes could be more efficient here\n");
  /* FIXME: we should use a 'merge-dictionary' function instead */
  if(src->attributes_msg==NULL) {
    ERR("Source CDO has no attributes message available (should have been built at SEAL time)\n");
    return MSTRO_FAIL;
  }
  s = mstro_cdo_attributes_update_incoming(dst, src->attributes_msg,
                                           num_precious_attr, precious_attributes);
  if(s!=MSTRO_OK) {
    WITH_CDO_ID_STR(idstr, &dst->gid,
                    ERR("Failed to update CDO |%s| (%p) attributes from src data: %d (%s)\n",
                        idstr, dst, s, mstro_status_description(s)););
  }

  return s;
}


/* FIXME: improve */
bool
mstro_cdo_accessible(mstro_cdo cdo)
{
  if(cdo!=NULL
     && !mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_POOLED)
     )
    return true;
  else
    return false;
}


mstro_status
mstro_cdo_access_ptr(mstro_cdo cdo, void **data, int64_t *data_len)
{
  if(cdo==NULL)
    return MSTRO_INVARG;
  if(data==NULL && data_len==NULL)
    return MSTRO_INVOUT;

  if(!mstro_cdo_accessible(cdo)) {
    ERR("CDO not currently accessible\n");
    return MSTRO_INVARG;
  }

  mstro_status s=MSTRO_OK;

  /* we can't simply use cdo->raw_ptr, as we may not have a sealed CDO */

  /* we need a r/w ref, so don't call the public CDO API. FIXME: do not cast away constness here... */
  if(data!=NULL) {
    void *ptr=NULL;

    s = mstro_attribute_dict_get(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                 NULL, (const void**)&ptr, NULL, false);
    if(s!=MSTRO_OK) {
      ERR("Cannot retrieve the raw-ptr on CDO: %s\n", mstro_status_description(s));
      return s;
    }
    *data = ptr;
  }
  if(data_len!=NULL) {
    const int64_t *tmp;
    s = mstro_attribute_dict_get(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                 NULL,(const void**)&tmp, NULL, true);
    *data_len = *tmp;
  }
  return s;
}

mstro_status
mstro_cdo_access_mamba_array(mstro_cdo cdo, mmbArray **array)
{
  if(cdo==NULL)
    return MSTRO_INVARG;
  if(array==NULL)
    return MSTRO_INVOUT;

  if(!mstro_cdo_accessible(cdo)) {
    ERR("CDO not currently accessible\n");
    return MSTRO_INVARG;
  }

  *array = cdo->mamba_array;
  return MSTRO_OK;
}



mstro_status
mstro_cdo_declare_completion(uint64_t serial,
                             const struct mstro_cdo_id *global_id,
                             uint64_t channel)
{
  if(global_id==NULL)
    return MSTRO_INVARG;

  // PM should not modify this, and we used it as the serial
  assert(serial==global_id->local_id);

  mstro_status status;
  mstro_cdo cdo;

  /* find outstanding declare for it */
  pthread_mutex_lock(&g_outstanding_cdo_decl_table_mtx);

  HASH_FIND(hh, g_outstanding_cdo_decl_table, &serial, sizeof(serial), cdo);
  if(cdo==NULL) {
    ERR("Cannot find serial %zu among outstanding declarations\n", serial);
    status=MSTRO_INVARG;
  } else {
    assert(serial==cdo->id.local_id);

    HASH_DEL(g_outstanding_cdo_decl_table, cdo);
    pthread_mutex_unlock(&g_outstanding_cdo_decl_table_mtx);

    cdo->gid.qw[0] = global_id->qw[0];
    cdo->gid.qw[1] = global_id->qw[1];
    cdo->gid.local_id = global_id->local_id;
    cdo->channel = channel;

    WITH_CDO_ID_STR(idstr,&cdo->gid, {
        DEBUG("CDO %s now has global CDOID %s\n", cdo->name, idstr);
      });

    /* add to CDO-ID->CDO table */
    struct cdo_table_entry *e = malloc(sizeof(struct cdo_table_entry));
    if(e==NULL) {
      ERR("Cannot allocate CDO table entry\n");
      status = MSTRO_NOMEM;
    } else {
      e->cdoid = *global_id;
      e->cdo = cdo;
      pthread_mutex_lock(&g_live_cdo_table_mtx);
      HASH_ADD(hh, g_live_cdo_table, cdoid, sizeof(e->cdoid), e);
      pthread_mutex_unlock(&g_live_cdo_table_mtx);

      /* make others notice */
      mstro_cdo_state_set(cdo, MSTRO_CDO_STATE_DECLARED);
      status=MSTRO_OK;
    }
  }
  return status;
}

mstro_status
mstro_cdo__find_cdo(const struct mstro_cdo_id *id,
                    mstro_cdo *cdo)
{
  if(id==NULL) return MSTRO_INVARG;
  if(cdo==NULL) return MSTRO_INVOUT;

  WITH_CDO_ID_STR(idstr, id,
                  DEBUG("Looking for %s in live-cdo-table\n", idstr););
  struct cdo_table_entry *res;
  pthread_mutex_lock(&g_live_cdo_table_mtx);
  HASH_FIND(hh, g_live_cdo_table, id, sizeof(struct mstro_cdo_id), res);
  pthread_mutex_unlock(&g_live_cdo_table_mtx);

  if(res)
    *cdo = res->cdo;
  else
    *cdo = NULL;

  return *cdo==NULL ? MSTRO_FAIL : MSTRO_OK;

}


mstro_status
mstro_cdo__mark_transfer_complete(mstro_cdo cdo)
{
  mstro_status status=MSTRO_UNIMPL;

  mstro_cdo_state s = cdo->state;
  if (!(cdo->state & MSTRO_CDO_STATE_IN_TRANSPORT)) 
    WARN("Unexpected CDO state `%s` , it should be marked IN_TRANSPORT", mstro_cdo_state_describe(cdo->state));

  int64_t n_pieces;

  /* reduce the number of outstanding segments by 1 as we received one part*/
  n_pieces = atomic_fetch_sub(&cdo->n_segments, 1);
  
  assert(n_pieces > 0); /* there is some outstanding communication */
  n_pieces--; /* decrement n_pieces to reflect cdo->n_segments after fetch and sub operation */

  if (n_pieces != 0)
  {
    DEBUG("There are %zu outstanding pieces for CDO %zu \n", n_pieces, cdo->id.local_id);
    return MSTRO_OK;
  }
  

  /* clear in-transport flag */
  s = s & ~(MSTRO_CDO_STATE_IN_TRANSPORT);
  switch(s) {
    case MSTRO_CDO_STATE_REQUIRED:
      mstro_cdo_state_set(cdo, s
                          /** satisfied */
                          |MSTRO_CDO_STATE_SATISFIED
                          /** behave like offer */
                          /* FIXME: this is a bit weird */
                          |MSTRO_CDO_STATE_OFFERED);
      status = mstro_pool__notify(cdo);
      break;
    case MSTRO_CDO_STATE_DEMANDED:
      mstro_cdo_state_set(cdo, s
                          /** satisfied */
                          |MSTRO_CDO_STATE_SATISFIED
                          /** behave like offer */
                          /* FIXME: this is a bit weird */
                          |MSTRO_CDO_STATE_OFFERED);
      status = mstro_pool__notify(cdo);
      break;
    default:
      ERR("Unexpected CDO state %d\n", cdo->state);
      status = MSTRO_FAIL;
  }

  goto BAILOUT;
BAILOUT:
  return status;
}

static const
struct {
    int32_t state;
    const char *name;
} states_and_names[] = {
  {MSTRO_CDO_STATE_INVALID,            "INVALID"},
  {MSTRO_CDO_STATE_CREATED,            "|CREATED"},
  {MSTRO_CDO_STATE_DECLARED,           "|DECLARED"},
  {MSTRO_CDO_STATE_SEALED,             "|SEALED"},
  {MSTRO_CDO_STATE_OFFERED_LOCALLY,    "|OFFERED_LOCALLY"},
  {MSTRO_CDO_STATE_OFFERED,            "|OFFERED"},
  {MSTRO_CDO_STATE_REQUIRED_LOCALLY,   "|REQUIRED_LOCALLY"},
  {MSTRO_CDO_STATE_REQUIRED,           "|REQUIRED"},
  {MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY, "|WITHDRAWN_GLOBALLY"},
  {MSTRO_CDO_STATE_WITHDRAWN,          "|WITHDRAWN"},
  {MSTRO_CDO_STATE_DEMANDED,           "|DEMANDED"},
  {MSTRO_CDO_STATE_RETRACTED,          "|RETRACTED"},
  {MSTRO_CDO_STATE_RETRACTED_GLOBALLY, "|RETRACTED_GLOBALLY"},
  {MSTRO_CDO_STATE_DEAD,               "|DEAD"},
  {    (1U<<13),                       "|inv13"},
  {    (1U<<14),                       "|inv14"},
  {    (1U<<15),                       "|inv15"},
  {    (1U<<16),                       "|inv16"},
  {    (1U<<17),                       "|inv17"},
  {    (1U<<18),                       "|inv18"},
  {    (1U<<19),                       "|inv19"},
  {    (1U<<20),                       "|inv20"},
  {    (1U<<21),                       "|inv21"},
  {    (1U<<22),                       "|inv22"},
  {    (1U<<23),                       "|inv23"},
  {    (1U<<24),                       "|inv24"},
  {    (1U<<25),                       "|inv25"},
  {    (1U<<26),                       "|inv26"},
  {    (1U<<27),                       "|inv27"},
  {    (1U<<28),                       "|inv28"},
  {MSTRO_CDO_STATE_INJECTED,           "|INJECTED"},
  {MSTRO_CDO_STATE_IN_TRANSPORT,       "|IN_TRANSPORT"},
  {MSTRO_CDO_STATE_SATISFIED,          "|SATISFIED"},
  /* multi-states */
  {MSTRO_CDO_STATE_POOLED,             ",POOLED"},
  {MSTRO_CDO_STATE_RETURNED,           ",RETURNED"},
  {MSTRO_CDO_STATE_DISPOSABLE,         ",DISPOSABLE"},
  };

/** a reasonably safe way to count the number of entries in an array of structures */
#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))


/** we support this many concurrent state buffers in flight per
 * thread. Hideous, but callers typically will use this function in a
 * PRINT statement, with up to two states (to compare them) at most
 */

#define MAX_STATE_DESCRIPTION_BUFFERS 3
const char *
mstro_cdo_state_describe(mstro_cdo_state s)
{
  static _Thread_local char buf[MAX_STATE_DESCRIPTION_BUFFERS][sizeof(states_and_names)];
  static _Thread_local size_t bufid = 0;

  // switch to new buffer
  bufid = (bufid+1) % MAX_STATE_DESCRIPTION_BUFFERS;

  buf[bufid][0]='\0';

  if(s==MSTRO_CDO_STATE_INVALID)
    return "INVALID";
  else {
    for(size_t i=0; i<COUNT_OF(states_and_names); i++) {
      if(s&states_and_names[i].state) {
        strcat(buf[bufid], states_and_names[i].name);
      }
    }

    return buf[bufid]+1; /* cut off leading '|' we put there for simplicity
                   * of the loop above */
  }
}

mstro_status
mstro_cdo_attributes_update_incoming(mstro_cdo cdo,
                                     const Mstro__Pool__Attributes *attr,
                                     size_t num_precious_attr,
                                     const char **precious_attributes)
{
  if(cdo==NULL || attr==NULL)
    return MSTRO_INVARG;

  if(attr->default_namespace && attr->default_namespace[0]!='\0') {
    WARN("Ignoring default-namespace |%s| on incoming attributes\n",
         attr->default_namespace);
  }

  switch(attr->val_case) {
    case MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP:
      /* ok */
      break;
    default:
      ERR("Unsupported attribute encoding: %d\n", attr->val_case);
      return MSTRO_FAIL;
  }

  const Mstro__Pool__Attributes__Map *map = attr->kv_map;
  if(map==NULL) {
    DEBUG("Empty attributes (class 0 CDO incoming?)\n");
    return MSTRO_OK;
  }

  mstro_status s, retstat;

  retstat=MSTRO_OK;

  DEBUG("%d attributes incoming\n", map->n_map);

  for(size_t i=0; i<map->n_map; i++) {
    const Mstro__Pool__KvEntry *entry = map->map[i];
    const char *key = entry->key;
    const Mstro__Pool__AVal * aval = entry->val;

    DEBUG("key len: %d (%s)\n", strlen(key), key);

    enum mstro_cdo_attr_value_type valtype;
    const void *val=NULL;
    s = mstro_attribute_dict_get(cdo->attributes, key,
                                 &valtype, &val, NULL, false);
    switch(s) {
      case MSTRO_NOENT: {
        DEBUG("attribute not set on destination CDO\n");
        if(valtype==MSTRO_CDO_ATTR_VALUE_NONE) {
          /* no value set: copy */
          WITH_CDO_ID_STR(idstr, &cdo->gid, {
              DEBUG("Inserting attribute %s on CDO %s (%p)\n",
                    key, idstr, cdo);});

          s = mstro_attribute_dict_set_kventry(cdo->attributes, entry);
          if(s!=MSTRO_OK) {
            WITH_CDO_ID_STR(idstr, &cdo->gid,{
                ERR("Failed to insert incoming attribute |%s|: %d (%s)\n",
                    key, s, mstro_status_description(s));
              };);
          }
        } else {
          WITH_CDO_ID_STR(idstr, &cdo->gid,{
              ERR("Failed to find incoming attribute |%s| in schemata of CDO %s, skipping\n",
                  key, idstr);});
        }
        retstat |= s;
        break;
      }
      case MSTRO_OK: {
        DEBUG("attribute already set on destination CDO\n");
        /* already have an entry */
        size_t i;
        for(i=0; i<num_precious_attr; i++) {
          /* FIXME: might need to schema-qualify KEY */
          if(strcmp(key, precious_attributes[i])==0) {
            WITH_CDO_ID_STR(idstr, &cdo->gid,{
                DEBUG("Skipping incoming attribute %s on CDO %s (%p) because it is precious\n",
                      key, idstr, cdo);
              };);
            break;
          }
        }
        if(i==num_precious_attr) {
          /* not a precious attribute */
          WITH_CDO_ID_STR(idstr, &cdo->gid, {
              DEBUG("Updating attribute %s on CDO %s (%p)\n",
                      key, idstr, cdo);});
          s = mstro_attribute_dict_set_kventry(cdo->attributes, entry);
          if(s!=MSTRO_OK) {
            WITH_CDO_ID_STR(idstr, &cdo->gid,{
                ERR("Failed to update attribute %s on CDO %s (%p): %d (%s)\n",
                      key, idstr, cdo, s, mstro_status_description(s));
            });
            retstat|=s;
          }
        }
        break;
      }
      default:
        WITH_CDO_ID_STR(idstr, &cdo->gid,{
            ERR("Failed to look up attribute |%s| in CDO %s (%p): %d (%s)\n",
                key, idstr, s, cdo, mstro_status_description(s));});
        retstat |= s;
    }
  }

  if(retstat!=MSTRO_OK)
    retstat=MSTRO_FAIL; /* normalize ORed error status */
  return retstat;
}

mstro_status
mstro_cdo_allocate_data(mstro_cdo cdo)
{
  if(cdo==NULL) {
    return MSTRO_INVARG;
  }
  /* Having both raw-ptr and mamba_array already is a user error */
  assert(!(cdo->raw_ptr!=NULL && cdo->mamba_array!=NULL));

  mstro_status s = MSTRO_OK;

  if(cdo->mamba_array!=NULL) {
    DEBUG("no raw-ptr, syncing with mamba_array (CDO |%s|)\n", cdo->name);
    /* sync raw-ptr with mamba because we are nice, and it will get later auto free'd anyway */
    s = mstro_cdo__sync_rawptr(cdo);
    if (s != MSTRO_OK) {
      // already printed an error
      return s;
    }

    DEBUG("Syncing mamba_array and attributes (CDO |%s|)\n", cdo->name);

    size_t* dimsz;
    dimsz = cdo->mamba_array->dims.d; /* Caution, pointer. FIXME see what happens when YAML array is used instead */

    assert(cdo->mamba_array->allocation->n_bytes<=INT64_MAX);
    int64_t localsz = cdo->mamba_array->allocation->n_bytes;

    assert(cdo->mamba_array->dims.size<=INT64_MAX);
    int64_t ndims = cdo->mamba_array->dims.size;

    assert(cdo->mamba_array->layout->element.size_bytes<=INT64_MAX);
    int64_t eltsz = cdo->mamba_array->layout->element.size_bytes;

    int64_t patt;
    switch(cdo->mamba_array->layout->element.order) {
      case MMB_ROWMAJOR:
      case MMB_LAYOUT_ORDER_NONE:
        patt = MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_ROWMAJOR; break;
      case MMB_COLMAJOR:
        patt = MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_COLMAJOR; break;
      default:
        ERR("Unexpected mamba layout order: %d\n",
            cdo->mamba_array->layout->element.order);
        return MSTRO_FAIL;
    }

    DEBUG("%zu %zu %d %d %p\n", localsz, eltsz, ndims, patt, dimsz);
    
    s = mstro_attribute_dict_set(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                 MSTRO_CDO_ATTR_VALUE_int64, &localsz, true, false);
    s |= mstro_attribute_dict_set(cdo->attributes,
                                  MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE,
                                  MSTRO_CDO_ATTR_VALUE_int64, &eltsz, true, false);
    s |= mstro_attribute_dict_set(cdo->attributes,
                                  MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS,
                                  MSTRO_CDO_ATTR_VALUE_uint64, &ndims, true, false);
    s |= mstro_attribute_dict_set(cdo->attributes,
                                  MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER,
                                  MSTRO_CDO_ATTR_VALUE_int64, &patt, true, false);
    s |= mstro_attribute_dict_set(cdo->attributes,
                                  MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE,
                                  MSTRO_CDO_ATTR_VALUE_pointer, dimsz, true, false);
    if (s != MSTRO_OK) {
      ERR("Can't sync mamba array because dict_set failed (%s)\n", mstro_status_description(s));
      return MSTRO_FAIL;
    }
  } else {
    DEBUG("No mamba_array, syncing with raw-ptr if there is one, otherwise I'm making one via mamba (CDO |%s|)\n",
          cdo->name);

    const void *valp=NULL;
    enum mstro_cdo_attr_value_type vt;

    s = mstro_attribute_dict_get(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                 &vt, &valp, NULL, false);
    if(s==MSTRO_NOENT && vt==MSTRO_CDO_ATTR_VALUE_INVALID) {
      ERR("CDO |%s| without mamba-array has no local-size, unsupported\n",
          cdo->name);
      return MSTRO_FAIL;
    }
    if(s!=MSTRO_OK) {
      ERR("Failed to retrieve local-size on raw-ptr based cdo\n");
      return s;
    }
    /* now we know a size */
    int64_t size = *(const int64_t*)valp;
	DEBUG("size (%" PRIi64 ")\n", size);

    if(size<0 && cdo->raw_ptr!=NULL) {
      ERR("CDO |%s| has negative local-size but non-NULL raw-ptr, unsupported\n",
          cdo->name);
      return MSTRO_FAIL;
    }

    s = mstro_cdo__create_mamba_array(cdo, MSTRO_ALLOC_POLICY_NONE);
    if(s!=MSTRO_OK) {
      ERR("Failed to create mmbArray wrapper for raw-ptr\n");
      return s;
    }
	DEBUG("size (%" PRIi64 ")\n", size);

  }

  return MSTRO_OK;
}


mstro_status
mstro_cdo_attribute_msg_get(const struct mstro_cdo_id *id, Mstro__Pool__Attributes **attributes)
{
  if(id==NULL) {
    return MSTRO_INVARG;
  }
  if(attributes==NULL) {
    return MSTRO_INVOUT;
  }
  mstro_cdo cdo=NULL;
  mstro_status s = mstro_cdo__find_cdo(id, &cdo);
  if(s!=MSTRO_OK) {
    return s;
  }
  *attributes = cdo->attributes_msg;
  return MSTRO_OK;
}

mstro_status
mstro_cdo__demand_by_id(const struct mstro_cdo_id *id, mstro_cdo *result)
{
  assert(id!=NULL);
  assert(result!=NULL);

  /* fetch name by ID */
  const char *name=NULL;
  mstro_status s = mstro_pool_resolve_cdoid(id, &name);
  if(s!=MSTRO_OK) {
    WITH_CDO_ID_STR(idstr, id, {
        ERR("Cannot resolve CDO ID %s to CDO name: %d (%s)\n",
            idstr, s, mstro_status_description(s));});
    return MSTRO_FAIL;
  } else {
    WITH_CDO_ID_STR(idstr, id, {
        DEBUG("Resolved CDO ID %s to |%s|\n",
              idstr, name);});
  }

  mstro_cdo res=NULL;

  s = mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &res);
  if(s!=MSTRO_OK) {
    ERR("Cannot declare CDO\n");
    return MSTRO_NOMEM;
  }

  s=mstro_cdo_require(res);
  if(s!=MSTRO_OK) {
    ERR("Failed to REQUIRE: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }
  s=mstro_cdo_demand(res);
  if(s!=MSTRO_OK) {
    ERR("Failed to DEMAND: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }

BAILOUT:
  if(s==MSTRO_OK) {
    *result = res;
  } else {
    WARN("Likely leaking memory\n");
  }
  return s;
}
