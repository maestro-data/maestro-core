/* -*- mode:c -*- */
/** @file
 ** @brief Maestro interface with OFI layer
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 * Copyright (C) 2020-2022 Hewlett-Packard (Schweiz) GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_drc.h"
#include "maestro/i_vni.h"
#include "maestro/logging.h"
#include "maestro/env.h"
#include "maestro/i_utlist.h"
#include "maestro/i_base64.h"
#include "maestro/i_pool_manager_protocol.h"
#include "maestro/i_ofi.h"
#include "maestro/i_globals.h"
#include "maestro/i_pool.h"
#include "i_maestro_numa.h"
#include "maestro/i_memory.h"
#include "maestro/i_pool_operations.h"

#include "protocols/maestro-endpoints.h"

#include "maestro/i_hashing.h"

#include "maestro/i_pool_client.h"
#include "maestro/i_pool_manager.h"
#include "maestro/i_memlock.h"

#include "i_thread_team.h"

#include "../transport/transport.h"
#include "../transport/transport_rdma.h"
#include <rdma/fabric.h>
#include "maestro/i_ofi_threads.h"

/* until CXI hits upstream: */
#define FI_FULL_VERSION(major,minor,revision) (((major) << 16) | ((minor) << 8) | (revision)) 
#if FI_VERSION_LT(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
#define FI_ADDR_CXI  (FI_ADDR_PSMX3 + 1)
#define FI_PROTO_CXI (FI_PROTO_PSMX3 + 1)
#endif

#include <rdma/fi_cm.h>
#include <rdma/fi_domain.h>
#include <rdma/fi_errno.h>
#include <rdma/fi_tagged.h>
#include <rdma/fi_rma.h>

#include <limits.h>

#include "maestro/i_uthash.h"
#include <stdatomic.h>

#include <sys/mman.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <sched.h>
#include <unistd.h>
#include <time.h>

#ifdef HAVE_MIO
// #define MSTRO_MIO_SEM_STR_MAXLEN 	128
 #include "transport/transport_mio.h"
 #include "mio.h"
#endif

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)


#define NFREE(x) do { if(x) free(x); } while(0)


#ifdef WITH_RMA_COUNTER
#define MSTRO_OFI_CAPS    (FI_MSG|FI_RMA|FI_RMA_EVENT|FI_TAGGED)
#else 
#define MSTRO_OFI_CAPS    (FI_MSG|FI_RMA|FI_TAGGED)
#endif

#define MSTRO_OFI_MODE    (FI_CONTEXT|FI_CONTEXT2|FI_RX_CQ_DATA)
#define MSTRO_OFI_MRMODE  (FI_MR_VIRT_ADDR | FI_MR_ALLOCATED | FI_MR_PROV_KEY | FI_MR_LOCAL | FI_MR_MMU_NOTIFY | FI_MR_ENDPOINT)
#define MSTRO_OFI_EP_TYPE FI_EP_RDM
#define MSTRO_OFI_VERSION FI_VERSION(1,14)

static mstro_drc_info g_drc_info = NULL;
static mstro_vni_info g_vni_info = NULL;

/** event domain for RDMA operations */
mstro_event_domain g_ofi_rdma_edom=NULL;


/**PC OFI independent threads handle*/
struct mstro_ofi_thread_team *g_pc_ofi_team = NULL;
extern struct mstro_ofi_thread_team *g_pm_ofi_team;
size_t g_ofi_op_num_retrials = 5;
/** globals */


/** the results of fi_getinfo after successful mstro_ofi_init */
static struct fi_info *g_fi = NULL;

/** ofi threads operation queues table */
extern khash_t(ofi_operation_queue) *g_ofi_operation_queues_table;

/** lock for the operation queues table */
extern pthread_mutex_t g_ofi_operation_queue_table_lock;



static
mstro_status
mstro_epd_create_ofi(const struct fi_info *fi, struct fid_ep *ep,
                     struct mstro_endpoint *dst)
{
  if(dst==NULL)
    return MSTRO_INVOUT;
  if(fi==NULL || ep==NULL)
    return MSTRO_INVARG;


  /* now build protobuf-based version */
  Mstro__Endpoint *e=NULL;
  mstro_status status = mstro_ep_fi_to_ep(fi, ep, &e);
  if(status!=MSTRO_OK) {
    ERR("Failed to build endpoint descriptor: %d (%s)\n",
        status, mstro_status_description(status));
  } else {
    dst->pbep = e;
  }
  
  return status;
}

/** free the toplevel of @arg epl, but don't descend into the
 * ep slot that may be shared with a mstro_endpoint->pbep */
static
mstro_status
mstro_epl__free_shallow(Mstro__EndpointList *epl)
{
  if(epl!=NULL) {
    NFREE(epl->eps);
    if(epl->credentials) {
      for(size_t i=0; i<epl->n_credentials; i++) {
        mstro__ofi_credential__free_unpacked(epl->credentials[i],
                                             NULL);
      }      
      free(epl->credentials);
    }
    if(epl->inforegs) {
      for(size_t i=0; i<epl->n_inforegs; i++) {
        mstro__ofi_memory_region__free_unpacked(epl->inforegs[i],
                                                NULL);
      }
      free(epl->inforegs);
    }    
    free(epl);
  }
  return MSTRO_OK;
}

/** Provide serialized description of EP.
 *
 * Result is a string of size at most MSTRO_EP_STRING_MAX.
 *
 * If EP is a linked list of endpoints, serialize them in order.
 *
 * If *result_p is NULL it will be allocated, otherwise
 * overwritten.
 *
 * This function re-uses the pbep/credentials/inforegs slots of the
 * @arg ep objects, so care must be taken when freeing the resulting
 * @arg *result_p to not invalidate them. Consider using
 * @ref mstro_epl__free_shallow().
 */

#define MSTRO_EP_STRING_MAX (8*1024)

static
mstro_status
mstro_ep__create_inforeg(const struct mstro_endpoint *ep,
                         Mstro__OfiMemoryRegion **res_p)
{
  mstro_status stat;
  Mstro__OfiMemoryRegion *res = malloc(sizeof(Mstro__OfiMemoryRegion));
  if(res==NULL) {
    ERR("Failed to allocate inforeg\n");
    stat=MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    mstro__ofi_memory_region__init(res);
    res->baseaddr = ep->component_info_addr;
    res->raw_key.len = ep->component_info_keysize;
    res->raw_key.data = malloc(res->raw_key.len);
    if(res->raw_key.data==NULL) {
      ERR("Failed to allocate space for raw MR key\n");
      free(res);
      stat=MSTRO_NOMEM;
      goto BAILOUT;
    }
    memcpy(res->raw_key.data,
           ep->component_info_raw_key,
           res->raw_key.len);
  }
  stat=MSTRO_OK;
  *res_p = res;
BAILOUT:
  return stat;
}

static
mstro_status
mstro_ep__create_cred(const struct mstro_endpoint *ep,
                      Mstro__OfiCredential **res_p)
{
  mstro_status stat;
  Mstro__OfiCredential *res = malloc(sizeof(Mstro__OfiCredential));
  if(res==NULL) {
    ERR("Failed to allocate credential region\n");
    stat=MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    mstro__ofi_credential__init(res);
    switch(ep->pbep->ofiproto) {
      case MSTRO__OFI_ENDPOINT_KIND__GNI:
        res->val_case = MSTRO__OFI_CREDENTIAL__VAL_DRC;
        res->drc = malloc(sizeof(Mstro__CredDRC));
        if(res->drc==NULL) {
          ERR("Failed to allocate credential space\n");
          free(res);
          stat=MSTRO_NOMEM;
          goto BAILOUT;
        } else {
          mstro__cred_drc__init(res->drc);
          mstro_drc_get_credential(&res->drc->credential,
                                   g_drc_info);
        }
        break;
      case MSTRO__OFI_ENDPOINT_KIND__CXI:
        if (g_vni_info->auth_key != NULL) {
	  res->val_case = MSTRO__OFI_CREDENTIAL__VAL_CXI;
          res->cxi = malloc(sizeof(Mstro__CredCXI));
          if (res->cxi == NULL)
          {
            ERR("Failed to allocate credential space\n");
            free(res);
            stat=MSTRO_NOMEM;
            goto BAILOUT;
          } else {
            mstro__cred_cxi__init(res->cxi);
            res->cxi->svc_id = g_vni_info->auth_key->svc_id;
            res->cxi->vni = (uint32_t) g_vni_info->auth_key->vni;
          }
        }
	else {
		WARN("No valid vni key \n");
		res->cxi = NULL;
	}
        break;
      default:
        DEBUG("No credential support for endpoint type %d (%s)\n",
              ep->pbep->ofiproto,
              protobuf_c_enum_descriptor_get_value(&mstro__ofi_endpoint_kind__descriptor,
                                                   ep->pbep->ofiproto)->name);
    }
  }
  stat=MSTRO_OK;
  *res_p = res;
BAILOUT:
  return stat;
}

static
mstro_status
mstro_ep__to_el(const struct mstro_endpoint *ep,
                Mstro__EndpointList **result_p)
{
  assert(result_p!=NULL);
  assert(*result_p==NULL);
  mstro_status stat=MSTRO_UNIMPL;
  Mstro__EndpointList *epl;

  assert(ep!=NULL);

  epl = malloc(sizeof(Mstro__EndpointList));
  if(epl==NULL) {
    ERR("Failed to allocate endpoint list\n");
    stat=MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    mstro__endpoint_list__init(epl);

    size_t count = 0;
    const struct mstro_endpoint *tmp;
    LL_COUNT(ep,tmp,count);
    epl->eps = malloc(count * sizeof(Mstro__Endpoint*));
    if(epl->eps==NULL) {
      ERR("Failed to allocate endpoint\n");
      stat=MSTRO_NOMEM;
      goto BAILOUT;
    }

    epl->inforegs = malloc(count * sizeof(Mstro__OfiMemoryRegion*));
    if(epl->inforegs==NULL) {
      ERR("Failed to allocate endpoint\n");
      stat=MSTRO_NOMEM;
      goto BAILOUT;
    }

    epl->credentials = malloc(count * sizeof(Mstro__OfiCredential*));
    if(epl->credentials==NULL) {
      ERR("Failed to allocate endpoint\n");
      stat=MSTRO_NOMEM;
      goto BAILOUT;
    }

    size_t i=0;
    const struct mstro_endpoint *elt;
    LL_FOREACH(ep,elt) {
      assert(ep->pbep!=NULL);

      epl->eps[i] = elt->pbep;
      assert(elt->pbep->proto_case==MSTRO__ENDPOINT__PROTO_OFIPROTO);
      epl->n_eps=i+1;

      stat = mstro_ep__create_inforeg(elt, &epl->inforegs[i]);
      if(stat!=MSTRO_OK) {
        ERR("Failed to allocate inforeg\n");
        goto BAILOUT;
      }
      epl->n_inforegs=i+1;

      stat = mstro_ep__create_cred(elt,  &epl->credentials[i]);
      if(stat!=MSTRO_OK) {
        ERR("Failed to allocate credential region\n");
        goto BAILOUT;
      }
      epl->n_credentials=i+1;

      i++;
    }
    assert(epl->n_eps == count);
  }
  stat = MSTRO_OK;
  *result_p = epl;

BAILOUT:
  if(stat!=MSTRO_OK) {
    if(epl) {
      if(epl->eps) {
        free(epl->eps); /* don't descend, entries are owned by ep->pbep entries */
        epl->eps=NULL;
        epl->n_eps=0;
      }
      mstro__endpoint_list__free_unpacked(epl, NULL);
    }
  }
  return stat;
}

/** serialize all endpoints in the linked list of endpoints starting
 * at @arg ep into a serialized and base64-encoded Mstro__AppInfo */
static
mstro_status
mstro_ofi__epl_to_serialized_appinfo(char **result_p,
                                     const struct mstro_endpoint *ep)
{
  unsigned char *encoded=NULL;
  uint8_t *pbbuf=NULL;
  Mstro__AppInfo appinfo = MSTRO__APP_INFO__INIT;
  
  mstro_status status = mstro_ep__to_el(ep, &appinfo.eps);
  if(status!=MSTRO_OK) {
    ERR("Failed to create protobuf AppInfo for endpoints\n");
    goto BAILOUT;
  }

  size_t pblen = mstro__app_info__get_packed_size(&appinfo);
  DEBUG("Endpoint AppInfo/PM info packing needs %zu bytes \n", pblen);
  pbbuf = malloc(sizeof(uint8_t)*pblen);
  if(pbbuf==NULL) {
    ERR("Failed to allocate protobuf buffer\n");
    status=MSTRO_NOMEM;
    goto BAILOUT;
  }
  if(mstro__app_info__pack(&appinfo, pbbuf)!=pblen) {
    ERR("Failed to pack appinfo\n");
    status = MSTRO_FAIL;
    goto BAILOUT;
  }

  /* b64 encode */
  size_t needed;
  encoded = base64_encode(pbbuf, pblen, &needed);
  if(encoded==NULL) {
    ERR("Cannot b64 encode endpoint descriptor list, out of memory\n");
    status = MSTRO_NOMEM;
    goto BAILOUT;
  } else {
    *result_p = (char*)encoded;
  }

#if 1
  {
    size_t count=0;
    const struct mstro_endpoint *tmp;
    LL_COUNT(ep,tmp,count);
    DEBUG("serialized %d EPs to |%s|\n",
          count, *result_p);
  }
#endif

BAILOUT:
  if(pbbuf)
    free(pbbuf);
  if(status!=MSTRO_OK) {
    if(encoded)
      free(encoded);
  }
  if(appinfo.eps) {
    mstro_epl__free_shallow(appinfo.eps);
  }
          
  return status;
}

mstro_status
mstro_appinfo_serialize(const struct mstro_endpoint *ep,
                        char **result_p)
{
  if(result_p==NULL)
    return MSTRO_INVOUT;
  if(ep==NULL)
    return MSTRO_INVARG;

  assert(ep!=NULL);
  
  return mstro_ofi__epl_to_serialized_appinfo(result_p, ep);
}


/** this one allocates the destination string */
static inline
mstro_status
mstro_ep_desc_deserialize__str(char **dst,
                               char *b64_strval)
{
  if(dst==NULL || *dst!=NULL)
    return MSTRO_INVOUT;
  if(b64_strval==NULL)
    return MSTRO_INVARG;

  mstro_status stat=MSTRO_UNIMPL;


  size_t buflen;
  unsigned char *buf = base64_decode((unsigned char*)b64_strval,
                                     strlen(b64_strval),
                                     &buflen);

  if(buf==NULL) {
    stat=MSTRO_NOMEM;
    goto BAILOUT;
  }
  *dst=(char*)buf;
BAILOUT:
  return stat;
}



/* deserialize AppInfo from base64 encoded AppInfo OOB data */
mstro_status
mstro_appinfo_deserialize(const char *serialized_eps,
                          Mstro__AppInfo **result_p)
{
  if(result_p==NULL)
    return MSTRO_INVOUT;
  if(serialized_eps==NULL)
    return MSTRO_INVARG;
  mstro_status stat=MSTRO_UNIMPL;

  Mstro__AppInfo *appinfo=NULL;
  
  size_t buflen;
  unsigned char *buf = base64_decode((unsigned char*)serialized_eps,
                                     strlen(serialized_eps),
                                     &buflen);

  if(buf==NULL) {
    ERR("Failed to base64-decode endpoint description -- invalid data or out of memory\n");
    stat=MSTRO_NOMEM;
    goto BAILOUT;
  }

  appinfo = mstro__app_info__unpack(NULL, buflen, buf);
  if(appinfo==NULL) {
    ERR("Failed to parse endpoint description\n");
    stat=MSTRO_FAIL;
    goto BAILOUT;
  }

  if(appinfo->eps->n_eps!=appinfo->eps->n_inforegs || appinfo->eps->n_eps != appinfo->eps->n_credentials) {
    ERR("Invalid EndpointList\n");
    stat=MSTRO_INVARG;
    goto BAILOUT;
  }
  DEBUG("Parsed %zu endpoints\n", appinfo->eps->n_eps);

  for(size_t i=0; i<appinfo->eps->n_eps; i++) {
    MSTRO_EP__EL_DESCRIBE(MSTRO_LOG_DEBUG,MSTRO_LOG_MODULE_COMM,
                          "Received endpoint descriptor",
                          appinfo->eps, i);
  }

  *result_p = appinfo;
  appinfo=NULL;
  
  stat=MSTRO_OK;

BAILOUT:
  if(buf)
    free(buf);

  if(appinfo)
    mstro__app_info__free_unpacked(appinfo, NULL);

  return stat;
}




/** Extract the address from @arg epd and return in in *addr.
 *
 * @arg *addr must be have some space preallocated, and the available
 * space must be passed in @arg *addrlen. If it is not sufficient an
 * MSTRO_NOMEM will be returned and @arg *addlen will be set to the
 * required space.
 */
static
mstro_status
mstro_epd_to_ofi_addr(const Mstro__Endpoint *epd,
                      uint32_t *addr_format,
                      void *addr,
                      size_t *addrlen)
{
  if(addr_format==NULL || addr==NULL || addrlen==NULL)
    return MSTRO_INVOUT;
  if(epd==NULL)
    return MSTRO_INVARG;

  assert(epd->proto_case==MSTRO__ENDPOINT__PROTO_OFIPROTO);
  assert(epd->addr_case==MSTRO__ENDPOINT__ADDR_OFIADDR);

  const Mstro__OfiAddr *a = epd->ofiaddr;
  switch(a->val_case) {
    case MSTRO__OFI_ADDR__VAL__NOT_SET:
      ERR("No address set, that's an error\n");
      return MSTRO_FAIL;
    case MSTRO__OFI_ADDR__VAL_UNSPEC:
      ERR("No address value specified\n");
      return MSTRO_FAIL;
    case MSTRO__OFI_ADDR__VAL_SOCK:
      ERR("Unspecific sockaddr not supported\n");
      return MSTRO_UNIMPL;

    case MSTRO__OFI_ADDR__VAL_IN4: {
      struct sockaddr_in res = {
        .sin_family      = a->in4->sin_family,
        .sin_addr.s_addr = a->in4->sin_addr,
        .sin_port        = a->in4->sin_port
      };
      if(*addrlen<sizeof(res)) {
        *addrlen = sizeof(res);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, &res, sizeof(res));
        *addr_format = FI_SOCKADDR_IN;
      }
      break;
    }
    case MSTRO__OFI_ADDR__VAL_IN6: {
      struct sockaddr_in6 res = {
        .sin6_family          = a->in6->sin6_family,
        .sin6_port            = a->in6->sin6_port,
        .sin6_flowinfo        = a->in6->sin6_flowinfo,
        .sin6_addr.s6_addr[0] = (a->in6->sin6_addr_0 & 0x000000ff) >> 0,
        .sin6_addr.s6_addr[1] = (a->in6->sin6_addr_0 & 0x0000ff00) >> 8,
        .sin6_addr.s6_addr[2] = (a->in6->sin6_addr_0 & 0x00ff0000) >> 16,
        .sin6_addr.s6_addr[3] = (a->in6->sin6_addr_0 & 0xff000000) >> 24,
        .sin6_addr.s6_addr[4] = (a->in6->sin6_addr_1 & 0x000000ff) >> 0,
        .sin6_addr.s6_addr[5] = (a->in6->sin6_addr_1 & 0x0000ff00) >> 8,
        .sin6_addr.s6_addr[6] = (a->in6->sin6_addr_1 & 0x00ff0000) >> 16,
        .sin6_addr.s6_addr[7] = (a->in6->sin6_addr_1 & 0xff000000) >> 24,
        .sin6_scope_id = a->in6->sin6_scope_id
      };
      if(*addrlen<sizeof(res)) {
        *addrlen = sizeof(res);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, &res, sizeof(res));
        *addr_format = FI_SOCKADDR_IN6;
      }
      break;
    }

    case MSTRO__OFI_ADDR__VAL_GNI: {
      uint64_t res[6] = { a->gni->a0, a->gni->a1, a->gni->a2,
                          a->gni->a3, a->gni->a4, a->gni->a5 };
      if(*addrlen<sizeof(res)) {
        *addrlen = sizeof(res);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, res, sizeof(res));
        *addr_format = FI_ADDR_GNI;
      }
      break;
    }
      
    case MSTRO__OFI_ADDR__VAL_STR: {
      size_t l=strlen(a->str)+1;
      if(*addrlen<l) {
        *addrlen = l;
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, a->str, l);
        *addr_format = FI_ADDR_STR;
      }
      break;
    }

    case MSTRO__OFI_ADDR__VAL_PSMX: {
      if(*addrlen<sizeof(uint64_t)) {
        *addrlen=sizeof(uint64_t);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, &a->psmx, sizeof(uint64_t));
        *addr_format = FI_ADDR_PSMX;
      }
      break;        
    }
      
    case MSTRO__OFI_ADDR__VAL_PSMX2: {
      uint64_t res[2] = { a->psmx2->a0, a->psmx2->a1 };
      if(*addrlen<sizeof(res)) {
        *addrlen = sizeof(res);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, res, sizeof(res));
        *addr_format = FI_ADDR_PSMX2;
      }
      break;
    }
      
    case MSTRO__OFI_ADDR__VAL_PSMX3: {
      uint64_t res[2] = { a->psmx3->a0, a->psmx3->a1 };
      if(*addrlen<sizeof(res)) {
        *addrlen = sizeof(res);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, res, sizeof(res));
        *addr_format = FI_ADDR_PSMX3;
      }
      break;
    }
      
    case MSTRO__OFI_ADDR__VAL_IB: {
      *addr_format = FI_SOCKADDR_IB;
/* #ifdef HAVE_IB */
/*     case MSTRO_EP_OFI_IB: */
/*       *addr_format = FI_SOCKADDR_IB; */
/*       *addr = &epd->ib; */
/*       *addrlen = sizeof(struct sockaddr_ib); */
/*       break; */
/* #endif */
      ERR("IB addresses unimplemented\n");
      return MSTRO_UNIMPL;
    }
    case MSTRO__OFI_ADDR__VAL_IB_UD: {
      uint64_t res[4] = { a->ib_ud->a0, a->ib_ud->a1,
                          a->ib_ud->a2, a->ib_ud->a3 };
      if(*addrlen<sizeof(res)) {
        *addrlen = sizeof(res);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, res, sizeof(res));
        *addr_format = FI_ADDR_IB_UD;
      }
      break;
    }
    case MSTRO__OFI_ADDR__VAL_CXI: {
      if(*addrlen<sizeof(uint64_t)) {
        *addrlen = sizeof(uint64_t);
        return MSTRO_NOMEM;
      } else {
        memcpy(addr, &a->cxi->raw, sizeof(uint64_t));
        *addr_format = FI_ADDR_CXI;
      }
      break;
    }
      
    case MSTRO__OFI_ADDR__VAL_BGQ:
    case MSTRO__OFI_ADDR__VAL_MLX:
    case MSTRO__OFI_ADDR__VAL_EFA:
    default:
      ERR("Unsupported OFI address kind: %d\n", a->val_case);
      return MSTRO_UNIMPL;
  }
  return MSTRO_OK;
}
/*     case MSTRO_EP_OFI_PSMX: */
/*       *addr_format = FI_ADDR_PSMX; */
/*       *addr = &epd->psmx; */
/*       *addrlen = sizeof(uint64_t); */
/*       break; */
/* #if FI_VERSION_GE(FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION), FI_VERSION(1,5)) */
/*     case MSTRO_EP_OFI_PSMX2: */
/*       *addr_format = FI_ADDR_PSMX2; */
/*       *addr = &epd->psmx2; */
/*       *addrlen = 2*sizeof(uint64_t); */
/*       break; */
/* #endif */
/*     default: */
/*       ERR("Unimplemented mgr address format: %d\n", epd->type); */
/*       return MSTRO_UNIMPL; */
/*   } */
/*   return MSTRO_OK; */
/* } */

/** the component descriptor read from the PM */
union mstro_component_descriptor g_pm_component_descriptor;

#define MSTRO_OFI_KEY_LEN_MAX 8

mstro_status
mstro_mr_key_get(struct fi_info* fi, struct fid_mr* mr, 
                 uint8_t** mkey, size_t* ksize, uint64_t* maddr)
{
  /* In an ideal world all providers would support fi_mr_raw_attr, as
   * the man page suggests (ofi issue #6095). Then we could play it
   * safe and always exchange raw attr/offset info. On SCALABLE mr
   * these conversions are no-ops. Instead we have to special-case things here ...
   */
  uint64_t addr=0;
  int stat;
  size_t keysize = fi->domain_attr->mr_key_size;
  *ksize = keysize;
  uint8_t* mr_key;

  if(keysize>MSTRO_OFI_KEY_LEN_MAX) {
    ERR("FIXME: provider requires MR key size exceeding hard-coded limit, consider increasing MSTRO_OFI_KEY_LEN_MAX (issue #81)\n");
    return MSTRO_FAIL;
  }

  mr_key=malloc(keysize);
  if(mr_key==NULL) {
    ERR("Failed to allocate raw key storage (%d bytes)\n", keysize);
    return MSTRO_NOMEM;
  }

  if(fi->domain_attr->mr_mode & FI_MR_RAW) {
    stat= fi_mr_raw_attr(mr, &addr, mr_key, &keysize, 0);
    if(stat<0) {
      ERR("Failed to retrieve raw key for component info memory registration: %d (%s)\n",
          stat, fi_strerror(-stat));
      return MSTRO_FAIL;
    }
  } else {
    uint64_t key = fi_mr_key(mr);
    if(key==FI_KEY_NOTAVAIL) {
      ERR("Failed to retrieve key for component info memory registration: %d (%s)\n");
      return MSTRO_FAIL;
    }
    assert(sizeof(key)<=MSTRO_OFI_KEY_LEN_MAX); 
    /* keysize is 4 on verbs ... we try to let the compiler handle byte order issues here by casting */
    switch(keysize) {
        case 1: {
           uint8_t k = (uint8_t) key;
	   memcpy(mr_key, &k, sizeof(k));
	   break;
	}
        case 2: {
           uint16_t k = (uint16_t) key;
	   memcpy(mr_key, &k, sizeof(k));
	   break;
	}
        case 4: {
           uint32_t k = (uint32_t) key;
	   memcpy(mr_key, &k, sizeof(k));
	   break;
	}
        case 8: {
	   memcpy(mr_key, &key, sizeof(key));
	   break;
	}
        default: 
            ERR("Unsupported MR key size %d\n", keysize);
	    abort();
    }
  }
  if(fi->domain_attr->mr_mode & FI_MR_VIRT_ADDR) {
    if(fi->domain_attr->mr_mode & FI_MR_RAW) {
      DEBUG("Both FI_MR_RAW and FI_MR_VIRT_ADDR specified, addr after translation %p, local %p\n",
            addr, *maddr);
    } else {
      DEBUG("FI_MR_VIRT_ADDR set, passing local virtual address to remote side\n");
      assert(sizeof(void*)<=sizeof(uint64_t));
      addr=*maddr;
    }
  }

  *maddr = addr;
  *mkey = mr_key;

  {
    /* copy up to first 8 bytes for printing */
    uint8_t k[8];
    for(size_t i=0; i<8; i++) {
      if(keysize>i) {
        k[i] = mr_key[i];
      } else {
        k[i] = 0;
      }
    }
    
    DEBUG("Component info RDMA block registered%s: local %p, MR %p, addr %" PRIx64 ", desc %p, keysize %zu (key start: %"
          PRIx8 " %" PRIx8 " %" PRIx8 " %" PRIx8 " %" PRIx8 " %" PRIx8 " %" PRIx8 " %" PRIx8 ")\n",
          fi->domain_attr->mr_mode & FI_MR_RAW ? " (raw keys)" : "",
          maddr, mr, addr, fi_mr_desc(mr), keysize,
          k[0], k[1], k[2], k[3], k[4], k[5], k[6], k[7]);
  }

  return MSTRO_OK;
}



mstro_status
mstro_ofi__mreg_attr(enum fi_hmem_iface iface, uint64_t device,
                struct fi_domain_attr *domain_attr,struct fid_domain *domain, uint64_t access,
		void* buf, size_t len,
                struct fid_mr **mr, void **local_buf_mr_desc)
{

	mstro_status status = MSTRO_UNIMPL;
	uint64_t requested_key =0;
        struct fi_mr_attr mr_attr = {0};
	struct iovec iov = {0};


	requested_key = domain_attr->mr_mode & FI_MR_PROV_KEY ? 0 : mstro_memory_new_key();
	if (domain_attr->mr_key_size == 8) /*avoid shift overflow */ 
	{
		if (requested_key > UINT64_MAX)
		{
			ERR("memory registration key out of bound for provider's domain attribute: requested %" PRIx64", domain keylen %" PRIu64 " bytes\n",
                                requested_key, domain_attr->mr_key_size);
		}
	} else
	{
		if(requested_key & ((uint64_t) 1) << (8*domain_attr->mr_key_size)) {
			ERR("memory registration key out of bound for provider's domain attribute: requested %" PRIx64", domain keylen %" PRIu64 " bytes\n",
					requested_key, domain_attr->mr_key_size);
		}
	}

	/*fill fi_mr_attr*/
	iov.iov_len = len;
	iov.iov_base = buf;
	mr_attr.mr_iov = &iov;
	mr_attr.iov_count = 1;
	mr_attr.access = access;
	mr_attr.offset = 0;
	mr_attr.requested_key = requested_key;
	mr_attr.context= NULL;
	mr_attr.auth_key_size = domain_attr->auth_key_size;
	mr_attr.auth_key = domain_attr->auth_key;
	mr_attr.iface = iface;
	switch(iface) {
		/*case FI_HMEM_NEURON: // not yet in libfabric 1.14
		  mr_attr.device.neuron = device;
		  break;*/
		case FI_HMEM_ZE:
			mr_attr.device.ze = device;
			break;
		case FI_HMEM_CUDA:
			mr_attr.device.cuda = device;
			break;
		default:
			break;
	}


	int err = fi_mr_regattr(domain, &mr_attr, 0, mr);
	if (err) {
		ERR("Couldn't register memory region for RDMA transport (err: %d, %s)\n",
				err, fi_strerror(-err));
		return MSTRO_OFI_FAIL;
	}
	else
	{
		DEBUG("Registered memory region for RDMA transport successfully\n");
		*local_buf_mr_desc = fi_mr_desc(*mr);
		status = MSTRO_OK;
	}

	return status;
}
static inline
mstro_status
mstro_ofi__bind_and_enable_mreg(struct fid_ep *ep, struct fid_mr *mr)
{
	mstro_status status = MSTRO_UNIMPL;
	int ret = fi_mr_bind(mr,(struct fid *) ep, 0);
	if(ret<0) {
		ERR("Failed to bind mreg to endpoint, skipping: %d (%s)\n",
				ret, fi_strerror(-ret));
		return MSTRO_OFI_FAIL;
	}
	ret = fi_mr_enable(mr);
	if(ret<0) {
		ERR("Failed to enable mreg: %d (%s)\n",
				ret, fi_strerror(-ret));
		return MSTRO_OFI_FAIL;
	}
	status = MSTRO_OK;
	DEBUG("Bound MR to EP and enabled it (required due to FI_MR_ENDPOINT)\n");
	return status;
}

/** Check if we need to register fi read local buffers (writes into)
 *  - Check if it host and device memory
 *  - Register memory regoin
 *  - Check if binding registration to endpoint is needed
 *  from https://ofiwg.github.io/libfabric/v1.15.0/man/fi_mr.3.html
 *  FI_LOCAL_MR is deprecated with 1.5 and above
 *  FI_MR_LOCAL means you always need to register memory, even for receive operations. 
 *  NULL should be used for desc if no registration was done. 
 *  FI_MR_HMEM  requires us to register any device memory passed in (when FI_HMEM is used).
 *  FI_MR_MMU_NOTIFY indicates that we need to call fi_mr_refresh if the memory region is backed by physical pages only later. 
 *  We always pin memory, so we can support this flag but never need to call refresh (currently)
 *  FI_MR_COLLECTIVE requires registration. This is for later if we ever start using libfabric collectives
 *
 *  So only if MR_LOCAL or HMEM is set do we need registration on the receiving side.
 */
mstro_status
mstro_ofi__maybe_register_local_buffer(
		enum fi_hmem_iface iface, uint64_t device, 
		struct fi_domain_attr *domain_attr, struct fid_ep *ep,
		struct fid_domain *domain, void* buf, size_t len,
                struct fid_mr **mr, void **local_buf_mr_desc)
{
	mstro_status status = MSTRO_UNIMPL;
	bool do_register = false;
	bool do_bind = false;

	if(iface == FI_HMEM_SYSTEM) { /* host */
		if(domain_attr->mr_mode & FI_MR_LOCAL) {
			do_register = true;
			if(domain_attr->mr_mode & FI_MR_ENDPOINT) {
				do_bind = true;
			}
		}
		{
			DEBUG("Fabric provider does not require registering host local buffers\n");
			*mr=NULL;
			*local_buf_mr_desc = NULL;

		}
        }
        else if((iface==FI_HMEM_CUDA) || (iface==FI_HMEM_ROCR) || (iface==FI_HMEM_ZE)){ /* device 
				                                               TODO add more devices when updating libfabric*/
                if(domain_attr->mr_mode & FI_MR_HMEM) { // we need to register local device memory
			do_register = true;
			if(domain_attr->mr_mode & FI_MR_ENDPOINT) {
				do_bind = true;
                        }

		}
		else {
			DEBUG("Fabric provider does not require registering device local buffers\n");
			*mr=NULL;
                        *local_buf_mr_desc = NULL;
		}
        }
        else
        {
                ERR("Unknown buffer memory layer\n");
		return status;
        }
	status = MSTRO_OK;
        if(do_register)
	{
    /**register memory for local access*/
		status = mstro_ofi__mreg_attr(iface, device, domain_attr,domain,(FI_READ | FI_WRITE | FI_SEND | FI_RECV),buf,len,mr,local_buf_mr_desc);
	}
	if(do_bind)
	{
		status = mstro_ofi__bind_and_enable_mreg(ep, *mr);
	}

	return status;
}

/** check if @arg mr_mode indicates that @arg mr needs to be bound to @arg ep and enabled before being usable. If so: do it */
mstro_status
mstro_ofi__maybe_bind_and_enable_mreg(struct fid_ep *ep, int mr_mode, struct fid_mr *mr)
{
  if(mr_mode & FI_MR_ENDPOINT) {
	  /* bind and enable mreg */
	  return mstro_ofi__bind_and_enable_mreg(ep,mr); 
  }
  return MSTRO_OK;
}


/* static inline */
mstro_status
mstro_ep_build_from_ofi(struct mstro_endpoint *dst,
                        struct fi_info *fi)
{
  int stat;
  mstro_status retstat=MSTRO_UNIMPL;
  if(dst==NULL)
    return MSTRO_INVOUT;
  if(fi==NULL)
    return MSTRO_INVARG;

  struct fid_fabric *fabric = NULL;
  struct fi_eq_attr eq_attr;
  struct fid_eq *eq = NULL;
  struct fid_domain *domain = NULL;
  struct fid_ep *ep = NULL;
  struct fi_av_attr av_attr;
  struct fid_av *av = NULL;
  struct fi_cq_attr cq_attr;
  struct fid_cq *cq = NULL;
  struct fi_cntr_attr cntr_attr;
  struct fid_cntr *rmacnt = NULL;
  uint8_t *mr_key = NULL;
  struct fid_mr *mr=NULL;
  void * local_buf_mr_desc = NULL;
  fi_vni_auth_key *auth_key;

  /* create fabric object */
  stat = fi_fabric(fi->fabric_attr, &fabric, NULL);
  if(stat!=0) {
    ERR("fi_fabric failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat = MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  switch(fi->addr_format) {
    case FI_ADDR_GNI: {
      /* put cookie into
       *  fi->domain_attr->auth_key = (void *) &auth_key;
       *  fi->domain_attr->auth_key_size = sizeof(auth_key);
       */
      /* we only support one cookie per workflow currently. It is
       * allocated early on in ofi_init (on pool manager) or obtained
       * from PM_INFO (on clients) */
      assert(g_drc_info!=NULL);

      stat = mstro_drc_insert_ofi(fi, g_drc_info);
      if(stat!=MSTRO_OK) {
        ERR("Failed to insert DRC credential into fabric info\n");
        retstat=MSTRO_FAIL;
        goto BAILOUT_FAIL;
      }
      break;
    }
    case FI_ADDR_CXI:
    /*CXI and CXI_COMPAT are the same due to the compatibility issue in 1.15.2*/
    #if FI_VERSION_GE(FI_FULL_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION,FI_REVISION_VERSION),FI_FULL_VERSION(1,15,2))
	  #define FI_ADDR_CXI_COMPAT (FI_ADDR_PSMX3+1)
    case FI_ADDR_CXI_COMPAT:
    #endif
      /* we only support one cookie per workflow currently. It is
       * allocated early on in ofi_init (on pool manager) or obtained
       * from PM_INFO (on clients) */
      //assert(g_vni_info!=NULL);
      //stat = mstro_vni_insert_ofi(fi,g_vni_info);
      //if(stat!=MSTRO_OK) {
       // ERR("Failed to insert vni auth_key credential into fabric info\n");
       // retstat=MSTRO_FAIL;
       // goto BAILOUT_FAIL;
     // }
      /*read vni directly from env specific for this cxi device - WLM puts them in all jobs currently */
      mstro_vni_get_auth_key_from_env(&auth_key, fi->nic->device_attr->name);
      if (auth_key == NULL)
      {
	      fi->domain_attr->auth_key = NULL;
        WARN("Missing VNI auth key\n");
      }
      else
      {
	      fi->domain_attr->auth_key = (uint8_t *) auth_key;
	      fi->domain_attr->auth_key_size = sizeof(auth_key);
      }
      break;
    default:
      DEBUG("No auth_key for address format %d, setting to NULL\n", fi->addr_format);
      fi->domain_attr->auth_key = NULL;
      break;
  }

  /* create domain */
  stat = fi_domain(fabric, fi, &domain, NULL);
  if(stat!=0) {
    ERR("fi_domain failed: %d (%s)\n", stat, fi_strerror(-stat));
    domain=NULL; /* we happened to see domain!=NULL on error on OSx */
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  /* completion queue */
  memset(&cq_attr,0, sizeof(cq_attr));
  cq_attr.format = FI_CQ_FORMAT_TAGGED;
  cq_attr.size = 0; /* provider-specific default. Consider fi->rx_attr->size; */
  /** FIXME (investigate): on macOS I get a 'not supported' for some
      provider endpoints if set to FI_WAIT_UNSPEC; FD also does not
      work, and MUTEX_COND only works for some. But leaving it at 0
      means NONE, which means we can't do blocking waits...

      For now we don't do blocking waits, and we disable some
      providers like rxd/rxm, and things are fine */
 /*  cq_attr.wait_obj = FI_WAIT_MUTEX_COND; // works on macOS for most
  *  providers */
  /* cq_attr.wait_obj = FI_WAIT_UNSPEC; */
  cq_attr.wait_obj = FI_WAIT_NONE;
  stat = fi_cq_open(domain, &cq_attr, &cq, NULL);
  if(stat!=0) {
    ERR("fi_cq_open failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  /* event queue */
  /* create event queue */
  memset(&eq_attr, 0, sizeof(eq_attr));
  eq_attr.size = 64; /* FIXME: check whether this makes sense */
  /* eq_attr.wait_obj = FI_WAIT_UNSPEC; /\* provider-optimal default *\/ */
  eq_attr.wait_obj = FI_WAIT_NONE;
  stat = fi_eq_open(fabric, &eq_attr, &eq, NULL);
  if(stat!=0) {
    ERR("fi_eq_open failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  /* create endpoint */
  stat = fi_endpoint(domain, fi, &ep, NULL);
  if(stat!=0) {
    ERR("fi_endpoint failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  /* INFO("This is a %s\n",  fi_tostr(fi,FI_TYPE_INFO)); */

  /* connectionless endpoint needs address vector */
  memset(&av_attr, 0, sizeof(av_attr));
  av_attr.type = FI_AV_MAP;
  stat = fi_av_open(domain, &av_attr, &av, NULL);
  if(stat!=0) {
    ERR("fi_av_open failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  /* bind address vector to endpoint */
  stat = fi_ep_bind(ep, &av->fid, 0);
  if(stat!=0) {
    ERR("fi_ep_bind for av failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

  /* bind event queue to endpoint */
  stat = fi_ep_bind(ep, &eq->fid, 0);
  if(stat!=0) {
    ERR("fi_ep_bind for eq failed: %d (%s)\n", stat, fi_strerror(-stat));
    if(stat==-FI_ENOSYS) {
      ERR("Keep fingers crossed, we'll try without (psmx2 does not support EQ for instance)\n");
      fi_close((struct fid*)eq);
      eq=NULL;
    } else {
      retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
    }
  }
  /* bind cq to endpoint */
  stat = fi_ep_bind(ep, &cq->fid, FI_TRANSMIT|FI_RECV);
  if(stat!=0) {
    ERR("fi_ep_bind for cq failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

#ifdef WITH_RMA_COUNTER
  memset(&cntr_attr,0, sizeof(cntr_attr));
  cntr_attr.events = FI_CNTR_EVENTS_COMP;
  cntr_attr.wait_obj = FI_WAIT_NONE;
  cntr_attr.wait_set = NULL;
  cntr_attr.flags = 0;
  stat = fi_cntr_open(domain, &cntr_attr, &rmacnt, 0);
  if(stat!=0) {
    ERR("fi_cntr_open for readctr failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }
  stat = fi_ep_bind(ep, &rmacnt->fid, FI_REMOTE_READ);
  if(stat!=0) {
    ERR("fi_ep_bind for readctr failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }
#endif
                
  /* enable! */
  stat = fi_enable(ep);
  if(stat!=0) {
    ERR("fi_enable failed: %d (%s)\n", stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
  }

#if 0
  /* FIXME: this is a workaround until we understand
     https://github.com/ofiwg/libfabric/issues/5453
  */
  if(fi->addr_format==FI_SOCKADDR_IN) {
    struct sockaddr_in in4;
    size_t space = sizeof(in4);
    int s= fi_getname(&ep->fid, &in4, &space);
    if(s<0) {
      ERR("Failed to obtain IPv4 address: %d (%s)\n", s, fi_strerror(-s));
      retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
    }
    if(in4.sin_port==0) {
      INFO("IPv4 endpoint has port==0, ignoring\n");
      retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
    }
  } else if(fi->addr_format==FI_SOCKADDR_IN6) {
    struct sockaddr_in6 in6;
    size_t space = sizeof(in6);
    int s= fi_getname(&ep->fid, &in6, &space);
    if(s<0) {
      ERR("Failed to obtain IPv6 address: %d (%s)\n", s, fi_strerror(-s));
      retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
    }
    if(in6.sin6_port==0) {
      INFO("IPv6 endpoint has port==0, ignoring\n");
      retstat=MSTRO_FAIL; goto BAILOUT_FAIL;
    }
  }
  /* end of workaround */
#endif

  /* register component block as RDMA-readable memory chunk */

  /* to be safe: mlock the descriptor. GNI needs it, others will like it */
  retstat = mstro_memlock(&g_component_descriptor, sizeof(g_component_descriptor));
  if(retstat!=MSTRO_OK) {
    ERR("Failed to mlock component descriptor (%d bytes at %p, pagesize %d): %d (%s)\n",
	sizeof(g_component_descriptor), &g_component_descriptor,sysconf(_SC_PAGESIZE),
        retstat, mstro_status_description(retstat));
    goto BAILOUT_FAIL;
  }

  retstat = mstro_ofi__mreg_attr(FI_HMEM_SYSTEM, /* system memory */
		  0, /*device id -- not used when using system memory */
		  fi->domain_attr, domain, /*domain attr and domain */
		  FI_REMOTE_READ,&g_component_descriptor, sizeof(g_component_descriptor), /*access, buf, len*/
		  &mr,  &local_buf_mr_desc); /*output mr and desc */
  if(retstat!=MSTRO_OK) {
    ERR("Failed to publish component descriptor on endpoint: %d (%s)\n",
        stat, fi_strerror(-stat));
    retstat=MSTRO_FAIL; goto BAILOUT_FAIL_UNLOCK;
  }
  /* some domains do per-endpoint memory registration, cater to that */
  retstat = mstro_ofi__maybe_bind_and_enable_mreg(ep,
                                                  fi->domain_attr->mr_mode,
                                                  mr);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to bind component descriptor mreg to endpoint\n");
    int ret = fi_close((struct fid *)mr);
    if(ret<0) {
      ERR("Failed to close PM-info mreg: %d (%s)\n", ret, strerror(-ret));
      retstat=MSTRO_OFI_FAIL;
    }
    retstat |= mstro_memunlock(&g_component_descriptor, sizeof(g_component_descriptor));
    goto BAILOUT_FAIL;
  }


  size_t keysize;
  uint64_t addr = (uint64_t)&g_component_descriptor;
  retstat = mstro_mr_key_get(fi, mr, &mr_key, &keysize, &addr);
  if (retstat != MSTRO_OK)
    goto BAILOUT_FAIL_UNLOCK;

  dst->cq = cq;
#ifdef WITH_RMA_COUNTER
  dst->rmacnt = rmacnt;
  dst->last_rma_cnt = 0;
#endif
  dst->av = av;
  dst->ep = ep;
  dst->domain = domain;
  dst->eq = eq;
  dst->fabric = fabric;
  dst->fi = fi;
  dst->component_info_mr = mr;
  dst->component_info_addr = addr;
  dst->component_info_keysize = keysize;
  dst->component_info_raw_key = mr_key;
  dst->live_ctxs = kh_init(ctxtab);
  int s = pthread_mutex_init(&dst->live_ctxs_mtx, NULL);
  if(s!=0) {
    ERR("Failed to init mutex\n");
    retstat = MSTRO_FAIL;
    goto BAILOUT_FAIL_CTXTAB;
  }

  dst->next = NULL;

  /* store serialized local address */
  retstat = mstro_epd_create_ofi(fi, ep, dst);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to construct worker mgmt ep description: %d\n", retstat);
    goto BAILOUT_FAIL_CTXTAB;
  }

  retstat = mstro_ep__create_inforeg(dst, &dst->inforeg);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to construct endpoint inforeg descriptor: %d\n", retstat);
    goto BAILOUT_FAIL_CTXTAB;
  }

  retstat = mstro_ep__create_cred(dst, &dst->cred);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to construct endpoint credential descriptor: %d\n", retstat);
    goto BAILOUT_FAIL_CTXTAB;
  }

  retstat = mstro_appinfo_serialize(dst, &dst->serialized);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to serialize worker mgmt ep description: %d\n", retstat);
    goto BAILOUT_FAIL_CTXTAB;
  }
  goto BAILOUT;

BAILOUT_FAIL_CTXTAB:
	kh_destroy(ctxtab, dst->live_ctxs);
BAILOUT_FAIL_UNLOCK:
  retstat |= mstro_memunlock(&g_component_descriptor, sizeof(g_component_descriptor));
BAILOUT_FAIL:
#define CLEANUP(thing,name) do {                        \
      if(thing!=NULL) {                                 \
          stat = fi_close((struct fid*)thing);          \
          if(stat!=0) {                                 \
            ERR("fi_close on %s failed: %d (%s)\n",       \
                name, stat, fi_strerror(-stat));        \
          }                                             \
      }                                                 \
  } while(0)

  /* recommended order as per OFI tutorials: */
  /* alias eps (none) */
  free(mr_key);
  CLEANUP(mr, "MR");
  CLEANUP(ep,"endpoint");
  /* poll sets (none) */
  CLEANUP(cq,"completion queue");
  /* counters (none) */
  CLEANUP(av,"address vector");
  CLEANUP(eq,"event queue");
  CLEANUP(domain,"domain");
  /* wait sets (none) */
  CLEANUP(fabric,"fabric");
#undef CLEANUP

BAILOUT:
  return retstat;
}

/** parse CXI address @arg cxi_addr and put printable representation
 * in nic/pid/validity form into @arg buf. Returns @arg buf for
 * convenience. Return NULL if @arg buflen indicates buffer is too
 * small. */
static char *
mstro_ofi__cxi_straddr(const uint8_t *remote_addr, char *buf, size_t buflen)
{
  /* parse CXI address */
  union cxi_addr_ {
    struct {
      uint32_t pid		: 9;
      uint32_t nic		: 20;
      uint16_t vni; /*since libfabric 1.20*/
    };
    uint64_t raw;
  };
  union cxi_addr_ tmp = { .raw = *((uint64_t*)remote_addr) };
  size_t s = snprintf(buf, buflen, "0x%x:%d:%"PRIu16")",
                   tmp.nic, tmp.pid, tmp.vni);
  if(s>=buflen) {
    return NULL;
  } else {
    return buf;
  }
}

#define EP_DESC_BUF_MAX 1024

const char *
mstro_endpoint_describe(const struct mstro_endpoint *ep)
{
  /* FIXME: this should lock the EP, save the string there, ... */
  /* for now this is only a debugging hack */
  static char ep_name_buf[EP_DESC_BUF_MAX];
  size_t size = EP_DESC_BUF_MAX;
  int ret;
  char strbuf[EP_DESC_BUF_MAX];
  uint8_t addrbuf[EP_DESC_BUF_MAX];

  strncpy(ep_name_buf, "(unprintable)", EP_DESC_BUF_MAX);

  /* first: fetch address */
  size=EP_DESC_BUF_MAX;
  ret = fi_getname(&ep->ep->fid,addrbuf,&size);
  DEBUG("addrbuf len %zu, ret %d\n", size, ret);
  if(ret<0 && ret!=-FI_ETOOSMALL) {
    ERR("fi_getname in EP describe failed: %d (%s)\n", ret, fi_strerror(-ret));
    goto DONE;
  }

  /* now translate to string */
  size = EP_DESC_BUF_MAX;
  /* may truncate but will 0-terminate the strbuf */
  fi_av_straddr(ep->av, addrbuf, strbuf, &size);
  //  DEBUG("strbuf len %zu, val %s\n", size, strbuf);
  
  char maybe_parsed_cxi_addr[32];
  maybe_parsed_cxi_addr[0]='\0';
  if(ep->fi->addr_format==FI_ADDR_CXI) {
    const char *prefix = " (CXI ";
    const char *suffix = ")";
    strncpy(maybe_parsed_cxi_addr, prefix, 32);
    mstro_ofi__cxi_straddr(addrbuf, maybe_parsed_cxi_addr+strlen(prefix), 32-strlen(prefix));
    strncat(maybe_parsed_cxi_addr,suffix, 32-strlen(maybe_parsed_cxi_addr));
  }

  size=snprintf(ep_name_buf, EP_DESC_BUF_MAX,
                "OFI EP prov %s name %s straddr %s%s",
                ep->fi->fabric_attr->prov_name,
                ep->fi->fabric_attr->name, strbuf, maybe_parsed_cxi_addr);
  assert(size<EP_DESC_BUF_MAX);
DONE:
  return ep_name_buf;
}

/* order endpoint list.
 *
 * The goal is to have the high-speed ones first. Ideally OFI should
 * do that for us (and does to some degree), but we want to do more:
 * On a laptop used for debugging we want the loopback IP to be up
 * front so that firewalling rules can rely on that
 *
 * Performs a destructive in-place reodering of its argument.
 */

static
int
mstro_ofi__fi_info_cmp(const struct fi_info *x1,
                       const struct fi_info *x2)
{
  assert(x1!=NULL); assert(x2!=NULL);

  uint32_t p1=FI_PROTO_UNSPEC, p2=FI_PROTO_UNSPEC;
  if(x1->ep_attr)
    p1=x1->ep_attr->protocol;

  if(x2->ep_attr)
    p2=x2->ep_attr->protocol;

  /* DEBUG("p1: %d, p2: %d\n", p1, p2); */

  /* newer versions may introduce unknown providers, so check here: */  
  assert( (FI_VERSION_GE(FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION), FI_VERSION(1,14)))
         && (FI_VERSION_GE(FI_VERSION(1,20), FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION)))
        );

#if FI_VERSION_GE(FI_VERSION(1,15),FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION))
#define MSTRO__FI_PROTO_MAX FI_PROTO_CXI
#else
#define MSTRO__FI_PROTO_MAX (FI_PROTO_CXI+4)
#endif

  /* provider order: sorted by preference */
  const int proto_order[MSTRO__FI_PROTO_MAX+1] = {
    [FI_PROTO_SHM]            = 0,
    [FI_PROTO_GNI]            = 5,
   // from 1.15.2, but we always define it to support the internal early CXI release:
    [FI_PROTO_CXI]            = 7,
    [FI_PROTO_IB_UD]          = 10,
    [FI_PROTO_IB_RDM]         = 50,
    [FI_PROTO_RXM]            = 70,
    [FI_PROTO_UDP]            = 90,
    [FI_PROTO_MRAIL]          = 100,
    [FI_PROTO_RDMA_CM_IB_XRC] = 200,
    [FI_PROTO_RDMA_CM_IB_RC]  = 250,
    [FI_PROTO_PSMX3]          = 200,
    [FI_PROTO_PSMX2]          = 300,
    [FI_PROTO_PSMX]           = 400,
    [FI_PROTO_EFA]            = 500,
    // from 1.15
    #if FI_VERSION_GE(FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION), FI_VERSION(1,15))
    [FI_PROTO_RXM_TCP]        = 600,
    [FI_PROTO_OPX]            = 610,
    #endif
    [FI_PROTO_SOCK_TCP]       = 650,
    [FI_PROTO_NETWORKDIRECT]  = 900,
    [FI_PROTO_MLX]            = 910,
    [FI_PROTO_RXD]            = 920,
    [FI_PROTO_RSTREAM]        = 940,
    [FI_PROTO_IWARP]          = 970,
    [FI_PROTO_MXM]            = 980,
    [FI_PROTO_IWARP_RDM]      = 990,
    [FI_PROTO_UNSPEC]         = 1000,
  };
  
  if(p1!=p2) {
    assert(p1<=MSTRO__FI_PROTO_MAX);
    assert(p2<=MSTRO__FI_PROTO_MAX);
    return proto_order[p1] - proto_order[p2];
  }
    
  /* break ties on anything IP-related to prefer localhost */
  {
    if(x1->addr_format==FI_SOCKADDR_IN
       && x2->addr_format==FI_SOCKADDR_IN
       && x1->src_addr && x2->src_addr) {
      /* DEBUG("both IPv4, preferring localhost\n"); */
      const struct sockaddr_in *a1 = (const struct sockaddr_in*)x1->src_addr;
      const struct sockaddr_in *a2 = (const struct sockaddr_in*)x2->src_addr;
      /* constant is 127.0.0.1 */
      if(a1->sin_addr.s_addr==htonl(0x7F000001)) {
        /* DEBUG("a1 ipv4 localhost\n"); */
        return -1;
      }
      if(a2->sin_addr.s_addr==htonl(0x7F000001)) {
        /* DEBUG("a2 ipv4 localhost\n"); */
        return +1;
      }
      return a1->sin_addr.s_addr < a2->sin_addr.s_addr ? -1 : +1;

    } else if (x1->addr_format==FI_SOCKADDR_IN6
               && x2->addr_format==FI_SOCKADDR_IN6
               && x1->src_addr && x2->src_addr) {
      /* DEBUG("both IPv6, preferring localhost\n"); */
      const struct sockaddr_in6 *a1 = (const struct sockaddr_in6*)x1->src_addr;
      const struct sockaddr_in6 *a2 = (const struct sockaddr_in6*)x2->src_addr;
      if(memcmp(&a1->sin6_addr.s6_addr,
                &in6addr_loopback,
                sizeof(struct in6_addr))==0) {
        /* DEBUG("a1 ipv6 localhost\n"); */
        return -1;
      }
      if(memcmp(&a2->sin6_addr.s6_addr,
                &in6addr_loopback,
                sizeof(struct in6_addr))==0){
        /* DEBUG("a2 ipv6 localhost\n"); */
        return +1;
      }
      size_t i;
      for(i=0; i<16; i++) {
        if(a1->sin6_addr.s6_addr[i] < a2->sin6_addr.s6_addr[i])
          return -1;
        else if (a1->sin6_addr.s6_addr[i] > a2->sin6_addr.s6_addr[i])
          return +1;
        else
          continue;
      }
      /* FIXME: also check for ::ffff:127.0.0.1, the ip4-in-ip6 loopback */
      /* FFFF7F000001 */
    }
  }

  
  DEBUG("not ordering: %d/%d vs %d/%d\n",
        p1, x1->addr_format, p2, x2->addr_format);

  return 0;
}

static inline
mstro_status
mstro_ofi__order_fi_list(struct fi_info **fi)
{
  if(fi==NULL)
    return MSTRO_INVARG;

  struct fi_info *head = *fi;
  LL_SORT(head, mstro_ofi__fi_info_cmp);
  *fi = head;

  return MSTRO_OK;
}


static inline
mstro_status
mstro_ofi__maybe_notify_completion(mstro_ofi_msg_context ctx)
{
  mstro_status status;
  
  /* In case of RDMA transport, completion operates via events rather than cond vars */
  if (ctx->ev != NULL) {
    mstro_event_id eid;
    status = mstro_event_id_get(ctx->ev, &eid);
    if(status == MSTRO_OK) {
      DEBUG("OFI RDMA completion event id %" PRIx64 "\n", eid);
      struct mstro_transport_rdma_cb_args* result = NULL; 
      status = mstro_event_trigger(ctx->ev, (void**)&result);
      if(status!=MSTRO_OK) {
        ERR("triggering event failed: %d (%s)\n", status,
            mstro_status_description(status));
      }
    }
    status |= mstro_event_destroy(ctx->ev);
    if (status != MSTRO_OK) {
      ERR("Failed destroying event\n");
    }
    if (status != MSTRO_OK) {
      ERR("Failed destroying event\n");
    }
    return status;

  } else {
    ERR("Deprecated RDMA completion notification path taken, should be replaced by events\n");
    int ret = pthread_mutex_lock(&ctx->lock);
    if(ret!=0) {
      ERR("msg handle could not lock completion context mutex: %d\n", ret);
      goto COMPLETION_NOTIFICATION_FAILED;
    }
    ret = pthread_cond_signal(&ctx->completion);
    if(ret!=0) {
      ERR("msg handler completion signalling failed: %d\n", ret);
      goto COMPLETION_NOTIFICATION_FAILED;
    }
    ret = pthread_mutex_unlock(&ctx->lock);
    if(ret!=0) {
      ERR("msg handler completion signalling failed: %d\n", ret);
      goto COMPLETION_NOTIFICATION_FAILED;
    }
 COMPLETION_NOTIFICATION_FAILED:
    if(ret!=0) {
      return MSTRO_FAIL;
    }
  }
  return MSTRO_OK;
}

static inline
mstro_status
mstro_ofi__handle_cq_entry__recv(mstro_ofi_msg_context ctx)
{
  assert(ctx->fi_cq_entry_flags&FI_RECV);
  assert(ctx->msg!=NULL);

  struct mstro_msg_envelope *msg = ctx->msg;
  mstro_status status = ctx->msg_handler(msg);
  
  if(status!=MSTRO_OK) {
    if(status==MSTRO_WOULDBLOCK) {
      ERR("Message handler should not block, this should use operations to restart properly\n");
        return status;
    } else {
      ERR("Error handling incoming message, dropping it: %d (%s)\n",
          status, mstro_status_description(status));
    }
    goto BAILOUT;
  }
  
  bool re_post = ctx->needs_repost;
  if(re_post) {
    /* repost of fresh receive (with a fresh ctx) has been done
       mstro_ofi__check_and_handle_cq before we got called, and slot
       messages have no completion watchers */
    ;
  } else {
    /* other recv */
    //DEBUG("non-slot message, cleaning up\n");
    if(ctx->has_completion == 1) {
      //DEBUG("ctx asks for completion notification\n");
      status = mstro_ofi__maybe_notify_completion(ctx);
    } else {
      //DEBUG("ctx wants no completion notification, cleaning up ctx\n");
      ;
    }
  }
BAILOUT:
  return status;
}

static inline
mstro_status
mstro_ofi__handle_cq_entry__send(mstro_ofi_msg_context ctx)
{
  assert(ctx->fi_cq_entry_flags&FI_SEND);
  assert(ctx->msg!=NULL);

  struct mstro_msg_envelope *msg = ctx->msg;
  mstro_status status = MSTRO_UNIMPL;

  /* completion of our sent messages */
  /* DEBUG("Send of message %p type %s completed\n", */
  /*       msg, msg->descriptor->name); */
  /* otherwise clean it up */
  if(ctx->has_completion == 1) {
    //DEBUG("ctx asks for completion notification\n");
    status = mstro_ofi__maybe_notify_completion(ctx);
  } else {
    ;
  }
  return status;
}

static inline
mstro_status
mstro_ofi__handle_cq_entry__rma(mstro_ofi_msg_context ctx)
{
  assert(ctx->fi_cq_entry_flags&FI_RMA);
  assert(ctx->msg==NULL);

  mstro_status status;

  //DEBUG("RDMA op completed, ctx %p\n", ctx);
  if (ctx->has_completion == 1) { 
    status = mstro_ofi__maybe_notify_completion(ctx);
  } else {
    WARN("Unexpected no-completion case on RMA entry, flags %ull\n",
         ctx->fi_cq_entry_flags);
    status = MSTRO_OK;
  }

  return status;
}

mstro_status
mstro_ofi__remember_ctx(struct mstro_endpoint *ep,
                        mstro_ofi_msg_context ctx)
{
  assert(ep!=NULL);
  assert(ctx!=NULL);
  assert(ep->live_ctxs!=NULL);

  mstro_status status = MSTRO_OK;

  int s=pthread_mutex_lock(&ep->live_ctxs_mtx);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

  assert(ctx->ep==NULL || ctx->ep==ep);
  ctx->ep = ep;
  
  int res;
  kh_put(ctxtab, ep->live_ctxs, ctx, &res);
  if(res==-1) {
    ERR("Failed to insert ctx\n");
  } else {
    if(res==0) {
      ERR("Context %p already in table\n", ctx);
      abort();
    }
  }

  s=pthread_mutex_unlock(&ep->live_ctxs_mtx);
  if(s!=0) {
    ERR("Failed to unlock mutex: %d (%s)\n", s, strerror(s));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

BAILOUT:
  return status;
}

/** drop CTX from table of live contexts. Needs to be called under lock. */
static
mstro_status
mstro_ofi__forget_ctx__locked(struct mstro_endpoint *ep,
                              mstro_ofi_msg_context ctx)
{
  assert(ep!=NULL);
  assert(ctx!=NULL);
  assert(ep->live_ctxs!=NULL);

  mstro_status status = MSTRO_OK;

  int res;
  khint_t i;
  i = kh_get(ctxtab, ep->live_ctxs, ctx);
  if(i==kh_end(ep->live_ctxs)) {
    ERR("ctx %p not in live table\n", ctx);
    abort();
  }
  kh_del(ctxtab, ep->live_ctxs, i);

BAILOUT:
  return status;
}

/** drop CTX from table of live contexts */
mstro_status
mstro_ofi__forget_ctx(struct mstro_endpoint *ep,
                      mstro_ofi_msg_context ctx)
{
  assert(ep!=NULL);
  assert(ctx!=NULL);
  assert(ep->live_ctxs!=NULL);

  mstro_status status = MSTRO_OK;

  int s=pthread_mutex_lock(&ep->live_ctxs_mtx);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

  status = mstro_ofi__forget_ctx__locked(ep, ctx);

  s=pthread_mutex_unlock(&ep->live_ctxs_mtx);
  if(s!=0) {
    ERR("Failed to unlock mutex: %d (%s)\n", s, strerror(s));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

BAILOUT:
  return status;
}

/** handle one entry from the CQ, run in a handler thread */
mstro_status
mstro_ofi__handle_cq_entry(void* item)
{
  mstro_ofi_msg_context ctx = (mstro_ofi_msg_context)item;
  mstro_status status = MSTRO_FAIL;

  DEBUG("Found CQ entry %p\n", ctx);
  
  if(ctx->fi_cq_entry_flags&FI_RECV) {
    status = mstro_ofi__handle_cq_entry__recv(ctx);
  } else if(ctx->fi_cq_entry_flags&FI_SEND) {
    status = mstro_ofi__handle_cq_entry__send(ctx);
  } else if(ctx->fi_cq_entry_flags&FI_RMA) {
    status = mstro_ofi__handle_cq_entry__rma(ctx);
  } else {
    ERR("Unexpected completion event. Flags: %ull\n",
        ctx->fi_cq_entry_flags);
    status = MSTRO_UNIMPL;
  }
  if(status==MSTRO_WOULDBLOCK) {
    ERR("Message handling should not block %p\n", item);
  } else {
    mstro_msg_envelope_free(ctx->msg);
    mstro_ofi__forget_ctx(ctx->ep, ctx);
    status = mstro_ofi__msg_context_destroy(ctx);
  }
  return status;
}

static
mstro_status
mstro_ofi__drop_unusable_fis(struct fi_info **fi)
{
  assert(fi!=NULL);
  struct fi_info *head = *fi;
  struct fi_info **el=fi;

  /** basically an implementation of LL_DELETE_IF() */
  while (*el) {
    /* check if we like it */
    /* IPv6 currently has issues: addresses are accepted by the
     * endpoint, but local-scope ones may hang afterwards */ 
    if((*el)->addr_format==FI_SOCKADDR_IN6)  {
      struct fi_info *victim = *el;
      *el = victim->next;
      victim->next=NULL;
      DEBUG("Dropping IPv6 endpoint %s\n", fi_tostr(victim, FI_TYPE_INFO));
      fi_freeinfo(victim);
    } else {
      el = &(*el)->next;
    }
  }
  return MSTRO_OK;    
}

mstro_status
mstro_ofi_discover_and_build_eps(struct mstro_endpoint_set **ep_set, enum fi_threading threading)
{
  int stat = 0;
  size_t i=0;
  size_t num_fis = 0;
  mstro_status retstat = MSTRO_UNIMPL;
  struct fi_info *fi = NULL,
                 *hints = NULL;
  struct mstro_endpoint_set *endpoints = NULL;

  if(ep_set == NULL)
  {
    return MSTRO_INVARG;
  }

  /* obtain info */
  hints = fi_allocinfo();
  if(!hints) {
    ERR("fi_allocinfo failed\n");
    return MSTRO_NOMEM;
  }

  hints->caps = MSTRO_OFI_CAPS;
  hints->mode = MSTRO_OFI_MODE;
  hints->ep_attr->type = MSTRO_OFI_EP_TYPE;
  hints->domain_attr->mr_mode = MSTRO_OFI_MRMODE;
  hints->domain_attr->threading = threading;
  /* we now support manual progress (with threaded CQ handling) */
  hints->domain_attr->control_progress = FI_PROGRESS_MANUAL;
  hints->domain_attr->data_progress = FI_PROGRESS_MANUAL;

  /**check if the user specified an OFI provider for Maestro*/
  char * prov_name = getenv(MSTRO_ENV_OFI_PROVIDER);
  if(prov_name) {
    hints->fabric_attr->prov_name = strdup(prov_name);
  }
  
  /* we really want 1.14 or above */
  stat = fi_getinfo(MSTRO_OFI_VERSION, NULL, NULL, 0, hints, &fi);
  fi_freeinfo(hints);
 
  if(stat!=0) {
    ERR("fi_getinfo failed: %d (%s)\n", stat, fi_strerror(-stat));
    return MSTRO_FAIL;
  }

  /* pre-select (= drop some) endpoints */
  stat = mstro_ofi__drop_unusable_fis(&fi);
  if(stat!=MSTRO_OK) {
    ERR("Failed to clean up FI-list\n");
    return MSTRO_FAIL;
    
  }

  /* order endpoint list.
   *
   * The goal is to have the high-speed ones first. Ideally OFI should
   * do that for us (and does to some degree), but we want to do more:
   * On a laptop used for debugging we want the loopback IP to be up
   * front so that firewalling rules can rely on that */
  stat = mstro_ofi__order_fi_list(&fi);
  if(stat!=MSTRO_OK) {
    ERR("Failed to order FI-list\n");
    return MSTRO_FAIL; 
  }

  /* allocate endpoints to accomodate all */
  struct fi_info *tmp;
  LL_COUNT(fi,tmp,num_fis);
  *ep_set = calloc(num_fis, sizeof(struct mstro_endpoint_set));
  endpoints = *ep_set; /**for easier code*/
  if(endpoints==NULL) {
    ERR("Failed to allocate endpoints structure\n");
    return  MSTRO_NOMEM; 
  }
  endpoints->num_free = 1+num_fis;
  endpoints->size = 0;
  endpoints->serialized_eps = NULL;

  DEBUG("%zu fis found\n", num_fis);

  /* create one entry in our global EP list per entry */
  LL_FOREACH(fi,tmp) {
    NOISE("index %zu\n", i);
    i++;
    DEBUG("potential endpoint: %s %s\n", tmp->fabric_attr->prov_name,
		    tmp->fabric_attr->name);
    retstat = mstro_ep_build_from_ofi(&(endpoints->eps[endpoints->size]),
                                      tmp);

    if(retstat!=MSTRO_OK) {
      WARN("Failed to build EP %zu, trying others\n", i);
      goto TRY_NEXT_EP;
    }

    DEBUG("%zu. usable endpoint: %s\n",
          endpoints->size,
          mstro_endpoint_describe(&endpoints->eps[endpoints->size]));
    
    /* also register a region for incoming data */
    struct fid_mr *pm_info_mr = NULL;
    
    const struct fi_info* fi = endpoints->eps[endpoints->size].fi;

    void* local_buf_mr_desc = NULL;
    /*maybe register and bind local component descriptor buffers for reads if needed*/
    retstat = mstro_ofi__maybe_register_local_buffer(
		    FI_HMEM_SYSTEM, /*system memory */
		    0, /*device is ignored when using system memory*/
		    fi->domain_attr, endpoints->eps[endpoints->size].ep,
		    endpoints->eps[endpoints->size].domain, &g_pm_component_descriptor,sizeof(g_pm_component_descriptor),
		    &pm_info_mr, &local_buf_mr_desc);
    if(retstat != MSTRO_OK) {
	    ERR("Failed to register or bind component descriptor read buffer on domain of ep %zu\n",
			    endpoints->size);
	    if (pm_info_mr != NULL) {
		    int ret = fi_close((struct fid *)pm_info_mr);
		    if(ret<0) {
			    ERR("Failed to close PM-info mreg: %d (%s)\n", ret, strerror(-ret));
			    retstat=MSTRO_OFI_FAIL;
		    }
	    }
	    retstat |= mstro_memunlock(&g_component_descriptor, sizeof(g_component_descriptor));
	    goto TRY_NEXT_EP;
    }
    else
    {
	    DEBUG("registered peer buffer for RDMA, addr %p, mr 0x%" PRIx64 ", desc %p\n",
			    &g_pm_component_descriptor, pm_info_mr, local_buf_mr_desc);
    }
      
    endpoints->eps[endpoints->size].peer_info_mr = pm_info_mr;
    endpoints->eps[endpoints->size].next
        = &endpoints->eps[endpoints->size+1];
    endpoints->size++;
 TRY_NEXT_EP:
    ;
  }
  /* fix pointer into invalid last entry */
  endpoints->eps[endpoints->size-1].next = NULL;

  if(endpoints!=NULL) {
    DEBUG("%zu usable endpoints\n", endpoints->size);
    g_fi = fi;
    retstat = MSTRO_OK;
  } else {
    ERR("No usable endpoints found\n");
    g_fi = NULL;
  }

  return retstat;

}

/** Initalise ofi_threads and supporting structures for communicating on
 * OFI endpoints */
mstro_status
mstro_ofi_init(void)
{ 
  mstro_status retstat = MSTRO_UNIMPL;

  retstat=mstro_event_domain_create("OFI RDMA internal events", &g_ofi_rdma_edom);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to initialize event domain\n");
    goto BAILOUT_FAIL;
  }
  
  /**create OFI operation queues hash table ... **/
  g_ofi_operation_queues_table = kh_init(ofi_operation_queue);

  if(g_ofi_operation_queues_table==NULL)
  {
    ERR("Failed to initialize OFI operation queues table\n");
    return MSTRO_NOMEM;
  }
  int err = pthread_mutex_init(&g_ofi_operation_queue_table_lock, NULL);
  if(err!=0) {
    ERR("Failed to init OFI operation queue table lock: %d (%s)\n", err, strerror(err));
    return MSTRO_FAIL;
  }

  /**set the number of ofi read/send retrials*/
  char *tmp_char_p = getenv(MSTRO_ENV_OFI_NUM_RETRY);
  if(tmp_char_p !=NULL) {
    g_ofi_op_num_retrials = atol(tmp_char_p);
  }

  int num_op_threads = 1;
  tmp_char_p = getenv(MSTRO_ENV_OPERATIONS_NUM_THREADS);
  if(tmp_char_p !=NULL) {
    num_op_threads = atoi(tmp_char_p);
    if(num_op_threads<1) {
      ERR("Illegal value for number of Operations threads, using %d\n", 1);
      num_op_threads = 1;
    }
  }

  /**Start operation handler thread team*/
  retstat = erl_thread_team_create("Pool operation handler",
                                   "OP",
                                   num_op_threads, 
                                   mstro_pool_op_engine,
                                   NULL,
                                   &g_pool_operations_team);
  if(retstat!=MSTRO_OK) {
    ERR("Failed to start pool operations handler thread team\n");
    goto BAILOUT_FAIL;
  }
  DEBUG("Started %d pool operations handler threads\n", num_op_threads);

  /* prepare for DRC */
  /* (only needed on GNI/Cray, but dummy emulation is provided) */
  if(g_drc_info==NULL) {
    DEBUG("No DRC cookie present, allocating one\n");
    retstat = mstro_drc_init(&g_drc_info);
    if(retstat!=MSTRO_OK) {
      ERR("Failed to create DRC credential\n");
      retstat=MSTRO_FAIL;
      goto BAILOUT_FAIL;
    }
  } else {
    DEBUG("DRC cookie already set, reusing\n");
  }

  /* prepare for vni */
  /* (only needed on CXI/SS11) */
  if(g_vni_info==NULL) {
    DEBUG("No VNI credential present, allocating one\n");
    retstat = mstro_vni_init(&g_vni_info);
    if(retstat!=MSTRO_OK) {
      ERR("Failed to create VNI credential\n");
      retstat=MSTRO_FAIL;
      goto BAILOUT_FAIL;
    }
  } else {
    DEBUG("VNI credential already set, reusing\n");
  }  

  
  /* to be safe: mlock the descriptor. GNI needs it, others will like it */
  assert(sizeof(g_component_descriptor)%sysconf(_SC_PAGESIZE)==0);
  retstat = mstro_memlock(&g_component_descriptor, sizeof(g_component_descriptor));
  if(retstat!=MSTRO_OK) {
    ERR("Failed to mlock our component descriptor (%d bytes at %p, pagesize %ul): %d (%s)\n",
	sizeof(g_component_descriptor), &g_component_descriptor, sysconf(_SC_PAGESIZE),
        retstat, mstro_status_description(retstat));
    goto BAILOUT_FAIL;
  }

  /* also do it for the PM descriptor block we'll RDMA read into */
  assert(sizeof(g_pm_component_descriptor)%sysconf(_SC_PAGESIZE)==0);
  retstat = mstro_memlock(&g_pm_component_descriptor,
                          sizeof(g_pm_component_descriptor));
  if(retstat!=MSTRO_OK) {
    ERR("Failed to mlock PM component descriptor (%d bytes at %p, pagesize %ul): %d (%s)\n",
	sizeof(g_pm_component_descriptor),
        &g_pm_component_descriptor, sysconf(_SC_PAGESIZE),
        retstat, mstro_status_description(retstat));
    goto BAILOUT_FAIL;
  }

  goto BAILOUT;
BAILOUT_FAIL:
  ; /* FIXME: free resources */

BAILOUT:
  return retstat;
}

/** kill all outstanding requests. */
static inline
mstro_status
mstro_ofi__endpoint_kill_outstanding_requests(struct mstro_endpoint *ep)
{
  assert(ep!=NULL);
  assert(ep->live_ctxs!=NULL);
  mstro_status status = MSTRO_OK;

  int s=pthread_mutex_lock(&ep->live_ctxs_mtx);
  if(s!=0) {
    ERR("Failed to lock mutex: %d (%s)\n", s, strerror(s));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

  mstro_ofi_msg_context ctx;
  char dummy;
  int r;
  size_t num_outstanding = kh_size(ep->live_ctxs);

  /* cannot use kh_foreach, since the table is a hash set (= has no
   * value entries), and using kh_val internally is documented to
   * cause a segfault in this case */
  khint_t i;
  for(i=kh_begin(ep->live_ctxs); i!=kh_end(ep->live_ctxs); i++) {
    if(!kh_exist(ep->live_ctxs, i))
      continue;
    ctx = kh_key(ep->live_ctxs, i);
    /* we assume that all OFI interaction is already quieted, so no
     * CQ handler is running anymore. Hence we can't/don't have to
     * fi_cancel the contexts, but can simply free them
     * here. Closing the endpoint will not touch the endpoints (as
     * per fi_fabric(3) on fi_close(3) */
    /* FIXME: check whether some data inside the context needs to be freed */
    assert(ctx!=NULL);
    assert(ctx->ep==ep);
    if(ctx->msg) {
      mstro_msg_envelope_free(ctx->msg);
    }
    if(ctx->ev) {
      mstro_event_destroy(ctx->ev);
    }

    mstro_ofi__forget_ctx__locked(ctx->ep, ctx);
    mstro_ofi__msg_context_destroy(ctx);
  }

  DEBUG("Killed %zu outstanding message contexts\n",
        num_outstanding);

  kh_destroy(ctxtab, ep->live_ctxs);

  ep->live_ctxs=NULL;

  s=pthread_mutex_unlock(&ep->live_ctxs_mtx);
  if(s!=0) {
    ERR("Failed to unlock mutex: %d (%s)\n", s, strerror(s));
    status=MSTRO_FAIL;
    goto BAILOUT;
  }

BAILOUT:
  return status;
}

mstro_status
mstro_ofi_destroy_ep_set(struct mstro_endpoint_set *endpoints)
{
  mstro_status s = MSTRO_UNIMPL;
  if(endpoints) {
    for(size_t i=0; i<endpoints->size; i++) {
      struct mstro_endpoint *e = &endpoints->eps[i];

      /* outstanding requests need to be killed before closing down EP */
      s = mstro_ofi__endpoint_kill_outstanding_requests(e);
      if(s!=MSTRO_OK) {
        ERR("Failed to cancel outstanding requests on ep %zu\n", i);
      }

      if(e->serialized)
        free(e->serialized);
      if(e->pbep)
        mstro__endpoint__free_unpacked(e->pbep, NULL);
      if(e->cred)
        mstro__ofi_credential__free_unpacked(e->cred, NULL);
      if(e->inforeg)
        mstro__ofi_memory_region__free_unpacked(e->inforeg, NULL);

      if(e->peer_info_mr) {
        DEBUG("closing RDMA peer_info MR for ep %zu\n", i);
        s = fi_close((struct fid*)e->peer_info_mr);
        if(s<0) {
          ERR("Failed to close RDMA MR for ep %zu: %d (%s)\n",
              i, s, fi_strerror(-s));
        }
      }
      if(e->component_info_mr) {
        DEBUG("closing RDMA component_info MR for ep %zu\n", i);
        s = fi_close((struct fid*)e->component_info_mr);
        if(s<0) {
          ERR("Failed to close RDMA MR for ep %zu: %d (%s)\n",
              i, s, fi_strerror(-s));
        }
      }
      if(e->component_info_raw_key)
        free(e->component_info_raw_key);

      /* every EP has one lock on the component descriptor */
      mstro_memunlock(&g_component_descriptor, sizeof(g_component_descriptor));

#define CLOSE_FID(member, descr) do {                                   \
        if(e->member) {                                                 \
          DEBUG("Closing down %s for ep %zu\n", descr, i);              \
          int s = fi_close((struct fid*)e->member);                     \
          if(s<0) { ERR("Failed to close %s for ep %zu: %d (%s)\n",     \
                        descr, i, s, fi_strerror(-s)); }                \
        }                                                               \
      } while(0)

      CLOSE_FID(ep,"EP");
      CLOSE_FID(cq,"CQ");
#ifdef WITH_RMA_COUNTER
      CLOSE_FID(rmacnt,"CNTR");
#endif
      CLOSE_FID(av,"AV");
      CLOSE_FID(eq,"EQ");
      CLOSE_FID(domain,"DOMAIN");
      CLOSE_FID(fabric,"FABRIC");
#undef CLOSE_FID

      s = pthread_mutex_destroy(&e->live_ctxs_mtx);
      if(s!=0) {
        ERR("Failed to destroy mutex: %d (%s)\n", s, strerror(s));
      }
    }

    free(endpoints);
  }
  return s;
}

mstro_status
mstro_ofi_finalize(bool destroy_drc_info)
{
  mstro_status s=MSTRO_OK;

  DEBUG("OFI finalization initiated\n");
  /*Shutdown fifo thread team threads that handle messages before closing the endpoints */
  WARN("Shutting down pool operations handling threads\n");
  s = erl_thread_team_stop_and_destroy(g_pool_operations_team);
  if(g_fi)
    fi_freeinfo(g_fi);

  DEBUG("OFI endpoints closed\n");

  /* It's safe to call this even if we've not yet locked the page; we
   * ignore the error that we'd get in this case */
  mstro_memunlock(&g_component_descriptor, sizeof(g_component_descriptor));
  mstro_memunlock(&g_pm_component_descriptor, sizeof(g_pm_component_descriptor));

  /* destroy but do not release the DRC info if requested */
  if(destroy_drc_info) {
    if(g_drc_info) {
      s = mstro_drc_destroy(g_drc_info, false);
      g_drc_info = NULL;
    }
  }

   /* destroy VNI info */
  if(destroy_drc_info) {
    if(g_vni_info) {
      s = mstro_vni_destroy(g_vni_info);
      g_vni_info = NULL;
    }
  }


  assert(g_ofi_rdma_edom!=NULL);
  s=mstro_event_domain_destroy(g_ofi_rdma_edom);
  if(s!=MSTRO_OK) {
    ERR("Failed to destroy event domain\n");
  }
  g_ofi_rdma_edom=NULL;
  
  /**destroy operation queues hash table FIXME this is assuming we have a single ofi threads team*/
  kh_destroy(ofi_operation_queue, g_ofi_operation_queues_table); 
  int err = pthread_mutex_destroy(&g_ofi_operation_queue_table_lock);
  if(err!=0) {
    ERR("Failed to destroy OFI operation queue table lock: %d (%s)\n", err, strerror(err));
    s=MSTRO_FAIL;
  }
  
  return s;
}

mstro_status
mstro_ofi__failing_msg_handler(const struct mstro_msg_envelope *e)
{
  const void *  dummy1 = e;
  ERR("Invoking default message handler that should never run, likely the context was not initialized at receive time\n");
  abort();
  return MSTRO_FAIL;
}






mstro_status
mstro_ofi__msg_context_create(mstro_ofi_msg_context *result_p,
                              struct mstro_msg_envelope *msg,
                              bool want_completion,
                              bool want_repost, mstro_mempool pool)
{
  mstro_status stat = MSTRO_UNIMPL;
  if(result_p==NULL)
    return MSTRO_INVOUT;
  /* RDMA ops don't have non-NULL msg */
  /* if(msg==NULL) */
  /*   return MSTRO_INVARG; */

  mstro_ofi_msg_context ctx = mstro_mempool_alloc(pool);
  if(ctx==NULL) {
    ERR("Failed to allocate ofi msg context\n");
    return MSTRO_NOMEM;
  }
  ctx->ev = NULL;
  ctx->ep = NULL;
  ctx->pool = pool;
  if(want_completion) {
    int res = pthread_mutex_init(&ctx->lock, NULL);
    if(res!=0) {
      ERR("Failed to init ofi msg ctx lock: %d\n", res);
      stat=MSTRO_FAIL;
      free(ctx);
      ctx=NULL;
      goto BAILOUT;
    }
    res = pthread_cond_init(&ctx->completion, NULL);
    if(res!=0) {
      ERR("Failed to init ofi msg cond var: %d\n", res);
      pthread_mutex_destroy(&ctx->lock);
      free(ctx);
      ctx=NULL;
      stat=MSTRO_FAIL;
      goto BAILOUT;
    }
    ctx->has_completion = 1;
  } else {
    ctx->has_completion = 0;
  }
  ctx->needs_repost = want_repost;
  ctx->msg = msg;
  ctx->fi_cq_entry_flags = 0;
  ctx->msg_handler = mstro_ofi__failing_msg_handler;
  ctx->msg_mr = NULL;
  ctx->msg_mr_desc = NULL;

  stat = MSTRO_OK;
BAILOUT:
  *result_p = ctx;
  return stat;
}

mstro_status
mstro_ofi__msg_context_destroy(mstro_ofi_msg_context ctx)
{
  if(ctx==NULL)
    return MSTRO_INVARG;
  if(ctx->has_completion == 1) {
    pthread_mutex_destroy(&ctx->lock);
    pthread_cond_destroy(&ctx->completion);
  }
  /* clear out things that have confusion potential on reuse */
  ctx->ep = NULL;
  ctx->ev = NULL; 
  if (ctx->msg_mr != NULL) 
	{
		int ret = fi_close((struct fid *)ctx->msg_mr);
		if(ret<0) {
			    ERR("Failed to close send/recv context envelope mreg: %d (%s)\n", ret, strerror(-ret));
		}
	}
  mstro_mempool_free(ctx->pool, ctx);
  
  return MSTRO_OK;
}

/** Check the schema list provided from pm is compatible with the component schema list */
/* compatible means that pm supports all of the schemas in the component schema list or more */
/* FIXME check that the schema versions are compatible between the pm and the component */
bool
schema_list_compatible_p(const char *cmp_schema_list,
		         const char *pm_schema_list)
{
  bool retval=false;
  char *schema_name;
  char *end_list_token;
  char* component_schema_list=NULL;

  if (cmp_schema_list)
    component_schema_list = strdup(cmp_schema_list);
  
  if (component_schema_list == NULL) {
    /*nothing to look for here*/
    //DEBUG("[schema_list_compatible_p] component schema list is NULL");
    retval=true;
    goto BAILOUT;
  } else if (pm_schema_list == NULL) {
    /*component_schema_list is not empty but pm_schema_list is empty ... we should look no further*/
    //DEBUG("[schema_list_compatible_p] pm schema list is NULL");
    retval=true;
    goto BAILOUT;
  }

  // split the schema names in the component_schema_list
  schema_name = strtok_r(component_schema_list, SCHEMA_LIST_SEP, &end_list_token);
  // for each schema name in the component_schema_list ...try to find it in the pm_schema_list
  while( schema_name != NULL ) {
    DEBUG("Looking for schema %s \n", schema_name);
    
    /* if we did not find the component schema name in the pm_schema_list */
    if(strstr(pm_schema_list, schema_name) == NULL) {
      DEBUG("Schema %s not found in pm schema list\n", schema_name);
      retval=false;
      goto BAILOUT;
    }
    DEBUG("Found schema %s in %s\n", schema_name, pm_schema_list);

    // read the next schema name
    schema_name = strtok_r(NULL, SCHEMA_LIST_SEP, &end_list_token);
  }

  /*We reach here only if all component schemas were found in the pm_schema_list*/
  retval=true;
BAILOUT:
  if(component_schema_list!=NULL)
    free(component_schema_list);

  return retval;
}

static
mstro_status
mstro_ofi__check_compatibility(bool *suitable_p,
                               const Mstro__Endpoint *remote,
                               const struct mstro_endpoint *local)
{
  if(suitable_p==NULL)
    return MSTRO_INVOUT;
  if(remote==NULL ||local==NULL)
    return MSTRO_INVARG;
  assert(remote->proto_case == MSTRO__ENDPOINT__PROTO_OFIPROTO);
  assert(local->pbep->proto_case == MSTRO__ENDPOINT__PROTO_OFIPROTO);

  if(remote->ofiproto
     == local->pbep->ofiproto) {
    *suitable_p = true;
     DEBUG("remote ep kind: %d (local match: %d)\n", remote->ofiproto, local->pbep->ofiproto);
  } else {
    *suitable_p = false;
     DEBUG("remote ep kind: %d (local don't match: %d)\n", remote->ofiproto, local->pbep->ofiproto);
  }
  return MSTRO_OK;
}


/** try to use insert the address specificed in @arg remote in @arg local_ep. On sucess returns the translated address in @arg *translated_addr_p */
static
mstro_status
mstro_ofi__try_epd_addr(const struct mstro_endpoint *local_ep,
                        const Mstro__Endpoint *remote,
                        fi_addr_t *translated_addr_p)
{
  mstro_status status=MSTRO_OK;
  
  /* try address insertion */
  uint32_t addr_format;
#define DEFAULT_ADDR_LEN 128
  uint8_t remote_addr[DEFAULT_ADDR_LEN];
  size_t addrlen=DEFAULT_ADDR_LEN;
  
  status = mstro_epd_to_ofi_addr(remote,
                                 &addr_format,
                                 remote_addr, &addrlen);
  if(status!=MSTRO_OK) {
    if(status==MSTRO_NOMEM) {
      /* FIXME: if we get NOMEM we could retry with larger addrlen allocation */
      WARN("FIXME: Endpoint buffer too small and not retrying with larger buffer (have %zu, need %zu)\n",
           DEFAULT_ADDR_LEN, addrlen);
    }
    ERR("Cannot convert EPD to address: %d\n", status);
    goto BAILOUT;
  }
  
  /* try that address in the EP */
  int ret = fi_av_insert(local_ep->av,
                         remote_addr, 1,
                         translated_addr_p,
                         /* no flags, no context: we want
                          * synchronous insert */
                         0, NULL);
  if(ret!=1) {
    /* we asked to insert 1, so we should get 1 insert confirmed */
    ERR("failed to insert source address into endpoint av: %d\n",
        ret);
    status = MSTRO_FAIL; 
    goto BAILOUT;
  }
  {
    size_t size = EP_DESC_BUF_MAX;
    char remote_straddr[EP_DESC_BUF_MAX];
    fi_av_straddr(local_ep->av, remote_addr, remote_straddr, &size);

    char maybe_parsed_cxi_addr[32];
    maybe_parsed_cxi_addr[0]='\0';
    
    if(local_ep->fi->addr_format==FI_ADDR_CXI) {
      const char *prefix = " (CXI ";
      const char *suffix = ")";
      strncpy(maybe_parsed_cxi_addr, prefix, 32);
      mstro_ofi__cxi_straddr(remote_addr, maybe_parsed_cxi_addr+strlen(prefix), 32-strlen(prefix));
      strncat(maybe_parsed_cxi_addr,suffix, 32-strlen(maybe_parsed_cxi_addr));
    }
    DEBUG("AV of ep %s accepted address %s%s\n",
          mstro_endpoint_describe(local_ep), remote_straddr, maybe_parsed_cxi_addr);
  }
  
BAILOUT:
  return status;
}

struct mstro_ofi__read_component_descriptor_ctx {
  /* the component descriptor that the read was performed into */
  union mstro_component_descriptor *cd;
  /* possibly a function to call upon finding an acceptable component descriptor */
  void (*cont)(union mstro_component_descriptor *cd);
  /* information on how to attempt further reads into cd if the data found was not acceptable */
  void *closure;
};

static inline
mstro_status
mstro_ofi__read_component_descriptor_ctx__alloc(struct mstro_ofi__read_component_descriptor_ctx **res)
{
  if(res==NULL)
    return MSTRO_INVOUT;

  *res = malloc(sizeof(struct mstro_ofi__read_component_descriptor_ctx));
  if(*res==NULL) {
    return MSTRO_NOMEM;
  } else {
    return MSTRO_OK;
  }
}

static inline
mstro_status
mstro_ofi__read_component_descriptor_ctx__free(struct mstro_ofi__read_component_descriptor_ctx *ctx)
{
  if(ctx==NULL) {
    ERR("NULL ctx\n");
    return MSTRO_INVARG;
  }
  if(ctx->cd!=NULL) {
    ERR("Unexpected non-NULL cd\n");
    return MSTRO_UNIMPL;
  }
  free(ctx);
  return MSTRO_OK;
}

/** structure to capture the progress state of endpoint selection */
struct mstro_ofi__select_endpoint_closure {
  /** remotes to try */
  const Mstro__AppInfo             *remote;
  /** current remote index */
  size_t                            remote_idx;
  /** set of local endpoints to try */
  struct mstro_endpoint_set        *my_eps; 
  /** current local index */
  size_t                            my_eps_idx; 
  /** component descriptor to be filled */
  union mstro_component_descriptor *dst_cd;
  /** the request handle that allows the user to observe our
   * progress */
  mstro_request                     request_handle;
  /** space for an fi_addr of the selected endpoint if the operation
   * succeeded */
  fi_addr_t                        *remote_addr_p;
  /** space for a reference to the local endpoint selected if the
   * operation succeeded */
  struct mstro_endpoint           **local_p;
};


/** submit an RDMA read operation using @arg my_ep to retrieve
 * component descriptor info from @arg remote/@arg inforeg. Expects message context
 * to be properly populated.
 *
 * Returns MSTRO_NOMATCH if @arg my_ep did not like the remote (and it should be skipped)
 * Returns MSTRO_FAIL if read operation failed repeatedly (and it should be skipped)
 * Returns other error to indicate caller should give up
 *
 * MSTRO_OK indicates that the read operation is submitted and the
 * completion  @arg event will indicate the result is available.
 **/
static
mstro_status
mstro_ofi__submit_component_descriptor_read(struct mstro_endpoint *my_ep,
                                            const Mstro__Endpoint *remote,
                                            const Mstro__OfiMemoryRegion *inforeg,
                                            mstro_event event,
                                            struct mstro_ofi__select_endpoint_closure *sec)
{
  fi_addr_t translated_addr=FI_ADDR_UNSPEC;

  mstro_status stat = mstro_ofi__try_epd_addr(my_ep, remote, &translated_addr);
  mstro_request request;
  if(stat!=MSTRO_OK) {
    DEBUG("local EP did not accept remote addr, skipping\n");
    return MSTRO_NOMATCH;
  }

  /* save address for caller in case this one succeeds */
  *sec->remote_addr_p = translated_addr;
  *sec->local_p = my_ep;
  

  union mstro_component_descriptor *dst = sec->dst_cd;
  
  /* since the EP accepted the address we interpolate that
   * MR_RAW will be set identically (while we really should be
   * checking a flag sent in the OOB info about the MR) */
  assert(!(my_ep->fi->domain_attr->mr_mode & FI_MR_RAW)); /* FIXME */

  uint64_t mr_addr = inforeg->baseaddr; /** FIXME: with RAW we'd need to translate */

  /* key is in tmp->inforeg->raw_key.data */
  uint64_t mr_key=0;
  switch(inforeg->raw_key.len) {
    case 1: { uint8_t  x; memcpy(&x,     inforeg->raw_key.data, sizeof(x)); mr_key = x; break; }
    case 2: { uint16_t x; memcpy(&x,     inforeg->raw_key.data, sizeof(x)); mr_key = x; break; }
    case 4: { uint32_t x; memcpy(&x,     inforeg->raw_key.data, sizeof(x)); mr_key = x; break; }
    case 8: {             memcpy(&mr_key,inforeg->raw_key.data, sizeof(mr_key));   break; }
    default: {
      ERR("Unhandled MR key length -- likely needs OFI based translation\n");
      return MSTRO_UNIMPL;
    }
  }
  
  DEBUG("Checking for PM config block MR at (remote addr) 0x%" PRIx64 ", key of len %zu value %" PRIx64 "\n",
        mr_addr, inforeg->raw_key.len, mr_key);
  
  if(sizeof(g_pm_component_descriptor) > my_ep->fi->ep_attr->max_msg_size) {
            ERR("component descriptor size exceeds endpoint's max_msg_size. FIXME: should split up operation\n");
           
          }

  /* buffer may have been registered at local endpoint set creation if FI_MR_LOCAL was required*/
  void * local_buf_mr_desc = NULL;
  if(my_ep->peer_info_mr!=NULL)
  {
	local_buf_mr_desc = fi_mr_desc(my_ep->peer_info_mr);
  }

  stat = mstro_request__allocate(MSTRO_REQUEST_GENERIC, &request);
  if(stat != MSTRO_OK) {
    ERR("Failed to allocate a maestro_request for fi_read\n");
  }
  stat = mstro_ofi_threads_enqueue_rdma_read(translated_addr, dst, sizeof(union mstro_component_descriptor), 
                                    local_buf_mr_desc, mr_addr, mr_key, 
                                    my_ep, event, request);
  assert(stat == MSTRO_OK);
  DEBUG("enqueued RDMA read operation\n");
  /*wait for request status ...*/
  stat = mstro_request_wait(request);
  if (stat != MSTRO_OK)
  {
    ERR("Failed to submit RDMA read\n");
  }
  
  /**should return here the status of the request*/
  return stat;  
}

/** bottom-half processing to be used in event handler of RDMA read operation on a remote component descriptor */
static
mstro_status
mstro_ofi__read_component_descriptor_bh(mstro_event event,
                                        void *closure)
{
  if(event==NULL) {
    ERR("NULL event\n"); abort();
  }
  if(closure==NULL) {
    ERR("NULL closure\n"); abort();
  }

  struct mstro_ofi__read_component_descriptor_ctx *ctx =
      (struct mstro_ofi__read_component_descriptor_ctx*)closure;


  union mstro_component_descriptor *cd = ctx->cd;
  assert(cd!=NULL);
  
  DEBUG("Component read completion event handler with ctx%p\n", ctx);
  DEBUG("Found remote config block (%s). type %" PRIu64 ", protocol version: %d.%d.%d, workflow: %s, component: %s\n",
        cd->version,
        cd->type,
        cd->protocol_version.major,
        cd->protocol_version.minor,
        cd->protocol_version.patch,
        cd->workflow_name,
        cd->component_name);
  if(cd->type==MSTRO_COMPONENT_TYPE_UNDEF
     || 0!=strcmp(cd->workflow_name, g_component_descriptor.workflow_name)) {
    DEBUG("Component descriptor mismatch, illegal type or other workflow, skipping\n");
    /* submit similar request again */
    goto MAYBE_CONTINUE;
  } else {
    DEBUG("PM schema list %s \n", cd->schema_list);
    DEBUG("Component schema list %s \n", g_component_descriptor.schema_list);
    if(!schema_list_compatible_p(g_component_descriptor.schema_list,
                                 cd->schema_list)) {
      DEBUG("Component descriptor mismatch, PM does not support the schema list of component, skipping\n");
      goto MAYBE_CONTINUE;
    }
  }
SUCCESS: {
  /* we're happy with the info we found */
    void(*cont)(union mstro_component_descriptor*) = ctx->cont; /* extract continuation */
    ctx->cont = NULL; /* kill ref to it in ctx */
    ctx->cd = NULL; /* kill ref to the (local) component descriptor in ctx */
    mstro_status s = mstro_ofi__read_component_descriptor_ctx__free(ctx);
    if(s!=MSTRO_OK) {
      ERR("Failed to deallocate read component descriptor context\n");
      /* really, no way to recover here */
      abort();
    }
    
    /* continue as instructed */
    if(cont) {
      cont(cd);
    }
    return MSTRO_OK;
  }
MAYBE_CONTINUE: {
    ERR("Unimplemented\n");
  }
  
  return MSTRO_UNIMPL;  
}


/* forward decl, since these are mutually recursive */
static
mstro_status
mstro_ofi__select_endpoint__submit_read(
    struct mstro_ofi__select_endpoint_closure *closure);

  
static
void
mstro_ofi__select_endpoint__handle_read_completion(
    mstro_event event, void *context)
{
  mstro_status s = MSTRO_UNIMPL;
  assert(context!=NULL);
  struct mstro_ofi__select_endpoint_closure *sec = (struct mstro_ofi__select_endpoint_closure*)context;
  DEBUG("Read completion for remote idx %zu, local idx %zu\n",
        sec->remote_idx, sec->my_eps_idx);

  /* when we reach this event notification we have successfully read
   * *something*. We may not like what we see and retry another
   * endpoint, but we do not deal with fabric errors here */

  /* TSAN may report a data race here with ofi_memcpy. This happens if
   * we read the EP descriptor multiple times (into the same
   * destination) and access it here .. ofi_memcpy is of course
   * running under various mutex locks, but we don't */
  DEBUG("Found remote config block (%s). type %" PRIu64 ", protocol version: %d.%d.%d, workflow: %s, component: %s\n",
        sec->dst_cd->version, sec->dst_cd->type,
        sec->dst_cd->protocol_version.major,
        sec->dst_cd->protocol_version.minor,
        sec->dst_cd->protocol_version.patch,
        sec->dst_cd->workflow_name, sec->dst_cd->component_name);
  
   if(sec->dst_cd->type==MSTRO_COMPONENT_TYPE_UNDEF
      || 0!=strcmp(sec->dst_cd->workflow_name, g_component_descriptor.workflow_name)) {
     DEBUG("Component descriptor mismatch, illegal type or other workflow, skipping\n");
     goto TRY_NEXT;
   }
   DEBUG("Remote schema list %s\n", sec->dst_cd->schema_list);
   DEBUG("Local component schema list %s\n", g_component_descriptor.schema_list);
   if(!schema_list_compatible_p(g_component_descriptor.schema_list,
                                sec->dst_cd->schema_list)) {
     DEBUG("Component descriptor mismatch, remote does not support the schema list of component, skipping\n");
     goto TRY_NEXT;
   }
   /* end of possible config block verification */

   /* (address and local_p have been saved in sec at submission time,
    * no need to extract/regenerate them here) */

   WITH_MSTRO_EP_DESCRIPTION(str,sec->my_eps->eps+sec->my_eps_idx,{
       INFO("Using local endpoint %s to communicate to remote at %s\n",
             mstro_endpoint_describe(*(sec->local_p)),
             str);});
   mstro_request__generic__mark_complete(sec->request_handle, MSTRO_OK, NULL);

   /* clean up */
   free(sec); /* content is all owned by someone else */
   return;

TRY_NEXT:
   DEBUG("Trying next option\n");
   if(sec->my_eps_idx+1==sec->my_eps->size) {
     sec->my_eps_idx=0;
     sec->remote_idx++;
   } else {
     sec->my_eps_idx++;
   }
   if(sec->remote_idx == sec->remote->eps->n_eps) {
     DEBUG("All endpoint combinations tried, giving up\n");
     goto BAILOUT;
   } else {
    struct mstro_endpoint *myep = sec->my_eps->eps + sec->my_eps_idx;
    const Mstro__Endpoint *remote_ep = sec->remote->eps->eps[sec->remote_idx];
    bool suitable=false;
    s = mstro_ofi__check_compatibility(&suitable, remote_ep, myep);
    if(s!=MSTRO_OK) {
      ERR("Transport coupling check failed: %d(%s)\n", s, mstro_status_description(s));
      goto BAILOUT;
    }
    if (suitable)
    {
       s = mstro_ofi__select_endpoint__submit_read(sec);
    }
    if((!suitable) || (s != MSTRO_OK)) {
      DEBUG("endpoints not compatible\n");
      goto TRY_NEXT;
    }
    /* otherwise: next completion will trigger us again */
     return;
  }

BAILOUT:
   mstro_request__generic__mark_complete(sec->request_handle,
                                             MSTRO_FAIL, NULL);
   return; 
}

static
mstro_status
mstro_ofi__select_endpoint__submit_read(
    struct mstro_ofi__select_endpoint_closure *closure)
{
  mstro_event event=NULL;
  struct mstro_endpoint *myep = NULL;
  
  mstro_status stat=mstro_event_create(
      g_ofi_rdma_edom,
      mstro_ofi__select_endpoint__handle_read_completion,
      closure,
      NULL, false, &event);
  if(stat!=MSTRO_OK) {
    ERR("Failed to create completion event\n");
    if(event) {
      mstro_event_destroy(event);
    }
  }

  /* we now submit the first read op with this context. completion
   * will trigger the event, calling @ref
   * mstro_ofi__handle_select_endpoint_read, which allows us to see
   * whether we were successful. If so, we progress the request handle
   * and clean up. If not we look into the closure to fire up the next
   * check, recycling as much of the ctx/msg/event. If all tries fail,
   * we progress the request handle and indicate an error state in
   * it. */

  myep = closure->my_eps->eps + closure->my_eps_idx;
  const Mstro__Endpoint *remote = closure->remote->eps->eps[closure->remote_idx];
  const Mstro__OfiMemoryRegion *inforeg = closure->remote->eps->inforegs[closure->remote_idx];

  DEBUG("EP: %p, remote %p, inforeg: %p, event: %p, closure->dst %p\n",
        myep,
        remote,
        inforeg,
        event,
        closure->dst_cd);
  
  stat = mstro_ofi__submit_component_descriptor_read(
      myep,
      remote,
      inforeg,
      event,
      closure);

  if(stat!=MSTRO_OK) {
    ERR("Failed to submit component descriptor read operation\n"); /**cleanup is already done at ofi_read*/
  }
  return stat;
}




/** select the best match between remote and local endpoints */
mstro_status
mstro_ofi__select_endpoint(const Mstro__AppInfo *remote,
                           struct mstro_endpoint **local_p,
                           fi_addr_t *remote_addr_p, struct mstro_endpoint_set *my_endpoints,
                           mstro_request *request_handle)
{
  mstro_status stat = MSTRO_UNIMPL;
  struct mstro_endpoint *myep = NULL;
  Mstro__Endpoint *remote_ep = NULL;

  if(remote==NULL) {
    return MSTRO_INVARG;
  }
  if(local_p==NULL || *local_p!=NULL ||remote_addr_p==NULL || request_handle==NULL) {
    return MSTRO_INVOUT;
  }
  
  struct mstro_ofi__select_endpoint_closure *closure=NULL;
  stat = mstro_request__allocate(MSTRO_REQUEST_GENERIC, request_handle);
  if(stat!=MSTRO_OK) {
    ERR("Failed to allocate request handle\n");
    goto BAILOUT;
  }
    
  DEBUG("Checking %zu remote against %zu local endpoints\n",
        remote->eps->n_eps, my_endpoints->size);

  closure = malloc(sizeof(struct mstro_ofi__select_endpoint_closure));
  if(closure==NULL) {
    ERR("Failed to allocate closure\n");
    goto BAILOUT;
  }

  /* state data */
  closure->remote = remote;
  closure->remote_idx = 0;
  closure->my_eps = my_endpoints;
  closure->my_eps_idx = 0;
  closure->dst_cd = &g_pm_component_descriptor;
  
  /* handle for user to check on */
  closure->request_handle = *request_handle;
  
  /* caller provided destinations */
  closure->local_p = local_p;
  closure->remote_addr_p = remote_addr_p;

TRY_NEXT:
  myep = closure->my_eps->eps + closure->my_eps_idx;
  remote_ep = closure->remote->eps->eps[closure->remote_idx];
  bool suitable=false;
  mstro_status status = mstro_ofi__check_compatibility(&suitable, remote_ep, myep);
  if(status!=MSTRO_OK) {
    ERR("Transport coupling check failed: %d\n", status);
    goto BAILOUT;
  }
  if(suitable)
  {
    DEBUG("endpoints compatible!\n");
    stat = mstro_ofi__select_endpoint__submit_read(closure);
  }
  if((!suitable) || (stat != MSTRO_OK)) {
    DEBUG("endpoints not compatible\n");
  
    DEBUG("Trying next option\n");
    if(closure->my_eps_idx+1==closure->my_eps->size) {
      closure->my_eps_idx=0;
      closure->remote_idx++;
    } else {
      closure->my_eps_idx++;
    }
    if(closure->remote_idx == closure->remote->eps->n_eps) {
      DEBUG("All endpoint combinations tried, giving up\n");
      mstro_request__generic__mark_complete(closure->request_handle,
                                              MSTRO_FAIL, NULL);
      return stat;
    }
    goto TRY_NEXT;
  }
  
BAILOUT:
  if(stat!=MSTRO_OK) {
    mstro_request__free(closure->request_handle);
    free(closure);
  }
  return stat;
}

/* FIXME: this needs to move to pool manager protocol code regions */
/* register a new app using its JOIN message. Allocates a new entry in
 * @ref g_mstro_pm_app_table and returns the key to identify it in
 * *entry_p
 *
 * The transport_methods slot will be consumed by the registration and
 * NULLed.
 */
mstro_status
mstro_pm__register_app(mstro_pool_operation op,
                          struct mstro_pm_app_registry_entry **entry_p)
{
  mstro_status s=MSTRO_OK;
  const struct mstro_endpoint *ep = op->join.ep;
  Mstro__Pool__Join *join_msg = op->msg->join;

  assert(entry_p!=NULL);
  assert(join_msg!=NULL);

  /* unpack serialized address */
  Mstro__AppInfo *epd=NULL;
  s = mstro_appinfo_deserialize(join_msg->serialized_endpoint, &epd);
  if(s!=MSTRO_OK) {
    ERR("Failed to deserialize endpoint in JOIN message\n");
    return s;
  }
  DEBUG("incoming serialized EPD: %s\n", join_msg->serialized_endpoint);
  if(epd->eps==NULL) {
    ERR("Empty EPD\n");
    s=MSTRO_FAIL;
    return s;
  }
  
  WITH_MSTRO_EPL_DESCRIPTION(str,epd->eps,{
      DEBUG("EPD parsed as %s\n", str);
    };);

  assert(epd->eps->n_eps>0);

  /* endpoint selection is async. We use an async request handle to find out when it's done */
  mstro_request request=NULL;
  fi_addr_t app_addr=FI_ADDR_NOTAVAIL;
  struct mstro_endpoint *local_ep=NULL;
  s=mstro_ofi__select_endpoint(epd, &local_ep, &app_addr,  mstro_ofi_thread_get_endpointset_ep(g_pm_ofi_team, ep), &request);
  if(s!=MSTRO_OK) {
    ERR("Failed to select a suitable endpoint to talk to component %s:%zu, (%s)\n", 
                                                       join_msg->component_name, 
                                                       join_msg->component_index,  
                                                       mstro_status_description(s));
    return s;
  }

  s=mstro_request_wait(request);
  if(s!=MSTRO_OK) {
    ERR("Failed to select a suitable endpoint to talk to component %s:%zu, (%s)\n", 
                                                          join_msg->component_name,
                                                          join_msg->component_index,
                                                          mstro_status_description(s));
     return s;
  }

  /**save component address to operation*/
  op->join.translated_addr = app_addr;
  
  DEBUG("App %s advertises %zu transport methods\n",
        join_msg->component_name, join_msg->transport_methods->n_supported);

  /* insert into registry table */
  s = mstro_pm_app_register(local_ep, op->join.translated_addr,
                            strdup(join_msg->serialized_endpoint),
                            join_msg->transport_methods,
                            NULL, join_msg->component_name, join_msg->component_index,
                            entry_p);
  if(s!=MSTRO_OK) {
    ERR("Failed to register application: %d (%s)\n", s, mstro_status_description(s));
    DEBUG("Sending WELCOME-NACK\n");
    return s;
  } else {
    join_msg->transport_methods = NULL; /* we kept a reference to it, caller will free the message */
    //DEBUG("Assigned app ID %"PRIu64"\n", (*entry_p)->appid);
  }
  
  return s;
} 
/** allocate a fresh 'short' message envelope, context, and post a receive for it on EP.
    Returns the message envelope in *envelope_p and the context in *ctxt_p.
*/
mstro_status
mstro_ofi__loop_post_recv(struct mstro_endpoint *ep, mstro_mempool pool)
{
  struct mstro_msg_envelope *envelope;
  mstro_status status = mstro_msg_envelope_allocate(&envelope);
  if(status!=MSTRO_OK) {
    goto BAILOUT_EARLY;
  }
  mstro_ofi_msg_context ctxt;
  status = mstro_ofi__msg_context_create(&ctxt, envelope, false, true, pool);
  if(status!=MSTRO_OK) {
    mstro_msg_envelope_free(envelope);
    goto BAILOUT_EARLY;
  }

  DEBUG("Checking and doing memory registration when required\n");
  status = mstro_ofi__maybe_register_local_buffer(
                FI_HMEM_SYSTEM, 
	              0, 	
		            ep->fi->domain_attr, ep->ep,
                ep->domain, envelope->data, envelope->size, 
                &(ctxt->msg_mr), &(ctxt->msg_mr_desc));
  if(status != MSTRO_OK) {
	  ERR("Failed to register or bind envelope buffer for fi_recv\n");
	  if (ctxt->msg_mr != NULL) 
	  {
		  int ret = fi_close((struct fid *)ctxt->msg_mr);
		  if(ret<0) {
			    ERR("Failed to close fi_recv mreg: %d (%s)\n", ret, strerror(-ret));
		  }
	  }
  }
  mstro_ofi__remember_ctx(ep, ctxt);
  
  int res;
  do {
    res = fi_recv(ep->ep,
                  envelope->data, envelope->size,
                  ctxt->msg_mr_desc, FI_ADDR_UNSPEC, ctxt);
    if(res!=0) {
      switch(res) {
        case -FI_EAGAIN:
          WARN("fi_recv wants us to try again; this may indicate overfill of recv queue\n");
          break;
        default:
          ERR("fi_recv failed: %d\n", res, fi_strerror(-res));
          status = MSTRO_OFI_FAIL;
          goto BAILOUT;
      }
    }
  } while(res!=0);

BAILOUT:
  if(status!=MSTRO_OK) {
    mstro_ofi__forget_ctx(ep, ctxt);
    mstro_msg_envelope_free(envelope);
    mstro_ofi__msg_context_destroy(ctxt);
  }

BAILOUT_EARLY:
  return status;
}


/** Poll EP for one completion and handle it, if there is one
 * If expected_slot_p is non-NULL it indicates that if the
 * completion is for a message in this known slot a new RECV should be
 * posted immediately.
 */
//static inline
mstro_status
mstro_ofi__check_and_handle_cq(struct mstro_endpoint *ep,
                               mstro_msg_handler incoming_msg_handler)
{
  assert(ep!=NULL);
  assert(incoming_msg_handler!=NULL);
  mstro_status status = MSTRO_OK;

#ifdef WITH_RMA_COUNTER
  /* check RMA counter (for progress and stats, mostly) */
  uint64_t cnt = fi_cntr_read(ep->rmacnt);
  if(ep->last_rma_cnt!=cnt) {
    DEBUG("Remote RMA occured, now counted %" PRIu64 " REMOTE_READ ops\n", cnt);
    ep->last_rma_cnt=cnt;
  }
#endif

#ifndef MAX_ENTRIES_PER_POLL
#define MAX_ENTRIES_PER_POLL 1
#endif  


  struct fi_cq_tagged_entry entry[MAX_ENTRIES_PER_POLL];
  int ret;
  ret = fi_cq_read(ep->cq, entry, MAX_ENTRIES_PER_POLL);

  static _Atomic(size_t) num_miss=0;
  #ifdef DEBUG_MSTRO_OFI
  static _Atomic(size_t) trials=1;
  static _Atomic(size_t) max_miss=0;
  static _Atomic(size_t) min_miss=SIZE_MAX;
  static _Atomic(size_t) avg_miss=0;
  static _Atomic(size_t) max_hits=0;
  static _Atomic(size_t) min_hits=SIZE_MAX;
  static _Atomic(size_t) avg_hits=0;
  #endif

  /* no-data poll? */
  if(ret== -FI_EAGAIN) {
    num_miss++;
    return MSTRO_OK; /* FI_EAGAIN .. we'll be back */
  }
  /* actual errors */
  if(ret<=0) {
    struct fi_cq_err_entry err_entry;
    memset(&err_entry, 0, sizeof(struct fi_cq_err_entry));
    fi_cq_readerr(ep->cq, &err_entry, 0);
    if(err_entry.err==FI_ECANCELED) {
      DEBUG("cancelation of operation for context %p, cleaning up\n",
            err_entry.op_context);
      mstro_ofi_msg_context ctx = (mstro_ofi_msg_context)err_entry.op_context;
      if(err_entry.flags & (FI_SEND | FI_RECV))
        mstro_msg_envelope_free(ctx->msg);
      mstro_ofi__forget_ctx(ctx->ep, ctx);
      return mstro_ofi__msg_context_destroy(ctx);
    } else {
      ERR("Failure in fi_cq_read: %s CQ error: %s\n",
          fi_strerror(err_entry.err),
          fi_cq_strerror(ep->cq,
                         err_entry.prov_errno,
                         err_entry.err_data, NULL, 0));
      /* FIXME: we should have a way to tell the handler that the
       * operation completed in error. For RMA read of the config block,
       * in particular, this will let the caller retry */
      ERR("FIXME: Dropping context %p\n", err_entry.op_context);
      return MSTRO_FAIL;
    }
  }

  DEBUG("Got %d completion%s after %zu misses\n",
        ret, (ret==1 ? "" : "s"), num_miss);
  #ifdef DEBUG_MSTRO_OFI
  trials++;
  max_miss = (max_miss>num_miss)?max_miss:num_miss;
  min_miss = (min_miss<num_miss)?min_miss:num_miss;
  avg_miss += num_miss;
  assert(ret>=0);
  max_hits = (max_hits>(size_t)ret) ? max_hits : (size_t)ret;
  min_hits = (min_hits<(size_t)ret) ? min_hits : (size_t)ret;
  avg_hits += ret;

  if(trials >= 1000) {
          avg_miss=avg_miss/trials;
          avg_hits = avg_hits/trials;
          INFO("min_miss %zu, max_miss %zu, avg_miss %zu min_hits %zu, max_hits %zu, avg_hits %zu\n",min_miss,max_miss,avg_miss,min_hits,max_hits,avg_hits);
          max_miss = 0;
          min_miss = SIZE_MAX;
          avg_miss = 0;
          trials = 1;
          max_hits = 0;
          min_hits = SIZE_MAX;
          avg_hits = 0;
  }
  #endif
  num_miss=0;

  if(ret>MAX_ENTRIES_PER_POLL) {
    ERR("Unexpectedly large number of CQ entries: %d, expected at most %d\n",
        ret, MAX_ENTRIES_PER_POLL);
    return MSTRO_FAIL;
  }
  DEBUG("found %d completion%s at once\n", ret, (ret==1 ? "" : "s"));

  for(int i=0; i<ret; i++) {
    /* completions can be from our 'slot' or from elsewhere. The
     * context tells us. Sometimes we get spurious ones -- we
     * just drop them. */
    /* DEBUG("Entry %d: %p. Context %p. Flags %ull\n", */
    /*       i, entry[i], entry[i].op_context, entry[i].flags); */

    mstro_ofi_msg_context ctx = (mstro_ofi_msg_context)entry[i].op_context;
    assert(ctx!=NULL);

    if(ctx->needs_repost) {
      //DEBUG("Posting fresh recv on ep \n");
      status = mstro_ofi__loop_post_recv(ep, ctx->pool);
      if(status!=MSTRO_OK) {
        ERR("Failed to re-post fresh recv context: %d\n", status);
      }
    }

    /* we only know how to handle some entry types, so check before digging deeper */

    if(! (entry[i].flags & (FI_RECV | FI_SEND | FI_RMA))) {
      WARN("Unexpected CQ event, flags %"PRIu64", ignoring\n", entry[i].flags);
    } else {
      /* these permit us to not pass ofi 'entry' to handler. That is a
       * pre-requisite to have multiple entries here (that we can free
       * at the end of the function), since contexts survive the entry
       * lifetime. */
      ctx->fi_cq_entry_flags = entry[i].flags;
      ctx->msg_handler = incoming_msg_handler;
      ctx->ep=ep;

      struct mstro_msg_envelope *msg = ctx->msg;
      if(msg!=NULL) {
        /* SEND and RECV */
        /* The len field of fi_cq_tagged_entry only applies to completed receive operations. 
        It indicates how many data bytes were placed into the associated receive buffer by a corresponding fi_send/fi_tsend/etc... call. 
        Please note that the recieved data is in the posted context. 
        The buf field of fi_cq_tagged_entry is only valid for completed receive operations, and only applies when the receive buffer was posted with the FI_MULTI_RECV flag.
        (COMPLETION FIELDS section: https://ofiwg.github.io/libfabric/v1.6.1/man/fi_cq.3.html )*/
        msg->payload_size = entry[i].len;
        msg->ep = ep;
      } else {
        assert(entry[i].flags & FI_RMA); /* here we have no msg */
      }
      DEBUG("Calling mstro_ofi__handle_cq_entry on %p\n", ctx);
      status = mstro_ofi__handle_cq_entry(ctx);
    }
  }

  return status;
}

/** value of @ref MSTRO_ENV_OFI_NUM_RECV environment variable.
 *
 * If 0, an endpoint-specific default will be used (computed using
 * mstro_ofi__default_num_recv()) */
static size_t g_mstro_ofi_num_recv_per_ep = 0;

/** compute a reasonable fraction of the supported number of receives
 ** of the @arg ep that we can use for pre-posting
 **/

static inline size_t
mstro_ofi__default_num_recv(const struct mstro_endpoint *ep)
{
#define MSTRO_OFI_NUM_RECV_DIVISOR ((double)(2.0/4.0))
  uint64_t val = (uint64_t)(ep->fi->rx_attr->size * MSTRO_OFI_NUM_RECV_DIVISOR);
  if(val==0) // ensure at least one
    val = 1;
  if(val>512) // cap it
    val = 512;
  DEBUG("EP claims %" PRIu64 " concurrent receives supported; pre-posting %zu\n",
       ep->fi->rx_attr->size, val);

  return val;
}

/** determine the number of recv contexts to pre-post on EP.
 *
 * consults @ref MSTRO_ENV_OFI_NUM_RECV, and if that's not set, uses
 * the ep-specific refault computed by @ref
 * mstro_ofi__default_num_recv()
 */
size_t
mstro_ofi__get_num_recv(const struct mstro_endpoint *ep)
{
  size_t res;
  if(g_mstro_ofi_num_recv_per_ep!=0) {
    res = g_mstro_ofi_num_recv_per_ep;
  } else {
    char * v = getenv(MSTRO_ENV_OFI_NUM_RECV);
    if(v) {
      g_mstro_ofi_num_recv_per_ep = atoi(v);
      res = g_mstro_ofi_num_recv_per_ep;
    } else {
      res = mstro_ofi__default_num_recv(ep);
    }
  }
  INFO("Using %zu outstanding receives on ep %p\n", res, ep);
  return res;
}

/* FIXME: should go to pool.c */

/* internal entry point: started in a pthread, runs Pool client loop until it
 * is told to terminate (when g_pc_stop is set) */

static _Atomic bool g_pc_stop; /* static init 0 means: no */


mstro_status
mstro_pc_terminate(void)
{
  mstro_status status;

  INFO("Pool client termination initiated\n");

  Mstro__Pool__Leave leave = MSTRO__POOL__LEAVE__INIT;

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&leave);
  if(status !=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        leave.base.descriptor->name);
    goto BAILOUT;
  }

  status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);

  /* The BYE message will be handled by the normal incoming message handler
   * We can tell it's done by checking g_mstro_pm_attached */
  switch(status) {
    case MSTRO_OK: {
      /* spin-wait */
      while(atomic_load(&g_mstro_pm_attached)) {
#if defined(_POSIX_PRIORITY_SCHEDULING)
        sched_yield();
#endif
        usleep(10); /*usec*/
      }
      DEBUG("Detached from pool manager\n");
      g_pm_endpoint=NULL;
      break;
    }
    case MSTRO_NO_PM: {
      /* simulate detach (local only) */
      atomic_store(&g_mstro_pm_attached, false);
      g_pool_app_id = MSTRO_APP_ID_INVALID;
      DEBUG("Simulated detach (no PM)\n");
      break;
    }
    default:
      ERR("Failed to send LEAVE message to pool manager: %d (%s)\n",
          status, mstro_status_description(status));
      return status;
  }

  /* terminate PC thread */
  atomic_store(&g_pc_stop, true);
  status = mstro_ofi_thread_team_stop_and_destroy(g_pc_ofi_team);
  if(status!= MSTRO_OK) {
    ERR("pool client OFI thread termination with error: %d (%s)\n",
        status, mstro_status_description(status));
    status = MSTRO_FAIL;
  } else {
    status = MSTRO_OK;
  }
  DEBUG("Pool client OFI thread(s) terminated\n");

  /* the main PC thread will have done the teardown of the msg handler threads */

  mstro_status status2 = mstro_ofi_finalize(true);
  if(status2!=MSTRO_OK) {
    ERR("Failed to finalize OFI layer: %d\n", status2);
  }
  if(status==MSTRO_OK) {
    status = status2;
  }

  

BAILOUT:
  return status;
}

/* end of FIXME (code for pool.c) */
static inline
char * 
mstro_ofi__find_target_pm_block(const char *remote_pm_info)
{
  /**inspect pm info and identify how many ofi threads pm has*/
  int pm_ofi_threads_count = 0;
  char *tmp = strdup(remote_pm_info);
  char *token=tmp;

  token = strtok(token, PM_INFO_THREAD_SEP);
	while(token!=NULL)
	{
		pm_ofi_threads_count++;
    token = strtok(NULL, PM_INFO_THREAD_SEP);
	}
  /** calculate which pm thread we should target based on our component name and index*/
  assert(g_initdata != NULL);
  khint_t index_hash = kh_int64_hash_func(g_initdata->component_index);
  khint_t name_hash = kh_str_hash_func(g_initdata->component_name);
  khint_t combined_hash = mstro_khash_combine(name_hash, index_hash);
  int target_pm_ofi_thread  = combined_hash%pm_ofi_threads_count;
  DEBUG("number of ofi blocks %d, combined hash %u , target %d\n", pm_ofi_threads_count, combined_hash, target_pm_ofi_thread);
  /**find the target_pm_ofi_thread block in remote_pm_info */
  int count = 0;
  token = remote_pm_info;
  token = strtok(token, PM_INFO_THREAD_SEP);
	while((token!=NULL) && (count < target_pm_ofi_thread))
	{
    count++;
		token = strtok(NULL,PM_INFO_THREAD_SEP);
	}
  DEBUG("found pm_info block %d, with %s\n", count, token);
  free(tmp);
  return token;
}

/** Attach current application to a running pool manager */
mstro_status
mstro_pm_attach(const char *remote_pm_info)
{
  /* GNI cookies are special. We need to have the matching cookie before we can enable the endpoint. */
  Mstro__AppInfo *pm_epd=NULL;

  char *target_pm_block = mstro_ofi__find_target_pm_block(remote_pm_info);
  mstro_status s=mstro_appinfo_deserialize(target_pm_block, &pm_epd);
  

  if(s!=MSTRO_OK) {
    ERR("Failed to parse pool manager info: %d (%s%s)\n",
        s, mstro_status_description(s),
        /* parser errrors also if a NULL result */
        s==MSTRO_NOMEM ? " or invalid pool-manager info data" : "");
    goto BAILOUT;
  }
  const Mstro__EndpointList *epl = pm_epd->eps;
    
  DEBUG("Parsed %d pool manager endpoints\n", epl->n_eps);
  assert(epl->n_eps==epl->n_inforegs);
  assert(epl->n_eps==epl->n_credentials);
  
  {
    size_t num_cookies = 0;

    for(size_t i=0; i<epl->n_credentials; i++) {
      WITH_MSTRO_EPL_ENTRY_DESCRIPTION(str, epl, i, {
          DEBUG("Inspecting pool manager endpoint for credentials: %s\n", str);
        });

      if(epl->credentials[i]) {
        if(epl->eps[i]->ofiproto == MSTRO__OFI_ENDPOINT_KIND__GNI) {
          assert(epl->credentials[i]->val_case == MSTRO__OFI_CREDENTIAL__VAL_DRC);
          DEBUG("GNI descriptor found, oob cookie %" PRIu32 "\n",
                epl->credentials[i]->drc->credential);
          if(num_cookies<1) {
            s = mstro_drc_init_from_credential(&g_drc_info,
                                               epl->credentials[i]->drc->credential);
            if(s!=MSTRO_OK) {
              ERR("Failed to use provided DRC credential\n");
              goto BAILOUT;
            } else {
              num_cookies++;
            }
          } else {
            WARN("Multiple GNI cookies found, first one will win\n");
          }          
        }
        else if (epl->eps[i]->ofiproto == MSTRO__OFI_ENDPOINT_KIND__CXI)
        {	
		      //assert(epl->credentials[i]->val_case == MSTRO__OFI_CREDENTIAL__VAL_CXI); //FIXME we can not check that now because if vni is null this test will fail
          if (g_vni_info == NULL)
          {
            s = mstro_vni_init_from_credential(&g_vni_info,
				    epl->credentials[i]->cxi);
		        if (s == MSTRO_UNIMPL) {
			        WARN("VNIs are not enabled yet\n");
		        }
		        else if(s!=MSTRO_OK) {
			        ERR("Failed to use provided VNI credential\n");
			        goto BAILOUT;
		        }
	        }
        } 
        else {
          if(epl->credentials[i]->val_case != MSTRO__OFI_CREDENTIAL__VAL__NOT_SET) {
            DEBUG("Credential found in non-GNI/VNI EP. This is unsupported -- FIXME\n");
          }
        }
      } else {
        if(epl->eps[i]->ofiproto == MSTRO__OFI_ENDPOINT_KIND__GNI) {
          WARN("GNI or CXI endpoint and no DRC/VNI token, things likely will go wrong\n");
        }
      }
    }
  }

  s=mstro_ofi_init();
  if(s!=MSTRO_OK) {
    ERR("Failed to initialize OFI layer\n");
    goto BAILOUT;
  }

  #define DEFAULT_OFI_NUM_THREADS 1
  char *e = getenv(MSTRO_ENV_PM_PC_NUM_THREADS);
  int num_pc_threads;
  
  if(e!=NULL) {
    num_pc_threads = atoi(e);
    if(num_pc_threads<DEFAULT_OFI_NUM_THREADS) {
      ERR("Value of %s=%e illegal, using %d\n", DEFAULT_OFI_NUM_THREADS);
    }
  } else {
      num_pc_threads = DEFAULT_OFI_NUM_THREADS;
  }
  atomic_store(&g_pc_stop,false); /* in case someone does a second round of mstro_init */

  s = mstro_ofi_thread_team_create("pc_ofi_team", "PC_OFI", 
                                        num_pc_threads,
                                        mstro_pc_handle_msg,
                                        &g_pc_ofi_team);
  if(s !=MSTRO_OK) {
    ERR("Failed to start OFI PC thread(s) \n");
    s=MSTRO_FAIL;
    goto BAILOUT;
  }
  /**wait for all threads to build their endpoints ...!!! BLOCKING CALL !!!*/
  mstro_ofi_thread_team_wait_ready(g_pc_ofi_team);
  /* all endpoints are now enabled. */

  g_component_descriptor.type = MSTRO_COMPONENT_TYPE_APP;

  /* endpoint selection is async. We use an async request
   * handle to find out when it's done */
  mstro_request request=NULL;
  s=mstro_ofi__select_endpoint(pm_epd, &g_pm_endpoint, &g_pm_addr, g_pc_ofi_team->members[0].ctx.ep_set,
                               &request);
  if(s!=MSTRO_OK) {
    ERR("Failed to select a suitable endpoint to talk to pool manager\n");
    goto BAILOUT;
  }

  
  s=mstro_request_wait(request);

  if(s!=MSTRO_OK) {
    ERR("Failed to select a suitable endpoint to talk to pool manager\n");
    goto BAILOUT;
  }

  assert(g_pm_endpoint!=NULL);
  WITH_MSTRO_EP_DESCRIPTION(str,g_pm_endpoint,
                 {DEBUG("Selected PM endpoint is %s == %s, addr %"PRIu64" \n", str, mstro_endpoint_describe(g_pm_endpoint), g_pm_addr);});
  /* register pool manager as a pmp app entry */
  s = mstro_pm_app_register_manager(g_pm_endpoint, g_pm_addr);
  if(s!=MSTRO_OK) {
    ERR("Failed to register Pool Manager in registry\n");
    goto BAILOUT;
  }


  /* send JOIN */


  /* FIXME make function set_transport_default() beware protobuf stack init
   */
  Mstro__Pool__TransportKind transport_kinds[] = {
    MSTRO__POOL__TRANSPORT_KIND__OFI
    ,MSTRO__POOL__TRANSPORT_KIND__GFS
#ifdef HAVE_MIO
    ,MSTRO__POOL__TRANSPORT_KIND__MIO
#endif
    ,MSTRO__POOL__TRANSPORT_KIND__INLINE
  };
  size_t num_available_transports = sizeof(transport_kinds)/sizeof(transport_kinds[0]);

  Mstro__Pool__TransportMethods transport_methods
      = MSTRO__POOL__TRANSPORT_METHODS__INIT;
  transport_methods.n_supported = num_available_transports;
  transport_methods.supported
      = (Mstro__Pool__TransportKind*)&transport_kinds;

  const char* env_transport_default = getenv(MSTRO_ENV_TRANSPORT_DEFAULT);
  if (env_transport_default != NULL) {
    int i;
    int found = 0;
    for (i = 0;	i < MSTRO__POOL__TRANSPORT_KIND__NUMBER_OF_KINDS; i++ ) {
      if (!strcmp(env_transport_default, g_transport_registry[i].name)) {
        transport_methods.supported[0] = (Mstro__Pool__TransportKind)i;
        found = 1;
        DEBUG("Setting default transport method to %s (env %s)\n",
              g_transport_registry[i].name,
              MSTRO_ENV_TRANSPORT_DEFAULT);
      }
    }
    if (! found)
      WARN("default transport method given (env %s)  not found, using maestro defaults\n",  env_transport_default);
  }
  DEBUG("Preferred transport list: \n");
  size_t i;
  for (i = 0; i < transport_methods.n_supported; i++ )
    DEBUG("#%d %s \n", i, g_transport_registry[transport_methods.supported[i]].name);
  /**/

  Mstro__Pool__Join join = MSTRO__POOL__JOIN__INIT;
  join.protocol_version = MSTRO_POOL_PROTOCOL_VERSION;
  assert(g_pc_ofi_team->members[0].ctx.ep_set->size>0);
  assert(g_pc_ofi_team->members[0].ctx.ep_set->eps[0].pbep!=NULL);
  s = mstro_ofi__epl_to_serialized_appinfo(&join.serialized_endpoint,
                                           &(g_pc_ofi_team->members[0].ctx.ep_set->eps[0]));
  if(s!=MSTRO_OK) {
    ERR("Failed to package serialized endpoint info\n");
    goto BAILOUT;
  }

  join.transport_methods = &transport_methods;
  join.component_name = g_initdata->component_name;
  join.component_index = g_initdata->component_index;

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  s = mstro_pmp_package(&msg, (ProtobufCMessage*)&join);
  if(s!=MSTRO_OK) {
    ERR("Failed to package %s into a pool manager message\n",
        join.base.descriptor->name);
    goto BAILOUT;
  }

  assert(atomic_load(&g_mstro_pm_attached)==false);
  atomic_store(&g_mstro_pm_rejected, false);

  s = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);

  /* The WELCOME message will be handled by the normal incoming
   * message handler.  We can tell it's done by checking
   * g_pool_app_id */

  switch(s) {
    case MSTRO_OK: {
      /* spin-wait */
      while(!atomic_load(&g_mstro_pm_attached)) {
	if(atomic_load(&g_mstro_pm_rejected)) {
	  ERR("Pool Manager rejected JOIN: workflow-name/component-name/component-index triple not unique\n");
          mstro_status stmp = mstro_pc_terminate();
          if (stmp != MSTRO_OK)
            ERR("Couldn't cleanup after PM rejection\n");
          else
            DEBUG("Successfully cleaned up after PM rejection\n");

	  s = MSTRO_NOT_TWICE;
	  goto BAILOUT;
	}
#if defined(_POSIX_PRIORITY_SCHEDULING)
        sched_yield();
#endif
        usleep(10); /*usec*/
      }
      break;
    }
    case MSTRO_NO_PM:
      ERR("Confusion: JOIN message failed with 'no PM info' error\n");
      /* fall-thru */
    default:
      ERR("Failed to send JOIN message: %d (%s)\n",
          s, mstro_status_description(s));
      goto BAILOUT;
  }


  /* Welcome received */
  INFO("Welcome message received, our id is: app %" PRIappid "\n",
       g_pool_app_id);


BAILOUT:
  if(join.serialized_endpoint) {
    free(join.serialized_endpoint);
  }
  if(pm_epd) {
    mstro__app_info__free_unpacked(pm_epd, NULL);
  }
  return s;
}

/* atexit handler, called from @ref mstro__atexit() */
mstro_status
mstro_ofi__atexit(void)
{
  /** we need to clean up DRC, if enabled, and we cannot do that
   * unless we're really terminating, because one workload manager
   * allocation can not obtain and release and re-obtain a cookie */
  /* g_drc_info should be non-NULL at termination only when we were pool manager at some point */
  if(g_drc_info!=NULL)
    mstro_drc_destroy(g_drc_info, true);

  if(g_vni_info!=NULL)
    mstro_vni_destroy(g_vni_info);

  return MSTRO_OK;
}




