#include "maestro/i_pool_client.h"

#include "maestro/i_globals.h"
#include "maestro/logging.h"
#include "maestro/i_pool.h"
#include "maestro/i_pool_manager_registry.h"
#include "maestro/i_pool_manager_protocol.h"
#include "../transport/transport.h"
#include "transport/transport_rdma.h"
#include "maestro/i_ofi.h"
#include "maestro/i_ofi_threads.h"
#include "maestro/i_statistics.h"
#include "i_subscription_registry.h"
#include "maestro/i_misc.h"
#include "i_thread_team.h"
#include "maestro/i_maestro_numa.h"

#include <unistd.h>
#include <inttypes.h>

#ifdef HAVE_MIO
#include <mio.h> /* the MIO one */
#include "transport/transport_mio.h"
#endif


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PC,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PC,__VA_ARGS__)


/**function stack to handle pc init transfer msg*/
const mstro_pool_op_st_handler mstro_pc__init_transfer_steps[] = {
                                                      NULL,                                    
                                                      mstro_pc__prepare_init_transfer,          
                                                      mstro_pc__app_befriend,                   
                                                      mstro_pc__reg_friend_app,       
                                                      mstro_pc__init_transfer_send_ticket,           
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };


/**function stack to handle pc transfer ticket */
const mstro_pool_op_st_handler mstro_pc__transfer_steps[] = {
                                                      NULL,                                    
                                                      mstro_pc__prepare_transfer,          
                                                      mstro_pc__app_befriend,  /**optional*/                  
                                                      mstro_pc__reg_friend_app,  /*optional*/     
                                                      mstro_pc__transfer_execute,           
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc declare ack */
const mstro_pool_op_st_handler mstro_pc__declare_ack_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__declare_ack,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc subscribe ack*/
const mstro_pool_op_st_handler mstro_pc__subscribe_ack_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__subscribe_ack,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc resolve reply*/
const mstro_pool_op_st_handler mstro_pc__resolve_reply_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__resolve_reply,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc PoolOp Ack steps*/
const mstro_pool_op_st_handler mstro_pc__poolop_ack_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__poolop_ack,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc bye operation steps*/
const mstro_pool_op_st_handler mstro_pc__bye_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__bye,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc event operation steps*/
const mstro_pool_op_st_handler mstro_pc__event_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__event,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };

/**function stack to handle pc transfer completed operation steps*/
const mstro_pool_op_st_handler mstro_pc__transfer_completed_steps[] = {
                                                      NULL,                                    
                                                      NULL,          
                                                      NULL,                   
                                                      mstro_pc__transfer_completed,  /*execute*/     
                                                      NULL,        
                                                      NULL,                   
                                                      NULL, 
                                                      NULL                 
                                                      };                                                    

/**PC OFI independent threads handle*/
extern struct mstro_ofi_thread_team *g_pc_ofi_team;

mstro_status
mstro_pc__declare_ack(mstro_pool_operation op)
{

  const Mstro__Pool__DeclareAck *declare_ack = op->msg->declare_ack;
  assert(declare_ack!=NULL);
  DEBUG("CDO DeclareAck for serial %zu\n", declare_ack->serial);

  if(declare_ack->cdoid==NULL)
    return MSTRO_INVMSG;

  struct mstro_cdo_id cdoid;
  cdoid.qw[0] = declare_ack->cdoid->qw0;
  cdoid.qw[1] = declare_ack->cdoid->qw1;
  cdoid.local_id = declare_ack->cdoid->local_id;
  assert(declare_ack->serial == cdoid.local_id);

  /* feed into local CDO layer */
  return mstro_cdo_declare_completion(declare_ack->serial, &cdoid, declare_ack->channel);
}


mstro_status
mstro_pc__subscribe_ack(mstro_pool_operation op)
{
  const Mstro__Pool__SubscribeAck *sack = op->msg->subscribe_ack;
  assert(sack!=NULL);
  assert(sack->handle!=NULL);
  DEBUG("SubscribeAck received, local id %" PRIu64", handle %" PRIu64 "\n",
        sack->serial, sack->handle->id);

  mstro_status s = mstro_subscription_register_bh(sack);
  if(s!=MSTRO_OK) {
    ERR("Failed to handle subscription ack for local id %" PRIu64 "\n",
        sack->serial);
  }
  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_SUBSCRIBE, 1);
  return s;
}


mstro_status
mstro_pc__resolve_reply(mstro_pool_operation op)
{
  const Mstro__Pool__ResolveReply *reply = op->msg->resolve_reply;
  assert(reply!=NULL);
  assert(reply->query!=NULL);
  switch(reply->query->query_case) {
    case MSTRO__POOL__RESOLVE__QUERY_CDOID:
      DEBUG("Incoming RESOLVE-REPLY for a CDO-ID (result: |%s|)\n",
            reply->name);
      mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_RESOLVE, 1);
      return mstro_pool_resolve_reply_bh(reply);
    default:
      ERR("FIXME: unhandled resolve-reply for a query of type %d\n",
          reply->query->query_case);
      return MSTRO_FAIL;
  }
}


/* store @arg new_attributes in the serialized attributes slot of @arg
 * cdo, and replace the attribute dict values accordingly. Consumes
 * @arg new_attributes. */
static inline
mstro_status
mstro_cdo__replace_attributes(mstro_cdo cdo,
                              Mstro__Pool__Attributes *new_attributes)
{
  /* We can't lock a CDO on it's own. But this function should only be
   * called before SEAL state change completes, or during WITHDRAW
   * etc. and so users can't be accessing the CDO legally, and it's
   * way before or after the CDO could be in the pool, so transport
   * etc can't touch it either. */
  assert(mstro_cdo_state_check(cdo, MSTRO_CDO_STATE_DECLARED));

  mstro_status s = MSTRO_UNIMPL;

  Mstro__Pool__Attributes *tmp = cdo->attributes_msg;
  cdo->attributes_msg = new_attributes;
  mstro__pool__attributes__free_unpacked(tmp, NULL);

  s = mstro_attribute_dict_clear(cdo->attributes);
  if(s!=MSTRO_OK) {
    ERR("Failed to clear existing attribute dictionary on CDO\n");
    goto BAILOUT;
  }

  /* insert the special ones */
  if(cdo->raw_ptr) {
    s = mstro_attribute_dict_set(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                 MSTRO_CDO_ATTR_VALUE_pointer,
                                 cdo->raw_ptr, true, false);
    if(s!=MSTRO_OK) {
      ERR("Failed to re-set raw-ptr on CDO\n");
      goto BAILOUT;
    }
  }

  if(cdo->mamba_array) {
    s = mstro_attribute_dict_set(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_MAMBA_ARRAY,
                                 MSTRO_CDO_ATTR_VALUE_pointer,
                                 cdo->mamba_array, true, false);
    if(s!=MSTRO_OK) {
      ERR("Failed to re-set mamba_array on CDO\n");
      goto BAILOUT;
    }
  }

  /* insert all from message */
  s = mstro_cdo_attributes_update_incoming(cdo, cdo->attributes_msg, 0, NULL);
  if(s!=MSTRO_OK) {
    ERR("Failed to insert new attributes into CDO\n");
    goto BAILOUT;
  }
  WITH_CDO_ID_STR(idstr, &cdo->gid,
                  DEBUG("Replaced attributes on CDO |%s| (%s, @%p)\n",
                        cdo->name, idstr, cdo););

BAILOUT:
  return s;
}


mstro_status
mstro_pc__poolop_ack(mstro_pool_operation op)
{
  Mstro__Pool__PoolOpAck *poack = op->msg->ack;
  mstro_status status = MSTRO_UNIMPL;
  assert(poack!=NULL);

  DEBUG("PoolOpAck for a %s operation\n",
        (protobuf_c_enum_descriptor_get_value(
            &mstro__pool__pool_op_ack__pool_op__descriptor, poack->op))
        ->name);

  if(!poack->op)
    return MSTRO_INVMSG;

  if(poack->status!=MSTRO__POOL__POOL_OP_ACK__POOL_OP_STATUS__OK) {
    WARN("PoolOpAck contains error status\n");
  }

  mstro_cdo_state new_state;
  Mstro__Pool__Attributes *new_attributes = NULL;

  switch(poack->op) {
    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__SEAL: {
      if(poack->payload_case == MSTRO__POOL__POOL_OP_ACK__PAYLOAD_UPDATED_ATTRIBUTES) {
        DEBUG("SEAL-ACK has updated attributes payload\n");
        assert(poack->updated_attributes!=NULL);
        /* blindly replace attributes, relying on the fact that the PM knows what's good for us */
        new_attributes = poack->updated_attributes;
        poack->updated_attributes = NULL; /* we consume this */
      }
      new_state = MSTRO_CDO_STATE_SEALED;
      goto shared_cdo_state_update;
    }

    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__OFFER:
      new_state = MSTRO_CDO_STATE_OFFERED;
      goto shared_cdo_state_update;

    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__REQUIRE: ;
      new_state = MSTRO_CDO_STATE_REQUIRED;
      goto shared_cdo_state_update;

    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__RETRACT:
      new_state = MSTRO_CDO_STATE_RETRACTED_GLOBALLY;
      goto shared_cdo_state_update;

    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__WITHDRAW:
      new_state = MSTRO_CDO_STATE_WITHDRAWN_GLOBALLY;
    shared_cdo_state_update: {
        assert(poack->operand_case==MSTRO__POOL__POOL_OP_ACK__OPERAND_CDOID);
        assert(poack->cdoid!=NULL);
        struct mstro_cdo_id id = {.qw[0] = poack->cdoid->qw0,
                                  .qw[1] = poack->cdoid->qw1,
                                  .local_id = poack->cdoid->local_id };
        mstro_cdo cdo;
        mstro_cdo__find_cdo(&id, &cdo);
        if(cdo==NULL) {
          WITH_CDO_ID_STR(idstr, &id, {
              ERR("PoolOpAck for unknown CDO %s\n", idstr);
              return MSTRO_INVARG;
            });
        }
        if(new_attributes!=NULL) {
          assert(poack->op == MSTRO__POOL__POOL_OP_ACK__POOL_OP__SEAL);
          mstro_cdo__replace_attributes(cdo, new_attributes);
        }
         if(new_state==MSTRO_CDO_STATE_REQUIRED) {
           mstro_cdo_state tmp = mstro_cdo_state_get(cdo);
           /* not waiting for require_ack in when retracting, so let's ignore it if it's late */
           if (tmp == MSTRO_CDO_STATE_RETRACTED)
        //     ||tmp == MSTRO_CDO_STATE_DISPOSED)
              goto BAILOUT;
        }
        if(new_state==MSTRO_CDO_STATE_OFFERED
           && (mstro_cdo_state_get(cdo) & MSTRO_CDO_STATE_OFFERED)) {
          /* an implicit ack was performed at initiate-transfer time */
          WITH_CDO_ID_STR(idstr, &id, {
              DEBUG("CDO %s: state already set to OFFERED by implicit ack due to INITIATE-TRANSPORT\n",
                    idstr);
            });
        } else {
          status = mstro_cdo_state_set_safe_flags(cdo, new_state);
          if (status == MSTRO_OK)
          {
              WITH_CDO_ID_STR(idstr, &id, {
              DEBUG("CDO %s now in state %d (%s)\n",
                    idstr, new_state, mstro_cdo_state_describe(new_state));
            });
          }
          else if ((status == MSTRO_INVARG) && (mstro_cdo_state_get(cdo) & new_state))
          {
            DEBUG("Some other thread was faster and already updated cdo state\n");
          }
          else{
            ERR("Failed to update CDO state\n");
            return status;
          }
          
          
        }
        break;
      }

    case MSTRO__POOL__POOL_OP_ACK__POOL_OP__UNSUBSCRIBE: {
        status = mstro_subscription_unregister_bh(poack->handle);
        if(status!=MSTRO_OK) {
          ERR("Failure unregistering subscription handle %" PRIu64 "\n",
              poack->handle->id);
          return status;
        }
        break;
      }
    default:
      ERR("PoolOpAck for invalid pool operation %d\n", poack->operand_case);
      return MSTRO_INVMSG;
  }
BAILOUT:
  return MSTRO_OK;
}


/** select the best way to transfer CDO, based on what the dst
 *   supports.  Fills in the (caller-provided) TransferTicket
 *   regarding the method and the ticket union.
 */
static inline
mstro_status
mstro_pc__select_transfer_method(mstro_cdo cdo,
                                 const Mstro__Pool__TransportMethods *dst_methods,
                                 Mstro__Pool__TransferTicket *selected)
{
  if(dst_methods==NULL)
    return MSTRO_INVARG;
  if(selected==NULL)
    return MSTRO_INVOUT;

  if(dst_methods->n_supported==0) {
    ERR("No transport method advertised by dst\n");
    return MSTRO_FAIL;
  }
  cdo=cdo; /* avoid 'unused param' warning */

  // Arbitrarily picking up the preferred transport method
  switch (dst_methods->supported[0]) {
    case MSTRO__POOL__TRANSPORT_KIND__GFS:
      selected->method = MSTRO__POOL__TRANSPORT_KIND__GFS;
      selected->ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_GFS;
      break;
    case MSTRO__POOL__TRANSPORT_KIND__MIO:
      selected->method = MSTRO__POOL__TRANSPORT_KIND__MIO;
      selected->ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_MIO;
      break;
    case MSTRO__POOL__TRANSPORT_KIND__OFI:
      selected->method = MSTRO__POOL__TRANSPORT_KIND__OFI;
      selected->ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_OFI;
      break;
    case MSTRO__POOL__TRANSPORT_KIND__INLINE:
      selected->method = MSTRO__POOL__TRANSPORT_KIND__INLINE;
      selected->ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_INLINE;
      break;
    default:
      ERR("Add a case in the TransportKind methods vs TicketCase switch\n");
      return MSTRO_UNIMPL;
  }
  
  if((size_t)selected->data_size < g_transport_inline_max) {
    INFO("CDO %s is small (%zu < %zu), choosing inline transport\n",
         selected->distributed_cdo ? "(fragment)" : "",
         selected->data_size, g_transport_inline_max);
    selected->method = MSTRO__POOL__TRANSPORT_KIND__INLINE;
    selected->ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_INLINE;
  }
    
  return MSTRO_OK;
}



static inline
mstro_status
mstro_pc__construct_gfs_path_for_cdo(const mstro_cdo src_cdo,
                                     char **path)
{
  mstro_status s=MSTRO_UNIMPL;
  assert(path!=NULL);
  assert(src_cdo!=NULL);

  size_t l1 = g_mstro_transport_gfs_dir_len // name incl. trailing separator
              + MSTRO_APP_ID_STR_LEN + 1 // appid plus separator
              + MSTRO_CDO_ID_STR_LEN + 1 // CDOID.local-id plus separator
              ;
  size_t l2 = MSTRO_CDO_ID_STR_LEN + 1   // local-id for local-index
              + 1                        // NUL
              ;
  size_t l = l1+l2;
  *path = malloc(l);
  if(*path==NULL) {
    return MSTRO_NOMEM;
  }

  /* make the directory path */
  WITH_CDO_ID_STR(idstr, &src_cdo->gid, {
    size_t n = snprintf(*path, l1, "%s%" PRIappid "/%s/",
                        g_mstro_transport_gfs_dir,
                        g_pool_app_id,
                        idstr);
    if(n>=l1) {
      ERR("GFS path truncated for CDO %s\n", idstr);
      free(*path);
      return MSTRO_FAIL;
    }
  });
  s = mkdirhier(*path);
  if (s != MSTRO_OK) {
    ERR("Failed to setup a directory for file transport\n");
    return s;
  }

  /* this time make the file path */
  WITH_CDO_ID_STR(idstr, &src_cdo->gid, {
    size_t n = snprintf(*path, l, "%s%" PRIappid "/%s/%" PRIlocalid,
                        g_mstro_transport_gfs_dir,
                        g_pool_app_id,
                        idstr,
                        src_cdo->gid.local_id);
    if(n>=l) {
      ERR("GFS path truncated for CDO %s\n", idstr);
      free(*path);
      return MSTRO_FAIL;
    }
  });
  
  DEBUG("Constructed GFS transport path %s\n", *path);
  return MSTRO_OK;
}

static inline
mstro_status
mstro_pc__calculate_data_size_and_offsets(
    mstro_cdo src_cdo,
    const Mstro__Pool__InitiateTransfer* init, 
    int64_t *data_length,
    int64_t *src_offset,
    int64_t *dst_offset)
{
  mstro_status status = MSTRO_OK;
  bool is_distributed = init->distributed_cdo;
  /* find out if the cdo (src or dst) is distributed */  
  Mstro__Pool__Attributes *dst_attributes = init->dst_attributes;
  
  
  if(is_distributed) {
    mmbLayout *src_layout = NULL;
    mmbLayout *dst_layout = NULL;
    mmbError stat = MMB_OK;
    /* read source and dst layouts */
    status = mstro_attribute_pool_find_dist_layout(src_cdo->attributes_msg, &src_layout);
    status = mstro_attribute_pool_find_dist_layout(dst_attributes, &dst_layout); 
    assert(status == MSTRO_OK);

    if(src_layout == NULL){
      /* cook a default layout for src from dst */
      stat = mmb_layout_dist_create_default_layout(dst_layout, &src_layout);
    }
    else if(dst_layout == NULL) {
      /* cook a default layout for dst from src */
      stat = mmb_layout_dist_create_default_layout(src_layout, &dst_layout);
    }
    assert(stat == MMB_OK);
    /* find the mapping between src and dst layouts */
    mmbLayoutIntersection *out_li = NULL;
    mmbError mmb_s;
    mmb_s = mmb_layout_compute_intersection(src_layout, dst_layout, &out_li);
    assert(MMB_OK == mmb_s);
    DEBUG("Found the intersection between required layout and my layout \n");
    if (out_li) {
      /*calculate my index at the intersection object*/
      size_t index; /*dst_layout_index * n_src_pieces + src_layout_index*/
      index = dst_layout->index * out_li->n_src_pieces + src_layout->index;
      if(out_li->overlap[index].length <= 0) {
        ERR("Invalid intersection length of %zu \n", out_li->overlap[index].length);
        status =  MSTRO_FAIL;
      }
      else {
        /*read the length, number of segments, and src and dst offsets from the intersection object*/
        *dst_offset = out_li->overlap[index].dst_offset * src_layout->element.size_bytes; /* offset in bytes */
        *src_offset = out_li->overlap[index].src_offset * src_layout->element.size_bytes; /* offset in bytes */
        /* data_length is equal to the length of intersection * size of element in bytes */
        DEBUG("Intersection length %zu and size of element is %zu \n", out_li->overlap[index].length, src_layout->element.size_bytes);
        *data_length = out_li->overlap[index].length * src_layout->element.size_bytes ; /* data size in bytes */
      }
      
      mmb_layout_destroy_mmbLayoutIntersection(out_li);
    }
    else {
      ERR("No intersection between dst layout and src layout ... incorrect ticket\n");
      status = MSTRO_FAIL;
    }

    /* free up allocated layout objects*/
    if(src_layout) {
      mmb_layout_destroy(src_layout);
    }
    if(dst_layout) {
      mmb_layout_destroy(dst_layout);
    }

  } /* not distributed */
  else {
    const void *size=NULL;
    enum mstro_cdo_attr_value_type vt;
    status = mstro_attribute_dict_get(src_cdo->attributes,
                               MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                               &vt, &size, NULL, false);
    if(status==MSTRO_NOENT && vt==MSTRO_CDO_ATTR_VALUE_INVALID) {
      ERR("CDO has mamba-array but no local-size\n");
      return MSTRO_FAIL;
    }
    if(status!=MSTRO_OK) {
      ERR("Failed to retrieve local-size on CDO\n");
      return MSTRO_FAIL;
    }

    *data_length = *(int64_t*)size;
    if(*data_length==-1) {
      DEBUG("Source CDO empty, doing NULL transfer\n");
      *data_length = 0;
    }

    *dst_offset = 0; /* not distributed  ... write from the begining */
    *src_offset = 0; /* not distributed ... read from the begining */
  }

  return status;

}


mstro_status
mstro_pc__init_transfer_send_ticket(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  const Mstro__Pool__InitiateTransfer* init = op->msg->initiate_transfer;

   /* Prepare the ticket */
  Mstro__Pool__CDOID srcc = MSTRO__POOL__CDOID__INIT;
  srcc.qw0 = init->srccdoid->qw0;
  srcc.qw1 = init->srccdoid->qw1;
  srcc.local_id = init->srccdoid->local_id;
  Mstro__Pool__CDOID dstc = MSTRO__POOL__CDOID__INIT;
  dstc.qw0 = init->dstcdoid->qw0;
  dstc.qw1 = init->dstcdoid->qw1;
  dstc.local_id = init->dstcdoid->local_id;

  Mstro__Pool__TransferTicket ticket = MSTRO__POOL__TRANSFER_TICKET__INIT;
  ticket.srccdoid = &srcc;
  ticket.dstcdoid = &dstc;
  Mstro__Pool__Appid myid = MSTRO__POOL__APPID__INIT;
  myid.id = g_pool_app_id;
  ticket.srcid = &myid;

  
  int64_t realsize, src_offset, dst_offset;
                                
  /* Calculate the length of the data, src and dst offsets and number of segments */
  status = mstro_pc__calculate_data_size_and_offsets(
      op->pc_transport.target_cdo, init, &realsize, &src_offset, &dst_offset);
  if (status != MSTRO_OK) {
    return status;
  }
  
  /* fill ticket with the gathered information */
  ticket.src_offset = src_offset;
  ticket.dst_offset = dst_offset;
  ticket.n_segments = init->n_segments;
  ticket.distributed_cdo = init->distributed_cdo;
  ticket.data_size = realsize;

  if (op->pc_transport.methods->supported[0] == MSTRO__POOL__TRANSPORT_KIND__MIO
      && (!g_mio_available || (realsize % getpagesize()) != 0 )
      ){
    WARN("Not issuing a ticket with MIO. Either not available or CDO size (%zu)"
         " is not a multiple of the page size (%d)."
         " Will use maestro-core default transport\n",
         realsize, getpagesize());
    op->pc_transport.methods->supported[0] = MSTRO__POOL__TRANSPORT_KIND__OFI;
  }

  status = mstro_pc__select_transfer_method(op->pc_transport.target_cdo, op->pc_transport.methods,
                                            &ticket);
  if(status!=MSTRO_OK) {
    ERR("Failed to select transport method\n");
    return MSTRO_FAIL;
  }

  Mstro__Pool__TransferTicketGFS gfs    = MSTRO__POOL__TRANSFER_TICKET_GFS__INIT;
  Mstro__Pool__TransferTicketMIO mio    = MSTRO__POOL__TRANSFER_TICKET_MIO__INIT;
  Mstro__Pool__TransferTicketOFI ofi    = MSTRO__POOL__TRANSFER_TICKET_OFI__INIT;
  Mstro__Pool__TransferTicketInline inl = MSTRO__POOL__TRANSFER_TICKET_INLINE__INIT;
  Mstro__Pool__RDMAHandle rh = MSTRO__POOL__RDMAHANDLE__INIT;
  Mstro__Pool__Appid appid = MSTRO__POOL__APPID__INIT;

  /* protobuf objects are all on stack so the code below is not split
   * into helper functions but all inline */
  switch(ticket.ticket_case) {
    case MSTRO__POOL__TRANSFER_TICKET__TICKET_GFS:
      NOISE("TICKET CASE GFS\n");
      status= mstro_pc__construct_gfs_path_for_cdo(op->pc_transport.target_cdo, &gfs.path);
      if(status!=MSTRO_OK) {
        ERR("Failed to construct GFS path for SRC-CDO: %d (%s)\n",
            status, mstro_status_description(status));
        return status;
      }
      ticket.gfs = &gfs;
      gfs.keep_file = 1; // don't arbitrarily rm the transport file on dst
      break;
      
    case MSTRO__POOL__TRANSFER_TICKET__TICKET_MIO:
#ifdef HAVE_MIO
      struct mstro_cdo_id* semid;
      semid = malloc(sizeof(struct mio_obj_id));
      if (semid == NULL) {
        ERR("No more memory.\n");
        return MSTRO_NOMEM;
      }
      struct mstro_cdo_id* objid;
      objid = malloc(sizeof(struct mio_obj_id));
      if (objid == NULL) {
        ERR("No more memory.\n");
        return MSTRO_NOMEM;
      }
      char* semname = NULL;
#define MSTRO_MIO_SEM_STR_MAXLEN 	128
#warning semname should not me constructed here with MIO-specific length
      mstro_str_random(&semname, MSTRO_MIO_SEM_STR_MAXLEN);
      if (semname == NULL) {
        ERR("Couldn't prepare an id for semaphore obj\n");
        return MSTRO_FAIL;
      }
      status = mstro_cdo_id_from_name(semname, semid); /* So we do collision
                                                          detection in only
                                                          one place */
      if (status != MSTRO_OK) {
        ERR("Couldn't make an id from name for semaphore obj\n");
        return MSTRO_FAIL;
      }

      WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)semid,
                       DEBUG("Semaphore has ID: %s\n",
                            idstr););
      WITH_CDO_ID_STR(idstr, &(op->init.target_cdo->gid),
                      DEBUG("(CDO associated has ID: %s)\n",
                           idstr););

      assert(sizeof(struct mstro_cdo_id) == 2*sizeof(uint64_t));
      assert(sizeof(struct mstro_cdo_id) == sizeof(struct mio_obj_id));

      mio.semid.len = sizeof(struct mstro_cdo_id);
      mio.semid.data = (uint8_t*)semid;
      mio.objid.len = sizeof(struct mstro_cdo_id);
      objid->qw[0] = op->init.target_cdo->gid.qw[0];
      objid->qw[1] = op->init.target_cdo->gid.qw[1];
      objid->local_id = op->init.target_cdo->gid->local_id;
      mio.objid.data = (uint8_t*)objid;

      mio.keep_obj = 0;

      ticket.mio = &mio;
#else
      ERR("Request to issue an MIO ticket, but built without MIO support\n");
      return MSTRO_FAIL;
#endif
      break;
      
    case MSTRO__POOL__TRANSFER_TICKET__TICKET_OFI: {
      NOISE("TICKET CASE RDMA\n");
      appid.id = op->pc_transport.target_appid;
      ofi.dstid = &appid;
      struct mstro_pm_app_registry_entry *e;
      mstro_status status = mstro_pm_app_lookup(appid.id, &e);
      assert(e!=NULL); // we befriended dst app earlier, it should be in the registry
      ticket.src_serialized_endpoint = e->ep->serialized;
      ofi.h = &rh; /* rest to be filled in transport_execute */
      ticket.want_completion = 1; /* so we can refcount-- */
      ticket.ofi = &ofi;
      break;
    }
      
    case MSTRO__POOL__TRANSFER_TICKET__TICKET_INLINE: {
      NOISE("TICKET CASE INLINE\n");
      appid.id = init->dst_appid->id;
      ofi.dstid = &appid;
      struct mstro_pm_app_registry_entry *e;
      mstro_status status = mstro_pm_app_lookup(appid.id, &e);
      assert(e!=NULL); // we befriended dst app earlier, it should be in the registry
      ticket.src_serialized_endpoint = e->ep->serialized;
      ticket.want_completion = false; 

      /* actual data filled in in transport_execute (source side) below */
      inl.data.data = NULL;
      inl.data.len = 0;
      
      ticket.inline_ = &inl;
      
      break;
    }
    default:
      ERR("Unsupported ticket kind %d\n", ticket.ticket_case);
      return MSTRO_UNIMPL;
  }

  if(op->pc_transport.target_cdo->attributes_msg==NULL) {
    ERR("source CDO has no attributes message data -- should not happen\n");
    return MSTRO_FAIL;
  }
  ticket.attributes = op->pc_transport.target_cdo->attributes_msg;
  assert(op->pc_transport.target_appid == init->dst_appid->id);
  INFO("Issued ticket to app %" PRIu64 " for CDO %s, and starting execute process\n", op->pc_transport.target_appid, op->pc_transport.target_cdo->name);

  NOISE("TransferTicket using path %s\n", ticket.gfs->path);
  NOISE("TransferTicket cdo size %" PRIi64 "\n", ticket.data_size);

  /* Execute transport (non-blocking) */
  status = mstro_transport_execute(op->pc_transport.target_cdo, &ticket, 1); //FIXME 1 is a dummy value ... value should not be used in this path
  if(MSTRO_OK != status) {
    ERR("Failure in transport execute for CDO %s\n",
        op->pc_transport.target_cdo->name);
    return MSTRO_FAIL;
  }

  /* do some clean-up if applicable */
  if (ticket.ticket_case == MSTRO__POOL__TRANSFER_TICKET__TICKET_GFS) {
    if (init->cp == 0) { // That means mv
      // TODO Detach raw pointer from CDO handle for Maestro resource
      // management while the CDO stays OFFERED by the new location? Or
      // is this part of a resource management protocol that originates
      // from pool manager and utilizes existing infrastructure for
      // movement?
      DEBUG("Unimplemented ticket case cp==0\n");
	}
  }

  /* Send ticket via pmp_send_nowait() */
  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;

  if (! (MSTRO_OK == mstro_pmp_package(&msg, (ProtobufCMessage*)&ticket))) {
    ERR("Failed to package %s into a pool manager message\n",
        ticket.base.descriptor->name);
    return MSTRO_FAIL;
  }

  status = mstro_pmp_send_nowait(op->pc_transport.target_appid, &msg);
  if(status!=MSTRO_OK) {
    ERR("Cannot send ticket to %zu: %d (%s)\n",
        op->pc_transport.target_appid, status, mstro_status_description(status));
    return MSTRO_FAIL;
  }

  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_TICKETS_OUT, 1);

  /* cleanup ticket content that is not stack-allocated */
  switch(ticket.ticket_case) {
    case MSTRO__POOL__TRANSFER_TICKET__TICKET_OFI: {
      free(ticket.ofi->h->mr_key.data);
      ticket.ofi->h->mr_key.len = 0;
      break;
    }
    default:
      break;
  }      
  
  return MSTRO_OK;
}

mstro_status
mstro_pc__reg_friend_app(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;

  if(!mstro_request_test(op->pc_transport.request)) {
    DEBUG("Async request still not done, try again later\n");
    status = MSTRO_WOULDBLOCK;
    return status;
  } else {
    /* execute wait to clean up handle */
    status = mstro_request_wait(op->pc_transport.request);
    if(status!=MSTRO_OK) {
      ERR("Async endpoint selection failed, could not find suitable endpoint to talk to app %" PRIappid ": %d\n",
          op->pc_transport.target_appid, status);
      return status;
    }
  }

  assert(op->pc_transport.target_ep!=NULL);

  if(op->pc_transport.methods==NULL)
  {
      /**FIXME In a transfer ticket we do not know the transfer methods, so fake one*/
      Mstro__Pool__TransportKind ofi_method[1] = { MSTRO__POOL__TRANSPORT_KIND__OFI };
      Mstro__Pool__TransportMethods m = MSTRO__POOL__TRANSPORT_METHODS__INIT;
      m.n_supported = 1;
      m.supported = &ofi_method[0];
      size_t len = mstro__pool__transport_methods__get_packed_size(&m);
      uint8_t *buf = malloc(sizeof(uint8_t)*len);
      mstro__pool__transport_methods__pack(&m,buf);
      Mstro__Pool__TransportMethods *methods = mstro__pool__transport_methods__unpack(NULL, len, buf);
      op->pc_transport.methods = methods;
      free(buf);
  }
  if(op->kind == MSTRO_OP_PC_INIT_TRANSFER)
  {
      //we should consume the transport methods in msg here
      op->msg->initiate_transfer->methods = NULL;
  } 
  /* pass into registry */
  status = mstro_pc_app_register(op->pc_transport.target_ep,
                                 op->pc_transport.target_addr,
                                 strdup(op->pc_transport.target_serialized_endpoint),
                                 op->pc_transport.methods,
                                 op->pc_transport.target_appid,
                                 NULL);
  if(status!=MSTRO_OK) {
    ERR("Failed to register peer app %zu: %d\n", op->pc_transport.target_appid);
  } else {
    DEBUG("Registered peer app %zu\n", op->pc_transport.target_appid);
  }

  mstro__app_info__free_unpacked(op->pc_transport.dst_epd, NULL);

  return status;
}

mstro_status
mstro_pc__app_befriend(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  struct mstro_pm_app_registry_entry *e;
  mstro_app_id appid = op->pc_transport.target_appid; 
  status = mstro_pm_app_lookup(appid, &e);
    /**found an entry ... good */
    if(status == MSTRO_WOULDBLOCK) {
			  DEBUG("app entry is already being read ... wait until it completes\n");
			  status = MSTRO_WOULDBLOCK; /* we need to wait until the read is complete */
			  return status;
    } else if ((e!=NULL)&& (status == MSTRO_OK)) 
    {
			  DEBUG("Found app %zu in local registry, good\n", appid);
        op->step++; /**jump app reg step to go directly to writing and sending the ticket */
			  return MSTRO_OK; /**we can continue execution */
    }
  else {
    /**Did not find an entry, will submit read*/
    DEBUG("Unknown app %zu, let's make friends\n", appid);
    /* mark that this app entry is being read now for others */
		status = mstro_pc_app_register_pending(appid);
    /*check status */
    if(status == MSTRO_FAIL)
    {
      /* we could not put a pending entry, 
       * probably someone else was faster than us
       * We should try this operation step again
       * returning wouldblock so that op engine would retry later*/
      return MSTRO_WOULDBLOCK;
    }

    /**Submit read */
    /* we need to be careful to not pass in any stack-local refs */
    Mstro__AppInfo *dst_epd=NULL;
    status=mstro_appinfo_deserialize(  op->pc_transport.target_serialized_endpoint, 
		                     &(op->pc_transport.dst_epd));

    /**get pc thread numa*/
    int numa_node = 0;
    #ifdef HAVE_NUMA
    /**update numa node*/
    numa_node = numa_node_of_cpu(mstro_numa_get_cpu());
    #endif
    
    /**assume that g_pc_ofi_team is already initialised*/
    assert(g_pc_ofi_team != NULL);
    status=mstro_ofi__select_endpoint(op->pc_transport.dst_epd,
                                    &(op->pc_transport.target_ep),
                                    &(op->pc_transport.target_addr), 
                                    mstro_ofi_thread_get_endpointset(g_pc_ofi_team, numa_node),
                                    &(op->pc_transport.request));

    if(status!=MSTRO_OK) {
      ERR("Failed to initiate partner lookup: %d\n", status);
    } else {
      /* return MSTRO_OK to advance the op step and wait for read to complete in the next step */
      status = MSTRO_OK;
    }
  }

  return status;
}
mstro_status
mstro_pc__prepare_init_transfer(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  const Mstro__Pool__InitiateTransfer* init = op->msg->initiate_transfer;
  assert( init!= NULL);

  if (init->dst_serialized_endpoint== NULL) {
    ERR("Cannot initiate transfer without dst app endpoint\n");
    return MSTRO_INVMSG;
  }

  NOISE("Initiating a transfer to endpoint %s\n",
        init->dst_serialized_endpoint);

  struct mstro_cdo_id srccdoid = { .qw[0] = init->srccdoid->qw0,
                                   .qw[1] = init->srccdoid->qw1,
                                   .local_id = init->srccdoid->local_id };
  struct mstro_cdo_id dstcdoid = { .qw[0] = init->dstcdoid->qw0,
                                   .qw[1] = init->dstcdoid->qw1,
                                   .local_id = init->dstcdoid->local_id };
  WITH_CDO_ID_STR(here_idstr, &srccdoid, {
      WITH_CDO_ID_STR(there_idstr, &dstcdoid, {
          NOISE("ticket for CDO gid %s (here), gid %s (there)\n",
               here_idstr, there_idstr);});});
  if (srccdoid.local_id != MSTRO_CDO_LOCAL_ID_NONE) {  
    /* We trust that the PM gave us the correct local-id */
    if (!(MSTRO_OK == mstro_pool__find_cdo_with_local_id(&srccdoid,
                                                         &(op->pc_transport.target_cdo)))) {
      WITH_CDO_ID_STR(idstr, &srccdoid,
                    ERR("Cannot transfer CDO gid %s because not in local pool\n",
                        idstr););
      return MSTRO_FAIL;
     }
  } else { /* we do not have a valid local-id ... looking for an appropriate cdo */
    if (!(MSTRO_OK == mstro_pool__find_source_cdo(&srccdoid,
                                                  init->dst_attributes,
                                                  &(op->pc_transport.target_cdo)))) {
      WITH_CDO_ID_STR(idstr, &srccdoid,
                      ERR("Cannot transfer CDO gid %s because not in local pool\n",
                          idstr););
      return MSTRO_FAIL;
    }
  }

  /* we may see the ticket before OFFER ack. In that case
   * we do an implicit ack by setting state to OFFERED
   * (PM would not send ticket if it has not handled the OFFER) */
    if(mstro_cdo_state_get(op->pc_transport.target_cdo)==MSTRO_CDO_STATE_OFFERED_LOCALLY) {
      WITH_CDO_ID_STR(idstr, &srccdoid, {
          DEBUG("Doing implicit OFFER-ACK for CDO %s at transport ticket creation (src side) time\n",
                idstr);
        });
      mstro_cdo_state state_flags = (mstro_cdo_state_get(op->pc_transport.target_cdo)
                                     & MSTRO_CDO_STATE_FLAGS);
      status = mstro_cdo_state_set(op->pc_transport.target_cdo,
                          MSTRO_CDO_STATE_OFFERED | state_flags);
      if (status == MSTRO_OK)
      {
          NOISE("Updated cdo state successfully\n");
      }
      else if ( (status == MSTRO_INVARG) && (mstro_cdo_state_get(op->pc_transport.target_cdo) & MSTRO_CDO_STATE_OFFERED )) 
      {
        DEBUG("CDO is already offered, another thread already processed the offer ack \n");
      } 
      else
      {
        ERR("Failed to update cdo state\n");
        return status;
      }

    }

  if(init->dst_attributes==NULL) {
    WARN("No attributes on CDO\n");
  } else {
    if(init->dst_attributes->val_case==MSTRO__POOL__ATTRIBUTES__VAL_KV_MAP) {
      DEBUG("%zu attributes in kv-map\n", init->dst_attributes->kv_map->n_map);
    } else {
      WARN("non-kv attributes\n");
    }
  }

  DEBUG("Initiating transfer from src app %" PRIappid " (me) to dst app %" PRIappid " of CDO %s\n",
        g_pool_app_id, op->pc_transport.target_appid, op->pc_transport.target_cdo->name);

  if(g_pool_app_id==op->pc_transport.target_appid) {
    WARN("FIXME: We will be talking to ourselves via transport, should use a shortcut\n");
  }


  return MSTRO_OK;
}


mstro_status
mstro_pc__bye(mstro_pool_operation op)
{
  const Mstro__Pool__Bye *bye = op->msg->bye;

  assert(bye!=NULL);
  DEBUG("Bye message on app %zu incoming\n", g_pool_app_id);

  /* reconfigure our app to be non-pool */
  atomic_store(&g_mstro_pm_attached, false);
  g_pool_app_id = MSTRO_APP_ID_INVALID;
  g_pool_appid.id = MSTRO_APP_ID_INVALID;
  g_pool_apptoken.appid = NULL;
  return MSTRO_OK;
}

mstro_status
mstro_pc__transport_send_completion(mstro_app_id srcappid,
                                    const struct mstro_cdo_id *srccdoid,
                                    const struct mstro_cdo_id *dstcdoid,
                                    int src_wants_completion)
{
  mstro_status status;

  /* update state, then tell manager */
  Mstro__Pool__TransferCompleted completion = MSTRO__POOL__TRANSFER_COMPLETED__INIT;
  Mstro__Pool__Appid srcid = MSTRO__POOL__APPID__INIT;
  srcid.id = srcappid;
  completion.srcid = &srcid;

  Mstro__Pool__Appid dstid = MSTRO__POOL__APPID__INIT;
  dstid.id = g_pool_app_id;
  completion.dstid = &dstid;

  Mstro__Pool__CDOID srccdo = MSTRO__POOL__CDOID__INIT;
  srccdo.qw0      = srccdoid->qw[0];
  srccdo.qw1      = srccdoid->qw[1];
  srccdo.local_id = srccdoid->local_id;

  Mstro__Pool__CDOID dstcdo = MSTRO__POOL__CDOID__INIT;
  dstcdo.qw0      = dstcdoid->qw[0];
  dstcdo.qw1      = dstcdoid->qw[1];
  dstcdo.local_id = dstcdoid->local_id;

  completion.srccdoid = &srccdo;
  completion.dstcdoid = &dstcdo;

  Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
  status = mstro_pmp_package(&msg, (ProtobufCMessage*)&completion);
  if(MSTRO_OK != status) {
    ERR("Failed to package %s into a pool manager message\n",
        completion.base.descriptor->name);
    return MSTRO_FAIL;
  }

  status = mstro_pmp_send_nowait(MSTRO_APP_ID_MANAGER, &msg);
  if(status!=MSTRO_OK) {
    ERR("Cannot send completion to pool manager: %d (%s)\n",
        status, mstro_status_description(status));
    return MSTRO_FAIL;
  }
  WITH_CDO_ID_STR(idstr, dstcdoid, {
      DEBUG("Sent completion of transfer for %s to PM\n", idstr);});

  if (src_wants_completion) {
    status = mstro_pmp_send_nowait(srcappid, &msg);
    if(status!=MSTRO_OK) {
      ERR("Cannot send completion to source %d: %d (%s)\n",
          srcappid, mstro_status_description(status));
      return MSTRO_FAIL;
    }
    WITH_CDO_ID_STR(idstr, srccdoid, {
        DEBUG("Sent completion of transfer for %s to app %" PRIappid "\n",
              idstr, srcappid);});
  }

  return MSTRO_OK;
}
mstro_status
mstro_pc__prepare_transfer(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  Mstro__Pool__TransferTicket* ticket = op->msg->transfer_ticket;

  if(ticket==NULL || ticket->srccdoid==NULL || ticket->dstcdoid==NULL) {
    ERR("Invalid transfer ticket: %p/%p/%p\n",
        ticket,
        ticket ? ticket->srccdoid : (void*)0xdeadbeef,
        ticket ? ticket->dstcdoid : (void*)0xdeadbeef);
    return MSTRO_INVMSG;
  }
  

  struct mstro_cdo_id cdoid = { .qw[0] = ticket->dstcdoid->qw0,
                                .qw[1] = ticket->dstcdoid->qw1,
                                .local_id = ticket->dstcdoid->local_id };

  WITH_CDO_ID_STR(idstr, &cdoid,
                  DEBUG("Incoming ticket for CDO gid %s, ticket kind %s\n",
                       idstr,
                       (protobuf_c_enum_descriptor_get_value(
                           &mstro__pool__transport_kind__descriptor,
                           ticket->method))
                       ->name););
  
  status = mstro_pool__find_cdo_with_local_id(&cdoid, &(op->pc_transport.target_cdo));

  if (status != MSTRO_OK) {
    /* we did not find the cdo by local id */
    status = mstro_pool__find_sink_cdo(&cdoid, ticket->attributes,
                                     &(op->pc_transport.target_cdo));
    if(MSTRO_OK!=status && MSTRO_NOENT!=status) {
    WITH_CDO_ID_STR(idstr, &cdoid,
                    ERR("Cannot transfer CDO gid %s because not in local pool\n",
                        idstr););
    return MSTRO_FAIL;
    }
  }
  
  if(MSTRO_NOENT==status) {
    WITH_CDO_ID_STR(idstr, &cdoid, {
        WARN("Incoming ticket for CDO %s, but CDO not present in local pool, skipping ticket\n",
             idstr);});
    if(ticket->force_offer) {
      WARN("Ticket has force-offer set, but this is unimplemented\n");
      /* FIXME: create a fresh CDO, use that CDO for incoming transport, then perform OFFER */
    }
  } else {
    WITH_CDO_ID_STR(idstr, &(op->pc_transport.target_cdo->gid), {
        DEBUG("Initiating incoming transfer to app %" PRIappid " (me) from %zu for CDO %s (%s)\n",
              g_pool_app_id, ticket->srcid->id, op->pc_transport.target_cdo->name, idstr);});

    if (ticket->ticket_case != MSTRO__POOL__TRANSFER_TICKET__TICKET_OFI) {
      op->step = 3; /*skip app befriend and app register and go directly to executing transport */
    }
  }
  return status;
}

mstro_status
mstro_pc__transfer_execute(mstro_pool_operation op)
{
  mstro_status status = MSTRO_UNIMPL;
  Mstro__Pool__TransferTicket* ticket = op->msg->transfer_ticket;
  /** check and update the number of segments on the dst cdo 
    * if n_segments of the cdo is zero, then it means this is the first ticket 
    * and we should update the n_segments value from the ticket
    * Otherwise, then we are not the first ticket, and we should leave it alone
    * with the completion of every transport the value of n_segments is decremented
    * until it reaches zero, marking that all required data is filled. 
  */
  int64_t expected = 0;
  atomic_compare_exchange_strong(&(op->pc_transport.target_cdo->n_segments), &expected, ticket->n_segments);
    /* Execute transport (non-blocking) */
    status = mstro_transport_execute(op->pc_transport.target_cdo, ticket, expected); //if expected ==0, then we are the first ticket in this transfer
  if(MSTRO_OK != status) {
    ERR("Failure in transport execute for CDO %s\n", op->pc_transport.target_cdo->name);
    return MSTRO_FAIL;
  }

  /* Each (GFS and MIO TODO) method waits for the transport to complete before sending completion themselves */
  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_TICKETS_IN, 1);
  return status;
}


mstro_status
mstro_pc__transfer_completed(mstro_pool_operation op)
{
  
  Mstro__Pool__TransferCompleted *completion = op->msg->transfer_completed;
  mstro_app_id app_id = op->msg->token->appid->id;
  assert(completion!=NULL); assert(app_id!=MSTRO_APP_ID_INVALID);
  mstro_status s = MSTRO_UNIMPL;

  DEBUG("Received transfer completion message from app %" PRIappid "\n", app_id);

  /* if this is an OFI ticket: call mstro_transport_rdma_src_execute_bh */
  struct mstro_transport_mreg_table_entry* regentry = NULL;

  s = mstro_transport_rdma_src_execute_bh(completion);
  if(s==MSTRO_NOENT) {
    mstro_cdo cdo;
    struct mstro_cdo_id cdoid = { .qw[0] = completion->srccdoid->qw0,
                                  .qw[1] = completion->srccdoid->qw1,
                                  .local_id = completion->srccdoid->local_id };

    mstro_cdo__find_cdo(&cdoid, &cdo);
    if(cdo==NULL) {
      WITH_CDO_ID_STR(idstr, &cdoid, {
          ERR("Cannot find CDO %s for which completion was signalled\n", idstr);
        });
      goto DONE;
    }
    const void *size=NULL;
    enum mstro_cdo_attr_value_type vt;
    s = mstro_attribute_dict_get(cdo->attributes,
                                 MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                 &vt, &size, NULL, false);
    if(s==MSTRO_NOENT && vt==MSTRO_CDO_ATTR_VALUE_INVALID) {
      ERR("Source CDO has no local-size\n");
      goto DONE;
    }
    int64_t realsize = *(int64_t*)size;
    if(realsize==-1 || realsize==0) {
      s=MSTRO_OK; // 0-size CDO or type-0 CDO
      DEBUG("Source CDO empty, doing NULL transfer\n");
      s=MSTRO_OK;
    } else {
      WITH_CDO_ID_STR(idstr, &cdoid, {
          ERR("Nonempty CDO had no memory registration to clean up\n");
        });
    }
  }
  /* do not free the message on the PC -- the message handler that called us will do that */
DONE:
  return s;
}


mstro_status
mstro_pc__event(mstro_pool_operation op)
{
  const Mstro__Pool__Event *ev = op->msg->event;

  if(ev==NULL) {
    ERR("Invalid event\n");
    return MSTRO_INVMSG;
  }
  if(ev->subscription_handle==NULL) {
    ERR("Missing subscription handle\n");
    return MSTRO_INVMSG;
  }

  DEBUG("Pool event for sid %" PRIu64 ", kind %d (payload case %d), ctime %" PRIu64 "\n",
        ev->subscription_handle->id, ev->kind, ev->payload_case,
        ((uint64_t)ev->ctime->sec * NSEC_PER_SEC) + (uint64_t) ev->ctime->nsec);

  mstro_stats_add_counter(MSTRO_STATS_CAT_PROTOCOL, MSTRO_STATS_L_PC_NUM_POOL_EVENTS, 1);
  //uint64_t sid = ev->subscription_handle->id;

  return mstro_pool_event_consume(ev);
}

/**Create mstro_pool_operations from incoming msgs
 * pushes the created operations to the *queue*
*/
mstro_status
mstro_pc__op_maker(Mstro__Pool__MstroMsg *msg)
{
    mstro_status status=MSTRO_OK;
    mstro_pool_operation op;

    /*Create an operation*/
    status = mstro_pool_op__allocate(&op);
    if (status != MSTRO_OK)
    {
        return status;
    }

    /**handle msg */
    switch(msg->msg_case) {
    /* 'good' messages: */
    case MSTRO__POOL__MSTRO_MSG__MSG_INITIATE_TRANSFER:
        /*fill initiate transfer specific parts */
        op->kind = MSTRO_OP_PC_INIT_TRANSFER;
        op->handler_steps = mstro_pc__init_transfer_steps;
        op->pc_transport.target_addr = 0;
        op->pc_transport.target_ep = NULL;
        op->pc_transport.request = NULL;
        op->pc_transport.target_appid = msg->initiate_transfer->dst_appid->id;
        op->pc_transport.methods = msg->initiate_transfer->methods;
        op->pc_transport.target_serialized_endpoint = msg->initiate_transfer->dst_serialized_endpoint;
        break;
    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_TICKET:
      /*fill transfer specific parts*/
      op->kind = MSTRO_OP_PC_TRANSFER;
      op->handler_steps = mstro_pc__transfer_steps;
      op->pc_transport.target_addr = 0;
      op->pc_transport.target_ep = NULL;
      op->pc_transport.request = NULL;
      op->pc_transport.target_appid = msg->transfer_ticket->srcid->id;
      op->pc_transport.methods = NULL;
      op->pc_transport.target_serialized_endpoint = msg->transfer_ticket->src_serialized_endpoint;
      break;
    case MSTRO__POOL__MSTRO_MSG__MSG_DECLARE_ACK:
      op->kind = MSTRO_OP_PC_DECLARE_ACK;
      op->handler_steps = mstro_pc__declare_ack_steps;   
      break;
    case MSTRO__POOL__MSTRO_MSG__MSG_ACK:
      op->kind = MSTRO_OP_PC_POOL_ACK;
      op->handler_steps = mstro_pc__poolop_ack_steps;
      break;                 
    case MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE_ACK:
      op->kind = MSTRO_OP_PC_SUBSCRIBE_ACK;
      op->handler_steps = mstro_pc__subscribe_ack_steps;
      break;           
    case MSTRO__POOL__MSTRO_MSG__MSG_BYE:
      op->kind = MSTRO_OP_PC_BYE;
      op->handler_steps = mstro_pc__bye_steps;
      break;                      
    case MSTRO__POOL__MSTRO_MSG__MSG_EVENT:
      op->kind = MSTRO_OP_PC_EVENT;
      op->handler_steps = mstro_pc__event_steps;
      break;                    
    case MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE_REPLY:
      op->kind = MSTRO_OP_PC_RESOLVE_REPLY;
      op->handler_steps = mstro_pc__resolve_reply_steps;
      break;             
    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_COMPLETED:
      op->kind = MSTRO_OP_PC_TRANSFER_COMPLETED;
      op->handler_steps = mstro_pc__transfer_completed_steps;
      break;
    default:
        WARN("%s message received, dropping it, not even sending ACK\n",
              msg->base.descriptor->name);
        /*cleanup*/
        mstro_pool_op__free(op);
        mstro_pm__msg_free(msg);
        status = MSTRO_UNIMPL;
        break;
    }

    if (status == MSTRO_OK)
    {
        /* continue to fill general operation fields from msg and push to queue */
        op->step = MSTRO_OP_ST_ANNOUNCE; // first step is to announce
        op->msg = msg;
        op->appid = msg->token->appid->id;
        DEBUG("Filled operation structure from msg\n");
        status = erl_thread_team_enqueue(g_pool_operations_team, op);
        assert(status == MSTRO_OK);
    }
    
    return status;
}

/** unpack envelope and handle PC-side message*/
mstro_status
mstro_pc_handle_msg(const struct mstro_msg_envelope *envelope)
{  
  mstro_status status=MSTRO_OK;

  NOISE("PC Handling incoming message size %" PRIu64 "\n",
       envelope->payload_size);

  Mstro__Pool__MstroMsg *msg
      = mstro__pool__mstro_msg__unpack(NULL,
                                       envelope->payload_size,
                                       envelope->data);

  if(msg==NULL) {
    ERR("Failed to unpack the message\n");
    status = MSTRO_INVMSG;
    goto BAILOUT;
  } else if(msg->msg_case==MSTRO__POOL__MSTRO_MSG__MSG__NOT_SET) {
    ERR("Invalid decoded message: no valid message verb found\n");
    status = MSTRO_INVMSG;
    goto BAILOUT;
  } else {
    DEBUG("... unpacked msg: type %s (%s), vsm %s\n",
          msg->base.descriptor->name,
          (msg->base.descriptor == &mstro__pool__mstro_msg__descriptor
           ? msg->declare->base.descriptor->name : "no subtype"),
          (msg->opts && msg->opts->vsm_data ? "yes" : "no"));
  }

  if(msg->base.descriptor != &mstro__pool__mstro_msg__descriptor) {
    ERR("Cannot handle messages other than MstroMsg messages\n");
    status = MSTRO_INVMSG;
    goto BAILOUT_FREE;
  }

  if(msg->opts && msg->opts->vsm_data!=NULL) {
    mstro_stats_add_counter(MSTRO_STATS_CAT_OFI, MSTRO_STATS_L_PC_NUM_VSM_IN, 1);
    ERR("VSM, Unimplemented\n");
    status=MSTRO_UNIMPL;
    goto BAILOUT_FREE;
  }

  /* early check for NACK */
  if(msg->msg_case == MSTRO__POOL__MSTRO_MSG__MSG_WELCOME
     && msg->welcome->nack) {
    INFO("WELCOME-NACK message received\n");
    atomic_store(&g_mstro_pm_rejected,true);
    status = MSTRO_NOT_TWICE;
    goto BAILOUT_FREE;
  }

  if(!msg->token) {
    ERR("Incoming message without token\n");
    status=MSTRO_INVMSG;
    goto BAILOUT_FREE;
  }
  if(!msg->token->appid) {
    ERR("Incoming message without App ID\n");
    status=MSTRO_INVMSG;
    goto BAILOUT_FREE;
  }

  if(msg->token->appid->id==MSTRO_APP_ID_INVALID) {
      ERR("Invalid app id %d\n");
      status=MSTRO_INVMSG;
      goto BAILOUT_FREE;
  }

  if(msg->token->appid->id!=MSTRO_APP_ID_MANAGER) {
    NOISE("Message from peer %d\n", msg->token->appid->id);
  }

  status = MSTRO_OK;
  switch(msg->msg_case) {
    /* 'good' messages: */
    case MSTRO__POOL__MSTRO_MSG__MSG_WELCOME: {
      assert(!msg->welcome->nack); /* this case handled above */
      g_pool_app_id = msg->welcome->token->appid->id;
      g_pool_appid.id = g_pool_app_id;
      g_pool_apptoken.appid = &g_pool_appid;
      /* FIXME: possibly add security token at this time */
      atomic_store(&g_mstro_pm_attached, true);
      mstro_pm__msg_free(msg);
      break;
    }

    /**handled by operations */

    case MSTRO__POOL__MSTRO_MSG__MSG_DECLARE_ACK:
      //DEBUG("PC DECLARE-ACK result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE_ACK:
      //DEBUG("PC SUBSCRIBE-ACK result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE_REPLY:
      //DEBUG("PC MSG-RESOLVE-REPLY result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_ACK:
      //DEBUG("PC MSG-ACK result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_BYE:
      //DEBUG("PC MSG-BYE result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_TICKET:
      //DEBUG("PC TRANSFER-TICKET result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_INITIATE_TRANSFER:
      //DEBUG("PC INIT-TRANSFER result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_EVENT:
      //DEBUG("PC MSG-EVENT result: %d\n", status);
    case MSTRO__POOL__MSTRO_MSG__MSG_TRANSFER_COMPLETED:
      //DEBUG("PC MSG-TRANSFER-COMPLETED result: %d\n", status);
      status = mstro_pc__op_maker(msg);
      break;

    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND_ATTR_RES:
    case MSTRO__POOL__MSTRO_MSG__MSG_QUERY_RES:
      WARN("Unhandled message kind: %s\n",
           msg->declare->base.descriptor->name);
     status = MSTRO_UNIMPL;
     goto BAILOUT_FREE;


      /* illegal on pool client: */
    case MSTRO__POOL__MSTRO_MSG__MSG_DECLARE:
    case MSTRO__POOL__MSTRO_MSG__MSG_SEAL:
    case MSTRO__POOL__MSTRO_MSG__MSG_SEAL_GROUP:
    case MSTRO__POOL__MSTRO_MSG__MSG_OFFER:
    case MSTRO__POOL__MSTRO_MSG__MSG_REQUIRE:
    case MSTRO__POOL__MSTRO_MSG__MSG_RETRACT:
    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND:
    case MSTRO__POOL__MSTRO_MSG__MSG_WITHDRAW:
    case MSTRO__POOL__MSTRO_MSG__MSG_DISPOSE:
    case MSTRO__POOL__MSTRO_MSG__MSG_JOIN:
    case MSTRO__POOL__MSTRO_MSG__MSG_LEAVE:
    case MSTRO__POOL__MSTRO_MSG__MSG_SUBSCRIBE:
    case MSTRO__POOL__MSTRO_MSG__MSG_UNSUBSCRIBE:
    case MSTRO__POOL__MSTRO_MSG__MSG_LOG:
    case MSTRO__POOL__MSTRO_MSG__MSG_DEMAND_ATTR:
    case MSTRO__POOL__MSTRO_MSG__MSG_QUERY:
    case MSTRO__POOL__MSTRO_MSG__MSG_RESOLVE:
    case MSTRO__POOL__MSTRO_MSG__MSG__NOT_SET: 
    default:
      ERR("Invalid message kind (incoming on PC side): %s\n",
          msg->declare->base.descriptor->name);
        status =  MSTRO_INVMSG;
	goto BAILOUT_FREE;
  }
  goto BAILOUT;

BAILOUT_FREE:
  mstro_pm__msg_free(msg);

 BAILOUT:

  return status;
}



