#include "maestro/i_memlock.h"
#include "maestro/i_misc.h"
#include "maestro/logging.h"
#include "maestro/i_hashing.h"

/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)


#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>

/* Terminology:
 *
 * * An allocation is described by starting address and length;
 *
 * * A pageset is given by the address of the starting page and the
 *   number of pages following it
 *
 * * A risky page ("risky to have another allocation overlapping it")
 *   is one that is at the start or end of a page set.
 *
 */

/** the pagesize -- set at call to mstro_memlock_init() */
static long g_pagesize = 0;

/** number of bits for addresses in a page */
static size_t g_page_bits = 0;

/** the bits valid in an address that is a valid page start */
static uintptr_t g_pagify_mask = 0;


/** Assign pageset data to STARTPAGE/EXTRAPAGES/ENDPAGE for allocation
 * (ADDR,len).  EXTRAPAGES is the number of pages other than STARTPAGE
 * spanned by the allocation */
#define WITH_PAGESET(startpage,extrapages,endpage,addr,len, body)       \
  do {                                                                  \
    startpage  = (void*)(((uintptr_t)addr)       & g_pagify_mask);      \
    endpage    = (void*)((((uintptr_t)addr)+len) & g_pagify_mask);      \
    extrapages = (((uintptr_t)endpage) - ((uintptr_t)startpage)) > g_page_bits; \
    do {                                                                \
      body;                                                             \
    } while(0);                                                         \
  } while(0)                                                            \



/* We optimize for reasonably few pages being locked. That is
 * reasonable because allocators should be grouping user allocations
 * into contiguous pages, or people will be using hugepages, of which
 * there are few anyway. */

/** define a hash table from page address to refcount (size_t) */
KHASH_INIT(page_set, void *, size_t, 1, kh_voidptr_hash_func, kh_voidptr_hash_equal)

/** the table of locked pages. */
khash_t(page_set) *g_locked_pages = NULL;

/** lock protecting g_locked_pages */
static pthread_mutex_t g_locked_pages_mtx = PTHREAD_MUTEX_INITIALIZER;

#define WITH_LOCKED_PAGETABLE(body,exitlabel) do {                      \
    int wlpt_stat=pthread_mutex_lock(&g_locked_pages_mtx);              \
    if(wlpt_stat!=0) {                                                  \
      ERR("Failed to lock page-set table: %d (%s)\n",                   \
          wlpt_stat, strerror(wlpt_stat));                              \
      goto exitlabel;                                                   \
    }                                                                   \
                                                                        \
    do {                                                                \
      body;                                                             \
    } while(0);                                                         \
                                                                        \
    wlpt_stat=pthread_mutex_unlock(&g_locked_pages_mtx);                \
    if(wlpt_stat!=0) {                                                  \
      ERR("Failed to unlock page-set table: %d (%s)\n",                 \
          wlpt_stat, strerror(wlpt_stat));                              \
      goto exitlabel;                                                   \
    }                                                                   \
  } while(0)


mstro_status
mstro_memlock_init(size_t min_required)
{
  g_pagesize = sysconf(_SC_PAGESIZE);
  assert(popcount(g_pagesize)==1);
  g_page_bits = ctz(g_pagesize);
  g_pagify_mask = ~((uintptr_t)g_pagesize-1); 

  DEBUG("Page size is %ld, %zu bits, mask %" PRIxPTR "\n",
        g_pagesize, g_page_bits, g_pagify_mask);

  struct rlimit l;
  int s = getrlimit(RLIMIT_MEMLOCK, &l);
  DEBUG("RLIMIT_MEMLOCK is %zu (soft), %zu (hard)\n",
        l.rlim_cur, l.rlim_max);
  if(l.rlim_cur < min_required) {
    ERR("RLIMIT_MEMLOCK too small; please set ulimit -l %d or higher\n",
        min_required/1024);
    /* We tried to do setrlimit here, but get a segv on linux (Cray XC)
     * after it, so ask the user to do it */
    return MSTRO_NOMEM;
  }

  g_locked_pages = kh_init(page_set);

  if(g_locked_pages==NULL)
    return MSTRO_NOMEM;
  else
    return MSTRO_OK;
}


mstro_status
mstro_memlock_finalize(void)
{
  mstro_status s=MSTRO_OK;

  WITH_LOCKED_PAGETABLE({
    if(g_locked_pages) {
      if(kh_size(g_locked_pages)!=0) {
        ERR("Table of locked page-sets nonempty: %d live entries\n",
	    kh_size(g_locked_pages));
        s=MSTRO_FAIL;
      }
      kh_destroy(page_set, g_locked_pages);
    } else {
      ERR("called before init, or called twice\n");
    }
  }, BAILOUT);
BAILOUT:
  // may leak on error, but will allow clean re-try:
  g_locked_pages=NULL;
  
  return s;
}


/** check whether @arg page is locked.
 *
 * Must be called while holding @ref g_locked_pages_mtx.
 */
static inline
bool
mstro_memlock__page_locked(void *page)
{
  assert(g_pagify_mask == ((uintptr_t)page |g_pagify_mask)); /* check that it's a page address */
  khiter_t entry_for_page = kh_get(page_set, g_locked_pages, page);
  if(entry_for_page!=kh_end(g_locked_pages)) {
    return true;
  } else {
    return false;
  }
}


/** Register one level of locking on @arg page.
 *
 * Must be called while holding @ref g_locked_pages_mtx.
 *
 * If this is the first entry for @arg page, mlock() it.
 */
static inline
mstro_status
mstro_memlock__page_ref(void *page)
{
  assert(g_pagify_mask == ((uintptr_t)page |g_pagify_mask)); /* check that it's a page address */

  int r=-2;

  khiter_t entry_for_page = kh_put(page_set, g_locked_pages, page, &r);
  switch(r) {
    case -1:
      ERR("Failed to insert %p into page-set table\n", page);
      return MSTRO_FAIL;
      break;
    case 0: /* page known: increase refcount */
      kh_val(g_locked_pages,entry_for_page) += 1;
      break;
    case 1: /* fallthrough */
    case 2: /* new entry */
      {
        kh_val(g_locked_pages,entry_for_page) = 1;
        int s=mlock(page,g_pagesize);
        if(s!=0) {
          ERR("Failed to mlock() page at %p (%zu bytes): %d (%s)\n",
              page, g_pagesize, errno, strerror(errno));
          kh_del(page_set, g_locked_pages, entry_for_page);
          return MSTRO_FAIL;
        }
        break;
      }
    default:
      ERR("Unexpected return code from kh_put: %d\n", r);
      return MSTRO_FAIL;
      break;
  }

  DEBUG("lock-page refcount for page %p now at %zu\n", page, kh_val(g_locked_pages, entry_for_page));

  return MSTRO_OK;
}

/** De-Register one level of locking on @arg page.
 *
 * Must be called while holding @ref g_locked_pages_mtx.
 *
 * If level drops to 0, munlock() the page.
 */
static inline
mstro_status
mstro_memlock__page_deref(void *page)
{
  assert(g_pagify_mask == ((uintptr_t)page |g_pagify_mask)); /* check that it's a page address */

  int r=-2;

  khiter_t entry_for_page = kh_get(page_set, g_locked_pages, page);
  if(entry_for_page==kh_end(g_locked_pages)) {
    ERR("Tried to deref page %p from page-set table, but it's not present\n",
        page);
    return MSTRO_FAIL;
  }

  int refcount = kh_val(g_locked_pages, entry_for_page);
  assert(refcount>0);

  kh_val(g_locked_pages, entry_for_page) -= 1;
  refcount--;

  DEBUG("lock-page refcount for page %p now at %zu\n", page, refcount);
  
  if(refcount==0) {
    int s = munlock(page, g_pagesize);
    if(s!=0) {
      ERR("Failed to munlock() page at %p: %d (%s)\n", page, errno, strerror(errno));
      return MSTRO_FAIL;
    }
    kh_del(page_set, g_locked_pages, entry_for_page);
  }
  
  return MSTRO_OK;
}

mstro_status
mstro_memlock(void* addr, size_t len)
{
  assert(g_pagesize!=0); /* users must call mstro_memlock_init() before */
  mstro_status status = MSTRO_OK;
  
  void *startpage,*endpage;
  size_t extrapages;
  
  WITH_PAGESET(startpage,extrapages,endpage, addr, len, {
      DEBUG("Allocation %p, %zu: page set %p,%p,%zu\n",
            addr, len, startpage, endpage,extrapages);
      WITH_LOCKED_PAGETABLE({
          status = mstro_memlock__page_ref(startpage);
          if(status==MSTRO_OK) {
            status = mstro_memlock__page_ref(endpage);
            if(status!=MSTRO_OK) {
              mstro_memlock__page_deref(startpage);
            } else {
              if(extrapages>1) {
                /* start and end on different pages, and at least one
                 * page in between: Lock the piece between */
                void *range_start = (void*)((uintptr_t)startpage + g_pagesize);
                size_t range = (extrapages-1) * g_pagesize;
                int s = mlock(range_start, range);
                if(s!=0) {
                  ERR("Failed to lock inner region for %p at %p (%zu bytes) for addr %p (%zu bytes): %d (%s)\n",
                      startpage, range_start, range, addr, len,
                      errno, strerror(errno));
                  status = MSTRO_FAIL;
                  mstro_memlock__page_deref(startpage);
                  mstro_memlock__page_deref(endpage);
                }
              }
            }
          }
        },
        BAILOUT_FAIL);
    });

BAILOUT:
  return status;
BAILOUT_FAIL:
  return MSTRO_FAIL;
}

mstro_status
mstro_memunlock(void* addr, size_t len)
{
  assert(g_pagesize!=0); /* users must call mstro_memlock_init() before */
  mstro_status status = MSTRO_OK;
  
  void *startpage,*endpage;
  size_t extrapages;
  
  WITH_PAGESET(startpage,extrapages,endpage, addr, len, {
      DEBUG("Allocation %p, %zu: page set %p,%p,%zu\n",
            addr, len, startpage, endpage,extrapages);
      WITH_LOCKED_PAGETABLE({
          status = mstro_memlock__page_deref(startpage);
          if(status==MSTRO_OK) {
            status = mstro_memlock__page_deref(endpage);
            if(status==MSTRO_OK) {
              if(extrapages>1) {
                /* start and end on different pages, and at least one
                 * page in between: Lock the piece between */
                void *range_start = (void*)((uintptr_t)startpage + g_pagesize);
                size_t range = (extrapages-1) * g_pagesize;
                int s = munlock(range_start, range);
                if(s!=0) {
                  ERR("Failed to unlock inner region for %p at %p (%zu bytes) for addr %p (%zu bytes): %d (%s)\n",
                      startpage, range_start, range, addr, len,
                      errno, strerror(errno));
                  status = MSTRO_FAIL;
                }
              }
            }
          }
        },
        BAILOUT_FAIL);
    });

BAILOUT:
  return status;
BAILOUT_FAIL:
  return MSTRO_FAIL;
}
