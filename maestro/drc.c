/* -*- mode:c -*- */
/** @file
 ** @brief Maestro DRC
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro/i_drc.h"
#include "maestro/logging.h"
#include "maestro/status.h"
#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>
#include <string.h>

/* for getuid */
#include <unistd.h>
#include <sys/types.h>


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_COMM,__VA_ARGS__)


#define NFREE(x) do { if(x) free(x); } while(0)

#ifdef HAVE_DRC
#include <rdmacred.h>

#ifndef LOCAL_LIBFABRIC
# include <rdma/fi_ext_gni.h>
#else
# include <fi_ext_gni.h> /* use uninstalled one */
#endif

#else

/* define some placeholders; we want to compile code with dummy
 * results instead of getting rid of it */
struct fi_gni_auth_key {
  int type;
  union {
    uint32_t protection_key;
  } raw;
};

#define DRC_SUCCESS 0

#define GNIX_AKT_RAW 4711
typedef void * drc_info_handle_t;

enum {
        DRC_FLAGS_FLEX_CREDENTIAL = 1 << 0,  /* acquire flag, flexible credential mode */
        DRC_FLAGS_PERSISTENT = 1 << 1, /* acquire flag, persistent credential */
        DRC_FLAGS_TARGET_WLM = 1 << 2, /* grant/revoke flag, value is WLM ID */
        DRC_FLAGS_TARGET_UID = 1 << 3, /* grant/revoke flag, value is UID */
        DRC_FLAGS_TARGET_GID = 1 << 4, /* grant/revoke flag, value is GID */
        DRC_MAX_FLAGS
};

static inline
int drc_acquire(uint32_t *credential, int flags)
{
  /* avoid unused arg warning */
  int dummy=flags;

  DEBUG("Dummy drc_acquire called\n");
  *credential = 0xffffffff;
  return DRC_SUCCESS;
}

static inline
int drc_access(uint32_t credential, int flags, drc_info_handle_t *info)
{
  /* avoid unused arg warnings */
  uint32_t dummy1=credential;
  uint32_t dummy2=flags;

  DEBUG("Dummy drc_access called\n");
  *info = (void*)0xdeadbeef;
  return DRC_SUCCESS;
}

static inline
int drc_grant(uint32_t credential, uint32_t value, int flags)
{
  /* avoid unused arg warnings */
  uint32_t dummy1=credential;
  uint32_t dummy2=value;
  int dummy3=flags;

  DEBUG("Dummy drc_grant called\n");
  return DRC_SUCCESS;
}

static inline
int drc_release(uint32_t credential, int flags)
{
  /* avoid unused arg warnings */
  uint32_t dummy1=credential;
  int dummy2=flags;

  // Since release is called very late when the logging infrastructure is already disabled we put this one at NOISE
  NOISE("Dummy drc_release called\n");
  return DRC_SUCCESS;
}

static inline
uint32_t
drc_get_first_cookie(drc_info_handle_t *info)
{
  void* dummy=info; /* avoid unused arg warning */
  return 0xdeadbeef;
}

#endif


#define MSTRO_DRC_SERIALIZED_LEN 11 /* uint32_t max is 4294967295 in decimal, + \0 */
struct mstro_drc_info_ {
  uint32_t drc_id;                                 /**< the DRC credential ID */
  struct fi_gni_auth_key auth_key;                /**< the actual key */
  char serialized_id[MSTRO_DRC_SERIALIZED_LEN];    /**< the serialized ID */
};

static inline
void
mstro_drc_init_internal(mstro_drc_info result,
                        drc_info_handle_t info)
{
  int ret = snprintf(result->serialized_id, MSTRO_DRC_SERIALIZED_LEN,
                     "%" PRIu32, result->drc_id);
  assert(ret<MSTRO_DRC_SERIALIZED_LEN);
  
  result->auth_key.type = GNIX_AKT_RAW;
  result->auth_key.raw.protection_key = drc_get_first_cookie(info);

  return;
}

/** obtain DRC credentials */
mstro_status
mstro_drc_init(mstro_drc_info *result_p)
{
  mstro_status stat = MSTRO_UNIMPL;
  if(result_p==NULL)
    return MSTRO_INVOUT;
  
  *result_p = malloc(sizeof(struct mstro_drc_info_));
  if(*result_p) {
    int ret;
    drc_info_handle_t info;
    char *do_nonflex = getenv(MSTRO_ENV_DRC_NON_FLEX);
    if(do_nonflex!=NULL && atoi(do_nonflex)!=0
       && do_nonflex[0]!='f' && do_nonflex[0]!='F' // fAlSe
       && do_nonflex[0]!='d' && do_nonflex[0]!='D' // DiSabled
       ) {
      // if user requests it: use non-flex credentials
      ret = drc_acquire(&(*result_p)->drc_id, 0);
    } else {
      // default: flex credentials, to allow multiple jobs on the same node ("DRC node insecure mode")
      ret = drc_acquire(&(*result_p)->drc_id,  DRC_FLAGS_FLEX_CREDENTIAL);
    }
      
    if(ret!=DRC_SUCCESS) {
      ERR("Failed to drc_acquire a new credential: %d\n", ret);
      stat=MSTRO_FAIL; goto BAILOUT_FREE;
    }

    ret = drc_access((*result_p)->drc_id, 0, &info);
    if(ret!=DRC_SUCCESS) {
      ERR("Failed to drc_access the new credential (%d): %d\n",
          (*result_p)->drc_id, ret);
      stat=MSTRO_FAIL; goto BAILOUT_FREE;
    }

    uid_t my_uid = getuid();
    
    ret = drc_grant((*result_p)->drc_id, my_uid, DRC_FLAGS_TARGET_UID);
    if(ret!=DRC_SUCCESS) {
      ERR("Failed to drc_grant to myself (uid %zu) the new credential (%d): %d\n",
          (size_t) my_uid, (*result_p)->drc_id, ret);
      stat=MSTRO_FAIL; goto BAILOUT_FREE;
    }
    
    mstro_drc_init_internal(*result_p, info);

    DEBUG("Allocated DRC credential %" PRIu32 " (OOB: %s, cookie %" PRIx64 ")\n",
         (*result_p)->drc_id, (*result_p)->serialized_id,
         (*result_p)->auth_key.raw.protection_key);
    stat=MSTRO_OK;
    
  } else {
    stat=MSTRO_NOMEM;
    goto BAILOUT_FREE;
  }
  goto BAILOUT;
BAILOUT_FREE:
  NFREE(*result_p);
BAILOUT:
  return stat;
}
  

/** release DRC credentials */
mstro_status
mstro_drc_destroy(mstro_drc_info info, bool do_release)
{
  if(info==NULL)
    return MSTRO_INVARG;
  if(do_release) {
    int ret = drc_release(info->drc_id, 0);
    if(ret!=DRC_SUCCESS) {
      /* JIRA CAST-26265 says that only one drc_release in one WLM
       * allocation will succeed, and the rest will fail, but that it is
       * ok to ignore that error on the failing 'ranks' */
      DEBUG("Failed to release DRC credential %d: %d (will pretend things are okay anyway)\n",
           info->drc_id, ret);
    }
  }
  free(info);
  return MSTRO_OK;
}
      

static
struct fi_gni_auth_key *
dup_authkey(const struct fi_gni_auth_key *k)
{
  assert(k!=NULL);
  struct fi_gni_auth_key *res = malloc(sizeof(struct fi_gni_auth_key));
  if(res) {
    memcpy(res, k, sizeof(struct fi_gni_auth_key));
  }
  return res;
}


/** Insert DRC credentials into a libfabric object */
mstro_status
mstro_drc_insert_ofi(struct fi_info *fi, const mstro_drc_info info)
{
  if(fi==NULL||info==NULL)
    return MSTRO_INVARG;

  assert(fi->addr_format==FI_ADDR_GNI);
#if FI_VERSION_GE(FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION), FI_VERSION(1,5))
  // auth key is free()ed by fabric.c on teardown, so we need to duplicate it
  fi->domain_attr->auth_key = (uint8_t*) dup_authkey(&info->auth_key);
  if(fi->domain_attr->auth_key==NULL) {
    return MSTRO_NOMEM;
  }
  fi->domain_attr->auth_key_size = sizeof(info->auth_key);
#else
  #warning Open Fabric version too old for auth_key suppport
#endif
  
  return MSTRO_OK;
}

/** Obtain cookie for passing to partners (as string, suitably serialized) */
mstro_status
mstro_drc_get_oob_string(char **result_p,
                         const mstro_drc_info info)
{
  if(result_p==NULL)
    return MSTRO_INVOUT;
  
  *result_p = strdup(info->serialized_id);
  if(*result_p==NULL)
    return MSTRO_NOMEM;
  else
    return MSTRO_OK;
}

mstro_status
mstro_drc_get_credential(uint32_t *result_p,
                         const mstro_drc_info info)
{
  if(result_p==NULL)
    return MSTRO_INVOUT;

  *result_p = info->drc_id;
  return MSTRO_OK;
}

/** Create drc object from OOB string info */
mstro_status
mstro_drc_init_from_oob_string(mstro_drc_info *result_p,
                               const char *info_string)
{
  mstro_status stat = MSTRO_UNIMPL;
  uint32_t id;
  int ret;

  if(result_p==NULL)
    return MSTRO_INVOUT;
  if(info_string==NULL)
    return MSTRO_INVARG;

  ret = sscanf(info_string, "%" PRIu32, &id);
  if(ret<1) {
    ERR("Failed to parse a DRC credential in |%s|\n", info_string);
    return MSTRO_FAIL;
  }
  
  *result_p = malloc(sizeof(struct mstro_drc_info_));
  if(*result_p) {
    drc_info_handle_t info;
    (*result_p)->drc_id = id;
    
    ret = drc_access((*result_p)->drc_id, 0, &info);
    if(ret!=DRC_SUCCESS) {
      ERR("Failed to drc_access credential %" PRIu32 ": %d\n", id, ret);
      goto BAILOUT_FREE;
    }

    mstro_drc_init_internal(*result_p, info);

    DEBUG("Accessing DRC credential %d (OOB: was %s, is %s), cookie %" PRIx64 ")\n",
         (*result_p)->drc_id,
         info_string, (*result_p)->serialized_id,
         (*result_p)->auth_key.raw.protection_key);
    stat = MSTRO_OK;
  } else {
    stat = MSTRO_NOMEM;
  }
  goto BAILOUT;
BAILOUT_FREE:
  NFREE(*result_p);
BAILOUT:
  return stat;  
}

mstro_status
mstro_drc_init_from_credential(mstro_drc_info *result_p,
                               uint32_t credential)
{
  mstro_status stat = MSTRO_UNIMPL;
  uint32_t id = credential;
  int ret;

  if(result_p==NULL)
    return MSTRO_INVOUT;
  
  *result_p = malloc(sizeof(struct mstro_drc_info_));
  if(*result_p) {
    drc_info_handle_t info;
    (*result_p)->drc_id = id;
    
    ret = drc_access((*result_p)->drc_id, 0, &info);
    if(ret!=DRC_SUCCESS) {
      ERR("Failed to drc_access credential %" PRIu32 ": %d\n", id, ret);
      goto BAILOUT_FREE;
    }

    mstro_drc_init_internal(*result_p, info);

    DEBUG("Accessing DRC credential %d (OOB: is %s), cookie %" PRIx64 ")\n",
          (*result_p)->drc_id,
          (*result_p)->serialized_id,
          (*result_p)->auth_key.raw.protection_key);
    stat = MSTRO_OK;
  } else {
    stat = MSTRO_NOMEM;
  }
  goto BAILOUT;
BAILOUT_FREE:
  NFREE(*result_p);
BAILOUT:
  return stat;  
}


  
