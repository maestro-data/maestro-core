/* -*- mode:c -*- */
/** @file
 ** @brief Maestro OpenFabric connections header file
 **/
/*
 * Copyright © 2023 Hewlett Packard Enterprise Development LP
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include "maestro/i_ofi.h"
#include "maestro/i_globals.h"
#include "maestro/i_maestro_numa.h"
#include "maestro/i_memlock.h"
#include "maestro/i_ofi_threads.h"
#include <rdma/fi_endpoint.h>
#include <rdma/fi_rma.h>

/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PM,__VA_ARGS__)

/**Number of retrials for fi_read/send operations*/
extern size_t g_ofi_op_num_retrials;

/** ofi threads operation queues table */
khash_t(ofi_operation_queue) *g_ofi_operation_queues_table = NULL;

/** lock for the operation queues table */
pthread_mutex_t g_ofi_operation_queue_table_lock;

/**lockless lookup ..assuming ofi operation queues are not added dynamically during execution*/
static inline
mstro_status
mstro_ofi_thread__lookup_op_queue_lockless(const struct mstro_endpoint *ep,
                                      mstro_fifo *operation_queue_p)
{
  assert(operation_queue_p!=NULL);
  assert(g_ofi_operation_queues_table != NULL);
  
  khiter_t id = kh_get(ofi_operation_queue, g_ofi_operation_queues_table,(struct mstro_endpoint *) ep);
 
  if(id==kh_end(g_ofi_operation_queues_table)) {
    ERR("Endpoint not present in the ofi operation queue table\n");
    return MSTRO_FAIL;
  }
  *operation_queue_p = kh_val(g_ofi_operation_queues_table, id);      
  return MSTRO_OK;
}

mstro_status
mstro_ofi_threads_enqueue_rdma_read(fi_addr_t target, void *buf, size_t len, 
                                    void *mr_desc, uint64_t mr_addr, uint64_t mr_key, 
                                    struct mstro_endpoint *ep, mstro_event completion_event, mstro_request request)
{
  mstro_status status = MSTRO_UNIMPL;
  mstro_fifo operation_queue = NULL;
  mstro_ofi_threads_op entry = NULL;
  
  /**find out which queue this operation should be pushed to*/
  status = mstro_ofi_thread__lookup_op_queue_lockless(ep, &operation_queue);
  if (status != MSTRO_OK)
  {
    ERR("Failed to find ep %p in the ofi operation queues table\n", ep);
    return status;
  }

  entry = malloc(sizeof(struct mstro_ofi_threads_op_));
  if (entry == NULL)
  {
    ERR("Could not allocate a queue entry\n");
    return MSTRO_NOMEM;
  }
  entry->kind = MSTRO_OFI_OP_READ;
  entry->read.buf = buf;
  entry->read.len = len;
  entry->read.ep = ep;
  entry->read.mr_addr = mr_addr;
  entry->read.mr_desc = mr_desc;
  entry->read.mr_key = mr_key;
  entry->read.target = target;
  entry->read.event = completion_event;
  entry->request = request;

  /*enqueue operation*/
  status = mstro_fifo_enqueue(operation_queue, entry);
  DEBUG("Message enqueued to be read from ep %p\n", ep);
  return status;
}



mstro_status
mstro_ofi_threads_enqueue_for_send(fi_addr_t dst, struct mstro_msg_envelope *msg, mstro_event completion_event)
{
  mstro_status status = MSTRO_UNIMPL;
  mstro_fifo operation_queue = NULL;
  mstro_ofi_threads_op entry = NULL;
  
  const struct mstro_endpoint *ep = msg->ep;
  /**find out which queue this msg should be pushed to*/
  status = mstro_ofi_thread__lookup_op_queue_lockless(ep,&operation_queue);
  if (status != MSTRO_OK)
  {
    ERR("Failed to find ep %p in the ofi operation queues table\n", ep);
    return status;
  }

  entry = malloc(sizeof(struct mstro_ofi_threads_op_));
  if (entry == NULL)
  {
    ERR("Could not allocate a queue entry\n");
    return MSTRO_NOMEM;
  }
  entry->kind = MSTRO_OFI_OP_SEND;
  entry->send.dst = dst;
  entry->send.msg = msg;
  entry->send.event = completion_event;
  entry->request = NULL;

  /*enqueue msg*/
  status = mstro_fifo_enqueue(operation_queue, entry);
  DEBUG("Message enqueued to be send from ep %p\n", ep);
  return status;
}

static inline
mstro_status
mstro_ofi_threads__make_progress(struct mstro_endpoint *ep, mstro_msg_handler msg_handler)
{
  
  usleep(1000);
  NOISE("making progress\n");
  return mstro_ofi__check_and_handle_cq(ep, msg_handler);
}

/**handle OFI operation queue entry (send)*/
static inline
mstro_status
mstro_ofi_threads__send_msg(mstro_ofi_threads_send_op entry, mstro_ofi_thread_context *thread_ctx, mstro_request request)
{
  mstro_ofi_msg_context ctx=NULL;
  assert(entry != NULL);
  size_t trials = 0;
  bool want_completion = (entry->event == NULL) ? false: true;
  mstro_status stat
      = mstro_ofi__msg_context_create(&ctx, entry->msg, want_completion, false, thread_ctx->msg_ctx_pool);
  if(stat!=MSTRO_OK) {
    ERR("Failed to create completion context: %d (%s)\n",
        stat, mstro_status_description(stat));
    return stat;
  } else {
     mstro_ofi__remember_ctx(entry->msg->ep,ctx);
  }
  if (want_completion)
  {
    ctx->ev = entry->event;
  }
  ctx->ep = entry->msg->ep;

  int res;
  do
  {
    res = fi_send(entry->msg->ep->ep, entry->msg->data, entry->msg->payload_size,
                    NULL, entry->dst, ctx);
    if (res != 0)
    {
      if (res == -FI_EAGAIN)
      {
        /**call make progress and try again*/
        stat = mstro_ofi_threads__make_progress(entry->msg->ep, thread_ctx->msg_handler);
        assert(stat == MSTRO_OK);
      }
      else /*fi_send fail */
      {
        break;
      }   
    }
    trials++;
  } while((res == -FI_EAGAIN) && (trials < g_ofi_op_num_retrials)); /*retry fi_send*/
  
  if(res != 0)
  {
    ERR("fi_send failed after %zu trials: %d (%s)\n", trials, res, fi_strerror(-res));
    /**destroy event*/
    if(ctx->ev) {
      mstro_event_destroy(ctx->ev);
    }
    if(ctx->msg) {
      mstro_msg_envelope_free(ctx->msg);
    }
    mstro_ofi__forget_ctx(ctx->ep, ctx);
    mstro_ofi__msg_context_destroy(ctx);
    stat = MSTRO_OFI_FAIL;
  }
  else
  {
    stat = MSTRO_OK;
  }

  /**update request*/
  if (request != NULL) {
    mstro_request__generic__mark_complete(request, stat, NULL);
  }

  return stat;
}

/**handle OFI operation queue entry (read)*/
static inline
mstro_status
mstro_ofi_threads__read(mstro_ofi_threads_read_op entry, mstro_ofi_thread_context *thread_ctx, mstro_request request)
{
  mstro_ofi_msg_context ctx=NULL;
  size_t trials = 0;
  assert(entry != NULL);
  bool want_completion = (entry->event == NULL) ? false: true;
  mstro_status stat
      = mstro_ofi__msg_context_create(&ctx, NULL, want_completion, false, thread_ctx->msg_ctx_pool);
  if(stat!=MSTRO_OK) {
    ERR("Failed to create completion context: %d (%s)\n",
        stat, mstro_status_description(stat));
    return stat;
  } else {
     mstro_ofi__remember_ctx(entry->ep,ctx);
  }
  if (want_completion)
  {
    ctx->ev = entry->event;
  }
  ctx->ep = entry->ep;

  int res;
  do
  {
    res = fi_read(entry->ep->ep, entry->buf, entry->len, 
                  entry->mr_desc, entry->target, entry->mr_addr, entry->mr_key, ctx);
    if (res != 0)
    {
      if (res == -FI_EAGAIN)
      {
        /**call make progress and try again*/
        stat = mstro_ofi_threads__make_progress(entry->ep, thread_ctx->msg_handler);
        assert(stat == MSTRO_OK);
      }
      else /*fi_read fail */
      {
        break;
      }   
    }
    trials++;
  } while((res == -FI_EAGAIN) && (trials < g_ofi_op_num_retrials)); /*retry fi_send*/

  if(res != 0)
  {
    ERR("fi_read failed after %zu trials: %d (%s)\n", trials, res, fi_strerror(-res));
    /**destroy event*/
    if(ctx->ev) {
      mstro_event_destroy(ctx->ev);
    }
    mstro_ofi__forget_ctx(ctx->ep, ctx);
    mstro_ofi__msg_context_destroy(ctx);
    stat = MSTRO_OFI_FAIL;
  }
  else
  {
    stat = MSTRO_OK;
  }

  /**update request*/
  if (request != NULL) {
    mstro_request__generic__mark_complete(request, stat, NULL);
  }

  return stat;
}

static inline
mstro_status
mstro_ofi_threads__register_operation_queue(struct mstro_endpoint *ep,
                              mstro_fifo operation_queue)
{
  int ret = 0;
  if((ep==NULL) || (operation_queue == NULL))
    return MSTRO_INVARG;

  assert(g_ofi_operation_queues_table != NULL);

  ret=pthread_mutex_lock(&g_ofi_operation_queue_table_lock);       
  if(ret!=0) {                                                     
    ERR("Failed to lock ofi threads operation queue table: %d (%s)\n",                       
        ret, strerror(ret));                                  
    abort();                                                            
  }
  
  khiter_t id = kh_put(ofi_operation_queue, g_ofi_operation_queues_table, ep, &ret);
  assert(id!=kh_end(g_ofi_operation_queues_table));
	assert(ret!=-1);
	assert(ret!=0);
  kh_val(g_ofi_operation_queues_table, id) = operation_queue;
  DEBUG("Registered ep %p in OFI operation queue table\n",ep);
  
  ret=pthread_mutex_unlock(&g_ofi_operation_queue_table_lock);         
  if(ret!=0) {                                                     
    ERR("Failed to unlock app registry: %d (%s)\n",                     
        ret, strerror(ret));                                  
    abort();                                                            
  }                        
  return MSTRO_OK;
}

static inline
mstro_status
mstro_ofi_thread__loop(mstro_ofi_thread_context *thread_ctx)
{
  mstro_status status = MSTRO_OK;
  size_t i;

  if (thread_ctx == NULL)
  {
    ERR("Thread context is NULL\n");
    return MSTRO_INVARG;
  }
  _Atomic bool *terminate = thread_ctx->team_stop;
  struct mstro_endpoint_set *endpoints = thread_ctx->ep_set;
  mstro_ofi_threads_op ofi_op = NULL;

  assert(endpoints != NULL);

  DEBUG("OFI loop starting\n");
  /* post receives on all endpoints */
  for(i=0; i<endpoints->size; i++) {
    size_t num_rec = mstro_ofi__get_num_recv(&endpoints->eps[i]);
    for(size_t j=0; j<num_rec; j++) {
      status = mstro_ofi__loop_post_recv(endpoints->eps + i, thread_ctx->msg_ctx_pool);
      if(status!=MSTRO_OK) {
        goto BAILOUT;
      }
    }
    DEBUG("Posted %zu RECV on ep %p, iterating cq_read\n",
          num_rec, &endpoints->eps[i]);

  }

  /* wait for completions on all endpoints. If a message arrives, handle
   * it (re-posting a receive) */
  do {
    /**check ofi op queue */
    ofi_op = mstro_fifo_i_dequeue(thread_ctx->operation_queue);
    if(ofi_op != NULL)
    {
      switch (ofi_op->kind)
      {
      case MSTRO_OFI_OP_SEND:
        status = mstro_ofi_threads__send_msg(&ofi_op->send, thread_ctx, ofi_op->request);
        free(ofi_op); /**shallow free, we still need msg and event parts*/
        ofi_op = NULL; 
        if(status != MSTRO_OK)
        {
          ERR("Failed to send a message from queue\n");
        }
        else
        {
          DEBUG("Submitted fi_send operation\n");
        }
        break;
      case MSTRO_OFI_OP_READ:
        status = mstro_ofi_threads__read(&ofi_op->read, thread_ctx, ofi_op->request);
        free(ofi_op); /**shallow free, we still need msg and event parts*/
        ofi_op = NULL; 
        if(status != MSTRO_OK)
        {
          ERR("Failed to do fi_read operation\n");
        }
        else
        {
          DEBUG("Submitted fi_read operation\n");
        }
        break;
      default:
        ERR("Invalid OFI Thread operation type\n");
        break;
      }
      
    }
    
    for(i=0; false==atomic_load(terminate) && i<endpoints->size; i++) {
      mstro_ofi__check_and_handle_cq(&endpoints->eps[i],
                                     thread_ctx->msg_handler);
    }
  } while(false==atomic_load(terminate));
  DEBUG("OFI thread quits: loop status %d\n", status);

BAILOUT:
   return status;
}

void *
mstro_ofi_thread__start(void *data)
{
  mstro_ofi_thread_context *ctx = (mstro_ofi_thread_context *) data;
  
  /**pin thread*/
  mstro_status status  =  mstro_numa_bind_thread(ctx->tidprefix); 
  #ifdef HAVE_NUMA
  /**update numa node*/
  int numa_node = numa_node_of_cpu(mstro_numa_get_cpu());
  ctx->numa_node = numa_node;
  #endif

  /**build thread specific id*/
  #define TID_LEN 24
  char threadid[TID_LEN];
  int n = snprintf(threadid, TID_LEN, "%s-%d-%zu",
                   ctx->tidprefix, ctx->numa_node, ctx->id);
  if(n>=TID_LEN) {
    WARN("threadid |%s-%d-%zu| truncated to %s\n",
         ctx->tidprefix, ctx->numa_node, ctx->id, threadid);
  }
  char *tid = pthread_getspecific(g_thread_descriptor_key);
  if(tid != NULL)
  {
	  free(tid);
  }
  tid = strdup(threadid); /* will be free()ed by cleanup of threadspecific */

  int ret = pthread_setspecific(g_thread_descriptor_key, tid);
  if(ret!=0) {
    ERR("Failed to set threadid %s: %d (%s)\n", threadid, ret, strerror(ret));
  }

  DEBUG("Thread starting and running on CPU: %d\n", mstro_numa_get_cpu());
  
  /**initialize operation queue*/
  status = mstro_fifo_init(&(ctx->operation_queue));
  assert(status == MSTRO_OK); 

  /*create our mempool*/
  ctx->msg_ctx_pool = mstro_mempool_create("OFI Message Context Pool",
                                        sizeof(struct mstro_ofi_msg_context_),
                                        1, /**only one arena, this is a local pool for this thread, (FIXME) might as well lose the locks*/
                                        2, true);
  if(ctx->msg_ctx_pool==NULL) {
    ERR("Failed to allocate OFI Message Context Pool\n");
    abort();
  } 

  /**init ofi */
  status = mstro_ofi_discover_and_build_eps(&(ctx->ep_set), FI_THREAD_DOMAIN); 
  assert(status == MSTRO_OK);

  /**register my operation queues*/
  for (size_t i = 0; i < ctx->ep_set->size; i++)
  {
    status = mstro_ofi_threads__register_operation_queue(&ctx->ep_set->eps[i], ctx->operation_queue);
    if (status != MSTRO_OK)
    {
      ERR("Failed to register my OFI operation queue, %d (%s)\n", status, mstro_status_description(status));
      return NULL;
    }
    
  }
  /*Signal that we are ready*/
  atomic_fetch_add(ctx->threads_ready,1);

  /**normal cq handling*/
  status = mstro_ofi_thread__loop(ctx);
  if(status!=MSTRO_OK) {
	  ERR("OFI loop terminated with error %d (%s)\n",
			  status, mstro_status_description(status));
  }
  DEBUG("OFI thread stopping\n");

  status = mstro_ofi_destroy_ep_set(ctx->ep_set);
  ctx->ep_set= NULL;
  assert(status == MSTRO_OK);

  /*destroy mempool*/
  mstro_mempool_destroy(ctx->msg_ctx_pool);
  ctx->msg_ctx_pool=NULL;

  /**destroy queue*/
  status = mstro_fifo_finalize(ctx->operation_queue);
  assert(status == MSTRO_OK);

  return NULL;
}

mstro_status
mstro_ofi_thread_team_create(
    const char *team_name,
    const char *tidprefix,
    size_t num_threads,
    mstro_msg_handler msg_handler,
    struct mstro_ofi_thread_team **ofi_team)
{
  int ret;
  if ((ofi_team == NULL) || (team_name == NULL) || (tidprefix == NULL) || (num_threads <= 0))
  {
    ERR("Invalid input arguments \n");
    return MSTRO_INVARG;
  }
  
  *ofi_team = malloc(sizeof(struct mstro_ofi_thread_team)
            + (num_threads * sizeof(struct mstro_ofi_thread_team_member)));
   if((*ofi_team)==NULL) {
    ERR("Failed to allocate %s thread team member handles\n", tidprefix);
    return MSTRO_NOMEM;
  }

  char *tidpfx = strdup(tidprefix);
  if(tidpfx==NULL) {
    ERR("Failed to allocate team prefix\n");
    free(*ofi_team);
    *ofi_team = NULL;
    return MSTRO_NOMEM;
  }

  (*ofi_team)->team_name = strdup(team_name);
  (*ofi_team)->num_members = num_threads;
  (*ofi_team)->team_stop = false;
  (*ofi_team)->threads_ready = 0;

  /**create ofi thread team members*/
  for(size_t i=0; i<num_threads; i++) {
    struct mstro_ofi_thread_team_member *thread_member = &(*ofi_team)->members[i];
    mstro_ofi_thread_context *ctx = &thread_member->ctx;
    ctx->tidprefix = tidpfx;
    ctx->id = i;
    ctx->numa_node = 0; 
    ctx->msg_handler = msg_handler;
    ctx->team_stop = &((*ofi_team)->team_stop);
    ctx->threads_ready = &((*ofi_team)->threads_ready);

    ret = pthread_create(&thread_member->t, NULL, mstro_ofi_thread__start, ctx);
    if(ret!=0) {
      ERR("Failed to start %s team thread %zu: %d (%s)\n",
          tidprefix, i, ret, strerror(ret));
      abort();
    }
    /* not detaching these threads; will cancel them to terminate and then
     * wait() before clearing ctx */
    DEBUG("Started %s team thread %zu\n", tidprefix, i);
  }
  return MSTRO_OK;
}

void
mstro_ofi_thread_team_wait_ready(struct mstro_ofi_thread_team *ofi_team)
{
  /**wait for all threads to build their endpoints*/
  while(atomic_load(&(ofi_team->threads_ready)) < ofi_team->num_members)
  			;	
}

mstro_status
mstro_ofi_thread_team__stop(struct mstro_ofi_thread_team *ofi_team)
{
  INFO("Termination of OFI thread team %s initiated\n", ofi_team->team_name);
  if(atomic_load(&ofi_team->team_stop)) {
    DEBUG("Duplicate %s termination request, ignoring it\n", ofi_team->team_name);
    return MSTRO_NOT_TWICE;
  }

  atomic_store(&ofi_team->team_stop,true); /**stop ofi thread team*/
  return MSTRO_OK;
}


/**FIXME ...very similar to erl_thread_team_stop_and_destroy_bl*/
mstro_status
mstro_ofi_thread_team_stop_and_destroy(struct mstro_ofi_thread_team *ofi_team)
{
  mstro_status stat=MSTRO_OK;
  
  stat = mstro_ofi_thread_team__stop(ofi_team);
  if(stat == MSTRO_NOT_TWICE)
  {
    /**ignore duplicate*/
    return MSTRO_OK;
  }

  for(size_t i =0; i < ofi_team->num_members; i++) {
    int s = pthread_join(ofi_team->members[i].t, NULL);
    if(s!=0) {
      ERR("Failed to join ofi thread %zu: %d (%s)\n",
          i, s, strerror(s));
      stat=MSTRO_FAIL;
    }

    DEBUG("OFI thread team handler thread %zu terminated\n", i);
  }
  free(ofi_team->members[0].ctx.tidprefix);
  free(ofi_team->team_name);
  free(ofi_team);
 
  return stat;
}

/**
 * @brief returns an endpoint set on this numa node. 
 * Operation threads wishing to befriend a new components need to use this function
 * to obtain an endpoint set on their same local numa 
 * and use it for connection with the new component
 * 
 * @param team ofi_thread team handle (pc or pm)
 * @param target_numa numa node
 * @return mstro_status enpointset on the target numa
 */
struct mstro_endpoint_set *
mstro_ofi_thread_get_endpointset(struct mstro_ofi_thread_team *team, int target_numa)
{
  size_t trials = 0;
  if(team==NULL)
  {
    ERR("Fatal Team handle is NULL \n");
    return NULL;
  }
  size_t i = rand() % team->num_members;

  /**count #trials to avoid inf loops*/
  while((target_numa !=   team->members[i].ctx.numa_node) && (trials < team->num_members))
  {
    /**try another time */
    i = rand() % team->num_members;
    trials++;
  }
  /**we either matched numa or are out of trials*/
  return team->members[i].ctx.ep_set;
}

/**
 * @brief returns an endpoint set that contains this endpoint. 
 * Operation threads wishing to befriend a new components need to use this function
 * to obtain an endpoint set on the contacted thread.
 * and use it for connection with the new component
 * 
 * @param team ofi_thread team handle (pc or pm)
 * @param ep contacted endpoint
 * @return mstro_status enpointset
 */
struct mstro_endpoint_set *
mstro_ofi_thread_get_endpointset_ep(struct mstro_ofi_thread_team *team, const struct mstro_endpoint *ep)
{

  struct mstro_endpoint *tmp = NULL;
  if(team==NULL)
  {
    ERR("Fatal Team handle is NULL \n");
    return NULL;
  }
  /** loop over thread team members*/
  for (size_t i = 0; i < team->num_members; i++)
  {
    /* loop over endpoints in endpoint set*/
    tmp = team->members[i].ctx.ep_set->eps;
    for (size_t j = 0; j < team->members[i].ctx.ep_set->size; j++)
    {
      if (strcmp(tmp->serialized, ep->serialized) == 0)
      {
        return team->members[i].ctx.ep_set;
      }
      else
      {
        tmp  = tmp->next;
      } 
    }
  }
  ERR("Could not find ep %s in team %s \n", ep->serialized, team->team_name);
  return NULL;
}
