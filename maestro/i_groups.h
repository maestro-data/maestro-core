#include "maestro/groups.h"
#include "maestro/i_cdo.h"

#include "maestro/logging.h"
#include "maestro/i_uthash.h"
#include "maestro/i_khash.h"


/** A bitfield describing the state of a CDO group */
enum mstro_group_state {
  MSTRO_GROUP_STATE_INVALID   = 0,
  MSTRO_GROUP_STATE_CREATED   = 1U <<0,
  MSTRO_GROUP_STATE_DECLARED  = 1U <<1,
  MSTRO_GROUP_STATE_SEALED    = 1U <<2,
  MSTRO_GROUP_STATE_OFFERED   = 1U <<3,
  MSTRO_GROUP_STATE_REQUIRED  = 1U <<4,
  MSTRO_GROUP_STATE_DEMANDED  = 1U <<5,
  MSTRO_GROUP_STATE_WITHDRAWN = 1U <<6,
  MSTRO_GROUP_STATE__pad      = 0xffff  
};

#define MSTRO_GROUP_STATE_DISPOSABLE \
  (MSTRO_GROUP_STATE_DECLARED|MSTRO_GROUP_STATE_SEALED|MSTRO_GROUP_STATE_DEMANDED|MSTRO_GROUP_STATE_WITHDRAWN)

#define MSTRO_GROUP_STATE_WITHDRAWABLE \
  (MSTRO_GROUP_STATE_OFFERED)

/** a set of CDO ids */
struct mstro_group_cdo_member {
  UT_hash_handle hh; /**< hash table: by entry (local CDO handle) */
  mstro_cdo entry;   /**< local handle. */
  bool      user_owned; /**< flag indicating whether this declaration
                         * handle is owned by us or was provided by
                         * the user */
  bool      user_pooled; /**< flag indicating whether this declaration
                          * handle was pooled by the user, or by us
                          * (so we can WITHDRAW or RETRACT it with the
                          * group WITHDRAW or RETRACT */
};

/** Declare a hash table type for a set of CDO names to be declared at group-seal time */
KHASH_SET_INIT_STR(group_member_name)

/** A CDO group handle */
struct mstro_group_ {
  char *name;                   /**< the group name */
  mstro_cdo group_cdo;          /**< A CDO that serves as proxy for the group */
  UT_hash_handle hh;            /**< hashable by ::self (address of the structure itself) */
  struct mstro_group_ *self;    /**< address of this instance -- for
                                 * hashing */
  UT_hash_handle idhh;          /**< also hashable by ::gid */
  struct mstro_cdo_id  id;      /**< the ID of the group. Currently identical to ::group_cdo->gid */

  _Atomic enum mstro_group_state state; /**< group state flag */
  struct mstro_group_cdo_member *members; /**< Group member table */
  /** Set of CDOs to be declared at group-seal. This slot will be NULL
   * after the group is sealed */
  khash_t(group_member_name)    *undeclared_cdos;
  /** Final member list after DEMAND */
  Mstro__Pool__GroupMembers     *post_demand_members; 
  size_t member_iter_idx;       /**< index when iterating over members after DEMAND */
};




  
  
