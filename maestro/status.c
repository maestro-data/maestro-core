/* -*- mode:c -*- */
/** @file
 ** @brief Status code implementation of maestro library
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/status.h"

static
const char *g_mstro_status_names[] = {
  [MSTRO_OK]     = "success",
  [MSTRO_FAIL]   = "generic failure",
  [MSTRO_UNINIT] = "maestro uninitialized -- you need to call mstro_init() first",
  [MSTRO_UNIMPL] = "unimplemented functionality",
  [MSTRO_INVARG] = "invalid argument",
  [MSTRO_INVOUT] = "invalid output argument pointer",
  [MSTRO_NOMEM]  = "insufficient memory to satisfy request",
  [MSTRO_INVMSG]  = "invalid pool protocol message",
  [MSTRO_OFI_FAIL] = "network fabric error",
  [MSTRO_TIMEOUT]  = "timeout occurred",
  [MSTRO_NOT_TWICE] = "can only be called once from a process",
  [MSTRO_WOULDBLOCK] = "would block current thread, please retry later",

  [MSTRO_NO_PM] = "No pool manager connection exists",
  [MSTRO_NOMATCH] = "A matching entry does not exist, or a template does not apply",
  [mstro_status_MAX] = "invalid status code"
};


const char *
mstro_status_description(mstro_status s)
{
  if(s>=mstro_status_MAX)
    s=mstro_status_MAX;

  return g_mstro_status_names[s];
}
