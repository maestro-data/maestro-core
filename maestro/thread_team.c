/* -*- mode:c -*- */
/** @file
 ** @brief thread team infrastructure
 **/

/*
 * Copyright (C) 2021 HPE Switzerland GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "i_thread_team.h"

#include "maestro/logging.h"
#include "maestro/i_globals.h"
#include "maestro/i_maestro_numa.h"

/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_ERL,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_ERL,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_ERL,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_ERL,__VA_ARGS__)



mstro_status
erl_thread_team_bind_and_update_numa(struct erl_thread_team_ctx *ctx)
{
 mstro_status status  =  mstro_numa_bind_thread(ctx->tidprefix);              
 
 #ifdef HAVE_NUMA
 /**update numa node*/
 int numa_node = numa_node_of_cpu(mstro_numa_get_cpu());
 ctx->numa_node = numa_node;
 #endif
 
 return status;       
}

/** function that is run inside a thread to pick up and handle messages
 * (in envelope) from the incoming queue */
void*
erl_thread_team_threadfun(void *closure)
{
  
#define TID_LEN 24
  char threadid[TID_LEN];
  struct erl_thread_team_ctx *ctx = (struct erl_thread_team_ctx*)closure;

  /*check thread pinning and update ctx numa*/
  mstro_status status = erl_thread_team_bind_and_update_numa(ctx);
  if(status != MSTRO_OK)
  {
          WARN("Failed to pin the thread \n");
  }


  int n = snprintf(threadid, TID_LEN, "%s-H-%d-%zu",
                   ctx->tidprefix, ctx->numa_node, ctx->id);
  if(n>=TID_LEN) {
    WARN("threadid |%s-H-%d-%zu| truncated to %s\n",
         ctx->tidprefix, ctx->numa_node, ctx->id, threadid);
  }
  /*check and free already set thread descriptor*/
  char *tid = pthread_getspecific(g_thread_descriptor_key);
  if(tid!= NULL)
  {
	  free(tid);
  }
  tid = strdup(threadid); /* will be free()ed by cleanup of threadspecific */
  int s = pthread_setspecific(g_thread_descriptor_key, tid);
  if(s!=0) {
    ERR("Failed to set threadid %s: %d (%s)\n", threadid, s, strerror(s));
    goto BAILOUT;
  }

  DEBUG("%s handler thread %zu running\n", ctx->tidprefix, ctx->id);
  INFO("Running on CPU: %d\n", mstro_numa_get_cpu());

  do {
    
    /* this one will block if no data is available */
    void *data = mstro_fifo_dequeue(ctx->queue);
    DEBUG("Picked up item %p from queue %zu (%p)\n",
          data, ctx->id, ctx->queue);

    mstro_status stat = ctx->item_handler(data);
    if(stat == MSTRO_WOULDBLOCK)
    {
      /*re-insert the operation in the queue */
      stat = mstro_fifo_enqueue(ctx->queue,data);
      if(stat!=MSTRO_OK) {
        ERR("Failed to enqueue item %p (on queue %zu)\n", data, ctx->queue);
      } else {
        DEBUG("Enqueued item %p on queue %zu\n",
                        data, ctx->queue);
      }
    }
    else if(stat!=MSTRO_OK) {
      ERR("Failed to handle item: %d (%s)\n",
          stat, mstro_status_description(stat));
    }

    if(ctx->item_destructor) 
      ctx->item_destructor(data);

    pthread_testcancel();

  } while(1);

 BAILOUT:
  return NULL;
}

mstro_status
erl_thread_team_create_start_func(
    const char *team_name,
    const char *tidprefix,
    size_t num_threads,
    mstro_status(*item_handler)(void *item),
    void(*item_destructor)(void *item),
    void *(*start_routine)(void *),
    struct erl_thread_team **t_p)
{
  mstro_status stat=MSTRO_OK;
  if(t_p==NULL)
    return MSTRO_INVOUT;
  if(num_threads==0) 
    return MSTRO_INVARG;
  if(team_name==NULL || tidprefix==NULL)
    return MSTRO_INVARG;

  *t_p = malloc(sizeof(struct erl_thread_team)
                + (num_threads * sizeof(struct erl_thread_team_member)));
  if((*t_p)==NULL) {
    ERR("Failed to allocate %s thread team member handles\n", tidprefix);
    return MSTRO_NOMEM;
  }

  (*t_p)->teamname = strdup(team_name);
  if((*t_p)==NULL) {
    ERR("Failed to allocate team name\n");
    free(*t_p);
    *t_p = NULL;
    return MSTRO_NOMEM;
  }
  (*t_p)->num_members = num_threads;
  
  char *tidpfx = strdup(tidprefix);
  if(tidpfx==NULL) {
    ERR("Failed to allocate team name\n");
    free((*t_p)->teamname);
    free(*t_p);
    *t_p = NULL;
    return MSTRO_NOMEM;
  }
  (*t_p)->next_target = 0;
  
  for(size_t i=0; i<num_threads; i++) {
    /* round robin numa distribution */
    struct erl_thread_team_member *ti = &(*t_p)->members[i];
    struct erl_thread_team_ctx *ctx = &ti->ctx;
    ctx->tidprefix = tidpfx;
    ctx->id = i;
    ctx->numa_node = i%mstro_numa__num_nodes();
    ctx->item_handler = item_handler;
    ctx->item_destructor = item_destructor;
    if (start_routine == erl_thread_team_threadfun)
    {
      stat = mstro_fifo_init(&ctx->queue);
      if(stat!=MSTRO_OK) {
        ERR("Failed to init fifo for %s thread team handler thread %zu\n",
            tidprefix, i);
        abort();
      }
    }
    else
    {
      ctx->queue = NULL; /**we do not need a fifo queue */
    }

    int s = pthread_create(&ti->t, NULL, start_routine, &ti->ctx);
    if(s!=0) {
      ERR("Failed to start %s thread team handler thread %zu: %d (%s)\n",
          tidprefix, i, s, strerror(s));
      abort();
    }
    /* not detaching these threads; will cancel them to terminate and then
     * wait() before clearing ctx */
    DEBUG("Started %s thread team thread %zu\n", tidprefix, i);
  }
  return stat;
}


mstro_status
erl_thread_team_create(
    const char *team_name,
    const char *tidprefix,
    size_t num_threads,
    mstro_status(*item_handler)(void *item),
    void(*item_destructor)(void *item),
    struct erl_thread_team **t_p)
{
  return erl_thread_team_create_start_func(team_name,tidprefix,num_threads,item_handler,item_destructor,erl_thread_team_threadfun,t_p);
}


mstro_status
erl_thread_team_stop_and_destroy(struct erl_thread_team *t)
{
  mstro_status stat=MSTRO_OK;

  for(size_t i =0; i<t->num_members; i++) {
    int s = pthread_cancel(t->members[i].t);
    if(s!=0) {
      ERR("Failed to cancel %s thread team handler thread %zu: %d (%s)\n",
          t->teamname, i, s, strerror(s));
      stat=MSTRO_FAIL;
    }
  }
  for(size_t i =0; i<t->num_members; i++) {
    int s = pthread_join(t->members[i].t, NULL);
    if(s!=0) {
      ERR("Failed to join %s thread team handler thread %zu: %d (%s)\n",
          t->teamname, i, s, strerror(s));
      stat=MSTRO_FAIL;
    }
    mstro_fifo_finalize(t->members[i].ctx.queue);
    DEBUG("%s thread team handler thread %zu terminated\n",
          t->teamname, i);
  }
  free(t->members[0].ctx.tidprefix); // TIDPREFIX shared across all threads
  free(t->teamname);
  free(t);

  return stat;
}

/**Convenience function to be called for pm/pc threads, with not fully populated contexts (no fifo queues)*/
mstro_status
erl_thread_team_stop_and_destroy_bl(struct erl_thread_team *t)
{
  mstro_status stat=MSTRO_OK;

  for(size_t i =0; i<t->num_members; i++) {
    int s = pthread_join(t->members[i].t, NULL);
    if(s!=0) {
      ERR("Failed to join %s thread team handler thread %zu: %d (%s)\n",
          t->teamname, i, s, strerror(s));
      stat=MSTRO_FAIL;
    }
    DEBUG("%s thread team handler thread %zu terminated\n",
          t->teamname, i);
  }
  free(t->members[0].ctx.tidprefix); // TIDPREFIX shared across all threads
  free(t->teamname);
  free(t);

  return stat;
}

/** insert an item into the team's work schedule */
mstro_status
erl_thread_team_enqueue(struct erl_thread_team *team, void *item)
{
  if(team==NULL)
    return MSTRO_INVARG;

  size_t i = atomic_fetch_add(&team->next_target,1);

  i= i%team->num_members;

  mstro_status s = mstro_fifo_enqueue(team->members[i].ctx.queue,
                                      item);
  if(s!=MSTRO_OK) {
    ERR("Failed to enqueue item %p (on queue %zu) for team %s\n",
        item, i, team->teamname);
  } else {
    DEBUG("Enqueued item %p on queue %zu (%p) of team %s\n",
          item, i, team->members[i].ctx.queue, team->teamname);
  }
  return s;
}

/** insert an item into the team's work schedule - try select a thread that is on the same numa to improve locality */
mstro_status
erl_thread_team_enqueue_on_numa(struct erl_thread_team *team, int target_numa, void *item)
{
  size_t trials = 0;
  if(team==NULL)
    return MSTRO_INVARG;

  size_t i = atomic_fetch_add(&team->next_target,1);

  i= i%team->num_members;
  /**count #trials to avoid inf loops*/
  while((target_numa !=  team->members[i].ctx.numa_node) && (trials < team->num_members))
  {
    /**try next*/
    i = atomic_fetch_add(&team->next_target,1);
    i= i%team->num_members;
    trials++;
  }
  /**we either matched numa or are out of trials*/
  mstro_status s = mstro_fifo_enqueue(team->members[i].ctx.queue,
                                      item);
  if(s!=MSTRO_OK) {
    ERR("Failed to enqueue item %p (on queue %zu) for team %s\n",
        item, i, team->teamname);
  } else {
    DEBUG("Enqueued item %p on queue %zu (%p) of team %s\n",
          item, i, team->members[i].ctx.queue, team->teamname);
  }
  return s;
}


bool
erl_thread_team_memberp(const struct erl_thread_team *team, pthread_t thread_id)
{
  if(!team) {
    return false;
  }
  for(size_t i=0; i<team->num_members; i++) {
    if(team->members[i].t==thread_id)
      return true;
  }
  return false;
}
