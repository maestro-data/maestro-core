/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Core -- basic infrastructure implementation
 **/
/*
 * Copyright (C) 2019-2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/core.h"
#include "maestro/env.h"
#include "maestro/logging.h"

#include "maestro/i_globals.h"
#include "maestro/i_pool.h"
#include "maestro/i_memlock.h"


#include "erl_logging.h"
#include "erl_status.h"

#include "mamba.h"

#include "i_subscription_registry.h"
#include "i_heartbeat.h"

#include <pthread.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <assert.h>
#include <sys/resource.h>
#include <stdatomic.h>

#include "maestro/i_ofi.h"
#include "maestro/i_state.h"
#include "transport/transport.h"

#ifdef HAVE_NUMA_H
#include <numa.h>
#endif


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)


#define NFREE(x) do { if (x!=NULL) free(x); } while(0)

/** atexit handler should only ever be registered once per process run */
_Atomic(bool) g_atexit_registered = false;

/** atexit handler */
void
mstro_core__atexit(void)
{
  /* any component that needs atexit cleanup should add their handler here */
  mstro_status s = mstro_ofi__atexit();
  if(s!=MSTRO_OK) {
    ERR("Failure of mstro_ofi__atexit(): %d (%s)\n",
        s, mstro_status_description(s));
  }
}

/* init status is kept globally, see i_globals.h */

/** function called at thread termination time if the initdata key
 * still has a non-NULL value */
static void
mstro_i_destroy_initdata(void* initdata)
{
  struct mstro_core_initdata *data = (struct mstro_core_initdata*)initdata;

  ERR("Thread %" PRIxPTR " terminating before it called mtro_core_finalize()\n",
      (intptr_t)pthread_self);
  NFREE(data->workflow_name);
  NFREE(data->component_name);
  NFREE(data);
  return;
}

/*call from mstro_core_numa_init to initialize g_numa_XX_bind_cores and g_numa_XX_bind_cores_len from MSTRO_ENV_BIND_XXXX 
 * we might replace this function with numa_parse_cpustring() to give us directly a cpumask*/
static inline
mstro_status
mstro_core__numa_parse_user_bind_threads(int **cores, int *len,const char *cores_binding_env)
{
  mstro_status status = MSTRO_UNIMPL;
  /**read user binding for cq thread */
  char *char_cores = getenv(cores_binding_env);
  if (char_cores != NULL)
  {
    /*parse list of int in the form 1,2,3 or 1-3 */
    /*FIXME allow user to pass also a strategy, such as local or spread, to in all threads to same NUMA node
    * or spread threads across NUMAs*/

    /*find out first if the list is comma separated or a range - */
    if(strchr(char_cores, ','))
    {
      /*comma separated list*/
		  /* count the commas*/
			char *s = char_cores;
			int i;
			for (i=0; s[i]; s[i]==',' ? i++ : *s++);
			*len = i+1; /*#cores = #commas+1*/
			*cores = malloc((*len)*sizeof(int));
			/* fill the array */
			s = char_cores;
			for(int i=0; i< *len; i++)
			{
				(*cores)[i] = strtol(s,&s, 10); 
				s++;/*jump ','*/
			}
			status = MSTRO_OK;
    }
    else if(strchr(char_cores, '-'))
    {
      /*range of cores*/
      char * pEnd; // rest of string
      int num1 = strtol(char_cores,&pEnd, 10);
      int num2 = strtol(pEnd+1,&pEnd, 10); /*jump '-' character */ 
			if(num2 < num1)
			{
				return MSTRO_FAIL;
			}
		  *len = num2 - num1 +1;
			*cores = malloc((*len)*sizeof(int));
			/* fill the array */
			for(int i=0; i< *len; i++)
			{
				(*cores)[i] = num1+i;
			}
			status = MSTRO_OK;
    }
    else /* not a list, take the first number */
    {
      *len = 1;
      *cores = malloc(sizeof(int));
      (*cores)[0] = atoi(char_cores);
      status = MSTRO_OK;
    }
  }
  else
  {
  /*User did not set any binding env*/
  *cores = NULL;
  *len = 0;
  status = MSTRO_OK;
  }
  return status;
}

static inline
mstro_status
mstro_core__numa_init(void)
{
#ifdef HAVE_NUMA
  g_have_numa = (numa_available()==-1 ? false : true);
  if(g_have_numa) {
	  g_numa_configured_cpus = numa_num_configured_cpus();
	  g_numa_configured_nodes = numa_num_configured_nodes();
	  DEBUG("NUMA: available with %d/%d nodes, %d/%d cpus\n",
			  numa_num_task_nodes(), g_numa_configured_nodes,
			  numa_num_task_cpus(), g_numa_configured_cpus);
    /*parse MSTRO_ENV_BIND_TRANSPORT_THREAD here to populate cores list */
    mstro_status s= mstro_core__numa_parse_user_bind_threads(&g_numa_transport_bind_cores, &g_numa_transport_bind_cores_len,MSTRO_ENV_BIND_TRANSPORT_THREAD);
    if(s != MSTRO_OK) 
    {
	     ERR("Failed to parse %s environment variable\n", MSTRO_ENV_BIND_TRANSPORT_THREAD);
	     return s;
    }
    /*parse MSTRO_ENV_BIND_PM_PC here to populate cores list */
    s= mstro_core__numa_parse_user_bind_threads(&g_numa_pm_pc_bind_cores, &g_numa_pm_pc_bind_cores_len,MSTRO_ENV_BIND_PM_PC);
    if(s != MSTRO_OK) 
    {
	     ERR("Failed to parse %s environment variable\n", MSTRO_ENV_BIND_PM_PC);
	     return s;
    }
    /*parse MSTRO_ENV_BIND_OP_THREAD here to populate cores list */
    s= mstro_core__numa_parse_user_bind_threads(&g_numa_op_bind_cores, &g_numa_op_bind_cores_len,MSTRO_ENV_BIND_OP_THREAD);
    if(s != MSTRO_OK) 
    {
	     ERR("Failed to parse %s environment variable\n", MSTRO_ENV_BIND_OP_THREAD);
	     return s;
    }
    /* FIXME: consider initializing some global data structures with this info */
  }
#else
  g_have_numa = false;
  DEBUG("NUMA: not supported\n");
#endif
  return MSTRO_OK;
}

static inline
mstro_status
mstro_core__mamba_init(void)
{
  mmbOptions *mamba_init_opts=NULL;
  mmbError stat = mmb_options_create_default(&mamba_init_opts);
  if(stat != MMB_OK) {
    ERR("Failed to create default options for mamba\n");
    goto BAILOUT;
  }
  stat = mmb_options_set_user_log_func(mamba_init_opts, &mstro_mamba_logging_bridge);
  if(stat != MMB_OK) {
    ERR("Failed to set debug option for mamba\n");
    goto BAILOUT;
  }

  stat = mmb_options_set_user_log_func(mamba_init_opts, &mstro_mamba_logging_bridge);
  if(stat != MMB_OK) {
    ERR("Failed to set debug option for mamba\n");
    goto BAILOUT;
  }
  
  erl_logging_options *erl_init_opts=NULL;
  erl_status erl_stat = erl_logging_options_create_default(&erl_init_opts);
  if(erl_stat != ERL_OK) {
    ERR("Failed to create default logging options for libERL\n");
    goto BAILOUT;
  }

  erl_stat =  erl_logging_options_set_user_log_func(erl_init_opts, mstro_liberl_logging_bridge);
  if(erl_stat != ERL_OK) {
    ERR("Failed to set logging options for libERL\n");
    goto BAILOUT;
  }

  erl_stat =  erl_logging_init(erl_init_opts);
  if(erl_stat != ERL_OK) {
    ERR("Failed to set logging bridge for libERL\n");
    goto BAILOUT;
  }

  stat = mmb_init(mamba_init_opts);
  if(stat != MMB_OK) {
    ERR("Failed to initialise mamba\n");
    goto BAILOUT;
  }

  int mmb_has_hwloc;
  stat = mmb_discovery_is_enabled(&mmb_has_hwloc);
  if(stat != MMB_OK) {
    ERR("Failed to obtain mamba discovery status\n");
    goto BAILOUT;
  }

  mmbMemSpaceConfig dram_config = {
    .size_opts = {
      .action = MMB_SIZE_SET,
      // FIXME: use some external resource limit
      .mem_size = SIZE_MAX, /* at this time we're not artificially
                             * limiting the space; mamba does not do
                             * aggressive pre-allocation either */
    },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };
  
  if(mmb_has_hwloc) {
    DEBUG("Built-in mamba compiled with hwloc resource discovery\n");
    mmb_dump_memory_state(stderr);
    fflush(stderr);
    
    dram_config.size_opts.action = MMB_SIZE_ANY; // see mamba issue #101
    stat = mmb_request_space(MMB_DRAM,
                             MMB_EXECUTION_CONTEXT_DEFAULT,
                             &dram_config,
                             &g_default_cdo_memspace);
    if(stat != MMB_OK) {
      ERR("Failed to request default DRAM memory for mamba: %d (%s)\n",
          stat, mmb_error_description(stat));
      goto BAILOUT;
    }
  } else {
    DEBUG("Built-in mamba compiled without hwloc resource discovery\n");
    // cannot request more than discovered, so adapt our requested value:
    stat = mmb_register_memory(MMB_DRAM,
                               MMB_EXECUTION_CONTEXT_DEFAULT,
                               &dram_config,
                               &g_default_cdo_memspace);
    if(stat != MMB_OK) {
      ERR("Failed to register some DRAM memory for mamba: %d (%s)\n",
          stat, mmb_error_description(stat));
      goto BAILOUT;
    }
  }

  stat = mmb_request_interface(g_default_cdo_memspace,
                            NULL,
                            &g_default_cdo_interface);
  if(stat!=MMB_OK) {
    ERR("Failed to retrieve suitable dram memory interface\n");
    goto BAILOUT;
  }

BAILOUT:
  if(mamba_init_opts)
    mmb_options_destroy(mamba_init_opts);
  if(erl_init_opts)
    erl_logging_options_destroy(erl_init_opts);

  if(stat!=MMB_OK)
    return MSTRO_FAIL;
  else
    return MSTRO_OK;
}


/** minimum mlock() limit */
#define MSTRO_MIN_MEMLOCK (4*sizeof(g_component_descriptor))


mstro_status mstro_core_init__setup_schemata(void)
{

  mstro_status status = MSTRO_OK;

  char * env_schema_list = getenv(MSTRO_ENV_SCHEMA_LIST);
  char * env_schema_path = getenv(MSTRO_ENV_SCHEMA_PATH);
  char *env_schema_path_plus_default = NULL;

  //check that neither are null ...if null make them empty strings
  if(env_schema_list == NULL) {
    env_schema_list = "";
  } else {
    INFO("List of user attributes schemata %s \n", env_schema_list);
    strncpy(g_component_descriptor.schema_list, env_schema_list, MSTRO_WORKFLOW_NAME_MAX-1);
    g_component_descriptor.schema_list[MSTRO_WORKFLOW_NAME_MAX-1] = '\0';
  }

  if(env_schema_path == NULL) {
    env_schema_path = "";
  }

  char *schema_list_token;
  char *schema_path_token;


  // parse and merge the builtin schemas first
  DEBUG("Parsing Maestro core schema\n");
  status=mstro_schema_parse(MSTRO_SCHEMA_BUILTIN_YAML_CORE,
                            MSTRO_SCHEMA_BUILTIN_YAML_CORE_LEN,
                            &g_mstro_core_schema_instance);
  if(status!=MSTRO_OK) {
    ERR("Failed to parse built-in core schema\n");
    goto BAILOUT;
  }

  DEBUG("Parsing ECMWF schema\nFIXME for 0.3.0 ... ECMWF schema shoud not be loaded by default\n");
  mstro_schema ecmwf;
  status=mstro_schema_parse(MSTRO_SCHEMA_BUILTIN_YAML_ECMWF,
                            MSTRO_SCHEMA_BUILTIN_YAML_ECMWF_LEN,
                            &ecmwf);
  if(status!=MSTRO_OK) {
    ERR("Failed to parse built-in ecmwf schema\n");
    goto BAILOUT;
  }
  status=mstro_schema_merge(g_mstro_core_schema_instance, ecmwf);
  if(status!=MSTRO_OK) {
    ERR("Failed to merge core and ECMWF schema\n");
    goto BAILOUT;
  }

   // start reading user-defined schemas and merge them.
   char *end_list_token;
   char *end_path_token;
   char *schema_full_path;

   // the lenght of all paths =  length of paths exported by user + separator + default path "."
   int path_len = strlen(env_schema_path) + 2 + 2;
   env_schema_path_plus_default = malloc(path_len*sizeof(char));
   if(env_schema_path_plus_default==NULL) {
     ERR("Failed to allocate schema path tempspace\n");
     status=MSTRO_NOMEM;
     goto BAILOUT;
   }

   /* get the first schema */
   schema_list_token = strtok_r(env_schema_list, SCHEMA_LIST_SEP, &end_list_token);
   DEBUG("first schema_list_token: %s \n", schema_list_token);

   /* walk through other tokens */
   while( schema_list_token != NULL ) {
      // parse a schema from the user
      mstro_schema user_schema;
      DEBUG("looking for schema_list_token: %s \n", schema_list_token);

      // creating list of paths to visit. i.e. current directory "." + user defined paths
      snprintf(env_schema_path_plus_default, path_len, "%s%s%s", ".", SCHEMA_PATH_SEP, env_schema_path);
      DEBUG("list of paths for schemas: %s\n", env_schema_path_plus_default);
      /* get the first path */
      schema_path_token = strtok_r(env_schema_path_plus_default, SCHEMA_PATH_SEP, &end_path_token);
      DEBUG("first schema_path_token: %s\n", schema_path_token);

      /* walk through other paths */
      while( schema_path_token != NULL ) {
        // forming the full path:          path token + / + list name + '\0'
        int schema_full_path_len = strlen(schema_path_token) + 2 + strlen(schema_list_token) + 2;
        schema_full_path = (char *) malloc(sizeof(char)*schema_full_path_len);
        snprintf(schema_full_path, schema_full_path_len, "%s%s%s", schema_path_token, "/", schema_list_token);
        DEBUG("Parsing user-defined schema from %s\n", schema_full_path);
        status=mstro_schema_parse_from_file(schema_full_path, &user_schema);

        if(status==MSTRO_OK) {
          DEBUG("user-defined schema is read from %s\n", schema_full_path);
          free(schema_full_path);
          schema_full_path = NULL;
          break; // no need to try other paths
        }

        free(schema_full_path);
        schema_full_path = NULL;
        // read the next path
          schema_path_token = strtok_r(NULL, SCHEMA_PATH_SEP, &end_path_token);

      }

      if(status!=MSTRO_OK) {
        ERR("Failed to parse user_schema from file: %s \n", schema_list_token);
	goto BAILOUT;
      }
      // merge the schema
      DEBUG("Merging user schema: %s\n", schema_list_token);
      status=mstro_schema_merge(g_mstro_core_schema_instance, user_schema);


      if(status!=MSTRO_OK) {
        ERR("Failed to merge core and user schema from file %s\n", schema_list_token);
	goto BAILOUT;
      }
      // read the next schema name
      schema_list_token = strtok_r(NULL, SCHEMA_LIST_SEP, &end_list_token);
   }

BAILOUT:
  if(env_schema_path_plus_default!=NULL)
    free(env_schema_path_plus_default);

  return  status;
}

/** the number of RLIMIT_ values in the table of @ref MSTRO_DEFAULT_RLIMIT definitions */
#define MSTRO_DEFAULT_RLIMIT_LEN 2

/** the list of minimal RLIMIT_ values maestro-core needs to be happy */
struct {
  const char* name;          /**< name of resource */
  int         resource;      /**< the RLIMIT_ constant to designate the resource */
  rlim_t      desired_val;   /**< the value we believe maestro-core needs at least */
} MSTRO_DEFAULT_RLIMIT[MSTRO_DEFAULT_RLIMIT_LEN] = {
  {"RLIMIT_NOFILE",  RLIMIT_NOFILE,  1024},
  {"RLIMIT_MEMLOCK", RLIMIT_MEMLOCK, MSTRO_MIN_MEMLOCK},
};

static inline
void
mstro_core__adapt_rlimits(void)
{
  // check some typical limits and try to increase (or warn)

  for(size_t i=0; i<MSTRO_DEFAULT_RLIMIT_LEN; i++) {
    struct rlimit rlp;

    int s = getrlimit(MSTRO_DEFAULT_RLIMIT[i].resource, &rlp);
    if(s!=0) {
      ERR("Failed to query %s value: %d (%s)\n",
          MSTRO_DEFAULT_RLIMIT[i].name, s, strerror(s));
    } else {
      DEBUG("%s: soft %zu, hard %zu\n",
            MSTRO_DEFAULT_RLIMIT[i].name,
            (uintmax_t)rlp.rlim_cur,
            (uintmax_t)rlp.rlim_max);
      if(rlp.rlim_cur<MSTRO_DEFAULT_RLIMIT[i].desired_val) {
        WARN("%s: soft limit %zu is lower than desired value %zu, trying to increase it.\n",
             MSTRO_DEFAULT_RLIMIT[i].name,
             (uintmax_t)rlp.rlim_cur,
             (uintmax_t)MSTRO_DEFAULT_RLIMIT[i].desired_val);
        if(rlp.rlim_max<MSTRO_DEFAULT_RLIMIT[i].desired_val) {
          WARN("%s: hard limit %zu is lower than desired value %zu, cannot increase it. Proceeding with fingers crossed.\n",
               MSTRO_DEFAULT_RLIMIT[i].name,
               (uintmax_t)rlp.rlim_max,
               (uintmax_t)MSTRO_DEFAULT_RLIMIT[i].desired_val);
        } else {
          rlp.rlim_cur = MSTRO_DEFAULT_RLIMIT[i].desired_val;
          s=setrlimit(MSTRO_DEFAULT_RLIMIT[i].resource, &rlp);
          if(s!=0) {
            ERR("Failed to raise soft limit of %s to %zu: %d (%s)\n",
                MSTRO_DEFAULT_RLIMIT[i].name,
                (uintmax_t)rlp.rlim_cur,
                s, strerror(s));
          } else {
            DEBUG("%s raised to %zu\n",
                  MSTRO_DEFAULT_RLIMIT[i].name, (uintmax_t)rlp.rlim_cur);
          }
        }
      }
    }
  }
}


/** indicator for earliest entry into init phase, reset as last step in finalize: */
static _Atomic(bool) g_core_init = false;

mstro_status
mstro_core_init(const char *workflow_name,
                const char *component_name,
                uint64_t component_index)
{
  mstro_status status = MSTRO_UNIMPL;

  bool ival = atomic_exchange(&g_core_init, true);
  if(ival) {
    ERR("Init called twice without finalization in-between.\n");
    return MSTRO_NOT_TWICE;
  }

  mstro_core__adapt_rlimits();
  
  status = mstro_memlock_init(MSTRO_MIN_MEMLOCK);
  if(status!=MSTRO_OK) {
    return status;
  }

  if(atomic_exchange(&g_atexit_registered, true)==false) {
    /* once only */
    int s = atexit(mstro_core__atexit);
    if(s!=0) {
      ERR("Failed to register atexit handler: %d (%s)\n", s, strerror(s));
      return MSTRO_FAIL;
    }
  }

  struct mstro_core_initdata *data
      = malloc(sizeof(struct mstro_core_initdata));

  if(data) {
    data->workflow_name = strdup(workflow_name);

    data->component_name = strdup(component_name);

    data->component_index = component_index;

    /* sanity check */
    if(data->workflow_name==NULL
       || data->component_name==NULL) {
      NFREE(data->workflow_name);
      NFREE(data->component_name);
      free(data);
      return MSTRO_NOMEM;
    }
  }

  mstro_status stat = mstro_core_heartbeat_init();
  if(stat!=MSTRO_OK) {
    return MSTRO_FAIL;
  }

  /* sanity check if someone changes the NAME_MAX or adds new component sizes */
  assert(sizeof(g_component_descriptor)==2U<<14);

  g_component_descriptor.type = MSTRO_COMPONENT_TYPE_UNDEF; /* will be upgraded at pc_thread start and pm_thread start */
  g_component_descriptor.protocol_version.major = MSTRO_POOL_PROTOCOL_VERSION_MAJOR;
  g_component_descriptor.protocol_version.minor = MSTRO_POOL_PROTOCOL_VERSION_MINOR;
  g_component_descriptor.protocol_version.patch = MSTRO_POOL_PROTOCOL_VERSION_PATCH;
  strncpy(g_component_descriptor.workflow_name, data->workflow_name, MSTRO_WORKFLOW_NAME_MAX-1);
  g_component_descriptor.workflow_name[MSTRO_WORKFLOW_NAME_MAX-1] = '\0';
  strncpy(g_component_descriptor.component_name, data->component_name, MSTRO_WORKFLOW_NAME_MAX-1);
  g_component_descriptor.component_name[MSTRO_WORKFLOW_NAME_MAX-1] = '\0';
  strcpy(g_component_descriptor.version, mstro_version());

  /* check GFS path setting */
  {
    char *gfs_dir = getenv(MSTRO_ENV_TRANSPORT_GFS_DIR);
    if(gfs_dir!=NULL) {
      bool need_sep = false;
      size_t l1 = strlen(gfs_dir);
      assert(l1>0);
      if(gfs_dir[l1-1]!='/') {
        need_sep=true;
      }
      size_t l2 = strlen(data->workflow_name);
      size_t l3 = strlen(data->component_name);

      g_mstro_transport_gfs_dir = malloc(l1 + (need_sep? 1 : 0)
                                         + l2 + 1
                                         + l3 + 1 +1);
      if(g_mstro_transport_gfs_dir==NULL) {
        ERR("Failed to allocate GFS transport dir pathname\n");
        return MSTRO_NOMEM;
      }
      size_t pos = 0;
      strcpy(g_mstro_transport_gfs_dir, gfs_dir);
      pos += l1;
      if(need_sep) {
        g_mstro_transport_gfs_dir[pos] = '/';
        pos++;
      }
      strcpy(g_mstro_transport_gfs_dir+pos, data->workflow_name);
      pos += l2;

      g_mstro_transport_gfs_dir[pos] = '/';
      pos++;

      strcpy(g_mstro_transport_gfs_dir+pos, data->component_name);
      pos += l3;
      g_mstro_transport_gfs_dir[pos] = '/';
      pos++;
      g_mstro_transport_gfs_dir[pos] = '\0';
      g_mstro_transport_gfs_dir_len = pos;
      assert(pos==strlen(g_mstro_transport_gfs_dir));

      DEBUG("Set GFS transport directory to %s\n",
            g_mstro_transport_gfs_dir);
    }
  }

  /*Reading and integrating attribute schemas*/
  status = mstro_core_init__setup_schemata();
  if(status!=MSTRO_OK) {
    goto BAILOUT;
  }

  DEBUG("init %s/%s/% "PRIi64 " in thread %" PRIxPTR" complete\n",
        data->workflow_name, data->component_name, data->component_index,
        (intptr_t)pthread_self());

  pthread_mutex_lock(&g_initdata_mtx);
  if(g_initdata==NULL) {
    g_initdata=data;
    status = MSTRO_OK;
  } else {
    ERR("ERROR: Thread %" PRIxPTR " calling mstro_core_init again\n",
        (intptr_t)pthread_self());
    status = MSTRO_NOT_TWICE;
    /*release lock and exit*/
    pthread_mutex_unlock(&g_initdata_mtx);
    goto BAILOUT;
  }
  pthread_mutex_unlock(&g_initdata_mtx);

  status = mstro_core__numa_init();
  if(status!=MSTRO_OK) {
    ERR("Failed to initialize NUMA support infrastructure\n");
    goto BAILOUT;
  }

  status = mstro_core__mamba_init();
  if(status!=MSTRO_OK) {
    goto BAILOUT;
  }
  NOISE("Initialized memory subsystem %s\n", mmb_version());

  status = mstro_subscriptions_init();
  if(status!=MSTRO_OK) {
    ERR("Failed to initialize subscription subsystem\n");
    goto BAILOUT;
  }

  /* transport init */
  status = mstro_transport_init();
  if (! (MSTRO_OK == status)) {
	/* Already printed an error */
	  goto BAILOUT;
  }
  DEBUG("Transport initalized\n");

  /* Start local pool */
  status = mstro_pool_init();
  if(status!=MSTRO_OK) {
    ERR("Failed to initialize pool: %d (%s)\n",
        status, mstro_status_description(status));
    ERR("If your job terminates after this error please check\n"
        " ... that firewalling is not prohibiting libfabric communication\n"
        " ... that ulimit -n is high enough to keep a couple of FDs per network endpoints open (ulimit -n 1024 is a good start)\n"
	" ... that FI_OFI_RXM_USE_SRX=1 and FI_VERBS_PREFER_XRC=0 with large MPI applications (>=64 ranks) when using verbs provider, especially with Cray MPICH\n"
	" ... that your workflow-name/component-name/component-index triple is correct\n");
    goto BAILOUT;
  }

  {
    WARN("Guessing absolute maximum size of an (encoded) transport ticket, FIXME\n");
    size_t implicit_transport_inline_limit = MSTRO_MSG_SHORT_SIZE - 200; /* rough guess estimating the size of a packed transport ticket */
    g_transport_inline_max = implicit_transport_inline_limit;
  }
  const char *have_transport_inline_limit = getenv(MSTRO_ENV_TRANSPORT_INLINE_MAX);
  if(have_transport_inline_limit) {
    int maxval = atoi(have_transport_inline_limit);
    if(maxval<0) {
      ERR("Invalid specification for %s\n", MSTRO_ENV_TRANSPORT_INLINE_MAX);
    } else {
      if((size_t)maxval > g_transport_inline_max) {
        ERR("Limit specified for inline transport exceeds implicit limit of %zu (consider changing %s definition)\n",
            g_transport_inline_max, "MSTRO_POOL_MSG_SMALL_MAX");
      } else {
        g_transport_inline_max = maxval;
      }
    }
  }
  INFO("Inline CDO transport limit is %zu bytes (or less)\n", g_transport_inline_max);
  
  /* maybe start pool manager */
  const char *want_pm_start=getenv(MSTRO_ENV_START_POOL_MANAGER);
  if(want_pm_start) {
    if(strcmp(want_pm_start,"1")==0
       ||strcasecmp(want_pm_start,"yes")==0
       ||strcasecmp(want_pm_start,"y")==0) {
      INFO("Auto-starting pool manager\n");
      status = mstro_pm_start();
      if(status!=MSTRO_OK) {
        ERR("Failed to auto-start pool manager\n");
      }
    }
  }

BAILOUT:
  if(status!=MSTRO_OK) {
    NFREE(data->workflow_name);
    NFREE(data->component_name);
    NFREE(data);
    g_initdata=NULL;
    if(g_mstro_core_schema_instance!=NULL) {
      if(MSTRO_OK != mstro_schema_free(g_mstro_core_schema_instance))
      {
        ERR("Failed to deallocate attribute schema\n");
      }
      g_mstro_core_schema_instance=NULL;
    }
  }
  return status;
}

mstro_status
mstro_core_finalize(void)
{
  mstro_status status = MSTRO_UNIMPL;
  // need to try running all cleanups, even if parts fail, so many
  // status codes to be collected:
  mstro_status
      s_sub=MSTRO_OK, s_transp=MSTRO_OK,
      s_pm=MSTRO_OK, s_pool=MSTRO_OK, s_hb=MSTRO_OK,
      s_schema=MSTRO_OK, s_mamba=MSTRO_OK, s_erl=MSTRO_OK, s_memlock=MSTRO_OK;
  mmbError x = MMB_OK;

  if(g_initdata==NULL) {
    /* in case someone calls finalize before init: */
    ERR("mstro_core_finalize() called before mstro_core_init()\n");
    status = MSTRO_FAIL;
  } else {
    DEBUG("finalize %s/%s/%"PRIi64 " in thread %" PRIxPTR "\n",
          g_initdata->workflow_name,
          g_initdata->component_name,
          g_initdata->component_index,
          (intptr_t)pthread_self());
    status = MSTRO_OK;
  }

  /* ensure there are no outstanding subscriptions before we detach from PM */
  /* (for cleanliness, but also to ensure that a local LEAVE
   * subscription would come in for our own LEAVE request) */
  s_sub = mstro_subscriptions_finalize();
  if(s_sub!=MSTRO_OK) {
    ERR("Failed to shut down subscription subsystem: %d (%s)\n",
        s_sub, mstro_status_description(s_sub));
  }

  s_transp = mstro_transport_finalize();
  if (s_transp != MSTRO_OK) {
    ERR("Failed to shutdown transport layer: %d (%s)\n",
        s_transp, mstro_status_description(s_transp));
  }
  DEBUG("Transport finalized\n");

  const char *wanted_pm_start=getenv(MSTRO_ENV_START_POOL_MANAGER);
  if(wanted_pm_start) {
    if(strcmp(wanted_pm_start,"1")==0
       ||strcasecmp(wanted_pm_start,"yes")==0
       ||strcasecmp(wanted_pm_start,"y")==0) {
      INFO("Auto-stopping pool manager\n");
      s_pm = mstro_pm_terminate();
      if(s_pm!=MSTRO_OK) {
        ERR("Failed to auto-stop pool manager: %d (%s)\n",
            s_pm, mstro_status_description(s_pm));
      }
    }
  }

  /* at this time the pool manager will either have been reassigned to
   * a different destination (if distributed PM is implemented), or
   * we'll run PM-less */
  s_pool = mstro_pool_finalize();
  if(s_pool!=MSTRO_OK) {
    ERR("Pool finalization failed: %d (%s)\n",
        s_pool, mstro_status_description(s_pool));
  }

  s_hb = mstro_core_heartbeat_finalize();
  if(s_hb!=MSTRO_OK) {
    ERR("Heartbeat finalization failed: %d (%s)\n",
        s_hb, mstro_status_description(s_hb));
    goto BAILOUT;
  }
  /**free allocated numa cores lists for pinning*/
  NFREE(g_numa_transport_bind_cores);
  NFREE(g_numa_pm_pc_bind_cores);
  NFREE(g_numa_op_bind_cores);

  if(g_initdata) {
    /* had some initdata */
    pthread_mutex_lock(&g_initdata_mtx);

    NFREE(g_initdata->workflow_name);
    NFREE(g_initdata->component_name);
    free(g_initdata);
    g_initdata = NULL;
    pthread_mutex_unlock(&g_initdata_mtx);
    
    /* all live schemata should be under this one.  FIXME: The active
     * schema should be part of the initdata, not a global variable like
     * this one.
     */
    if(g_mstro_core_schema_instance!=NULL) {
      s_schema = mstro_schema_free(g_mstro_core_schema_instance);
      if(s_schema!=MSTRO_OK) {
        ERR("Failed to deallocate attribute schema: %d (%s)\n",
            s_schema, mstro_status_description(s_schema));
      }
      g_mstro_core_schema_instance=NULL;
    }
  }

  x = mmb_finalize();
  if(x!=MMB_OK) {
    ERR("Failed to shut down Mamba: %d (%s)\n",
        x, mmb_error_description(x));
    s_mamba = MSTRO_FAIL;
  }

  erl_status es = erl_logging_finalize();
  if(es!=ERL_OK) {
    ERR("Failed to shut down libERL logging: %d\n", es);
    s_erl = MSTRO_FAIL;
  }

  s_memlock = mstro_memlock_finalize();
  if(s_memlock!=MSTRO_OK) {
    ERR("Failed to shut down memlock subsystem: %d (%s)\n",
        s_memlock, mstro_status_description(s_memlock));
  }

BAILOUT:
  g_core_init = false;
  status =  status
      || (s_sub || s_transp || s_pm || s_pool || s_hb || s_schema
          || s_mamba || s_erl || s_memlock);
  return status;
}


mstro_status
mstro_core_state_get(const struct mstro_core_initdata **res)
{
  mstro_status status=MSTRO_UNIMPL;
  const struct mstro_core_initdata *ptr;
  int s=pthread_mutex_lock(&g_initdata_mtx);
  if(s!=0) {
    ptr=NULL;
    status=MSTRO_FAIL;
  } else {
    ptr = g_initdata;
    s =pthread_mutex_unlock(&g_initdata_mtx);
    if(s!=0) {
      status = MSTRO_FAIL;
    } else {
      status = MSTRO_OK;
    }
  }
  *res = ptr;
  return status;
}




/* timing stuff */
#if defined(__linux)
#  define HAVE_POSIX_TIMER
#  include <time.h>
#  ifdef CLOCK_MONOTONIC
#     define CLOCKID CLOCK_MONOTONIC
#  else
#     define CLOCKID CLOCK_REALTIME
#  endif
  static struct timespec linux_rate;
#elif defined(__APPLE__)
#  include <time.h>
#  define HAVE_MACH_TIMER
#  ifdef CLOCK_MONOTONIC
#     define CLOCKID CLOCK_MONOTONIC
#  else
#     define CLOCKID CLOCK_REALTIME
#  endif
#  include <mach/mach_time.h>
   static mach_timebase_info_data_t info;
#elif defined(_WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
   static LARGE_INTEGER win_frequency;
#else
#error Unsupported system class
#endif

mstro_nanosec_t
mstro_clock(void)
{
#if defined(__APPLE__)
  mstro_nanosec_t now = clock_gettime_nsec_np(CLOCKID);
    /* now = mach_absolute_time(); */
    /* now *= info.numer; */
    /* now /= info.denom; */
    return now;
#elif defined(__linux)
    mstro_nanosec_t now;
    struct timespec spec;
    clock_gettime(CLOCKID, &spec);
    now = spec.tv_sec * 1.0e9 + spec.tv_nsec;
    return now;
#elif defined(_WIN32)
    LARGE_INTEGER now;
    QueryPerformanceCounter(&now);
    return (uint64_t) ((1e9 * now.QuadPart)  / win_frequency.QuadPart);
#else
    #error Unsupported system class
#endif
}
