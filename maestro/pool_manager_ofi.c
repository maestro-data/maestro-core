/* -*- mode:c -*- */
/** @file
 ** @brief Maestro Pool Manager component -- OFI based implementation
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/status.h"
#include "maestro/pool_manager.h"
#include "maestro/i_state.h"
#include "maestro/logging.h"
#include "maestro/i_ofi.h"
#include "maestro/i_globals.h"
#include "maestro/logging.h"

#include "maestro/i_event.h"
#include "i_maestro_numa.h"
#include <inttypes.h>
#include <unistd.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <pthread.h>
#include <assert.h>
#include "i_thread_team.h"
#include "maestro/i_ofi_threads.h"

/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_PM,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_PM,__VA_ARGS__)


/* internal entry point: started in a pthread, runs PM loop until it
 * is told to terminate (when g_pm_stop is set) */

_Atomic bool g_pm_stop; /* static init 0 means: no */

static _Atomic bool g_pm_transport_init_thread_started = false;
/**PM OFI independent threads handle*/
struct mstro_ofi_thread_team *g_pm_ofi_team = NULL;


static void*
mstro_pm_transport_init_thread_fun(void *closure)
{
  mstro_status s;	
  assert(closure==NULL);
  g_pm_transport_init_thread_started = true;

  DEBUG("PM transport initiator thread starting\n");
  /* check pinning of transport thread */
  s = mstro_numa_bind_thread("transport");
  INFO("Running on CPU: %d\n", mstro_numa_get_cpu());
  while(!atomic_load(&g_pm_stop)) {
    s = mstro_pm_handle_demand_queue();
    if(s!=MSTRO_OK) {
      ERR("Error in demand queue runner: %d (%s)\n",
          s, mstro_status_description(s));
    }
    pthread_testcancel();
  }

  DEBUG("PM transport initiator thread stopping.\n");

  return NULL;
}


/* User-side entry point: dedicate current thread to pool manager work */
mstro_status
mstro_pm_start(void)
{
  mstro_status stat = MSTRO_UNIMPL;
  int ret;
  /* if(!(g_pool_manager_size>=1)) { */
  /*   return MSTRO_INVARG; */
  /* } */

  /* if(g_pool_manager_size!=1) { */
  /*   WARN("DEFICIENCY: Cannot run distributed pool manager yet\n"); */
  /*   return MSTRO_UNIMPL; */
  /* } */

  const struct mstro_core_initdata *d;
  stat = mstro_core_state_get(&d);

  if(stat!=MSTRO_OK) {
    ERR("Failed to get initdata: %d\n", stat);
    return stat;
  }
  
  if(d==NULL) {
    ERR("Trying to start pool manager before mstro_init()\n");
    stat=MSTRO_FAIL;
    return stat;
  }
  
  /* INFO("Starting Pool Manager for %s," */
  /*      "expecting %" PRIi64 " partners\n", */
  /*      d->workflow_name, g_pool_manager_size); */
  INFO("Starting Pool Manager for workflow %s\n",
       d->workflow_name);

  stat = mstro_pm_reg_init();
  if(stat!=MSTRO_OK) {
    ERR("Failed to initialize PM registry\n");
    return stat;
  }
  
  /* organize quorum for pool_manager_size */
  
  /* create connection */

  /* create openfabric endpoint on all interfaces, listen on them,
   * create advertisable destination string */

  stat = mstro_ofi_init();  
  if(stat!=MSTRO_OK) {
     ERR("mstro_ofi_init() failed\n");
     return stat;
  }
  
  /* configure global app ID for manager role */
  g_pool_app_id = MSTRO_APP_ID_MANAGER;
  g_pool_appid.id = g_pool_app_id;
  g_pool_apptoken.appid = &g_pool_appid;
  g_component_descriptor.type = MSTRO_COMPONENT_TYPE_PM;

  #define DEFAULT_OFI_NUM_THREADS 1
  char *e = getenv(MSTRO_ENV_PM_PC_NUM_THREADS);
  int num_pm_threads;
  
  if(e!=NULL) {
    num_pm_threads = atoi(e);
    if(num_pm_threads<DEFAULT_OFI_NUM_THREADS) {
      ERR("Value of %s=%e illegal, using %d\n", DEFAULT_OFI_NUM_THREADS);
    }
  } else {
      num_pm_threads = DEFAULT_OFI_NUM_THREADS;
  }

  stat = mstro_ofi_thread_team_create("pm_ofi_team", "PM_OFI", 
                                        num_pm_threads,
                                        mstro_pm_handle_msg,
                                        &g_pm_ofi_team);
  assert(stat == MSTRO_OK);
  /**wait for all threads to build their endpoints ...!!! BLOCKING CALL !!!*/
  mstro_ofi_thread_team_wait_ready(g_pm_ofi_team);
  /* all endpoints are now enabled. */

  atomic_store(&g_pm_stop,false); //init

  INFO("Started %d OFI PM threads\n", num_pm_threads);

  int s = pthread_create(&g_pm_transport_init_thread, NULL,
                       mstro_pm_transport_init_thread_fun, NULL);
  if(s!=0) {
    ERR("Failed to start pool manager transport initator thread: %d\n", s);
    stat=MSTRO_FAIL;
  }
  
  return stat;
}


mstro_status
mstro_pm_terminate(void)
{
  mstro_status status;
  
  INFO("PM termination initiated\n");
  if(g_pm_stop==true) {
    DEBUG("Duplicate PM termination request, ignoring it\n");
    return MSTRO_OK;
  }
  
  atomic_store(&g_pm_stop, true);

  if(g_pm_transport_init_thread_started) {
    int stat=pthread_cancel(g_pm_transport_init_thread);
    if(stat!=0) {
      ERR("Failed to cancel transport initiator thread: %d (%s)\n",
          stat, strerror(stat));
    } else {
      void *res;
      stat=pthread_join(g_pm_transport_init_thread, &res);
      if(stat!=0) {
        ERR("Failed to join transport initiator thread: %d\n");
      }
      g_pm_transport_init_thread_started = false;
    }
  };

  
  status = mstro_ofi_thread_team_stop_and_destroy(g_pm_ofi_team);
  assert(status == MSTRO_OK);
  INFO("PM threads terminated\n");

  /* NOTE: we do not release the credentials on the PM, as we cannot obtain
   * another set in the same allocation.
   * Destruction (and release) happen in the @ref mstro__atexit handler
   */
  status |= mstro_ofi_finalize(false); 
  
  status |= mstro_pm_reg_finalize();
  
  return status;
}

mstro_status
mstro_pm_getinfo(char **result_p)
{
  char *pm_ofi_team_info = NULL;
  char *tmp;
  if(result_p==NULL)
    return MSTRO_INVOUT;

  if (g_pm_ofi_team == NULL)
  {
    ERR("PM OFI threads are not created yet\n");
    return MSTRO_FAIL;
  }
  
  pm_ofi_team_info = malloc(256*g_pm_ofi_team->num_members*g_pm_ofi_team->members[0].ctx.ep_set->size); /**assuming all threads will find the same number of eps*/
  if (pm_ofi_team_info == NULL)
  {
    ERR("Can not allocate memory for pm_info\n");
  }
  pm_ofi_team_info[0] = '\0'; /**initialize*/
  for (size_t i = 0; i < g_pm_ofi_team->num_members; i++)
  {
      mstro_appinfo_serialize(&g_pm_ofi_team->members[i].ctx.ep_set->eps[0], &tmp);
      DEBUG("thread %zu is %s\n", i,tmp);
      strcat(pm_ofi_team_info,tmp);
      strcat(pm_ofi_team_info,PM_INFO_THREAD_SEP); /**thread PM block separator - reader can count these seperators 
                                        and know how many ofi threads the PM has*/
      free(tmp); /**tmp was allocated in mstro_appinfo_serialize*/
  }
  INFO("combined is %s\n", pm_ofi_team_info);
  *result_p = pm_ofi_team_info;
  return MSTRO_OK;
}

/**@brief Deatch a client app from a running pool manager */
mstro_status
mstro_pm_detach(void)
{
  mstro_status s=MSTRO_UNIMPL;
  WARN("Not detaching from pool manager yet\n");

  goto BAILOUT;
BAILOUT:
  return s;
}

