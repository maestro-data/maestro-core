/* -*- mode:c -*- */
/** @file
 ** @brief Initialization functionality
 **
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "maestro.h"
#include "maestro/core.h"
#include "maestro/env.h"
#include "maestro/logging.h"
#include "maestro/i_statistics.h"
#include "maestro/i_globals.h"

#include <pthread.h>
#include <errno.h>
#include <string.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CORE,__VA_ARGS__)


static mstro_nanosec_t g_startup_time;

static
void free_thread_descriptor(void* x)
{
  /* this is not a good place to call DEBUG and friends anymore */
  free(x);
}

static void
mstro__atexit(void)
{
  /* nothing currently */
  ;
}

mstro_status
mstro_init(const char *workflow_name,
           const char *component_name,
           int64_t component_index)
{
  mstro_status status = MSTRO_UNIMPL;

  /* FIXME: should move to a threading-init function eventually */
  int s = pthread_key_create(&g_thread_descriptor_key, free_thread_descriptor);
  if(s!=0) {
    /* logging quality may be a bit off this early in a failure case, but we
     * can't help it */
    ERR("Failed to perform thread infrastructure init: %d (%s)\n",
        errno, strerror(errno));
    abort();
  }

  if(atexit(mstro__atexit)!=0) {
    ERR("Failed to register maestro at-exit handler: %d (%s)\n",
        errno, strerror(errno));
    abort();
  }

  INFO("Maestro Middleware startup: %s\n", mstro_version());


  status = mstro_stats_init();
  if(status!=MSTRO_OK) {
    ERR("Failed to initialize statistics module\n");
    goto BAILOUT;
  }
  g_startup_time = mstro_clock();

  /* FIXME: read config */
  DEBUG("should read config file here\n");

  /*Environment variables overrides values defined in user code*/
  if(getenv(MSTRO_ENV_WORKFLOW_NAME)!=NULL) {
    workflow_name = getenv(MSTRO_ENV_WORKFLOW_NAME);
  }
  if(getenv(MSTRO_ENV_COMPONENT_NAME)!=NULL) {
    component_name = getenv(MSTRO_ENV_COMPONENT_NAME);
  }
  if(workflow_name==NULL || component_name==NULL) {
    ERR("Workflow name or component name not provided\n");
    status = MSTRO_INVARG;
    goto BAILOUT;
  }
  /* check that component index is not negative, because negative is reserved */
  if(component_index < 0){
    ERR("Component index can not be negative \n");
    status = MSTRO_INVARG;
    goto BAILOUT;
  }
  status = mstro_core_init(workflow_name, component_name, (uint64_t) component_index
                           /* FIXME: core config file fragment parameter */
                           );
BAILOUT:
  return status;
}


mstro_status
mstro_finalize(void)
{
  mstro_status status;
  status = mstro_core_finalize();
  if(status!=MSTRO_OK) {
    ERR("Failed to finalize Maestro core\n");
    goto BAILOUT;
  }

  status = mstro_stats_store_timing(MSTRO_STATS_CAT_GENERAL,
                                    MSTRO_STATS_L_COMPONENT_RUNTIME,
                                    0, mstro_clock()-g_startup_time);

  status = mstro_stats_report_csv(NULL); /* default logging target, at
                                          * INFO level */
  status = mstro_stats_finalize();
  if(status!=MSTRO_OK) {
    ERR("Failed to finalize Maestro statistics module\n");
    goto BAILOUT;
  }

  /* dedicated threads will clean up at exit, but we need to do it manually */
  char *tid=pthread_getspecific(g_thread_descriptor_key);
  if(tid) {
    pthread_setspecific(g_thread_descriptor_key, NULL);
    free(tid);
  }
  
  int s = pthread_key_delete(g_thread_descriptor_key);
  if(s!=0) {
    ERR("Failed to perform thread infrastructure teardown: %d (%s)\n",
        errno, strerror(errno));
    abort();
  }

 BAILOUT:
  return status;
}
