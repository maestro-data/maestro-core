/* -*- mode:c -*- */
/** @file
 ** @brief Logging infrastructure implementation 
 **
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 * Copyright (C) 2020 HPE, HP Schweiz GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <ctype.h>

#include "erl_logging.h"

#include <pthread.h>
#ifdef HAVE_PTHREAD_NP_H
#include <pthread_np.h>
#endif /* HAVE_PTHREAD_NP_H */

/* Careful! Syslog defines some constants that we use as macros (like LOG_ERR) */
#ifdef HAVE_SYSLOG
# include <syslog.h>
# ifndef MSTRO_LOG_SYSLOG_FACILITY
/** Log facility to use for syslog logging. Can be overridden at
 * compile time */
#  define MSTRO_LOG_SYSLOG_FACILITY LOG_LOCAL0
# endif

static const int g_syslog_err = LOG_ERR;
static const int g_syslog_warning = LOG_WARNING;
static const int g_syslog_info = LOG_INFO;
static const int g_syslog_debug = LOG_DEBUG;
#undef LOG_ERR
#undef LOG_WARNING
#undef LOG_INFO
#undef LOG_DEBUG

#include "maestro/logging.h"

/* convert maestro log levels to syslog levels */
static int mstro_loglevel2syslog[MSTRO_log__MAX] = {
  [MSTRO_LOG_ERR]   = g_syslog_err,
  [MSTRO_LOG_WARN]  = g_syslog_warning,
  [MSTRO_LOG_INFO]  = g_syslog_info,
  [MSTRO_LOG_DEBUG] = g_syslog_debug,
  [MSTRO_LOG_NOISE] = g_syslog_debug
};

#endif /* SYSLOG */

#include "maestro/logging.h"


#include "maestro/env.h"
#include "maestro/i_globals.h"
#include "maestro/i_statistics.h"

/** log to stderr */
#define MSTRO_LOG_DST_STDERR 0
/** log to stdout */
#define MSTRO_LOG_DST_STDOUT 1
/** log to syslog */
#define MSTRO_LOG_DST_SYSLOG 2


/** ANSI terminal color symbols */
enum ansi_color_symbol {
  MSTRO_COLOR_DEFAULT = 0,
  /* reserved colors */
  MSTRO_COLOR_BRIGHTRED = 1,
  MSTRO_COLOR_BRIGHTYELLOW = 2,
  /* user-accessible colors start here: */
  MSTRO_COLOR_UNRESERVED = 3,
  MSTRO_COLOR_GREEN = 3,
  MSTRO_COLOR_BLUE,
  MSTRO_COLOR_MAGENTA,
  MSTRO_COLOR_CYAN,
  MSTRO_COLOR_WHITE,
  MSTRO_COLOR_RED,
  MSTRO_COLOR_YELLOW,
  MSTRO_COLOR_BRIGHTGREEN,
  MSTRO_COLOR_BRIGHTBLUE,
  MSTRO_COLOR_BRIGHTMAGENTA,
  MSTRO_COLOR_BRIGHTCYAN,
  MSTRO_COLOR_BRIGHTWHITE,
  MSTRO_COLOR__MAX
};

/** ANSI terminal color definition */
struct ansi_color {
  enum ansi_color_symbol sym;
  char *name;
  char *start;
  char *end;
};


/* \x1b is ESC ; \e is not C11 (a GNU extension we avoid) */

static const
struct ansi_color ansi_colors[MSTRO_COLOR__MAX] = {
  { .sym = MSTRO_COLOR_DEFAULT, .name = "default", .start = "",         .end = "" },
  { .sym = MSTRO_COLOR_RED,     .name = "red"    , .start = "\x1b[0;31m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_YELLOW,  .name = "yellow" , .start = "\x1b[0;33m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_GREEN,   .name = "green"  , .start = "\x1b[0;32m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BLUE,    .name = "blue"   , .start = "\x1b[0;34m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_MAGENTA, .name = "magenta", .start = "\x1b[0;35m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_CYAN,    .name = "cyan"   , .start = "\x1b[0;36m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_WHITE,   .name = "white"  , .start = "\x1b[0;37m", .end = "\x1b[0m" },

  /* often, but not always available */
  { .sym = MSTRO_COLOR_BRIGHTRED,     .name = "brightred"    , .start = "\x1b[1;31m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BRIGHTGREEN,   .name = "brightgreen"  , .start = "\x1b[1;32m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BRIGHTYELLOW,  .name = "brightyellow" , .start = "\x1b[1;33m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BRIGHTBLUE,    .name = "brightblue"   , .start = "\x1b[1;34m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BRIGHTMAGENTA, .name = "brightmagenta", .start = "\x1b[1;35m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BRIGHTCYAN,    .name = "brightcyan"   , .start = "\x1b[1;36m", .end = "\x1b[0m" },
  { .sym = MSTRO_COLOR_BRIGHTWHITE,   .name = "brightwhite"  , .start = "\x1b[1;37m", .end = "\x1b[0m" },
};
  

#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <unistd.h>
#include <inttypes.h>

#include <unistd.h>
#include <assert.h>
#include <string.h>

/** MIN of two numbers */
#define MIN(x,y) ((x)<(y) ? (x) : (y))


/** Indicator characters for log lines to indicate message severity */
static
const char * const mstro_log_labels[MSTRO_log__MAX] = {
 [MSTRO_LOG_ERR]   = "E",
 [MSTRO_LOG_WARN]  = "W",
 [MSTRO_LOG_INFO]  = "I",
 [MSTRO_LOG_DEBUG] = "D",
 [MSTRO_LOG_NOISE] = "N"
};


/** debugging level
 *
 * Only messages with level <= g_debug_level will be printed
 *
 * Defaults to the compile-time maximum supported log level
 * (MSTRO_MAX_LOG_LEVEL), unless a specific compile value is defined
 * for MSTRO_DEFAULT_LOG_LEVEL.
 *
 **/
#ifdef MSTRO_DEFAULT_LOG_LEVEL
static int g_debug_level = MSTRO_DEFAULT_LOG_LEVEL;
#else
static int g_debug_level = MSTRO_MAX_LOG_LEVEL;
#endif

/** Log print color */
static enum ansi_color_symbol g_log_color = MSTRO_COLOR_DEFAULT;

/** Color error/warning log messages */
static bool g_color_errors = false;

/** the workflow identifier */
static char *g_workflow_identifier = NULL;

/** An Instance identifier to be included in logging */
static _Atomic(char*) g_instance_identifier = NULL;

/** An Instance-index identifier to be included in logging */
static _Atomic(size_t) g_instance_index = 0;

/** The process ID to be include in logging */
static long g_pid = 0;

//static int g_rank = -1;

/* max length of hostname in log output */
#define MAX_HOSTNAME 256

/* set upon first call to mstro_local_hostname() */
static
char g_localhost[MAX_HOSTNAME] = "";

static
const char *
mstro_local_hostname(void)
{
  if(g_localhost[0]=='\0') {
    int s = gethostname(g_localhost, MAX_HOSTNAME);
    if(s!=0) {
      g_localhost[MAX_HOSTNAME-1] = '\0';
    }
  }
  return g_localhost;
}


static const char*
mstro__ensure_threadid(void)
{
  const char *tid=pthread_getspecific(g_thread_descriptor_key);
  /* on a given thread there is only one instance of this, so there's
   * no need for a mutex here */
  if(!tid) {
    /* first call -- compute and store */
    char *tmp = malloc(16+2);
    if(tmp==NULL) {
      /* cannot use our logging infrastructure yet, so use stderr */
      fprintf(stderr, "Failed to allocate thread-identifier\n");
      abort();
    }

    /* special-case the dedicated threads */
    pthread_t self = pthread_self();
    
    if(self==g_pm_transport_init_thread) {
      tid = strcpy(tmp, "TI");
    } else
      tid = NULL;
    
    
    if(tid==NULL) {
#ifdef HAVE_PTHREAD_THREADID_NP
      uint64_t id;
      pthread_threadid_np(pthread_self(),&id);
      snprintf(tmp, 16+2, "t%04" PRIu64, id);
#else
      uintptr_t id = (uintptr_t) pthread_self();
      snprintf(tmp, 16+2, "t%04" PRIuPTR, id);
#endif
      tid = tmp;
    }

    int s = pthread_setspecific(g_thread_descriptor_key, tid);
    if(s!=0) {
      /* cannot use our logging infrastructure yet, so use stderr */
      free(tmp);
      if(s==EINVAL) {
        fprintf(stderr, "Failed to set thread identifier; likely mstro_init has not been called, proceeding with caution\n");
        tid=NULL;
      } else {
        fprintf(stderr, "Failed to set thread identifier: %d (%s) (errno %d = %s)\n",
               s, strerror(s), errno, strerror(errno));
        abort();
      }
    }
  }
  return tid;
}

/* Calling getenv more than 6236 times on MSTRO_LOG_LEVEL gives a
 * segfault on OSx */
static _Atomic(bool) g_queried_env = false;
/* log destination choice */
static int g_log_dst = MSTRO_LOG_DST_STDERR;

static uint64_t g_log_module_mask = MSTRO_LOG_MODULE_ALL;

static const struct mstro_log_module_descriptor
g_module_descriptors[] = MSTRO_LOG_MODULES;


uint64_t mstro_log__parse_module_spec(char *module_spec)
{
  uint64_t result=0;
  char *sep = ",";
  char *word, *ctx;
  const size_t num_modules = (sizeof(g_module_descriptors)/
                              sizeof(struct mstro_log_module_descriptor));

  for (word = strtok_r(module_spec, sep, &ctx);
       word;
       word = strtok_r(NULL, sep, &ctx)) {
    char *modulename = word;
    bool flip = false;
    if(word[0]=='^') {
      flip=true;
      modulename++;
    }
    uint64_t val=0;
    if(strcasecmp("all",modulename)==0) {
      val = MSTRO_LOG_MODULE_ALL;
    } else {
      for(size_t i=0; i<num_modules; i++) {
        if(strcasecmp(g_module_descriptors[i].name, modulename)==0) {
          val = g_module_descriptors[i].selector;
          break;
        }
      }
    }
    if(val==0) {
      LOG_ERR(MSTRO_LOG_MODULE_CORE,
              "Invalid module specifier %s in module specification %s, skipping\n",
              modulename, module_spec);
    } else {
      /* LOG_DEBUG(MSTRO_LOG_MODULE_CORE, */
      /*           "Parsed |%s|, value %" PRIx64 ", complement? %d, result %" PRIx64" moves to %" PRIx64"\n", */
      /*           word, val, flip, result, */
      /*           flip? result & ~val : result |val); */
      
      if(!flip) {
        result |= val;
      } else {
        result &= ~val;
      }
    }
  }
  /* LOG_DEBUG(MSTRO_LOG_MODULE_CORE, "Parsed |%s| as %" PRIx64 "\n", */
  /*           module_spec, result); */
  return result;
}

static inline
const char *mstro_log_module_name(uint64_t module)
{
  /* FIXME: We could hash on the module */
  const size_t num_modules = (sizeof(g_module_descriptors)/
                              sizeof(struct mstro_log_module_descriptor));
  for(size_t i=0; i<num_modules; i++) {
    if((module & g_module_descriptors[i].selector)!=0) {
      return g_module_descriptors[i].name;
    }
  }
  return "???";
}

static inline
int mstro_log__parse_debug_level(const char *debug_level)
{
  /* FIXME: improve when merging with libERL */
  if(strncasecmp(debug_level, "noise", 1)==0) {
    return MSTRO_LOG_NOISE;
  } else if(strncasecmp(debug_level, "debug", 1)==0) {
    return MSTRO_LOG_DEBUG;
  } else if(strncasecmp(debug_level, "info", 1)==0) {
    return MSTRO_LOG_INFO;
  } else if(strncasecmp(debug_level, "warn", 1)==0) {
    return MSTRO_LOG_WARN;
  } else if(strncasecmp(debug_level, "err", 1)==0) {
    return MSTRO_LOG_ERR;
  } else {
    return MSTRO_MAX_LOG_LEVEL; /* default */
  }
}
/* one-time init function for log infrastructure */
static inline
void
mstro_log__init(void)
{
  
  static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
  
  int err = pthread_mutex_lock(&mtx);
  if(err!=0) 
    abort();
  
  if(!g_queried_env) {
    /* we're the first ones, do init */
    g_queried_env = true;
    g_pid=getpid();
    
    const char *env_log_dst = getenv(MSTRO_ENV_LOG_DST);
    if(env_log_dst!=NULL) {
      LOG_NOISE(MSTRO_LOG_MODULE_CORE,
                "Environment variable %s sets log destination to %s\n",
                MSTRO_ENV_LOG_DST, env_log_dst);
      if(strcasecmp(env_log_dst,"stderr")==0) {
        g_log_dst = MSTRO_LOG_DST_STDERR;
      } else if(strcasecmp(env_log_dst,"stdout")==0) {
        g_log_dst = MSTRO_LOG_DST_STDOUT;
      } else if(strcasecmp(env_log_dst,"syslog")==0) {
        g_log_dst = MSTRO_LOG_DST_SYSLOG;
      } else {
        LOG_ERR(MSTRO_LOG_MODULE_CORE,
                "Unsupported log destination value in environment variable %s: %s\n",
                MSTRO_ENV_LOG_DST, env_log_dst);
      }
    }
    
    char * env_debug_level = getenv(MSTRO_ENV_LOG_LEVEL);
    if(env_debug_level!=NULL) {
      if(isalpha(env_debug_level[0])) {
        g_debug_level = mstro_log__parse_debug_level(env_debug_level);
      } else {
        g_debug_level = atoi(env_debug_level);
      }
      if(g_debug_level<0 || g_debug_level>=MSTRO_log__MAX) {
        LOG_WARN(MSTRO_LOG_MODULE_CORE,
                 "Attempt to set debug level to %s in environment variable %s, ignored\n",
                 g_debug_level, MSTRO_ENV_LOG_LEVEL);
        g_debug_level = MSTRO_MAX_LOG_LEVEL;
      } else {
        g_debug_level = MIN(MSTRO_MAX_LOG_LEVEL,g_debug_level);
      }
      LOG_DEBUG(MSTRO_LOG_MODULE_CORE,
               "Environment variable %s sets log level to %d\n",
               MSTRO_ENV_LOG_LEVEL, g_debug_level);
    }    
    
    char *env_log_color = getenv(MSTRO_ENV_LOG_COLOR);
    if(env_log_color!=NULL) {
      enum ansi_color_symbol i;
      for (i=MSTRO_COLOR_UNRESERVED; i<MSTRO_COLOR__MAX; i++) {
        if (! strcasecmp(ansi_colors[i].name, env_log_color))
          break;
      }
      if(i==MSTRO_COLOR__MAX) {
        LOG_WARN(MSTRO_LOG_MODULE_CORE,
                 "Attempt to set log color to unknown value %s, ignored\n",
                 env_log_color);
      } else {
        g_log_color = i;
        LOG_DEBUG(MSTRO_LOG_MODULE_CORE,
                  "Log color set to %s from environment\n",
                  ansi_colors[g_log_color].name);
      }
    }

    char *env_color_errors = getenv(MSTRO_ENV_COLOR_ERRORS);
    if(env_color_errors!=NULL) {
      g_color_errors = true;
    }

    char *env_module_val = getenv(MSTRO_ENV_LOG_MODULES);
    static char all_val[] = "all"; /* needs to be modifiable since we'll pass it to strtok */
    if(env_module_val==NULL) {
      env_module_val=all_val;
    }
    g_log_module_mask = mstro_log__parse_module_spec(env_module_val);
    LOG_DEBUG(MSTRO_LOG_MODULE_CORE,
              "Environment variable %s select log modules %" PRIx64 "\n",
              MSTRO_ENV_LOG_MODULES, g_log_module_mask);
    

    /* thread ID can't be set here, every thread sets it at first call */
  } else {
    /* we're not the first */
    LOG_DEBUG(MSTRO_LOG_MODULE_CORE,
              "Competing init, lost to other thread\n");
  }
  err=pthread_mutex_unlock(&mtx);
  if(err!=0)
    abort();  
}


void
mstro_vlocation_aware_log(int level,
                          uint64_t module,
		          const char *func, const char* file, int line,
                          const char *fmtstring, va_list ap)
{
  char *header = alloca(1024); 
  header[0] = '\0';
  if(g_queried_env==false)
    mstro_log__init();
    
  assert(level>=0); assert(level<MSTRO_log__MAX);

  if(level <= g_debug_level
     && (module & g_log_module_mask)) {
    /* fprintf(stderr,"module %" PRIx64 ", mask %" PRIx64 "\n", */
    /*         module, g_log_module_mask); */
    const char *thread_desc = mstro__ensure_threadid();
    
    if(g_instance_identifier==NULL) {
      pthread_mutex_lock(&g_initdata_mtx);
      if(g_initdata) {
        g_instance_identifier = strdup(g_initdata->component_name);
	g_instance_index = g_initdata->component_index;
        g_workflow_identifier = strdup(g_initdata->workflow_name);
      }
      pthread_mutex_unlock(&g_initdata_mtx);
    }

    enum ansi_color_symbol color = g_log_color;
    if(g_color_errors) {
      if (level == MSTRO_LOG_ERR) {
        color = ansi_colors[MSTRO_COLOR_RED].sym;
      } else if (level == MSTRO_LOG_WARN) {
        color = ansi_colors[MSTRO_COLOR_YELLOW].sym;
      }
    }

    snprintf(&header[0], 1024,
             "%s[%s:%s] %s:%zu %" PRIappid " %s (%s %ld) %" PRIu64 ": %s(%s:%d) %s%s",

             ansi_colors[color].start,
             mstro_log_labels[level], mstro_log_module_name(module),
             g_instance_identifier? g_instance_identifier : "", g_instance_index,
	     mstro_appid(),
             thread_desc,
             mstro_local_hostname(), g_pid,
             mstro_clock(),
             func, file, line,
	     fmtstring,
             ansi_colors[color].end
             );
    
    switch(g_log_dst) {
      case MSTRO_LOG_DST_STDERR:
        vfprintf(stderr, header, ap);
        break;
      case MSTRO_LOG_DST_STDOUT:
        vfprintf(stdout, header, ap);
        break;
      case MSTRO_LOG_DST_SYSLOG:
#ifdef HAVE_SYSLOG
        vsyslog(MSTRO_LOG_SYSLOG_FACILITY|mstro_loglevel2syslog[level],
                header, ap);
#else
	vfprintf(stderr, header, ap);
#endif
        break;
      default:
        /* repair confusion */
        g_log_dst = MSTRO_LOG_DST_STDERR;
        LOG_ERR(MSTRO_LOG_MODULE_CORE,
                "Invalid log destination was set, reset to stderr\n");
        vfprintf(stderr, header, ap);
        break;
    }
  }
}


void
mstro_location_aware_log(int level,
                         uint64_t module,
                         const char *func, const char* file, int line,
                         const char *fmtstring, ...)
{
  va_list ap;
  va_start(ap, fmtstring);
  mstro_vlocation_aware_log(level, module, func, file, line, fmtstring, ap);
  va_end(ap);
}


const char *
mstro_workflow_id(void)
{
  return (g_workflow_identifier==NULL) ? "(null)" : g_workflow_identifier; 
}

const char *
mstro_component_name(void)
{
  return (g_instance_identifier==NULL) ? "(null)" : g_instance_identifier;
}

mstro_app_id
mstro_appid(void)
{
  return g_pool_app_id;
}

const char *
mstro_hostname(void)
{
  return mstro_local_hostname();
}

pid_t
mstro_pid(void)
{
  return g_pid;
}



const char *
mstro_threadid(void)
{
  const char *tid=pthread_getspecific(g_thread_descriptor_key);
  return (tid==NULL) ? "(no TID)" : tid;
}


/** function to inject into mamba logging
 *
 * This is injected at mamba build time using
 * -DMMB_USER_DEFINED_LOG_FUNC=mstro_mamba_logging_bridge
 */
void mstro_mamba_logging_bridge(int level, const char *func, const char *file,
                                int line, const char *fmtstring, ...)
{
  va_list ap;
  va_start(ap, fmtstring);

  int mstro_level;
  switch(level) {
    case MMB_LOG_ERR:   mstro_level = MSTRO_LOG_ERR; break;
    case MMB_LOG_WARN:  mstro_level = MSTRO_LOG_WARN; break;
    case MMB_LOG_INFO:  mstro_level = MSTRO_LOG_INFO; break;
    case MMB_LOG_DEBUG: mstro_level = MSTRO_LOG_DEBUG; break;
    case MMB_LOG_NOISE: mstro_level = MSTRO_LOG_NOISE; break;
    default:
      LOG_ERR(MSTRO_LOG_MODULE_CORE, "Invalid log level %d\n", level);
      abort();
  }
      
  mstro_vlocation_aware_log(mstro_level, MSTRO_LOG_MODULE_MAMBA,
                            func, file, line, fmtstring, ap);
  va_end(ap);
}


/** function to inject into liberl logging
 *
 */
void mstro_liberl_logging_bridge(int level, const char *func, const char *file,
                                int line, const char *fmtstring, ...)
{
  va_list ap;
  va_start(ap, fmtstring);

  int mstro_level;
  switch(level) {
    case ERL_LOG_ERR:   mstro_level = MSTRO_LOG_ERR; break;
    case ERL_LOG_WARN:  mstro_level = MSTRO_LOG_WARN; break;
    case ERL_LOG_INFO:  mstro_level = MSTRO_LOG_INFO; break;
    case ERL_LOG_DEBUG: mstro_level = MSTRO_LOG_DEBUG; break;
    case ERL_LOG_NOISE: mstro_level = MSTRO_LOG_NOISE; break;
    default:
      LOG_ERR(MSTRO_LOG_MODULE_CORE, "Invalid log level %d\n", level);
      abort();
  }
      
  mstro_vlocation_aware_log(mstro_level, MSTRO_LOG_MODULE_ERL,
                            func, file, line, fmtstring, ap);
  va_end(ap);
}
