/* -*- mode:c -*- */
/** @file
 ** @brief Maestro global variables
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Global variable instantiation */
#include "maestro/i_globals.h"
#include "maestro/i_cdo.h"

/** Init data; set at mstro_core_init() time;
    protected by @ref g_initdata_mtx */
struct mstro_core_initdata * g_initdata = NULL;
/** lock to protect @ref g_initdata */
pthread_mutex_t g_initdata_mtx = PTHREAD_MUTEX_INITIALIZER;



pthread_mutex_t g_mstro_pool_local_mtx = PTHREAD_MUTEX_INITIALIZER;
struct mstro_pool_entry * g_mstro_pool_local = NULL;


struct mstro_cdo_id MSTRO_CDO_ID_NULL = MSTRO_CDO_ID_INVALID;

_Atomic bool g_mstro_pm_attached = false;
_Atomic bool g_mstro_pm_rejected = false;



pthread_t g_pm_transport_init_thread;

/* set very early in core init */
pthread_key_t g_thread_descriptor_key;


/** the default (DRAM) memspace */
mmbMemSpace *g_default_cdo_memspace = NULL;

/** the default (DRAM) memory interface */
mmbMemInterface *g_default_cdo_interface = NULL;

bool g_mio_available = false;

/** all outgoing messages need to contain the (same) app token and our app-id.
 ** We save lots of mallocs and avoid that we need an explicit de-allocator for the 
 ** (typically stack-allocated) pool messages by having two re-usable
 "read-only" (shared across all messages) structures for this */
Mstro__Pool__Apptoken g_pool_apptoken = MSTRO__POOL__APPTOKEN__INIT;

/** re-usable app id structure */
Mstro__Pool__Appid    g_pool_appid = MSTRO__POOL__APPID__INIT;

/** the fundamental built-in schema. Filled early in mstro_core_init(), then constant */
mstro_schema g_mstro_core_schema_instance = NULL;

union mstro_component_descriptor g_component_descriptor;

/** the precomputed default path for GFS transport (incl. trailing '/') */
char *g_mstro_transport_gfs_dir = "./"; /* possibly overwritten at init
                                         * time from env*/

/** string length of @ref g_mstro_transport_gfs_dir */
size_t g_mstro_transport_gfs_dir_len = 2; /* possibly overwritten at
                                           * init time */

/** NUMA supported and working? */
bool g_have_numa = false;
/** Number of cpus in the system, derived from the cpu numbers in /sys/devices/system/cpu */
int g_numa_configured_cpus=1;
/** number of memory nodes in the system, derived from the node numbers in /sys/devices/system/node */
int g_numa_configured_nodes=1;

/**FIXME maybe use a more proper resource structure for avaiable cores for each type of threads */
/* list of cores to bind operation threads to it, populated by parsing MSTRO_ENV_BIND_OP_HANDLER at mstro_core__numa_init */
int *g_numa_op_bind_cores = NULL;
/* length of g_numa_op_bind_cores */
int g_numa_op_bind_cores_len = 0;
/* id of the next core to pin from g_numa_op_bind_cores list */
_Atomic(int) g_numa_op_bind_next = 0;

/* list of cores to bind PM/PC threads to it, populated by parsing MSTRO_BIND_PM_PC at mstro_core__numa_init */
int *g_numa_pm_pc_bind_cores = NULL;
/* length of g_numa_pm_pc_bind_cores */
int g_numa_pm_pc_bind_cores_len = 0;
/* id of the next core to pin from g_numa_pm_pc_bind_cores list */
_Atomic(int) g_numa_pm_pc_bind_next = 0;

/* list of cores to bind transport threads to it, populated by parsing MSTRO_BIND_TRANSPORT_THREAD at mstro_core__numa_init */
int *g_numa_transport_bind_cores= NULL;
/* length of g_numa_transport_bind_cores */
int g_numa_transport_bind_cores_len = 0;
/* id of the next core to pin from g_numa_transport_bind_cores list */
_Atomic(int) g_numa_transport_bind_next = 0;

/** APP id when connected to pool */
_Atomic(mstro_app_id) g_pool_app_id = MSTRO_APP_ID_INVALID;

/** handle to the pool operations handling thread team */
struct erl_thread_team *g_pool_operations_team = NULL;


/** will be set to a reasonable value at init time, or leaves inline
 * transport disabled when set to 0 */
size_t g_transport_inline_max = 0;
