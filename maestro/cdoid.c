/* -*- mode:c -*- */
/** @file
 ** @brief Maestro CDO ID handling 
 **/
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "maestro/i_cdo.h"
#include "maestro/i_uuid.h"
#include "maestro/i_base64.h"
#include "maestro/logging.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <limits.h>


/* simplify logging */
#define NOISE(...) LOG_NOISE(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_CDO,__VA_ARGS__)


/** ntohl is in strange places occasionally, and ntohll is
    nonstandard, so we use our own. These use the fact that C does all
    the heavy lifting for us, even on crazy middle-endian systems, but
    it should still optimize to good code on common machines. */
static inline
uint32_t udj__ntohl(uint32_t const net)
{
  uint8_t data[4] = {0,0,0,0};
  memcpy(&data, &net, sizeof(data));
  return ((uint32_t) data[3] <<0)
      |  ((uint32_t) data[2] <<8)
      |  ((uint32_t) data[1] <<16)
      |  ((uint32_t) data[0] <<24);
}

static inline
uint64_t udj__ntohll(uint64_t const net)
{
  uint8_t data[8] = {0,0,0,0, 0,0,0,0};
  memcpy(&data, &net, sizeof(data));
  return ((uint64_t) data[7] <<0)
      |  ((uint64_t) data[6] <<8)
      |  ((uint64_t) data[5] <<16)
      |  ((uint64_t) data[4] <<24)
      |  ((uint64_t) data[3] <<32)
      |  ((uint64_t) data[2] <<40)
      |  ((uint64_t) data[1] <<48)
      |  ((uint64_t) data[0] <<56);
}


mstro_status
mstro_cdo_id__str(const struct mstro_cdo_id *cdoid, char **name)
{
  /*  canonical form: 8-4-4-4-12 chars */
  /* We imitate MSB left */

  if(cdoid==NULL || name==NULL)
    return MSTRO_INVARG;
  if(*name==NULL) {
    *name=malloc(sizeof(char)*MSTRO_CDO_ID_STR_LEN);
  }
  
  uint64_t qw0 = udj__ntohll(cdoid->qw[0]);
  uint64_t qw1 = udj__ntohll(cdoid->qw[1]);
  /* portable longlong printf needed here ? */
  sprintf(*name,
          "%08" PRIx64
          "-%04" PRIx64
          "-%04" PRIx64
          "-%04" PRIx64
          "-%012" PRIx64
          ".0%" PRIlocalid "",
          (qw0 >>32) & 0xFFFFFFFFU,
          (qw0 >>16) & 0xFFFFU,
          qw0 & 0xFFFFU,
          qw1  >> 48,
          qw1 & 0x0000FFFFFFFFFFFFU,
          cdoid->local_id);

  return MSTRO_OK;
}

/** use TAG buffer of length tag_size to generate an ID.
    We use a UUID v5 with our own namespace */
mstro_status
mstro_cdo_id_from_name(const char *name,
                       struct mstro_cdo_id *result)
{
  //  INFO("id from tag on %p, len %d, to %p\n", tag, tag_size, result);
  if(name==NULL) 
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;
  
  uuid_t *uuid=NULL, *uuid_ns=NULL;
  mstro_status status=MSTRO_FAIL;

  /* encode name */
  unsigned char *cdo_name = NULL;

  cdo_name=base64_encode((const unsigned char*)name,
                         strlen((const char *)name), NULL);
  if(!cdo_name) {
    ERR("failed to b64-encode name %s\n", name);
    goto BAILOUT;
  }
  
  /* create engine */
  if(UUID_RC_OK!=uuid_create(&uuid)) {
    ERR("uuid_create failed\n");
    goto BAILOUT;
  }
  
  if(UUID_RC_OK!=uuid_create(&uuid_ns)) {
    ERR("uuid_create for namespace failed\n");
    uuid_destroy(uuid);
    goto BAILOUT;
  }
  
  /* configure for SHA-1 based V5 hash, and use our own namespace */
  if(UUID_RC_OK!=uuid_load(uuid_ns, "ns:CDO")) {
    ERR("uuid_load for ns:CDO failed\n");
    goto BAILOUT;
  }

  if(UUID_RC_OK!=uuid_make(uuid, UUID_MAKE_V5, uuid_ns, cdo_name)) {
    ERR("uuid_make failed (cdo_name=%s)\n", cdo_name);
    goto BAILOUT;
  }
    
  /* store */
  {
    /* a bug in ossp-uuid up to 1.6.2 makes it impossible to avoid the
     * buffer export */
    /* FIXME: we now have a local copy of the uuid code, so we can fix it */
    unsigned char *buf=NULL;
    size_t bufsize=0;
    uuid_rc_t stat = uuid_export(uuid,UUID_FMT_BIN,&buf,&bufsize);
    if(UUID_RC_OK!=stat) {
      ERR("uuid_export failed: %d (%s)\n", stat, uuid_error(stat));
      goto BAILOUT;
    }
    assert(MSTRO_CDO_ID_NUMBYTES==UUID_LEN_BIN);
    memcpy(&(result->id[0]), buf, MSTRO_CDO_ID_NUMBYTES);
    result->local_id = MSTRO_CDO_LOCAL_ID_NONE;
    free(buf);
  }
  status=MSTRO_OK;
    
BAILOUT:
  NOISE("mstro_cdo_id_from_name: status %d\n", status);
  if(NULL!=cdo_name)
    free(cdo_name);
  if(NULL!=uuid_ns)
    if(UUID_RC_OK!=uuid_destroy(uuid_ns))
      status=MSTRO_FAIL;
  if(NULL!=uuid)
    if(UUID_RC_OK!=uuid_destroy(uuid))
      status=MSTRO_FAIL;
  return status;
}

mstro_status
mstro_cdogrp_id_from_name(const char *name,
                          struct mstro_cdo_id *result)
{
  //  INFO("id from tag on %p, len %d, to %p\n", tag, tag_size, result);
  if(name==NULL) 
    return MSTRO_INVARG;
  if(result==NULL)
    return MSTRO_INVOUT;
  
  uuid_t *uuid=NULL, *uuid_ns=NULL;
  mstro_status status=MSTRO_FAIL;

  /* encode name */
  unsigned char *cdo_name = NULL;

  cdo_name=base64_encode((const unsigned char*)name,
                         strlen((const char *)name), NULL);
  if(!cdo_name) {
    ERR("failed to b64-encode name %s\n", name);
    goto BAILOUT;
  }
  
  /* create engine */
  if(UUID_RC_OK!=uuid_create(&uuid)) {
    ERR("uuid_create failed\n");
    goto BAILOUT;
  }
  
  if(UUID_RC_OK!=uuid_create(&uuid_ns)) {
    ERR("uuid_create for namespace failed\n");
    uuid_destroy(uuid);
    goto BAILOUT;
  }
  
  /* configure for SHA-1 based V5 hash, and use our own namespace */
  if(UUID_RC_OK!=uuid_load(uuid_ns, "ns:CDO-GROUP")) {
    ERR("uuid_load for ns:CDO-GROUP failed\n");
    goto BAILOUT;
  }

  if(UUID_RC_OK!=uuid_make(uuid, UUID_MAKE_V5, uuid_ns, cdo_name)) {
    ERR("uuid_make failed (cdo_name=%s)\n", cdo_name);
    goto BAILOUT;
  }
    
  /* store */
  {
    /* a bug in ossp-uuid up to 1.6.2 makes it impossible to avoid the
     * buffer export */
    /* FIXME: we now have a local copy of the uuid code, so we can fix it */
    unsigned char *buf=NULL;
    size_t bufsize=0;
    uuid_rc_t stat = uuid_export(uuid,UUID_FMT_BIN,&buf,&bufsize);
    if(UUID_RC_OK!=stat) {
      ERR("uuid_export failed: %d (%s)\n", stat, uuid_error(stat));
      goto BAILOUT;
    }
    assert(MSTRO_CDO_ID_NUMBYTES==UUID_LEN_BIN);
    memcpy(&(result->id[0]), buf, MSTRO_CDO_ID_NUMBYTES);
    result->local_id = MSTRO_CDO_LOCAL_ID_NONE;
    free(buf);
  }
  status=MSTRO_OK;
    
BAILOUT:
  NOISE("status %d\n", status);
  if(NULL!=cdo_name)
    free(cdo_name);
  if(NULL!=uuid_ns)
    if(UUID_RC_OK!=uuid_destroy(uuid_ns))
      status=MSTRO_FAIL;
  if(NULL!=uuid)
    if(UUID_RC_OK!=uuid_destroy(uuid))
      status=MSTRO_FAIL;
  return status;
}

mstro_status
mstro_str_random(char** s, size_t len)
{
  size_t max = MSTRO_CDO_ID_NUMBYTES * 8;
  if (len <= 0 || len > max) {
    ERR("Unreasonable length for a name (asked: %d, max: %d)\n",
        len,
        max);
    return MSTRO_FAIL;
  }
  size_t i;
  *s = malloc(sizeof(char)*len);
  if (*s == NULL) {
    ERR("No more memory.\n");
    return MSTRO_FAIL;
  }
  for (i=0; i<len; i++) {
    (*s)[i] = rand() % CHAR_BIT; 
  }
  return MSTRO_OK;
}

