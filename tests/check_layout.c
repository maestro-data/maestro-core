/* check maestro layout awareness */
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
/* FIXME shouldn't have to include those */
#include <mamba.h>
#include <transformation.h>


#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>
#include "maestro/logging.h"

#define N 3
#define M 4
#define ROWMAJ MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_ROWMAJOR
#define COLMAJ MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER_COLMAJOR

CHEAT_DECLARE (
	void
	print_mat(double* p)
	{
      size_t i,j;
	  for (i=0; i<N*M; i++) {
	      fprintf(stderr, "%2.0f ", p[i]);
	  }
     fprintf(stderr, "\n");

	}

	unsigned long 
	mamba_data_diff(mmbArray* src, mmbArray* dst)
	{
	  cheat_assert(src!=NULL && dst!=NULL);
	  size_t n;
	  n = dst->allocation->n_bytes;
	  cheat_assert(src->allocation->n_bytes == dst->allocation->n_bytes);
	  char* p_src, *p_dst;
      unsigned long x;
      size_t i;
      p_src = src->allocation->ptr;
      p_dst = dst->allocation->ptr;
      for (i=0,x=0;i<n; i++)
        x += ((unsigned char*)p_src)[i] ^ ((unsigned char*)p_dst)[i];

      return x;
    }

               )

CHEAT_TEST(layout_attribute_works,
  size_t data_count = N*M;
  int64_t bytes = data_count*sizeof(double);
  double* src_data = malloc(bytes);
  double* unpooled_data = malloc(bytes);
  double* unpooled_data_t = malloc(bytes);

  srandom(time(NULL));
  for(size_t i=0; i<data_count; i++) {
  //  src_data[i]=random()%100;
    src_data[i]=i;
  }
  
  cheat_assert(MSTRO_OK == mstro_init("Tests","LAYOUT",0));
  char name[] = "transformation_pioneer";
  char name_unpooled[] = "unpooled";

  mstro_cdo cdo_src=NULL;
  mstro_cdo cdo_dst=NULL;
  mstro_cdo cdo_unpooled=NULL;

  /* Layout information */
  int64_t ndims, patt_src, patt_dst, elsz;
  int64_t* dimsz_src, *dimsz_dst;
  elsz = sizeof(double);
  ndims = 2;
  dimsz_src = malloc(sizeof(int64_t)*ndims);
  cheat_assert(dimsz_src != NULL);
  dimsz_src[0] = N;
  dimsz_src[1] = M;
  dimsz_dst = malloc(sizeof(int64_t)*ndims);
  cheat_assert(dimsz_dst != NULL);
  dimsz_dst[0] = M;
  dimsz_dst[1] = N;

  patt_src = ROWMAJ;
  patt_dst = COLMAJ;

  /* Pass layout info through attributes */

  /* Producer side */
  cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                   src_data, false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                   (void**)&bytes, true, false));

  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src, 
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE, &elsz,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS, &ndims,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE, dimsz_src,
                                                   false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER, &patt_src,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_offer(cdo_src));

  /* Consumer side */
  cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_dst));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_dst,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE, &elsz,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_dst,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS, &ndims,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_dst,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE, dimsz_dst,
                                                   false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_dst,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER, &patt_dst,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_dst));
    cheat_assert(MSTRO_OK == mstro_cdo_require(cdo_dst));

  cheat_assert(MSTRO_OK == mstro_cdo_demand(cdo_dst));
  /* at this point automatic transformation was performed */

  /* Non pooled cdo: helps checksum result + test non-pooled transformation
   * FIXME it's a copy of cdo_dst*/
  cheat_assert(MSTRO_OK == mstro_cdo_declare(name_unpooled, MSTRO_ATTR_DEFAULT, &cdo_unpooled));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, (void**)&bytes,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR, unpooled_data,
                                                   false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE, &elsz,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS, &ndims,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE, dimsz_dst,
                                                   false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled,
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER, &patt_dst,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_unpooled));
  /* at this point cdo_unpooled has a mamba array (raw-ptr wrapper) */
  
  /* non-pool transformation */
  cheat_assert(MSTRO_OK == mstro_transform_layout(cdo_src, cdo_unpooled,
                                                  cdo_src->attributes, cdo_unpooled->attributes));
  /* checksum pool and non-pool results */
  mmbArray *m_unpooled;
  mmbArray* m_dst;
  cheat_assert(MSTRO_OK == mstro_cdo_access_mamba_array(cdo_dst, &m_dst));
  cheat_assert(MSTRO_OK == mstro_cdo_access_mamba_array(cdo_unpooled, &m_unpooled));
  cheat_assert(0 == mamba_data_diff(m_dst, m_unpooled));
  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_dst));

  cheat_assert(MSTRO_OK == mstro_cdo_withdraw(cdo_src));

 /* One last check: transpose again and compare with original CDO */ 
  mstro_cdo cdo_unpooled_t;
  cheat_assert(MSTRO_OK == mstro_cdo_declare(name_unpooled, MSTRO_ATTR_DEFAULT, &cdo_unpooled_t));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled_t,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, (void**)&bytes,
                                                   true, false));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled_t,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR, unpooled_data_t,
                                                   false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled_t, 
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE, &elsz,
                                                   true, false)); 
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled_t, 
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_NDIMS, &ndims,
                                                   true, false)); 
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled_t, 
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_DIMS_SIZE, dimsz_src,
                                                   false, true)); 
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_unpooled_t, 
                                                   MSTRO_ATTR_CORE_CDO_LAYOUT_ORDER, &patt_src,
                                                   true, false)); 
  cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_unpooled_t));

  cheat_assert(MSTRO_OK == mstro_transform_layout(cdo_unpooled, cdo_unpooled_t, 
                                                  cdo_unpooled->attributes, cdo_unpooled_t->attributes));
  mmbArray *m_unpooled_t;
  mmbArray* m_src;
  cheat_assert(MSTRO_OK == mstro_cdo_access_mamba_array(cdo_src, &m_src));
  cheat_assert(MSTRO_OK == mstro_cdo_access_mamba_array(cdo_unpooled_t, &m_unpooled_t));
  cheat_assert(0 == mamba_data_diff(m_src, m_unpooled_t));

  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_unpooled));
  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_unpooled_t));

  cheat_assert(MSTRO_OK == mstro_finalize());

  free(dimsz_src); free(dimsz_dst);
  free(unpooled_data_t);
  free(unpooled_data);
  free(src_data);

  )



