#
# Copyright (C) 2018-2020 Cray Computer GmbH
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# do enable valgrind on all tests uncomment the following:
#LOG_COMPILER=valgrind --leak-check=full
AM_DEFAULT_SOURCE_EXT = .c
AM_LDFLAGS = -no-install

TEST_INCLUDES = -I$(top_srcdir)/include \
	    -I$(top_srcdir)/deps/mamba/common \
            -I$(top_srcdir)/deps/mamba/memory \
            -I$(top_srcdir)/deps/libyaml/include \
	    -I$(top_srcdir)/deps/libcyaml/include \
	    -I$(top_srcdir)/protocols \
	    -I$(top_srcdir)/ \
	    -I$(top_srcdir)/transport \
	    -I$(top_srcdir)/transformation \
	    -I$(top_srcdir)/attributes \
	    -I$(top_builddir)/attributes   # schema_type_parse.h header is compile-time-generated

if WITH_LOCAL_LIBFABRIC
TEST_INCLUDES += -I$(top_srcdir)/deps/libfabric/include -I$(top_srcdir)/deps/libfabric/prov/gni/include
endif

AM_CPPFLAGS = $(TEST_INCLUDES) -DTOPSRCDIR=$(top_srcdir)

LDADD = $(top_builddir)/libmaestro.la

check_HEADERS = cheat.h ecmwf_config.h

TESTS = check_version check_init check_uuid \
	coverage check_memlock \
	check_schema_parse \
	check_cdo_selectors \
	check_type_parser \
	check_symtab \
	check_events \
	check_mempool \
        check_fifo \
	check_protobuf_c \
	check_transport_gfs \
	check_layout \
	check_dispose_reuse \
        check_declare \
	check_decode_pminfo.sh \
	check_pool_local \
	check_pool_local_putget \
	check_pool_local_stress \
	check_declaration_seal \
	check_pool_mamba \
	check_pool_retract \
	check_pool_local_rawptr \
	check_subscribe_local \
	run_demo.sh \
        check_pool_manager \
	check_pm_declare.sh \
	check_subscribe.sh \
	check_pm_reentrant_client.sh \
	check_pm_interlock.sh \
	check_pm_collision.sh \
	check_pm_redundant_interlock.sh \
	check_pm_interlock_async.sh \
	check_pm_dist_cdo.sh \
	check_ecmwf_attr.sh \
	check_ecmwf_events.sh \
	check_ecmwf_handle.sh \
	check_pm_declare_group.sh

XFAIL_TESTS = \
	check_events \
	check_subscribe_local

# too expensive, actually more of a benchmark:
#	check_pool_local_multi

check_PROGRAMS = check_version check_init check_uuid \
		 coverage check_memlock \
		 check_protobuf_c \
		 check_layout \
		 check_dispose_reuse \
		 check_transport_gfs \
                 check_declare \
		 check_schema_parse \
		 check_type_parser \
		 check_cdo_selectors \
		 check_symtab \
                 check_pool_local \
                 check_pool_local_stress \
		 check_pool_local_putget \
		 check_pool_local_multi  \
		 check_declaration_seal  \
                 check_pool_mamba \
		 check_pool_retract \
		 check_pool_local_rawptr \
                 check_pool_manager \
		 check_subscribe_local \
		 demo_mvp_d3_2 \
		 simple_pool_manager \
		 decode_pminfo \
		 simple_client \
		 simple_group_client \
		 simple_group_injector \
		 simple_interlock_client_1 \
		 redundant_interlock_client_1 \
		 simple_interlock_client_2 \
		 redundant_interlock_client_2 \
		 colliding_client_1 \
		 colliding_client_2 \
		 reentrant_client \
		 simple_interlock_async_client_1 \
		 simple_interlock_async_client_2 \
		 simple_dist_client_1 \
		 simple_dist_client_2 \
		 simple_injector \
		 simple_archiver \
		 simple_telemetry_listener \
		 check_events \
		 check_mempool \
	         check_fifo \
		 ecmwf_attr_producer \
		 ecmwf_attr_consumer \
		 ecmwf_handle_producer \
		 ecmwf_handle_consumer \
		 ecmwf_events_producer \
		 ecmwf_events_consumer \
		 check_mempool \
	         check_fifo


if WITH_MIO
AM_CPPFLAGS += $(TEST_INCLUDES) -I$(top_srcdir)/deps/mio/src -D_REENTRANT -D_GNU_SOURCE -DM0_INTERNAL= -DM0_EXTERN=extern -Wno-attributes
check_PROGRAMS += check_transport_mio
TESTS += check_transport_mio
endif

if WITH_SWIG
# for the moment that means: with python
TESTS += py_check_pm \
	 py_check_local_pm \
	 py_check_threaded_interlock \
	 py_check_group \
	 py_check_subscribe 
#	 py_check_selector
endif

CLIENT1_OPS="DECLARE cdo1 1025\
             SEAL cdo1 -1 \
             DECLARE cdo2 20480\
             REQUIRE cdo2 -1\
             OFFER cdo1 -1 \
             DEMAND cdo2 -1\
             WITHDRAW cdo1 -1\
             DISPOSE cdo1 -1\
             DISPOSE cdo2 -1"

R_CLIENT1_OPS="DECLARE cdo1a 11\
             SEAL cdo1a -1 \
             DECLARE cdo2 20480\
             REQUIRE cdo2 -1\
             OFFER cdo1a -1 \
             DEMAND cdo2 -1\
             WITHDRAW cdo1a -1\
             DISPOSE cdo1a -1\
             DISPOSE cdo2 -1"

CLIENT2_OPS="DECLARE cdo2 20480\
             SEAL cdo2 -1 \
             DECLARE cdo1 1025\
             REQUIRE cdo1 -1\
             DECLARE cdo1a 11     REQUIRE cdo1a -1   \
             OFFER cdo2 -1 \
             DEMAND cdo1 -1\
	     	 RETRACT cdo1a -1      DISPOSE cdo1a -1 \
             WITHDRAW cdo2 -1\
             DISPOSE cdo2 -1\
             DISPOSE cdo1 -1"

R_CLIENT2_OPS="DECLARE cdo2 20480\
             SEAL cdo2 -1 \
             DECLARE cdo1 1025\
             REQUIRE cdo1 -1\
             DECLARE cdo1a 11     REQUIRE cdo1a -1   \
             OFFER cdo2 -1 \
             DEMAND cdo1 -1\
	     	 DEMAND cdo1a -1       DISPOSE cdo1a -1 \
             WITHDRAW cdo2 -1\
             DISPOSE cdo2 -1\
             DISPOSE cdo1 -1"

ASYNC_CLIENT1_OPS="	DECLARE cdo1 1025\
				   	DECLARE cdo2 1024\
		           	DECLARE cdo3 20480\
				 	WAIT cdo1 -1\
					SEAL cdo1 -1\
					WAIT cdo2 -1\
					SEAL cdo2 -1\
					WAIT cdo3 -1\
					SEAL cdo3 -1\
					WAIT cdo3 -1\
					REQUIRE cdo3 -1\
					WAIT cdo1 -1\
					WAIT cdo2 -1\
					WAIT cdo3 -1\
					OFFER cdo1 -1\
					WAIT cdo1 -1\
					DEMAND cdo3 -1\
					WAIT cdo3 -1\
					WITHDRAW cdo1 -1\
					WAIT cdo1 -1\
					DISPOSE cdo1 -1\
					DISPOSE cdo2 -1\
					DISPOSE cdo3 -1"

ASYNC_CLIENT2_OPS=" DECLARE cdo3 20480\
	           		DECLARE cdo2 1024\
					WAIT cdo2 -1\
					WAIT cdo3 -1\
					SEAL cdo3 -1\
	             		DECLARE cdo1 1025\
					WAIT cdo1 -1\
					SEAL cdo1 -1\
					SEAL cdo2 -1\
					WAIT cdo1 -1\
					WAIT cdo2 -1\
	             		REQUIRE cdo1 -1\
					REQUIRE cdo2 -1\
					WAIT cdo3 -1\
					WAIT cdo1 -1\
	             		OFFER cdo3 -1\
					WAIT cdo3 -1\
	             		DEMAND cdo1 -1\
					WAIT cdo1 -1\
	             		WITHDRAW cdo3 -1\
					WAIT cdo2 -1\
					RETRACT cdo2 -1\
					WAIT cdo2 -1\
					WAIT cdo3 -1\
	             		DISPOSE cdo2 -1\
					DISPOSE cdo3 -1\
	             		DISPOSE cdo1 -1"

INJECTOR_OPS="\
			       DECLARE cdo1 1023      SEAL cdo1 -1       OFFER cdo1 -1    \
			       DECLARE cdo2 1023000   SEAL cdo2 -1       OFFER cdo2 -1    \
			       DECLARE cdo3 102       SEAL cdo3 -1       OFFER cdo3 -1    \
			       DECLARE cdo1a 107      REQUIRE cdo1a -1                    \
			 	   RETRACT cdo1a -1       DISPOSE cdo1a -1                    \
			       WITHDRAW cdo1 -1       WITHDRAW cdo2 -1   WITHDRAW cdo3 -1 \
			 	   DISPOSE  cdo3 -1       DISPOSE  cdo2 -1   DISPOSE  cdo1 -1"




simple_interlock_client_1_SOURCES = simple_interlock_client.c
simple_interlock_client_1_CPPFLAGS = $(AM_CPPFLAGS)  -DCOMPONENT_ID=1 -DCLIENT_ARGS=$(CLIENT1_OPS)
simple_interlock_client_2_SOURCES = simple_interlock_client.c
simple_interlock_client_2_CPPFLAGS = $(AM_CPPFLAGS)  -DCOMPONENT_ID=2 -DCLIENT_ARGS=$(CLIENT2_OPS)

redundant_interlock_client_1_SOURCES = simple_interlock_client.c
redundant_interlock_client_1_CPPFLAGS = $(AM_CPPFLAGS)  -DCOMPONENT_ID=1 -DCLIENT_ARGS=$(R_CLIENT1_OPS)
redundant_interlock_client_2_SOURCES = simple_interlock_client.c
redundant_interlock_client_2_CPPFLAGS = $(AM_CPPFLAGS)  -DCOMPONENT_ID=2 -DCLIENT_ARGS=$(R_CLIENT2_OPS)

colliding_client_1_SOURCES = colliding_client.c
colliding_client_1_CPPFLAGS = $(AM_CPPFLAGS)  -DREPETITION=1
colliding_client_2_SOURCES = colliding_client.c
colliding_client_2_CPPFLAGS = $(AM_CPPFLAGS)  -DREPETITION=2
reentrant_client_SOURCES = reentrant_client.c
reentrant_client_CPPFLAGS = $(AM_CPPFLAGS)

simple_interlock_async_client_1_SOURCES = simple_interlock_async_client.c
simple_interlock_async_client_1_CPPFLAGS = $(AM_CPPFLAGS) -DCLIENT_ARGS=$(ASYNC_CLIENT1_OPS)
simple_interlock_async_client_2_SOURCES = simple_interlock_async_client.c
simple_interlock_async_client_2_CPPFLAGS = $(AM_CPPFLAGS) -DCLIENT_ARGS=$(ASYNC_CLIENT2_OPS)

simple_dist_client_1_SOURCES = simple_dist_client.c
simple_dist_client_1_CPPFLAGS = $(AM_CPPFLAGS)  -DCOMPONENT_ID=1
simple_dist_client_2_SOURCES = simple_dist_client.c
simple_dist_client_2_CPPFLAGS = $(AM_CPPFLAGS)  -DCOMPONENT_ID=2
simple_injector_SOURCES = simple_interlock_client.c
simple_injector_CPPFLAGS = $(AM_CPPFLAGS) -DCOMPONENT_ID=0 -DCLIENT_ARGS=$(INJECTOR_OPS)
simple_group_injector_SOURCES = simple_group_client.c
simple_group_injector_CPPFLAGS = $(AM_CPPFLAGS) -DINJECT_GROUP_MEMBERS=1

simple_archiver_SOURCES = simple_archiver.c
simple_archiver_CPPFLAGS = $(AM_CPPFLAGS) -DPATH=DUMMY

#check_mempool
check_SCRIPTS = run_demo.sh \
		check_pm_declare.sh \
		check_pm_declare_group.sh \
		check_pm_interlock.sh \
		check_pm_interlock_async.sh \
		check_subscribe.sh

user_defined_test_schemas = benchmark_attributes.yaml test_attributes.yaml sample_attributes.yaml

EXTRA_DIST = $(check_SCRIPTS) $(user_defined_test_schemas) ecmwf_config.h generate-mio-config.sh demo_mvp_d3_2_config.yaml run_demo.sh 

clean-local: clean-local-check
.PHONY: clean-local-check
clean-local-check:
	-rm -rf $(top_builddir)/tests/CDOs
	-rm -f $(top_builddir)/tests/mio-config*.yaml
	-rm -f $(top_builddir)/tests/m0trace.*
	-rm -f $(top_builddir)/tests/C-handle\ [0-9]*
	-rm -f $(top_builddir)/tests/C-name\ [0-9]*
	-rm -f $(top_builddir)/tests/{Consumer,Producer}-done-CDO
	-rm -f $(top_builddir)/tests/Test\ group


.PHONY: all
all:

if WITH_MIO
check_PROGRAMS += mio-config-default.yaml mio-config-C1.yaml mio-config-C2.yaml mio-config-C1a.yaml mio-config-C2a.yaml mio-config-PM1.yaml mio-config-PM2.yaml mio-config-PM3.yaml mio-config-CINJ.yaml mio-config-CARCH.yaml

# avoid getting weird default values from automake for these
mio_config_default_yaml_SOURCES =# empty
mio_config_PM1_yaml_SOURCES =# empty
mio_config_PM2_yaml_SOURCES =# empty
mio_config_PM3_yaml_SOURCES =# empty
mio_config_C1_yaml_SOURCES =# empty
mio_config_C1a_yaml_SOURCES =# empty
mio_config_C2_yaml_SOURCES =# empty
mio_config_C2a_yaml_SOURCES =# empty
mio_config_CINJ_yaml_SOURCES =# empty
mio_config_CARCH_yaml_SOURCES =# empty

mio-config-default.yaml: generate-mio-config.sh
	./generate-mio-config.sh 101 && mv mio-config.yaml $@
mio-config-PM1.yaml: generate-mio-config.sh
	./generate-mio-config.sh 102 && mv mio-config.yaml $@
mio-config-PM2.yaml: generate-mio-config.sh
	./generate-mio-config.sh 103 && mv mio-config.yaml $@
mio-config-PM3.yaml: generate-mio-config.sh
	./generate-mio-config.sh 104 && mv mio-config.yaml $@
mio-config-C1.yaml: generate-mio-config.sh
	./generate-mio-config.sh 105 && mv mio-config.yaml $@
mio-config-C2.yaml: generate-mio-config.sh
	./generate-mio-config.sh 106 && mv mio-config.yaml $@
mio-config-C1a.yaml: generate-mio-config.sh
	./generate-mio-config.sh 107 && mv mio-config.yaml $@
mio-config-C2a.yaml: generate-mio-config.sh
	./generate-mio-config.sh 108 && mv mio-config.yaml $@
mio-config-CINJ.yaml: generate-mio-config.sh
	./generate-mio-config.sh 109 && mv mio-config.yaml $@
mio-config-CARCH.yaml: generate-mio-config.sh
	./generate-mio-config.sh 110 && mv mio-config.yaml $@

# inject mio config location into test:
AM_TESTS_ENVIRONMENT=  MSTRO_MIO_CONFIG=mio-config-default.yaml; export MSTRO_MIO_CONFIG;
# some tests set their component-name specific config when they run to replace this

endif
