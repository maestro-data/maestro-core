#!/usr/bin/env bash
#
# Run pool manager, a CDO producer task, and an consumer task
# that tries to retrieve a 'AAAAA..' message
#

# aggressive error tracing
set -euo pipefail

# set a workflow name
MSTRO_WORKFLOW_NAME="ecmwf_events_$$"
export MSTRO_WORKFLOW_NAME

# ensure error and warnings get colors
MSTRO_LOG_COLOR_ERRORS=1
export MSTRO_LOG_COLOR_ERRORS

# start pool manager as named pipe
PM_CMD="@top_builddir@/tests/simple_pool_manager"
PRODUCER_CMD="@top_builddir@/tests/ecmwf_events_producer"
CONSUMER_CMD="@top_builddir@/tests/ecmwf_events_consumer"

# start pool manager, connect its output to fd 3:
# (we need to run in a subshell to start a new process group)
exec 3< <(env MSTRO_LOG_LEVEL=${MSTRO_LOG_LEVEL:-3} ${PM_CMD})
PM_PID=$!

terminate () {
    #$1 is PM_PID, $2 is desired exit code Note that killing PM_PID is
    # not enough, the actual PM will likely be a child of this one,
    # and if we kill it it will be reparented to init(1).
    echo "exit, stopping pool manager"
    # close stdout of the pool manager; its heartbeat output on stdout
    # will make it receive a SIGPIPE eventually, which initiates
    # termination.
    exec 3<&-
    echo "waiting pm to terminate"
    wait 
    exit ${2:-99}
}

# ensure USR2 is not caught by this shell
trap '' USR2

# trap error exit of script: close pipe to pool manager and return error
trap "terminate ${PM_PID} 99" err

# read pm info: It comes as a CSV of MSTRO_POOL_INFO; base64 text;
# Potentially we might (later) have the pool manager report changed
# info over time which we could read with the same read command.
#
read -d ';' -u 3 pm_info_varname
read -d ';' -u 3 pm_info
echo "PM info: $pm_info"

if test x"${pm_info_varname}" != xMSTRO_POOL_MANAGER_INFO; then
    exit 99;
fi


MSTRO_POOL_MANAGER_INFO="$pm_info"
export MSTRO_POOL_MANAGER_INFO

# start consumer
(env MSTRO_COMPONENT_NAME="CONSUMER" MSTRO_LOG_COLOR_ERRORS=1 MSTRO_LOG_COLOR="CYAN" ${CONSUMER_CMD} -n) || exit 99  &

# wait for consumer to register subscriptions
sleep 3

# start producer
(env MSTRO_COMPONENT_NAME="PRODUCER" MSTRO_LOG_COLOR_ERRORS=1 MSTRO_LOG_COLOR="BLUE" ${PRODUCER_CMD} -n) || exit 99  &

wait %1 || exit 99
wait %2 || exit 99

# trap normal script termination: close pipe to pool manager
terminate ${PM_PID} 0
