/* -*- mode:c -*- */
/** @file
 ** @brief Simple pool manager application
 *
 * Intended to be run as a standalone application on 1 or 2 cores.
 *
 * At startup will start a pool manager service, print the pool
 * manager info on stdout, and wait for SIGUSR2 to terminate.
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro.h"
#include "maestro/logging.h"
#include "maestro/env.h"

#include <stdbool.h>
#include <stdatomic.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)



#define HEARTBEAT_INTERVAL (5*1) /* seconds */


static void
handle_signal(int signal, siginfo_t *act, void*);

#define NEW 0
#define OLD 1
/* one pair per signal name, NEW and OLD */
static struct sigaction g_sig_action_term[2];
static struct sigaction g_sig_action_usr2[2];
static struct sigaction g_sig_action_intr[2];
static struct sigaction g_sig_action_pipe[2];

static mstro_status
install_signal_handlers(void)
{

  memset(&g_sig_action_term[NEW], 0, sizeof(struct sigaction));
  memset(&g_sig_action_usr2[NEW], 0, sizeof(struct sigaction));
  memset(&g_sig_action_intr[NEW], 0, sizeof(struct sigaction));
  memset(&g_sig_action_pipe[NEW], 0, sizeof(struct sigaction));

  g_sig_action_usr2[NEW].sa_sigaction = handle_signal;
  g_sig_action_usr2[NEW].sa_flags = SA_RESTART | SA_SIGINFO;
  sigemptyset(&g_sig_action_usr2[NEW].sa_mask);

  g_sig_action_term[NEW].sa_sigaction = handle_signal;
  g_sig_action_term[NEW].sa_flags = SA_RESTART | SA_SIGINFO;
  sigemptyset(&g_sig_action_term[NEW].sa_mask);

  g_sig_action_intr[NEW].sa_sigaction = handle_signal;
  g_sig_action_intr[NEW].sa_flags = SA_RESTART | SA_SIGINFO;
  sigemptyset(&g_sig_action_intr[NEW].sa_mask);

  g_sig_action_pipe[NEW].sa_sigaction = handle_signal;
  g_sig_action_pipe[NEW].sa_flags = SA_RESTART | SA_SIGINFO;
  sigemptyset(&g_sig_action_pipe[NEW].sa_mask);

  sigaction(SIGUSR2, &g_sig_action_usr2[NEW], &g_sig_action_usr2[OLD]);
  sigaction(SIGTERM, &g_sig_action_term[NEW], &g_sig_action_term[OLD]);
  sigaction(SIGINT,  &g_sig_action_intr[NEW], &g_sig_action_intr[OLD]);
  sigaction(SIGPIPE, &g_sig_action_pipe[NEW], &g_sig_action_pipe[OLD]);

  return MSTRO_OK;
}

/* not strictly necessary for this use case, but shows how to do it
 * right in general */
static mstro_status
restore_signal_handlers(void)
{

  sigaction(SIGUSR2, &g_sig_action_usr2[OLD], NULL);
  sigaction(SIGTERM, &g_sig_action_term[OLD], NULL);
  sigaction(SIGINT,  &g_sig_action_intr[OLD], NULL);
  sigaction(SIGPIPE, &g_sig_action_pipe[OLD], NULL);

  return MSTRO_OK;
}



/* flag to check for termination request */
static _Atomic bool g_termination_requested; /* static init: false */
/* signal causing termination */
static volatile sig_atomic_t g_last_signal;
/* lock to protect multiple signal handlers from racing against each other */
static atomic_flag g_termination_handler_ran = ATOMIC_FLAG_INIT;

/* Mixing signals and pthreads is tricky. The signal can be caught by
 * all threads inheriting the signal handler setup, so we should
 * either have only one dedicated thread or be prepared for concurrent
 * ops. We go with the latter and make sure the handler is one-shot */
static void
handle_signal(int signal, siginfo_t *act, void * oact)
{
  if(false==atomic_flag_test_and_set(&g_termination_handler_ran)) {
    /* we're first */
    atomic_store(&g_termination_requested,true);
    g_last_signal = signal;
    /* restore signal handlers; if that fails we give up */
    mstro_status s = restore_signal_handlers();
    if(s!=MSTRO_OK)
      abort();
  } else {
    /* someone other invocation of the handler was first -- that's fine */
    ;
  }
}

int
main(int argc, char ** argv)
{
  unsigned int heartbeat_interval = HEARTBEAT_INTERVAL;

#ifdef MPMD
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  if(provided < MPI_THREAD_MULTIPLE)
  {
          printf("The threading support level is lesser than that demanded.\n");
          MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
  }

  int size;
  
  MPI_Group world_group;
  MPI_Comm_group(MPI_COMM_WORLD, &world_group);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // Keep only the process 0 in the new group.
  int ranks[1] = {size-1};
  MPI_Group new_group;
  MPI_Group_incl(world_group, 1, ranks, &new_group);

  // Create the new communicator from that group of processes.
  MPI_Comm comm;
  MPI_Comm_create(MPI_COMM_WORLD, new_group, &comm);
 
  if(comm == MPI_COMM_NULL)
  {
	  MPI_Finalize();
	return EXIT_SUCCESS;
  }

#endif

  mstro_status s = mstro_init(NULL, "Simple_Pool_Manager", 0);
  if(s!=MSTRO_OK) {
    ERR("Simple Maestro Pool Manager: Failed to initialize Maestro: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }

  s = mstro_pm_start();
  if(s!=MSTRO_OK) {
    ERR("Simple Maestro Pool Manager: Failed to start pool: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }
  
  s = install_signal_handlers();
  if(s!=MSTRO_OK) {
    ERR("Simple Maestro Pool Manager: Failed to setup signal handlers: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }

  char *info = NULL;
  s = mstro_pm_getinfo(&info);
  if(s!=MSTRO_OK) {
    ERR("Simple Maestro Pool Manager: Failed to obtain pool contact info: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }

        
  while(!atomic_load(&g_termination_requested)) {
    DEBUG("Pool manager alive\n");
    /* print pool info in a format suitable for use in shell scripts: 2
       CSV entries with semicolon as field sep (which is a safe
       separator to use for posix shell in  a
         read -d ';' PM_VAR_NAME PM_INFO
       command.

       Since we repeat this every once in a while we can both present
       updated info (once we have distributed pools) and more
       importantly will notice SIGPIE if the caller no longer listens
       to us (like check_pm_declare.sh which puts us into a process
       substitution subprocess and closes the file descriptor to
       inform us we can terminate)
    */
    
    fprintf(stdout, "%s;%s;\n", MSTRO_ENV_POOL_INFO, info);
    fflush(stdout);

    /* sleep; signal will interrup this, handler will set flag and
     * we'll exit the loop */
    if(heartbeat_interval>0) 
      sleep(HEARTBEAT_INTERVAL);
    else
      pause();
  }
  DEBUG("Simple Maestro Pool: Terminating (caught signal %s)\n",
        strsignal(g_last_signal));

  free(info);

  s = mstro_pm_terminate();
  if(s!=MSTRO_OK) {
    ERR("Simple Maestro Pool Manager: Failed to shut down pool: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }

  s = mstro_finalize();
  if(s!=MSTRO_OK) {
    ERR("Simple Maestro Pool Maestro: failed to terminate: %d (%s)\n",
        s, mstro_status_description(s));
    goto BAILOUT;
  }

  /* re-raise the signal that we caught. */
  kill(getpid(),g_last_signal);

#ifdef MPMD
  MPI_Finalize();
#endif

  s = MSTRO_OK;
BAILOUT:
  if(s!=MSTRO_OK)
    return EXIT_FAILURE;
  else
    return EXIT_SUCCESS;
}
