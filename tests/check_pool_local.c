/* -*- mode:c -*- */
/** @file
 ** @brief simple check local pool
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>


CHEAT_TEST(cdo_local_pool_works,
           cheat_assert(MSTRO_OK == mstro_init("Tests","POOL",0));
           char name[] = "Test CDO name üÜöÖäÄ";
           mstro_cdo cdo=NULL;
           cheat_assert(MSTRO_OK
                        == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_offer(cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_withdraw(cdo));
           cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo));
           cheat_assert(MSTRO_OK == mstro_finalize());
           )

CHEAT_TEST(cdo_local_pool_async_works,
          cheat_assert(MSTRO_OK == mstro_init("Tests","POOL",0));
          char name[] = "Test CDO name üÜöÖäÄ";
          mstro_cdo cdo=NULL;
          mstro_cdo c1,c2,c3;
          mstro_request request, r1,r2,r3;
          cheat_assert(MSTRO_OK
                 == mstro_cdo_declare_async(name, MSTRO_ATTR_DEFAULT, &cdo, &request));
          /*declare multiple cdos async*/
          cheat_assert(MSTRO_OK
                 == mstro_cdo_declare_async("c1", MSTRO_ATTR_DEFAULT, &c1, &r1));
          cheat_assert(MSTRO_OK
                        == mstro_cdo_declare_async("c2", MSTRO_ATTR_DEFAULT, &c2, &r2));
          cheat_assert(MSTRO_OK
                        == mstro_cdo_declare_async("c3", MSTRO_ATTR_DEFAULT, &c3, &r3));

          cheat_assert(MSTRO_OK == mstro_request_wait(request));
          cheat_assert(MSTRO_OK == mstro_request_wait(r1));
          cheat_assert(MSTRO_OK == mstro_request_wait(r2));
          cheat_assert(MSTRO_OK == mstro_request_wait(r3));

          /**Request is freed by the wait, the request is invalid at this point*/
          /** FIXME asan complains if we chech freeed pointers, i.e., request
          cheat_assert(false == mstro_request_test(request)); */

          cheat_yield();

          /** cdo must be sealed before calling async offer*/
          cheat_assert(MSTRO_INVARG == mstro_cdo_offer_async(cdo,&request));
          cheat_yield();
          cheat_assert(MSTRO_OK == mstro_cdo_seal_async(cdo, &request));
          cheat_assert(MSTRO_OK == mstro_cdo_seal_async(c1, &r1));
          cheat_assert(MSTRO_OK == mstro_cdo_seal_async(c2, &r2));
          cheat_assert(MSTRO_OK == mstro_cdo_seal_async(c3, &r3));
          cheat_assert(MSTRO_OK == mstro_request_wait(request));
          cheat_assert(MSTRO_OK == mstro_request_wait(r1));
          cheat_assert(MSTRO_OK == mstro_request_wait(r2));
          cheat_assert(MSTRO_OK == mstro_request_wait(r3));
          cheat_assert(MSTRO_OK == mstro_cdo_offer_async(cdo,&request));
          cheat_assert(MSTRO_OK == mstro_cdo_require_async(c1,&r1));
          cheat_assert(MSTRO_OK == mstro_cdo_require_async(c2,&r2));
          cheat_assert(MSTRO_OK == mstro_cdo_require_async(c3,&r3));
          cheat_assert(MSTRO_OK == mstro_request_wait(request));
   	  cheat_assert(MSTRO_OK == mstro_request_wait(r1));
          cheat_assert(MSTRO_OK == mstro_request_wait(r2));
          cheat_assert(MSTRO_OK == mstro_request_wait(r3));
          cheat_yield();
          cheat_assert(MSTRO_OK == mstro_cdo_withdraw_async(cdo,&request));
          mstro_request_wait(request);
          cheat_yield();
          cheat_assert(MSTRO_OK == mstro_cdo_retract_async(c1,&r1));
          cheat_assert(MSTRO_OK == mstro_cdo_retract_async(c2,&r2));
          cheat_assert(MSTRO_OK == mstro_cdo_retract_async(c3,&r3));
          cheat_assert(MSTRO_OK == mstro_request_wait(r1));
          cheat_assert(MSTRO_OK == mstro_request_wait(r2));
          cheat_assert(MSTRO_OK == mstro_request_wait(r3));
          cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo));
          cheat_assert(MSTRO_OK == mstro_cdo_dispose(c1));
          cheat_assert(MSTRO_OK == mstro_cdo_dispose(c2));
          cheat_assert(MSTRO_OK == mstro_cdo_dispose(c3));
          cheat_assert(MSTRO_OK == mstro_finalize());
          )
