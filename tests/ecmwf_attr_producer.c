#include <stdio.h>
#include <assert.h>
#include "maestro.h"

#include "ecmwf_config.h"
#include <unistd.h>

int main(int argc, char *argv[])
{
    mstro_status s = MSTRO_OK;	
    s = mstro_init(getenv("MSTRO_WORKFLOW_NAME"), getenv("MSTRO_COMPONENT_NAME"), 0);
    assert(s == MSTRO_OK);

    mstro_cdo cdo = NULL;
    s = mstro_cdo_declare("my-cdo-1", MSTRO_ATTR_DEFAULT, &cdo);
    assert(s == MSTRO_OK);
    int64_t step = 2;
    int64_t number = 1;
    s  = mstro_cdo_attribute_set(cdo, ".maestro.ecmwf.step", (void **)&step, true, false);
    s |= mstro_cdo_attribute_set(cdo, ".maestro.ecmwf.number", (void **)&number, true, false);
    s |= mstro_cdo_seal(cdo);
    s |= mstro_cdo_offer(cdo);
    assert(s == MSTRO_OK);
    sleep(5); // sleep until we sync with startup of consumer (=fix acknowledge issue)
    s  = mstro_cdo_withdraw(cdo);
    s |= mstro_cdo_dispose(cdo);
    s |= mstro_finalize();
    assert(s == MSTRO_OK);

    return 0;    
}
