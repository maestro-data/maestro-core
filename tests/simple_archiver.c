/* Connect to pool and subscribe to many events, archiving all CDOs */
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_statistics.h"
#include "maestro/logging.h"
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include "maestro/i_utlist.h"
#include "maestro/i_uthash.h"
#include "maestro/i_misc.h"


#ifndef PATH
#error You need to define PATH when compiling this file
#endif

#define XSTRINGIFY(s) #s
#define STRINGIFY(s) XSTRINGIFY(s)


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)



CHEAT_DECLARE(
    const mstro_nanosec_t  MAX_WAIT = ((mstro_nanosec_t)360)*1000*1000*1000; /* 10s */
    struct cdo_entry {
      UT_hash_handle hh;
      char *name; /**< key in hash table */
      bool demanded;
      mstro_cdo cdo;
      mstro_request request;
    };
    struct cdo_entry *g_cdo_table=NULL;

    mstro_status
    write_cdo_to_disk(mstro_cdo cdo,const char *path)
    {
      mstro_status status;
      /* if path is "DUMMY" we do not write to disk */
      if (strcmp(path,"DUMMY") == 0) {
        DEBUG("DUMMY We will not write any thing there \n");
        return MSTRO_OK;
      } else{
        /* make a dir at the specified path*/
        status = mkdirhier(path);
        if (status != MSTRO_OK) {
          ERR("Failed to setup a directory for file transport\n");
          return status;
        }
        FILE *f = NULL;
        const char *cdo_name = mstro_cdo_name(cdo);
        size_t path_len = strlen(cdo_name) + strlen(path) + 2; /* \0 and // for the path*/
        char *cdo_path = malloc(sizeof(char)*path_len);
        sprintf(cdo_path,"%s/%s", path, cdo_name);

        DEBUG("Path for archiving CDOs: %s\n", cdo_path);
        if (cdo_path == NULL) {
          ERR("Invalid archiving path\n");
          status = MSTRO_INVARG;
          goto BAILOUT;
        }

        f = fopen(cdo_path,"w");
        if (f == NULL) {
          ERR("Failed to open %s for archiving CDOs (errno: %d -- %s)\n",
              cdo_path, errno, strerror(errno));
          status = MSTRO_FAIL;
          goto BAILOUT;
        }

        /* Obtain cdo data raw pointer and size of data*/
        void * data;
        mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_RAW_PTR, NULL,(const void **) &data);
        
        int64_t *tmp;
        mstro_cdo_attribute_get(cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, NULL,(const void **) &tmp);
        size_t data_len = (size_t) *tmp;
        DEBUG("CDO %s size is %zu \n", cdo_name, data_len);
        size_t bytes_written = fwrite(data, sizeof(char), data_len, f); 
	      if (bytes_written != data_len) {
	        ERR("Partial write on %s for archiving CDO %s (errno: %d -- %s)\n",
                    cdo_path,cdo_name, errno, strerror(errno));
	        status = MSTRO_FAIL;
          goto BAILOUT;
	      }

	      if (fclose(f) != 0) {
		      ERR("Failed to close %s for archiving the CDO (errno: %d -- %s)\n",
                    cdo_path, errno, strerror(errno));
		      status = MSTRO_FAIL;
          goto BAILOUT;
	      }
        status = MSTRO_OK;
        BAILOUT:
          free(cdo_path);
          return status;
      }
    }
    )

CHEAT_TEST(simple_archiver,

           mstro_cdo cdo;
           
           cheat_assert(MSTRO_OK == mstro_init(NULL,NULL,0));
           cheat_yield();

           mstro_cdo_selector selector=NULL;
           mstro_cdo_selector ecmwf_selector=NULL;
           mstro_subscription cdo_subscription=NULL;
           mstro_subscription ecmwf_subscription=NULL;
           mstro_subscription join_leave_subscription=NULL;
           
           cheat_assert(MSTRO_OK
                        == mstro_cdo_selector_create(
                            NULL, NULL,
                            "(has .maestro.core.cdo.name)",
                            &selector));
           cheat_yield();
           cheat_assert(MSTRO_OK
                        == mstro_cdo_selector_create(
                            NULL, NULL,
                            "(.maestro.ecmwf.param = 2)",
                            &ecmwf_selector));
           cheat_yield();
           
           cheat_assert(MSTRO_OK
                        == mstro_subscribe(selector,
                                           /*MSTRO_POOL_EVENT_DECLARE*/
                                           MSTRO_POOL_EVENT_OFFER
                                           /* add as they are implemented: */
                                           /* |MSTRO_POOL_EVENT_DISPOSE */
                                           /*|MSTRO_POOL_EVENT_SEAL*/
                                           /* |MSTRO_POOL_EVENT_DEMAND */
                                           /*|MSTRO_POOL_EVENT_REQUIRE*/
                                           /* |MSTRO_POOL_EVENT_RETRACT*/
                                           /*|MSTRO_POOL_EVENT_WITHDRAW*/
                                           ,true, /**FIXME should specify !self to avoid recieving event notification about own events*/
                                           &cdo_subscription));
           cheat_yield();
           cheat_assert(MSTRO_OK
                        == mstro_subscribe(ecmwf_selector,
                                           // just a few here
                                           MSTRO_POOL_EVENT_OFFER
                                           |MSTRO_POOL_EVENT_WITHDRAW
                                           ,true,
                                           &ecmwf_subscription));
           cheat_yield();
           
           cheat_assert(MSTRO_OK
                        == mstro_cdo_selector_dispose(selector));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_selector_dispose(ecmwf_selector));
           cheat_yield();
           
           cheat_assert(MSTRO_OK
                        == mstro_subscribe(
                            NULL,
                            ( MSTRO_POOL_EVENT_APP_JOIN
                              | MSTRO_POOL_EVENT_APP_LEAVE),
                            true,
                            &join_leave_subscription));

           bool done = false;

           mstro_nanosec_t starttime = mstro_clock();
           
           while(!done) {
             if(mstro_clock()>starttime+MAX_WAIT) {
               fprintf(stderr, "Waited %" PRIu64 "s and still not done\n",
                       MAX_WAIT/(1000*1000*1000));
               cheat_assert((((void)"Completion within reasonable time"),
                             false));
               cheat_yield();
             }
             
             if(done)
               break; /* from WHILE */

             mstro_pool_event e=NULL;
             
POLL_JOIN_LEAVE:
             cheat_assert(MSTRO_OK==mstro_subscription_poll(
                 join_leave_subscription, &e));
             DEBUG("join/leave poll: %p\n", e);
             if(e!=NULL) {
               fprintf(stdout, "JOIN/LEAVE event(s)\n");
               mstro_pool_event tmp=e;
               /* handle all */
               while(tmp) {
                 switch(tmp->kind) {
                   case MSTRO_POOL_EVENT_APP_JOIN:
                     fprintf(stdout, "Noticed JOIN event of app %s\n",
                             tmp->payload.join.component_name);
                     break;
                   case MSTRO_POOL_EVENT_APP_LEAVE:
                     fprintf(stdout, "Noticed LEAVE event of app %d\n",
                             tmp->payload.leave.appid);
                     done = true;
                     DEBUG("will terminate\n");
                     break;
                   default:
                     fprintf(stderr, "Unexpected event %d\n", tmp->kind);
                     cheat_assert(false);
                 }
                 fflush(stdout);
                 tmp=tmp->next;
               }
               /* acknowledge all */
               cheat_assert(MSTRO_OK==mstro_subscription_ack(
                   join_leave_subscription, e));
               /* dispose all */
               cheat_assert(MSTRO_OK==mstro_pool_event_dispose(e));
               continue;
             }
POLL_CDOS:
             // try cdo subscription
             cheat_assert(MSTRO_OK==mstro_subscription_poll(
                 cdo_subscription, &e));
             DEBUG("CDO op poll: %p\n", e);
             if(e) {
               struct cdo_entry *cdo_e=NULL;
               mstro_pool_event tmp=e;
               /* handle all */
               while(tmp) {
                 const char *event_name=NULL;
                 const char *cdo_name=NULL;
                 event_name = mstro_pool_event_description(tmp->kind);

                 fprintf(stderr, "event kind: %s\n", event_name);
                 switch(tmp->kind) {
                   case MSTRO_POOL_EVENT_OFFER:
                    cdo_name = tmp->payload.offer.cdo_name;
                    /* check if we have already seen the cdo */
                    HASH_FIND_STR(g_cdo_table, cdo_name, cdo_e);
                    if (cdo_e == NULL) {
                      /* make a new entry*/
                      cdo_e=malloc(sizeof(struct cdo_entry));
                      cheat_assert(cdo_e!=NULL);
                      //INFO("Allocated cdo_e %p\n", cdo_e);
                      cdo_e->name = strdup(cdo_name);
                      cdo_e->demanded = false; /*init*/
                      cheat_assert(cdo_e->name!=NULL);
                      HASH_ADD_STR(g_cdo_table, name, cdo_e);
                      cheat_assert(MSTRO_OK==mstro_cdo_declare(cdo_e->name,
                                                                MSTRO_ATTR_DEFAULT,
                                                                &cdo_e->cdo));
                     }                   
                     /* Immediately post a REQUIRE and DEMAND for it */
                     cheat_assert(MSTRO_OK == mstro_cdo_require(cdo_e->cdo));
                     cheat_assert(MSTRO_OK == mstro_cdo_demand_async(cdo_e->cdo,
                                                                     &cdo_e->request));
                     cdo_e->demanded = true;
                     break;
                     
                   case MSTRO_POOL_EVENT_DECLARE:
                     cdo_name = tmp->payload.declare.cdo_name;
                     break;
                   case MSTRO_POOL_EVENT_DISPOSE:
                     cdo_name = tmp->payload.dispose.cdo_name;
                     break;
                   case MSTRO_POOL_EVENT_SEAL:
                     cdo_name = tmp->payload.seal.cdo_name;
                     break;
                   case MSTRO_POOL_EVENT_DEMAND:
                     cdo_name = tmp->payload.demand.cdo_name;
                     break;
                   case MSTRO_POOL_EVENT_REQUIRE:
                     cdo_name = tmp->payload.require.cdo_name;
                     break;
                   case MSTRO_POOL_EVENT_RETRACT:
                     cdo_name = tmp->payload.retract.cdo_name;
                     break;
                   case MSTRO_POOL_EVENT_WITHDRAW:
                     cdo_name = tmp->payload.withdraw.cdo_name;
                     break;
                   default:
                     fprintf(stderr, "Unexpected CDO event %d\n", tmp->kind);
                 }
                 fprintf(stdout, "CDO event %s for CDO |%s|\n",
                         event_name, cdo_name ? cdo_name : "??");
                 
                 tmp=tmp->next;
               }
               /* acknowledge all */
               cheat_assert(MSTRO_OK==mstro_subscription_ack(
                   cdo_subscription, e));
               /* dispose all */
               cheat_assert(MSTRO_OK==mstro_pool_event_dispose(e));
               continue;
             }

POLL_ECMWF_PARAM:
             cheat_assert(MSTRO_OK==mstro_subscription_poll(
                 ecmwf_subscription, &e));
             DEBUG("ECMWF int-param op poll: %p\n", e);
             if(e) {
               mstro_pool_event tmp=e;
               /* handle all */
               while(tmp) {
                 const char *event_name=NULL;
                 const char *cdo_name=NULL;
                 event_name = mstro_pool_event_description(tmp->kind);
                 
                 switch(tmp->kind) {
                   case MSTRO_POOL_EVENT_OFFER:
                     cdo_name = tmp->payload.offer.cdo_name;
                     break;
                     
                   case MSTRO_POOL_EVENT_DECLARE:
                     cdo_name = tmp->payload.offer.cdo_name;
                     break;
                   default:
                     fprintf(stderr, "Unexpected CDO event %d\n", tmp->kind);
                 }
                 fprintf(stdout, "CDO event %s for CDO |%s|, based on .mstro.ecmwf.param==2 subscription\n",
                         event_name, cdo_name ? cdo_name : "??");
                 
                 tmp=tmp->next;
               }
               /* acknowledge all */
               cheat_assert(MSTRO_OK==mstro_subscription_ack(
                   ecmwf_subscription, e));
               /* dispose all */
               cheat_assert(MSTRO_OK==mstro_pool_event_dispose(e));
               continue;
             }
NOTHING_AVAILABLE:
               //fprintf(stdout, "No event available, looping\n");

               /* NULL e should give an error, even if subscription
                * itself would like ack */
TRY_ILLEGAL_ACK:
             cheat_assert(MSTRO_OK!=mstro_subscription_ack(
                 join_leave_subscription, e));
             /* sleep(1); */
           }
           DEBUG("Left polling loop\n");
           
           /* loop over catched CDOs */
           struct cdo_entry *cdo_e=NULL;
           struct cdo_entry *tmp=NULL;
           HASH_ITER(hh, g_cdo_table, cdo_e, tmp) {
             if(cdo_e->demanded) {
               /* wait for the demand to complete*/
               cheat_assert(MSTRO_OK == mstro_request_wait(cdo_e->request));
               /* write cdo to disk here*/
               char *path = STRINGIFY(PATH);
               cheat_assert(MSTRO_OK == write_cdo_to_disk(cdo_e->cdo, path));
             }
             cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_e->cdo));
             free(cdo_e->name);
             HASH_DEL(g_cdo_table,cdo_e);
             free(cdo_e);
             //INFO("Deallocated cdo_e %p\n", cdo_e);
           }
           
           cheat_assert(MSTRO_OK
                        == mstro_subscription_dispose(cdo_subscription));
           cheat_assert(MSTRO_OK
                        == mstro_subscription_dispose(ecmwf_subscription));
           cheat_assert(MSTRO_OK
                        == mstro_subscription_dispose(join_leave_subscription));
           cheat_assert(MSTRO_OK == mstro_finalize());
           )



