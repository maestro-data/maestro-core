/* -*- mode:c -*- */
/** @file
 ** @brief Connect to pool manager and offer/demand CDOs in sequence
 **/

/*
 * Copyright (C) 2021 HPE Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_utlist.h"
#include "maestro/i_uthash.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/** Client-args is a list of triples
    op name size
    where OP is one of DECLAR SEAL OFFER RETRACT WITHDRAW DISPOSE REQUIRE DEMAND
    name is a valid 1-word CDO name
    and size is a numeric CDO size, -1 for 'not specified'

    Example: DECLARE CDO1 1025  SEAL CDO1 -1  OFFER CDO1 -1  WITHDRAW CDO1 -1  DISPOSE CDO1 -1

**/
#ifndef CLIENT_ARGS
#error You need to define CLIENT_ARGS when compiling this file
#endif


#define XSTRINGIFY(s) #s
#define STRINGIFY(s) XSTRINGIFY(s)


CHEAT_DECLARE(

    char *g_client_args= STRINGIFY(CLIENT_ARGS);

    size_t iterations_count = 0;
    size_t item_count = 0;

    enum cdo_op {
      DECLARE_OP, SEAL_OP, OFFER_OP,
      REQUIRE_OP, DEMAND_OP,
      RETRACT_OP,
      WITHDRAW_OP, DISPOSE_OP, WAIT_OP
    };
    struct cdo_step {
      struct cdo_step *next;
      char *name;
      ssize_t size;
      enum cdo_op op;
    };

    struct cdo_step *g_steps = NULL;

    int
    parse_args(char *triples) {
      int num_ops=0;
      char *sep = " \t";
      char *word;
      int idx;
      fprintf(stderr, "test steps: |%s|\n", triples);
      char *tmp = strdup(triples);
      cheat_assert(tmp!=NULL);
      struct cdo_step *s=NULL;

      for(idx=0,word=strtok(tmp, sep);
          word;
          idx++,word=strtok(NULL, sep)) {
        if(idx%3==0) {
          s = malloc(sizeof(struct cdo_step));
          cheat_assert(s!=NULL);
          s->next = g_steps;
          g_steps = s;
        }
        switch(idx%3) {
          case 0:
            /* expect OP. Only compare significant characters */
            if(strncasecmp(word,"DECLARE",3)==0) {
              s->op = DECLARE_OP;
            } else if(strncasecmp(word, "SEAL",1)==0) {
              s->op = SEAL_OP;
            } else if(strncasecmp(word, "OFFER",1)==0) {
              s->op = OFFER_OP;
            } else if(strncasecmp(word, "REQUIRE",3)==0) {
              s->op = REQUIRE_OP;
            } else if(strncasecmp(word, "DEMAND",3)==0) {
              s->op = DEMAND_OP;
            } else if(strncasecmp(word, "RETRACT",3)==0) {
              s->op = RETRACT_OP;
            } else if(strncasecmp(word, "WITHDRAW",2)==0) {
              s->op = WITHDRAW_OP;
            } else if(strncasecmp(word, "DISPOSE",2)==0) {
              s->op = DISPOSE_OP;
            } else if(strncasecmp(word, "WAIT",2)==0) {
              s->op = WAIT_OP;
            } else {
              fprintf(stderr, "Invalid CDO op %s\n", word);
              cheat_assert(0);
            }
            break;
          case 1:
            /* expect CDO name */
            s->name = strdup(word);
            cheat_assert(s->name!=NULL);
            cheat_assert(s->name[0]!='\0');
            break;
          case 2:
            /* expect size */
            s->size = atoi(word);
            break;
        };
        /* fprintf(stderr, "looking at |%s|\n", word); */
      }
      //fprintf(stderr, "idx is %d\n", idx);
      cheat_assert(0==idx%3);

      /* This should be available as LL_REVERSE */
      struct cdo_step *head=NULL;
      struct cdo_step *tail=g_steps;
      while(tail!=NULL) {
        struct cdo_step *tt = tail->next;
        tail->next = head;
        head = tail;
        tail = tt;
      }
      g_steps = head;

      free(tmp);
      return 0;
    }

    struct cdo_entry {
      UT_hash_handle hh;
      char *name; /**< key in hash table */
      uint8_t *buf;
      mstro_cdo cdo;
      mstro_request request;
    };
    struct cdo_entry *g_cdo_table=NULL;

    int
    execute_steps(struct cdo_step *steps) {
      struct cdo_step *s, *tmp;
      size_t num_steps=0;
      LL_COUNT(steps,tmp,num_steps);
      LL_FOREACH(steps, s) {
        fprintf(stderr, "Doing %d on %s size %d\n", s->op, s->name, s->size);
        fflush(stderr);
        struct cdo_entry *e=NULL;
        switch(s->op) {
          case WAIT_OP:
                HASH_FIND_STR(g_cdo_table, s->name, e);
                cheat_assert(e!=NULL);
                cheat_assert(MSTRO_OK == mstro_request_wait(e->request));
              break;
          case DECLARE_OP:
            e=malloc(sizeof(struct cdo_entry));
            cheat_assert(e!=NULL);
            e->name = strdup(s->name);
            int retval = posix_memalign((void**)&e->buf, (size_t) sysconf(_SC_PAGESIZE), sizeof(uint8_t)*s->size);
			      cheat_assert(retval==0);
            cheat_assert(e->name!=NULL);

            HASH_ADD_STR(g_cdo_table, name, e);

            cheat_assert(MSTRO_OK==mstro_cdo_declare_async(e->name,
                                                     MSTRO_ATTR_DEFAULT,
                                                     &e->cdo,&e->request));
            cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(
                e->cdo,
                MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                &s->size, true, false));
            cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(
                e->cdo,
                MSTRO_ATTR_CORE_CDO_RAW_PTR,
                e->buf, false, true));
            fprintf(stderr, "declared %s, handle %x\n",
                    e->name, e->cdo);
            break;
          case SEAL_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_seal_async(e->cdo, &e->request));
            break;
          case OFFER_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_offer_async(e->cdo, &e->request));
            break;

          case WITHDRAW_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_withdraw_async(e->cdo, &e->request));
            break;
          case REQUIRE_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_require_async(e->cdo, &e->request));
            break;
          case DEMAND_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_demand_async(e->cdo, &e->request));
            break;
          case RETRACT_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_retract_async(e->cdo, &e->request));
            break;
          case DISPOSE_OP:
            HASH_FIND_STR(g_cdo_table, s->name, e);
            cheat_assert(e!=NULL);
            cheat_assert(MSTRO_OK==mstro_cdo_dispose(e->cdo));
            break;
          default:
            fprintf(stderr, "invalid ip %d\n", s->op);
            cheat_assert(0);
        }

      }
      return 0;
    }
      )

CHEAT_TEST(simple_interlock_async_client,
           cheat_assert(MSTRO_OK == mstro_init(NULL,NULL,0));
           cheat_yield();
           cheat_assert(0==parse_args(g_client_args));
           cheat_assert(0==execute_steps(g_steps));
           cheat_assert(MSTRO_OK == mstro_finalize());
           )
