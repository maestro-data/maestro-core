/* check maestro dispose and reuse */
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"

#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>
#include "maestro/logging.h"

#define N 10

CHEAT_TEST(dispose_reuse_works,
  size_t data_count = N*N;
  int64_t bytes = data_count*sizeof(double);
  double* src_data = malloc(bytes);
  for(size_t i=0; i<data_count; i++) {
    src_data[i]=random();
  }
  enum mstro_cdo_attr_value_type type;
  const void* val;

  cheat_assert(MSTRO_OK == mstro_init("Tests","DISPOSE_AND_REUSE",0));
  char name[] = "recycling_pioneer";

  mstro_cdo cdo_src=NULL;

  cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                   src_data, false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                   (void**)&bytes, true, false));
  int64_t user_attr = 0;
  int i;
  for (i=0; i<10; i++) {
    user_attr++;
  /* We'll hijack the ELEMENT_SIZE attribute for this test's purpose.  
	The aim is to show the user can fiddle with attributes even after the CDO
	has been SEALED/OFFERED/WITHDRAWN, without re-declaring and setting the
	attributes all over again */
    cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src, 
                                                     MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE, &user_attr,
                                                     true, false));
 
    cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(
                            cdo_src, MSTRO_ATTR_CORE_CDO_LAYOUT_ELEMENT_SIZE, &type, &val));
           fprintf(stdout, "CDO %s *user_attr* has now value: %d\n",
                   name,*(const int64_t*)val);
  
  /* Producer side (there won't be any consumer here) */
    cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_src));
    cheat_assert(MSTRO_OK == mstro_cdo_offer(cdo_src));
    cheat_assert(MSTRO_OK == mstro_cdo_withdraw(cdo_src));

  /* unsealing *cdo_src* */
    cheat_assert(MSTRO_OK == mstro_cdo_dispose_and_reuse(cdo_src));
  }

  /* Free resources */
  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_src));
  free(src_data);

  cheat_assert(MSTRO_OK == mstro_finalize());

  )



