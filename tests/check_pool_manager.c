/* -*- mode:c -*- */
/** @file
 ** @brief  check pool manager startup
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif
#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif
#define _POSIX_C_SOURCE 200112L


#include "cheat.h"

#include "maestro.h"
#include "maestro/pool_manager.h"
#include "maestro/env.h"
#include <string.h>
#include <stdio.h>

/* to simplify the test (i.e., not run MPI or somesuch) we run two threads: one pool manager, one that terminates it */
#include <pthread.h>




CHEAT_TEST(cdo_start_stop_pool_manager,
           cheat_assert(MSTRO_OK == mstro_init("start_stop_pm","test",0));
	   cheat_assert(MSTRO_OK == mstro_pm_start());
           char *info=NULL;
           cheat_assert(MSTRO_OK == mstro_pm_getinfo(&info));
           printf("Got PM info: |%s|\n", info);
           free(info);
	   cheat_assert(MSTRO_OK == mstro_pm_terminate());
           cheat_assert(MSTRO_OK == mstro_finalize());
           )

CHEAT_TEST(cdo_start_stop_pool_manager_implicit,
           /* do implicit pm start */
           setenv(MSTRO_ENV_START_POOL_MANAGER, "YES", 1);
           cheat_assert(MSTRO_OK == mstro_init("auto_start_stop_pm","test",0));
           cheat_assert(MSTRO_OK == mstro_finalize());
           )




