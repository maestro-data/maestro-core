/* -*- mode:c -*- */
/** @file
 ** @brief check uuid handling
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_uuid.h"


CHEAT_TEST(uuid_version,
       cheat_assert(0x106203==uuid_version());
	)
CHEAT_TEST(uuid_create_export,
       uuid_t *uuid1;
        cheat_assert(UUID_RC_OK==uuid_create(&uuid1));
        cheat_assert(UUID_RC_OK==uuid_load(uuid1,"nil"));

       char *str = NULL;
       cheat_assert(UUID_RC_OK==uuid_export(uuid1,UUID_FMT_STR,&str, NULL));
       free(str);
       cheat_assert(UUID_RC_OK==uuid_destroy(uuid1));
       )


CHEAT_TEST(uuid_create_dup,
       uuid_t *uuid1, *uuid2;
       cheat_assert(UUID_RC_OK==uuid_create(&uuid1));
       cheat_assert(UUID_RC_OK==uuid_load(uuid1,"nil"));

       cheat_assert(UUID_RC_OK==uuid_clone(uuid1, &uuid2));
       cheat_assert(UUID_RC_OK!=uuid_load(uuid2,"foo"));

       cheat_assert(UUID_RC_OK==uuid_destroy(uuid1));
       cheat_assert(UUID_RC_OK==uuid_destroy(uuid2));
	)

  CHEAT_TEST(uuid_compare,
         uuid_t *uuid1, *uuid2;

         int result = -1;
         cheat_assert(UUID_RC_OK==uuid_create(&uuid1));
         cheat_assert(UUID_RC_OK==uuid_load(uuid1,"nil"));


         cheat_assert(UUID_RC_OK==uuid_clone(uuid1, &uuid2));
         /*Test cloned uuids if they are the same*/
         uuid_compare(uuid1, uuid2, &result);
         cheat_assert(result==0);


         cheat_assert(UUID_RC_OK==uuid_destroy(uuid1));
         uuid1 = NULL; /*set the pointer manually to NULL to avoid further use*/

         cheat_assert(UUID_RC_OK==uuid_compare(uuid1, uuid2, &result));
         cheat_assert(UUID_RC_OK==uuid_compare(uuid2, uuid1, &result));
         cheat_assert(UUID_RC_OK==uuid_destroy(uuid2));
         uuid2 = NULL; /*set the pointer manually to NULL to avoid further use*/

         //compare two null uuids
         cheat_assert(UUID_RC_OK==uuid_compare(uuid1, uuid2, &result));
         cheat_assert(result==0);
  	)


CHEAT_TEST(uuid_import_export,
       uuid_t *uuid1, *uuid2;
       uuid_rc_t msg;
       size_t data_len = 100;
       char *data = NULL;
       char *data2 = NULL;
       cheat_assert(UUID_RC_OK==uuid_create(&uuid1));
       cheat_assert(UUID_RC_OK==uuid_create(&uuid2));
       cheat_assert(UUID_RC_OK==uuid_load(uuid1,"nil"));


       cheat_assert(UUID_RC_OK==uuid_export(uuid1, UUID_FMT_TXT, &data, NULL));

       cheat_assert(UUID_RC_OK==uuid_export(uuid1, UUID_FMT_STR, &data2, NULL));

       msg = uuid_import(uuid2, UUID_FMT_STR, data2, data_len);

       printf("%s", uuid_error(msg));
       
       cheat_assert(UUID_RC_OK==uuid_destroy(uuid1));
       cheat_assert(UUID_RC_OK==uuid_destroy(uuid2));
       free(data);
       free(data2);

     )
