/* -*- mode:c -*- */
/** @file
 ** @brief check mempool
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro/i_mempool.h"
#include "maestro/i_statistics.h"
#include "maestro.h"
#include <pthread.h>

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#define NUM_REPEATS 10000
#define DEPTH 100
#define NUM_THREADS 10

CHEAT_DECLARE(
    struct s1 {
      char x;
    };
    struct s2 {
      double x;
      void*y;
    };
    struct s3 {
      char foo[127];
      double x;
    };

    void *
    run_builtin_s1(void* arg)
    {
      void *unused = arg;
      for(size_t i=0; i<NUM_REPEATS; i++) {
        struct s1 *x[DEPTH];
        for (size_t j=0; j<DEPTH; j++) {
          x[j] = malloc(sizeof(struct s1));
          /* ensure optimizer does not kill this loop as dead code */
          struct s1 ** volatile y = &x[j];
        }
        for(size_t j=0; j<DEPTH; j++) {
          free(x[j]);
        }
      }                                   
      return NULL;
    }


    void *
    run_pool_s1(void* arg)
    {
      mstro_mempool p = (mstro_mempool)arg;
      for(size_t i=0; i<NUM_REPEATS; i++) {
        struct s1 *x[DEPTH];
        for (size_t j=0; j<DEPTH; j++) {
          x[j] = mstro_mempool_alloc(p);
          /* ensure optimizer does not kill this loop as dead code */
          struct s1 ** volatile y = &x[j];
        }
        for(size_t j=0; j<DEPTH; j++) {
          mstro_mempool_free(p,x[j]);
        }
      }                                   
      return NULL;
    }

    
              )

    
CHEAT_TEST(mempools,
           mstro_mempool p1,p2,p3;
	   cheat_assert(MSTRO_OK == mstro_init("Tests","mempool",0));
           /* since we skip mstro_init() we need to init the stats
            * module separately */
           cheat_assert(MSTRO_OK==mstro_stats_init());
           double duration, ms;
           cheat_assert(NULL!= (p1=mstro_mempool_create(
               "Pool 1",
               sizeof(struct s1),
               27,
               2, false)));
           cheat_assert(NULL!= (p2=mstro_mempool_create(
               "Pool 2",
               sizeof(struct s2),
               1,
               2, true)));
           cheat_assert(NULL!= (p3=mstro_mempool_create(
               "Pool 3",
               sizeof(struct s3),
               11,
               0, true)));

           cheat_yield();

           
           pthread_t threads[NUM_THREADS];
           mstro_nanosec_t before_s1_builtin = mstro_clock();
           
           for(size_t i=0;i<NUM_THREADS; i++) {
             int s = pthread_create(threads+i,
                                       NULL,
                                       run_builtin_s1, NULL);
             cheat_assert(s==0);
           }
           
           for(size_t i=0; i<NUM_THREADS; i++) {
             int s= pthread_join(threads[i],NULL);
             cheat_assert(s==0);
           }
           mstro_nanosec_t after_s1_builtin = mstro_clock();

           duration = after_s1_builtin-before_s1_builtin;
           ms = duration/(1000.0*1000.0);

           fprintf(stdout, "builtin s1: %d threads, %d repeats: %.2f ms\n",
                   NUM_THREADS, NUM_REPEATS, ms);


           mstro_nanosec_t before_s1_pool = mstro_clock();
           
           for(size_t i=0;i<NUM_THREADS; i++) {
             int s=pthread_create(threads+i,
                                       NULL,
                                       run_pool_s1, p1);
             cheat_assert(s==0);
           }
           for(size_t i=0; i<NUM_THREADS; i++) {
             int s=pthread_join(threads[i], NULL);
             cheat_assert(s==0);
           }
           mstro_nanosec_t after_s1_pool = mstro_clock();
           
           duration = after_s1_pool-before_s1_pool;
           ms = duration/(1000.0*1000.0);

           fprintf(stdout, "pooled s1: %d threads, %d repeats: %.2f ms\n",
                   NUM_THREADS, NUM_REPEATS, ms);

           mstro_mempool_destroy(p1);
           mstro_mempool_destroy(p2);
           mstro_mempool_destroy(p3);
           cheat_assert(MSTRO_OK==mstro_stats_finalize());
	   cheat_assert(MSTRO_OK == mstro_finalize());

	)


