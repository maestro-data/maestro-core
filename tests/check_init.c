/* -*- mode:c -*- */
/** @file
 ** @brief check init/finalize
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/env.h"
#include "maestro/logging.h"

/* this tests that we can call toplevel API init/finalize  */
CHEAT_TEST(init_finalize_works,
           cheat_assert(MSTRO_OK == mstro_init("Tests","Init",0));
           cheat_assert(MSTRO_OK == mstro_finalize());
           )


CHEAT_TEST(init_finalize_fromenv_works,
           {
             setenv(MSTRO_ENV_WORKFLOW_NAME, "Tests", 1);
             setenv(MSTRO_ENV_COMPONENT_NAME, "InitEnv", 1);
             cheat_assert(mstro_status_description(mstro_status_MAX)==mstro_status_description(mstro_status_MAX+1));
             cheat_assert(MSTRO_OK == mstro_init(NULL, NULL, 42));
	           cheat_assert(NULL!=mstro_component_name());
             cheat_assert(MSTRO_OK == mstro_finalize());
           }
           )

CHEAT_TEST(init_finalize_fromenv_onlycomponnent,
          {
            unsetenv(MSTRO_ENV_WORKFLOW_NAME);
            setenv(MSTRO_ENV_COMPONENT_NAME, "InitEnv", 1);
            cheat_assert(mstro_status_description(mstro_status_MAX)==mstro_status_description(mstro_status_MAX+1));
            cheat_assert(MSTRO_INVARG == mstro_init(NULL, NULL, 42));
            cheat_assert(NULL!=mstro_component_name());
            cheat_assert(MSTRO_FAIL == mstro_finalize());
           }
           )

CHEAT_TEST(init_finalize_negativecomponentindex,
          {
            cheat_assert(mstro_status_description(mstro_status_MAX)==mstro_status_description(mstro_status_MAX+1));
            cheat_assert(MSTRO_INVARG == mstro_init("Tests","Init",-20));
            cheat_assert(MSTRO_FAIL == mstro_finalize());
          }
          )

CHEAT_TEST(init_twice_finalize,
          {
            cheat_assert(mstro_status_description(mstro_status_MAX)==mstro_status_description(mstro_status_MAX+1));
            cheat_assert(MSTRO_OK == mstro_init("Tests","Init",20));
            cheat_assert(MSTRO_NOT_TWICE == mstro_init("Tests","Init",20));
            cheat_assert(MSTRO_OK == mstro_finalize());
          }
          )
