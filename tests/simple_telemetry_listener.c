/* Connect to pool and subscribe to all JOIN and all CDO ID creating
 * events. Log them. */
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* FIXME: set signal handler to flush log and stop cleanly */


#include "maestro.h"
#include "maestro/i_statistics.h"
#include "maestro/i_ketopt.h"

#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

#include <syslog.h>
#include <stdarg.h>


const mstro_nanosec_t  DEFAULT_MAX_WAIT = ((mstro_nanosec_t)15)*NSEC_PER_SEC; /* 15s */

/** Configurable settings */

/** workflow name to attach to (default: from env)*/
char *g_conf_workflow_name = NULL;
/** component name to use (default: argv[0])*/
char *g_conf_component_name = NULL;
/** component name after whose BYE event we should terminate (default: none) */
char *g_conf_terminate_after = NULL;
/** the appid for this component, as observed during its WELCOME */
mstro_app_id g_conf_terminate_after_appid = 0;
/** timeout after which to terminate, if no events are observed anymore (default: DEFAULT_MAX_WAIT) */
mstro_nanosec_t g_conf_max_wait = DEFAULT_MAX_WAIT;
/** pool manager info (default: from env) */
char *g_conf_pminfo = NULL;
#define LOGDST_STDOUT 1
#define LOGDST_STDERR 2
#define LOGDST_MAESTRO 3
#define LOGDST_SYSLOG 4
/** logging destination */
int g_conf_logdst = LOGDST_STDOUT;
/** verbose operation */
bool g_verbose = true;


/** usage */

void
print_usage(const char *argv0)
{
  fprintf(stderr, "Usage: %s [OPTIONS]\n", argv0);
  fprintf(stderr, " Initialization OPTIONS:\n");
  fprintf(stderr, " (environment variables that are used as default in {curly brackets})\n");
  fprintf(stderr,
          " --workflow NAME, -w NAME         Maestro Workflow name to attach to {MSTRO_WORKFLOW_NAME}\n"
          " --component NAME, -c NAME        Maestro component name for this listener {MSTRO_COMPONENT_NAME}\n"
          " --terminate-after NAME, -q NAME  Terminate after observing LEAVE of component NAME\n"
          " --max-idle SECONDS, -i SECONDS   Terminate after no events for SECONDS {%g}\n"
          " --destination DEST, -d DEST      Logging destination: 'syslog', 'stdout', 'stderr', or 'mstro' {stdout}\n"
          " --pm-info PMINFO, -p PMINFO      Pool Manager OOB info {MSTRO_POOL_MANAGER_INFO}\n"
          " --help, -h                       This help\n"
          " --verbose, -v                    Verbose output\n"
          " --version, -V                    Version information\n"
          ,((double)DEFAULT_MAX_WAIT)/NSEC_PER_SEC);
}

/** argument parsing and setting configuration */
mstro_status
parse_arguments(int argc, char **argv)
{
  /* default */
  g_conf_component_name = argv[0];

  static ko_longopt_t longopts[] = {
    { "workflow",        ko_required_argument,       'w' },
    { "component",       ko_required_argument,       'c' },
    { "terminate-after", ko_required_argument,       'q' },
    { "max-idle",        ko_required_argument,       'i' },
    { "destination",     ko_required_argument,       'd' },
    { "pm-info",         ko_required_argument,       'p' },
    { "pm_info",         ko_required_argument,       'p' },
    { "help",            ko_no_argument,             'h' },
    { "verbose",         ko_no_argument,             'v' },
    { "version",         ko_no_argument,             'V' },
    { NULL, 0, 0 }
  };
  ketopt_t opt = KETOPT_INIT;
  int c;
  
  while ((c = ketopt(&opt, argc, argv, 1, "w:c:q:i:d:hv", longopts)) >= 0) {
    switch(c) {
      case 'w':
        if(!opt.arg) {
          fprintf(stderr, "--workflow option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_workflow_name = opt.arg;
        break;
      case 'c':
        if(!opt.arg) {
          fprintf(stderr, "--component option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_component_name = opt.arg;
        break;
      case 'q':
        if(!opt.arg) {
          fprintf(stderr, "--terminate-after option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_terminate_after = opt.arg;
        break;
      case 'i':
        if(!opt.arg) {
          fprintf(stderr, "--max-idle option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        g_conf_max_wait = atoi(opt.arg)* NSEC_PER_SEC;
        break;
      case 'd': {
        const char *dst = opt.arg;
        if(strncasecmp(dst,"sy",2)==0) {
          g_conf_logdst=LOGDST_SYSLOG;
        } else if(strncasecmp(dst,"stdo",4)==0) {
          g_conf_logdst=LOGDST_STDOUT;
        } else if(strncasecmp(dst,"stde",4)==0) {
          g_conf_logdst=LOGDST_STDERR;
        } else if(strncasecmp(dst,"m",1)==0) {
          g_conf_logdst=LOGDST_MAESTRO;
        } else {
          fprintf(stderr, "Illegal output destination (or none) specified: %s\n", dst);
          print_usage(argv[0]);
          return MSTRO_FAIL;
        }
        break;
      }
      case 'p': {
        if(!opt.arg) {
          fprintf(stderr, "--pm-info option is missing its argument\n");
          print_usage(argv[0]);
          exit(EXIT_FAILURE);
        }
        if(setenv(MSTRO_ENV_POOL_INFO, opt.arg, 1)!=0) {
          fprintf(stderr, "Failed to set PM-INFO: %d (%s)\n", errno, strerror(errno));
          exit(EXIT_FAILURE);
        }
        break;
      }
      case 'v':
        g_verbose = true;
        break;
      case 'h':
        print_usage(argv[0]);
        exit(EXIT_SUCCESS);
      case 'V':
        fprintf(stdout, "%s, part of %s\n", argv[0], mstro_version());
        exit(EXIT_SUCCESS);
        break;
      default:
        fprintf(stderr, "Unrecognized option: %c\n", optopt? optopt : ' ');
        print_usage(argv[0]);
        return MSTRO_FAIL;
    }
  }
  if(opt.ind<argc) {
    fprintf(stderr, "Unexpected non-option arguments: %s (...)\n", argv[opt.ind]);
    print_usage(argv[0]);
    return MSTRO_FAIL;
  }

  if(g_verbose) {
    fprintf(stderr, "Configuration: %s/%s/%s/%" PRInanosec "/%d\n",
            g_conf_workflow_name, g_conf_component_name,
            g_conf_terminate_after, g_conf_max_wait, g_conf_logdst);
  }
  return MSTRO_OK;
}

static void
store_telemetry_file(FILE* fp, mstro_pool_event e, const char *message, va_list args)
{
  e=e; /* avoid unused arg */
  vfprintf(fp, message, args);
}

static void
store_telemetry_syslog(mstro_pool_event e, const char *message, va_list args)
{
  e=e; /* avoid unused arg */
  vsyslog(LOG_ERR, message, args);
}

/** maestro/logging.h redefines LOG_ERROR from syslog.h, so we cannot include it at the top */
#undef LOG_ERR
#undef LOG_WARNING
#undef LOG_INFO
#undef LOG_DEBUG
#include "maestro/logging.h"

static void
store_telemetry_maestro(mstro_pool_event e, const char *message, va_list args)
{
  e=e; /* avoid unused arg */

  mstro_vlocation_aware_log(MSTRO_LOG_INFO, MSTRO_LOG_MODULE_TELEMETRY,
                            __func__, __FILE__, __LINE__, message, args);  
}

static void
store_telemetry_dispatch(mstro_pool_event e, const char *message, ...)
{
  va_list ap;
  va_start(ap, message);

  switch(g_conf_logdst) {
    case LOGDST_STDOUT:
      store_telemetry_file(stdout, e, message, ap);
      break;
    case LOGDST_STDERR:
      store_telemetry_file(stderr, e, message, ap);
      break;
    case LOGDST_MAESTRO:
      store_telemetry_maestro(e, message, ap);
      break;
    case LOGDST_SYSLOG:
      store_telemetry_syslog(e, message, ap);
      break;
    default:
      fprintf(stderr, "Illegal telemetry log destination %d\n", g_conf_logdst);
  }
  va_end(ap);
}

static void
store_telemetry(mstro_pool_event e)
{
  /* basic event type */
  const char *kind = mstro_pool_event_description(e->kind);

  /* Until that description is not detailed enough, generate extra info here for some events */

  /* format: nested CSV  "timestamp, event name, serial, { per-kind-info }" */
  switch(e->kind) {
    case MSTRO_POOL_EVENT_APP_JOIN:
      /* { assigned appid, "component name" } */
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { %" PRIappid ",\"%s\" }\n",
                               mstro_clock(),
                               kind, e->serial,
                               e->payload.join.appid, e->payload.join.component_name);
      break;
    case MSTRO_POOL_EVENT_APP_WELCOME:
      /* { assigned appid, "component name" } */
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { %" PRIu64 ",\"%s\"}\n",
                               mstro_clock(),
                               kind, e->serial, e->payload.welcome.appid, e->payload.welcome.component_name);
      break;
      
    case MSTRO_POOL_EVENT_APP_LEAVE:
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { %" PRIu64 " }\n",
                               mstro_clock(),
                               kind, e->serial, e->payload.leave.appid);
      break;
    case MSTRO_POOL_EVENT_APP_BYE:
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { %" PRIu64 " }\n",
                               mstro_clock(),
                               kind, e->serial, e->payload.bye.appid);
      break;
    case MSTRO_POOL_EVENT_DECLARE:
      /* { appid, "cdo name" } */
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { %" PRIu64 ",\"%s\"}\n",
                               mstro_clock(),
                               kind, e->serial, e->payload.declare.appid, e->payload.declare.cdo_name);
      break;
    case MSTRO_POOL_EVENT_SEAL:
      /* { appid, "cdo name" } */
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { %" PRIu64 ",\"%s\"}\n",
                               mstro_clock(),
                               kind, e->serial, e->payload.seal.appid, e->payload.seal.cdo_name);
      break;
    default:
      store_telemetry_dispatch(e, "%" PRIu64 ",%s,%" PRIu64 ", { }\n",
                               mstro_clock(),
                               kind, e->serial);
  }
}

/** main loop: handle events and log them */
mstro_status
event_loop(void)
{
  /* wildcard selector: any CDO */
  mstro_cdo_selector selector=NULL;
  mstro_status s=mstro_cdo_selector_create(
      NULL, NULL,
      "(has .maestro.core.cdo.name)",
      &selector);
  

  /* Subscribe to DECLARE and SEAL.
   *
   * That allows us to log app-id/name/local-id and later
   * app-id/local-id/cdo-id
   */
  mstro_subscription cdo_subscription=NULL;
  s = mstro_subscribe(selector,
                      MSTRO_POOL_EVENT_DECLARE |MSTRO_POOL_EVENT_SEAL|MSTRO_POOL_EVENT_DISPOSE,
                      MSTRO_SUBSCRIPTION_OPTS_DEFAULT,
                      &cdo_subscription);
  s=mstro_cdo_selector_dispose(selector);
  

  /* Subscribe to JOIN and LEAVE ('after' events, after handling on the PM).
   *
   * That allows us to log component names and implement a termination
   * criterion like "if component X has left, terminate telemetry
   * listener" */
  mstro_subscription join_leave_subscription=NULL;
  s=mstro_subscribe(
      NULL, MSTRO_POOL_EVENT_APP_JOIN|MSTRO_POOL_EVENT_APP_LEAVE,
      MSTRO_SUBSCRIPTION_OPTS_DEFAULT,
      &join_leave_subscription);
  
  bool done = false;

  mstro_nanosec_t starttime = mstro_clock();

  /* instead of a busy loop we could use event wait sets -- but we
   * don't have them yet */
  while(!done) {
    mstro_pool_event e=NULL;
    /* poll join/leave */
    s=mstro_subscription_poll(join_leave_subscription, &e);
    
    if(e!=NULL) {
      /* reset start time, since we saw some event */
      starttime = mstro_clock();

      if(g_verbose)
        fprintf(stdout, "JOIN/LEAVE event(s)\n");
      
      mstro_pool_event tmp=e;
      /* handle all */
      while(tmp) {
        store_telemetry(tmp);

        switch(tmp->kind) {
          case MSTRO_POOL_EVENT_APP_JOIN:
            if(g_verbose)
              fprintf(stdout, "Noticed JOIN event of app %s\n",
                      tmp->payload.join.component_name);
            if(g_conf_terminate_after) {
              if(strcmp(g_conf_terminate_after, tmp->payload.join.component_name)==0) {
                if(g_verbose) {
                  fprintf(stdout, "App recognized as the one that will trigger termination of this listener\n");
                }
                g_conf_terminate_after_appid = tmp->payload.join.appid;
              }
            }
            break;
            
          case MSTRO_POOL_EVENT_APP_LEAVE:
            if(g_verbose) {
              fprintf(stdout, "Noticed LEAVE event of app %" PRIappid "\n",
                      tmp->payload.leave.appid);
            }
            if(g_conf_terminate_after) {
              if(g_conf_terminate_after_appid==tmp->payload.leave.appid) {
                if(g_verbose) {
                  fprintf(stdout, "LEAVE of app %" PRIappid " triggers termination of telemetry listener\n",
                          tmp->payload.leave.appid);
                }
                done=true;
              }
            }
            break;
            
          default:
            fprintf(stderr, "Unexpected event %d\n", tmp->kind);
        }

        if(g_verbose)
          fflush(stdout);
        
        tmp=tmp->next;
      }

      /* acknowledge all */
      /* no need to ack these events s=mstro_subscription_ack(join_leave_subscription, e); */
      /* dispose all */
      s=mstro_pool_event_dispose(e);
    } else {
      s=mstro_subscription_poll(cdo_subscription, &e);
      if(e!=NULL) {
        /* reset start time, since we saw some event */
        starttime = mstro_clock();

        mstro_pool_event tmp=e;
        /* handle all */
        while(tmp) {
          store_telemetry(tmp);

          switch(tmp->kind) {
            case MSTRO_POOL_EVENT_DECLARE:
            case MSTRO_POOL_EVENT_SEAL:
              if(g_verbose) {
                fprintf(stdout, "CDO event %s\n",
                        mstro_pool_event_description(tmp->kind));
              }
              break;
            default:
              fprintf(stderr, "Unexpected CDO event %d\n", tmp->kind);
          }

          tmp=tmp->next;
        }
        /* acknowledge all */
        /* no need to ack these events s=mstro_subscription_ack(cdo_subscription, e); */
        /* dispose all */
        s=mstro_pool_event_dispose(e);
      } else {
        sleep(1); /* core will queue up events for us, no need to do
                   * intensive busy-loop */
      }
    }
    
    if(mstro_clock()>starttime+DEFAULT_MAX_WAIT) {
      fprintf(stderr, "Waited %" PRIu64 "s and still not done\n",
              DEFAULT_MAX_WAIT/(1000*1000*1000));
      done=true;
    }
    if(done)
      break; /* from WHILE */
             
  }
  s= mstro_subscription_dispose(cdo_subscription);
  s= mstro_subscription_dispose(join_leave_subscription);

BAILOUT:
  return s;
}

/** main entrypoint */
char *g_default_loglevel=NULL;

int
main(int argc, char ** argv)
{

  mstro_status s = parse_arguments(argc, argv);
  if(s!=MSTRO_OK) {
    fprintf(stderr, "Failed to parse arguments\n");
    exit(EXIT_FAILURE);
  }
  if(g_conf_logdst==LOGDST_SYSLOG) {
    openlog(g_conf_component_name, LOG_CONS|LOG_PID, LOG_USER);
  }

  /** ensure pool info is set (by user or args) */
  if(getenv(MSTRO_ENV_POOL_INFO)==NULL ) {
    fprintf(stderr, "No pool manager info provided, cannot attach to anything\n");
    exit(EXIT_FAILURE);
  }

  /** try to reduce logging, unless user set something */
  if(getenv(MSTRO_ENV_LOG_LEVEL)==NULL) {
      setenv(MSTRO_ENV_LOG_LEVEL, "err", 1);
  } 
  else {
      ;/* well, then try without reduced logging */ 
  }

  /** start */
  s=mstro_init(g_conf_workflow_name, g_conf_component_name, 0);
  if(s!=MSTRO_OK) {
    fprintf(stderr, "Failed to initialize Maestro: %d (%s)\n",
            s, mstro_status_description(s));
    exit(EXIT_FAILURE);
  }

  s = event_loop();

  mstro_status s1=mstro_finalize();

  if(s1!=MSTRO_OK || s!=MSTRO_OK)
    return EXIT_FAILURE;
  else
    return EXIT_SUCCESS;
}

