#include <stdio.h>
#include <assert.h>
#include "maestro.h"

#include "ecmwf_config.h"
#include <unistd.h>

int main(int argc, char *argv[])
{
    mstro_status s;
    s=mstro_init(getenv("MSTRO_WORKFLOW_NAME"), getenv("MSTRO_COMPONENT_NAME"), 0);
    assert(s==MSTRO_OK);

    mstro_cdo cdos[number_of_cdos];

    for (int i = 0; i < number_of_cdos; i++) {
        char cdo_name[20];
        snprintf(cdo_name, 20, "my-cdo-%0d", i);
        printf("Declaring and offering cdo: %s\n", cdo_name);
        s=mstro_cdo_declare(cdo_name, MSTRO_ATTR_DEFAULT, &cdos[i]);
	assert(s==MSTRO_OK);
        s=mstro_cdo_seal(cdos[i]);
	assert(s==MSTRO_OK);
        s=mstro_cdo_offer(cdos[i]);
	assert(s==MSTRO_OK);
    }

    for (int i = 0; i < number_of_cdos; i++) {
        printf("Withdrawing and disposing cdo: %s\n", mstro_cdo_name(cdos[i]));
        s=mstro_cdo_withdraw(cdos[i]);
	assert(s==MSTRO_OK);
        s=mstro_cdo_dispose(cdos[i]);
	assert(s==MSTRO_OK);
    }

    s=mstro_finalize();
    assert(s==MSTRO_OK);
    return 0;    
}
