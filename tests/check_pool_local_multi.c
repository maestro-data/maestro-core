/* -*- mode:c -*- */
/** @file
 ** @brief check CDO declare/.../dispose, many times, from multiple threads
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_statistics.h"
#include "maestro/logging.h"
#include <string.h>
#include <pthread.h>


#define CDO_NAME_MAX 32

CHEAT_DECLARE(
    /* everyone will OR in their value; need to do atomic access */
    pthread_mutex_t global_result_mtx = PTHREAD_MUTEX_INITIALIZER;
    mstro_status global_result = MSTRO_OK;
    
    struct thread_args {
      size_t num_repeats;
      size_t num_different_cdos;
      size_t num_max_declares_per_cdo;
    };
    
    
    void
    create_name(char *dst, size_t idx)
    {
      snprintf(dst, CDO_NAME_MAX, "Test CDO %zu\n", idx);
    }
    
    /* intended to be run in a thread */
    void*
    run_declare_thread(void *closure) {
      struct thread_args *args = (struct thread_args *)closure;
      mstro_status s = MSTRO_OK;
      
      for(size_t i=0; i< args->num_repeats; i++) {
        long rnum = random();
        size_t cdoidx = rnum % args->num_different_cdos;
        char name[CDO_NAME_MAX];
        create_name(name, cdoidx);
        mstro_cdo cdos[args->num_max_declares_per_cdo];
        
        for(size_t j=0; j< args->num_max_declares_per_cdo; j++) {
          mstro_status s1,s2;

          s1= mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, & (cdos[j]));
          s2= mstro_cdo_offer(cdos[j]);
          
          s = s | s1 | s2 ;
        }
        for(size_t j=0; j< args->num_max_declares_per_cdo; j++) {
          mstro_status s3,s4;

          s3= mstro_cdo_withdraw(cdos[j]);
          s4= mstro_cdo_dispose(cdos[j]);
          s = s | s3 | s4 ;
        }


      }

      pthread_mutex_lock(&global_result_mtx);
      global_result |= s;
      pthread_mutex_unlock(&global_result_mtx);

      return NULL;
    }
              )

#define MAX_THREADS 9 /* power-of-two + 1 and >1 */

CHEAT_TEST(cdo_local_pool_works,
           /* save print time: */
	   setenv("MSTRO_LOG_LEVEL", "0", 1);
           srandom(42);
           cheat_assert(MSTRO_OK == mstro_init("Tests","POOL",0));
   
			 fprintf(stdout, "# num_diff=100k; time /= decl_per_cdo\n");

           for(size_t numdecl=1; numdecl<129; numdecl*=2) {
//             for(size_t num_diff=1; num_diff<100001; num_diff*=10) {
     		size_t num_diff = 100000; 

             struct thread_args args;
             args.num_repeats = 100000 / num_diff;
             args.num_different_cdos = num_diff;
             args.num_max_declares_per_cdo = numdecl;

			 fprintf(stdout, "%8zu ", args.num_max_declares_per_cdo);
			 

	           for(size_t numthr=1; numthr<MAX_THREADS; numthr+=4) {
		           if (numthr==5) numthr--; // yes that's ugly
                       pthread_t threads[MAX_THREADS];


                 mstro_nanosec_t before = mstro_clock();
               
                 for(size_t i=0; i<numthr; i++) {
                   pthread_create(& (threads[i]),
                                  NULL,
                                  run_declare_thread,
                                  &args);
                 }
                 for(size_t i=0; i<numthr; i++) {
                   void *res;
                   pthread_join(threads[i], &res);
                 }
                 mstro_nanosec_t after = mstro_clock();
                 double duration = after-before;
                 double ms = duration/(1000.0*1000.0);
/*                 fprintf(stdout, "%8zu reps, %8zu different, %8zu per CDO, %3zu threads: %.2f ms\n",
                         args.num_repeats, args.num_different_cdos, args.num_max_declares_per_cdo,
                         numthr,
                         ms);
  */              fprintf(stdout, "%.5f ", ms/(double)args.num_max_declares_per_cdo);

               }
//             }
			 fprintf(stdout, "\n");

           }
           cheat_assert(global_result==MSTRO_OK);

           cheat_assert(MSTRO_OK == mstro_finalize());
           )

