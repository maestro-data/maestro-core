/* check CDO operations with another local thread being subscribed to events */
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/logging.h"
#include "maestro/pool.h"
#include "maestro/i_utlist.h"
#include <string.h>
#include <stdatomic.h>
#include <unistd.h>
#include <pthread.h>

CHEAT_DECLARE(
    mstro_subscription decl_sub, decl_ack_sub, offer_sub, withdraw_sub, seal_sub, dispose_sub;
    mstro_cdo_selector match_all_selector;

    pthread_mutex_t start_mtx = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t  start_cvar = PTHREAD_COND_INITIALIZER;
    pthread_mutex_t done_mtx  = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t  done_cvar = PTHREAD_COND_INITIALIZER;

    /* perform the subscriptions */
    void
    subscribe(void)
    {

      cheat_assert(MSTRO_OK==mstro_cdo_selector_create(
          NULL, NULL, NULL, &match_all_selector));
      cheat_assert(MSTRO_OK==mstro_subscribe(match_all_selector,
                                             MSTRO_POOL_EVENT_DECLARE_ACK,
                                             false,
                                             &decl_ack_sub));

      cheat_assert(MSTRO_OK==mstro_subscribe(match_all_selector,
                                                MSTRO_POOL_EVENT_DECLARE,
                                                          false,
                                                        &decl_sub));

      cheat_assert(MSTRO_OK==mstro_subscribe(match_all_selector,MSTRO_POOL_EVENT_SEAL,false, &seal_sub));


      cheat_assert(MSTRO_OK==mstro_subscribe(match_all_selector,
                                             MSTRO_POOL_EVENT_OFFER,
                                             false,
                                             &offer_sub));
      cheat_assert(MSTRO_OK==mstro_subscribe(match_all_selector,
                                             MSTRO_POOL_EVENT_WITHDRAW,
                                             true,
                                             &withdraw_sub));

      cheat_assert(MSTRO_OK==mstro_subscribe(match_all_selector,MSTRO_POOL_EVENT_DISPOSE,false, &dispose_sub));
    }

    /* perform the un-subscriptions */
    void
    unsubscribe(void)
    {
      cheat_assert(MSTRO_OK==mstro_cdo_selector_dispose(match_all_selector));
      cheat_assert(MSTRO_OK==mstro_subscription_dispose(decl_sub));
      cheat_assert(MSTRO_OK==mstro_subscription_dispose(offer_sub));
      cheat_assert(MSTRO_OK==mstro_subscription_dispose(withdraw_sub));
      cheat_assert(MSTRO_OK==mstro_subscription_dispose(decl_ack_sub));
      cheat_assert(MSTRO_OK==mstro_subscription_dispose(seal_sub));
      cheat_assert(MSTRO_OK==mstro_subscription_dispose(dispose_sub));
    }

    pthread_mutex_t counter_mtx = PTHREAD_MUTEX_INITIALIZER;
    
    size_t num_offer=0, num_withdraw=0, num_declare=0, num_decl_ack=0, num_seal=0, num_dispose=0;


    void mark_subscriber_dead(void*unused)
    {
      void* unused2=unused;

      /* counter_mtx still locked from subscriber thread which was interrupted */
      
      cheat_assert(num_declare==3);
      cheat_assert(num_decl_ack==3);
      cheat_assert(num_seal==3);
      cheat_assert(num_offer==3);
      cheat_assert(num_withdraw==3);
      cheat_assert(num_dispose==3);

      cheat_assert(0==pthread_mutex_unlock(&counter_mtx));

      fprintf(stdout, "Subscriber terminated\n");
      cheat_assert(0==pthread_mutex_lock(&done_mtx));
      cheat_assert(0==pthread_cond_signal(&done_cvar));
      cheat_assert(0==pthread_mutex_unlock(&done_mtx));

    }

    void*
    subscriber_thread(void*unused)
    {
      unused=unused;
      struct timespec t;
      t.tv_sec = 0;
      t.tv_nsec = 100;
      mstro_pool_event e2;

      pthread_cleanup_push(mark_subscriber_dead, NULL);


      cheat_assert(MSTRO_OK!=mstro_subscription_timedwait(decl_sub, &t, &e2));

      fprintf(stdout, "subscriber started\n");

      cheat_assert(0==pthread_mutex_lock(&start_mtx));
      cheat_assert(0==pthread_cond_signal(&start_cvar));
      cheat_assert(0==pthread_mutex_unlock(&start_mtx));

      cheat_assert(0==pthread_mutex_lock(&counter_mtx));

      while(1) {
        mstro_pool_event e;
        mstro_status s;


        cheat_assert(MSTRO_OK==mstro_subscription_poll(decl_sub,&e));
        if(e!=NULL) {
          mstro_pool_event tmp;
          size_t count=0;
          LL_COUNT(e,tmp, count);
          fprintf(stdout, "%zu decl event(s)\n", count);
          num_declare+=count;
        }

        cheat_assert(MSTRO_OK==mstro_subscription_poll(decl_ack_sub,&e));
        if(e!=NULL) {
          mstro_pool_event tmp;
          size_t count=0;
          LL_COUNT(e,tmp, count);
          fprintf(stdout, "%zu decl ack event(s)\n", count);
          num_decl_ack+=count;
        }

        cheat_assert(MSTRO_OK==mstro_subscription_poll(seal_sub,&e));
        if(e!=NULL) {
          mstro_pool_event tmp;
          size_t count=0;
          LL_COUNT(e,tmp, count);
          fprintf(stdout, "%zu seal event(s)\n", count);
          num_seal+=count;
        }

        cheat_assert(MSTRO_OK==mstro_subscription_poll(offer_sub,&e));
        if(e!=NULL) {
          mstro_pool_event tmp;
          size_t count=0;
          LL_COUNT(e,tmp, count);
          fprintf(stdout, "%zu offer event(s)\n", count);
          num_offer+=count;
        }
        // test invalid input
        // FIXME mstro_subscription_wait wait forever with correct input
        cheat_assert(MSTRO_OK!=mstro_subscription_wait(withdraw_sub, NULL));

        cheat_assert(MSTRO_OK==mstro_subscription_poll(withdraw_sub,&e));
        if(e!=NULL) {
          mstro_pool_event tmp;
          size_t count=0;
          LL_COUNT(e,tmp, count);

          fprintf(stdout, "%zu withdraw event(s), acknowledging\n", count);
          cheat_assert(MSTRO_OK==mstro_subscription_ack(withdraw_sub, e));
          num_withdraw+=count;
        }

        cheat_assert(MSTRO_OK==mstro_subscription_poll(dispose_sub,&e));
        if(e!=NULL) {
          mstro_pool_event tmp;
          size_t count=0;
          LL_COUNT(e,tmp, count);
          fprintf(stdout, "%zu dispose event(s)\n", count);
          num_dispose+=count;
        }

        if(e==NULL)
          cheat_assert(MSTRO_INVARG==mstro_pool_event_dispose(e));
        else
          cheat_assert(MSTRO_OK==mstro_pool_event_dispose(e));

        
        pthread_testcancel();
      }


      pthread_cleanup_pop(1);


    }

    void
    operate(void)
    {
      mstro_cdo c1,c2,c3;
      cheat_assert(MSTRO_OK==mstro_cdo_declare("C1", MSTRO_ATTR_DEFAULT, &c1));
      cheat_assert(MSTRO_OK==mstro_cdo_declare("C2", MSTRO_ATTR_DEFAULT, &c2));
      cheat_assert(MSTRO_OK==mstro_cdo_declare("C3", MSTRO_ATTR_DEFAULT, &c3));

      cheat_assert(MSTRO_OK==mstro_cdo_offer(c1));
      cheat_assert(MSTRO_OK==mstro_cdo_offer(c3));
      cheat_assert(MSTRO_OK==mstro_cdo_withdraw(c1));
      cheat_assert(MSTRO_OK==mstro_cdo_offer(c2));
      cheat_assert(MSTRO_OK==mstro_cdo_withdraw(c2));
      cheat_assert(MSTRO_OK==mstro_cdo_withdraw(c3));
      cheat_assert(MSTRO_OK==mstro_cdo_declare("C3", MSTRO_ATTR_DEFAULT, &c1));

      cheat_assert(MSTRO_OK==mstro_cdo_dispose(c1));
      cheat_assert(MSTRO_OK==mstro_cdo_dispose(c2));
      cheat_assert(MSTRO_OK==mstro_cdo_dispose(c3));
    }
              )

CHEAT_TEST(cdo_local_subscriptions_work,
           cheat_assert(MSTRO_OK == mstro_init("Tests","LOCALSUBSCRIPTIONS",0));

           /* set up semaphore to wait for subscriber process startup */
           cheat_assert(0==pthread_mutex_lock(&start_mtx));
           
           subscribe();
           pthread_t sub_thread;
           cheat_assert(0==pthread_create(&sub_thread,
                                          NULL, subscriber_thread, NULL));
           cheat_assert(0==pthread_detach(sub_thread));

           cheat_assert(0==pthread_cond_wait(&start_cvar, &start_mtx));
           cheat_assert(0==pthread_mutex_unlock(&start_mtx));

           operate();

           cheat_assert(0==pthread_mutex_lock(&done_mtx));
           
           cheat_assert(0==pthread_cancel(sub_thread));
           
           cheat_assert(0==pthread_cond_wait(&done_cvar, &done_mtx));
           cheat_assert(0==pthread_mutex_unlock(&done_mtx));

           unsubscribe();
           cheat_assert(MSTRO_OK == mstro_finalize());
           )
