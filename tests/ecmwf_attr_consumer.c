#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "maestro.h"
#include <stdint.h>

#include "ecmwf_config.h"

int main(int argc, char* argv[])
{
    mstro_cdo_selector selector = NULL;
    mstro_subscription subscription = NULL;
    mstro_status s = MSTRO_OK;

    s = mstro_init(getenv("MSTRO_WORKFLOW_NAME"), getenv("MSTRO_COMPONENT_NAME"), 0);
    assert(s==MSTRO_OK);

    s = mstro_cdo_selector_create(NULL, NULL, "(and (.maestro.ecmwf.step = 2) (.maestro.ecmwf.number = 1))", &selector);
    s |= mstro_subscribe(selector, MSTRO_POOL_EVENT_OFFER, false, &subscription);
    s |= mstro_cdo_selector_dispose(selector);
    assert(s==MSTRO_OK);

    mstro_pool_event e = NULL;
    s = mstro_subscription_wait(subscription, &e);
    assert(s==MSTRO_OK);

    if (e != NULL) {
        fprintf(stdout, "Offer event: %p, kind: %d, cdo_name: %s\n", (void*)e, e->kind, e->payload.offer.cdo_name);
        mstro_cdo cdo = NULL;
        s = mstro_cdo_declare(e->payload.offer.cdo_name, MSTRO_ATTR_DEFAULT, &cdo);
        s |= mstro_cdo_require(cdo);
        s |= mstro_cdo_demand(cdo);
        assert(s==MSTRO_OK);
        mstro_cdo_attributes_print(cdo);  

        enum mstro_cdo_attr_value_type ttype;
        const int64_t* tval = NULL;
        s = mstro_cdo_attribute_get(cdo, ".maestro.ecmwf.step", &ttype, (const void**)&tval);
        assert(s==MSTRO_OK);
	assert(ttype == MSTRO_CDO_ATTR_VALUE_int64);
        fprintf(stdout, "Step = %" PRIi64 "\n", *tval);

        s = mstro_cdo_dispose(cdo);
	assert(s==MSTRO_OK);
    }

    s = mstro_pool_event_dispose(e);
    s |= mstro_subscription_dispose(subscription);
    s |= mstro_finalize();
    assert(s==MSTRO_OK);

    return 0;
}
