#include <stdio.h>
#include <assert.h>
#include "maestro.h"

int main(int argc, char *argv[])
{
	mstro_status s = MSTRO_OK;

	s = mstro_init(getenv("MSTRO_WORKFLOW_NAME"), getenv("MSTRO_COMPONENT_NAME"), 0);
        assert(s==MSTRO_OK);

	mstro_cdo cdo;

	s = mstro_cdo_declare("my-cdo-0", MSTRO_ATTR_DEFAULT, &cdo);
	s |= mstro_cdo_seal(cdo);
	s |= mstro_cdo_offer(cdo);
        assert(s==MSTRO_OK);

	printf("Withdrawing and disposing my-cdo-0");
	s = mstro_cdo_withdraw(cdo);
	s |= mstro_cdo_dispose(cdo);
        assert(s==MSTRO_OK);

	s = mstro_finalize();
	assert(s==MSTRO_OK);

	return 0;    
}
