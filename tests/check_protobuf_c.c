/* -*- mode:c -*- */
/** @file
 ** @brief check protobuf messages (de)serialization
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"

/* slightly ugly, but it's not exposed normally */

#include "../protocols/mstro_pool.pb-c.h"

/* this tests that we can serialize and deserialize a protobuf-based message */

CHEAT_TEST(protobuf_code_found,
           cheat_assert(NULL!=mstro_version());
           char * cdo_name = "Kilroy was here!";

           /* create 'declare' message */
           Mstro__Pool__Declare declmsg = MSTRO__POOL__DECLARE__INIT;
           declmsg.serial = 4711;
           declmsg.cdo_name = cdo_name;

           /* wrap in normal short message */
           Mstro__Pool__MstroMsg msg = MSTRO__POOL__MSTRO_MSG__INIT;
           msg.msg_case = MSTRO__POOL__MSTRO_MSG__MSG_DECLARE;
           msg.declare = &declmsg;
           msg.data = NULL;

           /* serialize */
           size_t s = mstro__pool__mstro_msg__get_packed_size(&msg);
           uint8_t *buf = malloc(sizeof(uint8_t)*s);
           cheat_assert(buf!=NULL);
           cheat_assert(s==mstro__pool__mstro_msg__pack(&msg, buf));

           /* de-serialize */
           Mstro__Pool__MstroMsg *unpacked =
           mstro__pool__mstro_msg__unpack(NULL, s, buf);
           cheat_assert(unpacked!=NULL);
           cheat_assert(unpacked->msg_case == MSTRO__POOL__MSTRO_MSG__MSG_DECLARE);

           cheat_assert(unpacked->declare->serial == 4711);
           cheat_assert(strcmp(unpacked->declare->cdo_name, cdo_name)==0);

           mstro__pool__mstro_msg__free_unpacked(unpacked, NULL);
           
           free(buf);
           
           )


