/* -*- mode:c -*- */
/** @file
 ** @brief check that a group can be created and demanded, where another application offers the actual CDOs in the group.
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>

/* this code is compilable in two ways, as a group client, but also as an injector.
   We do this in one C file to keep things together.
   We could do the injection in a single application if multiple CDO handles would be supported for the same CDO name, which is an open bug (#91)
*/

#define NUM_NAMES 3
#define NUM_HANDLES 2

CHEAT_DECLARE(
    char g_name[] = "Test group";
    char *c_names[NUM_NAMES] = { "C-name 1", "C-name 2", "C-name 3" };
    char *c_handle_names[NUM_HANDLES] = { "C-handle 1", "C-handle 2" };
    mstro_cdo cdo_handles[NUM_HANDLES];
    mstro_cdo cdo_handles_for_names[NUM_NAMES];

    char *producer_done_cdo_name = "Producer-done-CDO"; /*only needed because of #93 */
    char *consumer_done_cdo_name = "Consumer-done-CDO";

              )
    
#ifdef INJECT_GROUP_MEMBERS
    /* injection component */
    CHEAT_TEST(simple_group_inject,
               size_t i;
               cheat_assert(MSTRO_OK == mstro_init(NULL,NULL,0));
               cheat_yield();

               /* prepare to be able to wait for consumer to tell us they're done */
               mstro_cdo done_cdo;
               cheat_assert(MSTRO_OK==mstro_cdo_declare(consumer_done_cdo_name, MSTRO_ATTR_DEFAULT, &done_cdo));
               cheat_assert(MSTRO_OK==mstro_cdo_require(done_cdo));

               /* inject CDOs for all group members */
               for(size_t i=0; i<NUM_NAMES; i++) {
                 cheat_assert(MSTRO_OK
                              == mstro_cdo_declare(c_names[i], MSTRO_ATTR_DEFAULT, &(cdo_handles_for_names[i])));
                 cheat_assert(MSTRO_OK
                              == mstro_cdo_offer(cdo_handles_for_names[i]));
               }

               /* give go-ahead to consumers */
               mstro_cdo ready_cdo;
               cheat_assert(MSTRO_OK==mstro_cdo_declare(producer_done_cdo_name, MSTRO_ATTR_DEFAULT, &ready_cdo));
               cheat_assert(MSTRO_OK==mstro_cdo_offer(ready_cdo));

               /* wait for consumer to be done */
               cheat_assert(MSTRO_OK==mstro_cdo_demand(done_cdo));

               /* cleanup */
               cheat_assert(MSTRO_OK==mstro_cdo_dispose(done_cdo));
               for(size_t i=0; i<NUM_NAMES; i++) {
                 cheat_assert(MSTRO_OK
                              == mstro_cdo_withdraw(cdo_handles_for_names[i]));
                 cheat_assert(MSTRO_OK
                              == mstro_cdo_dispose(cdo_handles_for_names[i]));
               }
               cheat_assert(MSTRO_OK==mstro_cdo_withdraw(ready_cdo));
               cheat_assert(MSTRO_OK==mstro_cdo_dispose(ready_cdo));

               /* done */
               cheat_assert(MSTRO_OK == mstro_finalize());
               )
#else
CHEAT_TEST(simple_group_client,
           size_t i;
           cheat_assert(MSTRO_OK == mstro_init(NULL,NULL,0));
           cheat_yield();
           
	   for(i=0; i<NUM_HANDLES; i++) {
	     cheat_assert(MSTRO_OK
                          == mstro_cdo_declare(c_handle_names[i], MSTRO_ATTR_DEFAULT, &(cdo_handles[i])));
           }

           mstro_group g_prod=NULL;
           mstro_group g_cons=NULL;

           cheat_assert(MSTRO_OK==mstro_group_declare(g_name,&g_prod));
           cheat_yield();
           for(i=0; i<NUM_NAMES; i++) {
             cheat_assert(MSTRO_OK==mstro_group_add_by_name(g_prod, c_names[i]));
           }
           cheat_yield();
           for(i=0; i<NUM_HANDLES; i++) {
             cheat_assert(MSTRO_OK==mstro_group_add(g_prod, cdo_handles[i]));
           }
           cheat_yield();
                          
           cheat_assert(MSTRO_OK == mstro_group_offer(g_prod));
           cheat_yield();

           cheat_assert(MSTRO_OK==mstro_group_declare(g_name,&g_cons));
           cheat_assert(MSTRO_OK==mstro_group_require(g_cons));
           cheat_assert(MSTRO_OK == mstro_group_demand(g_cons));
           size_t count=0;
           cheat_assert(MSTRO_OK == mstro_group_size_get(g_cons, &count));
           cheat_assert(count == NUM_NAMES + NUM_HANDLES);
           cheat_yield();

           /* because of #93 we need to wait a moment here so that we
            * can be sure that the CDO IDs can actually be resolved
            * for name-only members */
           mstro_cdo ready_cdo;
           cheat_assert(MSTRO_OK==mstro_cdo_declare(producer_done_cdo_name, MSTRO_ATTR_DEFAULT, &ready_cdo));
           cheat_assert(MSTRO_OK==mstro_cdo_require(ready_cdo));
           cheat_assert(MSTRO_OK==mstro_cdo_demand(ready_cdo));


           mstro_cdo c;
           for(size_t idx=0; idx<count; idx++) {
             cheat_assert(MSTRO_OK==mstro_group_next(g_cons, &c));
             cheat_assert(NULL!=c);
             cheat_assert(MSTRO_OK==mstro_cdo_dispose(c));
           }
           /* now group is traversed, we should get a NULL */
           cheat_assert(MSTRO_OK==mstro_group_next(g_cons, &c));
           cheat_assert(NULL==c);
           /* now things should restart */
           cheat_assert(MSTRO_OK==mstro_group_next(g_cons, &c));
           cheat_assert(NULL!=c);
           cheat_assert(MSTRO_OK==mstro_cdo_dispose(c));

           cheat_yield();

           cheat_assert(MSTRO_OK == mstro_group_dispose(g_cons));
           cheat_yield();
           
           cheat_assert(MSTRO_OK == mstro_group_withdraw(g_prod));
           cheat_assert(MSTRO_OK == mstro_group_dispose(g_prod));
           
           /* user-allocated cdo handles need to be disposed explicitly */
           for(i=0; i<NUM_HANDLES; i++) {
             cheat_assert(MSTRO_OK==mstro_cdo_dispose(cdo_handles[i]));
           }
           
           /* send DONE CDO */
           mstro_cdo done_cdo;
           cheat_assert(MSTRO_OK==mstro_cdo_declare(consumer_done_cdo_name, MSTRO_ATTR_DEFAULT, &done_cdo));
           cheat_assert(MSTRO_OK==mstro_cdo_offer(done_cdo));
           cheat_assert(MSTRO_OK==mstro_cdo_withdraw(done_cdo));
           cheat_assert(MSTRO_OK==mstro_cdo_dispose(done_cdo));

           cheat_assert(MSTRO_OK == mstro_finalize());
           
           )
#endif



