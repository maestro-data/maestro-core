/* -*- mode:c -*- */
/** @file
 ** @brief  check schema type parsing
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/logging.h"
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>

#include "schema_type_parse.h"


#define XSTRINGIFY(s) #s
#define STRINGIFY(s) XSTRINGIFY(s)

CHEAT_DECLARE(
    const char *strings[] = {
      /* built-in types, no restrictions */
      "bool()", "uint()", "int()", "float()", "double()",
      "str()", "blob()",
      "timestamp()",
      /* regex is also built-in but can't be used without args;
       * see below */

      /* typical restricted types */
      "uint(max=10, min=7)",
      "int(min=-17, max=42)",
      "str(exclude='XYZ')", /* string that can not include the
                             * specified characters */
      "str(min=42)",
      "str(max=8)",
      "str(max=8, min=8)",
      "\tblob ( max = 4711) ",
      "mmblayout (max = 4711)",
      "regex(';.*$',ignore_case=False, name='lisp comment')",
      "regex(';.*$',ignore_case=True, name='lisp comment')",
      /* multiple patterns */
      "regex('^$','^[ \\t\\n\\r\\v]*#.*$',ignore_case=true, name='empty line or shell block comment')",
      /* /\* FIXME: add nested types *\/ */
      NULL,
          };
              )


CHEAT_TEST(core_type_parse,
           struct mstro_stp_val *result;
           const char **s;
	   cheat_assert(MSTRO_OK == mstro_init("Tests","Type parse",0));
           for(s=strings; (*s)!=NULL; s++) {
             /* FIXME: could be improved to also have expected parse
              * results listed above and compare them */
             LOG_INFO(MSTRO_LOG_MODULE_USER, "parsing type |%s|\n", *s);
             cheat_assert(MSTRO_OK==mstro_schema_type_parse(*s, &result));
             cheat_yield();
             cheat_assert(MSTRO_OK==mstro_stp_val_dispose(result));
             cheat_yield();
           }
	   cheat_assert(MSTRO_OK == mstro_finalize());
           )
