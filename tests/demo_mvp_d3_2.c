/* -*- mode:c -*- */
/** @file
 ** @brief   MVP for Maestro D3.2:
 **/

/*
   Driver

   - A multi-threaded application (pthread) consisting of

   - A proxy for the workflow component (WP4) (work description)

   - A producer application

   - two consumer applications C1 and C2

   - The producer creates CDOs (type -,1,2)

   - The consumer C1 (consumer) retrieves the CDOs and performs computations on it, then drops them

   - The consumer C2 (archiver) writes out the CDOs to permanent storage


   Maestro Pool MVP

    - Single node pool operation

    - CDO type 0, 1 and 2 declarations

    - Basic Offer-Demand functionality

    - Task coupling / CDO resolution through the pool

    - DEMAND operations with user-allocated buffers

    - Basic interface to Mamba for type 1,2 CDOs
 */

/** Implementation:
 * main thread:
 *    - constructs work description
 *    - posts (announces) a work description CDO for all to see.
 *    - starts threads for consumer, archiver tasks
 *    - posts CONSUMER_READY REQUIRE
 *      -- if multiple consumers, multiple such CDOs
 *    - posts ARCHIVER_READY REQUIRE
 *      -- if multiple archivers, multiple such CDOs
 *    - starts producer
 *    - waits for threads to finish
 *    - withdraws work description CDO
 *    - quits
 *
 * archiver:
 *    - waits for work description
 *    - posts CDO REQUIRES for all CDOs in the list
 *    - demands them one by one
 *    - writes them to disk (file name = CDO name) using Mamba tiling interface
 *    - disposes them
 *      -- when multiple archive threads run, archiver i
 *         handles CDOs with index%num_archivers==i
 *
 * consumer:
 *    - waits for work description
 *    - posts CDO REQUIRES for all CDOS in the list
 *    - demands them one by one
 *    - computes a checksum on the data, prints it
 *    - disposes them
 *      -- when multiple consumer threads run, consumer i
 *         handles CDOs with index%num_consumers==i
 *
 * producer:
 *    - waits for work description
 *    - posts CDO OFFER for all CDOs
 *    - posts CDO WITHDRAW for all CDOs
 *    - disposes them
 *      -- when multiple producer threads run, producer i
 *         handles CDOs with index%num_consumers==i
 *
 */
/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro.h"
#include "maestro/logging.h"

#include "mamba.h"
#include "mmb_tile_iterator.h"

#include <cyaml/cyaml.h>

#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "maestro/i_misc.h"


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)



/** name template of data CDOs sent around */
#define CDO_NAME_TEMPLATE "CDO %zu"
/** UB on length of CDO_NAME_TEMPLATE */
#define CDO_NAME_MAX (strlen(CDO_NAME_TEMPLATE)+10)

/* name of the dir to write CDOs*/
#define DIR_NAME "CDOs"

/** maximum number of CDOs in one run */
#define CDO_COUNT_MAX 100000

/** A structure holding the workflow configuration */
struct cdo_announcement {
  size_t num_producers;      /** number of producer threads */
  size_t num_consumers;      /** number of consumer threads */
  size_t num_archivers;      /** number of archiver threads */
  int64_t cdo_size;           /** size of each CDO */
  size_t num_entries;        /** number of CDOs */
  char *cdo_names[CDO_COUNT_MAX];   /** string names of the CDOs */
};

/** A suitable scope/layout/size string for the announcement */
char *announcement_scope_string;

/** A CDO name for the work announcement */
#define ANNOUNCEMENT_TOKEN_NAME "CDO Announcement"

/** A CDO name template for 'Consumer ready' */
#define CONSUMER_READY_NAME_TEMPLATE "Consumer Ready %zu"

/** A CDO name template for 'Archiver ready' */
#define ARCHIVER_READY_NAME_TEMPLATE "Archiver Ready %zu"

#define READY_NAME_MAX 32

/** A rigid version of the workflow configuration for cyaml to fill */
struct mvp_config_
{
  int num_producers;
  int num_consumers;
  int num_archivers;
  int cdo_size;
  int cdo_count;
};

typedef struct mvp_config_ * mvp_config;

/** cyaml schema */
static const cyaml_schema_field_t top_mapping_schema[] = {
	CYAML_FIELD_INT("num_producers", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mvp_config_, num_producers),
	CYAML_FIELD_INT("num_consumers", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mvp_config_, num_consumers),
	CYAML_FIELD_INT("num_archivers", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mvp_config_, num_archivers),
	CYAML_FIELD_INT("cdo_size", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mvp_config_, cdo_size),
	CYAML_FIELD_INT("cdo_count", CYAML_FLAG_DEFAULT | CYAML_FLAG_OPTIONAL,
			struct mvp_config_, cdo_count),
       	CYAML_FIELD_END
};

/* cyaml value schema for the top level mapping. */
static const cyaml_schema_value_t top_schema = {
	CYAML_VALUE_MAPPING(CYAML_FLAG_POINTER,
			struct mvp_config_, top_mapping_schema),
};

/** Basic cyaml config */
static const cyaml_config_t config = {
	.log_level = CYAML_LOG_WARNING, /* Logging errors and warnings only. */
	.log_fn = cyaml_log,            /* Use the default logging function. */
	.mem_fn = cyaml_mem,            /* Use the default memory allocator. */
};

/** Call cyaml to parse config file */
int
mvp_config_parse(mvp_config* handle, const char* filename)
{
  if (handle == NULL) return MSTRO_INVARG;

  cyaml_err_t err;
  err = cyaml_load_file(filename, &config,
  		&top_schema, (cyaml_data_t **)handle, NULL);
  if (err != CYAML_OK) {
    fprintf(stderr, "ERROR: %s (file %s)\n", cyaml_strerror(err), filename);
    return -1;
  }

  return 0;
}

/** announcement message receive: */
static void
wait_for_announcement(struct cdo_announcement *announcement)
{
  mstro_status s;
  mstro_cdo announcement_cdo;
  /* start by waiting for the announcement */
  s = mstro_cdo_declare(ANNOUNCEMENT_TOKEN_NAME,
                        MSTRO_ATTR_DEFAULT,
                        &announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to declare announcement CDO\n");
    abort();
  }
  s = mstro_cdo_attribute_set(announcement_cdo,
                              MSTRO_ATTR_CORE_CDO_RAW_PTR,
                              announcement, false, true);
  if(s!=MSTRO_OK) {
    ERR("Failed to set raw-ptr attribute on announcement CDO\n");
    abort();
  }

  s = mstro_cdo_attribute_set_yaml(announcement_cdo,
                                   announcement_scope_string);
  if(s!=MSTRO_OK) {
    ERR("Failed to set raw-ptr size attribute on announcement CDO\n");
    abort();
  }

  s = mstro_cdo_require(announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to require announcement CDO\n");
    abort();
  }

  s = mstro_cdo_demand(announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to withdraw announcement CDO\n");
    abort();
  }

  s = mstro_cdo_dispose(announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to dispose announcement CDO\n");
    abort();
  }

  return;
}

static void
do_announce(struct cdo_announcement *announcement, mstro_cdo *result)
{
  mstro_status s;
  mstro_cdo announcement_cdo;
  /* start by waiting for the announcement */
  s = mstro_cdo_declare(ANNOUNCEMENT_TOKEN_NAME,
                        MSTRO_ATTR_DEFAULT,
                        &announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to declare announcement CDO\n");
    abort();
  }
  s = mstro_cdo_attribute_set(announcement_cdo,
                              MSTRO_ATTR_CORE_CDO_RAW_PTR,
                              announcement, false, true);

  if(s!=MSTRO_OK) {
    ERR("Failed to set raw-ptr attribute on  announcement CDO\n");
    abort();
  }
  s = mstro_cdo_attribute_set_yaml(announcement_cdo,
                                   announcement_scope_string);
  if(s!=MSTRO_OK) {
    ERR("Failed to set raw-ptr size attribute on announcement CDO\n");
    abort();
  }

  s = mstro_cdo_offer(announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to offer announcement CDO\n");
    abort();
  }

  *result = announcement_cdo;
}

static void
withdraw_announcement(mstro_cdo announcement_cdo)
{
  mstro_status s;

  s = mstro_cdo_withdraw(announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to withdraw announcement CDO\n");
    abort();
  }

  s = mstro_cdo_dispose(announcement_cdo);
  if(s!=MSTRO_OK) {
    ERR("Failed to dispose announcement CDO\n");
    abort();
  }

  return;
}

/* do the equivalent of a sem_post using the given CDO name */
static void
cdo_sem_post(const char *name, mstro_cdo *sem)
{
  mstro_cdo cdo;
  mstro_status s;
  /* use type 0 CDO */
  s = mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT,
                        &cdo);
  s |= mstro_cdo_offer(cdo);
  if(s!=MSTRO_OK) {
    ERR("CDO sem post failed\n");
    abort();
  }
  *sem = cdo;
}

static void
cdo_sem_post_cleanup(mstro_cdo sem)
{
  mstro_status s = mstro_cdo_withdraw(sem);
  s |= mstro_cdo_dispose(sem);

  if(s!=MSTRO_OK) {
    ERR("CDO sem post cleanup failed\n");
    abort();
  }
}

/* do the equivalent of a sem_wait using the given CDO name */
static void
cdo_sem_wait(const char *name)
{
  mstro_cdo cdo;
  mstro_status s;
  /* use type 0 CDO */
  s = mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT,
                        &cdo);
  s |= mstro_cdo_require(cdo);
  s |= mstro_cdo_demand(cdo);
  s |= mstro_cdo_dispose(cdo);

  if(s!=MSTRO_OK) {
    ERR("CDO sem wait failed\n");
    abort();
  }

}
/* Quickly flash the "Hello" token */
void
declare_archiver_ready(size_t index, mstro_cdo *cdo)
{
  char name[READY_NAME_MAX];
  size_t s = snprintf(name, READY_NAME_MAX,
                      ARCHIVER_READY_NAME_TEMPLATE, index);
  if(s>=READY_NAME_MAX) {
    ERR("Failed to construct name");
    abort();
  }
  cdo_sem_post(name, cdo);
}

/* Block on a "Hello" token */
void
wait_archiver_ready(size_t index)
{
  char name[READY_NAME_MAX];
  size_t s = snprintf(name, READY_NAME_MAX,
                      ARCHIVER_READY_NAME_TEMPLATE, index);
  if(s>=READY_NAME_MAX) {
    ERR("Failed to construct name");
    abort();
  }
  cdo_sem_wait(name);
}

void
declare_consumer_ready(size_t index, mstro_cdo *cdo)
{
  char name[READY_NAME_MAX];
  size_t s = snprintf(name, READY_NAME_MAX,
                      CONSUMER_READY_NAME_TEMPLATE, index);
  if(s>=READY_NAME_MAX) {
    ERR("Failed to construct name");
    abort();
  }
  cdo_sem_post(name,cdo);
}

void
wait_consumer_ready(size_t index)
{
  char name[READY_NAME_MAX];
  size_t s = snprintf(name, READY_NAME_MAX,
                      CONSUMER_READY_NAME_TEMPLATE, index);
  if(s>=READY_NAME_MAX) {
    ERR("Failed to construct name");
    abort();
  }
  cdo_sem_wait(name);
}


static void
archiver_flush_to_disk(const char *name, mmbArray *a)
{
  size_t chunkdims = 4096;
  mmbDimensions chunks = {1, &chunkdims};
  mmbError stat = mmb_array_tile(a, &chunks);
  
  /* make a dir at the specified path*/
  mstro_status status = mkdirhier(DIR_NAME);
  if(status != MSTRO_OK) {
    ERR("Failed to create archiving dir %s: %d (%s)\n",
          DIR_NAME, errno, strerror(errno));
    return ;
  }

  size_t path_len = strlen(DIR_NAME) + strlen(name) + 2; /*separator and \0*/
  char *path = malloc(path_len*sizeof(char));
  sprintf(path,"%s/%s",DIR_NAME,name);
  if(stat != MMB_OK) {
    ERR("Failed to tile mamba array (chunked tiles)\n");
    abort();
  }
  mmbDimensions *tiling_dims = NULL;
  stat = mmb_tiling_dimensions(a, &tiling_dims);
  
  FILE *dst = fopen(path, "w");
  if(dst==NULL) {
    ERR("Failed to open %s for writing\n", path);
    abort();
  }

  for(size_t i = 0; i < tiling_dims->d[0]; i++){
    mmbArrayTile* tile = NULL;
    stat = mmb_tile_at_1d(a, i, &tile);
    if(stat != MMB_OK) {
      ERR("Failed to obtain tile %zu: %d\n", i, stat);
      abort();
    }
    size_t count = fwrite(&MMB_IDX_1D(tile, tile->lower[0], char),
                          1,
                          tile->upper[0]-tile->lower[0],
                          dst);
    if(count!= tile->upper[0]-tile->lower[0]) {
      ERR("Incomplete write on tile %d of %s\n", i, path);
      abort();
    }
  }
  if(0!=fclose(dst)) {
    ERR("Failed to close %s after writing\n", path);
    abort();
  }

  // destroy tiling dim info:
  stat = mmb_dimensions_destroy(tiling_dims);
  if(stat != MMB_OK) {
    ERR("Failed to destroy tiling dimension info\n");
    abort();
  }
  
  // Remove tiling
  stat = mmb_array_untile(a);
  if(stat != MMB_OK) {
    ERR("Failed to untile mamba array\n");
    abort();
  }

  free(path);

}

/*
 * archiver:
 *    - waits for work description
 *    - posts CDO REQUIRES for all CDOs in the list
 *    - demands them one by one
 *    - writes them to disk (file name = CDO name) using Mamba tiling interface
 *    - disposes them
 *      -- when multiple archive threads run, archiver i
 *         handles CDOs with index%num_archivers==i
 */
void*
archiver_thread_fun(void *closure)
{
  mstro_status s;
  size_t my_idx = * (size_t*)closure;
  INFO("Archiver %zu starting\n", my_idx);

  struct cdo_announcement *announcement
      = malloc(sizeof(struct cdo_announcement));
  if(announcement==NULL) {
    ERR("Failed to allocate for incoming announcement\n");
    abort();
  }

  wait_for_announcement(announcement);

  mstro_cdo incoming[announcement->num_entries];
  void *incoming_buffers[announcement->num_entries];

  /* declare and require all that we are responsible for */
  for(size_t i=0; i<announcement->num_entries; i++) {
    if(i%announcement->num_archivers==my_idx) {
      /* declare it */
      s = mstro_cdo_declare(announcement->cdo_names[i],
                            MSTRO_ATTR_DEFAULT,
                            &(incoming[i]));
      if(s!=MSTRO_OK) {
        ERR("Failed to declare CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* add allocation */
      incoming_buffers[i] = malloc(announcement->cdo_size);
      if(incoming_buffers[i]==NULL) {
        ERR("Cannot allocate CDO buffer for archiving\n");
        abort();
      }
      s = mstro_cdo_attribute_set(incoming[i],
                                  MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                  incoming_buffers[i], false, true);
      s |= mstro_cdo_attribute_set(incoming[i],
                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                   &announcement->cdo_size, true, false);
      //      INFO("archiver cdo %d incoming buffer %p\n", i, incoming_buffers[i]);

      if(s!=MSTRO_OK) {
        ERR("Failed to add buffer to CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* require it */
      s = mstro_cdo_require(incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to require CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
    }
  }
  /* send ACK that we're ready */
  INFO("Declaring Archiver %zu ready\n", my_idx);
  mstro_cdo ready_cdo;
  declare_archiver_ready(my_idx,&ready_cdo);

  /* process all in some order */
  for(size_t i=0; i<announcement->num_entries; i++) {
    if(i%announcement->num_archivers==my_idx) {
      s = mstro_cdo_demand(incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to demand CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }

      /* extract mamba pointer */
      mmbArray *mamba_array;
      s = mstro_cdo_access_mamba_array(incoming[i], &mamba_array);
      if(s!=MSTRO_OK) {
        ERR("Failed to extract mamba array from CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }

      /* do write-out */
      archiver_flush_to_disk(announcement->cdo_names[i], mamba_array);

      s = mstro_cdo_dispose(incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to dispose CDO %s after archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
      free(incoming_buffers[i]);
    }
  }

  cdo_sem_post_cleanup(ready_cdo);
  free(announcement);
  return NULL;
}

void buf_fill_rand(const char* name, char* b, int size)
{
  size_t i;
  for (i=0; i<size/sizeof(char); i++)
    b[i] = (unsigned char)rand()%256;
}

/*
 * producer:
 *    - waits for work description
 *    - posts CDO OFFER for all CDOs
 *    - posts CDO WITHDRAW for all CDOs
 *    - disposes them
 *      -- when multiple producer threads run, producer i
 *         handles CDOs with index%num_consumers==i
 */
void*
producer_thread_fun(void *closure)
{
  mstro_status s;
  size_t my_idx = * (size_t*)closure;
  INFO("Producer %zu starting\n", my_idx);

  struct cdo_announcement *announcement
      = malloc(sizeof(struct cdo_announcement));
  if(announcement==NULL) {
    ERR("Failed to allocate for incoming announcement\n");
    abort();
  }

  wait_for_announcement(announcement);

  /* produce */
  mstro_cdo outgoing[announcement->num_entries];
  void *outgoing_buffers[announcement->num_entries];

  for(size_t i=0; i<announcement->num_entries; i++) {
    if(i%announcement->num_producers==my_idx) {
      /* declare it */
      s = mstro_cdo_declare(announcement->cdo_names[i],
                            MSTRO_ATTR_DEFAULT,
                            &(outgoing[i]));
      if(s!=MSTRO_OK) {
        ERR("Failed to declare outgoing CDO %s\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* add allocation */
      outgoing_buffers[i] = malloc(announcement->cdo_size);
      if(outgoing_buffers[i]==NULL) {
        ERR("Cannot allocate outgoing CDO buffer\n");
        abort();
      }
      /* add some data */
      buf_fill_rand(announcement->cdo_names[i],
                    outgoing_buffers[i], announcement->cdo_size);

      s = mstro_cdo_attribute_set(outgoing[i],
                                  MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                  outgoing_buffers[i], false, true);
      s |= mstro_cdo_attribute_set(outgoing[i],
                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                   &announcement->cdo_size, true, false);

      if(s!=MSTRO_OK) {
        ERR("Failed to add outgoing buffer to CDO %s\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* OFFER it */
      s = mstro_cdo_offer(outgoing[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to offer CDO %s\n",
            announcement->cdo_names[i]);
        abort();
      }
    }
  }
  /* now claim them back */
  for(size_t i=0; i<announcement->num_entries; i++) {
    if(i%announcement->num_producers==my_idx) {
      s = mstro_cdo_withdraw(outgoing[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to demand outgoing CDO %s\n",
            announcement->cdo_names[i]);
        abort();
      }

      s = mstro_cdo_dispose(outgoing[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to dipose outgoing CDO %s\n",
            announcement->cdo_names[i]);
        abort();
      }
      free(outgoing_buffers[i]);
    }
  }

  free(announcement);
  return NULL;
}


void
mvp_checksum(const char* name, void* rawptr, uint64_t size)
{
  unsigned char x;
  uint64_t i;

  for (i=0,x=0;i<size; i++)
    x ^= ((unsigned char*)rawptr)[i];

  INFO("Checksum for cdo \"%s\": %d\n", name, x);
}

void
consumer_flush_to_disk(const char *name, void *a, uint64_t size)
{
  
  size_t len = strlen(DIR_NAME) + 1 + strlen("consumer_") + strlen(name)+1; 
  char *file_name = malloc(sizeof(char)*len);
  sprintf(file_name, "%s/consumer_%s",DIR_NAME,name);

  /* make a dir at the specified path*/
  mstro_status status = mkdirhier(DIR_NAME);
  if(status != MSTRO_OK) {
    ERR("Failed to create archiving dir %s: %d (%s)\n",
          DIR_NAME, errno, strerror(errno));
    return ;
  }

  FILE *dst = fopen((const char*)file_name, "w");
  if(dst==NULL) {
    ERR("Failed to open %s for writing\n", file_name);
    abort();
  }

  size_t count = fwrite(a,
                        sizeof(char),
                        size/sizeof(char),
                        dst);
  if(count != size) {
    ERR("Incomplete write of %s (%zu of  %"PRIu64")\n", file_name, count, size);
    abort();
  }
  if(0!=fclose(dst)) {
    ERR("Failed to close %s after writing\n", file_name);
    abort();
  }
  free(file_name);
}

/*
 * consumer:
 *    - waits for work description
 *    - posts CDO REQUIRES for all CDOS in the list
 *    - demands them one by one
 *    - computes a checksum on the data, prints it
 *    - disposes them
 *      -- when multiple consumer threads run, consumer i
 *         handles CDOs with index%num_consumers==i
 */
void *
consumer_thread_fun(void *closure)
{
  mstro_status s;
  size_t my_idx = * (size_t*)closure;
  INFO("Consumer %zu starting\n", my_idx);

  struct cdo_announcement *announcement
      = malloc(sizeof(struct cdo_announcement));
  if(announcement==NULL) {
    ERR("Failed to allocate for incoming announcement\n");
    abort();
  }

  wait_for_announcement(announcement);

  /* post requests */
  mstro_cdo incoming[announcement->num_entries];
  void *incoming_buffers[announcement->num_entries];

  /* declare and require all that we are responsible for */
  for(size_t i=0; i<announcement->num_entries; i++) {
    if(i%announcement->num_consumers==my_idx) {
      /* declare it */
      s = mstro_cdo_declare(announcement->cdo_names[i],
                            MSTRO_ATTR_DEFAULT,
                            &incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to declare CDO %s for consumption\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* add allocation */
      incoming_buffers[i] = malloc(announcement->cdo_size);
      if(incoming_buffers[i]==NULL) {
        ERR("Cannot allocate CDO buffer for comsumption\n");
        abort();
      }
      s = mstro_cdo_attribute_set(incoming[i],
                                  MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                  incoming_buffers[i], false, true);
      s |= mstro_cdo_attribute_set(incoming[i],
                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                   &announcement->cdo_size, true, false);
      //      INFO("consumer cdo %d incoming buffer %p\n", i, incoming_buffers[i]);

      if(s!=MSTRO_OK) {
        ERR("Failed to add buffer to CDO %s for consumption\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* require it */
      s = mstro_cdo_require(incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to require CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
    }
  }

  /* send ACK that we're ready */
  mstro_cdo ready_cdo;

  INFO("Declaring Consumer %zu ready\n", my_idx);
  declare_consumer_ready(my_idx,&ready_cdo);

  /* process all in some order */
  for(size_t i=0; i<announcement->num_entries; i++) {
    if(i%announcement->num_consumers==my_idx) {
      s = mstro_cdo_demand(incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to demand CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }

      /* extract raw ptr */
      void* rawptr;
      enum mstro_cdo_attr_value_type type;
      const int64_t* size;
      s = mstro_cdo_access_ptr(incoming[i], &rawptr, NULL);
      if(s!=MSTRO_OK) {
        ERR("Failed to extract raw pointer from CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
      /* query the size */
      s = mstro_cdo_attribute_get(incoming[i],
                                  MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                  &type, (const void**)&size);
      if(s!=MSTRO_OK) {
        ERR("Failed to extract local size from CDO %s for archiving\n",
            announcement->cdo_names[i]);
        abort();
      }

      /* do some work */
      mvp_checksum(announcement->cdo_names[i], rawptr, *size);

      /* write-out */
      consumer_flush_to_disk(announcement->cdo_names[i],
                             rawptr, *size);

      s = mstro_cdo_dispose(incoming[i]);
      if(s!=MSTRO_OK) {
        ERR("Failed to dispose CDO %s after archiving\n",
            announcement->cdo_names[i]);
        abort();
      }
      free(incoming_buffers[i]);
    }
  }

  /* demand data and process */


  cdo_sem_post_cleanup(ready_cdo);
  free(announcement);
  return NULL;
}



#define DEFAULT_NUM_PRODUCERS 1
#define DEFAULT_NUM_CONSUMERS 1
#define DEFAULT_NUM_ARCHIVERS 1
#define DEFAULT_CDO_SIZE      1024
#define DEFAULT_CDO_COUNT     42

#ifdef  TOPSRCDIR
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define CONFIG_FILE_PATH TOSTRING(TOPSRCDIR) "/tests/demo_mvp_d3_2_config.yaml"
#else
#define CONFIG_FILE_PATH "./demo_mvp_d3_2_config.yaml"
#endif


/*
 * main thread:
 *    - constructs work description
 *    - posts (announces) a work description CDO for all to see.
 *    - starts threads for consumer, archiver tasks
 *    - posts CONSUMER_READY REQUIRE
 *      -- if multiple consumers, multiple such CDOs
 *    - posts ARCHIVER_READY REQUIRE
 *      -- if multiple archivers, multiple such CDOs
 *    - starts producer
 *    - waits for threads to finish
 *    - withdraws work description CDO
 *    - quits
*/
int main(int argc, char **argv)
{
 /* verbosity */
  setenv("MSTRO_LOG_LEVEL", "2", 1);
  setenv("MSTRO_LOG_COLOR_ERRORS", "1", 1);
  setenv("MMB_LOG_LEVEL","0", 1);

  mstro_status s = mstro_init("Maestro D3.2 Demo MVP",
                              "Main", 0);
  if(s!=MSTRO_OK) {
    ERR("Failed to initialize Maestro\n");
    return s;
  }

  struct cdo_announcement announcement;
  mvp_config config_handle=NULL;
  int err;

 /* constructs work description */
  switch(argc) {
    default:
      WARN("Command line arguments ignored\n");
      /* fallthrough */
    case 1:

      err = mvp_config_parse(&config_handle, CONFIG_FILE_PATH);
      if (err != 0) {
        WARN("Failed to parse a config file. Using default config.\n");
        announcement.num_producers = DEFAULT_NUM_PRODUCERS;
        announcement.num_consumers = DEFAULT_NUM_CONSUMERS;
        announcement.num_archivers = DEFAULT_NUM_ARCHIVERS;
        announcement.cdo_size = DEFAULT_CDO_SIZE;
        announcement.num_entries = DEFAULT_CDO_COUNT;
      }
      else {
        announcement.num_producers = config_handle->num_producers;
        announcement.num_consumers = config_handle->num_consumers;
        announcement.num_archivers = config_handle->num_archivers;
        announcement.cdo_size = config_handle->cdo_size;
        announcement.num_entries = config_handle->cdo_count;
      }

      break;
  }
  assert(announcement.num_entries<=CDO_COUNT_MAX);

  pthread_t producers[announcement.num_producers];
  pthread_t consumers[announcement.num_consumers];
  pthread_t archivers[announcement.num_archivers];
  size_t producer_idxs[announcement.num_producers];
  size_t consumer_idxs[announcement.num_consumers];
  size_t archiver_idxs[announcement.num_archivers];

  for(size_t i=0; i<announcement.num_entries; i++) {
    announcement.cdo_names[i] = malloc(CDO_NAME_MAX);
    if(announcement.cdo_names[i]==NULL) {
      ERR("Failed to allocate CDO name\n");
      goto BAILOUT;
    }
    size_t l =
        snprintf(announcement.cdo_names[i], CDO_NAME_MAX,
                 CDO_NAME_TEMPLATE, i);
    if(l>=CDO_NAME_MAX) {
      ERR("CDO name does not fit:" CDO_NAME_TEMPLATE, i);
      goto BAILOUT;
    }
  }

  announcement_scope_string = malloc(128);
  if(announcement_scope_string==NULL)
    abort();
  snprintf(announcement_scope_string, 128,
           "scope:\n"
           "  local-size: %zu", sizeof(struct cdo_announcement));

 /* posts (announces) a work description CDO for all to see */
  mstro_cdo announcement_cdo;
  do_announce(&announcement, &announcement_cdo);


  /* start consumers */
  INFO("Starting %zu consumers\n", announcement.num_consumers);
  for(size_t i=0; i<announcement.num_consumers; i++) {
    consumer_idxs[i] = i;
    pthread_create(&consumers[i], NULL,
                   consumer_thread_fun,
                   &(consumer_idxs[i]));
  }
  /* start archivers */
  INFO("Starting %zu archivers\n", announcement.num_archivers);
  for(size_t i=0; i<announcement.num_archivers; i++) {
    archiver_idxs[i] = i;
    pthread_create(&archivers[i], NULL,
                   archiver_thread_fun,
                   &(archiver_idxs[i]));
  }

  for(size_t i=0; i<announcement.num_consumers; i++) {
    INFO("Waiting for Consumer %zu to be ready\n");
    wait_consumer_ready(i);
  }
  for(size_t i=0; i<announcement.num_archivers; i++) {
    INFO("Waiting for Archiver %zu to be ready\n");
    wait_archiver_ready(i);
  }

  /* start producers */
  INFO("Starting %zu producers\n", announcement.num_producers);
  for(size_t i=0; i<announcement.num_producers; i++) {
    producer_idxs[i]=i;
    pthread_create(&producers[i], NULL,
                   producer_thread_fun,
                   &(producer_idxs[i]));
  }


  /* wait for threads to complete */
  for(size_t i=0; i<announcement.num_producers; i++) {
    pthread_join(producers[i], NULL);
  }
  for(size_t i=0; i<announcement.num_consumers; i++) {
    pthread_join(consumers[i], NULL);
  }
  for(size_t i=0; i<announcement.num_archivers; i++) {
    pthread_join(archivers[i], NULL);
  }

  /* withdraw announcement */
  withdraw_announcement(announcement_cdo);


  for(size_t i=0; i<announcement.num_entries; i++) {
    free(announcement.cdo_names[i]);
  }

  fprintf(stdout, "Sent %zu CDOs of %" PRIi64 " bytes each from %zu produce to %zu compute and %zu archiving threads\n",
          announcement.num_entries,
          announcement.cdo_size,
          announcement.num_producers,
          announcement.num_consumers,
          announcement.num_archivers);



BAILOUT:
  if(config_handle) free(config_handle);

  return s;
}
