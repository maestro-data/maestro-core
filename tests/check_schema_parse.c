/* -*- mode:c -*- */
/** @file
 ** @brief check schema parsing
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>

#include "maestro-schema.h"

#ifndef TOPSRCDIR
#error TOPSRCDIR needs to be defined to compile this so that we can find the yaml schema sources
#endif

#define XSTRINGIFY(s) #s
#define STRINGIFY(s) XSTRINGIFY(s) 

#define CORE_YAML STRINGIFY(TOPSRCDIR)  "/attributes/maestro-core.yaml"
#define USER_YAML STRINGIFY(TOPSRCDIR)  "/attributes/user.yaml"
#define ECMWF_YAML STRINGIFY(TOPSRCDIR) "/attributes/ecmwf.yaml"
#define SAMPLE_YAML_FILE STRINGIFY(TOPSRCDIR) "/tests/sample_attributes.yaml"

#define SAMPLE_YAML_STRING                      \
  ".maestro.core.cdo:\n"                        \
  "   persist: \"on\"\n"                        \
  "   scope:\n"                                 \
  "    size: 1024\n"                            \
  ".maestro.core:\n"                            \
  "   cdo.maestro-provided-storage: false"



CHEAT_TEST(core_schema_parse,
           mstro_schema s1=NULL, s2=NULL, s3=NULL;
           mstro_attribute_dict dict=NULL;
           mstro_status s=MSTRO_OK;
           cheat_assert(MSTRO_OK == mstro_init("Tests","Schema parse",0));
           s=mstro_schema_parse_from_file(USER_YAML, &s2);
           cheat_assert(MSTRO_OK==s);
           cheat_yield();
          
           /* some error cases with documented error codes */
           cheat_assert(MSTRO_INVARG==mstro_schema_free(NULL));
           cheat_assert(strcmp("(null)",mstro_schema_name(NULL))==0);
           cheat_assert(UINT64_MAX==mstro_schema_version(NULL));
           cheat_assert(MSTRO_INVARG==mstro_schema_parse(NULL,0,NULL));
           cheat_assert(MSTRO_INVOUT==mstro_schema_parse( (uint8_t *)"foo",3,NULL));
           cheat_assert(MSTRO_FAIL==mstro_schema_parse_from_file("nonexistant_schema",&s1));
           cheat_assert(MSTRO_INVARG==mstro_attribute_dict_message_dispose(NULL,NULL));
           cheat_assert(MSTRO_INVARG==mstro_attribute_dict_to_message(NULL,NULL));
           cheat_yield();

           s=mstro_schema_parse_from_file(CORE_YAML, &s1);
           if(MSTRO_OK!=s) {
             /* clean up successful one so that valgrind is happy
              * even on failure */
             cheat_assert(MSTRO_OK==mstro_schema_free(s2));
           }
           cheat_assert(MSTRO_OK==s);
           cheat_yield();
           
           s=mstro_schema_parse_from_file(ECMWF_YAML, &s3);
           if(MSTRO_OK!=s) {
             /* clean up successful ones so that valgrind is happy
              * even on failure */
             cheat_assert(MSTRO_OK==mstro_schema_free(s1));
             cheat_assert(MSTRO_OK==mstro_schema_free(s2));
           }
           cheat_assert(MSTRO_OK==s);
           cheat_yield();

           cheat_assert(MSTRO_OK==mstro_schema_merge(s1, s2));
           cheat_yield();
           cheat_assert(MSTRO_OK==mstro_schema_merge(s1, s3));
           cheat_yield();

           /* now use the merged schema to parse some sample defs */
           cheat_assert(MSTRO_OK==mstro_attributes_parse(s1,
                                                         SAMPLE_YAML_STRING,
                                                         &dict, NULL));
           cheat_assert(MSTRO_OK==mstro_attributes_parse_from_file(s1,
                                                         SAMPLE_YAML_FILE,
                                                         &dict, NULL));
           cheat_yield();

           cheat_assert(MSTRO_OK==mstro_attribute_dict_dispose(dict));

           /* s1 is the merged one, and freeing it will recurse */
           cheat_assert(MSTRO_OK==mstro_schema_free(s1)); 
           cheat_assert(MSTRO_OK == mstro_finalize());           
           )

CHEAT_TEST(core_schema_parse_builtin,
           mstro_schema s1=NULL, s2=NULL;

           mstro_attribute_dict dict=NULL;
           mstro_status s=MSTRO_OK;
           cheat_assert(MSTRO_OK == mstro_init("Tests","Schema parse buitin",0));
           cheat_assert(MSTRO_OK==mstro_schema_parse(MSTRO_SCHEMA_BUILTIN_YAML_CORE,
                                                    MSTRO_SCHEMA_BUILTIN_YAML_CORE_LEN, &s1));
           cheat_yield();
           
           s=mstro_schema_parse(MSTRO_SCHEMA_BUILTIN_YAML_ECMWF,
                                MSTRO_SCHEMA_BUILTIN_YAML_ECMWF_LEN,
                                &s2);
           if(MSTRO_OK!=s) {
             /* clean up successful ones so that valgrind is happy
              * even on failure */
             cheat_assert(MSTRO_OK==mstro_schema_free(s1));
           }
           cheat_assert(MSTRO_OK==s);
           cheat_yield();

           cheat_assert(MSTRO_OK==mstro_schema_merge(s1, s2));
           cheat_yield();



           /* now use the merged schema to parse some sample defs */
           cheat_assert(MSTRO_OK==mstro_attributes_parse(s1,
                                                         SAMPLE_YAML_STRING,
                                                         &dict, NULL));
           cheat_yield();

           /* no value present, fetching without default: no entry for KEY */
           cheat_assert(MSTRO_NOENT==
                        mstro_attribute_dict_get(dict, "nonexistant", NULL, NULL, NULL, false));

           /* try a direct lookup */
           const char *key = ".maestro.core.cdo.scope.size";
           enum mstro_cdo_attr_value_type type;
           const void *val;
           mstro_attribute_entry entry;
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, key, NULL, NULL, NULL, true));
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, key, NULL, NULL, NULL, false));
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, key, &type, NULL, NULL, true));
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, key, NULL, &val, NULL, true));
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, key, NULL, NULL, &entry, true));
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, key, &type, &val, &entry, true));
           cheat_assert(type==MSTRO_CDO_ATTR_VALUE_int64); // since the schema says 'int(min=-1)'
           cheat_assert(*((int64_t*)val)==1024);
           cheat_assert(entry!=NULL);

           int64_t newval = 42;
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_set(dict, key, MSTRO_CDO_ATTR_VALUE_int64, &newval, false, true));
           /* wrong type expectation must fail */
           cheat_assert(MSTRO_INVARG==
                        mstro_attribute_dict_set(dict, key, MSTRO_CDO_ATTR_VALUE_uint64, &newval, false, true));
           
           /* illegal key must fail */
           char *wrongkey = ".maestro.core.cdo.funny";
           cheat_assert(MSTRO_NOENT==
                        mstro_attribute_dict_set(dict, wrongkey, MSTRO_CDO_ATTR_VALUE_int64, &newval, false, true));

           /* legal, but previously unset key */
           mstro_timestamp ts = { 1,2,3};
           char *ecmwfdate = ".maestro.ecmwf.date";
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_set(dict, ecmwfdate, MSTRO_CDO_ATTR_VALUE_INVALID, &ts, false, true));

           /* and query the TS */
           const void *tsval;
           cheat_assert(MSTRO_OK==
                        mstro_attribute_dict_get(dict, ecmwfdate, &type, &tsval, NULL, true));
           const mstro_timestamp *tsval_cast = (const mstro_timestamp*)tsval;
           cheat_assert(tsval_cast->sec==ts.sec && tsval_cast->nsec==ts.nsec && tsval_cast->offset==ts.offset);
           
           
           cheat_assert(MSTRO_OK==mstro_attribute_dict_dispose(dict));
                        
           /* s1 is the merged one, and freeing it will recurse */
           cheat_assert(MSTRO_OK==mstro_schema_free(s1)); 
           cheat_assert(MSTRO_OK == mstro_finalize());
           )


           
