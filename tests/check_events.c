/* check event handling infrastructure  */
/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/logging.h"
#include "maestro/i_event.h"

#include <inttypes.h>

CHEAT_DECLARE(
    void handler1(mstro_event e, void* closure) {
      mstro_event_id id;
      cheat_assert(MSTRO_OK==mstro_event_id_get(e, &id));
      
      fprintf(stdout,
              "Handler 1 called on event %p (id %" PRIu64 "), closure %p\n",
              e, id, closure);
    }

    void handler2(mstro_event e, void* closure) {
      mstro_event_id id;
      cheat_assert(MSTRO_OK==mstro_event_id_get(e, &id));
      
      fprintf(stdout,
              "Handler 1 called on event %p (id %" PRIu64 "), closure %p\n",
              e, id, closure);
    }
              )

CHEAT_TEST(core_events,
           
           mstro_event_domain d1,d2;
           cheat_assert(MSTRO_OK==mstro_event_domain_create(
               MSTRO_EVENT_DOMAIN_ANONYMOUS, &d1));
           cheat_assert(MSTRO_OK==mstro_event_domain_create(
               "Domain 2", &d2));
           void *cl1 = malloc(12);
           void *cl2 = malloc(13);
           cheat_assert(cl1!=NULL);
           cheat_assert(cl2!=NULL);

           mstro_event e1,e2;
           mstro_event e3,e4,e5;

           cheat_yield();
           cheat_assert(MSTRO_OK==mstro_event_create(d1, handler1, cl1,
                                                     free, true,  &e1));
           cheat_assert(MSTRO_OK==mstro_event_create(d2, handler1, NULL,
                                                     NULL, false, &e2));
           cheat_assert(MSTRO_OK==mstro_event_create(d2, handler2, cl2,
                                                     NULL, false, &e3));
           cheat_assert(MSTRO_OK==mstro_event_create(d2, handler2, NULL,
                                                     NULL, false,  &e4));
           cheat_assert(MSTRO_OK==mstro_event_create(d2, handler2, NULL,
                                                     NULL, false,  &e5));

           cheat_yield();
           
           mstro_event_id id1,id2;
           mstro_event_id id3,id4, id5;
           cheat_assert(MSTRO_OK==mstro_event_id_get(e1, &id1));
           cheat_assert(MSTRO_OK==mstro_event_id_get(e2, &id2));
           cheat_assert(MSTRO_OK==mstro_event_id_get(e3, &id3));
           cheat_assert(MSTRO_OK==mstro_event_id_get(e4, &id4));
           cheat_assert(MSTRO_OK==mstro_event_id_get(e5, &id5));

           cheat_yield();


           void *result1=(void*)(intptr_t)0xdeadbeef;
           void *result2=(void*)(intptr_t)0xdeadbeef;
           cheat_assert(MSTRO_OK==mstro_event_trigger(e1,&result1));
           cheat_assert(MSTRO_OK==mstro_event_trigger(e3,&result2));

           /* auto-destroy: no result */
           cheat_assert(NULL==result1);
           /* receive closure on non-auto-destroy event */
           cheat_assert(cl2==result2);
           free(cl2);
           
           /* e1 auto-deallocated */
           /* e2 left to be auto-destroyed when deallocating d1 */

           cheat_assert(MSTRO_OK==mstro_event_destroy(e4));
           
           cheat_assert(MSTRO_OK==mstro_event_destroy_by_id(d2, id3));
           /* /\* e4,e5 auto-deallocated *\/ */

           fprintf(stdout, "before domain-dtors\n");
           cheat_yield();
           fflush(stdout);
           cheat_assert(MSTRO_OK==mstro_event_domain_destroy(d1));
           fflush(stdout);
           cheat_assert(MSTRO_OK==mstro_event_domain_destroy(d2));
                        
           )
           
