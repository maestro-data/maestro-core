#include <stdio.h>
#include <assert.h>
#include "maestro.h"

int main(int argc, char* argv[])
{
	mstro_status s = MSTRO_OK;

	s = mstro_init(getenv("MSTRO_WORKFLOW_NAME"), getenv("MSTRO_COMPONENT_NAME"), 0);
        assert(s==MSTRO_OK);

	mstro_cdo_selector selector = NULL;
	s = mstro_cdo_selector_create(NULL, NULL, "(has .maestro.core.cdo.name)", &selector);
        assert(s==MSTRO_OK);

	mstro_subscription offer_subscription;
	s = mstro_subscribe(selector, MSTRO_POOL_EVENT_OFFER,
				MSTRO_SUBSCRIPTION_OPTS_SIGNAL_BEFORE|MSTRO_SUBSCRIPTION_OPTS_REQUIRE_ACK,
				&offer_subscription);
	assert(s==MSTRO_OK);

	s = mstro_cdo_selector_dispose(selector);
        assert(s==MSTRO_OK);

	mstro_pool_event event;
	s = mstro_subscription_wait(offer_subscription, &event);
	assert(s==MSTRO_OK);
	
	const char* name = event->payload.offer.cdo_name;
	mstro_cdo cdo;
	printf("Offer occured for: %s\n", event->payload.offer.cdo_name);

	s  = mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo);
	s |= mstro_cdo_require(cdo);
	s |= mstro_subscription_ack(offer_subscription, event);
        assert(s==MSTRO_OK);

	printf("Demanding and disposing alternate handle: %s", name);
	mstro_cdo another_handle;
	s  = mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &another_handle);
	s |= mstro_cdo_require(another_handle);
	s |= mstro_cdo_demand(another_handle);
	s |= mstro_cdo_dispose(another_handle);
        assert(s==MSTRO_OK);

	s = mstro_cdo_retract(cdo);
	s |= mstro_cdo_dispose(cdo);
        assert(s==MSTRO_OK);
        s  = mstro_pool_event_dispose(event);
	s |= mstro_subscription_dispose(offer_subscription);

	s |= mstro_finalize();
        assert(s==MSTRO_OK);

	return 0;
}
