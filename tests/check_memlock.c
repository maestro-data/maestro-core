/* -*- mode:c -*- */
/** @file
 ** @brief check mstro_memlock
 **/

/*
 * Copyright (C) 2020 HPE, HP Schweiz GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_memlock.h"
#include <unistd.h>


CHEAT_TEST(lock_unlock,
           {
	     cheat_assert(MSTRO_OK == mstro_init("Tests","Memlock",0));
             void *x =malloc(8000);
             cheat_assert(x!=NULL);

             cheat_assert(MSTRO_OK==mstro_memlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memunlock(x,8000));

             free(x);
	     cheat_assert(MSTRO_OK == mstro_finalize());
           })


CHEAT_TEST(lock_unlock_multi,
           {
             cheat_assert(MSTRO_OK==mstro_memlock_init(2*8000));
             void *x =malloc(8000);
             cheat_assert(x!=NULL);

             cheat_assert(MSTRO_OK==mstro_memlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memunlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memunlock(x,8000));

             cheat_assert(MSTRO_OK==mstro_memlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memunlock(x,8000));
             cheat_assert(MSTRO_OK==mstro_memunlock(x,8000));

             cheat_assert(MSTRO_FAIL==mstro_memunlock(x,8000));

             free(x);
             cheat_assert(MSTRO_OK==mstro_memlock_finalize());
           })

CHEAT_TEST(lock_overlapping,
           {
             long pagesize = sysconf(_SC_PAGESIZE);
             void *x = malloc(3*pagesize);
             cheat_assert(x!=NULL);
             cheat_assert(pagesize>4);

             cheat_assert(MSTRO_OK==mstro_memlock_init(4*pagesize));

             /* one on the first page */
             void *start1 = x;
             size_t  len1 = pagesize/2;
             /* one on the same page */
             void *start2 = (void*)((uintptr_t)x+len1);
             size_t  len2 = pagesize/4;
             /* one extending into the third page */
             void *start3 = (void*)((uintptr_t)x+len1+len2);
             size_t  len3 = 2*pagesize;

             cheat_assert(MSTRO_OK==mstro_memlock(start1, len1));
             cheat_assert(MSTRO_OK==mstro_memlock(start2, len2));
             cheat_assert(MSTRO_OK==mstro_memlock(start3, len3));

             cheat_assert(MSTRO_OK==mstro_memunlock(start2, len2));
             cheat_assert(MSTRO_OK==mstro_memunlock(start1, len1));
             cheat_assert(MSTRO_OK==mstro_memunlock(start3, len3));

             free(x);
             cheat_assert(MSTRO_OK==mstro_memlock_finalize());
           })
