/* -*- mode:c -*- */
/** @file
 ** @brief check CDO declare/.../dispose, many times 
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_statistics.h"
#include "maestro/logging.h"
#include <string.h>


CHEAT_TEST(cdo_local_pool_stress,
           /* save print time: */
	   setenv("MSTRO_LOG_LEVEL","0", 1);
           cheat_assert(MSTRO_OK == mstro_init("Tests","POOL",0));
           char name[] = "Test CDO name üÜöÖäÄ";
           mstro_cdo cdo=NULL;
           mstro_status s = MSTRO_OK;
           mstro_nanosec_t before = mstro_clock();
           for(size_t i=0; i<100000; i++) {
                mstro_status s1,s2,s3,s4;
		s1= mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo);
		s2= mstro_cdo_offer(cdo);
		s3= mstro_cdo_withdraw(cdo);
		s4= mstro_cdo_dispose(cdo);

		s = s | s1 | s2 | s3 | s4;
		}
           mstro_nanosec_t after = mstro_clock();
           double duration = after-before;
           double ms = duration/(1000.0*1000.0);
           fprintf(stdout, "Spent %.2f ms for 100k CDO round trips\n",
                   ms);
           cheat_assert(MSTRO_OK==s);

           cheat_assert(MSTRO_OK == mstro_finalize());
           )



