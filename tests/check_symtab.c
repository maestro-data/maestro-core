/* -*- mode:c -*- */
/** @file
 ** @brief check symbol table operations
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>

#include "symtab.h"



CHEAT_TEST(symtab,
           const char *s1 = "Constant string";
           const char *s2 = "Constant string";
           char *s3 = "Different string";
           char *s4 = "Another string";
           char *s5 = "Another string";


           mstro_symtab tab;
           mstro_symbol sym1, sym1a, sym2, sym3, sym4;

           cheat_assert(MSTRO_OK == mstro_init("Tests","Symtab",0)); 
           cheat_assert(MSTRO_OK==mstro_symtab_create(&tab));
           cheat_assert(tab==NULL);
           cheat_yield();

           /* two permanent entries get same symbol? */
           cheat_assert(MSTRO_OK==mstro_symtab_intern_permanent(&tab,s1,&sym1));
           cheat_assert(MSTRO_OK==mstro_symtab_intern_permanent(&tab,s1,&sym1a));
           cheat_assert(sym1==sym1a);
           cheat_yield();

           /* non-permanent same string gets same entry */
           cheat_assert(MSTRO_OK==mstro_symtab_intern(&tab,s2,&sym2));
           cheat_assert(sym2==sym1);
           cheat_yield();

           /* other things work */
           cheat_assert(MSTRO_OK==mstro_symtab_intern(&tab,s3,NULL));
           cheat_assert(MSTRO_OK==mstro_symtab_intern(&tab,s3,&sym3));
           cheat_assert(MSTRO_OK==mstro_symtab_intern(&tab,s4,&sym4));
           cheat_yield();

           /* illegal to add permanent version of existing non-permanent one */
           cheat_assert(MSTRO_INVARG==mstro_symtab_intern_permanent(&tab,s4,NULL));
           cheat_yield();
           /* illegal to add permanent version with different address of existing permanent one */
           cheat_assert(MSTRO_INVARG==mstro_symtab_intern_permanent(&tab,s5,NULL));
           cheat_yield();



           
           mstro_symbol sym1b, sym2b, sym3b, sym4b, other;
           cheat_assert(MSTRO_OK==mstro_symtab_lookup(tab,s3,&sym3b));
           cheat_assert(sym3b==sym3);
           cheat_yield();
           cheat_assert(MSTRO_OK==mstro_symtab_lookup(tab,"nonexistant",&other));
           cheat_assert(other==NULL);
           cheat_yield();

           cheat_assert(MSTRO_OK==mstro_symtab_destroy(&tab));
           cheat_assert(MSTRO_OK == mstro_finalize());
           )
           
