/* -*- mode:c -*- */
/** @file
 ** @brief check corner cases to improve coverage reports
 **/

/*
 * Copyright (C) 2020 HPE, HP Schweiz GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_cdo.h"
#include <stdint.h>
#include "maestro/i_uuid_ui64.h"
#include "maestro/i_uuid_ui128.h"
#include "maestro/i_base64.h"

CHEAT_TEST(base64,
           {
             size_t dummy;
             const unsigned char *str = (const unsigned char*)"foo";
             cheat_assert(NULL==base64_encode(str, SIZE_MAX,&dummy));
             /* hopefully no one has that much memory... */
             cheat_assert(NULL==base64_encode(str, SIZE_MAX/3 ,&dummy));
           })


    CHEAT_TEST(base64decode,
               {
                 size_t dummy;
                 cheat_assert(NULL==base64_decode(NULL, 0,&dummy));
                 cheat_assert(NULL==base64_decode((const unsigned char*)"===", 3, &dummy));
               })


CHEAT_TEST(null_cdo, {
		cheat_assert(MSTRO_CDO_STATE_INVALID==mstro_cdo_state_get(NULL)) ;
		cheat_assert(NULL==mstro_cdo_name(NULL));
		})

CHEAT_TEST(invalid_cdo_declare, {
	mstro_cdo res;
	cheat_assert(MSTRO_OK == mstro_init("Tests","Init",0));
	cheat_assert(MSTRO_INVARG==mstro_cdo_declare(NULL, MSTRO_ATTR_DEFAULT, &res));
	cheat_assert(MSTRO_INVOUT==mstro_cdo_declare("foo", MSTRO_ATTR_DEFAULT, NULL));

	cheat_assert(NULL==MSTRO_ATTR_DEFAULT);
	mstro_cdo_attributes really_invalid_attr=(mstro_cdo_attributes)(1);
	cheat_assert(MSTRO_UNIMPL==mstro_cdo_declare("foo", really_invalid_attr, &res));
	cheat_assert(MSTRO_OK == mstro_finalize());
	})

CHEAT_TEST(invalid_cdo_seal, {
	cheat_assert(MSTRO_UNINIT==mstro_cdo_declaration_seal(NULL));
			})

CHEAT_TEST(invalid_cdo_offer, {
	cheat_assert(MSTRO_UNINIT==mstro_cdo_offer(NULL));
})

CHEAT_TEST(invalid_cdo_withdraw, {
	cheat_assert(MSTRO_UNINIT==mstro_cdo_withdraw(NULL));
})

CHEAT_TEST(invalid_cdo_dispose, {
	cheat_assert(MSTRO_UNINIT==mstro_cdo_dispose(NULL));
})

CHEAT_TEST(math64,
        ui64_t zero,max, value_64, tmp_64;
        unsigned long value_ul = 50505050;
        unsigned long value_ul2;
        char * str;
        zero = ui64_zero();
        max = ui64_max();
        value_64 = ui64_n2i(value_ul);
        value_ul2 = ui64_i2n(value_64);
        value_64 = ui64_s2i("01010101010101", NULL, 2);
        cheat_assert( NULL == ui64_i2s(value_64, NULL, 0 , 2));

        tmp_64 = ui64_add(max, zero, NULL);
        cheat_assert(0 == ui64_cmp(max,tmp_64));

        tmp_64 = ui64_sub(max, zero, NULL);
        cheat_assert(0 == ui64_cmp(max,tmp_64));

        tmp_64 = ui64_addn(zero, 1, NULL);

        tmp_64 = ui64_subn(tmp_64, 1, NULL);

        cheat_assert(0 == ui64_cmp(tmp_64,zero));

        tmp_64 = ui64_mul(max, zero, NULL);

        cheat_assert(0 == ui64_cmp(tmp_64,zero));

        tmp_64 = ui64_muln(max,1, NULL);

        cheat_assert(0 == ui64_cmp(tmp_64,max));

        tmp_64 = ui64_div(max,value_64, NULL);

        cheat_assert(0 != ui64_cmp(max,tmp_64));

        tmp_64 = ui64_divn(value_64, 1, NULL);

        cheat_assert(0 == ui64_cmp(value_64,tmp_64));

        tmp_64 = ui64_rol(value_64,1, NULL);

        tmp_64 = ui64_ror(tmp_64,1, NULL);

        cheat_assert(0 == ui64_cmp(value_64,tmp_64));

        cheat_assert(1 < ui64_len(max));

  )

  CHEAT_TEST(math128,
          ui128_t zero,max, value_128, tmp_128;
          unsigned long value_ul = 50505050;
          unsigned long value_ul2;
          char * str;
          zero = ui128_zero();
          max = ui128_max();
          value_128 = ui128_n2i(value_ul);
          value_ul2 = ui128_i2n(value_128);
          value_128 = ui128_s2i("01010101010101", NULL, 2);
          cheat_assert( NULL == ui128_i2s(value_128, NULL, 0 , 2));

          tmp_128 = ui128_add(max, zero, NULL);
          cheat_assert(0 == ui128_cmp(max,tmp_128));

          tmp_128 = ui128_sub(max, zero, NULL);
          cheat_assert(0 == ui128_cmp(max,tmp_128));

          tmp_128 = ui128_addn(zero, 1, NULL);

          tmp_128 = ui128_subn(tmp_128, 1, NULL);

          cheat_assert(0 == ui128_cmp(tmp_128,zero));

          tmp_128 = ui128_mul(max, zero, NULL);

          cheat_assert(0 == ui128_cmp(tmp_128,zero));

          tmp_128 = ui128_muln(max,1, NULL);

          cheat_assert(0 == ui128_cmp(tmp_128,max));

          tmp_128 = ui128_div(max,value_128, NULL);

          cheat_assert(0 != ui128_cmp(max,tmp_128));

          tmp_128 = ui128_divn(value_128, 1, NULL);

          cheat_assert(0 == ui128_cmp(value_128,tmp_128));

          tmp_128 = ui128_rol(value_128,1, NULL);

          tmp_128 = ui128_ror(tmp_128,1, NULL);

          cheat_assert(0 == ui128_cmp(value_128,tmp_128));

          cheat_assert(1 < ui128_len(max));

    )
