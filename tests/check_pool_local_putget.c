/* -*- mode:c -*- */
/** @file
 ** @brief Status code implementation of maestro library

 * check CDO provide/withdraw of a set of CDOs that have all been
 * REQUIRED before from multiple threads
 *
 * Design is a bit intricate:
 * K producer threads and L consumer threads start up.
 
 * * The producers post REQUIRE/DEMAND for 'consumer i ready' CDOs.
 
 * * Consumer i posts a bunch of CDO REQUIRE requests, then a PROVIDE
     for 'consumer i ready'. Then it iterates DEMAND ops for its CDOs.

 * * Producers start producing (all the same) CDOs for the
     consumers. Then they withdraw all of them
 *
 * This is supposed to show that CDOs can be used for synchronization
 * and that equivalent CDOs produced by multiple producers can be
 * picked up by consumers.
 **/

 /*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "maestro/i_statistics.h"
#include "maestro/logging.h"
#include <string.h>
#include <pthread.h>
#include <inttypes.h>

#define CDO_NAME_MAX 64
#define CONSUMER_READY_STRING "Consumer ready"
#define WORK_ITEM_STRING      "Work item"

CHEAT_DECLARE(
    /* everyone will OR in their value; need to do atomic access */
    pthread_mutex_t global_result_mtx = PTHREAD_MUTEX_INITIALIZER;
    mstro_status global_result = MSTRO_OK;
    
    struct thread_args {
      size_t consumer_id;
      size_t producer_id;
      size_t num_producers;
      size_t num_consumers;
      size_t num_repeats;
      size_t num_different_cdos;
    };
    
    
    void
    create_name(char *dst, const char *base, size_t idx)
    {
      snprintf(dst, CDO_NAME_MAX, "%s %zu", base, idx);
    }
    
    /* intended to be run in a thread */
    void*
    run_provider_thread(void *closure) {
      struct thread_args *args = (struct thread_args *)closure;
      mstro_status s = MSTRO_OK;


      LOG_INFO(MSTRO_LOG_MODULE_USER,
               "Producer %" PRIuPTR " started\n",
               (uintptr_t)pthread_self());

      for(size_t i=0; i< args->num_repeats; i++) {
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "Producer %zu iteration %zu starts\n", args->producer_id, i);
        
        /* wait for consumers to be ready */
        mstro_cdo tokens[args->num_consumers];
        for(size_t i=0; i<args->num_consumers; i++) {
          mstro_status s1,s2;
          char name[CDO_NAME_MAX];
          create_name(name, CONSUMER_READY_STRING, i);
          s1= mstro_cdo_declare(name,
                                MSTRO_ATTR_DEFAULT, & (tokens[i]));
          s2= mstro_cdo_require(tokens[i]);
          
          s = s | s1 | s2 ;
        }
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "%zu consumer ready tokens REQUIREd\n",
                 args->num_consumers);
        
        for(size_t i=0; i<args->num_consumers; i++) {
          mstro_status s1,s2;
          s1= mstro_cdo_demand(tokens[i]);
          s2= mstro_cdo_dispose(tokens[i]);
          s = s | s1 | s2 ;
        }
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "%zu consumer ready tokens DEMANDed\n",
                 args->num_consumers);

        /* Ok, produce stuff */
        
        mstro_cdo cdos[args->num_different_cdos];
        
        for(size_t cdoidx=0; cdoidx < args->num_different_cdos; cdoidx++) {
          char name[CDO_NAME_MAX];
          create_name(name, WORK_ITEM_STRING, cdoidx);
          
          mstro_status s1,s2;
          
          s1= mstro_cdo_declare(name,
                                MSTRO_ATTR_DEFAULT, & (cdos[cdoidx]));
          s2= mstro_cdo_offer(cdos[cdoidx]);
          
          s = s | s1 | s2 ;
        }
        
        LOG_INFO(MSTRO_LOG_MODULE_USER, "Work items PROVIDed\n");
        
        for(size_t cdoidx=args->num_different_cdos;
            cdoidx-->0; /* nothing */) {
          mstro_status s3,s4;
          
          s3 = mstro_cdo_withdraw(cdos[cdoidx]);
          s4 = mstro_cdo_dispose(cdos[cdoidx]);
          s = s | s3 | s4 ;
        }

        LOG_INFO(MSTRO_LOG_MODULE_USER, "Work items WITHDRAWn\n");

      }

      
      pthread_mutex_lock(&global_result_mtx);
      global_result |= s;
      pthread_mutex_unlock(&global_result_mtx);
      LOG_INFO(MSTRO_LOG_MODULE_USER, "Producer done, status %d\n", s);
      return NULL;
    }

    /* intended to be run in a thread */
    void*
    run_consumer_thread(void *closure) {
      struct thread_args *args = (struct thread_args *)closure;
      mstro_status s = MSTRO_OK;

      LOG_INFO(MSTRO_LOG_MODULE_USER,
               "Consumer %zu (%" PRIuPTR ") starting\n",
               args->consumer_id, (uintptr_t)pthread_self());
      
      /* post work */
      mstro_cdo cdos[args->num_different_cdos];
      for(size_t i=0; i< args->num_repeats; i++) {
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "Consumer %zu: iteration %zu starts\n", args->consumer_id, i);

        for(size_t cdoidx=0; cdoidx < args->num_different_cdos; cdoidx++) {
          char name[CDO_NAME_MAX];
          create_name(name, WORK_ITEM_STRING, cdoidx);
          
          mstro_status s1,s2;

          s1= mstro_cdo_declare(name,
                                MSTRO_ATTR_DEFAULT, & (cdos[cdoidx]));
          s2= mstro_cdo_require(cdos[cdoidx]);
          
          s = s | s1 | s2 ;
        }
        
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "Consumer %zu REQUIREs posted\n", args->consumer_id);
        
        
        /* let producers know I'm here */
        mstro_cdo token;
        char name[CDO_NAME_MAX];
        create_name(name, CONSUMER_READY_STRING, args->consumer_id);
        {
          mstro_status s1,s2;
          
          s1= mstro_cdo_declare(name,
                                MSTRO_ATTR_DEFAULT, &token);
          s2= mstro_cdo_offer(token);
          
          s = s | s1 | s2 ;
        }
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "Consumer %zu token OFFERED\n", args->consumer_id);
        
        /* wait for results to come in */
        for(size_t cdoidx=args->num_different_cdos;
            cdoidx-->0; /* nothing */) {
          mstro_status s3,s4;
          
          s3= mstro_cdo_demand(cdos[cdoidx]);
          s4= mstro_cdo_dispose(cdos[cdoidx]);
          s = s | s3 | s4 ;
        }
        fprintf(stdout, "Consumer %zu data DEMANDEd successfully\n",
                args->consumer_id);
        
        /* then withdraw our ready token */
        {
          mstro_status s1,s2;
          
          s1= mstro_cdo_withdraw(token);
          s2= mstro_cdo_dispose(token);
          
          s = s | s1 | s2 ;
        }
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "Consumer %zu ready token withdrawn\n", args->consumer_id);
        LOG_INFO(MSTRO_LOG_MODULE_USER,
                 "Consumer %zu: iteration %zu ends\n", args->consumer_id, i);
      }
      pthread_mutex_lock(&global_result_mtx);
      global_result |= s;
      pthread_mutex_unlock(&global_result_mtx);

      LOG_INFO(MSTRO_LOG_MODULE_USER,
               "Consumer %zu done, status %d\n", args->consumer_id, s);

      return NULL;
    }
    
              )

#define MAX_PROD_THREADS 1
#define MAX_CONS_THREADS 1


CHEAT_TEST(cdo_pool_ops_work,
           /* save print time: */
	   //setenv("MSTRO_LOG_LEVEL","0", 1);

           cheat_assert(MSTRO_OK == mstro_init("Tests","POOL",0));

           struct thread_args prodthread_args[MAX_PROD_THREADS];
           prodthread_args[0].num_producers = MAX_PROD_THREADS;
           prodthread_args[0].num_consumers = MAX_CONS_THREADS;
           prodthread_args[0].num_repeats = 1;
           prodthread_args[0].num_different_cdos = 20;
           
           struct thread_args consthread_args[MAX_CONS_THREADS];
           pthread_t prodthreads[MAX_PROD_THREADS];
           pthread_t consthreads[MAX_CONS_THREADS];
           
           mstro_nanosec_t before = mstro_clock();

           for(size_t prodidx=0; prodidx<MAX_PROD_THREADS; prodidx++) {
             prodthread_args[prodidx] = prodthread_args[0];
             prodthread_args[prodidx].producer_id = prodidx;

             pthread_create(& (prodthreads[prodidx]),
                            NULL,
                            run_provider_thread,
                            &prodthread_args[prodidx]);
           }
           LOG_INFO(MSTRO_LOG_MODULE_USER,
                    "started producers\n");
                        
           for(size_t considx=0; considx<MAX_CONS_THREADS; considx++) {
             consthread_args[considx] = prodthread_args[0];
             consthread_args[considx].consumer_id = considx;
             pthread_create(& (consthreads[considx]),
                            NULL,
                            run_consumer_thread,
                            &consthread_args[considx]);
           }
           LOG_INFO(MSTRO_LOG_MODULE_USER,
                    "started consumers\n");

           for(size_t i=0; i<MAX_PROD_THREADS; i++) {
             void *res;
             pthread_join(prodthreads[i], &res);
           }
           LOG_INFO(MSTRO_LOG_MODULE_USER,
                    "joined producer\n");

           for(size_t i=0; i<MAX_CONS_THREADS; i++) {
             void *res;
             pthread_join(consthreads[i], &res);
           }
           LOG_INFO(MSTRO_LOG_MODULE_USER,
                    "joined consumers\n");
           mstro_nanosec_t after = mstro_clock();
           double duration = after-before;
           double ms = duration/(1000.0*1000.0);
           LOG_INFO(MSTRO_LOG_MODULE_USER,
                    "%zu producers, %zu consumers: %zu reps, %zu different took %.2f ms\n",
                    MAX_PROD_THREADS,
                    MAX_CONS_THREADS,
                    prodthread_args[0].num_repeats,
                    prodthread_args[0].num_different_cdos,
                    ms);
           
           cheat_assert(global_result==MSTRO_OK);

           cheat_assert(MSTRO_OK == mstro_finalize());
           )

