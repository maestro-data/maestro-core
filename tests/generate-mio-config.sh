#!/bin/sh
#
# Generate a MIO configuration for the node we're on


if test $# -ne 1; then
	echo "usage: $0 <mero-id>" >&2
	exit 1
fi

HOSTNAME=`hostname`
if test -f /etc/mero/conf.xc; then
	# fetch from file (mainly for single-host installation)
	MERO_HA_IP=`grep 'M0_CST_HA' /etc/mero/conf.xc |sed -s 's/^.*@M0_CST_HA.* "\([^"]*\)".*$/\1/'`
	CLOVIS_IP=`grep 'M0_CST_CONFD' /etc/mero/conf.xc |sed -s 's/^.*@M0_CST_CONFD.* "\([^"]*\)".*$/\1/'|sed -s "s/\(.*\):1$/\1:$1/"`
else
	# find the full address of the CST_HA service on the host we're running on
	MERO_HA_IP=`hctl mero status | grep -2 CST_HA|grep -2 "$HOSTNAME" |head -2|tail -1|sed -e 's/^.*\[[^]]\+] [^ ]\+:[^ ]\+ \+\([^ ]\+\).*$/\1/'`
	# find the part of the local clovis service address that we stick our mero-id onto
	CLOVIS_IP=`hctl mero status | grep -A10 "$HOSTNAME" |grep -A1 CST_RMS|grep -v CST_RMS|head -1|sed -e 's/^.*\[[^]]\+] [^ ]\+:[^ ]\+ \+\([^ ]\+\).*$/\1/' | sed -e 's/\(.*\):[0-9]\+ $/\1:$1/'`
fi


cat >mio-config.yaml <<EOF
# auto-generated MIO config file for $HOSTNAME 

#MIO_Config_Sections: [MIO_CONFIG, MERO_CONFIG]
MIO_CONFIG:
  MIO_LOG_FILE:
  MIO_LOG_LEVEL: MIO_DEBUG 
  MIO_DRIVER: MERO

MERO_CONFIG:
  MERO_CLOVIS_INST_ADDR: $CLOVIS_IP
  MERO_HA_ADDR: $MERO_HA_IP
  MERO_PROFILE: <0x7000000000000001:0>
  MERO_PROCESS_FID: <0x7200000000000000:0>
  MERO_DEFAULT_UNIT_SIZE: 1048576
  MERO_IS_OOSTORE: 1
  MERO_IS_READ_VERIFY: 0
  MERO_TM_RECV_QUEUE_MIN_LEN: 2
  MERO_MAX_RPC_MSG_SIZE: 131072

EOF

