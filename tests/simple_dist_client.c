/* -*- mode:c -*- */
/** @file
 ** @brief Connect to pool manager and offer/demand distributed CDOs
 **/

/*
 * Copyright (C) 2018-2020 Cray Computer GmbH
 * Copyright (C) 2021 HPE Switzerland GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "mamba.h"
#include "maestro.h"
#include "maestro/i_utlist.h"
#include "maestro/i_uthash.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef COMPONENT_ID
#error You need to define COMPONENT_ID when compiling this file
#endif




#define XSTRINGIFY(s) #s
#define STRINGIFY(s) XSTRINGIFY(s)


CHEAT_DECLARE(
void fill_data(double *data, mmbLayout *layout) {
   
   size_t data_count = layout->irregular.lengths[layout->index]; 
   size_t offset = layout->irregular.offsets[layout->index];
   for (size_t i = 0; i < data_count; i++) {
      data[i] = i + offset;
   }
   
}

void print_data(double *data, mmbLayout *layout) {
   size_t data_count = layout->irregular.lengths[layout->index]; 
   printf("data length = %zu \n ", data_count);
   for (size_t i = 0; i < data_count; i++) {
      printf("%lf ", data[i]);
      if(i%10 == 0) {
         printf("\n");
      }
   }
   printf("\n--------------------------\n");
   
}
)

CHEAT_TEST(simple_dist_cdo,

           char *component_id = STRINGIFY(COMPONENT_ID);
           int my_id = atoi(component_id);
           mmbLayoutEquivalence diff;

           size_t *offsets = malloc(sizeof(size_t) *2);
           size_t *sizes = malloc(sizeof(size_t) *2);

           size_t *offsets2 = malloc(sizeof(size_t) *2);
           size_t *sizes2 = malloc(sizeof(size_t) *2);

           offsets[0] = 0;
           offsets[1] = 100;
           sizes[0] = 100;
           sizes[1] = 50;

           offsets2[0] = 0;
           offsets2[1] = 75;
           sizes2[0] = 75;
           sizes2[1] = 75;

           size_t *offsets3 = malloc(sizeof(size_t) *3);
           size_t *sizes3 = malloc(sizeof(size_t) *3);
           
           offsets3[0] = 0;
           offsets3[1] = 50;
           offsets3[2] = 100;
           sizes3[0] = 50;
           sizes3[1] = 50;
           sizes3[2] = 50;

           char *sync_buf;
           int64_t sync_size = 1;
           double *data;

           double *dist_buf=NULL, *dist_buf2=NULL, *dist_buf3=NULL;
           size_t element_size = sizeof(double);
           size_t nblocks = 2;
           int64_t dist_size = 50 * element_size;
           int64_t dist_size2 = 75 * element_size;
           int64_t dist_size3 = 75 * element_size;
           //
           mmbLayout *dist_layout1, *dist_layout2, *dist_layout3, *dist_layout4;
           cheat_assert(MMB_OK == mmb_layout_create_dist_irregular_1d(element_size,
                                                1,
                                                nblocks,
                                                offsets,
                                                sizes,
                                                &dist_layout1));


           cheat_assert(MMB_OK == mmb_layout_create_dist_irregular_1d(element_size,
                                                1,
                                                nblocks,
                                                offsets2,
                                                sizes2,
                                                &dist_layout2));

           cheat_assert(MMB_OK == mmb_layout_create_dist_irregular_1d(element_size,
                                                        0,
                                                        nblocks,
                                                        offsets2,
                                                        sizes2,
                                                        &dist_layout3));


           cheat_assert(MMB_OK == mmb_layout_cmp(dist_layout1, dist_layout3, &diff));

           cheat_assert(MMB_LAYOUT_DIFF_FIELDS == diff);
           cheat_assert(MSTRO_OK == mstro_init(NULL,NULL,my_id));
           cheat_yield();

           mstro_cdo sync_cdo, dist_cdo, dist_cdo3, dist_cdo2, dist_cdo4;
           char sync_cdo_name[20];
           char dist_cdo_name[20];
           
           size_t my_index;

           switch (my_id) {
             case 1:

             my_index = my_id - 1;
             cheat_assert(0 == posix_memalign((void**)&dist_buf, (size_t) sysconf(_SC_PAGESIZE), dist_size));
             cheat_assert(0 == posix_memalign((void**)&dist_buf2, (size_t) sysconf(_SC_PAGESIZE), dist_size2));
             cheat_assert(0 == posix_memalign((void**)&dist_buf3, (size_t) sysconf(_SC_PAGESIZE), dist_size3));

             fill_data(dist_buf, dist_layout1); 
             fill_data(dist_buf2, dist_layout2);
             fill_data(dist_buf3, dist_layout3);
             print_data(dist_buf2,dist_layout2);
             print_data(dist_buf3,dist_layout3);
             sprintf(dist_cdo_name, "dist-cdo-%d", my_id);
             
             //cheat_assert(MSTRO_OK == mstro_cdo_declare(dist_cdo_name, MSTRO_ATTR_DEFAULT, &dist_cdo));
             cheat_assert(MSTRO_OK == mstro_cdo_declare(dist_cdo_name, MSTRO_ATTR_DEFAULT, &dist_cdo2));
             cheat_assert(MSTRO_OK == mstro_cdo_declare(dist_cdo_name, MSTRO_ATTR_DEFAULT, &dist_cdo3));
             //cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo, MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, dist_layout1, true, false));
             cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo2, MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, dist_layout2, true, false));
             cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo3, MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, dist_layout3, true, false));
             /*add distributed cdo layout*/
             //cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, &dist_size, true, false));
             //cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo, MSTRO_ATTR_CORE_CDO_RAW_PTR, dist_buf, false, true));
             cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo2, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, &dist_size2, true, false));
             cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo2, MSTRO_ATTR_CORE_CDO_RAW_PTR, dist_buf2, false, true));
             cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo3, MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE, &dist_size3, true, false));
             cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo3, MSTRO_ATTR_CORE_CDO_RAW_PTR, dist_buf3, false, true));

             //cheat_assert(MSTRO_OK == mstro_cdo_seal(dist_cdo));
             cheat_assert(MSTRO_OK == mstro_cdo_seal(dist_cdo2));
             cheat_assert(MSTRO_OK == mstro_cdo_seal(dist_cdo3));
             // Syncing with the consumer
             sprintf(sync_cdo_name, "sync-cdo-%d", my_id);
             cheat_assert(MSTRO_OK == mstro_cdo_declare(sync_cdo_name, MSTRO_ATTR_DEFAULT, &sync_cdo));
             cheat_assert(MSTRO_OK == mstro_cdo_require(sync_cdo));

             //cheat_assert(MSTRO_OK == mstro_cdo_offer(dist_cdo));
             cheat_assert(MSTRO_OK == mstro_cdo_offer(dist_cdo2));
             cheat_assert(MSTRO_OK == mstro_cdo_offer(dist_cdo3));
             
             cheat_assert(MSTRO_OK == mstro_cdo_demand(sync_cdo));

             cheat_yield();
             //cheat_assert(MSTRO_OK == mstro_cdo_withdraw(dist_cdo));
             cheat_assert(MSTRO_OK == mstro_cdo_withdraw(dist_cdo2));
             cheat_assert(MSTRO_OK == mstro_cdo_withdraw(dist_cdo3));
             

             // Clean up
             cheat_assert(MSTRO_OK == mstro_cdo_dispose(sync_cdo));
             
             //cheat_assert(MSTRO_OK == mstro_cdo_dispose(dist_cdo));
             cheat_assert(MSTRO_OK == mstro_cdo_dispose(dist_cdo2));
             cheat_assert(MSTRO_OK == mstro_cdo_dispose(dist_cdo3));

	     free(dist_buf);
	     free(dist_buf2);
	     free(dist_buf3);

             break;

            case 2:
               
              mmb_layout_create_dist_irregular_1d(sizeof(double),
                                                 1,
                                                 3,
                                                 offsets3,
                                                 sizes3,
                                                 &dist_layout4);
              
              sprintf(sync_cdo_name, "sync-cdo-%d", my_id-1);
              cheat_assert(MSTRO_OK == mstro_cdo_declare(sync_cdo_name, MSTRO_ATTR_DEFAULT, &sync_cdo));
              
              sprintf(dist_cdo_name, "dist-cdo-%d", my_id-1);
              cheat_assert(MSTRO_OK == mstro_cdo_declare(dist_cdo_name, MSTRO_ATTR_DEFAULT, &dist_cdo));
              cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo, MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, dist_layout4, true, false));
             
	      
	      cheat_assert(MSTRO_OK == mstro_cdo_declare(dist_cdo_name, MSTRO_ATTR_DEFAULT, &dist_cdo4));
              cheat_assert(MSTRO_OK==mstro_cdo_attribute_set(dist_cdo4, MSTRO_ATTR_CORE_CDO_DIST_LAYOUT, dist_layout1, true, false));

              cheat_assert(MSTRO_OK == mstro_cdo_seal(dist_cdo));
              cheat_assert(MSTRO_OK == mstro_cdo_require(dist_cdo));
              cheat_assert(MSTRO_OK == mstro_cdo_seal(dist_cdo4));
              cheat_assert(MSTRO_OK == mstro_cdo_require(dist_cdo4));

              cheat_assert(MSTRO_OK == mstro_cdo_offer(sync_cdo));
              cheat_assert(MSTRO_OK == mstro_cdo_demand(dist_cdo));
              cheat_assert(MSTRO_OK == mstro_cdo_demand(dist_cdo4));

              cheat_assert(MSTRO_OK == mstro_cdo_withdraw(sync_cdo));
              cheat_yield();
               
              cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(dist_cdo, MSTRO_ATTR_CORE_CDO_RAW_PTR, NULL,(const void **) &data));
              print_data(data,dist_layout4);
	      cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(dist_cdo4, MSTRO_ATTR_CORE_CDO_RAW_PTR, NULL,(const void **) &data));
              print_data(data,dist_layout1);

              // Clean up
              cheat_assert(MSTRO_OK == mstro_cdo_dispose(sync_cdo));
              cheat_assert(MSTRO_OK == mstro_cdo_dispose(dist_cdo));
	      cheat_assert(MSTRO_OK == mstro_cdo_dispose(dist_cdo4));
              cheat_assert(MMB_OK == mmb_layout_destroy(dist_layout4));
              
              cheat_yield();
              break;
           }

           cheat_assert(MSTRO_OK == mstro_finalize());
           cheat_assert(MMB_OK == mmb_layout_destroy(dist_layout1));
           cheat_assert(MMB_OK == mmb_layout_destroy(dist_layout2));
           cheat_assert(MMB_OK == mmb_layout_destroy(dist_layout3));

           free(sizes);
           free(sizes2);
           free(sizes3);
           free(offsets);
           free(offsets2);
           free(offsets3);
           )
