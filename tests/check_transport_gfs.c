/* -*- mode:c -*- */
/** @file
 ** @brief check transport GFS
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "../transport/transport.h"
#include "../transport/transport_gfs.h"
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>
#include "maestro/logging.h"

CHEAT_DECLARE (
    unsigned char
    transport_checksum(const char* name, void* rawptr, size_t size)
    {
      unsigned char x;
      size_t i;
      
      for (i=0,x=0;i<size; i++) 
        x ^= ((unsigned char*)rawptr)[i]; 
      
      fprintf(stderr, "Checksum for cdo \"%s\":\t%d (%p)\n", name, x, rawptr);
	  return x;
    }
               )
    
    /* this tests GFS transport by using this single thread as both Producer and Consumer  */
CHEAT_TEST(transport_gfs_works,
  unsigned char chksum_dep, chksum_arr;
  size_t data_count = 1031;
  int64_t bytes = data_count*sizeof(double);
  double* src_data = malloc(bytes);
  double* dst_data = malloc(bytes);
  for(size_t i=0; i<data_count; i++) {
    src_data[i]=random()%10;
  }
  bzero(dst_data,bytes);
  chksum_dep = transport_checksum("pioneer departure", src_data, bytes);
  
  cheat_assert(MSTRO_OK == mstro_init("Tests","TRANSPORT",0));
  char name[] = "transport_pioneer";
  char filename_dst[] = "./CDOs/__CDO_gfs_transport_pioneer";
  mstro_cdo cdo_src=NULL;
  mstro_cdo cdo_dst=NULL;
  int s = mkdir("./CDOs", S_IRWXU);
  cheat_assert(s==0 || errno==EEXIST);
  
  cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                   src_data, false, true));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                   (void**)&bytes, true, false));
  cheat_yield();
  
  enum mstro_cdo_attr_value_type ttype;
  const int64_t* tval=NULL;
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                   &ttype,
                                                   (const void**)&tval));
  cheat_assert(*tval==bytes);
  cheat_assert(ttype==MSTRO_CDO_ATTR_VALUE_int64);
  cheat_yield();
  
  cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_src,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                   &ttype,
                                                   (const void**)&tval));
  cheat_assert(*tval==bytes);
  cheat_assert(ttype==MSTRO_CDO_ATTR_VALUE_int64);
  cheat_yield();
                 
  cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_dst));
  
  /* Particular case where the offerer is also the src, so let us execute
   * the src transport before offering, otherwise the raw-ptr won't be
   * accessible anymore */
  
  /* Manually issue a ticket for src */
  //Mstro__Pool__UUID id = MSTRO__POOL__UUID__INIT; // unused 
  //Mstro__Pool__TransferTicket__TicketCase method;
  Mstro__Pool__TransferTicket ticket = MSTRO__POOL__TRANSFER_TICKET__INIT;
  Mstro__Pool__TransferTicketGFS gfs = MSTRO__POOL__TRANSFER_TICKET_GFS__INIT;
  gfs.path = filename_dst;
  gfs.keep_file = 0;
  ticket.ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_GFS;
  ticket.gfs = &gfs;
  ticket.data_size = bytes;
  
  cheat_assert(MSTRO_OK == mstro_transport_gfs_src_execute(cdo_src, &ticket)); 
  
  cheat_assert(MSTRO_OK == mstro_cdo_offer(cdo_src));

                 
  /* ticket is supposed to be sent to dst (4-step protocol), so here let
   * us just pretend it was, and use ticket directly for dst transport
   * execute, while it is being implemented */


  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_dst,
                                                   MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                   (void**)&bytes, true, false));
  /* this file is an old test from a time with a much simpler transport layer
 * -- especially, we have the adjust space function is in mstro_transport_execute,
 * which we don't use for the purpose of this test. In short: let's have a dst
 * buffer ready */
  cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_dst,
                                                   MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                   dst_data, false, true));

  cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_dst));

  cheat_assert(MSTRO_OK == mstro_transport_gfs_dst_execute(cdo_dst, &ticket)); 

  cheat_assert(MSTRO_OK == mstro_cdo_require(cdo_dst));
  cheat_assert(MSTRO_OK == mstro_cdo_demand(cdo_dst));
  
  double* data;
  int64_t len;
  const int64_t* val=NULL;
  
//  cheat_assert(MSTRO_OK == mstro_cdo_access_ptr(cdo_dst, (void**)&data, NULL));
  mmbArray* m_dst;
  cheat_assert(MSTRO_OK == mstro_cdo_access_mamba_array(cdo_dst, &m_dst));
  data = m_dst->allocation->ptr;

  cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_dst, "scope.local-size",
                                                   &ttype, (const void**)&val));
  cheat_assert(ttype==MSTRO_CDO_ATTR_VALUE_int64);
  cheat_yield();
  
  LOG_DEBUG(MSTRO_LOG_MODULE_USER,"val %p, *val %" PRIi64 "\n", val, *val);
  len = *val;
/*  len /= sizeof(double);
  LOG_DEBUG(MSTRO_LOG_MODULE_USER, "val %p, *val %" PRIi64 ", len %" PRIi64 " \n",
            val, *val, len);
 */ 
  chksum_arr = transport_checksum("pioneer arrival", data, len);
  
  cheat_assert(chksum_dep == chksum_arr);
  
  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_dst));
  cheat_assert(MSTRO_OK == mstro_cdo_withdraw(cdo_src));
  cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_src));
  cheat_assert(MSTRO_OK == mstro_finalize());
  
  free(src_data);
  free(dst_data);
  )



