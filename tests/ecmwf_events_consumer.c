#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "maestro.h"
#include <stdint.h>

#include "ecmwf_config.h"

int main(int argc, char* argv[])
{
    mstro_status s;
    argc=argc, argv=argv; /* avoid unused arg warning */

    mstro_cdo_selector selector = NULL;
    mstro_subscription subscription = NULL;

    s=mstro_init(getenv("MSTRO_WORKFLOW_NAME"), getenv("MSTRO_COMPONENT_NAME"), 0);
	assert(s==MSTRO_OK);
    selector = NULL;
    s=mstro_cdo_selector_create(NULL, NULL, "(has .maestro.core.cdo.name)", &selector);
	assert(s==MSTRO_OK);

    mstro_subscription offer_subscription;
    s=mstro_subscribe(selector, MSTRO_POOL_EVENT_OFFER,
        MSTRO_SUBSCRIPTION_OPTS_SIGNAL_BEFORE|MSTRO_SUBSCRIPTION_OPTS_REQUIRE_ACK,
        &offer_subscription);
	assert(s==MSTRO_OK);
    s=mstro_cdo_selector_dispose(selector);
	assert(s==MSTRO_OK);

    mstro_cdo cdos[number_of_cdos];

    // require all offered CDOs
    int count = 0;
    while (count < number_of_cdos) {
        mstro_pool_event event;
        s=mstro_subscription_poll(offer_subscription, &event);
		assert(s==MSTRO_OK);
        if (event != NULL) {
	    assert(event->kind==MSTRO_POOL_EVENT_OFFER);
            mstro_pool_event tmp = event;
            while (tmp != NULL) {
                printf("Offer occured for: %s\n", tmp->payload.offer.cdo_name);
                s=mstro_cdo_declare(tmp->payload.offer.cdo_name, MSTRO_ATTR_DEFAULT, &cdos[count]);
			assert(s==MSTRO_OK);
                s=mstro_cdo_require(cdos[count]);
			assert(s==MSTRO_OK);
                s=mstro_subscription_ack(offer_subscription, tmp);
			assert(s==MSTRO_OK);
                count++;
                tmp = tmp->next;
            }
	    mstro_pool_event_dispose(event);
        }
    }

    s=mstro_subscription_dispose(offer_subscription);
	assert(s==MSTRO_OK);

    // demand all CDOs
    // problem is that require did not block the producer to withdraw, so demand hangs
    for (int i = 0; i < number_of_cdos; i++) {
        s=mstro_cdo_demand(cdos[i]);
		assert(s==MSTRO_OK);
        s=mstro_cdo_dispose(cdos[i]);
		assert(s==MSTRO_OK);
    }

    s=mstro_finalize();
	assert(s==MSTRO_OK);

    return 0;
}
