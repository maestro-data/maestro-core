/* -*- mode:c -*- */
/** @file
 ** @brief check transport MIO
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include "../transport/transport.h"
#include "../transport/transport_mio.h"
#include "mio.h"
#define MSTRO_MIO_SEM_STR_MAXLEN 	128

#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>

CHEAT_DECLARE (
    unsigned char
    transport_checksum(const char* name, void* rawptr, uint64_t size)
    {
      unsigned char x;
      int i;

      for (i=0,x=0;i<size; i++)
        x ^= ((unsigned char*)rawptr)[i];

      fprintf(stderr, "Checksum for cdo \"%s\":\t%d\n", name, x);
      return x;
    }
               )

    /* this tests MIO transport by using this single thread as both Producer and Consumer  */
    CHEAT_TEST(transport_mio_works,
               size_t data_count = 1031;
               int64_t bytes = data_count*sizeof(double);
               size_t pad = 0;
               double src_data[data_count];
               for(size_t i=0; i<data_count; i++) {
                 src_data[i]=random();
               }
               unsigned char sender_checksum = transport_checksum("pioneer departure", src_data, data_count);

               cheat_assert(MSTRO_OK == mstro_init("Tests","TRANSPORT",0));
               char name[] = "transport_pioneer";
               char filename_dst[] = "./CDOs/__CDO_mio_transport_pioneer";
               mstro_cdo cdo_src=NULL;
               mstro_cdo cdo_dst=NULL;
               int s = mkdir("./CDOs", S_IRWXU);
               cheat_assert(s==0 || errno==EEXIST);

               cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_src));
               cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                                MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                                src_data, false));
               cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                                ".maestro.core.cdo.scope.local-size",
                                                                &bytes, true));
               cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(cdo_src,
                                                                ".maestro.core.cdo.layout.pre-pad",
                                                                &pad, true));

               const int64_t* tval;

               cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_src,
                                                                MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
								NULL,
                                                                (const void**)&tval));
               cheat_assert(*(const uint64_t*)tval==bytes);
               cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_src));
               cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_src,
                                                                MSTRO_ATTR_CORE_CDO_SCOPE_LOCAL_SIZE,
                                                                NULL,
                                                                (const void**)&tval));
               cheat_assert(*tval==bytes);

               cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo_dst));

               /* Particular case where the offerer is also the src, so let us execute
                * the src transport before offering, otherwise the raw-ptr won't be
                * accessible anymore */

               /* Have a ticket issued for src */
               Mstro__Pool__UUID id = MSTRO__POOL__UUID__INIT;
		size_t i;
		srand(time(NULL));
		char* str_cdoid = malloc(sizeof(struct mstro_cdo_id));

		/* Let us use a random CDOID for Mero storage, otherwise bugs
		 * arise when we reuse the same for some reason */

		for (i=0; i<sizeof(struct mstro_cdo_id); i++)
			str_cdoid[i]=random();
/*
		  id.qw0 = cdo_src->gid.qw[0];
		  id.qw1 = cdo_src->gid.qw[1];
*/
		struct mstro_cdo_id* cdoid = (struct mstro_cdo_id*)str_cdoid;
		id.qw0 = cdoid->qw[0];
		id.qw1 = cdoid->qw[1];
               id.local_id = cdoid->local_id;

		Mstro__Pool__TransferTicket ticket = MSTRO__POOL__TRANSFER_TICKET__INIT;
		ticket.cdoid = &id;

		mstro_status status;
	        const int64_t* val1;
	        cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_src, "scope.local-size", NULL, (const void**)&val1));
	        ticket.data_size = *val1;

		ticket.ticket_case = MSTRO__POOL__TRANSFER_TICKET__TICKET_MIO;
		Mstro__Pool__TransferTicketMIO mio = MSTRO__POOL__TRANSFER_TICKET_MIO__INIT;
		ticket.mio = &mio;
		struct mstro_cdo_id* semid;
		semid = malloc(sizeof(struct mio_obj_id));
		cheat_assert(semid != NULL);
		struct mstro_cdo_id* objid;
		objid = malloc(sizeof(struct mio_obj_id));
		cheat_assert (objid != NULL);
		char* semname = NULL;
		mstro_str_random(&semname, MSTRO_MIO_SEM_STR_MAXLEN);
		cheat_assert (semname != NULL);
    		status = mstro_cdo_id_from_name(semname, semid); /* So we do collision
							     detection in only
							     one place */
		cheat_assert (status == MSTRO_OK);
/*
	WITH_MIO_OBJ_STR(idstr, (struct mstro_cdo_id*)semid,
		INFO("Semaphore has ID: %s\n",
                        idstr););
	WITH_CDO_ID_STR(idstr, &(cdo_src->gid),
		INFO("(CDO associated has ID: %s)\n",
                        idstr););
*/
		cheat_assert(sizeof(struct mstro_cdo_id) == 2*sizeof(uint64_t));
		cheat_assert(sizeof(struct mstro_cdo_id) == sizeof(struct mio_obj_id));

		ticket.mio->semid.len = sizeof(struct mstro_cdo_id);
		ticket.mio->semid.data = (uint8_t*)semid;
		ticket.mio->objid.len = sizeof(struct mstro_cdo_id);
		objid->qw[0] = ticket.cdoid->qw0;
		objid->qw[1] = ticket.cdoid->qw1;
		ticket.mio->objid.data = (uint8_t*)objid;

		ticket.mio->keep_obj = 0;

	/* No clean issuing, have to duplicate code above from maestro/ofi.c*/
//		cheat_assert(MSTRO_OK == mstro_transport_ticket_issue(cdo_src, &ticket));

 		cheat_assert(MSTRO_OK == mstro_transport_mio_src_execute(cdo_src, &ticket));

		cheat_assert(MSTRO_OK == mstro_cdo_offer(cdo_src));
	       /* ticket is supposed to be sent to dst (4-step protocol), so here let
                * us just pretend it was, and use ticket directly for dst transport
                * execute, while it is being implemented */

		cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo_dst));
               cheat_assert(MSTRO_OK == mstro_transport_mio_dst_execute(cdo_dst, &ticket));

               cheat_assert(MSTRO_OK == mstro_cdo_require(cdo_dst));
               cheat_assert(MSTRO_OK == mstro_cdo_demand(cdo_dst));


               double* data;
               size_t len;
               enum mstro_cdo_attr_value_type type;
               const void* val;

               cheat_assert(MSTRO_OK == mstro_cdo_access_ptr(cdo_dst, (void**)&data, NULL));
               cheat_assert(MSTRO_OK == mstro_cdo_attribute_get(cdo_dst, "scope.local-size", &type, &val));
               len = *(size_t*)val;
               len /= sizeof(double);
	       cheat_assert(len > 0 && data != NULL);

               unsigned char receiver_checksum = transport_checksum("pioneer arrival", data, len);
               cheat_assert(sender_checksum == receiver_checksum);

               cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_dst));
               cheat_assert(MSTRO_OK == mstro_cdo_withdraw(cdo_src));
               cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo_src));
               cheat_assert(MSTRO_OK == mstro_finalize());

               )
