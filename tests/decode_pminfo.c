/* -*- mode:c -*- */
/** @file
 ** @brief Tool to decode serialized pool manager info
 **/

/*
 * Copyright (C) 2021 Hewlett Packard (Schweiz) GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "maestro.h"
#include "maestro/logging.h"
#include "maestro/env.h"

#include "protocols/maestro-endpoints.h"
#include "maestro/i_ofi.h"

#include <stdbool.h>
#include <stdatomic.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>


/* simplify logging */
#define DEBUG(...) LOG_DEBUG(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define INFO(...)  LOG_INFO(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define WARN(...)  LOG_WARN(MSTRO_LOG_MODULE_USER,__VA_ARGS__)
#define ERR(...)   LOG_ERR(MSTRO_LOG_MODULE_USER,__VA_ARGS__)



int
main(int argc, char ** argv)
{
  if(MSTRO_OK!=mstro_init("", "PMINFO decoder", 0))
    exit(EXIT_FAILURE);

  if(! (argc==1 || argc==2)) {
usage:
    fprintf(stderr, "Usage: %s [PM-INFO.txt]\n", argv[0]);
    exit(1);
  }

  char* pm_info=NULL;
  
  
  if(argc==1) {
    /* read from stdin */
#define BLOCKSIZE 1023
    size_t total_size=0;
    size_t pos=0;
    do {
	if(pos==total_size) {
	  total_size+=BLOCKSIZE;
	  pm_info=realloc(pm_info,total_size+1); // one extra so that there's always space for a NUL
	  if(pm_info==NULL) {
	    fprintf(stderr, "Failed to allocate buffer for input data\n");
	    exit(EXIT_FAILURE);
	  }
	}
    } while(!feof(stdin)
	    && (pm_info[pos++]=fgetc(stdin))!=EOF);
    pm_info[pos]='\0';
  } else {
    /* read from file */
    FILE *f = fopen (argv[1], "r");
    if(!f) {
      goto usage;
    }
    fseek (f, 0, SEEK_END);
    long length = ftell (f);
    fseek (f, 0, SEEK_SET);
    pm_info = malloc (length+1);
    if (!pm_info) {
      fprintf(stderr, "Failed to allocate buffer for file content of %s\n", argv[1]);
      exit(EXIT_FAILURE);
    }
    size_t items_read = fread (pm_info, length, 1, f);
    if(items_read!=1) {
      fprintf(stderr, "Failed to read PMINFO: %d (%s)\n",
              errno, strerror(errno));
      exit(EXIT_FAILURE);
    }
    pm_info[length] = '\0';
    fclose (f);
  }

  /* parse */
  Mstro__AppInfo *epd=NULL;
  mstro_status s=mstro_appinfo_deserialize(pm_info, &epd);
  if(pm_info)
    free(pm_info);

  if(s!=MSTRO_OK) 
    exit(EXIT_FAILURE);
    

  /* print */
  WITH_MSTRO_EPL_DESCRIPTION(str, epd->eps, {
      fprintf(stdout, "Decoded %s", str);
    });

  mstro__app_info__free_unpacked(epd, NULL);

  if(MSTRO_OK!=mstro_finalize())
    exit(EXIT_FAILURE);

  return EXIT_SUCCESS;
}
