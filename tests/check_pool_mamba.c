/* -*- mode:c -*- */
/** @file
 ** @brief simple mamba use through maestro check
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>
#include <mamba.h> // FIXME Should not have to include that

CHEAT_TEST(cdo_local_pool_mamba_works,
           cheat_assert(MSTRO_OK == mstro_init("Tests","POOL",0));
           char name[] = "Test CDO name üÜöÖäÄ";
           mstro_cdo cdo=NULL;
           cheat_assert(MSTRO_OK == mstro_cdo_declare(name, MSTRO_ATTR_DEFAULT, &cdo));
           int64_t size = 4096;
           enum mstro_cdo_attr_value_type type;
           float* buf;
           void *rec;
           buf = malloc(sizeof(float)*size);
           fprintf(stderr, "buf: %p\n", buf);
           rec = NULL;

// TODO declare/set a Mamba array
           mmbArray* ma_ptr = NULL;
// TODO associate (cdo, mmb_array_ptr)
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set(
                            cdo, ".maestro.core.cdo.raw-ptr", (void*)buf, false, true));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set(
                            cdo, ".maestro.core.cdo.scope.local-size", &size, true, false));

           mmbArray *mamba_ptr=NULL;
           /* mamba array not automatically available */
           mstro_status s1 = mstro_cdo_attribute_get(
               cdo, ".maestro.core.cdo.mamba-array", &type, (const void**)&mamba_ptr);

           fprintf(stderr,"Mamba array fetch stat 1: %d, val %p\n", s1, mamba_ptr);

           cheat_assert(MSTRO_OK==s1);
           cheat_assert(mamba_ptr==NULL);

           /* requesting it does not create it */
           mmbArray *tmp=NULL;
           cheat_assert(MSTRO_OK==mstro_cdo_access_mamba_array(cdo,&tmp));
           cheat_assert(tmp==NULL);

           /* now seal */
           cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo));


           cheat_assert(MSTRO_OK
                        == mstro_cdo_access_ptr(
                            cdo, &rec, NULL));
           fprintf(stderr, "rec: %p\n", (float*)rec);
           cheat_assert(rec==buf);

           /* afterwards we can also get mamba array as an attribute */
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_get(
                            cdo, ".maestro.core.cdo.mamba-array", &type, (const void**)&mamba_ptr));
           cheat_assert(type==MSTRO_CDO_ATTR_VALUE_pointer);
           cheat_assert(mamba_ptr!=NULL);

           /* and through mamba-array API */
           cheat_assert(MSTRO_OK==mstro_cdo_access_mamba_array(cdo,&tmp));
           cheat_assert(tmp!=NULL);

           /* an they are the same */
           cheat_assert(tmp==mamba_ptr);


           cheat_assert(MSTRO_OK == mstro_cdo_offer(cdo));
           cheat_assert(MSTRO_OK == mstro_cdo_withdraw(cdo));
           cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo));
           cheat_assert(MSTRO_OK == mstro_finalize());
           free(buf);
           )
