/* -*- mode:c -*- */
/** @file
 ** @brief check local pool basic operations
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>


CHEAT_TEST(cdo_local_pool_works,
           size_t data_count = 1031;
           double src_data[data_count];
           double dst_data[data_count];
           
           for(size_t i=0; i<data_count; i++) {
             src_data[i]=random();
           }
           cheat_assert(MSTRO_OK == mstro_init("Tests","POOL with rawptr",0));
           
           char name[] = "CDO wrapping a raw ptr";
           
           mstro_cdo src_cdo=NULL;
           mstro_cdo dst_cdo=NULL;
           
           cheat_assert(MSTRO_OK == mstro_cdo_declare(name,
                                                      MSTRO_ATTR_DEFAULT,
                                                      &src_cdo));
           cheat_assert(MSTRO_OK == mstro_cdo_declare(name,
                                                      MSTRO_ATTR_DEFAULT,
                                                      &dst_cdo));
           char scope_string[128];
           snprintf(scope_string, 128,
                    "scope:\n"
                    "  local-size: %zu", sizeof(src_data));
           fprintf(stdout, "Using scope string: `%s`\n", scope_string);
                    
           cheat_assert(MSTRO_OK == mstro_cdo_attribute_set_yaml(src_cdo, scope_string));
           cheat_assert(MSTRO_OK == mstro_cdo_attribute_set_yaml(dst_cdo, scope_string));

           cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(src_cdo,
                                                            MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                            src_data, false, true));
           cheat_assert(MSTRO_OK == mstro_cdo_attribute_set(dst_cdo,
                                                            MSTRO_ATTR_CORE_CDO_RAW_PTR,
                                                            dst_data, false, true));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_require(dst_cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_offer(src_cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_demand(dst_cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_withdraw(src_cdo));
           cheat_yield();

           for(size_t i=0; i<data_count; i++) {
           //  cheat_assert(src_data[i]==dst_data[i]);
               fprintf(stdout, "%f v %f\n", src_data[i],dst_data[i]);
           }
           cheat_yield();
           
           cheat_assert(MSTRO_OK == mstro_cdo_dispose(src_cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_cdo_dispose(dst_cdo));
           cheat_yield();
           cheat_assert(MSTRO_OK == mstro_finalize());
           )



