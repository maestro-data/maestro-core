/* -*- mode:c -*- */
/** @file
 ** @brief check CDO declaration seal
 **/

/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#ifdef  TOPSRCDIR
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define SRC_PATH TOSTRING(TOPSRCDIR) "/tests"
#else
#define SRC_PATH "."
#endif

#define SAMPLE_YAML_FILE TOSTRING(TOPSRCDIR) "/tests/sample_attributes.yaml"

#include "cheat.h"

#include "maestro.h"
#include <string.h>
#include <stdio.h>

/* This test declares a CDO, sets some attributes, seals the declaration and does a couple of lookups*/
CHEAT_TEST(cdo_declaration_seal_works,
           cheat_assert(MSTRO_OK == mstro_init("Tests","DECLARE",0));
           char name[] = "my_cdo_1";
           mstro_cdo cdo=NULL;
           enum mstro_cdo_attr_value_type type;
           const void* val;
           int64_t size;
           size = 16000;
           cheat_assert(MSTRO_OK
                        == mstro_cdo_declare(
                            name, MSTRO_ATTR_DEFAULT, &cdo));

           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_get(
                            cdo, "allocate-now", &type, &val));
           fprintf(stdout, "check: default allocate_now: %d (%p)\n",
                   *(const bool*)val, val);

           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set_yaml(
                            cdo, "allocate-now: \"yes\""));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set_yaml_from_file(
                            cdo, SAMPLE_YAML_FILE));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set_yaml(
                            cdo, "persist: \"yes\""));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set_yaml(
                            /* look, unicode utf-8 literals: */
                            cdo, u8"name: \"Robert Données \xF0\x9F\x90\x81\""));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set_yaml(
                            cdo, "scope: \n  local-size: 4096"));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set_yaml(
                            cdo,
                            "scope: \n"
                            "  local-size: 8192 \n"
                            "  layout: \n"
                            "    regular-1d: \n"
                            "      element-size: 8"));
           cheat_assert(!(MSTRO_OK
                        == mstro_cdo_attribute_set(
                            cdo, "maestro.core.cdo.scope.local-size",
                            &size, true, false))); // not an absolute path, not a valid relative path

           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_set(
                            cdo, ".maestro.core.cdo.scope.local-size",
                            &size, true, false));

           cheat_assert(MSTRO_OK == mstro_cdo_declaration_seal(cdo));


           cheat_assert(MSTRO_FAIL
                        == mstro_cdo_attribute_set_yaml(
                            cdo, "allocate-now: \"no\""));
           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_get(
                            cdo, "allocate-now", &type, &val));
           fprintf(stdout, "check: allocate-now: %d (%p)\n", *(const bool*)val, val);


           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_get(
                            cdo, "persist", &type, &val));
           fprintf(stdout, "check: persist: %d (%p)\n", *(const bool*)val, val);

           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_get(
                            cdo, "name", &type, &val));
           fprintf(stdout, "check: name == %s (%p)\n", (const char*)val, val);

           cheat_assert(MSTRO_OK
                        == mstro_cdo_attribute_get(
                            cdo, "scope.local-size", &type, &val));
           fprintf(stderr, "check: scope.local-size == %d (%p)\n",
                   *(const int*)val, val);

           cheat_assert(MSTRO_NOENT
                        == mstro_cdo_attribute_get(
                            cdo, "totally_an_attribute", &type, &val));

           cheat_assert(MSTRO_OK == mstro_cdo_dispose(cdo));

           cheat_assert(MSTRO_OK == mstro_finalize());
           )


/* put a path to user-defined schema*/

CHEAT_TEST(user_defined_schemas_works,

           setenv("MSTRO_SCHEMA_LIST","test_attributes.yaml", 1);
           setenv("MSTRO_SCHEMA_PATH",SRC_PATH, 1);

           cheat_assert(MSTRO_OK == mstro_init("Tests","DECLARE",0));

           mstro_cdo cdo;
           cheat_assert(MSTRO_OK ==mstro_cdo_declare("test cdo", MSTRO_ATTR_DEFAULT, &cdo));
           cheat_assert(MSTRO_OK ==mstro_cdo_attribute_set(cdo, ".maestro.test.t_1", "test value",true, false));
           cheat_assert(MSTRO_OK ==mstro_cdo_declaration_seal(cdo));

           cheat_assert(MSTRO_OK ==mstro_cdo_offer(cdo));

           cheat_assert(MSTRO_OK ==mstro_cdo_withdraw(cdo));
           cheat_assert(MSTRO_OK ==mstro_cdo_dispose(cdo));
           cheat_assert(MSTRO_OK == mstro_finalize());

          )

/* put a path to user-defined schema with wrong path and wrong separators*/

CHEAT_TEST(user_defined_schemas_doesnot_exist,

           setenv("MSTRO_SCHEMA_LIST", "user_attributes.yaml", 1);
           cheat_assert(MSTRO_OK != mstro_init("Tests","DECLARE",0));
           cheat_assert(MSTRO_FAIL == mstro_finalize());

          )

CHEAT_TEST(user_defined_schemas_paths,
           setenv("MSTRO_SCHEMA_LIST","benchmark_attributes.yaml;test_attributes.yaml",1);
           setenv("MSTRO_SCHEMA_PATH",SRC_PATH, 1);
           
           cheat_assert(MSTRO_OK == mstro_init("Tests","DECLARE",0));
           mstro_cdo cdo;
           cheat_assert(MSTRO_OK ==mstro_cdo_declare("test cdo", MSTRO_ATTR_DEFAULT, &cdo));
           cheat_assert(MSTRO_OK ==mstro_cdo_attribute_set(cdo, ".maestro.test.t_1", "test value", true, false));
           cheat_assert(MSTRO_OK ==mstro_cdo_attribute_set(cdo, ".maestro.benchmark.b_1", "bechmark value", true, false));
           cheat_assert(MSTRO_OK ==mstro_cdo_declaration_seal(cdo));
           
            cheat_assert(MSTRO_OK ==mstro_cdo_offer(cdo));

            cheat_assert(MSTRO_OK ==mstro_cdo_withdraw(cdo));
            cheat_assert(MSTRO_OK ==mstro_cdo_dispose(cdo));
            cheat_assert(MSTRO_OK == mstro_finalize());

  )
