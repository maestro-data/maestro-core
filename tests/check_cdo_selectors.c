/* -*- mode:c -*- */
/** @file
 ** @brief check CDO selector creation and application
 **/

/*
 * Copyright (C) 2020 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"

#include "maestro.h"
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>


#include "maestro/cdo_sel_parse.h"

CHEAT_DECLARE(
    mstro_schema s1=NULL, s2=NULL, s3=NULL;
    struct mstro_csq_val *q=NULL;

    const char *q1str= " .maestro.core.cdo.persist = false ";
    const char *q2str= " .maestro.core.cdo.persist = false ";
    const char *q3str="(.maestro.core.cdo.name ~= \"^CDO.*\")";
    const char *q3astr="( .maestro.core.cdo.name ~= \"^CDO.*\"/i )";
    const char *q4str="(and (.maestro.core.cdo.name ~= \"^CDO.*\"/i)"
                      "     (.maestro.core.cdo.persist = true))";
    const char *q5str="(or (.maestro.core.cdo.name ~= \"^CDO.*\"/i)"
                      "   \n  (.maestro.core.cdo.persist = true))";
    const char *q6str="(and (or (has-not .maestro.core.cdo.allocate-now)"
                      "         (.maestro.core.cdo.name ~= /^CDO.*/i   )"
                           ")"
                      "     (.maestro.core.cdo.persist = true))";
    const char *q7str="(or (and (has  .maestro.core.cdo.allocate-now)"
                      "         (.maestro.core.cdo.name ~= /^CDO.*/i   )"
                      "    )"
                      "    (.maestro.core.cdo.persist = false))";
              )


CHEAT_SET_UP(
    cheat_assert(MSTRO_OK==mstro_init("Tests","CSQ",0));
    cheat_yield();
  )

CHEAT_TEAR_DOWN(
    cheat_assert(MSTRO_OK==mstro_finalize());
                )
              

CHEAT_TEST(core_cdo_selector_NULL,
           /* check NULL selector */
           cheat_assert(MSTRO_OK==mstro_selector_parse(NULL, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_empty,
           /* check empty selector */
           cheat_assert(MSTRO_OK==mstro_selector_parse("", &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_single_unbracketed_comp,
           /* single unbracketed comparison */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q1str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_bracketed_comp,
           /* single bracketed comparison */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q2str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_single_regex,
           /* single regex match */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q3str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )
           
CHEAT_TEST(core_cdo_selector_single_ci_regex,
           /* single regex, c/i match */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q3astr, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )
           
CHEAT_TEST(core_cdo_selector_and,
           /* AND expression */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q4str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_or,
           /* OR expression */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q5str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_and_or,
           /* AND-OR expression */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q6str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )

CHEAT_TEST(core_cdo_selector_or_and,
           /* OR-AND expression */
           cheat_assert(MSTRO_OK==mstro_selector_parse(q7str, &q));
           cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
           cheat_yield();
           )


CHEAT_TEST(ecmwf_int_comparison,
           /* https://gitlab.version.fz-juelich.de/maestro/maestro-core/-/issues/16 */
	   /* parsing of integral CDO selector values */
           const char *s = "(.maestro.ecmwf.param = 2)";
	   cheat_assert(MSTRO_OK==mstro_selector_parse(s,&q));
	   cheat_yield();
	   cheat_assert(MSTRO_OK==mstro_csq_val_dispose(q));
	   cheat_yield();
	  )
