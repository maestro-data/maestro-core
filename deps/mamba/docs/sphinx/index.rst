.. Mamba documentation master file, created by
   sphinx-quickstart on Wed Nov  3 14:45:02 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mamba: Managed Abstracted Memory Array documentation
====================================================

A library-based programming model for C, C++ and Fortran based on Managed Abstract Memory Arrays, aiming to deliver simplified and efficient usage of diverse memory systems to application developers in a performance portable way. Mamba arrays exploit a unified memory interface to abstract memory from both traditional memory devices, accelerators, and storage. This library aims to achieve good performance portability with an easy to use approach that requires minimal code intrusion. 


.. toctree::
   :maxdepth: 2
   :caption: First steps

   quickstart
   examples

.. toctree::
   :maxdepth: 2
   :caption: Architecture

   overview
   mamba_abstract_memory_model
   mamba_array
   mamba_memory_management
   mamba_resource_manager
   mamba_library_implementation
