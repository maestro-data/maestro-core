Overview
========

The Memory Abstraction Device software stack, shown in the below figure, is positioned between a user application code and the underlying memory hardware/software to provide a generic and portable approach for accessing the diverse memories of a heterogeneous high performance computing system. The primary component of the stack is the Mamba library, consisting of a set of data containers that provide a high-level array-like interface for the user to manage application data, supported by a resource manager that defines an abstract memory model to map to system hardware, and provides a low-level support for managing memory allocation and movement in heterogeneous memory hierarchies. The resource manager may interface to one of a variety of underlying memory providers, the default of which is the built-in Mamba system provider. 

A number of supporting libraries make up the remainder of the software stack: the cost model library is intended to support the runtime in making decisions regarding tile sizing and data movement; the loop analysis library is intended to support analysis of data access within loop kernels; and the uMMap-IO library, which serves as an alternate memory provider.

.. figure:: /_static/images/Memory_abstraction_device_software_stack.png
   :width: 80%
   :align: center
   :alt: The software stack of the Memory Abstraction Device. User applications may exploit both the high level Mamba Data Containers and the lower level memory abstractions provided via the Resource Manager, to interact with the underlying memory system hardware/software stack.

   The software stack of the Memory Abstraction Device. User applications may exploit both the high level Mamba Data Containers and the lower level memory abstractions provided via the Resource Manager, to interact with the underlying memory system hardware/software stack.
