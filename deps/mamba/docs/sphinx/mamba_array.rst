Mamba Array
===========

A Mamba array is an array-like data structure that forms the core abstraction of the Mamba library. It is built on top of the abstract memory model, and the underlying data in a Mamba array may be transparently distributed across multiple types of memory. Subsets of the array may be duplicated or moved between memories, either explicitly by the user or implicitly by the Mamba runtime. The remainder of this section describes the process to construct, access, and move Mamba arrays.

.. figure:: /_static/images/array_distribution.png
   :width: 60%
   :align: center
   :alt: A conceptual illustration of Mamba arrays distributed across the different types of memory in an abstract memory space representing a heterogeneous memory system. Array subsets may reside in one or more memory space

   A conceptual illustration of Mamba arrays distributed across the different types of memory in an abstract memory space representing a heterogeneous memory system. Array subsets may reside in one or more memory space


Array Construction
-------------------
A Mamba array may be constructed with internal allocation, or by wrapping an existing user pointer. The constructor for a Mamba array requires four input arguments.

.. code:: C

   mmbError mmb_array_create(mmbDimensions *in_dims, 
                            mmbLayout *in_layout,
                            mmbMemInterface *in_interface, 
                            mmbAccessType in_access, 
                            mmbArray **out_mba);

   mmbError mmb_array_create_wrapped(void *in_data, 
                                    mmbDimensions *in_dims,
                                    mmbLayout *in_layout, 
                                    mmbMemInterface *in_interface, 
                                    mmbAccessType in_access,
                                    mmbArray **out_mba);
   mmbError mmb_array_destroy(mmbArray *in_mba);

The ``mmbDimensions`` argument, in dims, specifies the number of dimensions, and size of each, for the array.

The second argument, ``in_layout``, defines the layout of the array in physical memory, represented in the Mamba API as an ``mmbLayout`` data structure. This object defines the mapping of array indices to physical memory layout, and encapsulates array characteristics such as:   

* Object type (Element/AOS/SOA) and size in bytes
* Layout order (e.g. row major vs column major ordering)
* Layout type (e.g. regular, block cyclic, irregular)
* Array padding (in bytes)

The third argument for Mamba array construction, ``in_interface``, provides the mem- ory interface through which the array should be allocated. This may be obtained through memory registration or discovery.

The fourth argument, ``in_access``, indicates the access type for the array, represented by ``mmbAccessType`` in the Mamba API. This describes how the array is intended to be used and may be:

* MMB_READ
* MMB_WRITE
* MMB_READ_WRITE

The access type may influence where the array is placed in a particular memory space, such as using fast read-only memory where applicable, and is further exploited for array tiles.
Finally, a Mamba array may also be constructed wrapped around existing user data, using the constructor ``mmb_create_wrapped``. This allows a user to pass in pre-allocated data for management by the Mamba library, using the additional argument ``in_data``.

Array Tiling
------------

In the Mamba library, arrays may be decomposed into subsets for iteration or movement between memory spaces. These subsets are known as array tiles, and represented by the ``mmbArrayTile`` data structure. The process of decomposing an array into these subsets is known as tiling an array, and the set of array tiles that constitute the full array is known as an array tiling.

The following code snippet shows the API provided to tile, and untile, a Mamba array. The only argument required to tile an array, beyond the array itself, is a dimensions object in dims, which defines the size of a single tile.

.. code:: C

   mmbError mmb_array_tile(mmbArray *in_mba, mmbDimensions *in_dims); 
   mmbError mmb_array_untile(mmbArray *in_mba);

The full array will then be decomposed into tiles with these dimensions by the Mamba runtime. As indicated by the API, a tiling is an action applied to an array object, and only a single tiling may be active on an array at any one time. Repeated calls to ``mmb_array_tile`` with new dimensions will result in removal of any previous tiling applied to the provided array. 

.. figure:: /_static/images/array_tiling.png
   :width: 80%
   :align: center
   :alt: An illustration of the concept of array tiling in one and two dimensions, each tile may reside in one or more memory spaces.

   An illustration of the concept of array tiling in one and two dimensions, each tile may reside in one or more memory spaces.

Array tiles may be accessed either directly by requesting a tile by index in the array tiling, or indirectly by requesting an iterator over the set of array tiles. Below code snippet shows the API for direct access by index. N-dimensional tiles may be accessed via the generic ``mmb_tile_at``, whilst specific dimension tiles may be accessed more conveniently with per-dimension index arguments.

.. code:: C

   mmbError mmb_tile_at(mmbArray *in_mba, mmbIndex *in_idx, 
                      mmbArrayTile **out_tile);
   // ...
   mmbError mmb_tile_at_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1, 
                      mmbArrayTile **out_tile);
   // ...


Once a tile is acquired, it may be used to access the underlying array data. There are two mechanisms to do this, direct access via pointer or assisted indexing via convenience macros defined by the Mamba library. Direct access allows the user to retrieve a raw pointer from the array tile, and index it directly using indexing bounds stored in the tile structure. Alternatively, one may use the indexing macros provided by Mamba to simplify either just the index expression, or the full pointer access expression for common cases and/or more complicated layouts such as block cyclic.
