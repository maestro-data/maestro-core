/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif


#include "cheat.h"
#include "cheats.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#include <mamba.h>
#include <mmb_logging.h>


#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif



CHEAT_TEST(check_array_tiling_1d,

  // Initialise mamba
  cheat_assert (MMB_OK == mmb_init(MMB_INIT_DEFAULT));

  cheat_assert (MMB_OK == mmb_logging_set_debug_level(MMB_LOG_DEBUG));

  mmbLayout *src_layout, *src_layout2, *dst_layout, *regular_layout;
  int src_blocks = 2;
  size_t *src_offsets = malloc(sizeof(size_t) *src_blocks);
  size_t *src_lengths = malloc(sizeof(size_t) *src_blocks);

  int dst_blocks = 4;
  size_t *dst_offsets = malloc(sizeof(size_t) *dst_blocks);
  size_t *dst_lengths = malloc(sizeof(size_t) *dst_blocks);


  src_offsets[0] = 0;
  src_offsets[1] = 100;
  src_lengths[0] = 100;
  src_lengths[1] = 50;


  dst_offsets[0] = 0;
  dst_offsets[1] = 25;
  dst_offsets[2] = 75;
  dst_offsets[3] = 100;
  dst_lengths[0] = 25;
  dst_lengths[1] = 50;
  dst_lengths[2] = 25;
  dst_lengths[3] = 50;

  cheat_assert (MMB_OK == mmb_layout_create_dist_irregular_1d(sizeof(double),
                                       0,
                                       src_blocks,
                                       src_offsets,
                                       src_lengths,
                                       &src_layout));

  cheat_assert (MMB_OK == mmb_layout_create_dist_irregular_1d(sizeof(double),
                                       1,
                                       src_blocks,
                                       src_offsets,
                                       src_lengths,
                                       &src_layout2));


  cheat_assert (MMB_OK == mmb_layout_create_dist_irregular_1d(sizeof(double),
                                       0,
                                       dst_blocks,
                                       dst_offsets,
                                       dst_lengths,
                                       &dst_layout));

  cheat_assert (MMB_OK ==  mmb_layout_create_regular_1d(sizeof(float), MMB_PADDING_NONE, &regular_layout));


  /*check mmb_layout_cmp*/
  mmbLayoutEquivalence diff;

  cheat_assert (MMB_OK == mmb_layout_cmp(src_layout, src_layout, &diff));

  cheat_assert(MMB_LAYOUT_EQUAL == diff);

  cheat_assert (MMB_OK == mmb_layout_cmp(src_layout, src_layout2, &diff));

  cheat_assert(MMB_LAYOUT_DIFF_INDEX == diff);

  cheat_assert (MMB_OK == mmb_layout_cmp(src_layout, dst_layout, &diff));

  cheat_assert(MMB_LAYOUT_DIFF_FIELDS == diff);

  cheat_assert (MMB_OK == mmb_layout_cmp(src_layout, regular_layout, &diff));

  cheat_assert(MMB_LAYOUT_DIFF_TYPES == diff);

  /*check mmb_layout_compute_intersection */
  mmbLayoutIntersection *intersection;

  /* Two different types of layouts ... not implemented FIXME  */
  cheat_assert (MMB_OK != mmb_layout_compute_intersection(regular_layout, dst_layout, &intersection));

  cheat_assert (MMB_OK == mmb_layout_compute_intersection(src_layout, src_layout, &intersection));
  /** Intersection with myself ... same layout
    * dst piece 0 intersects src piece 0 at:
    * src_offset: = 0
    * dst_offset: = 0
    * length: = 100

    * dst piece 1 intersects src piece 1 at:
    * src_offset: = 0
    * dst_offset: = 0
    * length: = 50
    */

  /** check dst piece 0 intersects src piece 0 */
   int index = 0*intersection->n_src_pieces + 0;
   cheat_assert(intersection->overlap[index].src_offset == 0 );
   cheat_assert(intersection->overlap[index].dst_offset == 0 );
   cheat_assert(intersection->overlap[index].length == 100 );

  cheat_assert (MMB_OK == mmb_layout_destroy_mmbLayoutIntersection(intersection));

  cheat_assert (MMB_OK == mmb_layout_compute_intersection(src_layout, dst_layout, &intersection));

  /** intersection should be
    * dst piece 0 intersects src piece 0 at:
    * src_piece_offset: = 0
    * dst_piece_offset: = 0
    * length: = 25

    * dst piece 1 intersects src piece 0 at:
    * src_piece_offset: = 25
    * dst_piece_offset: = 0
    * length: = 50

    * dst piece 2 intersects src piece 0 at:
    * src_piece_offset: = 75
    * dst_piece_offset: = 0
    * length: = 25

    * dst piece 3 intersects src piece 1 at:
    * src_piece_offset: = 0
    * dst_piece_offset: = 0
    * length: = 50
  */

  /** check dst piece 1 intersects src piece 0 */
  index = 1*intersection->n_src_pieces + 0;
  cheat_assert(intersection->overlap[index].src_offset == 25 );
  cheat_assert(intersection->overlap[index].dst_offset == 0 );
  cheat_assert(intersection->overlap[index].length == 50 );

  cheat_assert (MMB_OK == mmb_layout_destroy_mmbLayoutIntersection(intersection));

  cheat_assert(MMB_OK == mmb_finalize());
)
