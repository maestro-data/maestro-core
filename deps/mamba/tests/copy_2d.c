/*
 * Copyright (C) 2021      Hewlett Packard Entreprise
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "cheats.h"
#include "mmb_cheats.h"

#include <stdlib.h>
#include <stdbool.h>

#include <mamba.h>
#include <mmb_memory.h>

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

CHEAT_DECLARE(
  mmbOptions *MMB_TESTS_INIT_DEFAULT;
#if defined(__GNUC__) || defined(__GNUG__)
#define TESTED_ARRAY_TYPE MMB_READ_WRITE
#define CONFIG_MEM_SIZE 8000UL
#define big_size (2 * (CONFIG_MEM_SIZE >> 10) * 1024*1024)
#else
  const mmbAccessType TESTED_ARRAY_TYPE = MMB_READ_WRITE;
  const size_t CONFIG_MEM_SIZE = 8000;
  const size_t big_size = 2 * (CONFIG_MEM_SIZE >> 10) * 1024*1024;
#endif /* defined(__GNUC__) || defined(__GNUG__) */

  const mmbMemSpaceConfig space_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = CONFIG_MEM_SIZE >> 10 },
    .interface_opts = { .provider = MMB_SYSTEM, .strategy = MMB_STRATEGY_NONE }
  };
  mmbLayout *layout = NULL;

  const size_t halo = 2;
  const size_t width = 4;
  const size_t height = 4;
  const long int buffer[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

  mmbMemSpace *dram_space = NULL;
  mmbMemInterface *dram_interface = NULL;

  mmbError mmb_copy_2d_wrapper(mmbAllocation *dev, mmbAllocation *host)
  {
    const size_t pitch = width + 2 * halo;
    return mmb_copy_2d(dev, halo*sizeof *buffer, halo, pitch*sizeof *buffer,
                       host, 0, 0, width*sizeof *buffer,
                       width*sizeof *buffer, height);
  }
)

CHEAT_SET_UP(
  cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK,
                   mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                               MAX_LOG_LEVEL));
  // Initialise mamba and register some memory
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  /* Don't use discovery, but force the use of space limited declared memory. */
  /* Size is given in MiB, we don't want as much as 8000MiB, but only 8MiB */
  cheat_assert_int(MMB_OK,
      mmb_register_memory(MMB_DRAM, MMB_CPU, &space_config, &dram_space));
  cheat_assert_int(MMB_OK,
      mmb_create_interface(dram_space, &space_config.interface_opts, &dram_interface));
)

CHEAT_TEAR_DOWN(
  /* Cleanup intermediate objects, cleanup mamba */
  cheat_assert_int(MMB_OK, mmb_finalize());
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

CHEAT_TEST(copy2d_cpu,
  int is_valid = 1;
  mmbAllocation* host_allocation;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped((void *)buffer, sizeof buffer,
                                                 dram_interface, &host_allocation));

  /* Set halo */
  mmbAllocation *dev_local_alloc;
  const size_t pitch = width + 2 * halo;
  long int local_buff[pitch * (height + 2 * halo)];
  for (size_t row = 0; height + 2 * halo > row; ++row)
    for (size_t column = 0; pitch > column; ++column)
      local_buff[row*pitch+column] = -1L;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped(local_buff, sizeof local_buff,
                                                 dram_interface, &dev_local_alloc));
  /* CPU */
  /* Check copy 2d */
  cheat_assert_int(MMB_OK, mmb_copy_2d_wrapper(dev_local_alloc, host_allocation));
  /* Check the tile into centered in the halo */
  long int *buff = dev_local_alloc->ptr;
  for (size_t row = 0; height > row; ++row)
    for (size_t column = 0; width > column; ++column)
      if (buff[pitch*(halo+row)+halo+column] != (long int)(row*width+column))
        is_valid = 0;
  cheat_assert_int(1, is_valid);
  is_valid = 1;
  /* Check rows from the halo */
  for (size_t row = 0; halo > row; ++row)
    for (size_t column = 0; pitch > column; ++column)
      if (   buff[row*pitch+column] != -1   /* top row */
          || buff[(row+halo+height)*pitch+column] != -1)    /* bottom row */
        is_valid = 0;
  cheat_assert_int(1, is_valid);
  is_valid = 1;
  /* Check column from the halo */
  for (size_t row = 0; height > row; ++row)
    for (size_t column = 0; halo > column; ++column)
      if (   buff[pitch*(halo+row)+column] != -1    /* left column */
          || buff[pitch*(halo+row)+width+halo+column] != -1)    /* right column */
        is_valid = 0;
  cheat_assert_int(1, is_valid);
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(dev_local_alloc));
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(host_allocation));
)

/***************************************************************************
 *
 *  CUDA
 *
 ***************************************************************************/

#if HAVE_CUDA

CHEAT_DECLARE(
  mmbError check_copy_2d_cuda(mmbAllocation *cu_buff,
                              const size_t halo,
                              const size_t width,
                              const size_t height,
                              int *err);

  mmbMemSpace *cuda_space = NULL;
  mmbMemInterface *cuda_interface = NULL;

  mmbError init_cuda_space_and_interface(void)
  {
    mmbExecutionContext ex_con = MMB_GPU_CUDA;
    mmbMemSpace **space = &cuda_space;
    const mmbMemInterfaceConfig *interface_config = &space_config.interface_opts;
    mmbMemInterface **interface = &cuda_interface;
    mmbError stat = MMB_OK;
    stat = mmb_request_space(MMB_GDRAM, ex_con, &space_config, space);
    switch (stat) {
      case MMB_OK:
        break;
      case MMB_NOT_FOUND:
        stat = MMB_OK;
        /* FALLTHROUGH */
      default:
        return stat;
    }
    cheat_assert_not_pointer(NULL, *space);
    stat = mmb_create_interface(*space, interface_config, interface);
    cheat_assert_int(MMB_OK, stat);
    cheat_assert_not_pointer(NULL, *interface);
    return stat;
  }
)

CHEAT_TEST(copy2d_cuda,
  /* Only test the 2d copy to/from GPU if discovery is available */
  int discovery_enabled = 0;
  mmbError stat = mmb_discovery_is_enabled(&discovery_enabled);
  cheat_assert_int(MMB_OK, stat);
  if (MMB_OK == stat && discovery_enabled) {
    /* CUDA */
    cheat_assert_int(MMB_OK, stat = init_cuda_space_and_interface());
  }

  if (MMB_OK != stat || NULL == cuda_interface) {
    return; /* Early completion if the conditions to run the test are met. */
  }

  int err = 0;
  mmbAllocation* host_allocation;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped((void *)buffer, sizeof buffer,
                                                 dram_interface, &host_allocation));

  /* Set halo */
  mmbAllocation *dev_local_alloc;
  const size_t pitch = width + 2 * halo;
  long int local_buff[pitch * (height + 2 * halo)];
  for (size_t row = 0; height + 2 * halo > row; ++row)
    for (size_t column = 0; pitch > column; ++column)
      local_buff[row*pitch+column] = -1L;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped(local_buff, sizeof local_buff,
                                                 dram_interface, &dev_local_alloc));
  cheat_assert_not_pointer(NULL, cuda_interface);
  /* CUDA */
  if (NULL != cuda_interface) {
    mmbAllocation *cuda_allocation;
    cheat_assert_int(MMB_OK,
        mmb_allocate(sizeof local_buff, cuda_interface, &cuda_allocation));
    cheat_assert_int(MMB_OK, mmb_copy(cuda_allocation, dev_local_alloc));

    /* Check copy 2d */
    cheat_assert_int(MMB_OK, mmb_copy_2d_wrapper(cuda_allocation, host_allocation));
    cheat_assert_int(MMB_OK,
        check_copy_2d_cuda(cuda_allocation, halo, width, height, &err));
    cheat_assert_int(0, err);
    cheat_assert_int(MMB_OK, mmb_free(cuda_allocation));
  }
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(dev_local_alloc));
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(host_allocation));
)

#endif /* HAVE_CUDA */

/***************************************************************************
 *
 *  OpenCL
 *
 ***************************************************************************/

#if HAVE_OPENCL

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include <mmb_opencl.h>

CHEAT_DECLARE(
  mmbError init_opencl_parameters(cl_device_id *device,
                                  cl_context *context,
                                  cl_command_queue *queue,
                                  int *stat);

  mmbError check_copy_2d_opencl(mmbAllocation *ocl_buff,
                                const size_t halo,
                                const size_t width,
                                const size_t height,
                                const struct mmb_opencl_t *oclopt,
                                int *err);

  mmbMemSpace *opencl_space = NULL;
  mmbMemInterface *opencl_interface = NULL;
  struct mmb_opencl_t oclopt = {0};

  mmbError init_gpu_space_and_interface(mmbExecutionContext ex_con,
                                        const mmbMemSpaceConfig *space_config,
                                        mmbMemSpace **space,
                                        const mmbMemInterfaceConfig *interface_config,
                                        mmbMemInterface **interface)
  {
    mmbError stat = MMB_OK;
    stat = mmb_request_space(MMB_GDRAM, ex_con, space_config, space);
    switch (stat) {
      case MMB_OK:
        break;
      case MMB_NOT_FOUND:
        stat = MMB_OK;
        /* FALLTHROUGH */
      default:
        return stat;
    }
    cheat_assert_not_pointer(NULL, *space);
    stat = mmb_create_interface(*space, interface_config, interface);
    cheat_assert_int(MMB_OK, stat);
    cheat_assert_not_pointer(NULL, *interface);
    return stat;
  }

  mmbError init_opencl_space_and_interface(void)
  {
    mmbMemInterfaceConfig interface_config = space_config.interface_opts;
    int err = 0;
    cl_device_id     d;
    cl_context       c;
    cl_command_queue q;

    mmbError stat = init_opencl_parameters(&d, &c, &q, &err);
    cheat_assert_int(MMB_OK, stat);
    cheat_assert_int(0, err);
    /* Skip test on error */
    if (MMB_OK != stat) return stat;

    oclopt = (struct mmb_opencl_t) { .device = d, .context = c, .queue = q};
    interface_config.provider_opts.ocl = &oclopt;
    mmbExecutionContext ex_con = MMB_OPENCL;
    mmbMemSpace **space = &opencl_space;
    mmbMemInterface **interface = &opencl_interface;
    stat = mmb_request_space(MMB_GDRAM, ex_con, &space_config, space);
    switch (stat) {
      case MMB_OK:
        break;
      case MMB_NOT_FOUND:
        stat = MMB_OK;
        /* FALLTHROUGH */
      default:
        return stat;
    }
    cheat_assert_not_pointer(NULL, *space);
    stat = mmb_create_interface(*space, &interface_config, interface);
    cheat_assert_int(MMB_OK, stat);
    cheat_assert_not_pointer(NULL, *interface);
    return stat;
  }
)

CHEAT_TEST(copy2d_opencl,
  /* Only test the 2d copy to/from GPU if discovery is available */
  int discovery_enabled = 0;
  mmbError stat = mmb_discovery_is_enabled(&discovery_enabled);
  cheat_assert_int(MMB_OK, stat);
  if (MMB_OK == stat && discovery_enabled) {
    /* OpenCL */
    cheat_assert_int(MMB_OK, stat = init_opencl_space_and_interface());
  }

  if (MMB_OK != stat || NULL == opencl_interface) {
    return; /* Early completion if the conditions to run the test are met. */
  }

  int err = 0;
  mmbAllocation* host_allocation;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped((void *)buffer, sizeof buffer,
                                                 dram_interface, &host_allocation));

  /* Set halo */
  mmbAllocation *dev_local_alloc;
  const size_t pitch = width + 2 * halo;
  long int local_buff[pitch * (height + 2 * halo)];
  for (size_t row = 0; height + 2 * halo > row; ++row)
    for (size_t column = 0; pitch > column; ++column)
      local_buff[row*pitch+column] = -1L;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped(local_buff, sizeof local_buff,
                                                 dram_interface, &dev_local_alloc));
  cheat_assert_not_pointer(NULL, opencl_interface);
  /* OpenCL */
  if (NULL != opencl_interface) {
    mmbAllocation *ocl_allocation;
    cheat_assert_int(MMB_OK,
        mmb_allocate(sizeof local_buff, opencl_interface, &ocl_allocation));
    cheat_assert_int(MMB_OK, mmb_copy(ocl_allocation, dev_local_alloc));

    /* Check copy 2d */
    cheat_assert_int(MMB_OK, mmb_copy_2d_wrapper(ocl_allocation, host_allocation));
    cheat_assert_int(MMB_OK,
        check_copy_2d_opencl(ocl_allocation, halo, width, height, &oclopt, &err));
    cheat_assert_int(0, err);
    cheat_assert_int(MMB_OK, mmb_free(ocl_allocation));
  }
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(dev_local_alloc));
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(host_allocation));
)

#endif /* HAVE_OPENCL */

/***************************************************************************
 *
 *  Rocm HIP
 *
 ***************************************************************************/

#if HAVE_ROCM

CHEAT_DECLARE(
  mmbError check_copy_2d_hip(mmbAllocation *hip_buff,
                             const size_t halo,
                             const size_t width,
                             const size_t height,
                             int *err);

  mmbMemSpace *hip_space = NULL;
  mmbMemInterface *hip_interface = NULL;

  mmbError init_hip_space_and_interface(void)
  {
    mmbError stat = MMB_OK;
    mmbExecutionContext ex_con = MMB_GPU_HIP;
    mmbMemSpace **space = &hip_space;
    const mmbMemInterfaceConfig *interface_config = &space_config.interface_opts;
    mmbMemInterface **interface = &hip_interface;
    stat = mmb_register_memory(MMB_GDRAM, ex_con, &space_config, space);
    /* TODO: uncomment the function call once the backend discovery works, and
     * remove registration. */
    /* stat = mmb_request_space(MMB_GDRAM, ex_con, &space_config, space); */
    switch (stat) {
      case MMB_OK:
        break;
      case MMB_NOT_FOUND:
        stat = MMB_OK;
        /* FALLTHROUGH */
      default:
        return stat;
    }
    cheat_assert_not_pointer(NULL, *space);
    stat = mmb_create_interface(*space, interface_config, interface);
    cheat_assert_int(MMB_OK, stat);
    cheat_assert_not_pointer(NULL, *interface);
    return stat;
  }
)

CHEAT_TEST(copy2d_hip,
  /* Only test the 2d copy to/from GPU if discovery is available */
  int discovery_enabled = 0;
  mmbError stat = mmb_discovery_is_enabled(&discovery_enabled);
  cheat_assert_int(MMB_OK, stat);
  if (MMB_OK == stat && discovery_enabled) {
    /* ROCM HIP */
    cheat_assert_int(MMB_OK, stat = init_hip_space_and_interface());
  }

  if (MMB_OK != stat || NULL == hip_interface) {
    return; /* Early completion if the conditions to run the test are met. */
  }

  int err = 0;
  mmbAllocation* host_allocation;
  /* TODO: _buffer is a workaround issue #91 and should be removed once the
   * issue with hipMemcpy2D is resolved. */
  long int _buffer[16];
  memcpy(_buffer, buffer, sizeof buffer);
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped((void *)_buffer, sizeof buffer,
                                                 dram_interface, &host_allocation));

  /* Set halo */
  mmbAllocation *dev_local_alloc;
  const size_t pitch = width + 2 * halo;
  long int local_buff[pitch * (height + 2 * halo)];
  for (size_t row = 0; height + 2 * halo > row; ++row)
    for (size_t column = 0; pitch > column; ++column)
      local_buff[row*pitch+column] = -1L;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped(local_buff, sizeof local_buff,
                                                 dram_interface, &dev_local_alloc));
  cheat_assert_not_pointer(NULL, hip_interface);
  /* ROCm HIP */
  if (NULL != hip_interface) {
    mmbAllocation *hip_allocation;
    cheat_assert_int(MMB_OK,
        mmb_allocate(sizeof local_buff, hip_interface, &hip_allocation));
    cheat_assert_int(MMB_OK, mmb_copy(hip_allocation, dev_local_alloc));

    /* Check copy 2d */
    cheat_assert_int(MMB_OK, mmb_copy_2d_wrapper(hip_allocation, host_allocation));
    cheat_assert_int(MMB_OK,
        check_copy_2d_hip(hip_allocation, halo, width, height, &err));
    cheat_assert_int(0, err);
    cheat_assert_int(MMB_OK, mmb_free(hip_allocation));
  }
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(dev_local_alloc));
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(host_allocation));
)

#endif /* HAVE_ROCM */
