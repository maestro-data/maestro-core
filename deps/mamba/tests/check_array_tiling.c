/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif


#include "cheat.h"
#include "cheats.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#include <mamba.h>
#include <mmb_tile.h>
#include <mmb_tile_iterator.h>
#include <mmb_memory.h>
#include <mmb_logging.h>
#include <mmb_options.h>

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

CHEAT_DECLARE(
  mmbError register_test_memory (void)
  {
    int discovery_enabled = 0;
    cheat_assert_int(MMB_OK, mmb_discovery_is_enabled(&discovery_enabled));
    if (discovery_enabled) {
      return MMB_OK;
    }
    const mmbMemSpaceConfig dram_config = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };
    cheat_assert(MMB_OK == mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, NULL));
    return MMB_OK;
  }
)

CHEAT_DECLARE( mmbOptions *MMB_TESTS_INIT_DEFAULT; )

CHEAT_SET_UP(
    cheat_assert_int(mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT), MMB_OK);
    cheat_assert_int(mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                                 MAX_LOG_LEVEL),
                     MMB_OK);
)

CHEAT_TEAR_DOWN(
    cheat_assert_int(mmb_options_destroy(MMB_TESTS_INIT_DEFAULT), MMB_OK);
)

/****************************************************************************
 * 
 *  Check 1D Tiling
 * 
 * *************************************************************************/
CHEAT_DECLARE(
  size_t array_size_1d = 32;
  size_t tile_size_1d = 8;
  size_t irregular_tile_size_1d = 7;
  size_t oversize_tile_size_1d = 100;
  mmbError init_matrix_buffer(mmbArray *mba_a);
  mmbError write_to_tile(mmbArrayTile *tile);
  mmbError check_result(mmbArray *mba);
)

CHEAT_TEST(check_array_tiling_1d,

  // Initialise mamba and register some memory
  cheat_assert (MMB_OK == mmb_init(MMB_TESTS_INIT_DEFAULT));
  cheat_assert (MMB_OK == register_test_memory());

  mmbMemSpace *dram_space;
  /* Whether we registered the memory ourselves or we let the discovery do it,
   * we don't need to provide any specific arguments for the space request,
   * hence we can give a NULL pointer. */
  cheat_assert(MMB_OK == mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space));

  mmbMemInterface *dram_interface;
  cheat_assert(MMB_OK == mmb_request_interface(dram_space, NULL, &dram_interface));
 
  // Create mmb array
  mmbArray *mba;
  mmbLayout *layout;
  size_t arrdims[1] = {array_size_1d};
  mmbDimensions dims = {1, arrdims};

  cheat_assert(MMB_OK == mmb_layout_create_regular_1d(sizeof(int), MMB_PADDING_NONE, &layout));
  cheat_assert(MMB_OK == mmb_array_create(&dims, layout, dram_interface, MMB_READ_WRITE, &mba));
  cheat_assert(MMB_OK == mmb_layout_destroy(layout));

  // Zero array, tile it, write to tiles, check result
  cheat_assert(MMB_OK == init_matrix_buffer(mba));
  mmbDimensions chunks = {1, &tile_size_1d};
  cheat_assert(MMB_OK == mmb_array_tile(mba, &chunks));
  mmbArrayTile *tile;
  mmbIndex *idx;
  mmbDimensions *tiling_dims;
  cheat_assert(MMB_OK == mmb_index_create(1, &idx));
  cheat_assert(MMB_OK == mmb_tiling_dimensions(mba, &tiling_dims));
  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {
      cheat_assert(MMB_OK == mmb_index_set(idx,ti));
      cheat_assert(MMB_OK == mmb_tile_at(mba, idx, &tile));
      cheat_assert(MMB_OK == write_to_tile(tile));
  }
  cheat_assert(MMB_OK == mmb_dimensions_destroy(tiling_dims));
  cheat_assert(MMB_OK == check_result(mba));

  // Repeat with a tile size that does not distribute evenly
  cheat_assert(MMB_OK == init_matrix_buffer(mba));
  mmbDimensions irregular_chunks = {1, &irregular_tile_size_1d};
  cheat_assert(MMB_OK == mmb_array_tile(mba, &irregular_chunks));
  cheat_assert(MMB_OK == mmb_index_create(1, &idx));
  cheat_assert(MMB_OK == mmb_tiling_dimensions(mba, &tiling_dims));
  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {  
    cheat_assert(MMB_OK == mmb_index_set(idx,ti));
    cheat_assert(MMB_OK == mmb_tile_at(mba, idx, &tile));
    cheat_assert(MMB_OK == write_to_tile(tile));
  }
  cheat_assert(MMB_OK == mmb_dimensions_destroy(tiling_dims));
  cheat_assert(MMB_OK == check_result(mba));

  // Repeat with a tile size larger than the array
  cheat_assert(MMB_OK == init_matrix_buffer(mba));
  mmbDimensions oversize_chunks = {1, &oversize_tile_size_1d};
  cheat_assert(MMB_OK == mmb_array_tile(mba, &oversize_chunks));
  cheat_assert(MMB_OK == mmb_index_create(1, &idx));
  cheat_assert(MMB_OK == mmb_tiling_dimensions(mba, &tiling_dims));
  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {  
    cheat_assert(MMB_OK == mmb_index_set(idx,ti));
    cheat_assert(MMB_OK == mmb_tile_at(mba, idx, &tile));
    cheat_assert(MMB_OK == write_to_tile(tile));
  }
  cheat_assert(MMB_OK == mmb_dimensions_destroy(tiling_dims));
  cheat_assert(MMB_OK == check_result(mba));

  // Cleanup intermediate objects, cleanup mamba
  cheat_assert(MMB_OK == mmb_index_destroy(idx));
  cheat_assert(MMB_OK == mmb_array_destroy(mba));
  cheat_assert(MMB_OK == mmb_finalize());
)

CHEAT_DECLARE(
  mmbError init_matrix_buffer(mmbArray *mba_a)
  {
    // Tile array with single tile and initialise to 0
    mmbError stat = mmb_array_tile(mba_a, &mba_a->dims);
    CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

    mmbTileIterator *it;
    stat = mmb_tile_iterator_create(mba_a, &it);
    CHECK_STATUS(stat, "Failed to create tile iterator\n", BAILOUT);

    mmbArrayTile* tile;
    stat = mmb_tile_iterator_first(it,&tile);
    CHECK_STATUS(stat, "Failed to set tile iterator to first\n", BAILOUT);

    for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
      MMB_IDX_1D(tile, i, int) = 0;
    }

    stat = mmb_tile_iterator_destroy(it);
    CHECK_STATUS(stat, "Failed to free tile iterator\n", BAILOUT);

    stat = mmb_array_untile(mba_a);
    CHECK_STATUS(stat, "Failed to untile mamba array\n", BAILOUT);

  BAILOUT:
    return stat;
  }

  mmbError write_to_tile(mmbArrayTile *tile)
  {
    // Fill array tile with indices
    for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
      MMB_IDX_1D(tile, i, int) = (int)i;
    }
    return MMB_OK;
  }


  mmbError check_result(mmbArray *mba)
  {
    // Check array is full of indices
    mmbError stat = mmb_array_tile(mba, &mba->dims);
    CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

    mmbTileIterator *it;
    stat = mmb_tile_iterator_create(mba, &it);
    CHECK_STATUS(stat, "Failed to get tile iterator\n", BAILOUT);

    mmbArrayTile* tile;
    stat = mmb_tile_iterator_first(it,&tile);
    CHECK_STATUS(stat, "Failed to set tile iterator to first\n", BAILOUT);

    if(tile->lower[0] != 0 || tile->upper[0] != array_size_1d) {
      MMB_ERR("Check array tiling bounds incorrect\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    } 
    for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
      if(MMB_IDX_1D(tile, i, int) != (int) i) {
        MMB_ERR("Array data incorrect at idx %zu, expected %d, got: %d\n", i, (int) i, MMB_IDX_1D(tile, i, int));
        stat = MMB_ERROR;
        goto BAILOUT;
      }
    }

    stat = mmb_tile_iterator_destroy(it);
    CHECK_STATUS(stat, "Failed to free tile iterator\n", BAILOUT);

    stat = mmb_array_untile(mba);
    CHECK_STATUS(stat, "Failed to untile mamba array\n", BAILOUT);

  BAILOUT:
    return stat;
  }
)

/****************************************************************************
 * 
 *  Check 2D Tiling
 * 
 * *************************************************************************/

CHEAT_DECLARE(
  size_t array_size_2d_w = 32;
  size_t array_size_2d_h = 32;
  size_t tile_size_2d_w = 4;
  size_t tile_size_2d_h = 4;
  size_t irregular_tile_size_2d_w = 9;
  size_t irregular_tile_size_2d_h = 9;
  size_t oversize_tile_size_2d_w = 200;
  size_t oversize_tile_size_2d_h = 100;
  mmbError init_matrix_buffer_2d(mmbArray *mba_a);
  mmbError write_to_tile_2d(mmbArrayTile *tile);
  mmbError check_result_2d(mmbArray *mba);
)

CHEAT_TEST(check_array_tiling_2d,

  // Initialise mamba and register some memory
  cheat_assert (MMB_OK == mmb_init(MMB_TESTS_INIT_DEFAULT));
  cheat_assert (MMB_OK == register_test_memory());

  mmbMemSpace *dram_space;
  /* Whether we registered the memory ourselves or we let the discovery do it,
   * we don't need to provide any specific arguments for the space request,
   * hence we can give a NULL pointer. */
  cheat_assert(MMB_OK == mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space));

  mmbMemInterface *dram_interface;
  cheat_assert(MMB_OK == mmb_request_interface(dram_space, NULL, &dram_interface));
 
  // Create mmb array
  mmbArray *mba;
  mmbLayout *layout;
  size_t arrdims[2] = {array_size_2d_w, array_size_2d_h};
  mmbDimensions dims = {2, arrdims};

  cheat_assert(MMB_OK == mmb_layout_create_regular_nd(sizeof(int), 2, MMB_COLMAJOR, MMB_PADDING_NONE, &layout));
  cheat_assert(MMB_OK == mmb_array_create(&dims, layout, dram_interface, MMB_READ_WRITE, &mba));
  cheat_assert(MMB_OK == mmb_layout_destroy(layout));

  mmbArrayTile *tile;
  mmbIndex *idx;
  mmbDimensions *tiling_dims;

  // Zero array, tile it, write to tile, and check result
  cheat_assert(MMB_OK == init_matrix_buffer(mba));
  size_t chunkdims[2] = {tile_size_2d_w, tile_size_2d_h};
  mmbDimensions chunks = {2, chunkdims};
  cheat_assert(MMB_OK == mmb_array_tile(mba, &chunks));
  cheat_assert(MMB_OK == mmb_index_create(2, &idx));
  cheat_assert(MMB_OK == mmb_tiling_dimensions(mba, &tiling_dims));
  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {
      for (size_t tj = 0; tj < tiling_dims->d[1]; ++tj) {
      cheat_assert(MMB_OK == mmb_index_set(idx,ti,tj));
      cheat_assert(MMB_OK == mmb_tile_at(mba, idx, &tile));
      cheat_assert(MMB_OK == write_to_tile(tile));
      }
  }
  cheat_assert(MMB_OK == mmb_dimensions_destroy(tiling_dims));
  cheat_assert(MMB_OK == check_result(mba));

  // Repeat for irregular size tiles
  cheat_assert(MMB_OK == init_matrix_buffer(mba));
  size_t irregular_chunkdims[2] = {irregular_tile_size_2d_w, irregular_tile_size_2d_h};
  mmbDimensions irregular_chunks = {2, irregular_chunkdims};
  cheat_assert(MMB_OK == mmb_array_tile(mba, &irregular_chunks));
  cheat_assert(MMB_OK == mmb_index_create(2, &idx));
  cheat_assert(MMB_OK == mmb_tiling_dimensions(mba, &tiling_dims));
  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {
      for (size_t tj = 0; tj < tiling_dims->d[1]; ++tj) {
      cheat_assert(MMB_OK == mmb_index_set(idx,ti,tj));
      cheat_assert(MMB_OK == mmb_tile_at(mba, idx, &tile));
      cheat_assert(MMB_OK == write_to_tile(tile));
      }
  }
  cheat_assert(MMB_OK == mmb_dimensions_destroy(tiling_dims));
  cheat_assert(MMB_OK == check_result(mba));

  // Repeat for oversize tiles
  cheat_assert(MMB_OK == init_matrix_buffer(mba));
  size_t oversize_chunkdims[2] = {oversize_tile_size_2d_w, oversize_tile_size_2d_h};
  mmbDimensions oversize_chunks = {2, oversize_chunkdims};
  cheat_assert(MMB_OK == mmb_array_tile(mba, &oversize_chunks));
  cheat_assert(MMB_OK == mmb_index_create(2, &idx));
  cheat_assert(MMB_OK == mmb_tiling_dimensions(mba, &tiling_dims));
  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {
      for (size_t tj = 0; tj < tiling_dims->d[1]; ++tj) {
      cheat_assert(MMB_OK == mmb_index_set(idx,ti,tj));
      cheat_assert(MMB_OK == mmb_tile_at(mba, idx, &tile));
      cheat_assert(MMB_OK == write_to_tile(tile));
      }
  }
  cheat_assert(MMB_OK == mmb_dimensions_destroy(tiling_dims));
  cheat_assert(MMB_OK == check_result(mba));

  // Cleanup intermediate objects, cleanup mamba
  cheat_assert(MMB_OK == mmb_index_destroy(idx));
  cheat_assert(MMB_OK == mmb_array_destroy(mba));
  cheat_assert(MMB_OK == mmb_finalize());
)

CHEAT_DECLARE(
  mmbError init_matrix_buffer_2d(mmbArray *mba_a)
  {
    // Tile array with single tile and initialise to 0
    mmbError stat = mmb_array_tile(mba_a, &mba_a->dims);
    CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

    mmbTileIterator *it;
    stat = mmb_tile_iterator_create(mba_a, &it);
    CHECK_STATUS(stat, "Failed to create tile iterator\n", BAILOUT);

    mmbArrayTile* tile = it->tile;
    stat = mmb_tile_iterator_first(it,&tile);
    CHECK_STATUS(stat, "Failed to set tile iterator to first\n", BAILOUT);

    for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
      for (size_t j = tile->lower[1]; j < tile->upper[1] ;++j) {
        MMB_IDX_2D(tile, i, j, int) = 0;
      }
    }

    stat = mmb_tile_iterator_destroy(it);
    CHECK_STATUS(stat, "Failed to free tile iterator\n", BAILOUT);

    stat = mmb_array_untile(mba_a);
    CHECK_STATUS(stat, "Failed to untile mamba array\n", BAILOUT);

  BAILOUT:
    return stat;
  }

  mmbError write_to_tile_2d(mmbArrayTile *tile)
  {
    // Fill array tile with indices
    for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
      for (size_t j = tile->lower[1]; j < tile->upper[1] ;++j) {
        MMB_IDX_2D(tile, i, j, int) = (int) i * tile->abs_dim[1] + j;
      }
    }
    return MMB_OK;
  }


  mmbError check_result_2d(mmbArray *mba)
  {
    // Check array is full of indices
    mmbError stat = mmb_array_tile(mba, &mba->dims);
    CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

    mmbTileIterator *it;
    stat = mmb_tile_iterator_create(mba, &it);
    CHECK_STATUS(stat, "Failed to get tile iterator\n", BAILOUT);

    mmbArrayTile* tile = it->tile;
    stat = mmb_tile_iterator_first(it,&tile);
    CHECK_STATUS(stat, "Failed to set tile iterator to first\n", BAILOUT);
    
    if(tile->lower[0] != 0 || tile->lower[1] != 0 ||
       tile->upper[0] != array_size_2d_w || tile->upper[1] != array_size_2d_h) {
      MMB_ERR("Check array tiling bounds incorrect\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    } 
    for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
      for (size_t j = tile->lower[1]; j < tile->upper[1] ;++j) {
        if(MMB_IDX_2D(tile, i, j, int) != (int) (i * array_size_2d_w + j)) {
          MMB_ERR("Array data incorrect at idx %zu, expected %d, got: %d\n", i, (int) i, MMB_IDX_1D(tile, i, int));
          stat = MMB_ERROR;
          goto BAILOUT;
        }
      }
    }

    stat = mmb_tile_iterator_destroy(it);
    CHECK_STATUS(stat, "Failed to free tile iterator\n", BAILOUT);

    stat = mmb_array_untile(mba);
    CHECK_STATUS(stat, "Failed to untile mamba array\n", BAILOUT);

  BAILOUT:
    return stat;
  }
)
