/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstdio>
#include <hip/hip_runtime.h>

#include "mamba.h"
#include "mmb_logging.h"
#include "mmb_error.h"
#include "mmb_memory.h"

__device__ int d_is_valid = 0;

__global__
void check_tile(const long int *buff,
                const std::size_t halo,
                const std::size_t width,
                const std::size_t height)
{
  const std::size_t tid = hipThreadIdx_x + hipBlockIdx_x * hipBlockDim_x;

  /* Only check with one worker */
  if (0 != tid) return;

  /* Get pitch size */
  const std::size_t pitch = width + 2 * halo;

  /* Check the tile into centered in the halo */
  for (std::size_t row = 0; height > row; ++row)
    for (std::size_t column = 0; width > column; ++column)
      if (buff[pitch*(halo+row)+halo+column] != (long int)(row*width+column))
        return;
  /* Check rows from the halo */
  for (std::size_t row = 0; halo > row; ++row)
    for (std::size_t column = 0; pitch > column; ++column)
      if (   buff[row*pitch+column] != -1   /* top row */
          || buff[(row+halo+height)*pitch+column] != -1)    /* bottom row */
        return;
  /* Check column from the halo */
  for (std::size_t row = 0; height > row; ++row)
    for (std::size_t column = 0; halo > column; ++column)
      if (   buff[pitch*(halo+row)+column] != -1    /* left column */
          || buff[pitch*(halo+row)+width+halo+column] != -1)    /* right column */
        return;
  d_is_valid = 1;
}

extern "C"
mmbError check_copy_2d_hip(mmbAllocation *hip_buff,
                           const std::size_t halo,
                           const std::size_t width,
                           const std::size_t height,
                           int *err)
{
  mmbError stat = MMB_ERROR;
  hipError_t hip_err = hipSuccess;
  int is_valid = -1;
  const std::size_t pitch = width + 2 * halo;
  /* Check 2D copy */
  hipLaunchKernelGGL(check_tile, dim3(), dim3(), 0, 0,
                     (const long int *)hip_buff->ptr, halo, width, height);
  hip_err = hipMemcpyFromSymbol(&is_valid, d_is_valid, sizeof is_valid, 0);
  if (hipSuccess != hip_err) {
    MMB_ERR("Error: unable to retrieve validity check. (err: %s)\n",
            hipGetErrorString(hip_err));
    *err = 5;
    return stat;
  } else if (1 != is_valid) {
    MMB_ERR("Invalid check. Bad data copy. (is_valid=%d)\n", is_valid);
    *err = 6;
    if (MMB_LOG_DEBUG <= MMB_MAX_LOG_LEVEL) {
      long int local_buff[pitch * (height + 2 * halo)];
      hip_err = hipMemcpyDtoH(local_buff, hip_buff->ptr, sizeof local_buff);
      if (hipSuccess != hip_err) {
        MMB_DEBUG("Error: unable to 2D copy back from device buffer.\n");
        *err = 7;
        return stat;
      } else {
        int line_s = 0;
        const std::size_t max_line = 5 * pitch;
        char line[max_line];
        MMB_DEBUG("Buffer after copy:\n");
        for (std::size_t row = 0; height + 2 * halo > row; ++row) {
          for (std::size_t column = 0; pitch > column; ++column)
            line_s += snprintf(&line[line_s], max_line - line_s, " %2ld%c",
                               local_buff[row*pitch+column],
                               column + 1 == pitch ? '\n' : ',');
          MMB_DEBUG("%s", line);
          line_s = 0;
        }
      }
    }
    return stat;
  }
  stat = MMB_OK;
  return stat;
}

