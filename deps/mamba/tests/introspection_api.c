/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif


#include "cheat.h"
#include "cheats.h"
#include "mmb_cheats.h"

#include <stdlib.h>
#include <stdbool.h>

#include <mamba.h>
#include <mmb_memory.h>
#include <mmb_layout.h>

#include "../memory/i_memory.h"

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

CHEAT_DECLARE(
  mmbOptions *MMB_TESTS_INIT_DEFAULT;
  const mmbMemLayer TESTED_LAYER = MMB_DRAM;
  const mmbExecutionContext TESTED_EX_CON = MMB_CPU;
  const mmbProvider TESTED_PROVIDER = MMB_SYSTEM;
  const mmbStrategy TESTED_STRATEGY = MMB_STRATEGY_NONE;
  const char *DEFAULT_NAME = "default dram interface";
  const char *TESTED_NAME = "dram_interface";
  const mmbAccessType TESTED_ARRAY_TYPE = MMB_READ_WRITE;

  mmbMemSpace *dram_space = NULL;
  mmbMemInterface *dram_interface = NULL;
  mmbLayout *layout = NULL;
  mmbArray *mba = NULL;
  size_t arrdims[1] = {0};
  mmbDimensions dims = {1, arrdims};
)

CHEAT_SET_UP(
  cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT, MAX_LOG_LEVEL));
  // Initialise mamba and register some memory
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
    .interface_opts = { .provider = TESTED_PROVIDER, .strategy = TESTED_STRATEGY, .name = DEFAULT_NAME },
  };
  const mmbMemInterfaceConfig interface_dram_config =
    { .provider = TESTED_PROVIDER, .strategy = TESTED_STRATEGY, .name = TESTED_NAME };
  cheat_assert_int(MMB_OK, mmb_register_memory(TESTED_LAYER, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &dram_space));
  cheat_assert_int(MMB_OK, mmb_create_interface(dram_space, &interface_dram_config, &dram_interface));
  cheat_assert_string(TESTED_NAME, dram_interface->name);
  /* Create empty mmb array */
  cheat_assert_int(MMB_OK, mmb_layout_create_regular_1d(sizeof(int), MMB_PADDING_NONE, &layout));
  cheat_assert_int(MMB_OK, mmb_array_create(&dims, layout, dram_interface, TESTED_ARRAY_TYPE, &mba));
  /* Ensure we got an empty array */
  bool empty = false;
  cheat_assert_int(MMB_OK, mmb_array_is_empty(mba, &empty));
  cheat_assert_int(true, empty);
)

CHEAT_TEAR_DOWN(
  /* Cleanup intermediate objects, cleanup mamba */
  cheat_assert_int(MMB_OK, mmb_array_destroy(mba));
  cheat_assert_int(MMB_OK, mmb_layout_destroy(layout));
  cheat_assert_int(MMB_OK, mmb_finalize());
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

/****************************************************************************
 *
 *  Check Accessors
 *
 * *************************************************************************/

CHEAT_TEST(get_layout,
  /* Check get_layout */
  mmbLayout *layout = NULL;
  cheat_assert_int(MMB_OK, mmb_array_get_layout(mba, &layout));
  cheat_assert_not_pointer(NULL, layout);
  cheat_assert_pointer(mba->layout, layout);
  /* Check error cases in get_layout */
  layout = NULL;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_layout(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_layout(NULL, &layout));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_layout(mba, NULL));
  cheat_assert_pointer(NULL, layout);
)

CHEAT_TEST(get_dimension,
  /* Check get_dimensions */
  mmbDimensions *dimensions = NULL;
  cheat_assert_int(MMB_OK, mmb_array_get_dimensions(mba, &dimensions));
  cheat_assert_not_pointer(NULL, dimensions);
  cheat_assert_pointer(&mba->dims, dimensions);
  /* Check error cases in get_dimensions */
  dimensions = NULL;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_dimensions(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_dimensions(NULL, &dimensions));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_dimensions(mba, NULL));
  cheat_assert_pointer(NULL, dimensions);
)

CHEAT_TEST(copy_dimension,
  /* Check copy_dimensions */
  mmbDimensions dimensions = {0};
  cheat_assert_int(MMB_OK, mmb_array_copy_dimensions(mba, &dimensions));
  cheat_assert_size(dims.size, dimensions.size);
  cheat_assert_size(mba->dims.size, dimensions.size);
  cheat_assert_not_pointer(NULL, dimensions.d);
  cheat_assert_not_pointer(mba->dims.d, dimensions.d);
  for (size_t i=0; dimensions.size>i; ++i) {
    cheat_assert_size(arrdims[i], dimensions.d[i]);
    cheat_assert_size(mba->dims.d[i], dimensions.d[i]);
  }
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_dimensions_resize(&dimensions, 0));
  /* Check error cases in copy_dimensions */
  dimensions = (mmbDimensions) {0};
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_copy_dimensions(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_copy_dimensions(NULL, &dimensions));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_copy_dimensions(mba, NULL));
  cheat_assert_size(0, dimensions.size);
  cheat_assert_pointer(NULL, dimensions.d);
)

CHEAT_TEST(get_space,
  {
  /* Check mmb_meminterface_get_space */
  mmbMemSpace *space = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_get_space(dram_interface, &space));
  cheat_assert_not_pointer(NULL, space);
  cheat_assert_pointer(dram_space, space);
  /* Check error cases in mmb_meminterface_get_space */
  space = NULL;
  cheat_assert_int(MMB_INVALID_INTERFACE, mmb_meminterface_get_space(NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE, mmb_meminterface_get_space(NULL, &space));
  cheat_assert_int(MMB_INVALID_ARG, mmb_meminterface_get_space(dram_interface, NULL));
  cheat_assert_pointer(NULL, space);
  }
  {
  /* Check mmb_array_get_space */
  mmbMemSpace *space = NULL;
  cheat_assert_int(MMB_OK, mmb_array_get_space(mba, &space));
  cheat_assert_not_pointer(NULL, space);
  cheat_assert_pointer(dram_space, space);
  /* Check error cases in mmb_array_get_space */
  space = NULL;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_space(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_space(NULL, &space));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_space(mba, NULL));
  cheat_assert_pointer(NULL, space);
  }
  {
  /* Check mmb_allocation_get_space */
  mmbMemSpace *space = NULL;
  cheat_assert_int(MMB_OK, mmb_allocation_get_space(mba->allocation, &space));
  cheat_assert_not_pointer(NULL, space);
  cheat_assert_pointer(dram_space, space);
  /* Check error cases in mmb_allocation_get_space */
  space = NULL;
  cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_space(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_space(NULL, &space));
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocation_get_space(mba->allocation, NULL));
  cheat_assert_pointer(NULL, space);
    { /* tempering the allocation to check the errors */
    space = NULL;
    mmbMemInterface *old_interface = mba->allocation->interface;
    mba->allocation->interface = NULL;
    cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_space(mba->allocation, &space));
    mba->allocation->interface = old_interface;
    cheat_assert_pointer(NULL, space);
    }
    { /* tempering the interface to check the errors */
    space = NULL;
    mmbMemSpace *old_space = mba->allocation->interface->space;
    mba->allocation->interface->space = NULL;
    cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_space(mba->allocation, &space));
    mba->allocation->interface->space = old_space;
    cheat_assert_pointer(NULL, space);
    }
  }
)

CHEAT_TEST(get_layer,
  {
  /* Check mmb_memspace_get_layer */
  mmbMemLayer layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_OK, mmb_memspace_get_layer(dram_space, &layer));
  cheat_assert_int(TESTED_LAYER, layer);
  /* Check error cases in mmb_memspace_get_layer */
  layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_INVALID_SPACE, mmb_memspace_get_layer(NULL, NULL));
  cheat_assert_int(MMB_INVALID_SPACE, mmb_memspace_get_layer(NULL, &layer));
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_get_layer(dram_space, NULL));
  cheat_assert_int(MMB_MEMLAYER__MAX, layer);
  }
  {
  /* Check mmb_meminterface_get_layer */
  mmbMemLayer layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_OK, mmb_meminterface_get_layer(dram_interface, &layer));
  cheat_assert_int(TESTED_LAYER, layer);
  /* Check error cases in mmb_meminterface_get_layer */
  layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_INVALID_INTERFACE, mmb_meminterface_get_layer(NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE, mmb_meminterface_get_layer(NULL, &layer));
  cheat_assert_int(MMB_INVALID_ARG, mmb_meminterface_get_layer(dram_interface, NULL));
  cheat_assert_int(MMB_MEMLAYER__MAX, layer);
  }
  {
  /* Check mmb_array_get_layer */
  mmbMemLayer layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_OK, mmb_array_get_layer(mba, &layer));
  cheat_assert_int(TESTED_LAYER, layer);
  /* Check error cases in mmb_array_get_layer */
  layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_layer(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_layer(NULL, &layer));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_layer(mba, NULL));
  cheat_assert_int(MMB_MEMLAYER__MAX, layer);
  }
  {
  /* Check mmb_allocation_get_layer */
  mmbMemLayer layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_OK, mmb_allocation_get_layer(mba->allocation, &layer));
  cheat_assert_int(TESTED_LAYER, layer);
  /* Check error cases in mmb_allocation_get_layer */
  layer = MMB_MEMLAYER__MAX;
  cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_layer(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_layer(NULL, &layer));
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocation_get_layer(mba->allocation, NULL));
  cheat_assert_int(MMB_MEMLAYER__MAX, layer);
  }
)

CHEAT_TEST(get_execution_context,
  {
  /* Check mmb_memspace_get_execution_context */
  mmbExecutionContext execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_OK,
                   mmb_memspace_get_execution_context(dram_space,
                                                      &execution_context));
  cheat_assert_int(TESTED_EX_CON, execution_context);
  /* Check error cases in mmb_memspace_get_execution_context */
  execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_INVALID_SPACE,
                   mmb_memspace_get_execution_context(NULL, NULL));
  cheat_assert_int(MMB_INVALID_SPACE,
                   mmb_memspace_get_execution_context(NULL, &execution_context));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_memspace_get_execution_context(dram_space, NULL));
  cheat_assert_int(MMB_EXECUTION_CONTEXT_NONE, execution_context);
  }
  {
  /* Check mmb_meminterface_get_execution_context */
  mmbExecutionContext execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_OK,
                   mmb_meminterface_get_execution_context(dram_interface,
                                                          &execution_context));
  cheat_assert_int(TESTED_EX_CON, execution_context);
  /* Check error cases in mmb_meminterface_get_execution_context */
  execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_execution_context(NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_execution_context(NULL, &execution_context));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_get_execution_context(dram_interface, NULL));
  cheat_assert_int(MMB_EXECUTION_CONTEXT_NONE, execution_context);
  }
  {
  /* Check mmb_array_get_execution_context */
  mmbExecutionContext execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_OK, mmb_array_get_execution_context(mba, &execution_context));
  cheat_assert_int(TESTED_EX_CON, execution_context);
  /* Check error cases in mmb_array_get_execution_context */
  execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_execution_context(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY,
                   mmb_array_get_execution_context(NULL, &execution_context));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_execution_context(mba, NULL));
  cheat_assert_int(MMB_EXECUTION_CONTEXT_NONE, execution_context);
  }
  {
  /* Check mmb_allocation_get_execution_context */
  mmbExecutionContext execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_get_execution_context(mba->allocation,
                                                        &execution_context));
  cheat_assert_int(TESTED_EX_CON, execution_context);
  /* Check error cases in mmb_allocation_get_execution_context */
  execution_context = MMB_EXECUTION_CONTEXT_NONE;
  cheat_assert_int(MMB_INVALID_ALLOCATION,
                   mmb_allocation_get_execution_context(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ALLOCATION,
                   mmb_allocation_get_execution_context(NULL, &execution_context));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_allocation_get_execution_context(mba->allocation, NULL));
  cheat_assert_int(MMB_EXECUTION_CONTEXT_NONE, execution_context);
  }
)

CHEAT_TEST(get_interface,
  {
    /* Check mmb_array_get_interface */
    mmbMemInterface *interface = NULL;
    cheat_assert_int(MMB_OK, mmb_array_get_interface(mba, &interface));
    cheat_assert_not_pointer(NULL, interface);
    cheat_assert_pointer(dram_interface, interface);
    /* Check error cases in mmb_array_get_interface */
    interface = NULL;
    cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_interface(NULL, NULL));
    cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_interface(NULL, &interface));
    cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_interface(mba, NULL));
    cheat_assert_pointer(NULL, interface);
  }
  {
    /* Check mmb_allocation_get_interface */
    mmbMemInterface *interface = NULL;
    cheat_assert_int(MMB_OK,
                     mmb_allocation_get_interface(mba->allocation, &interface));
    cheat_assert_not_pointer(NULL, interface);
    cheat_assert_pointer(dram_interface, interface);
    /* Check error cases in mmb_allocation_get_interface */
    interface = NULL;
    cheat_assert_int(MMB_INVALID_ALLOCATION,
                     mmb_allocation_get_interface(NULL, NULL));
    cheat_assert_int(MMB_INVALID_ALLOCATION,
                     mmb_allocation_get_interface(NULL, &interface));
    cheat_assert_int(MMB_INVALID_ARG,
                     mmb_allocation_get_interface(mba->allocation, NULL));
    cheat_assert_pointer(NULL, interface);
  }
)

CHEAT_TEST(get_provider,
  {
  /* Check mmb_meminterface_get_provider */
  mmbProvider provider = MMB_PROVIDER_ANY;
  cheat_assert_int(MMB_OK, mmb_meminterface_get_provider(dram_interface, &provider));
  cheat_assert_int(TESTED_PROVIDER, provider);
  /* Check error cases in mmb_meminterface_get_provider */
  provider = MMB_PROVIDER_ANY;
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_provider(NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_provider(NULL, &provider));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_get_provider(dram_interface, NULL));
  cheat_assert_int(MMB_PROVIDER_ANY, provider);
  }
  {
  /* Check mmb_array_get_provider */
  mmbProvider provider = MMB_PROVIDER_ANY;
  cheat_assert_int(MMB_OK, mmb_array_get_provider(mba, &provider));
  cheat_assert_int(TESTED_PROVIDER, provider);
  /* Check error cases in mmb_array_get_provider */
  provider = MMB_PROVIDER_ANY;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_provider(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_provider(NULL, &provider));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_provider(mba, NULL));
  cheat_assert_int(MMB_PROVIDER_ANY, provider);
  }
  {
  /* Check mmb_allocation_get_provider */
  mmbProvider provider = MMB_PROVIDER_ANY;
  cheat_assert_int(MMB_OK, mmb_allocation_get_provider(mba->allocation, &provider));
  cheat_assert_int(TESTED_PROVIDER, provider);
  /* Check error cases in mmb_allocation_get_provider */
  provider = MMB_PROVIDER_ANY;
  cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_provider(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ALLOCATION,
                   mmb_allocation_get_provider(NULL, &provider));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_allocation_get_provider(mba->allocation, NULL));
  cheat_assert_int(MMB_PROVIDER_ANY, provider);
  }
)

CHEAT_TEST(get_strategy,
  {
  /* Check mmb_meminterface_get_strategy */
  mmbStrategy strategy = MMB_STRATEGY_ANY;
  cheat_assert_int(MMB_OK, mmb_meminterface_get_strategy(dram_interface, &strategy));
  cheat_assert_int(TESTED_STRATEGY, strategy);
  /* Check error cases in mmb_meminterface_get_strategy */
  strategy = MMB_STRATEGY_ANY;
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_strategy(NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_strategy(NULL, &strategy));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_get_strategy(dram_interface, NULL));
  cheat_assert_int(MMB_STRATEGY_ANY, strategy);
  }
  {
  /* Check mmb_array_get_strategy */
  mmbStrategy strategy = MMB_STRATEGY_ANY;
  cheat_assert_int(MMB_OK, mmb_array_get_strategy(mba, &strategy));
  cheat_assert_int(TESTED_STRATEGY, strategy);
  /* Check error cases in mmb_array_get_strategy */
  strategy = MMB_STRATEGY_ANY;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_strategy(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_strategy(NULL, &strategy));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_strategy(mba, NULL));
  cheat_assert_int(MMB_STRATEGY_ANY, strategy);
  }
  {
  /* Check mmb_allocation_get_strategy */
  mmbStrategy strategy = MMB_STRATEGY_ANY;
  cheat_assert_int(MMB_OK, mmb_allocation_get_strategy(mba->allocation, &strategy));
  cheat_assert_int(TESTED_STRATEGY, strategy);
  /* Check error cases in mmb_allocation_get_strategy */
  strategy = MMB_STRATEGY_ANY;
  cheat_assert_int(MMB_INVALID_ALLOCATION, mmb_allocation_get_strategy(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ALLOCATION,
                   mmb_allocation_get_strategy(NULL, &strategy));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_allocation_get_strategy(mba->allocation, NULL));
  cheat_assert_int(MMB_STRATEGY_ANY, strategy);
  }
)

CHEAT_TEST(get_name,
  {
  /* Check mmb_meminterface_get_name */
  char *name = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_get_name(dram_interface, &name));
  cheat_assert_not_pointer(NULL, name);
  cheat_assert_not_pointer(TESTED_NAME, name);
  cheat_assert_string(TESTED_NAME, name);
  free(name);
  /* Check error cases in mmb_meminterface_get_name */
  name = NULL;
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_name(NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_meminterface_get_name(NULL, &name));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_get_name(dram_interface, NULL));
  cheat_assert_pointer(NULL, name);
  }
)

CHEAT_TEST(get_access_type,
  /* Check mmb_array_get_access_type */
  mmbAccessType access_type = MMB_ACCESSTYPE_MAX;
  cheat_assert_int(MMB_OK, mmb_array_get_access_type(mba, &access_type));
  cheat_assert_int(TESTED_ARRAY_TYPE, access_type);
  /* Check error cases in mmb_array_get_access_type */
  access_type = MMB_ACCESSTYPE_MAX;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_access_type(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_get_access_type(NULL, &access_type));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_get_access_type(mba, NULL));
  cheat_assert_int(MMB_ACCESSTYPE_MAX, access_type);
)

CHEAT_TEST(owns_data_true,
  /* Check for owned allocation */
  /* Check mmb_array_get_owns_data */
  bool data_owned = false;
  cheat_assert_int(MMB_OK, mmb_array_owns_data(mba, &data_owned));
  cheat_assert_int(true, data_owned);
  /* Check error cases in mmb_array_get_owns_data */
  const bool data_owned_prev = data_owned = !data_owned;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_owns_data(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_owns_data(NULL, &data_owned));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_owns_data(mba, NULL));
  cheat_assert_int(data_owned_prev, data_owned);
)

CHEAT_TEST(owns_data_false,
  /* Check for unowned allocation */
  int buffer[0];
  mmbMemSpace *local_space = NULL;
  mmbMemInterface *local_interface = NULL;
  cheat_assert_int(MMB_OK, mmb_register_memory(MMB_DRAM, MMB_CPU, NULL, &local_space));
  cheat_assert_int(MMB_OK, mmb_request_interface(local_space, NULL, &local_interface));
  /* Create empty local mmb_array */
  mmbArray *local_mba = NULL;
  cheat_assert_int(MMB_OK, mmb_array_create_wrapped(buffer, &dims, layout, local_interface, TESTED_ARRAY_TYPE, &local_mba));
  /* Ensure we got an empty array */
  bool empty = false;
  cheat_assert_int(MMB_OK, mmb_array_is_empty(local_mba, &empty));
  cheat_assert_int(true, empty);
  /* Check mmb_array_get_owns_data */
  bool data_owned = true;
  cheat_assert_int(MMB_OK, mmb_array_owns_data(local_mba, &data_owned));
  cheat_assert_int(false, data_owned);
  /* Check error cases in mmb_array_get_owns_data */
  const bool data_owned_prev = data_owned = !data_owned;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_owns_data(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_owns_data(NULL, &data_owned));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_owns_data(local_mba, NULL));
  cheat_assert_int(data_owned_prev, data_owned);
  /* Cleanup local_mba */
  cheat_assert_int(MMB_OK, mmb_array_destroy(local_mba));
)

CHEAT_TEST(is_tiled_true,
  uint32_t local_buffer[12] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
  size_t local_arrdims[1] = { 12 };
  mmbDimensions local_dims = {1, local_arrdims};
  mmbLayout *local_layout = NULL;
  mmbArray *local_mba = NULL;
  /* Create non-empty mmb arrays */
  cheat_assert_int(MMB_OK,
                   mmb_layout_create_regular_1d(sizeof(uint32_t), MMB_PADDING_NONE,
                                                &local_layout));
  cheat_assert_int(MMB_OK,
                   mmb_array_create_wrapped((void *)local_buffer, &local_dims,
                                            local_layout, dram_interface, MMB_READ,
                                            &local_mba));
  mmbArrayTile *tiling = NULL;
  cheat_assert_int(MMB_OK, mmb_array_tile_single(local_mba, &tiling));
  cheat_assert_not_pointer(NULL, tiling);
  /* Check "manually" we are not actually empty */
  cheat_assert_not_pointer(NULL, local_mba->tiling);
  /* Check mmb_array_get_owns_data */
  bool is_tiled = false;
  cheat_assert_int(MMB_OK, mmb_array_is_tiled(local_mba, &is_tiled));
  cheat_assert_int(true, is_tiled);
  /* Clean up the tiling */
  cheat_assert_int(MMB_OK, mmb_array_untile(local_mba));
  cheat_assert_int(MMB_OK, mmb_array_destroy(local_mba));
  cheat_assert_int(MMB_OK, mmb_layout_destroy(local_layout));
)

CHEAT_TEST(is_tiled_false,
  /* Check "manually" we are actually empty */
  cheat_assert_pointer(NULL, mba->tiling);
  /* Check mmb_array_get_owns_data */
  bool is_tiled = true;
  cheat_assert_int(MMB_OK, mmb_array_is_tiled(mba, &is_tiled));
  cheat_assert_int(false, is_tiled);
  /* Check error cases in mmb_array_get_owns_data */
  const bool is_tiled_prev = is_tiled = !is_tiled;
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_is_tiled(NULL, NULL));
  cheat_assert_int(MMB_INVALID_ARRAY, mmb_array_is_tiled(NULL, &is_tiled));
  cheat_assert_int(MMB_INVALID_ARG, mmb_array_is_tiled(mba, NULL));
  cheat_assert_int(is_tiled_prev, is_tiled);
)

CHEAT_TEST(get_options,
    /* Check mmb_allocation_get_interface */
    mmbAllocateOptions opts = { .is_numa_allocation = true };
    cheat_assert_int(MMB_OK, mmb_allocation_get_options(mba->allocation, &opts));
    cheat_assert_bool(false, opts.is_numa_allocation);
    /* Check error cases in mmb_allocation_get_options */
    opts = (mmbAllocateOptions) { .is_numa_allocation = true };
    cheat_assert_int(MMB_INVALID_ALLOCATION,
                     mmb_allocation_get_options(NULL, NULL));
    cheat_assert_int(MMB_INVALID_ALLOCATION,
                     mmb_allocation_get_options(NULL, &opts));
    cheat_assert_int(MMB_INVALID_ARG,
                     mmb_allocation_get_options(mba->allocation, NULL));
    cheat_assert_bool(true, opts.is_numa_allocation);
)
