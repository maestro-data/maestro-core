/*
 * Copyright (C) 2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "cheats.h"

#include <mamba.h>
#include <mmb_options.h>

/* this tests that we can call toplevel API init/finalize  */
CHEAT_TEST(init_default_finalize,
    cheat_assert(MMB_OK == mmb_init(MMB_INIT_DEFAULT));
    cheat_assert(MMB_OK == mmb_finalize());
)

/* Check init on NULL works */
CHEAT_TEST(init_null_finalize,
    cheat_assert(MMB_OK == mmb_init(NULL));
    cheat_assert(MMB_OK == mmb_finalize());
)

/* Check return value on erroneous call to is_initialized */
CHEAT_TEST(isinit_null,
    cheat_assert_not(MMB_OK == mmb_is_initialized(NULL));
    cheat_assert(MMB_INVALID_ARG == mmb_is_initialized(NULL));
)

/* Check value returned by side-effect */
CHEAT_TEST(isinit_init_isinit_finalize_isinit,
    bool is_initialized = false;
    cheat_assert(MMB_OK == mmb_is_initialized(&is_initialized));
    cheat_assert_not(is_initialized);
    cheat_assert(MMB_OK == mmb_init(MMB_INIT_DEFAULT));
    cheat_assert(MMB_OK == mmb_is_initialized(&is_initialized));
    cheat_assert(is_initialized);
    cheat_assert(MMB_OK == mmb_finalize());
    cheat_assert(MMB_OK == mmb_is_initialized(&is_initialized));
    cheat_assert_not(is_initialized);
)

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/* Check changing the the max_logging level */
CHEAT_TEST(init_default_debug_log_finalize,
  mmbOptions *MMB_TESTS_INIT_DEFAULT;
  cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                                       MAX_LOG_LEVEL));
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, mmb_finalize());
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

/* this tests that we can call toplevel API init/finalize  */
CHEAT_TEST(init_finalize_works,
  cheat_assert(MMB_OK == mmb_init(MMB_INIT_DEFAULT));
  cheat_assert(MMB_OK == mmb_finalize());
)
