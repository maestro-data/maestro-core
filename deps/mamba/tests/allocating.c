/*
 * Copyright (C) 2021      Hewlett Packard Entreprise
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif


#include "cheat.h"
#include "cheats.h"
#include "mmb_cheats.h"

#include <stdlib.h>
#include <stdbool.h>

#include <mamba.h>
#include <mmb_memory.h>

#include "../memory/i_memory.h"

#if HAVE_NUMA
#include <numaif.h>
#include "../memory/numa_utils.h"
#endif

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

CHEAT_DECLARE(
  mmbOptions *MMB_TESTS_INIT_DEFAULT;
#if defined(__GNUC__) || defined(__GNUG__)
#define TESTED_LAYER MMB_DRAM
#define TESTED_EX_CON MMB_CPU
#define TESTED_PROVIDER MMB_SYSTEM
#define TESTED_STRATEGY MMB_STRATEGY_NONE
#define TESTED_ARRAY_TYPE MMB_READ_WRITE
#define CONFIG_MEM_SIZE 8000UL
#define big_size (2 * (CONFIG_MEM_SIZE >> 10) * 1024*1024)
#else
  const mmbMemLayer TESTED_LAYER = MMB_DRAM;
  const mmbExecutionContext TESTED_EX_CON = MMB_CPU;
  const mmbProvider TESTED_PROVIDER = MMB_SYSTEM;
  const mmbStrategy TESTED_STRATEGY = MMB_STRATEGY_NONE;
  const mmbAccessType TESTED_ARRAY_TYPE = MMB_READ_WRITE;
  const size_t CONFIG_MEM_SIZE = 8000;
  const size_t big_size = 2 * (CONFIG_MEM_SIZE >> 10) * 1024*1024;
#endif /* defined(__GNUC__) || defined(__GNUG__) */

  mmbMemSpace *dram_space = NULL;
  mmbMemInterface *dram_interface = NULL;
  mmbLayout *layout = NULL;

#if HAVE_NUMA
#if defined(__GNUC__) || defined(__GNUG__)
#define numa_count 2
#else
  const size_t numa_count = 2;
#endif /* defined(__GNUC__) || defined(__GNUG__) */
  uint64_t numa_sizes[numa_count] = { CONFIG_MEM_SIZE >> 10, CONFIG_MEM_SIZE >> 10 };
  mmbMemSpace *dram_space_numa = NULL;
  mmbMemInterface *dram_interface_numa = NULL;
#endif /* HAVE_NUMA */
)

CHEAT_SET_UP(
  cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK,
                   mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                               MAX_LOG_LEVEL));
  // Initialise mamba and register some memory
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  /* Don't use discovery, but force the use of space limited declared memory. */
  /* Size is given in MiB, we don't want as much as 8000MiB, but only 8MiB */
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = CONFIG_MEM_SIZE >> 10 },
    .interface_opts = { .provider = TESTED_PROVIDER, .strategy = TESTED_STRATEGY }
  };
  cheat_assert_int(MMB_OK,
                   mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT,
                                       &dram_config, &dram_space));
  cheat_assert_bool(false, dram_space->is_numa_aware);
  cheat_assert_size(big_size/2, dram_space->max_bytes);
  cheat_assert_int(MMB_OK, mmb_create_interface(dram_space, &dram_config.interface_opts, &dram_interface));
#if HAVE_NUMA
  const mmbMemSpaceConfig dram_config_numa = {
    .size_opts = { .action = MMB_SIZE_SET, .is_numa_aware = true, .numa_sizes = { numa_count, numa_sizes } },
    .interface_opts = { .provider = TESTED_PROVIDER, .strategy = TESTED_STRATEGY }
  };
  cheat_assert_int(MMB_OK,
                   mmb_register_numa_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT,
                                            &dram_config_numa, &dram_space_numa));
  cheat_assert_bool(true, dram_space_numa->is_numa_aware);
  cheat_assert_size(big_size, dram_space_numa->max_bytes);
  cheat_assert_size(big_size/2, dram_space_numa->numa_sizes[0]);
  cheat_assert_size(big_size/2, dram_space_numa->numa_sizes[1]);
  cheat_assert_int(MMB_OK, mmb_create_interface(dram_space_numa, &dram_config_numa.interface_opts, &dram_interface_numa));
#endif /* HAVE_NUMA */
)

CHEAT_TEAR_DOWN(
  /* Cleanup intermediate objects, cleanup mamba */
  cheat_assert_int(MMB_OK, mmb_finalize());
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

CHEAT_TEST(allocate,
  /* Check small allocation */
  size_t size = sizeof(int);
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate(size, dram_interface, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_free(alloc));
  /* Check 4 MiB allocation */
  size = sizeof(int) << 20;
  alloc = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate(size, dram_interface, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_free(alloc));
  /* Check error cases in mmb_allocate */
  alloc = NULL;
  mmbMemInterface local_interface = { 0 };
  cheat_assert_int(MMB_INVALID_SIZE, mmb_allocate(big_size, dram_interface, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE, mmb_allocate(size, NULL, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocate(size, &local_interface, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocate(size, dram_interface, NULL));
)

CHEAT_TEST(allocation_create_wrapped,
  int *buffer = NULL;
  /* Check small allocation */
  int buff_int = 42;
  size_t size = sizeof buff_int;
  buffer = &buff_int;
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped(buffer, size, dram_interface,
                                                 &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(false, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check 4 MiB allocation */
  int buff[1 << 20] = { 0 };
  size = sizeof buff;
  buffer = buff;
  alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped(buffer, size, dram_interface,
                                                 &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(false, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check error cases in mmb_allocation_create_wrapped */
  alloc = NULL;
  mmbMemInterface local_interface = { 0 };
  cheat_assert_int(MMB_INVALID_SIZE,
                   mmb_allocation_create_wrapped(NULL, size, dram_interface,
                                                 &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocation_create_wrapped(buffer, size, NULL, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocation_create_wrapped(buffer, size, &local_interface,
                                                 &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_allocation_create_wrapped(buffer, size, dram_interface,
                                                 NULL));
)

CHEAT_TEST(allocate_opts,
  mmbAllocateOptions opts = { .is_numa_allocation = false };
  /* Check small allocation */
  size_t size = sizeof(int);
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocate_opts(size, dram_interface, &opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check 4 MiB allocation */
  size = sizeof(int) << 20;
  alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocate_opts(size, dram_interface, &opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check error cases in mmb_allocate_opts */
  alloc = NULL;
  mmbMemInterface local_interface = { 0 };
  cheat_assert_int(MMB_INVALID_SIZE,
                   mmb_allocate_opts(big_size, dram_interface, &opts, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocate_opts(size, NULL, &opts, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocate_opts(size, &local_interface, &opts, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_allocate_opts(size, dram_interface, &opts, NULL));
  /* Warning printed. Maybe we should return something? */
  cheat_assert_int(MMB_OK,
                   mmb_allocate_opts(size, dram_interface, NULL, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
)

CHEAT_TEST(allocation_create_wrapped_opts,
  mmbAllocateOptions opts = { .is_numa_allocation = false };
  int *buffer = NULL;
  /* Check small allocation */
  int buff_int = 42;
  size_t size = sizeof buff_int;
  buffer = &buff_int;
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, dram_interface, &opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(false, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check 4 MiB allocation */
  int buff[1 << 20] = { 0 };
  size = sizeof buff;
  buffer = buff;
  alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, dram_interface, &opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(false, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check error cases in mmb_allocation_create_wrapped_opts */
  alloc = NULL;
  mmbMemInterface local_interface = { 0 };
  cheat_assert_int(MMB_INVALID_SIZE,
                   mmb_allocation_create_wrapped_opts(
                     NULL, size, dram_interface, &opts, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, NULL, &opts, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, &local_interface, &opts, &alloc));
  cheat_assert_pointer(NULL, alloc);
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, dram_interface, &opts, NULL));
  /* Warning printed. Maybe we should return something? */
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, dram_interface, NULL, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
)

CHEAT_TEST(allocate_options_create_destroy,
  mmbAllocateOptions *opts = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate_options_create(&opts));
  cheat_assert_not_pointer(NULL, opts);
  cheat_assert_int(MMB_OK, mmb_allocate_options_destroy(opts));
  /* Check error cases in mmb_allocate_options_create */
  mmbAllocateOptions **popts = NULL;
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocate_options_create(NULL));
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocate_options_create(popts));
  /* Check error cases in mmb_allocate_options_destroy */
  opts = NULL;
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocate_options_destroy(NULL));
  cheat_assert_int(MMB_INVALID_ARG, mmb_allocate_options_destroy(opts));
)

CHEAT_TEST(allocate_opts_create,
  mmbAllocateOptions *opts = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate_options_create(&opts));
  cheat_assert_not_pointer(NULL, opts);
  opts->is_numa_allocation = false;
  /* Check small allocation */
  size_t size = sizeof(int);
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocate_opts(size, dram_interface, opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_free(alloc));
  /* Check 4 MiB allocation */
  size = sizeof(int) << 20;
  alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocate_opts(size, dram_interface, opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_free(alloc));
  cheat_assert_int(MMB_OK, mmb_allocate_options_destroy(opts));
)

CHEAT_TEST(allocation_create_wrapped_opts_create,
  mmbAllocateOptions *opts = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate_options_create(&opts));
  cheat_assert_not_pointer(NULL, opts);
  opts->is_numa_allocation = false;
  int *buffer = NULL;
  /* Check small allocation */
  int buff_int = 42;
  size_t size = sizeof buff_int;
  buffer = &buff_int;
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, dram_interface, opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(false, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  /* Check 4 MiB allocation */
  int buff[1 << 20] = { 0 };
  size = sizeof buff;
  buffer = buff;
  alloc = NULL;
  cheat_assert_int(MMB_OK,
                   mmb_allocation_create_wrapped_opts(
                     buffer, size, dram_interface, opts, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface, alloc->interface);
  cheat_assert_bool(false, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_allocation_destroy(alloc));
  cheat_assert_int(MMB_OK, mmb_allocate_options_destroy(opts));
)

#if HAVE_NUMA

CHEAT_TEST(allocate_numa,
  /* Check small allocation */
  size_t size = sizeof(int);
  mmbAllocation *alloc = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate(size, dram_interface_numa, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface_numa, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_free(alloc));
  /* Check 4 MiB allocation */
  size = sizeof(int) << 20;
  alloc = NULL;
  cheat_assert_int(MMB_OK, mmb_allocate(size, dram_interface_numa, &alloc));
  cheat_assert_not_pointer(NULL, alloc);
  cheat_assert_not_pointer(NULL, alloc->ptr);
  cheat_assert_size(size, alloc->n_bytes);
  cheat_assert_pointer(dram_interface_numa, alloc->interface);
  cheat_assert_bool(true, alloc->owned);
  cheat_assert_bool(false, alloc->opts.is_numa_allocation);
  /* Cleanup */
  cheat_assert_int(MMB_OK, mmb_free(alloc));
)

CHEAT_TEST(allocate_opts_numa,
  int numa_node = -1;
  int max_numa_node = -1;
  cheat_assert_int(MMB_OK, mmb_numa_max_node(&max_numa_node));
  for (int target_node = 0; max_numa_node >= target_node; ++target_node) {
    mmbAllocateOptions opts = { .is_numa_allocation = true,
                                .numa = { .node_id = target_node } };
    /* Check small allocation */
    size_t size = sizeof(int);
    mmbAllocation *alloc = NULL;
    cheat_assert_int(MMB_OK,
                     mmb_allocate_opts(size, dram_interface_numa, &opts, &alloc));
    cheat_assert_not_pointer(NULL, alloc);
    cheat_assert_not_pointer(NULL, alloc->ptr);
    cheat_assert_size(size, alloc->n_bytes);
    cheat_assert_pointer(dram_interface_numa, alloc->interface);
    cheat_assert_bool(true, alloc->owned);
    cheat_assert_bool(true, alloc->opts.is_numa_allocation);
    cheat_assert_bool(target_node, alloc->opts.numa.node_id);
    /* Page needs to be actually mapped (hence accessed) before testing the node.
     * Otherwise, the status returned will be -14 (-EFAULT). */
    memset(MMBALLOC2BUF(alloc), 42, size);
    cheat_assert_int(MMB_OK, mmb_numa_get_node_id(&alloc->ptr, 1, &numa_node));
    cheat_assert_int(target_node, numa_node);
    cheat_assert_size((big_size/2)-size, dram_space_numa->numa_sizes[numa_node]);
    cheat_assert_size(size, dram_space_numa->allocated_bytes);
    /* Cleanup */
    cheat_assert_int(MMB_OK, mmb_free(alloc));
    /* Check 4 MiB allocation */
    numa_node = -1;
    size = sizeof(int) << 20;
    alloc = NULL;
    cheat_assert_int(MMB_OK,
                     mmb_allocate_opts(size, dram_interface_numa, &opts, &alloc));
    cheat_assert_not_pointer(NULL, alloc);
    cheat_assert_not_pointer(NULL, alloc->ptr);
    cheat_assert_size(size, alloc->n_bytes);
    cheat_assert_pointer(dram_interface_numa, alloc->interface);
    cheat_assert_bool(true, alloc->owned);
    cheat_assert_bool(true, alloc->opts.is_numa_allocation);
    cheat_assert_bool(target_node, alloc->opts.numa.node_id);
    /* Page needs to be actually mapped (hence accessed) before testing the node.
     * Otherwise, the status returned will be -14 (-EFAULT). */
    memset(MMBALLOC2BUF(alloc), 42, size);
    cheat_assert_int(MMB_OK, mmb_numa_get_node_id(&alloc->ptr, 1, &numa_node));
    cheat_assert_int(target_node, numa_node);
    cheat_assert_size((big_size/2)-size, dram_space_numa->numa_sizes[numa_node]);
    cheat_assert_size(size, dram_space_numa->allocated_bytes);
    /* Cleanup */
    cheat_assert_int(MMB_OK, mmb_free(alloc));
  }
)

#endif /* HAVE_NUMA */
