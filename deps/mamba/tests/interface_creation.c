/*
 * Copyright (C) 2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "cheats.h"
#include "mmb_cheats.h"

#include "../common/i_mmb_hash.h"

#include <limits.h>

#include <mamba.h>
#include <mmb_options.h>
#include <mmb_logging.h>

#include "../memory/memory_options.h"
#include "../memory/i_memory.h"

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

CHEAT_DECLARE(
  mmbOptions *MMB_TESTS_INIT_DEFAULT;
#if defined(__GNUC__) || defined(__GNUG__)
#define TESTED_LAYER MMB_DRAM
#define TESTED_EX_CON MMB_CPU
#define TESTED_PROVIDER MMB_SYSTEM
#define TESTED_STRATEGY MMB_STRATEGY_NONE
#define TESTED_NAME "dram_interface"
#else
  const mmbMemLayer TESTED_LAYER = MMB_DRAM;
  const mmbExecutionContext TESTED_EX_CON = MMB_CPU;
  const mmbProvider TESTED_PROVIDER = MMB_SYSTEM;
  const mmbStrategy TESTED_STRATEGY = MMB_STRATEGY_NONE;
  const char *TESTED_NAME = "dram_interface";
#endif /* defined(__GNUC__) || defined(__GNUG__) */
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
    .interface_opts = { .provider = TESTED_PROVIDER,
                        .strategy = TESTED_STRATEGY,
                        .name = "default dram interface",
                      },
  };
  mmbMemSpace *dram_space = NULL;
)

CHEAT_DECLARE(
  size_t count_interfaces(const mmbMemSpace *space)
  {
    size_t interface_counter;
    mmbMemInterface *interface_it;
    CDL_COUNT2(space->interfaces, interface_it, interface_counter, link.next);
    return interface_counter;
  }
)

CHEAT_SET_UP(
  cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT, MAX_LOG_LEVEL));
  // Initialise mamba and register some memory
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, mmb_register_memory(TESTED_LAYER, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &dram_space));
)

CHEAT_TEAR_DOWN(
  /* Cleanup intermediate objects, cleanup mamba */
  cheat_assert_int(MMB_OK, mmb_finalize());
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

/****************************************************************************
 *
 *  Check interface requests
 *
 * *************************************************************************/


CHEAT_TEST(default_interface,
  mmbMemInterface *dram_interface = NULL;
  mmbMemInterfaceConfig interface_conf = dram_config.interface_opts;
  cheat_assert_size(1, count_interfaces(dram_space));
  cheat_assert_int(MMB_OK, mmb_request_interface(dram_space, &interface_conf, &dram_interface));
  cheat_assert_int(TESTED_PROVIDER, dram_interface->provider);
  cheat_assert_int(TESTED_STRATEGY, dram_interface->strategy);
  cheat_assert_string(interface_conf.name, dram_interface->name);
  cheat_assert_pointer(dram_space, dram_interface->space);
)

CHEAT_TEST(check_errors,
  {
    /* test request */
    mmbMemInterface *dram_interface = NULL;
    mmbMemInterfaceConfig interface_conf = dram_config.interface_opts;
    interface_conf.name = "";
    cheat_assert_int(MMB_INVALID_SPACE, mmb_request_interface(NULL, &interface_conf, &dram_interface));
    cheat_assert_int(MMB_INVALID_ARG, mmb_request_interface(dram_space, &interface_conf, NULL));
    cheat_assert_pointer(NULL, dram_interface);
    cheat_assert_size(1, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
  }
  {
    /* test create */
    mmbMemInterface *dram_interface = NULL;
    mmbMemInterfaceConfig interface_conf = dram_config.interface_opts;
    interface_conf.name = "";
    cheat_assert_int(MMB_INVALID_SPACE, mmb_create_interface(NULL, &interface_conf, &dram_interface));
    cheat_assert_int(MMB_INVALID_ARG, mmb_create_interface(dram_space, &interface_conf, NULL));
    cheat_assert_pointer(NULL, dram_interface);
    cheat_assert_size(1, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
  }
  {
    /* test destroy */
    cheat_assert_int(MMB_INVALID_INTERFACE, mmb_destroy_interface(NULL));
    cheat_assert_size(1, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
  }
)

CHEAT_TEST(anonymous_interface,
  mmbMemInterface *tofree[2];
  {
    /* test creation of new anonymous interface */
    mmbMemInterface *dram_interface = NULL;
    mmbMemInterfaceConfig interface_conf = dram_config.interface_opts;
    interface_conf.name = NULL;
    cheat_assert_int(MMB_OK, mmb_create_interface(dram_space, &interface_conf, &dram_interface));
    cheat_assert_int(TESTED_PROVIDER, dram_interface->provider);
    cheat_assert_int(TESTED_STRATEGY, dram_interface->strategy);
    cheat_assert_pointer(NULL, dram_interface->name);
    cheat_assert_pointer(dram_space, dram_interface->space);
    cheat_assert_size(2, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
    tofree[0] = dram_interface;
  }
  {
    /* test creation of new anonymous interface */
    mmbMemInterface *dram_interface = NULL;
    mmbMemInterfaceConfig interface_conf = dram_config.interface_opts;
    interface_conf.name = "";
    cheat_assert_int(MMB_OK, mmb_create_interface(dram_space, &interface_conf, &dram_interface));
    cheat_assert_int(TESTED_PROVIDER, dram_interface->provider);
    cheat_assert_int(TESTED_STRATEGY, dram_interface->strategy);
    cheat_assert_pointer(NULL, dram_interface->name);
    cheat_assert_pointer(dram_space, dram_interface->space);
    cheat_assert_size(3, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
    tofree[1] = dram_interface;
  }
  /* Freeing interfaces */
  for (size_t i = 0; 2 > i; ++i) {
    cheat_assert_int(MMB_OK, mmb_destroy_interface(tofree[i]));
    cheat_assert_size(3 - (i+1), count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
  }
)

CHEAT_TEST(named_interface,
  mmbMemInterface *dram_interface = NULL;
  mmbMemInterfaceConfig interface_conf = dram_config.interface_opts;
  interface_conf.name = TESTED_NAME;
  cheat_assert_int(MMB_OK, mmb_create_interface(dram_space, &interface_conf, &dram_interface));
  cheat_assert_int(TESTED_PROVIDER, dram_interface->provider);
  cheat_assert_int(TESTED_STRATEGY, dram_interface->strategy);
  cheat_assert_string(TESTED_NAME, dram_interface->name);
  cheat_assert_not_pointer(TESTED_NAME, dram_interface->name);
  cheat_assert_not_pointer(dram_space->default_name, dram_interface->name);
  cheat_assert_pointer(dram_space, dram_interface->space);
  cheat_assert_size(2, count_interfaces(dram_space));
  cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
  {
    /* try to request from hashmap */
    mmbMemInterface *local_dram_interface = NULL;
    mmbMemInterfaceConfig local_interface_conf = interface_conf;
    local_interface_conf.name = TESTED_NAME;
    cheat_assert_int(MMB_OK, mmb_request_interface(dram_space, &local_interface_conf, &local_dram_interface));
    cheat_assert_size(2, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(2, mmb_hash_size(dram_space->named_interfaces));
    cheat_assert_pointer(dram_interface, local_dram_interface);
  }
  {
    /* try to request default from hashmap */
    mmbMemInterface *local_dram_interface = NULL;
    mmbMemInterfaceConfig local_interface_conf = { .name = dram_config.interface_opts.name };
    interface_conf.name = dram_config.interface_opts.name;
    cheat_assert_int(MMB_OK, mmb_request_interface(dram_space, &local_interface_conf, &local_dram_interface));
    cheat_assert_size(2, count_interfaces(dram_space));
    cheat_assert_size(dram_space->num_interfaces, count_interfaces(dram_space));
    cheat_assert_mmb_hash_int32(2, mmb_hash_size(dram_space->named_interfaces));
    cheat_assert_int(dram_config.interface_opts.provider,
                     local_dram_interface->provider);
    cheat_assert_int(dram_config.interface_opts.strategy,
                     local_dram_interface->strategy);
    cheat_assert_string(dram_config.interface_opts.name,
                        local_dram_interface->name);
    cheat_assert_pointer(dram_space, local_dram_interface->space);
  }
  /* Freeing interfaces */
  cheat_assert_int(MMB_OK, mmb_destroy_interface(dram_interface));
  cheat_assert_size(1, count_interfaces(dram_space));
  cheat_assert_mmb_hash_int32(1, mmb_hash_size(dram_space->named_interfaces));
)
