/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#include <mamba.h>
#include <mmb_tile.h>
#include <mmb_tile_iterator.h>
#include <mmb_memory.h>
#include <mmb_logging.h>
 
void init_matrix_buffers(float* buf_a, float* buf_b, const size_t M, const size_t N, int b_is_identity);
int  check_result(float* buf_a, float* buf_b, float* buf_c, const size_t M, const size_t N);
int  check_result_identity(float* buf_a, float* buf_c, const size_t M, const size_t N);
void print_matrices(float* buf_a, float* buf_b, float* buf_c, const size_t M, const size_t N);

int main(int argc, char *const argv[])
{
  /* 2D MxN tiled matrix multiplication: C = A * B */
  /* Only M == N supported so far... */
  size_t array_size_M = 16;
  size_t array_size_N = 16;
  size_t tile_size_M = 4;
  size_t tile_size_N = 4;
  int verbose = 0;
  int b_is_identity = 0;

/* Parse command line arguments, detailed in README */
  int c;
  optarg = 0;
  while ((c = getopt(argc, argv, "vm:t:hi")) != -1)
    switch (c) {
    case 'v':
      verbose = 1;
      break;
    case 'm':
      array_size_M = (size_t)atoi(optarg);
      array_size_N = array_size_M;
      break;
    case 't':
      tile_size_M = (size_t)atoi(optarg);
      tile_size_N = tile_size_M;
      break;
    case 'h':
      printf("Usage (all args optional): ./matrix_multiply -v (for verbose mode) -t N "
             "(for tile size NxN) -m N (for matrix size NxN) -i (use identity for matrix B)\n");
      return 0;
      break;
    case 'i':
      b_is_identity = 1;
      break;
    case '?':
      if (optopt == 'c')
        MMB_ERR("Option -%c requires an argument.\n", optopt);
      else if (isprint(optopt))
        MMB_ERR("Unknown option `-%c'.\n", optopt);
      else
        MMB_ERR("Unknown option character `\\x%x'.\n", optopt);

      printf("Usage: ./matrix_multiply -v (for verbose mode) -t N "
             "(for tile size NxN) -m N (for matrix size NxN) -i (use identity for matrix B)\n");
      return 1;
    default:
      abort();
    }

  // Allocate 2D (flattened) C-array for arrays A, B, and C
  size_t bytes = sizeof(float)*array_size_M*array_size_N;
  float* buffer_a = (float*)malloc(bytes);
  float* buffer_b = (float*)malloc(bytes);
  float* buffer_c = (float*)malloc(bytes);

  init_matrix_buffers(buffer_a, buffer_b, array_size_M, 
                      array_size_N, b_is_identity);

  /* Initialize Mamba and enable debug logging */
  mmb_init(MMB_INIT_DEFAULT);
  mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  
  // Memory configuration options
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };

  mmbMemSpace *dram_space;
  int discovery_enabled = 0;
  mmb_discovery_is_enabled(&discovery_enabled);
  printf("Discovery enabled: %d\n", discovery_enabled);
  fflush(0);
  if (!discovery_enabled)
    mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &dram_space);
  else
    mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &dram_space);

  mmbMemInterface *dram_interface;
  mmb_request_interface(dram_space, NULL, &dram_interface);

  // Create mmb arrays for each matrix
  mmbArray *mba_a, *mba_b, *mba_c;
  mmbLayout *layout;
  size_t arrdims[2] = {array_size_M, array_size_N};
  mmbDimensions dims = {2, arrdims};
  mmb_layout_create_regular_nd(sizeof(float), 2, MMB_COLMAJOR, MMB_PADDING_NONE, &layout);
  mmb_array_create_wrapped(buffer_a, &dims, layout, dram_interface, MMB_READ, &mba_a);
  mmb_array_create_wrapped(buffer_b, &dims, layout, dram_interface, MMB_READ, &mba_b);
  mmb_array_create_wrapped(buffer_c, &dims, layout, dram_interface, MMB_READ_WRITE, &mba_c);
  mmb_layout_destroy(layout);


  // Initialise mba_c
  // Create a tiling that covers whole array with single tile
  mmb_array_tile(mba_c, &dims);

  // Ask for the next (only) tile
  mmbTileIterator* it_c;
  mmb_tile_iterator_create(mba_c, &it_c); 
  mmbArrayTile* tile;
  mmb_tile_iterator_first(it_c, &tile);

  // Initialize to 0
  for(size_t i = tile->lower[0]; i < tile->upper[0]; i++)
    for(size_t j = tile->lower[1]; j < tile->upper[1]; j++){
      MMB_IDX_2D(tile, i, j, float) = 0;
    }

  // Free the iterator we were using
  mmb_tile_iterator_destroy(it_c);

  // Request tiling of all arrays with chunkx chunky
  size_t chunkdims[2] = {tile_size_M,tile_size_N};
  mmbDimensions chunks = {2, chunkdims};
  mmb_array_tile(mba_a, &chunks);
  mmb_array_tile(mba_b, &chunks);
  mmb_array_tile(mba_c, &chunks);

  /* 
     Loop over tiles using iterators
     TODO: Need to define the concept of iterator schedules to do this
     ...
  */

  /* Loop over tiles using standard indexing */
  mmbArrayTile *tile_a, *tile_b, *tile_c;
  mmbIndex *idx_a, *idx_b, *idx_c;
  mmb_index_create(2, &idx_a);
  mmb_index_create(2, &idx_b);
  mmb_index_create(2, &idx_c);
  mmbDimensions *tiling_dims;
  mmb_tiling_dimensions(mba_a, &tiling_dims);

  /* Tiling loops */
  for(size_t ti = 0; ti < tiling_dims->d[0]; ti++) {
    for(size_t tj = 0; tj < tiling_dims->d[1]; tj++) {
      /* Set tile indices for c array */
      mmb_index_set(idx_c,ti,tj);
      mmb_tile_at(mba_c, idx_c, &tile_c);
      for(size_t tk = 0; tk < tiling_dims->d[1]; tk++) {
        /* Set tile indices for a & b arrays */
        mmb_index_set(idx_a,ti,tk);
        mmb_index_set(idx_b,tk,tj);
        /* Extract tile for each array */
        mmb_tile_at(mba_a, idx_a, &tile_a);
        mmb_tile_at(mba_b, idx_b, &tile_b);

        /* Element loops for single tile matrix multiply */
        for(size_t ei = 0; ei < tile_c->dim[0]; ei++) {
          for(size_t ej = 0; ej < tile_c->dim[1]; ej++) {
            for(size_t ek = 0; ek < tile_a->dim[1]; ek++) {
              MMB_IDX_2D_NORM(tile_c, ei, ej, float) += 
                MMB_IDX_2D_NORM(tile_a, ei, ek, float) * 
                MMB_IDX_2D_NORM(tile_b, ek, ej, float);
            }
          }
        }
      }
    }
  }

  /* Cleanup mamba */
  mmb_dimensions_destroy(tiling_dims);
  mmb_index_destroy(idx_a);
  mmb_index_destroy(idx_b);
  mmb_index_destroy(idx_c);
  mmb_array_destroy(mba_a);
  mmb_array_destroy(mba_b);
  mmb_array_destroy(mba_c);
  mmb_finalize();
  
  /* Check results */
  if(verbose)
    print_matrices(buffer_a, buffer_b, buffer_c, array_size_M, array_size_N);

  int success;
  if (b_is_identity) {
    success = check_result_identity(buffer_a, buffer_c, array_size_M, array_size_N);
  } else {
    success = check_result(buffer_a, buffer_b, buffer_c, array_size_M, array_size_N);
  }
  printf("Checked matrix multiply with buffer sizes (%zu,%zu) and tile sizes (%zu,%zu): ",
            array_size_M, array_size_N, tile_size_M, tile_size_N);
  if(success)
    printf("success.\n");
  else 
    printf("fail.\n");

  free(buffer_a);
  free(buffer_b);
  free(buffer_c);

  return success ? EXIT_SUCCESS : EXIT_FAILURE;
}

void init_matrix_buffers(float* buf_a, float* buf_b, 
                         const size_t M, const size_t N, int b_is_identity) {
  srand((unsigned int)time(NULL));
  for(unsigned i = 0; i < M ; i++){
    for(unsigned j = 0; j < N ; j++){
      buf_a[i*N+j] = rand()%10;
      if(b_is_identity) 
        buf_b[i*N+j] = (i==j) ? 1 : 0;
      else
        buf_b[i*N+j] = rand()%10;
    }
  }
}

int check_result(float* buf_a, float* buf_b, float* buf_c, 
                  const size_t M, const size_t N) {
  int success = 1;
  float *check_buffer = (float*)malloc(sizeof(float)*M*N);
  for(size_t i = 0; i < M; i++) {
    for(size_t j = 0; j < N; j++) {
      check_buffer[i * N + j] = 0;
      for(size_t k = 0; k < N; k++) {
        check_buffer[i * N + j] += buf_a[i * N + k] * 
                                  buf_b[k * N + j];
      }
    }
  }

  for(size_t i = 0; i < M && success; i++){
    for(size_t j = 0; j < N && success; j++){
      if(buf_c[i*N+j] != check_buffer[i*N+j]){
        printf("First error:\n\tcheck_buffer[%zu,%zu]: %f\n\tbuf_c[%zu,%zu]: %f\n",
               i, j,check_buffer[i*N+j], i, j, buf_c[i*N+j]);
        success = 0;
        break;
      }      
    }
  }
    
  free(check_buffer);
  return success;
}

int check_result_identity(float* buf_a, float* buf_c,
                          const size_t M, const size_t N) {
  int success = 1;
  for(size_t i = 0; i < M && success; i++){
    for(size_t j = 0; j < N && success; j++){
      if(buf_c[i*N+j] != buf_a[i*N+j]){
        printf("First error:\n\tbuf_a[%zu,%zu]: %f\n\tbuf_c[%zu,%zu]: %f\n",
               i, j, buf_a[i*N+j], i, j, buf_c[i*N+j]);
        success = 0;
      }
    }
  }
  return success;
}

void print_matrices(float* buf_a, float* buf_b, float* buf_c,
                    const size_t M, const size_t N) {
  printf("\nMatrix A: \n");
  for(size_t i = 0; i < M ; i++){
    for(size_t j = 0; j < N ; j++){
      printf("%2d ", (int)buf_a[i*N+j]); 
    }
    printf("\n"); 
  }

  printf("\nMatrix B: \n");
  for(size_t i = 0; i < M ; i++){
    for(size_t j = 0; j < N ; j++){
      printf("%2d ", (int)buf_b[i*N+j]); 
    }
    printf("\n"); 
  }

  printf("\nMatrix C: \n");
  for(size_t i = 0; i < M ; i++){
    for(size_t j = 0; j < N ; j++){
      printf("%4d ", (int)buf_c[i*N+j]); 
    }
    printf("\n"); 
  }
  printf("\n"); 
}

