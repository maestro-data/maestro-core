/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <mamba.h>
#include <mmb_logging.h>

/* Function signature must look like this */
void my_logging_function(int level, const char *func, const char *file, \
                                    int line, const char *fmtstring, ...);

int main()
{
  mmbError stat = MMB_OK;

  /* MMB_* log macros may be used prior to Mamba initialisation, however will not use user 
   * supplied logging function until Mamba is initialised. Additionally, if an error occurs
   * during mamba init prior to log initialisation, this will not use the user log function either.
   * There are only a few places where this may happen, e.g. error during init options construction, 
   * or error during logging initialisation */

  mmbOptions *init_opts;
  stat = mmb_options_create_default(&init_opts);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create default options for mamba\n");
    goto BAILOUT;
  }

  stat = mmb_options_set_user_log_func(init_opts, &my_logging_function);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to set debug option for mamba\n");
    goto BAILOUT;
  }

  stat = mmb_init(init_opts);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialise mamba\n");
    goto BAILOUT;
  }

  /* Users can use the MMB_* logging macros like so*/
  /* Note that the compile time logging cut off MMB_MAX_LOG_LEVEL also applies to 
   * the user logging callback. By default, Mamba is compiled with MAX_LOGGING_LEVEL=MMB_LOG_DEBUG 
   * To see NOISE output, or further limit output, set MMB_MAX_LOG_LEVEL at compile-time */
  MMB_NOISE("NOISE logging function with arg: %d\n", 1);
  MMB_DEBUG("DEBUG logging function with arg: %d\n", 2);
  MMB_INFO("INFO logging function with arg: %d\n", 3);
  MMB_WARN("WARN logging function with arg: %d\n", 4);
  MMB_ERR("ERR logging function with arg: %d\n", 5);

  stat = mmb_finalize();
  if(stat != MMB_OK) {
    MMB_ERR("Failed to finalise mamba\n");
    goto BAILOUT;
  }

  stat = mmb_options_destroy(init_opts);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to destroy options for mamba\n");
    goto BAILOUT;
  }

BAILOUT:
    return EXIT_SUCCESS;
}

/* A user-defined logging function, the signature must match this. */
void my_logging_function(int level, const char *func, const char *file, \
                                    int line, const char *fmtstring, ...) {
    va_list ap;
    va_start(ap, fmtstring);
    char header[1024] = "";
    snprintf(&header[0], 1024, "User supplied log function msg: [%s] (%ld): %s(%s:%d) %s",
             mmb_log_level_to_str(level), (long)getpid(), func,
             file, line, fmtstring);
    vfprintf(stdout, header, ap);
    va_end(ap);                               
}

/* To bridge to another variadic logging function, it must accept a va_list 
 * and so should look something like this */
void my_logging_bridge(int level, const char *func, const char *file, \
                                    int line, const char *fmtstring, ...) {
    /* For the example, we just bridge back to the mmb logger */
    va_list ap;
    va_start(ap, fmtstring);
    mmb_vlocation_aware_log(level, func, file, line, fmtstring, ap);
    va_end(ap);                               
}
