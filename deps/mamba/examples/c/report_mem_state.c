/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <mamba.h>
#include <mmb_logging.h>

int main(void)
{
  mmbError stat = MMB_OK;

  /* Initialize Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialise mamba\n");
    goto BAILOUT;
  }
  
  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to set debug option for mamba\n");
    goto BAILOUT;
  }

  stat = mmb_dump_memory_state(stdout);
  if(MMB_OK != stat) {
    MMB_ERR("Failed to dump memory report\n");
    goto BAILOUT;
  }

  stat = mmb_finalize();
  if(MMB_OK != stat) {
    MMB_ERR("Failed to finalise mamba library\n");
    goto BAILOUT;
  }

BAILOUT:
  return MMB_OK == stat ? EXIT_SUCCESS : EXIT_FAILURE;
}
