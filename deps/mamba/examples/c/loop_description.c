/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <mamba.h>
#include <mmb_logging.h>
#include <mmb_loop.h>

int main()
{
  mmbError stat = MMB_OK;

  // Matrix multiply, with scalar multiplier alpha
  // C = alpha * A * B
  int M = 100;
  int N = 100;
  int alpha = 1;
  float *array_A = malloc(sizeof(float) * M * N);
  float *array_B = malloc(sizeof(float) * M * N);
  float *array_C = malloc(sizeof(float) * M * N);


  /* Initialize Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  CHECK_STATUS(stat, "Failed to initialise mamba\n", BAILOUT);

  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  CHECK_STATUS(stat, "Failed to turn on debug logging for mamba\n", BAILOUT);

  // Create data references for A, B, C, alpha
  mmbDataRef *ref_A, *ref_B, *ref_C;
  mmbDataRef *ref_alpha;
  stat = mmb_data_ref_create_array(array_A, 2, sizeof(float), "A", &ref_A);
  CHECK_STATUS(stat, "Failed to create array A data ref\n", BAILOUT);
  stat = mmb_data_ref_create_array(array_B, 2, sizeof(float), "B", &ref_B);
  CHECK_STATUS(stat, "Failed to create array B data ref\n", BAILOUT);
  stat = mmb_data_ref_create_array(array_C, 2, sizeof(float), "C", &ref_C);
  CHECK_STATUS(stat, "Failed to create array C data ref\n", BAILOUT);
  stat = mmb_data_ref_create_scalar(&alpha, sizeof(int), "alpha", &ref_alpha);
  CHECK_STATUS(stat, "Failed to create integer scalar data ref alpha\n", BAILOUT);
  
  // Create loop nest object
  mmbLoopNest *loops;
  stat = mmb_loop_nest_create(&loops);
  CHECK_STATUS(stat, "Failed to create mmbLoopNest object\n", BAILOUT);

  // Add outer loop
  mmbLoopId *outer;
  stat = mmb_loop_nest_add_loop(loops, &outer);
  CHECK_STATUS(stat, "Failed to add outer loop to loop nest\n", BAILOUT);

  // Create some reuseable expressions
  mmbExpr *zero, *one, *i, *j, *k;
  stat = mmb_expr_create_int(0,&zero);
  CHECK_STATUS(stat, "Failed to create integer expr\n", BAILOUT);
  stat = mmb_expr_create_int(1,&one);
  CHECK_STATUS(stat, "Failed to create integer expr\n", BAILOUT);
  stat = mmb_expr_create_iterator("i",&i);
  CHECK_STATUS(stat, "Failed to create iterator expr i\n", BAILOUT);
  stat = mmb_expr_create_iterator("j",&j);
  CHECK_STATUS(stat, "Failed to create iterator expr j\n", BAILOUT);
  stat = mmb_expr_create_iterator("k",&k);
  CHECK_STATUS(stat, "Failed to create iterator expr k\n", BAILOUT);


  // Set init, condition, and increment expressions for outer loop
  mmbExpr *init;
  stat = mmb_expr_create_binary_decl_op(MMB_OP_ASSIGN, i, zero, &init);
  CHECK_STATUS(stat, "Failed to create assignment expr\n", BAILOUT);
  stat = mmb_loop_set_loop_init(outer, init);
  CHECK_STATUS(stat, "Failed to set loop init expr\n", BAILOUT);

  mmbExpr *cond;
  mmbExpr *expr_M;
  stat = mmb_expr_create_const_parameter("M", &expr_M);
  CHECK_STATUS(stat, "Failed to create const parameter expr\n", BAILOUT);
  stat = mmb_expr_create_binary_op(MMB_OP_LT, i, expr_M, &cond);
  CHECK_STATUS(stat, "Failed to create LT op expr\n", BAILOUT);
  stat = mmb_loop_set_loop_cond(outer, cond);
  CHECK_STATUS(stat, "Failed to set loop cond expr\n", BAILOUT);

  mmbExpr *inc;
  stat = mmb_expr_create_unary_op(MMB_OP_PRE_INC, i, &inc);
  CHECK_STATUS(stat, "Failed to create unary pre inc expr\n", BAILOUT);
  stat = mmb_loop_set_loop_inc(outer, inc);
  CHECK_STATUS(stat, "Failed to set loop inc expr\n", BAILOUT);

  // Create first inner loop of matmul within outer
  mmbLoopId *middle;
  stat = mmb_loop_add_inner_loop(outer, &middle);
  CHECK_STATUS(stat, "Failed to add inner loop (middle)\n", BAILOUT);

   // Set init, condition, and increment expressions for middle loop
  mmbExpr *init_middle;
  stat = mmb_expr_create_binary_decl_op(MMB_OP_ASSIGN,j,zero,&init_middle);
  CHECK_STATUS(stat, "Failed to create assign expr\n", BAILOUT);
  stat = mmb_loop_set_loop_init(middle, init_middle);
  CHECK_STATUS(stat, "Failed to set loop init expr\n", BAILOUT);

  mmbExpr *cond_middle;
  mmbExpr *expr_N;
  stat = mmb_expr_create_const_parameter("N", &expr_N);
  CHECK_STATUS(stat, "Failed to create const parameter N expr\n", BAILOUT);
  stat = mmb_expr_create_binary_op(MMB_OP_LT, j, expr_N, &cond_middle);
  CHECK_STATUS(stat, "Failed to create LT expr\n", BAILOUT);
  stat = mmb_loop_set_loop_cond(middle, cond_middle);
  CHECK_STATUS(stat, "Failed to set loop cond expr\n", BAILOUT);

  mmbExpr *inc_middle;
  CHECK_STATUS(stat, "Failed to copy integer expr\n", BAILOUT);
  stat = mmb_expr_create_unary_op(MMB_OP_PRE_INC, j, &inc_middle);
  CHECK_STATUS(stat, "Failed to create unary pre inc expr\n", BAILOUT);
  stat = mmb_loop_set_loop_inc(middle, inc_middle);
  CHECK_STATUS(stat, "Failed to set loop inc expr\n", BAILOUT);

  // Create reusabe [i][j] access expr
  mmbExpr * i_j_access;
  stat = mmb_expr_create_access(i, &i_j_access);
  CHECK_STATUS(stat, "Failed to create i j inner access\n", BAILOUT);
  stat = mmb_expr_extend_access_dim(i_j_access, j);

  // Add write access statement to C[i][j]  in middle loop
  stat = mmb_loop_add_access(middle, ref_C, i_j_access, MMB_EXPR_ACCESS_WRITE);
  CHECK_STATUS(stat, "Failed to add C[i][j] access\n", BAILOUT);

  // Create innermost loop for multiply
  mmbLoopId *inner;
  stat = mmb_loop_add_inner_loop(middle, &inner);
  CHECK_STATUS(stat, "Failed to add inner loop (inner)\n", BAILOUT);

  // Set init, condition, and increment expressions for innermost loop
  mmbExpr *init_inner;
  stat = mmb_expr_create_binary_decl_op(MMB_OP_ASSIGN,k,zero,&init_inner);
  CHECK_STATUS(stat, "Failed to create assignment expr\n", BAILOUT);
  stat = mmb_loop_set_loop_init(inner, init_inner);
  CHECK_STATUS(stat, "Failed to set loop init expr\n", BAILOUT);

  mmbExpr *cond_inner;
  stat = mmb_expr_create_binary_op(MMB_OP_LT, k, expr_M, &cond_inner);
  CHECK_STATUS(stat, "Failed to create LT op expr\n", BAILOUT);
  stat = mmb_loop_set_loop_cond(inner, cond_inner);
  CHECK_STATUS(stat, "Failed to set loop cond expr\n", BAILOUT);

  mmbExpr *inc_inner;
  stat = mmb_expr_create_unary_op(MMB_OP_PRE_INC, k, &inc_inner);
  CHECK_STATUS(stat, "Failed to create PRE_INC expr\n", BAILOUT);
  stat = mmb_loop_set_loop_inc(inner, inc_inner);
  CHECK_STATUS(stat, "Failed to set loop inc expr\n", BAILOUT);

  // Add read access statement to C in inner
  stat = mmb_loop_add_access(inner, ref_C, i_j_access, MMB_EXPR_ACCESS_READ);
  CHECK_STATUS(stat, "Failed to add C[i][j] read access\n", BAILOUT);

  stat = mmb_loop_add_access(inner, ref_alpha, NULL, MMB_EXPR_ACCESS_READ);
  CHECK_STATUS(stat, "Failed to add ref_alpha scalar access\n", BAILOUT);

  mmbExpr * i_k_access;
  stat = mmb_expr_create_access(i, &i_k_access);
  CHECK_STATUS(stat, "Failed to create i k inner access\n", BAILOUT);
  stat = mmb_expr_extend_access_dim(i_k_access, k);
  // Add read access statement to A in inner
  stat = mmb_loop_add_access(inner, ref_A, i_k_access, MMB_EXPR_ACCESS_READ);
  CHECK_STATUS(stat, "Failed to add A[i][k] read access\n", BAILOUT);

  mmbExpr * k_j_access;
  stat = mmb_expr_create_access(k, &k_j_access);
  CHECK_STATUS(stat, "Failed to create k j inner access\n", BAILOUT);
  stat = mmb_expr_extend_access_dim(k_j_access, j);
  // Add read access statement to B in inner
  stat = mmb_loop_add_access(inner, ref_B, k_j_access, MMB_EXPR_ACCESS_READ);
  CHECK_STATUS(stat, "Failed to add B[k][j] read access\n", BAILOUT);

  // Add write access statement to C in inner
  stat = mmb_loop_add_access(inner, ref_C, i_j_access, MMB_EXPR_ACCESS_WRITE);
  CHECK_STATUS(stat, "Failed to add C[i][j] write access\n", BAILOUT);

  stat = mmb_loop_nest_print(loops);
  CHECK_STATUS(stat, "Failed to print loop nest\n", BAILOUT);
 

  stat = mmb_loop_nest_compute(loops);
  CHECK_STATUS(stat, "Failed to run loop nest compute\n", BAILOUT);

  // Clean up reuseable exprs
  stat = mmb_expr_destroy(i);
  CHECK_STATUS(stat, "Failed to destroy i iterator mmbExpr\n", BAILOUT);
  stat = mmb_expr_destroy(j);
  CHECK_STATUS(stat, "Failed to destroy j iterator mmbExpr\n", BAILOUT);
  stat = mmb_expr_destroy(k);
  CHECK_STATUS(stat, "Failed to destroy k iterator mmbExpr\n", BAILOUT);

  stat = mmb_expr_destroy(expr_M);
  CHECK_STATUS(stat, "Failed to destroy M const par mmbExpr\n", BAILOUT);
  stat = mmb_expr_destroy(expr_N);
  CHECK_STATUS(stat, "Failed to destroy N const par mmbExpr\n", BAILOUT);

  stat = mmb_expr_destroy(i_j_access);
  CHECK_STATUS(stat, "Failed to destroy i j access mmbExpr\n", BAILOUT);
  stat = mmb_expr_destroy(i_k_access);
  CHECK_STATUS(stat, "Failed to destroy i k access mmbExpr\n", BAILOUT);
  stat = mmb_expr_destroy(k_j_access);
  CHECK_STATUS(stat, "Failed to destroy k j access mmbExpr\n", BAILOUT);

  stat = mmb_expr_destroy(one);
  CHECK_STATUS(stat, "Failed to destroy integer 1 mmbExpr\n", BAILOUT);
  stat = mmb_expr_destroy(zero);
  CHECK_STATUS(stat, "Failed to destroy integer 0 mmbExpr\n", BAILOUT);

  /// Destroy data refs
  stat = mmb_data_ref_destroy(ref_A);
  CHECK_STATUS(stat, "Failed to destroy data ref A\n", BAILOUT);
  stat = mmb_data_ref_destroy(ref_B);
  CHECK_STATUS(stat, "Failed to destroy data ref B\n", BAILOUT);
  stat = mmb_data_ref_destroy(ref_C);
  CHECK_STATUS(stat, "Failed to destroy data ref C\n", BAILOUT);
  stat = mmb_data_ref_destroy(ref_alpha);
  CHECK_STATUS(stat, "Failed to destroy data ref alpha\n", BAILOUT);

  // Clean up loop nest
  // This will destroy every object referenced by the loop nest
  // Including data references...
  stat = mmb_loop_nest_destroy(loops);
  CHECK_STATUS(stat, "Failed to destroy mmbLoopNest object\n", BAILOUT);

  free(array_A);
  free(array_B);
  free(array_C);

  stat = mmb_finalize();
  CHECK_STATUS(stat, "Failed to finalise mamba library\n", BAILOUT);

  MMB_DEBUG("loop_description completed successfully\n");

BAILOUT:
  return 0;
}


