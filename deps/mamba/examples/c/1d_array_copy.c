/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <mamba.h>
#include <mmb_tile.h>
#include <mmb_tile_iterator.h>
#include <mmb_memory.h>
#include <mmb_logging.h>

mmbError mmb_array_cmp(mmbArray *mba0, mmbArray *mba1, bool *result);
mmbError mmb_tile_cmp(mmbArrayTile *tile0, mmbArrayTile *tile1, bool *result);

int main()
{
  // User allocates 1D C-array
  size_t M = 128;
  mmbError stat = MMB_OK;

  /* Initialize Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialise mamba\n");
    goto BAILOUT;
  }
  
  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to set debug option for mamba\n");
    goto BAILOUT;
  }

  // Memory configuration options
  mmbMemSpace* dram_space;
  int discovery_enabled = 0;
  stat = mmb_discovery_is_enabled(&discovery_enabled);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to query discovery status\n");
    goto BAILOUT;
  }
  if (!discovery_enabled) {
    const mmbMemSpaceConfig dram_config = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };
    stat = mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, NULL);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to register dram memory for mamba\n");
      goto BAILOUT;
    }
  }
  stat = mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to retreive suitable dram memory\n");
    goto BAILOUT;
  }

  mmbMemInterface *dram_interface;
  stat = mmb_request_interface(dram_space, NULL, &dram_interface);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to retreive suitable dram memory\n");
    goto BAILOUT;
  }

  // Create mmb array 
  mmbArray* mba0;
  mmbLayout *layout;
  mmbDimensions *dims;
  stat = mmb_dimensions_create_fill(1, &M, &dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create dimensions object\n");
    goto BAILOUT;
  }

  stat = mmb_layout_create_regular_1d(sizeof(float), MMB_PADDING_NONE, &layout);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create regular 1d layout\n");
    goto BAILOUT;
  }

  stat = mmb_array_create(dims, layout, dram_interface, MMB_READ_WRITE, &mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mamba array\n");
    goto BAILOUT;
  }

  // Create a tiling that covers whole array with single tile
  stat = mmb_array_tile(mba0, dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (single tile)\n");
    goto BAILOUT;
  }

  // Ask for the next (only) tile
  mmbTileIterator* it;
  stat = mmb_tile_iterator_create(mba0, &it); 
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }
  mmbArrayTile* tile;
  stat = mmb_tile_iterator_first(it, &tile);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }

  // User reads/generates data
  time_t t;
  srand((unsigned) time(&t));
  for(size_t i = tile->lower[0]; i < tile->upper[0]; i++) {
    MMB_IDX_1D(tile, i, float) = (double)rand()/(double)(RAND_MAX);
  }

  // Free the iterator we were using
  stat = mmb_tile_iterator_destroy(it);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }

  stat = mmb_array_untile(mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to untile mamba array\n");
    goto BAILOUT;
  } 

  // Copy to new array with same layout
  mmbArray* mba1;
  stat = mmb_array_create(dims, layout, dram_interface, MMB_READ_WRITE, &mba1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mamba array\n");
    goto BAILOUT;
  }
  stat = mmb_layout_destroy(layout);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free layout\n");
    goto BAILOUT;
  }

  stat = mmb_array_copy(mba1, mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy mamba arrays\n");
    goto BAILOUT;
  }

  bool result = 0;
  stat = mmb_array_cmp(mba0, mba1, &result);

  stat = mmb_array_destroy(mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free mamba array\n");
    goto BAILOUT;
  }

  stat = mmb_array_destroy(mba1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free mamba array\n");
    goto BAILOUT;
  }

  stat = mmb_dimensions_destroy(dims);
  if (stat != MMB_OK) {
    MMB_ERR("Failed to destroy the dimensions object.\n");
    goto BAILOUT;
  }

  stat = mmb_finalize();
  if(stat != MMB_OK) {
    MMB_ERR("Failed to finalise mamba library\n");
    goto BAILOUT;
  }

  if(result) {
    printf("Example did not complete successfully, array copy failed\n");
  } else {
    printf("Example completed successfully\n");
  }

BAILOUT:
  if (stat != MMB_OK)
    return EXIT_FAILURE;
  else
    return EXIT_SUCCESS;
}

/* TODO: Extend this to be a full comparison and then include in commom API */
mmbError mmb_array_cmp(mmbArray* mba0, mmbArray* mba1, bool *result) {
  mmbError stat = MMB_OK;

  if (!mba0 || !mba1){
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Check array sizes match
  int dims_cmp = mmb_dimensions_cmp(&mba0->dims, &mba1->dims);
  if(dims_cmp) {
    MMB_ERR("src and dst arrays are not the same size\n");
    goto BAILOUT;
  }

  // Tile with single tile
  stat = mmb_array_tile(mba0, &mba0->dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (chunked tiles)\n");
    goto BAILOUT;
  }
  stat = mmb_array_tile(mba1, &mba1->dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (chunked tiles)\n");
    goto BAILOUT;
  }

  // Loop over tiles
  mmbTileIterator *it0, *it1;
  stat = mmb_tile_iterator_create(mba0, &it0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }
  stat = mmb_tile_iterator_create(mba1, &it1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }

  mmbArrayTile* tile0;
  stat = mmb_tile_iterator_first(it0, &tile0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }
  mmbArrayTile* tile1;
  stat = mmb_tile_iterator_first(it1, &tile1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }


  stat = mmb_tile_cmp(tile0, tile1, result);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to compare array tiles\n");
    goto BAILOUT;
  }

  stat = mmb_tile_iterator_destroy(it0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }
  stat = mmb_tile_iterator_destroy(it1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }
  // Remove tiling
  stat = mmb_array_untile(mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to untile mamba array\n");
    goto BAILOUT;
  } 

  stat = mmb_array_untile(mba1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to untile mamba array\n");
    goto BAILOUT;
  } 

BAILOUT:
  return stat;
}


/* TODO: Extend this to be a full comparison and then include in commom API */
mmbError mmb_tile_cmp(mmbArrayTile *tile0, mmbArrayTile *tile1, bool *result) {
  mmbError stat = MMB_OK;
  *result = false;
  for(size_t i = tile0->lower[0]; i < tile0->upper[0]; i++){
    if(MMB_IDX_1D(tile0, i, float) != MMB_IDX_1D(tile1, i, float)) {
      *result = true;
      break;
    }
  }
  return stat;
}
