/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <mamba.h>
#include <mmb_tile_iterator.h>
#include <mmb_memory.h>
#include <mmb_logging.h>

int check_buffer(float* buf, const size_t M){
  int success = 1;
  for(size_t i = 0; i < M && success; i++){
    if(buf[i] != 1){
      printf("First error at: (%zu)\n", i);
      success = 0;
    }      
  }
  
  return success;
}

void cpu_operate(mmbArrayTile* tile) 
{
  MMB_DEBUG("Operating on tile, dim0: %zu, %zu;\n",
          tile->lower[0], tile->upper[0]);
  for(size_t i = tile->lower[0]; i < tile->upper[0]; i++){
      MMB_IDX_1D(tile, i, float) = 1;
    }
}


int main()
{
  // User allocates 1D C-array
  size_t M = 128;
  size_t bytes = sizeof(float)*M;
  float* buffer0 = (float*)malloc(bytes);

  mmbError stat = MMB_OK;

  /* Initialize Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialise mamba\n");
    goto BAILOUT;
  }
  
  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to set debug option for mamba\n");
    goto BAILOUT;
  }

  mmbMemSpace* dram_space;
  int discovery_enabled = 0;
  stat = mmb_discovery_is_enabled(&discovery_enabled);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to query discovery status\n");
    goto BAILOUT;
  }
  if (!discovery_enabled) {
    // Memory configuration options
    const mmbMemSpaceConfig dram_config = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };
    stat = mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, NULL);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to register dram memory for mamba\n");
      goto BAILOUT;
    }
  }
  stat = mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to retreive suitable dram memory\n");
    goto BAILOUT;
  }

  mmbMemInterface *dram_interface;
  stat = mmb_request_interface(dram_space, NULL, &dram_interface);
  if (MMB_OK != stat) {
    MMB_ERR("CPU interface request failed. (%d)\n", stat);
    goto BAILOUT;
  }

  // Create mmb array 
  mmbArray* mba0;
  mmbLayout *layout;
  mmbDimensions dims = {1, &M};
  stat = mmb_layout_create_regular_1d(sizeof(float), MMB_PADDING_NONE, &layout);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create regular 1d layout\n");
    goto BAILOUT;
  }

  stat = mmb_array_create_wrapped(buffer0, &dims, layout, dram_interface, MMB_READ_WRITE, &mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mamba array\n");
    goto BAILOUT;
  }

  // Create a tiling that covers whole array with single tile
  stat = mmb_array_tile(mba0, &dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (single tile)\n");
    goto BAILOUT;
  }

  // Ask for the next (only) tile
  mmbTileIterator* it;
  stat = mmb_tile_iterator_create(mba0, &it); 
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }
  mmbArrayTile* tile;
  stat = mmb_tile_iterator_first(it,&tile);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }

  // User reads/generates data
  for(size_t i = tile->lower[0]; i < tile->upper[0]; i++) {
    MMB_IDX_1D(tile, i, float) = 0;
  }

  // Free the iterator we were using
  stat = mmb_tile_iterator_destroy(it);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }

  // Request tiling of array with chunksize 32
  size_t chunkdims = 32;
  mmbDimensions chunks = {1, &chunkdims};
  stat = mmb_array_tile(mba0, &chunks);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (chunked tiles)\n");
    goto BAILOUT;
  }

  // Loop over tiles
  stat = mmb_tile_iterator_create(mba0, &it);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }

  size_t nt; 
  stat = mmb_tile_iterator_count(it, &nt);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator count\n");
    goto BAILOUT;
  }

  stat = mmb_tile_iterator_first(it, &tile);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }
  for(size_t i = 0; i < nt; i++){
    cpu_operate(tile);
    stat = mmb_tile_iterator_next(it, &tile);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to increment tile iterator\n");
      goto BAILOUT;
    }
  }
  stat = mmb_tile_iterator_destroy(it);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }

  // Remove tiling
  stat = mmb_array_untile(mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to untile mamba array\n");
    goto BAILOUT;
  }

  // Copy to new array with same layout
  float* buffer1 = (float*)malloc(bytes);
  mmbArray* mba1;
  stat = mmb_array_create_wrapped(buffer1, &dims, layout, dram_interface, MMB_READ_WRITE, &mba1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mamba array\n");
    goto BAILOUT;
  }
  stat = mmb_layout_destroy(layout);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free layout\n");
    goto BAILOUT;
  }

  stat = mmb_array_copy(mba1, mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy mamba arrays\n");
    goto BAILOUT;
  }

  stat = mmb_array_destroy(mba0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free mamba array\n");
    goto BAILOUT;
  }

  stat = mmb_array_destroy(mba1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free mamba array\n");
    goto BAILOUT;
  }

  stat = mmb_finalize();
  if(stat != MMB_OK) {
    MMB_ERR("Failed to finalise mamba library\n");
    goto BAILOUT;
  }
  

  int success0 = check_buffer(buffer0, M);
  if(success0)
    printf("Checked buffer 0 size (%zu): success\n", M);
  else 
    printf("Checked buffer 0 size (%zu): fail\n", M);


  int success1 = check_buffer(buffer1, M);
  if(success1)
    printf("Checked buffer 1 size (%zu): success\n", M);
  else 
    printf("Checked buffer 1 size (%zu): fail\n", M);

  if(success0 && success1) {
    printf("Example completed successfully\n");
  }
  free(buffer0);
  free(buffer1);

BAILOUT:
  if (MMB_OK != stat || !success0 || !success1)
    return EXIT_FAILURE;
  else
    return EXIT_SUCCESS;
}


