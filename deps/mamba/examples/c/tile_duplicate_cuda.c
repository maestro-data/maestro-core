/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#include <mamba.h>
#include <mmb_tile.h>
#include <mmb_tile_iterator.h>
#include <mmb_memory.h>
#include <mmb_logging.h>

static mmbError init_matrix_buffer(mmbArray *mba_a);
static mmbError check_result(mmbArray *mba);

/* Wrapper to cuda kernel */
extern mmbError write_to_tile_cuda(mmbArrayTile *tile, bool verbose);

int main()
{
  printf("Running example duplicate_tile...\n");

  mmbError stat;
  size_t array_size = 32;
  size_t tile_size = 8;

  /* Initialise Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  CHECK_STATUS(stat, "Failed to initialise mamba\n", BAILOUT);

  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  CHECK_STATUS(stat, "Failed to turn on debug logging for mamba\n", BAILOUT);

  mmbMemSpace *dram_space, *gpu_space;
  int discovery_enabled = 0;
  stat = mmb_discovery_is_enabled(&discovery_enabled);
  CHECK_STATUS(stat, "Failed to check the discovery status\n", BAILOUT);
  if (!discovery_enabled) {
    // Memory configuration options
    const mmbMemSpaceConfig conf = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };

    stat = mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &conf, NULL);
    CHECK_STATUS(stat, "Failed to register dram memory for mamba\n", BAILOUT);

    stat = mmb_register_memory(MMB_GDRAM, MMB_GPU_CUDA, &conf, NULL);
    CHECK_STATUS(stat, "Failed to register gpu memory for mamba\n", BAILOUT);
  }
  stat = mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space);
  CHECK_STATUS(stat, "Failed to retrieve suitable dram memory\n", BAILOUT);

  stat = mmb_request_space(MMB_GDRAM, MMB_GPU_CUDA, NULL, &gpu_space);
  CHECK_STATUS(stat, "Failed to retrieve suitable gpu memory\n", BAILOUT);

  mmbMemInterface *dram_interface, *gpu_interface;
  stat = mmb_request_interface(dram_space, NULL, &dram_interface);
  CHECK_STATUS(stat, "Failed to retrieve suitable dram memory interface\n", BAILOUT);

  stat = mmb_request_interface(gpu_space, NULL, &gpu_interface);
  CHECK_STATUS(stat, "Failed to retrieve suitable gpu memory interface\n", BAILOUT);

  // Create mmb array
  mmbArray *mba;
  mmbLayout *layout;
  size_t arrdims[1] = {array_size};
  mmbDimensions dims = {1, arrdims};

  stat = mmb_layout_create_regular_1d(sizeof(float), MMB_PADDING_NONE, &layout);
  CHECK_STATUS(stat, "Failed to create 1d array layout\n", BAILOUT);

  stat = mmb_array_create(&dims, layout, dram_interface, MMB_READ_WRITE, &mba);
  CHECK_STATUS(stat, "Failed to create mamba array for test matrix\n", BAILOUT);

  stat = init_matrix_buffer(mba);
  CHECK_STATUS(stat, "Failed to initialise matrix buffers\n", BAILOUT);

  // Request tiling of all arrays with chunkx chunky
  size_t chunkdims[1] = {tile_size};
  mmbDimensions chunks = {1, chunkdims};
  stat = mmb_array_tile(mba, &chunks);
  CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

  // Loop over tiles using standard indexing
  // Duplicate each tile, write to it, and merge back to original 
  mmbArrayTile *tile;
  mmbIndex *idx;
  mmbDimensions *tiling_dims;
  stat = mmb_index_create(1, &idx);
  CHECK_STATUS(stat, "Failed to create mmb index\n", BAILOUT);
  stat = mmb_tiling_dimensions(mba, &tiling_dims);
  CHECK_STATUS(stat, "Failed to get mamba array tiling dimensions\n", BAILOUT);

  for (size_t ti = 0; ti < tiling_dims->d[0]; ++ti) {
      // Set tile indices for c array
      stat = mmb_index_set(idx,ti);
      CHECK_STATUS(stat, "Failed to set mmb index\n", BAILOUT);
      stat = mmb_tile_at(mba, idx, &tile);
      CHECK_STATUS(stat, "Failed to get tile at index\n", BAILOUT);

      // Duplicate tile in DRAM; write to and merge duplicated tile
      mmbArrayTile *duplicate_tile;
      stat = mmb_tile_duplicate(tile, gpu_interface, MMB_READ_WRITE, layout, &duplicate_tile);
      CHECK_STATUS(stat, "Failed to duplicate mmb array tile\n", BAILOUT);

      stat = write_to_tile_cuda(duplicate_tile, true);
      CHECK_STATUS(stat, "Failed to write to duplicate tile\n", BAILOUT);

      stat = mmb_tile_merge(duplicate_tile, MMB_OVERWRITE);
      CHECK_STATUS(stat, "Failed to merge duplicate tile\n", BAILOUT);
  }


  // Cleanup intermediate objects, check result buffer, cleanup mamba
  stat = mmb_layout_destroy(layout);
  CHECK_STATUS(stat, "Failed to destroy mmb layout\n", BAILOUT);
  stat = mmb_dimensions_destroy(tiling_dims);
  CHECK_STATUS(stat, "Failed to destroy mmb dimensions\n", BAILOUT);
  stat = mmb_index_destroy(idx);
  CHECK_STATUS(stat, "Failed to destroy mmb index\n", BAILOUT);

  stat = check_result(mba);
  if(stat == MMB_OK) {
      printf("Array successfully tiled and duplicated in GPU (cuda) memory\n");
   } else {
      MMB_ERR("Array data validation check failed.\n");
      goto BAILOUT;
  }

  stat = mmb_array_destroy(mba);
  CHECK_STATUS(stat, "Failed to destroy mmb array\n", BAILOUT);

  stat = mmb_finalize();
  CHECK_STATUS(stat, "Failed to finalize mmb library\n", BAILOUT);

BAILOUT:
  return (stat == MMB_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}


static mmbError init_matrix_buffer(mmbArray *mba_a)
{
  // Tile array with single tile and initialise to 0
  mmbError stat = mmb_array_tile(mba_a, &mba_a->dims);
  CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

  mmbTileIterator *it;
  stat = mmb_tile_iterator_create(mba_a, &it);
  CHECK_STATUS(stat, "Failed to create tile iterator\n", BAILOUT);

  mmbArrayTile* tile;
  stat = mmb_tile_iterator_first(it, &tile);
  CHECK_STATUS(stat, "Failed to set tile iterator to first\n", BAILOUT);

  for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
    MMB_IDX_1D(tile, i, float) = 0.f;
  }

  stat = mmb_tile_iterator_destroy(it);
  CHECK_STATUS(stat, "Failed to free tile iterator\n", BAILOUT);

  stat = mmb_array_untile(mba_a);
  CHECK_STATUS(stat, "Failed to untile mamba array\n", BAILOUT);

BAILOUT:
  return stat;
}

static mmbError check_result(mmbArray *mba)
{
  // Check array is full of 1's
  mmbError stat = mmb_array_tile(mba, &mba->dims);
  CHECK_STATUS(stat, "Failed to tile mamba array\n", BAILOUT);

  mmbTileIterator *it;
  stat = mmb_tile_iterator_create(mba, &it);
  CHECK_STATUS(stat, "Failed to get tile iterator\n", BAILOUT);

  mmbArrayTile* tile;
  stat = mmb_tile_iterator_first(it, &tile);
  CHECK_STATUS(stat, "Failed to set tile iterator to first\n", BAILOUT);

  for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
    if(MMB_IDX_1D(tile, i, float) != 1.f) {
      MMB_ERR("Array data incorrect at idx %lu, expected 1.0, got: %f\n", i, MMB_IDX_1D(tile, i, float));
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }

  stat = mmb_tile_iterator_destroy(it);
  CHECK_STATUS(stat, "Failed to free tile iterator\n", BAILOUT);

  stat = mmb_array_untile(mba);
  CHECK_STATUS(stat, "Failed to untile mamba array\n", BAILOUT);

BAILOUT:
  return stat;
}
