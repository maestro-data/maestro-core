/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>


/* We use OpenCL 1.2 APIs for compatibility with Xilinx OpenCL API 
  This isnt required for normal user code, and this example
  will still work (with a warning) if not defined. */
#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif 

#include <mamba.h>
#include <mmb_memory.h>
#include <mmb_logging.h>

#include <mmb_opencl.h>

#ifndef MAX_PLATFORMS
#define MAX_PLATFORMS 8
#endif /* MAX_PLATFORMS */
#ifndef MAX_DEVICES
#define MAX_DEVICES 16
#endif /* MAX_DEVICES */
#ifndef MAX_DEVICE_NAME
#define MAX_DEVICE_NAME 1024
#endif /* MAX_DEVICE_NAME */
static inline const char *
opencl_error_decode(const cl_int ocl_err);

static mmbError init_host_buffer(mmbAllocation *host_allocation, size_t n_elements);
static mmbError check_result(mmbAllocation *host_allocation, size_t n_elements);
static mmbError load_file_to_memory(const char *filename, char **result, int* size);

/* If we have an OpenCL compiler for the used platform, build this binary */
const char* kernel_src =  
"__kernel void write_to_buffer(__global unsigned int* buf, int size) {"
"  for(int i = 0; i < size; ++i) {"
"    buf[i] = i * buf[i];"
"  }"
"}";
/* If not, we will read a binary file from the path specified by env var 
 * MMB_CONFIG_BUFFER_COPY_OPENCL_BINARY that must contain a compiled kernel 
 * matching the above. */

const char* config_binary_var="MMB_CONFIG_BUFFER_COPY_OPENCL_BINARY";

int main()
{
  printf("Running example buffer_copy...\n");

  mmbError stat = MMB_OK;
  size_t n_elements = 32;
  cl_int ocl_err;

  /* OpenCL Setup */
  /* Get list of platforms */
  cl_uint num_platforms = 0;
  cl_platform_id platforms[MAX_PLATFORMS];
  ocl_err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Unable to gather plarforms ID's (%s).\n",
        opencl_error_decode(ocl_err));
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  /* Get list of devices */
  cl_uint total_devices = 0;
  cl_device_id devices[MAX_DEVICES];
  for (cl_uint p = 0; p < num_platforms; ++p) {
    cl_uint num_devices = 0;
    ocl_err = clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL,
                         MAX_DEVICES - total_devices, devices + total_devices,
                         &num_devices);
    if (CL_SUCCESS != ocl_err) {
      MMB_ERR("Failed to get device name. (%s).\n",
          opencl_error_decode(ocl_err));
      stat = MMB_ERROR;
      goto BAILOUT;
    }
    total_devices += num_devices;
  }

  /* Print list of devices */
  char name[MAX_DEVICE_NAME];
  size_t name_len = MAX_DEVICE_NAME;
  MMB_NOISE("Available OpenCL devices:\n");
  for (cl_uint d = 0; d < total_devices; ++d) {
    ocl_err = clGetDeviceInfo(devices[d], CL_DEVICE_NAME,
                          name_len, name, &name_len);
    if (CL_SUCCESS == ocl_err) {
      MMB_NOISE("\t%2d: %s%s\n", d, name,
                MAX_DEVICE_NAME < name_len ? " (possibly truncated)" : "");
    } else {
      MMB_DEBUG("\t%2d: (err: %s)\n", d, opencl_error_decode(ocl_err));
    }
  }

  /* Use first device unless OCL_DEVICE environment variable is given */
  cl_uint device_index = 0;
  char *dev_env = getenv("OCL_DEVICE");
  if (NULL != dev_env) {
    char *end;
    device_index = strtol(dev_env, &end, 10);
    if (0 < strlen(end)) {
      MMB_ERR("Invalid OCL_DEVICE variable \"%s\".\n", dev_env);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }

  if (device_index >= total_devices) {
    MMB_ERR("Device index set to %d but only %d devices available.\n",
        device_index, total_devices);
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  /* Print OpenCL device name */
  ocl_err = clGetDeviceInfo(devices[device_index], CL_DEVICE_NAME,
                        name_len, name, &name_len);
  if (CL_SUCCESS == ocl_err) {
    MMB_DEBUG("Selected OpenCL device: %s%s (index=%d).\n", name,
              MAX_DEVICE_NAME < name_len ? " (possibly truncated)" : "",
              device_index);
  } else {
    MMB_DEBUG("Unable to retrieve device's name (err: %s).\n",
          opencl_error_decode(ocl_err));
  }

    /* Create OpenCL context */
  cl_context context = clCreateContext(NULL, 1, &devices[device_index], 
                                       NULL, NULL, &ocl_err);
  if (CL_SUCCESS != ocl_err) {
      MMB_DEBUG("Failed to create opencl context, err: %s\n", 
                opencl_error_decode(ocl_err));
  }
  
  /* Create Command Queue */
  cl_command_queue queue = clCreateCommandQueue(context, devices[device_index], 
                                                0, &ocl_err);
  if (CL_SUCCESS != ocl_err) {
      MMB_DEBUG("Failed to create opencl command queue, err: %s\n", 
                opencl_error_decode(ocl_err));
  }

  /* Build/read program binary */
  cl_program program;
  cl_bool avail;
  clGetDeviceInfo(devices[device_index], CL_DEVICE_COMPILER_AVAILABLE, sizeof(cl_bool), &avail, NULL);
  if(avail == CL_TRUE) {
    size_t src_len = strlen(kernel_src);
    program = clCreateProgramWithSource(context, 1, (const char **)&kernel_src,
                                        (const size_t *)&src_len, &ocl_err);
    if (CL_SUCCESS != ocl_err) {
        MMB_DEBUG("Failed to create program with source, err: %s\n", 
                  opencl_error_decode(ocl_err));
    }

    /* Build Kernel Program */
    ocl_err = clBuildProgram(program, 1, &devices[device_index], NULL, NULL, NULL);
    if (CL_SUCCESS != ocl_err) {
        MMB_DEBUG("Failed to build program, err: %s\n", 
                  opencl_error_decode(ocl_err));
    }    
  } else {
    unsigned char *kernelbinary;
    char *binary_path = getenv(config_binary_var);
    if (binary_path == NULL) { 
      MMB_ERR("No compiler found for OpenCL binary, and no binary path specified for loading\n");
      goto BAILOUT;
    }

    MMB_DEBUG("Loading binary %s\n", binary_path);
    int size;
    stat = load_file_to_memory(binary_path, (char **) &kernelbinary, &size);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to load binary kernel\n");
      goto BAILOUT;
    }

    size_t size_var = size; 
    program = clCreateProgramWithBinary(context, 1, &devices[device_index], 
                                      &size_var,(const unsigned char **) &kernelbinary, 
                                      0, &ocl_err);
    if (CL_SUCCESS != ocl_err) {
        MMB_ERR("Failed to create program from binary, err: %s\n", 
                  opencl_error_decode(ocl_err));
        stat =  MMB_ERROR;
        goto BAILOUT;
    }  
  }
  
  /* Create OpenCL Kernel */
  cl_kernel kernel = clCreateKernel(program, "write_to_buffer", &ocl_err);
  if (CL_SUCCESS != ocl_err) {
      MMB_DEBUG("Failed to build program, err: %s\n", 
                opencl_error_decode(ocl_err));
  }

  
  /* Initialise Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  CHECK_STATUS(stat, "Failed to initialise mamba\n", BAILOUT);

  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  CHECK_STATUS(stat, "Failed to turn on debug logging for mamba\n", BAILOUT);

  mmbMemSpace *dram_space, *dev_space;

  /* Check if discovery is enabled, set up memory spaces if not */
  int discovery_enabled = 0;
  stat = mmb_discovery_is_enabled(&discovery_enabled);
  CHECK_STATUS(stat, "Failed to check the discovery status\n", BAILOUT);
  if (!discovery_enabled) {
    // Memory configuration options
    const mmbMemSpaceConfig conf = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };

    stat = mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &conf, NULL);
    CHECK_STATUS(stat, "Failed to register dram memory for mamba\n", BAILOUT);

    stat = mmb_register_memory(MMB_GDRAM, MMB_OPENCL, &conf, NULL);
    CHECK_STATUS(stat, "Failed to register device memory for mamba\n", BAILOUT);
  }

  /* Request memory spaces, and associated interfaces for DRAM, and device/OpenCL */
  stat = mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space);
  CHECK_STATUS(stat, "Failed to retrieve suitable dram memory\n", BAILOUT);

  stat = mmb_request_space(MMB_GDRAM, MMB_OPENCL, NULL, &dev_space);
  CHECK_STATUS(stat, "Failed to retrieve suitable device memory\n", BAILOUT);

  mmbMemInterface *dram_interface, *dev_interface;
  stat = mmb_request_interface(dram_space, NULL, &dram_interface);
  CHECK_STATUS(stat, "Failed to retrieve suitable dram memory interface\n", BAILOUT);

  cl_device_id     d = devices[device_index];
  cl_context       c = context;
  cl_command_queue q = queue;

  struct mmb_opencl_t oclopt = { .device = d, .context = c, .queue = q};

  mmbMemInterfaceConfig dev_opencl_conf = (mmbMemInterfaceConfig){
    .provider = MMB_PROVIDER_ANY,
    .strategy = MMB_STRATEGY_ANY,
    .name = "",
    .provider_opts = {.ocl = &oclopt}
  };

  stat = mmb_create_interface(dev_space, &dev_opencl_conf, &dev_interface);
  CHECK_STATUS(stat, "Failed to create device memory interface\n", BAILOUT);

  /* Allocate a block of local memory, and a block of device memory */
  mmbAllocation* host_allocation, *dev_allocation;
  stat = mmb_allocate(n_elements*sizeof(unsigned int), dram_interface, &host_allocation);
  CHECK_STATUS(stat, "Failed to allocated device buffer\n", BAILOUT);

  stat = mmb_allocate(n_elements*sizeof(unsigned int), dev_interface, &dev_allocation);
  CHECK_STATUS(stat, "Failed to allocated device buffer\n", BAILOUT);

  /* Initialise local buffer */
  init_host_buffer(host_allocation, n_elements);

  /* Copy our local buffer to device */
  stat = mmb_copy(dev_allocation, host_allocation);
  CHECK_STATUS(stat, "Failed to copy allocation to device\n", BAILOUT);

  cl_mem buffer = (cl_mem)dev_allocation->ptr;

  /* Set OpenCL Kernel Parameters */
  ocl_err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&buffer);
  if (CL_SUCCESS != ocl_err) {
      MMB_DEBUG("Failed to set kernel arg, err: %s\n", 
                opencl_error_decode(ocl_err));
  }
  /* Set OpenCL Kernel Parameters */
  cl_int n_elem_cl = n_elements;
  ocl_err = clSetKernelArg(kernel, 1, sizeof(cl_int), (void *)&n_elem_cl);
  if (CL_SUCCESS != ocl_err) {
      MMB_DEBUG("Failed to set kernel arg, err: %s\n", 
                opencl_error_decode(ocl_err));
  }
  
  /* Execute OpenCL Kernel */
  size_t work_dim = 1;
  ocl_err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &work_dim,
                                    &work_dim, 0, NULL, NULL);
  if (CL_SUCCESS != ocl_err) {
      MMB_DEBUG("Failed to enqueue task err: %s\n", 
                opencl_error_decode(ocl_err));
  }

  /* Copy back to host */
  stat = mmb_copy(host_allocation, dev_allocation);
  CHECK_STATUS(stat, "Failed to copy device allocation to host\n", BAILOUT);

  // Check result and cleanup

  stat = check_result(host_allocation, n_elements);
  if(stat == MMB_OK) {
      printf("Buffer copy to and from device (OpenCL) memory successful.\n");
   } else {
      MMB_ERR("Buffer data validation check failed.\n");
      goto BAILOUT;
  }

  stat = mmb_free(dev_allocation);
  CHECK_STATUS(stat, "Failed to free device allocation\n", BAILOUT);  
  stat = mmb_free(host_allocation);
  CHECK_STATUS(stat, "Failed to free host allocation\n", BAILOUT);
  stat = mmb_finalize(); 
  CHECK_STATUS(stat, "Failed to finalize mmb library\n", BAILOUT);

BAILOUT:
  return (stat == MMB_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}



static mmbError init_host_buffer(mmbAllocation *host_allocation, 
                                 size_t n_elements) {
  unsigned int* buffer = MMBALLOC2TBUF(host_allocation, unsigned int*);
  for(unsigned i = 0; i < n_elements; i++) 
    buffer[i] = i;
  return MMB_OK;
}

static mmbError check_result(mmbAllocation *host_allocation, 
                             size_t n_elements)
{
  mmbError stat = MMB_OK;
  unsigned int* buffer = MMBALLOC2TBUF(host_allocation, unsigned int*);
  for(unsigned i = 0; i < n_elements; i++) {
    if(buffer[i] != i*i) {
      MMB_ERR("Buffer data incorrect at idx %lu, expected idx^2 (%f), got: %f\n", i, 
              i*i, buffer[i]); 
      stat = MMB_ERROR;
      goto BAILOUT;      
    }
  }
BAILOUT:
  return stat;
}

static mmbError load_file_to_memory(const char *filename, char **result, int *size)
{
  uint s = 0;
  FILE *f = fopen(filename, "rb");
  if (f == NULL) {
    *result = NULL;
    MMB_ERR("Failed to open file\n");
    return MMB_ERROR;
  }
  fseek(f, 0, SEEK_END);
  s = ftell(f);
  fseek(f, 0, SEEK_SET);
  *result = (char *)malloc(s+1);
  if (s != fread(*result, sizeof(char), s, f)) {
    free(*result);
    MMB_ERR("Failed to read file\n");
    return MMB_ERROR; 
  }
  fclose(f);
  (*result)[s] = 0;
  *size = s;
  return MMB_OK;
}

#ifndef CASE_TO_STR
#define CASE_TO_STR(a) case a: return #a
#endif /* CASE_TO_STR */

static inline const char *
opencl_error_decode(const cl_int ocl_err)
{
  switch(ocl_err) {
    /* run-time and JIT compiler errors */
    CASE_TO_STR(CL_SUCCESS);
    CASE_TO_STR(CL_DEVICE_NOT_FOUND);
    CASE_TO_STR(CL_DEVICE_NOT_AVAILABLE);
    CASE_TO_STR(CL_COMPILER_NOT_AVAILABLE);
    CASE_TO_STR(CL_MEM_OBJECT_ALLOCATION_FAILURE);
    CASE_TO_STR(CL_OUT_OF_RESOURCES);
    CASE_TO_STR(CL_OUT_OF_HOST_MEMORY);
    CASE_TO_STR(CL_PROFILING_INFO_NOT_AVAILABLE);
    CASE_TO_STR(CL_MEM_COPY_OVERLAP);
    CASE_TO_STR(CL_IMAGE_FORMAT_MISMATCH);
    CASE_TO_STR(CL_IMAGE_FORMAT_NOT_SUPPORTED);
    CASE_TO_STR(CL_BUILD_PROGRAM_FAILURE);
    CASE_TO_STR(CL_MAP_FAILURE);
    CASE_TO_STR(CL_MISALIGNED_SUB_BUFFER_OFFSET);
    CASE_TO_STR(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST);
    CASE_TO_STR(CL_COMPILE_PROGRAM_FAILURE);
    CASE_TO_STR(CL_LINKER_NOT_AVAILABLE);
    CASE_TO_STR(CL_LINK_PROGRAM_FAILURE);
    CASE_TO_STR(CL_DEVICE_PARTITION_FAILED);
    CASE_TO_STR(CL_KERNEL_ARG_INFO_NOT_AVAILABLE);

    /* compile-time errors */
    CASE_TO_STR(CL_INVALID_VALUE);
    CASE_TO_STR(CL_INVALID_DEVICE_TYPE);
    CASE_TO_STR(CL_INVALID_PLATFORM);
    CASE_TO_STR(CL_INVALID_DEVICE);
    CASE_TO_STR(CL_INVALID_CONTEXT);
    CASE_TO_STR(CL_INVALID_QUEUE_PROPERTIES);
    CASE_TO_STR(CL_INVALID_COMMAND_QUEUE);
    CASE_TO_STR(CL_INVALID_HOST_PTR);
    CASE_TO_STR(CL_INVALID_MEM_OBJECT);
    CASE_TO_STR(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
    CASE_TO_STR(CL_INVALID_IMAGE_SIZE);
    CASE_TO_STR(CL_INVALID_SAMPLER);
    CASE_TO_STR(CL_INVALID_BINARY);
    CASE_TO_STR(CL_INVALID_BUILD_OPTIONS);
    CASE_TO_STR(CL_INVALID_PROGRAM);
    CASE_TO_STR(CL_INVALID_PROGRAM_EXECUTABLE);
    CASE_TO_STR(CL_INVALID_KERNEL_NAME);
    CASE_TO_STR(CL_INVALID_KERNEL_DEFINITION);
    CASE_TO_STR(CL_INVALID_KERNEL);
    CASE_TO_STR(CL_INVALID_ARG_INDEX);
    CASE_TO_STR(CL_INVALID_ARG_VALUE);
    CASE_TO_STR(CL_INVALID_ARG_SIZE);
    CASE_TO_STR(CL_INVALID_KERNEL_ARGS);
    CASE_TO_STR(CL_INVALID_WORK_DIMENSION);
    CASE_TO_STR(CL_INVALID_WORK_GROUP_SIZE);
    CASE_TO_STR(CL_INVALID_WORK_ITEM_SIZE);
    CASE_TO_STR(CL_INVALID_GLOBAL_OFFSET);
    CASE_TO_STR(CL_INVALID_EVENT_WAIT_LIST);
    CASE_TO_STR(CL_INVALID_EVENT);
    CASE_TO_STR(CL_INVALID_OPERATION);
    CASE_TO_STR(CL_INVALID_GL_OBJECT);
    CASE_TO_STR(CL_INVALID_BUFFER_SIZE);
    CASE_TO_STR(CL_INVALID_MIP_LEVEL);
    CASE_TO_STR(CL_INVALID_GLOBAL_WORK_SIZE);
    CASE_TO_STR(CL_INVALID_PROPERTY);
    CASE_TO_STR(CL_INVALID_IMAGE_DESCRIPTOR);
    CASE_TO_STR(CL_INVALID_COMPILER_OPTIONS);
    CASE_TO_STR(CL_INVALID_LINKER_OPTIONS);
    CASE_TO_STR(CL_INVALID_DEVICE_PARTITION_COUNT);
    default: return "Unknown OpenCL error";
  }
}

#undef CASE_TO_STR
