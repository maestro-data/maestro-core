program array_copy_wrapped_1d
  use mamba
  implicit none
  logical, parameter :: debug=.true.
  integer, parameter :: M=128
  integer(mmbErrorKind) err
  integer(mmbSizeKind) ntiles
  integer(mmbSizeKind), allocatable, dimension(:) :: tile_dims
  type(mmbMemSpace) dram_space
  type(mmbMemSpaceConfig) dram_config
  type(mmbMemInterface) dram_interface
  type(mmbArray) :: mba0
  integer(mmbIndexKind) :: dims(1)
  type(mmbLayout) layout
  type(mmbTileIterator) it
  type(mmbArrayTile) tile
  real, dimension(:), allocatable,target :: buffer0
  real, dimension(:), pointer :: tp
  integer :: i,itile,chunksize
  
  print *,"Starting example 1d_array_copy_wrapped (Fortran)"

  allocate( buffer0(m) )

  call mmb_init(err=err)
  if (err .ne. MMB_OK) then
     ERROR STOP 'Failed to initialise Mamba'
   else if (debug) then
     print *,'Completed mmb_init()'
   end if

   dram_config = mmbMemSpaceConfig(mmbSizeConfig(MMB_SIZE_SET,.false.,8000),&
                                   MMB_MEMINTERFACE_CONFIG_DEFAULT)
  ! register memory but don't ask for memory space
  call mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                           dram_config,err=err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to register DRAM memory for Mamba'
   end if

  ! Get memory space, space config is optional
  call mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                         new_space=dram_space, err=err )

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get memory space'
   end if

  ! interface config_opts are optional
  call mmb_request_interface(dram_space, new_interface=dram_interface, err=err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get memory interface'
   end if

  dims = [M] 

  ! routine is wrongly named, mmb_layout_create_padding_zero better
  call mmb_layout_create_regular_1d(int(storage_size(buffer0)/8,mmbSizeKind),&
                       mmb_layout_padding_create_zero(), &
                       layout,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to create regular 1d layout'
   end if

  call mmb_array_create_wrapped(buffer0,dims, layout, &
                                 dram_interface, MMB_READ_WRITE, mba0,err )

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to create mamba array'
   end if

  call mmb_array_tile(mba0, dims, err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to tile mamba array (single tile)'
   end if

  call mmb_tile_iterator_create(mba0, it, err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get tile iterator'
   end if

  call mmb_tile_iterator_first(it,tile,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to set tile iterator to first'
   end if
   
  call mmb_tile_get_pointer(tile,tp)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Unable to extract tile dataA'
   end if
   
  print *,'tile_info'
  print *,'   rank',tile%rank
  print *,'  lower',tile%lower
  print *,'  upper',tile%upper
  print *,' alower',tile%alower
  print *,' aupper',tile%aupper
  print *,'    dim',tile%dim
  print *,'abs_dim',tile%abs_dim

  ! Details of pointer
  print *,'pointer  shape ',shape(tp)
  print *,'        bounds ',lbound(tp),ubound(tp)
   
  ! set array

  block
    asynchronous :: buffer0
  do i=1,M
     tp(i)=i*0.001+3.0
  end do
  end block

  print '(tr1,10f7.4)',buffer0

  ! Free the iterator
  call mmb_tile_iterator_destroy(it,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to free tile iterator'
   end if

  chunksize = 32
  dims = [chunksize] 
  
  call mmb_array_tile(mba0, dims,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to tile mamba array (single tile)'
   end if

  call mmb_tile_count(mba0, ntiles, err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get tile count for array'
   end if

  allocate(tile_dims(1))

  call mmb_tiling_dimensions(mba0, tile_dims )

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get tile count for array'
   end if

  print *,'Array has',int(ntiles),'tiles'
  write(*,'("  tiles per dim:")',advance='no')
  do i=1,1
     write(*,'(tr1,i0)',advance='no')tile_dims(i)
  end do
  write(*,*)

  deallocate(tile_dims)

  call mmb_tile_iterator_create(mba0, it, err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get tile iterator'
   end if

  call mmb_tile_iterator_count(it, ntiles, err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get tile iterator count'
  end if
 
  print *,"Number of tiles spanned by iterator",ntiles
  call mmb_tile_iterator_first(it,tile,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to set tile iterator to first'
   end if

  do itile=1,ntiles

    call mmb_tile_get_pointer(tile,tp)

    if (err .ne. MMB_OK) then
      ERROR STOP 'Unable to extract tile dataA'
     end if
   
    print *,'tile_info for tile',itile
    print *,'   rank',tile%rank
    print *,'  lower',tile%lower
    print *,'  upper',tile%upper
    print *,' alower',tile%alower
    print *,' aupper',tile%aupper
    print *,'    dim',tile%dim
    print *,'abs_dim',tile%abs_dim

    ! Details of pointer
    print *,'pointer  shape ',shape(tp)
    print *,'        bounds ',lbound(tp),ubound(tp)

    print '(tr1,10f7.4)',&
         (tp(i),i=tile%lower(1),tile%upper(1))
    
    if (itile < ntiles) then
     call mmb_tile_iterator_next(it,tile,err)

      if (err .ne. MMB_OK) then
        ERROR STOP 'Failed to set tile iterator to first'
       end if
    end if

   end do

  ! Free the iterator
  call mmb_tile_iterator_destroy(it,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to free tile iterator'
   end if

  call mmb_array_untile(mba0,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Unale to untie mamba array'
   end if

  call mmb_array_destroy(mba0,err)

  if (err .ne. MMB_OK) ERROR STOP 'Failed to free array mba1'

  call mmb_layout_destroy(layout,err)

  if (err .ne. MMB_OK) ERROR STOP 'Failed to create mamba array'

  call mmb_finalize(err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to initialise Mamba'
   end if

  deallocate( buffer0 )

  print *,"Ended example 1d_array_copy_wrapped_1d (Fortran)"

end program array_copy_wrapped_1d
