! Copyright (C) 2018-2020 Cray UK
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are
! met:
!
! 1. Redistributions of source code must retain the above copyright
!    notice, this list of conditions and the following disclaimer.
!
! 2. Redistributions in binary form must reproduce the above copyright
!    notice, this list of conditions and the following disclaimer in the
!    documentation and/or other materials provided with the distribution.
!
! 3. Neither the name of the copyright holder nor the names of its
!    contributors may be used to endorse or promote products derived from
!    this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
! IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
! TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
! PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
! HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
program array_copy_1d
  use mamba
  implicit none
  logical, parameter :: debug=.true.
  integer, parameter :: M=128
  integer(mmbErrorKind) err
  type(mmbMemSpace) dram_space
  type(mmbMemSpaceConfig) dram_config
  type(mmbMemInterface) dram_interface
  type(mmbArray) :: mba0,mba1
  integer(mmbIndexKind) :: dims(1)
  type(mmbLayout) layout
  type(mmbTileIterator) it
  type(mmbArrayTile) tile
  real, dimension(:), pointer :: tp
  integer :: i
  logical compared_equal
  
  print *,"Starting example 1d_array_copy (Fortran)"
  
  call mmb_init(err=err)
  if (err .ne. MMB_OK) then
     ERROR STOP 'Failed to initialise Mamba'
   else if (debug) then
     print *,'Completed mmb_init()'
   end if

  dram_config = mmbMemSpaceConfig(mmbSizeConfig(MMB_SIZE_SET,.false.,8000),MMB_MEMINTERFACE_CONFIG_DEFAULT)
  ! register memory but don't ask for memory space
  call mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                           dram_config,err=err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to register DRAM memory for Mamba'
   end if

  ! Get memory space, space config is optional
  call mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                         new_space=dram_space, err=err )

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get memory space'
   end if

  ! interface config_opts are optional
  call mmb_request_interface(dram_space, new_interface=dram_interface, err=err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get memory interface'
   end if
   
  dims=[M] 
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to create dimensions object'
   end if

  ! routine is wrongly named, mmb_layout_create_padding_zero better
   call mmb_layout_create_regular_1d(int(storage_size(1.0)/8,mmbSizeKind),&
                       mmb_layout_padding_create_zero(), &
                       layout,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to create regular 1d layout'
   end if

  print '("array pointer before allocation (mba0): ",z0)',mba0
  call mmb_array_create(dims, layout, dram_interface, MMB_READ_WRITE, mba0,err )

  print '("array pointer after allocation (mba0): ",z0)',mba0
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to create mamba array'
   end if

  call mmb_array_tile(mba0, dims, err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to tile mamba array (single tile)'
   end if

  call mmb_tile_iterator_create(mba0, it, err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to get tile iterator'
   end if

  call mmb_tile_iterator_first(it,tile, err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to set tile iterator to first'
   end if
   
  call mmb_tile_get_pointer(tile,tp)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Unable to extract tile dataA'
   end if
   
  print *,'tile_info'
  print *,'   rank',tile%rank
  print *,'  lower',tile%lower
  print *,'  upper',tile%upper
  print *,' alower',tile%alower
  print *,' aupper',tile%aupper
  print *,'    dim',tile%dim
  print *,'abs_dim',tile%abs_dim

  ! Details of pointer
  print *,'pointer  shape ',shape(tp)
  print *,'        bounds ',lbound(tp),ubound(tp)
   
  ! set array

  do i=1,M
    tp(i)=i-3.0
   end do
   
  ! Free the iterator
  call mmb_tile_iterator_destroy(it,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to free tile iterator'
   end if

  call mmb_array_untile(mba0,err)

  if (err .ne. MMB_OK) then
    ERROR STOP 'Unale to untile mamba array'
   end if
   
  ! Copy to new array with same layout
  call mmb_array_create(dims, layout, dram_interface, MMB_READ_WRITE, mba1,err )

  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to create mamba array'
   end if
 
  call mmb_layout_destroy(layout,err)

  if (err .ne. MMB_OK) ERROR STOP 'Failed to create mamba array'
   
  call mmb_array_copy(mba1, mba0)

  if (err .ne. MMB_OK) ERROR STOP 'Failed to copy mamba arrays'

  call mmb_array_compare(mba0,mba1,compared_equal)

  call mmb_array_destroy(mba0,err)

  if (err .ne. MMB_OK) ERROR STOP 'Failed to free array mba1'
  
  call mmb_array_destroy(mba1,err)

  if (err .ne. MMB_OK) ERROR STOP 'Failed to free array mba1'
  
  call mmb_finalize(err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to initialise Mamba'
   end if

   if (compared_equal) then
     print *,'Example completed successfully'
    else 
     print *,'Example did not complete successfully, array copy failed'
    end if
   
  print *,"Ended example 1d_array_copy (Fortran)"

  contains

  subroutine mmb_array_compare(mba0,mba1,compared_equal)
    type(mmbArray),intent(in) :: mba0,mba1
    type(mmbTileIterator) it0,it1
    type(mmbArrayTile) tile0,tile1
    logical, intent(out) :: compared_equal
    real, dimension(:), pointer :: tp0, tp1
    integer(mmbErrorKind) err
    integer i
    
    compared_equal=.true.

    ! We use full-array tiling so only one tile
    call mmb_array_tile(mba0,err=err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to tile array 0'  
    call mmb_array_tile(mba1,err=err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to tile array 1'  

    call mmb_tile_iterator_create(mba0,it0, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to get tile iterator for mba0'  
    call mmb_tile_iterator_create(mba1,it1, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to get tile iterator for mba1'  

    call mmb_tile_iterator_first(it0,tile0, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to get first tile from iterator 0'  
    call mmb_tile_iterator_first(it1,tile1, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to get first tile from iterator 1'

    call mmb_tile_get_pointer(tile0,tp0, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to get tile pointer for tile 0'
    call mmb_tile_get_pointer(tile1,tp1, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to get tile pointer for tile 0'

    if (rank(tile0%dim) .ne. rank(tile1%dim)) then
      ERROR STOP 'Arrays are not of the same rank'
      if (any(tile0%dim .ne. tile1%dim)) then
        ERROR STOP 'Arrays are not of the same dimension'
      end if
     end if

    ! 1 based indexing within tile for Fortran
    do i=tile0%lower(1),tile0%upper(1)
       ! print *,i,tp0(i),tp1(i)
       if (tp0(i) .ne. tp1(i)) compared_equal=.false.
     end do
    
    call mmb_tile_iterator_destroy(it0, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to free iterator 0'
    call mmb_tile_iterator_destroy(it1, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to free iterator 1'

    call mmb_array_untile(mba0, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to untile array 0'
    call mmb_array_untile(mba1, err)
    if (err .ne. MMB_OK) ERROR STOP 'Failed to untile array 1'
    
  end subroutine mmb_array_compare
      
end program array_copy_1d
