/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_TO_PET_EXPR_H
#define MMB_TO_PET_EXPR_H

#include "mmb_to_pet_expr.h"

#include "isl/id.h"

static
enum pet_op_type mmb_op_to_pet_op_types_table[MMB_OP_TYPE__MAX] = {
  [MMB_OP_UNDEFINED]   = pet_op_last,
  [MMB_OP_ASSIGN]      = pet_op_assign,
  [MMB_OP_ADD]         = pet_op_add,
  [MMB_OP_SUB]         = pet_op_sub,
  [MMB_OP_LT]          = pet_op_lt,
  [MMB_OP_GT]          = pet_op_gt,
  [MMB_OP_PRE_INC]     = pet_op_pre_inc,
  [MMB_OP_PRE_DEC]     = pet_op_pre_dec

  // More to come...
};


mmbError mmb_op_type_to_pet_op_type(mmbOpType mmb_type, enum pet_op_type *pet_type) {
  if(mmb_type >= MMB_OP_TYPE__MAX) {
    MMB_ERR("Invalid mmbOpType\n");
    return MMB_ERROR;
  }
  *pet_type = mmb_op_to_pet_op_types_table[mmb_type];
  return MMB_OK;
}

mmbError mmb_expr_op_pet_type_size(mmbExpr *expr, int *type_size) {
  mmbError stat = MMB_OK;

  if(!expr) {
    return MMB_INVALID_ARG;
  } 

  if(expr->type != MMB_EXPR_BINARY_OP &&
     expr->type != MMB_EXPR_UNARY_OP) {
    MMB_ERR("Invalid expr type, expected mmb operation\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  int size;
  switch (expr->u.op.type) {
    case MMB_OP_UNDEFINED:
      size = 0;
    break;
    case MMB_OP_PRE_INC:
    case MMB_OP_PRE_DEC:
    case MMB_OP_ASSIGN:
      stat = mmb_expr_pet_type_size(expr->args[0], &size);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to get pet type size for operation argument\n");
        goto BAILOUT;
      }
    break;
    // TODO: This should be evaluated correctly, use 0 for unknown atm
    case MMB_OP_ADD:
    case MMB_OP_SUB:
      size = 0;
    break;
    case MMB_OP_LT:
    case MMB_OP_GT:
      *type_size = -(((int)sizeof(int))*8);
    break;
    default:
      MMB_ERR("Unsupported operation type\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }

BAILOUT:
  return stat;
}

mmbError mmb_expr_pet_type_size(mmbExpr *expr, int *type_size) {
  mmbError stat = MMB_OK;

  if(!expr) {
    return MMB_INVALID_ARG;
  } 

  int size = 0;
  switch(expr->type) {
    case MMB_EXPR_INT:
      size = -(((int)sizeof(int))*8);
    break;
    case MMB_EXPR_UNARY_OP:
    case MMB_EXPR_BINARY_OP:
      stat = mmb_expr_op_pet_type_size(expr, &size);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to extract pet type size for mmb expr op\n");
        goto BAILOUT;
      }
    break;
    case MMB_EXPR_CONST_PAR:
      size = -(((int)sizeof(int))*8);
    break;
    case MMB_EXPR_ITERATOR:
      size = -(((int)sizeof(int))*8);
    break;
    case MMB_EXPR_ACCESS:
      size = 0;
    break;
    default:
      MMB_ERR("Unsupported mmb_expr type\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }
   *type_size = size;

BAILOUT:
  return stat;
}


mmbError mmb_expr_to_pet_expr_int(mmbExpr *mmb_expr, pet_expr **pe_expr) {
  mmbError stat = MMB_OK;

  if(!mmb_expr) {
    return MMB_INVALID_ARG;
  } 

  if(mmb_expr->type != MMB_EXPR_INT) {
    MMB_ERR("Invalid mmbExpr, expexted MMB_EXPR_INT\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  isl_val *i = isl_val_int_from_si(g_isl_ctx, mmb_expr->u.i);
  *pe_expr = pet_expr_new_int(i);
BAILOUT:
  return stat;
}

mmbError mmb_expr_to_pet_expr_unary_op(mmbExpr *mmb_expr, pet_expr **pe_expr) {
  mmbError stat = MMB_OK;

  if(!mmb_expr) {
    return MMB_INVALID_ARG;
  } 

  if(mmb_expr->type != MMB_EXPR_UNARY_OP) {
    MMB_ERR("Invalid mmbExpr, expexted MMB_EXPR_UNARY_OP\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Convert operation type
  enum pet_op_type op_type;
  stat = mmb_op_type_to_pet_op_type(mmb_expr->u.op.type, &op_type); 
  if(stat != MMB_OK) {
    MMB_ERR("Failed to convert mmb op type to pet op type\n");
    goto BAILOUT;
  }

  // Pull out and convert operand
  pet_expr *arg;
  stat = mmb_expr_to_pet_expr(mmb_expr->args[0], &arg);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract pet expr for unary operation mmbExpr (arg)\n");
    goto BAILOUT;
  }

  // Mark arg as read/write if neccesary
  bool assignment;
  stat = mmb_expr_op_is_assignment(mmb_expr, &assignment);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to check mmbExpr for assignment quality\n");
    goto BAILOUT;
  }
  if(assignment && pet_expr_get_type(arg) == pet_expr_access){
    arg = pet_expr_access_set_write(arg, 1);
    arg = pet_expr_access_set_read(arg, 1);
  }
  

  // Build unary op
  int type_size;
  stat = mmb_expr_pet_type_size(mmb_expr, &type_size);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract type size of mmbExpr\n");
    goto BAILOUT;
  }
  pet_expr *pe = pet_expr_new_unary(type_size, op_type, arg);
  if(!pe) {
    MMB_ERR("Failed to build new binary pet expr\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  *pe_expr = pe;
BAILOUT:
  return stat;
}

mmbError mmb_expr_to_pet_expr_binary_op(mmbExpr *mmb_expr, pet_expr **pe_expr) {
  mmbError stat = MMB_OK;

  if(!mmb_expr) {
    return MMB_INVALID_ARG;
  } 

  if(mmb_expr->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Invalid mmbExpr, expected MMB_EXPR_BINARY_OP\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Convert operation type
  enum pet_op_type op_type;
  stat = mmb_op_type_to_pet_op_type(mmb_expr->u.op.type, &op_type); 
  if(stat != MMB_OK) {
    MMB_ERR("Failed to convert mmb op type to pet op type\n");
    goto BAILOUT;
  }

  // Pull out and convert operands
  pet_expr *lhs, *rhs;
  stat = mmb_expr_to_pet_expr(mmb_expr->args[0], &lhs);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract pet expr for binary operation mmbExpr (lhs)\n");
    goto BAILOUT;
  }
  stat = mmb_expr_to_pet_expr(mmb_expr->args[1], &rhs);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract pet expr for operation mmbExpr (rhs)\n");
    goto BAILOUT;
  }

  // Mark lhs as written if neccesary
  bool assignment;
  stat = mmb_expr_op_is_assignment(mmb_expr, &assignment);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to check mmbExpr for assignment quality\n");
    goto BAILOUT;
  }
  if(assignment && pet_expr_get_type(lhs) == pet_expr_access){
    lhs = pet_expr_access_set_write(lhs, 1);
    bool compoundAssignment;
    stat = mmb_expr_op_is_compound_assignment(mmb_expr, &compoundAssignment);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to check mmbExpr for compound assignment quality\n");
      goto BAILOUT;
    }
    lhs = pet_expr_access_set_read(lhs, compoundAssignment);
  }

  // Build binary op
  int type_size;
  stat = mmb_expr_pet_type_size(mmb_expr, &type_size);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract type size of mmbExpr\n");
    goto BAILOUT;
  }
  pet_expr *pe = pet_expr_new_binary(type_size, op_type, lhs, rhs);
  if(!pe) {
    MMB_ERR("Failed to build new binary pet expr\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  *pe_expr = pe;
BAILOUT:
  return stat;
}

mmbError mmb_expr_to_pet_expr_const_par(mmbExpr *mmb_expr, pet_expr **pe_expr) {
  mmbError stat = MMB_OK;

  if(!mmb_expr) {
    return MMB_INVALID_ARG;
  } 

  if(mmb_expr->type != MMB_EXPR_CONST_PAR) {
    MMB_ERR("Invalid mmbExpr, expexted MMB_EXPR_CONST_PAR\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  mmbCpId *cp_id = mmb_expr->u.cpid;
  // Create an index expression with a zero index to the variable represented by id
  isl_space *space;
	space = isl_space_alloc(isl_id_get_ctx(cp_id->id), 0, 0, 0);
	space = isl_space_set_tuple_id(space, isl_dim_out, isl_id_copy(cp_id->id));
  pet_expr *iv_index =  pet_expr_from_index(isl_multi_pw_aff_zero(space));
  // Create an access expr to the loop induction variable, by setting the type
  // size (bit width of variable) and depth of the index
  // NOTE: pet extracts the depth from pe_expr before set_type_size, and sets it again after
  //    is this necessary? see expr_plus.cc:pet_expr_access_from_index for example
  //    int depth = extract_depth(pe_expr);
  // Signed types are represented by negating the type_size
  int size = cp_id->type_signed ? -cp_id->type_size : cp_id->type_size;
  *pe_expr = pet_expr_set_type_size(iv_index, size);
  //  pe_expr = pet_expr_access_set_depth(pe_expr, depth);

BAILOUT:
  return stat;
}

mmbError mmb_expr_to_pet_expr_iterator(mmbExpr *mmb_expr, pet_expr **pe_expr) {
  mmbError stat = MMB_OK;

  if(!mmb_expr) {
    return MMB_INVALID_ARG;
  } 

  if(mmb_expr->type != MMB_EXPR_ITERATOR) {
    MMB_ERR("Invalid mmbExpr, expexted MMB_EXPR_INT\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  mmbItId *it_id = mmb_expr->u.itid;

  // Create an index expression with a zero index to the variable represented by id
  isl_space *space;
	space = isl_space_alloc(isl_id_get_ctx(it_id->id), 0, 0, 0);
	space = isl_space_set_tuple_id(space, isl_dim_out, isl_id_copy(it_id->id));
  pet_expr *iv_index =  pet_expr_from_index(isl_multi_pw_aff_zero(space));
  // Create an access expr to the loop induction variable, by setting the type
  // size (bit width of variable) and depth of the index
  // NOTE: pet extracts the depth from pe_expr before set_type_size, and sets it again after
  //    is this necessary? see expr_plus.cc:pet_expr_access_from_index for example
  //    int depth = extract_depth(pe_expr);
  // Signed types are represented by negating the type_size
  int size = it_id->type_signed ? -it_id->type_size : it_id->type_size;
  *pe_expr = pet_expr_set_type_size(iv_index, size);
  //  pe_expr = pet_expr_access_set_depth(pe_expr, depth);

BAILOUT:
  return stat;
}

mmbError mmb_expr_to_pet_expr(mmbExpr *mmb_expr, pet_expr** pe_expr) {
  if(!mmb_expr) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  switch(mmb_expr->type) {
    case MMB_EXPR_INT:
      stat = mmb_expr_to_pet_expr_int(mmb_expr, pe_expr);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to build pet expr for integer mmbExpr\n");
        goto BAILOUT;
      }
    break;
    case MMB_EXPR_UNARY_OP:
      stat = mmb_expr_to_pet_expr_unary_op(mmb_expr, pe_expr);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to build pet expr for op mmbExpr\n");
        goto BAILOUT;
      }
    break;
    case MMB_EXPR_BINARY_OP:
      stat = mmb_expr_to_pet_expr_binary_op(mmb_expr, pe_expr);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to build pet expr for op mmbExpr\n");
        goto BAILOUT;
      }
    break;
    case MMB_EXPR_CONST_PAR:
      stat = mmb_expr_to_pet_expr_const_par(mmb_expr, pe_expr);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to build pet expr for const par mmbExpr\n");
        goto BAILOUT;
      }
    break;
    case MMB_EXPR_ITERATOR:
      stat = mmb_expr_to_pet_expr_iterator(mmb_expr, pe_expr);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to build pet expr for iterator mmbExpr\n");
        goto BAILOUT;
      }
    break;
    case MMB_EXPR_ACCESS:
      MMB_ERR("Access not yet supported\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
    default:
      MMB_ERR("Unsupported mmb_expr type\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }

BAILOUT:
  return stat;
}

#endif