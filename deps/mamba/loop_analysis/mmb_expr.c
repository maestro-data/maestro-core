/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_expr.h"
#include "mmb_logging.h"
#include "mmb_loop.h"
#include <stdlib.h>
#include <assert.h>


static mmbError create_const_parameter(const char* id, mmbCpId ** cpid) {
  mmbCpId *c = (mmbCpId*)malloc(sizeof(mmbCpId));
  if(!c) 
    return MMB_OUT_OF_MEMORY;
  
  c->id = isl_id_alloc(g_isl_ctx, id, NULL);

  // Currently all constant parameters are signed 32 bit integers
  c->type_signed = 1;
  c->type_size = sizeof(int) * 8;

  *cpid = c;
  return MMB_OK;
}

static mmbError create_iterator(const char* id, mmbItId ** itid) {
  mmbItId *i = (mmbItId*)malloc(sizeof(mmbItId));
  if(!i) 
    return MMB_OUT_OF_MEMORY;
  
  i->id = isl_id_alloc(g_isl_ctx, id, NULL);

  // Currently all iterators are signed 32 bit integers
  i->type_signed = 1;
  i->type_size = sizeof(int) * 8;

  *itid = i;
  return MMB_OK;
}

mmbError mmb_expr_create_int(int i, mmbExpr **out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  e->type = MMB_EXPR_INT;
  e->u.i = i;

  *out_expr = e;
BAILOUT:
 return stat;
}

mmbError mmb_expr_create_const_parameter(const char* id, 
                                          mmbExpr ** out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  e->type = MMB_EXPR_CONST_PAR;

  stat = create_const_parameter(id, &e->u.cpid);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create constant parameter\n");
    goto BAILOUT;
  }

  *out_expr = e;
BAILOUT:
 return stat;
}

mmbError mmb_expr_create_iterator(const char* id, 
                                    mmbExpr ** out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  e->type = MMB_EXPR_ITERATOR;

  stat = create_iterator(id, &e->u.itid);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create iterator\n");
    goto BAILOUT;
  }

  *out_expr = e;
BAILOUT:
 return stat;
}

mmbError mmb_expr_create_unary_op(mmbOpType type, mmbExpr *arg, 
                                  mmbExpr **out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  if(!arg) {
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  // TODO: Would be nice to check if arg is an lvalue here

  e->type = MMB_EXPR_UNARY_OP;
  e->n_args = 1;
  e->args = (mmbExpr **)malloc(sizeof(mmbExpr*)*1);
  if(!e->args) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  mmbExpr *arg_copy;
  stat = mmb_expr_create_copy(arg, &arg_copy);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy arg expr\n");
    goto BAILOUT;
  }

  e->args[0] = arg_copy;
  e->u.op.type = type;
  e->u.op.is_decl = false;

  *out_expr = e;
BAILOUT:
  return stat;
}

mmbError mmb_expr_create_binary_op(mmbOpType type, mmbExpr *lhs,
                                    mmbExpr *rhs, mmbExpr ** out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  if(!lhs || !rhs) {
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  e->type = MMB_EXPR_BINARY_OP;
  e->n_args = 2;
  e->args = (mmbExpr **)malloc(sizeof(mmbExpr*)*2);
  if(!e->args) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  
  mmbExpr *lhs_copy;
  stat = mmb_expr_create_copy(lhs, &lhs_copy);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy lhs expr\n");
    goto BAILOUT;
  }

  mmbExpr *rhs_copy;
  stat = mmb_expr_create_copy(rhs, &rhs_copy);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy rhs expr\n");
    goto BAILOUT;
  }

  e->args[0] = lhs_copy;
  e->args[1] = rhs_copy;
  e->u.op.type = type;
  e->u.op.is_decl = false;

  *out_expr = e;
BAILOUT:
  return stat;
}

mmbError mmb_expr_create_binary_decl_op(mmbOpType type, mmbExpr *lhs,
                                        mmbExpr *rhs, mmbExpr ** out_expr) {
  mmbError stat = MMB_OK;
  stat =  mmb_expr_create_binary_op(type, lhs, rhs, out_expr);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create binary op\n");
    goto BAILOUT;
  }

  (*out_expr)->u.op.is_decl = true;

BAILOUT:
  return stat;
}

mmbError mmb_expr_create_access(mmbExpr *access, mmbExpr ** out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  if(!access) {
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  
  e->type = MMB_EXPR_ACCESS;
  e->n_args = 1;
  e->args = (mmbExpr **)malloc(sizeof(mmbExpr*));
  if(!e->args) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  mmbExpr *access_copy;
  stat = mmb_expr_create_copy(access, &access_copy);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy access expr\n");
    goto BAILOUT;
  }
  e->args[0] = access_copy;
  *out_expr = e;
BAILOUT:
  return stat;
}

mmbError mmb_expr_create_copy(mmbExpr *in_expr, mmbExpr **out_expr) {
  mmbError stat = MMB_OK;
  *out_expr = NULL;
  if(!in_expr)
    return stat;
  
  mmbExpr *e = (mmbExpr*)malloc(sizeof(mmbExpr));
  if(!e ){
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  e->type = in_expr->type;
  switch(in_expr->type) {
    case MMB_EXPR_INT:
      e->u.i = in_expr->u.i;
    break;
    case MMB_EXPR_CONST_PAR:
      e->u.cpid = (mmbCpId*)malloc(sizeof(mmbCpId));
      if(!e->u.cpid) {
        stat = MMB_OUT_OF_MEMORY;
        goto BAILOUT;
      }
      e->u.cpid->id = isl_id_copy(in_expr->u.cpid->id);
      e->u.cpid->type_signed = in_expr->u.cpid->type_signed;
      e->u.cpid->type_size = in_expr->u.cpid->type_size;
    break;
    case MMB_EXPR_ITERATOR:
      e->u.itid = (mmbItId*)malloc(sizeof(mmbItId));
      if(!e->u.itid) {
        stat = MMB_OUT_OF_MEMORY;
        goto BAILOUT;
      }
      e->u.itid->id = isl_id_copy(in_expr->u.itid->id);
      e->u.itid->type_signed = in_expr->u.itid->type_signed;
      e->u.itid->type_size = in_expr->u.itid->type_size;
    break;
    case MMB_EXPR_UNARY_OP:
    case MMB_EXPR_BINARY_OP:
      e->u.op = in_expr->u.op;
      e->args = (mmbExpr **) malloc(sizeof(mmbExpr*)*in_expr->n_args);
      if(!e->args) {
        stat = MMB_OUT_OF_MEMORY;
        goto BAILOUT;
      }
      e->n_args = in_expr->n_args;
      for(unsigned i = 0; i < e->n_args; i++) {
        stat = mmb_expr_create_copy(in_expr->args[i], &e->args[i]);
        if(stat != MMB_OK) {
          MMB_ERR("Failed to copy construct mmbExpr\n");
          goto BAILOUT;
        }
      }
    break;
    case MMB_EXPR_ACCESS:
      e->args = (mmbExpr **) malloc(sizeof(mmbExpr*)*in_expr->n_args);
      if(!e->args) {
        stat = MMB_OUT_OF_MEMORY;
        goto BAILOUT;
      }
      e->n_args = in_expr->n_args;
      for(unsigned i = 0; i < e->n_args; i++) {
        stat = mmb_expr_create_copy(in_expr->args[i], &e->args[i]);
        if(stat != MMB_OK) {
          MMB_ERR("Failed to copy construct mmbExpr\n");
          goto BAILOUT;
        }
      }
    break;
    default:
      MMB_ERR("Expr type not supported\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }
  *out_expr = e;
BAILOUT:
  return stat;
}

mmbError mmb_expr_extend_access_dim(mmbExpr *access, mmbExpr *new_dim_expr) {
  mmbError stat = MMB_OK;
  if(!access || !new_dim_expr) {
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  int nexpr = access->n_args;
  access->args = (mmbExpr **)realloc(access->args, sizeof(mmbExpr*)*(nexpr+1));
  if(!access->args) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  mmbExpr *new_dim_expr_copy;
  stat = mmb_expr_create_copy(new_dim_expr, &new_dim_expr_copy);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy new dimension expr\n");
    goto BAILOUT;
  }
  access->args[nexpr] = new_dim_expr_copy;

  access->n_args++;
BAILOUT:
  return stat;
}

mmbError mmb_expr_op_is_assignment(mmbExpr *expr, bool *assignment) {
  if(!expr) {
    return MMB_INVALID_ARG;
  }
  if(expr->type != MMB_EXPR_BINARY_OP && 
     expr->type != MMB_EXPR_UNARY_OP) {
    MMB_ERR("Invalid expr type, expected mmb operation\n");
    return MMB_ERROR;
  }
  // When support for compound assignments is added, they will be included here
  switch(expr->u.op.type) {
    case MMB_OP_PRE_INC:
    case MMB_OP_PRE_DEC:
    case MMB_OP_ASSIGN:
      *assignment = true;
    break;
    default:
     *assignment = false;
    break;
  }
  return MMB_OK;
}

mmbError mmb_expr_op_is_compound_assignment(mmbExpr *expr, bool *cassignment) {
  if(!expr) {
    return MMB_INVALID_ARG;
  }
  if(expr->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Invalid expr type, expected MMB_EXPR_BINARY_OP\n");
    return MMB_ERROR;
  }
  // When support for compound assignments is added, they will be included here
  switch(expr->u.op.type) {
    default:
     *cassignment = false;
    break;
  }
  return MMB_OK;
}

mmbError mmb_expr_print(mmbExpr *in_expr, int indent) {
  mmbError stat = MMB_OK;
  const char * name;
  if(!in_expr)
    return stat;
  switch(in_expr->type) {
    case MMB_EXPR_INT:
      MMB_DEBUG("%*s- type: int\n", indent,"");
      MMB_DEBUG("%*s- value: %d\n", indent,"",in_expr->u.i);
    break;
    case MMB_EXPR_CONST_PAR:
      MMB_DEBUG("%*s- type: const parameter\n", indent,"");
      name = isl_id_get_name(in_expr->u.cpid->id);
      MMB_DEBUG("%*s- name: %s\n", indent,"", name ? name : "none");
    break;
    case MMB_EXPR_ITERATOR:
      MMB_DEBUG("%*s- type: iterator \n", indent,"");
      name = isl_id_get_name(in_expr->u.itid->id);
      MMB_DEBUG("%*s- name: %s\n", indent,"", name ? name : "none");
    break; 
    case MMB_EXPR_UNARY_OP:
        MMB_DEBUG("%*s- type: unary op \n", indent,"");
      MMB_DEBUG("%*s- op type: %s\n", indent,"",
              mmb_expr_op_type_to_string(in_expr->u.op.type));
      MMB_DEBUG("%*s- n_args: %d \n", indent,"", in_expr->n_args);
      for(unsigned i = 0; i < in_expr->n_args; i++) {
        MMB_DEBUG("%*s- arg %d: \n", indent,"", i);
        mmb_expr_print(in_expr->args[i], indent+2);
      }
    break;
    case MMB_EXPR_BINARY_OP:
      MMB_DEBUG("%*s- type: binary op \n", indent,"");
      MMB_DEBUG("%*s- op type: %s\n", indent,"",
              mmb_expr_op_type_to_string(in_expr->u.op.type));
      MMB_DEBUG("%*s- op is decl: %s\n", indent,"",
              in_expr->u.op.is_decl ? "true" : "false");
      MMB_DEBUG("%*s- n_args: %d \n", indent,"", in_expr->n_args);
      for(unsigned i = 0; i < in_expr->n_args; i++) {
        MMB_DEBUG("%*s- arg %d: \n", indent,"", i);
        mmb_expr_print(in_expr->args[i], indent+2);
      }
    break;
    case MMB_EXPR_ACCESS:
      MMB_DEBUG("%*s- type: access \n", indent,"");
      MMB_DEBUG("%*s- n_args: %d \n", indent,"", in_expr->n_args);
      for(unsigned i = 0; i < in_expr->n_args; i++) {
        MMB_DEBUG("%*s- arg %d: \n", indent,"", i);
        mmb_expr_print(in_expr->args[i], indent+2);
      }
    break;
    default:
    break;
  }

BAILOUT:
  return stat;
}

mmbError mmb_expr_destroy(mmbExpr *in_expr) {
  mmbError stat = MMB_OK;
  if(!in_expr)
    return stat;
  switch(in_expr->type) {
    case MMB_EXPR_INT:
      free(in_expr);
    break;
    case MMB_EXPR_CONST_PAR:
      isl_id_free(in_expr->u.cpid->id);
      free(in_expr->u.cpid);
      free(in_expr);
    break;
    case MMB_EXPR_ITERATOR:
      isl_id_free(in_expr->u.itid->id);
      free(in_expr->u.itid);
      free(in_expr);
    break;
    case MMB_EXPR_UNARY_OP:
      mmb_expr_destroy(in_expr->args[0]);
      free(in_expr->args);
      free(in_expr);
    break;
    case MMB_EXPR_BINARY_OP:
      mmb_expr_destroy(in_expr->args[0]);
      mmb_expr_destroy(in_expr->args[1]);
      free(in_expr->args);
      free(in_expr);
    break;
    case MMB_EXPR_ACCESS:
      for(unsigned i = 0; i < in_expr->n_args; i++)
        mmb_expr_destroy(in_expr->args[i]);
      free(in_expr->args);
      free(in_expr);
    default:
    break;
  }

BAILOUT:
  return stat;
}

mmbError mmb_expr_is_decl(mmbExpr *in_expr, bool* out_result) {
  mmbError stat = MMB_OK;

  if(!in_expr || !out_result) {
    return MMB_INVALID_ARG;
  } 

  if(in_expr->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Invalid mmbExpr, expected MMB_EXPR_BINARY_OP\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  *out_result = in_expr->u.op.is_decl;

BAILOUT:
  return stat;
}
 


static
const char * const expr_access_types_table[MMB_EXPR_ACCESS_TYPE__MAX] = {
  [MMB_EXPR_ACCESS_UNDEFINED]   = "undefined",
  [MMB_EXPR_ACCESS_READ]      = "read",
  [MMB_EXPR_ACCESS_WRITE]       = "write"
};

const char* mmb_expr_access_type_to_string(mmbExprAccessType type) {
  assert(type<MMB_EXPR_ACCESS_TYPE__MAX);
  return expr_access_types_table[type];
}

static
const char * const expr_op_types_table[MMB_OP_TYPE__MAX] = {
  [MMB_OP_UNDEFINED]   = "undefined",
  [MMB_OP_ASSIGN]      = "assign",
  [MMB_OP_ADD]      = "add",
  [MMB_OP_SUB]      = "sub",
  [MMB_OP_LT]       = "less than",
  [MMB_OP_PRE_INC]       = "pre increment",
  [MMB_OP_PRE_DEC]       = "pre decrement"
};

const char* mmb_expr_op_type_to_string(mmbOpType type) {
  assert(type<MMB_OP_TYPE__MAX);
  return expr_op_types_table[type];
}


