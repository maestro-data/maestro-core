
 /*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_LOOP_H
#define MMB_LOOP_H

#include "../common/mmb_logging.h"
#include "../common/mmb_error.h"
#include "mmb_expr.h"
#include "mmb_array_to_isl_id.h"

#include "isl/ctx.h"

#include <stdbool.h>

typedef enum mmbStmtType {
  MMB_STMT_UNDEFINED = 0,
  MMB_STMT_ACCESS,
  MMB_STMT_LOOP,
  MMB_STMT_TYPE__MAX
} mmbStmtType;

struct mmbBlock;

struct mmbLoop;
struct mmbStmt; 
struct mmbLoopNest;
struct mmbArray;

typedef struct mmbLoopId {
  isl_id *id;
  struct mmbLoop *self;
} mmbLoopId;

typedef struct mmbBlock {
  size_t n_stmt;
  struct mmbStmt **stmts;
} mmbBlock;

typedef struct mmbLoopNest {
  mmbBlock* block;
  mmb_data_ref_to_isl_id *data_refs;
} mmbLoopNest;

typedef struct mmbLoop {
  mmbLoopNest *nest;
  mmbBlock* block;
  mmbLoopId id;

  mmbExpr *init;
  mmbExpr *cond;
  mmbExpr *inc;
} mmbLoop;

typedef struct mmbAccess {
  mmbExprAccessType type;
  mmbExpr *expr;
  mmbDataRef *ref;
} mmbAccess;

typedef struct mmbStmt {
  mmbStmtType type;
  union {
    struct mmbAccess access;
    struct mmbLoop *loop;
  }u;
} mmbStmt;

extern isl_ctx *g_isl_ctx;

mmbError mmb_loop_initialize();
mmbError mmb_loop_finalize();

mmbError mmb_block_create(mmbBlock **out_block);
mmbError mmb_block_destroy(mmbBlock *in_block);
mmbError mmb_block_push_stmt(mmbBlock *in_block, mmbStmt *stmt);

mmbError mmb_loop_nest_create(mmbLoopNest **out_loop);
mmbError mmb_loop_nest_destroy(mmbLoopNest *in_loop);
mmbError mmb_loop_nest_add_loop(mmbLoopNest *in_loop, mmbLoopId **id);
mmbError mmb_loop_nest_add_data_ref(mmbLoopNest *in_nest, 
                                    mmbDataRef *in_ref);
mmbError mmb_loop_nest_print(mmbLoopNest *nest);
mmbError mmb_loop_nest_print_data_refs(mmbLoopNest *in_nest);
mmbError mmb_loop_nest_compute(mmbLoopNest *nest);

mmbError mmb_loop_create(mmbLoop **out_loop);
mmbError mmb_loop_destroy(mmbLoop *in_loop);
mmbError mmb_loop_set_loop_init(mmbLoopId *ln, mmbExpr *init);
mmbError mmb_loop_set_loop_cond(mmbLoopId *ln, mmbExpr *cond);
mmbError mmb_loop_set_loop_inc(mmbLoopId *ln, mmbExpr *inc);
mmbError mmb_loop_add_inner_loop(mmbLoopId *in_loop, mmbLoopId **out_loop);
mmbError mmb_loop_add_access(mmbLoopId *in_loop, mmbDataRef *in_ref, 
                              mmbExpr *access, mmbExprAccessType type);

mmbError mmb_stmt_destroy(mmbStmt *stmt);
mmbError mmb_stmt_print(mmbStmt *stmt, int indent);

#endif
