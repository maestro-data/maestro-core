/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_loop.h"
#include "mmb_logging.h"
#include "mmb_error.h"
#include "mmb_array_to_isl_id.h"
#include "mmb_to_pet_tree.h"

#include "stdlib.h"
#include "stdbool.h"
#include "assert.h"
#include "string.h"

#include "isl/ctx.h"
#include "isl/options.h"
#include "isl/set.h"
#include "isl/union_set.h"
#include "isl/space.h"
#include "isl/printer.h"
#include "isl/id.h"


#include "pet.h"
#include "pet/tree.h"
#include "pet/tree2scop.h"
#include "pet/expr.h"

#include "loop_analyzer.h"


isl_ctx *g_isl_ctx = NULL;
static bool mmb_loop_initialized = false;
static struct isl_printer *yaml_printer;

la_init_args initargs;
struct la_context* g_lactx;

static inline mmbError init_isl() {
  mmbError stat = MMB_OK;
  g_isl_ctx = isl_ctx_alloc_with_pet_options();
  isl_options_set_on_error(g_isl_ctx, ISL_ON_ERROR_ABORT);
BAILOUT:
  return stat;
}

/**
 * Should be private
 */
mmbError mmb_loop_initialize() {
  mmbError stat = MMB_OK;
  if(mmb_loop_initialized) {
    MMB_ERR("mmb_loop_initialize called while already initialized\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  stat = init_isl();
  if(stat != MMB_OK) {
    MMB_ERR("Unabled to initialize ISL\n");
    goto BAILOUT;
  }

  yaml_printer = isl_printer_to_str(g_isl_ctx);
  yaml_printer = isl_printer_set_yaml_style(yaml_printer, 
                                              ISL_YAML_STYLE_BLOCK);

  initargs.islctx = g_isl_ctx;
  initargs.verbose = true;
  la_status lastat = la_init(&initargs, &g_lactx);
  mmb_loop_initialized = true;

BAILOUT:
  return stat;
}

mmbError mmb_loop_finalize() {
  mmbError stat = MMB_OK;
  if(!mmb_loop_initialized) {
    MMB_ERR("mmb_loop_finalize called while not initialized\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  la_status lastat = la_finalize(g_lactx);

  isl_printer_free(yaml_printer);
  isl_ctx_free(g_isl_ctx);  
  mmb_loop_initialized = false;
BAILOUT:
  return stat;
}

mmbError mmb_block_create(mmbBlock **out_block) {
  mmbError stat = MMB_OK;
  *out_block = NULL;
  mmbBlock *b = (mmbBlock*)calloc(1, sizeof(mmbBlock));
  if(!b) {
    return MMB_OUT_OF_MEMORY;
  } 
  *out_block = b;
  return stat;
}

mmbError mmb_block_destroy(mmbBlock *in_block) {
  mmbError stat = MMB_OK;
  for(unsigned i = 0; i < in_block->n_stmt; i++) {
    stat = mmb_stmt_destroy(in_block->stmts[i]);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to destroy stmt in block\n");
      goto BAILOUT;
    }
  }
  free(in_block);
BAILOUT:
  return stat;
}

mmbError mmb_block_push_stmt(mmbBlock *in_block, mmbStmt *stmt) {
  if(!stmt) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  size_t pos = in_block->n_stmt;
  in_block->stmts = (mmbStmt**) realloc(in_block->stmts, sizeof(mmbStmt*) * 
                                        (in_block->n_stmt+1));
  if(!in_block->stmts) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  } 
  in_block->stmts[pos] = stmt;
  in_block->n_stmt++;
BAILOUT:
  return stat;
}

mmbError mmb_loop_nest_create(mmbLoopNest **out_loop) {
  mmbError stat = MMB_OK;
  *out_loop = NULL;
  mmbLoopNest *l = (mmbLoopNest*)calloc(1, sizeof(mmbLoopNest));
  if(!l) {
    return MMB_OUT_OF_MEMORY;
  }
  stat = mmb_block_create(&l->block);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create block in loop nest\n");
    goto BAILOUT;
  }

  l->data_refs = mmb_data_ref_to_isl_id_alloc(g_isl_ctx, 1);

  *out_loop = l;
BAILOUT:
  return stat;
}

mmbError mmb_loop_nest_destroy(mmbLoopNest *in_loop) {
  if(!in_loop) {
    return MMB_OK;
  }
  mmbError stat = mmb_block_destroy(in_loop->block);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to destroy block in loop nest\n");
    goto BAILOUT;
  }

  mmb_data_ref_to_isl_id_free(in_loop->data_refs);

  free(in_loop);  
BAILOUT:
  return stat;
}

mmbError mmb_loop_nest_add_data_ref(mmbLoopNest *in_nest, 
                                    mmbDataRef *in_ref) {
  mmbError stat = MMB_OK;
  if(!in_nest || !in_ref) {
    return MMB_INVALID_ARG;
  }
  if(!in_ref->ptr) {
    MMB_ERR("Data reference may not refer to a null pointer\n");
    return MMB_INVALID_ARG;
  }
  // Check if the reference already exists
  isl_bool exists = 
    mmb_data_ref_to_isl_id_has(in_nest->data_refs, in_ref);
  
  if(exists == isl_bool_false) {
    // Add to data_ref -> isl_id hash map
    isl_id *consumable_id = isl_id_copy(in_ref->id);
    mmbDataRef *consumable_ref = mmbDataRef_copy(in_ref);
    in_nest->data_refs = 
      mmb_data_ref_to_isl_id_set(in_nest->data_refs, 
                                 consumable_ref, consumable_id);
  } 
BAILOUT:
  return stat;
}

static isl_stat print_data_ref(__isl_take mmbDataRef *ar, 
                                __isl_take isl_id *id, void* usr) {
  mmbError stat = mmb_data_ref_print(ar, 0);
  if(stat != MMB_OK){
    MMB_ERR("Failed to print data reference\n");
  }
  stat = mmb_data_ref_destroy(ar);
  if(stat != MMB_OK){
    MMB_ERR("Failed to free data reference\n");
  }
  isl_id_free(id);
  return isl_stat_ok;
}

mmbError mmb_loop_nest_print_data_refs(mmbLoopNest *in_nest) {
  if(!in_nest) {
    return MMB_INVALID_ARG;
  }
  MMB_DEBUG("Loop nest data refs:\n");
  mmb_data_ref_to_isl_id_foreach(in_nest->data_refs, &print_data_ref, NULL);

  return MMB_OK;
}

mmbError mmb_loop_nest_add_loop(mmbLoopNest *in_loop, mmbLoopId **id) {
  if(!in_loop) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  *id = NULL;
  /* Create new loop */
  mmbLoop *l;
  stat = mmb_loop_create(&l);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mmbLoop\n");
    goto BAILOUT;
  }
  l->nest = in_loop;
  /* Add to loop nest */
  mmbStmt *loop_stmt = calloc(1, sizeof(mmbStmt));
  if(!loop_stmt) {
    return MMB_OUT_OF_MEMORY;
  }
  loop_stmt->type = MMB_STMT_LOOP;
  loop_stmt->u.loop = l;

  stat = mmb_block_push_stmt(in_loop->block, loop_stmt);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to add loop stmt to loop nest\n");
    goto BAILOUT;
  }
  *id = &l->id;
BAILOUT:
  return stat;
}

mmbError mmb_loop_nest_print(mmbLoopNest *nest) {
  if(!nest) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  MMB_DEBUG("- NEST\n");
  int indent = 0;
  for(unsigned i = 0; i < nest->block->n_stmt; i++) {
    stat = mmb_stmt_print( nest->block->stmts[i], indent+1);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to output mmbStmt in loop nest\n");
      goto BAILOUT;
    }
  }

  stat = mmb_loop_nest_print_data_refs(nest);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to print data refs in loop nest\n");
    goto BAILOUT;
  }
BAILOUT:
  return stat;
}

mmbError mmb_loop_create(mmbLoop **out_loop) {
  *out_loop = NULL;
  mmbError stat = MMB_OK;
  mmbLoop *l = calloc(1, sizeof(mmbLoop));
  if(!l) {
    return MMB_OUT_OF_MEMORY;
  }

  l->id.id = isl_id_alloc(g_isl_ctx, NULL, l);
  if(!l->id.id) {
    MMB_ERR("Failed to allocate ISL id for loop\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  l->id.self = l;

  stat = mmb_block_create(&l->block);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create block in loop\n");
    goto BAILOUT;
  }
  
  *out_loop = l;
BAILOUT:
  return stat;
}

mmbError mmb_loop_destroy(mmbLoop *in_loop) {
  if(!in_loop) 
    return MMB_OK;

  mmbError stat = mmb_block_destroy(in_loop->block);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to destroy block in loop nest\n");
    goto BAILOUT;
  }

  isl_id_free(in_loop->id.id);

  stat = mmb_expr_destroy(in_loop->init);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to destroy loop init expr\n");
    goto BAILOUT;
  }
  stat = mmb_expr_destroy(in_loop->cond);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to destroy loop cond expr\n");
    goto BAILOUT;
  }  
  stat = mmb_expr_destroy(in_loop->inc);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to destroy loop inc expr\n");
    goto BAILOUT;
  }

BAILOUT:
  return stat;
}


mmbError mmb_loop_set_loop_init(mmbLoopId *lid, mmbExpr *init) {
  if(!lid) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  lid->self->init = init;
BAILOUT:
  return stat;
}

mmbError mmb_loop_set_loop_cond(mmbLoopId *lid, mmbExpr *cond) {
  if(!lid) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  lid->self->cond = cond;
BAILOUT:
  return stat;
}

mmbError mmb_loop_set_loop_inc(mmbLoopId *lid, mmbExpr *inc) {
  if(!lid) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  lid->self->inc = inc;
BAILOUT:
  return stat;
}

mmbError mmb_loop_add_inner_loop(mmbLoopId *in_loop, mmbLoopId **out_loop) {
  if(!in_loop) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  *out_loop = NULL;
  /* Create new loop */
  mmbLoop *l;
  stat = mmb_loop_create(&l);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mmbLoop\n");
    goto BAILOUT;
  }
  l->nest = in_loop->self->nest;
  /* Add to loop nest */
  mmbStmt *loop_stmt = calloc(1, sizeof(mmbStmt));
  if(!loop_stmt) {
    return MMB_OUT_OF_MEMORY;
  }
  loop_stmt->type = MMB_STMT_LOOP;
  loop_stmt->u.loop = l;


  stat = mmb_block_push_stmt(in_loop->self->block, loop_stmt);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to add inner loop to loop\n");
    goto BAILOUT;
  }
  *out_loop = &l->id;

BAILOUT:
  return stat;
}


mmbError mmb_loop_add_access(mmbLoopId *in_loop, mmbDataRef *in_ref, 
                              mmbExpr *in_access, mmbExprAccessType in_type) {
  if(!in_loop || !in_ref) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  // Register in_ref as a data ref in the loopnest
  stat = mmb_loop_nest_add_data_ref(in_loop->self->nest, in_ref);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to add data ref to loop nest\n");
    goto BAILOUT;
  }
  // Create an access statement with in_ref and in_access expr
  mmbStmt *access_stmt = calloc(1, sizeof(mmbStmt));
  if(!access_stmt) {
    return MMB_OUT_OF_MEMORY;
  }
  access_stmt->type = MMB_STMT_ACCESS;

  access_stmt->u.access.type = in_type;
  mmbExpr *access_copy;
  stat = mmb_expr_create_copy(in_access, &access_copy);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy access expr\n");
    goto BAILOUT;
  }
  access_stmt->u.access.expr = access_copy;

  access_stmt->u.access.ref = mmbDataRef_copy(in_ref);
  if(access_stmt->u.access.ref == NULL) {
    MMB_ERR("Failed to copy data reference\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  
  stat = mmb_block_push_stmt(in_loop->self->block, access_stmt);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to push access statement to loop block\n");
    goto BAILOUT;
  }

BAILOUT:
  return stat;
}

mmbError mmb_loop_print(mmbLoop *in_loop, int indent) {
  if(!in_loop) {
    return MMB_INVALID_ARG;
  }
  mmbError stat = MMB_OK;
  MMB_DEBUG("%*s- LOOP:\n",indent,"");
  indent += 2;
  MMB_DEBUG("%*s- INIT:\n",indent,"");
  stat = mmb_expr_print(in_loop->init, indent+2);
  if(stat != MMB_OK) {
    MMB_DEBUG("Failed to print mmbExpr for loop init\n");
    goto BAILOUT;
  }
  MMB_DEBUG("%*s- COND:\n",indent,"");
  stat = mmb_expr_print(in_loop->cond, indent+2);
  if(stat != MMB_OK) {
    MMB_DEBUG("Failed to print mmbExpr for loop cond\n");
    goto BAILOUT;
  }
  MMB_DEBUG("%*s- INC:\n",indent,"");
  stat = mmb_expr_print(in_loop->inc, indent+2);
  if(stat != MMB_OK) {
    MMB_DEBUG("Failed to print mmbExpr for loop inc\n");
    goto BAILOUT;
  }
  for(unsigned i = 0; i < in_loop->block->n_stmt; i++) {
    stat = mmb_stmt_print( in_loop->block->stmts[i], indent+2);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to output mmbStmt in loop nest\n");
      goto BAILOUT;
    }
  }
BAILOUT:
  return stat;
}

// This needs to create a pet_array from an access expression
static struct pet_array *extract_array(__isl_keep pet_expr *access,
		__isl_keep pet_context *pc, void *user) {
    
  // First extract isl_id from access
  isl_id *id = pet_expr_access_get_id(access);

  // Create array object
  struct pet_array *array;
	array = isl_calloc_type(g_isl_ctx, struct pet_array);
	if (!array)
		return NULL;    

  // Extract depth again here
  // Unclear if its ok to just pull depth out of access like this..
  int depth = access->acc.depth;

  isl_space *space = isl_space_set_alloc(g_isl_ctx, 0, depth);
	space = isl_space_set_tuple_id(space, isl_dim_set, isl_id_copy(id));

  // Initialise extent and context
	array->extent = isl_set_nat_universe(space);
	space = isl_space_params_alloc(g_isl_ctx, 0);
	array->context = isl_set_universe(space);

  // Set upper bounds here, which are stored as args to a pet call expr
  // we dont know the array type - so upper bound 
  // is going to be infinity for all dimensions

  // pet_expr *inf = pet_expr_new_int(isl_val_infty(g_isl_ctx));
  // pet_expr *bounds = pet_expr_new_call(g_isl_ctx, "bounds", depth);
  // for(unsigned i = 0; i < depth; i++) 
  //   bounds = pet_expr_set_arg(bounds, i, pet_expr_copy(inf));
  // pet_expr_free(inf);

  // For non-infinate dimensions we would convert bounds to affine expressions 
  // and update the array with these. We wont do this currently as all dimensions
  // are infinite. See PET: scan.cc PetScan::set_upper_bounds()

  // We arent doing anything about types yet here, so the typename is blank
  char * name = strdup("<unnamed>");
  // Type of element is used in array comparisons, 
  // this appears to be the only place it matters
	array->element_type = name;
  // If the element is a struct/union/class
	array->element_is_record = 0; // in PET this is set to base->isRecordType();
  // Should get this in a better way
	array->element_size = abs(pet_expr_get_type_size(access) / 8);//in PET this is set to size_in_bytes(ast_context, base);

  return array;
}

// This function should extract the size of array accessed by pet_expr access
// So far only unknown-size arrays are supported, so this function either returns
// an empty expr (for depth 0 scalars), or an all-infinite expr (for depth n arrays)
static __isl_give pet_expr *get_array_size(__isl_keep pet_expr *access,
	void *user) {
  // Extract depth again here
  // Unclear if its ok to just pull depth out of access like this..
  int depth = access->acc.depth;

  // Set upper bounds here, which are stored as args to a pet call expr
  // we dont know the array type - so upper bound 
  // is going to be infinity for all dimensions
  pet_expr *inf = pet_expr_new_int(isl_val_infty(g_isl_ctx));
  pet_expr *bounds = pet_expr_new_call(g_isl_ctx, "bounds", depth);
  for(unsigned i = 0; i < depth; i++) 
    bounds = pet_expr_set_arg(bounds, i, pet_expr_copy(inf));
  pet_expr_free(inf);

  return bounds;
}


struct extract_arrays_from_data_refs_info {
  int i;
  struct pet_array ** arrays;
};

static isl_stat extract_arrays_from_data_refs(__isl_take mmbDataRef *ar, 
                                __isl_take isl_id *id, void* usr) {
  
  struct extract_arrays_from_data_refs_info* info = 
    (struct extract_arrays_from_data_refs_info*)usr;

  // Create pet array from data ref, in same way as extract_array()
  struct pet_array *array;
	array = isl_calloc_type(g_isl_ctx, struct pet_array);
	if (!array)
		return isl_stat_error;  
  int depth = ar->dim;
  isl_space *space = isl_space_set_alloc(g_isl_ctx, 0, depth);
	space = isl_space_set_tuple_id(space, isl_dim_set, isl_id_copy(id));
  array->extent = isl_set_nat_universe(space);
	space = isl_space_params_alloc(g_isl_ctx, 0);
	array->context = isl_set_universe(space);
  char * name = strdup("<unnamed>");
	array->element_type = name;
	array->element_is_record = 0; 
	array->element_size = ar->type_size;
  info->arrays[info->i++] = array;

  mmbError stat = mmb_data_ref_destroy(ar);
  if(stat != MMB_OK){
    MMB_ERR("Failed to free data reference\n");
    return isl_stat_error;
  }
  isl_id_free(id);
  return isl_stat_ok;
}


static isl_stat count_data_refs(__isl_take mmbDataRef *ar, 
                                __isl_take isl_id *id, void* usr) {
  
  int* i = (int*)usr;
  (*i)++; 
  mmbError stat = mmb_data_ref_destroy(ar);
  if(stat != MMB_OK){
    MMB_ERR("Failed to free data reference\n");
    return isl_stat_error;
  }
  isl_id_free(id);
  return isl_stat_ok;
}

pet_scop *scan_arrays(pet_scop *scop,  pet_context *pc, mmbLoopNest *nest) {
  if(!scop) 
    return NULL;

  // Check if there are existing arrays in the scop_array list, there shouldnt be
  // Its unclear where in pet arrays might be added to scop->arrays before scan_arrays()
  // Pet simply removes them and readds them, in any case, we dont handle them. 
  mmbError stat = MMB_OK;
  if(scop->n_array > 0) {
    MMB_ERR("Not expecting arrays in scop before scan_arrays\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Collect a list of all accessed arrays or scalars in the scop
  // We can use the existing list of data refs in the loop nest
  // First count list of data refs, and realloc scop array list accordingly 
  int n_data_ref = 0;
  isl_stat istat = mmb_data_ref_to_isl_id_foreach(nest->data_refs, 
                                                  &count_data_refs, &n_data_ref);
  if(istat != isl_stat_ok) {
    MMB_ERR("Failed to count data refs in loop nest\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
	struct pet_array **scop_arrays;
	scop_arrays = isl_realloc_array(g_isl_ctx, scop->arrays, struct pet_array *,
				                          n_data_ref);
	if (!scop_arrays) {
    MMB_ERR("Failed to realloc scop array list\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  
  // For each ref in in_nest->data_refs, call pet_array_from_data_ref and add to the list
  struct extract_arrays_from_data_refs_info info;
  info.i = 0;
  info.arrays = scop_arrays;
  istat = mmb_data_ref_to_isl_id_foreach(nest->data_refs, 
                                         &extract_arrays_from_data_refs, &info);

	scop->arrays = scop_arrays;
  scop->n_array = n_data_ref;
  // Intersect the context of the scop with the context of the array
  for(unsigned i = 0; i < scop->n_array; i++) {
    scop->context = isl_set_intersect(scop->context,
						isl_set_copy(scop->arrays[i]->context));
    if (!scop->context) {
      MMB_ERR("Failed to intersect scop context with array context\n");
      return NULL;
    }
  }
  // Note that we dont handle nested objects (structs) or care about typenames etc here

  // Return the scop
BAILOUT:
  if(stat != MMB_OK)
    return NULL;
  return scop;
}

mmbError mmb_loop_nest_compute(mmbLoopNest *nest) {
  mmbError stat = MMB_OK;
  // Build a pet tree from the loop nest
  pet_tree *tree;
  stat = extract_pet_tree_from_nest(nest, &tree);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to extract pet_tree from nest\n");
    goto BAILOUT;
  }

  MMB_DEBUG("Pet tree:\n");
  pet_tree_dump_with_indent(tree, 2);

  // Create a pet context for scop extraction
  isl_set *domain = isl_set_universe(isl_space_set_alloc(g_isl_ctx, 0, 0));
  pet_context *pc = pet_context_alloc(domain);
  // Add each parameter in the tree to the context
  pc = pet_context_add_parameters(pc, tree, &get_array_size, pc);
  // Convert the tree to a scop
  int int_size = sizeof(int);
	pet_scop *scop = pet_scop_from_pet_tree(tree, int_size,
				                                  &extract_array, nest, pc);
  // Add all pet arrays to the scop here
  scop = scan_arrays(scop, pc, nest);
  if(!scop) {
    MMB_ERR("Failed to build pet_scop from pet_tree\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  int n = isl_set_dim(scop->context, isl_dim_param);
  MMB_DEBUG("Number of param dims: %d\n", n);

  MMB_DEBUG("Successfully created scop\n");

  if(scop->schedule) {
    yaml_printer = isl_printer_print_schedule(yaml_printer, scop->schedule);
    printf("Scop schedule:\n%s\n", isl_printer_get_str(yaml_printer));
    yaml_printer = isl_printer_flush(yaml_printer);
  } else {
    MMB_ERR("No schedule found in scop\n");
  }

  for (unsigned i = 0; i < scop->n_stmt; i++) {
    printf("Dumping statement %u\n", i);
    pet_tree_dump_with_indent(scop->stmts[i]->body, 2);
  }

  printf("\nLoop analysis:\n\n");

  la_status lastat = la_analyze_scop(scop, g_lactx);
  if(lastat != LA_OK) {
    MMB_ERR("Failed to analyze scop via la_analyze_scop\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }


  pet_scop_free(scop);
  pet_context_free(pc);

BAILOUT:
  return stat;
}

mmbError mmb_stmt_destroy(mmbStmt *stmt) {
  if(!stmt) 
    return MMB_OK;

  mmbError stat = MMB_OK;
  switch(stmt->type) {
    case MMB_STMT_ACCESS:
      stat = mmb_expr_destroy(stmt->u.access.expr);
      if(stat != MMB_OK) {
        MMB_ERR("Unable to destroy access stmt expr\n");
        goto BAILOUT;
      }
      stat = mmb_data_ref_destroy(stmt->u.access.ref);
      if(stat != MMB_OK) {
        MMB_ERR("Unable to destroy access stmt data ref\n");
        goto BAILOUT;
      }
    break;
    case MMB_STMT_LOOP:
      stat = mmb_loop_destroy(stmt->u.loop);
      if(stat != MMB_OK) {
        MMB_ERR("Unable to destroy loop stmt\n");
        goto BAILOUT;
      }
    break;
    default:
      MMB_ERR("Unsupported mmbStmt type\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }

BAILOUT:
  return stat;
}


mmbError mmb_stmt_print(mmbStmt *stmt, int indent) {
  if(!stmt) 
    return MMB_INVALID_ARG;

  mmbError stat = MMB_OK;
  MMB_DEBUG("%*s- STMT:\n",indent,"");
  indent+=2;
  switch(stmt->type) {
    case MMB_STMT_ACCESS:
      MMB_DEBUG("%*s- ACCESS:\n",indent,"");
      indent += 2;
      MMB_DEBUG("%*s- type: access\n",indent,"");
      MMB_DEBUG("%*s- access type: %s\n",indent,"",
            mmb_expr_access_type_to_string(stmt->u.access.type));
      MMB_DEBUG("%*s- ref:\n",indent,"");
      stat = mmb_data_ref_print(stmt->u.access.ref,indent+2);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to print data ref in access stmt\n");
        goto BAILOUT;
      }
      MMB_DEBUG("%*s- access expr:\n",indent,"");
      stat = mmb_expr_print(stmt->u.access.expr, indent+2);
      if(stat != MMB_OK) {
        MMB_DEBUG("Failed to print mmbExpr\n");
        goto BAILOUT;
      }
    break;
    case MMB_STMT_LOOP:
      stat = mmb_loop_print(stmt->u.loop, indent+2);
      if(stat != MMB_OK) {
        MMB_ERR("Unable to destroy loop stmt\n");
        goto BAILOUT;
      }
    break;
    default:
      MMB_ERR("Unsupported mmbStmt type\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }

BAILOUT:
  return stat;
}


