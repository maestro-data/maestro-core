/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_TO_PET_TREE_H
#define MMB_TO_PET_TREE_H
#include "mmb_to_pet_tree.h"

#include "mmb_to_pet_expr.h"

#include "isl/id.h"
#include "pet/expr.h"
#include "pet/loc.h"

#include <stdbool.h>
#include <string.h>

static int g_line_no = 0;
/**
  * Create a pet expr representing an index expression for the access to loop
  * induction variable which is stored as an iterator in the lhs of the init 
  * operation
  */
static mmbError extract_pe_iv_from_loop_init(mmbLoop *loop, pet_expr **pe_iv) {
  mmbError stat = MMB_OK;
  if(!loop) 
    return MMB_INVALID_ARG;

  if(loop->init->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Malformed loop init expr: init must be an operation\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  if(loop->init->args[0]->type != MMB_EXPR_ITERATOR) {
    MMB_ERR("Malformed loop init expr: init must initialise an iterator\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  mmbItId *mmb_iv;
  mmb_iv = loop->init->args[0]->u.itid; 

  pet_expr *iv_access;
  stat = mmb_expr_to_pet_expr(loop->init->args[0], &iv_access);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract pet extr from iterator mmbExpr\n");
    goto BAILOUT;
  }

  // Mark as a write access
  iv_access = pet_expr_access_set_write(iv_access, 1);
	iv_access = pet_expr_access_set_read(iv_access, 0);

  *pe_iv = iv_access;
BAILOUT:
  return stat;
}

/**
 *  Create a pet expr representing the initialisation expression assigned to the 
 *  loop induction variable during loop init   
 */
static mmbError extract_pe_init_from_loop_init(mmbLoop *loop, pet_expr **pe_init) {
  mmbError stat = MMB_OK;
  if(!loop) 
    return MMB_INVALID_ARG;

  if(loop->init->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Malformed loop init expr: init must be an operation\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Pull mmbExpr from rhs of loop init
  mmbExpr* init_rhs = loop->init->args[1];
  // Convert to pet expr
  stat = mmb_expr_to_pet_expr(init_rhs, pe_init);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to build pet expr from mmb_expr\n");
    goto BAILOUT;
  }
  

BAILOUT:
  return stat;
}

/**
 *  Create a pet expr representing the condition expression in loop->cond
 */
static mmbError extract_pe_cond_from_loop_cond(mmbLoop *loop, pet_expr **pe_cond) {
  mmbError stat = MMB_OK;
  if(!loop) 
    return MMB_INVALID_ARG;

  if(loop->cond->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Malformed loop cond expr: condition must be an operation\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Convert to pet expr
  stat = mmb_expr_to_pet_expr(loop->cond, pe_cond);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to build pet expr from mmb_expr\n");
    goto BAILOUT;
  }
  
BAILOUT:
  return stat;
}

static mmbError extract_pe_inc_from_loop_inc(mmbLoop *loop, pet_expr **pe_inc) {
  mmbError stat = MMB_OK;
  if(!loop) 
    return MMB_INVALID_ARG;

  if(loop->inc->type != MMB_EXPR_BINARY_OP && 
     loop->inc->type != MMB_EXPR_UNARY_OP) {
    MMB_ERR("Malformed loop inc expr: must be an operation\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  if(loop->inc->args[0]->type != MMB_EXPR_ITERATOR) {
    MMB_ERR("Malformed loop inc expr: must be an operation on an iterator\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  mmbItId *operand;
  operand = loop->inc->args[0]->u.itid;

  // Confirm loop init is valid then 
  // check increment operand is the loop induction variable 
  if(loop->init->type != MMB_EXPR_BINARY_OP) {
    MMB_ERR("Malformed loop init expr: init must be an operation\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  if(loop->init->args[0]->type != MMB_EXPR_ITERATOR) {
    MMB_ERR("Malformed loop init expr: init must initialise an iterator\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  mmbItId *iv;
  iv = loop->init->args[0]->u.itid; 

  if(iv->id != operand->id) {
    MMB_ERR("Malformed loop inc expr: inc must increment the loop induction variable\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  
  // Extract the increment value
  // Only constant integer addition or subtraction supported
  isl_val *v;
  switch(loop->inc->u.op.type) {
    // case MMB_OP_ADD_ASSIGN:

    // break;
    // case MMB_OP_SUB_ASSIGN:

    // break;
    case MMB_OP_PRE_INC:
      v = isl_val_one(g_isl_ctx);
    break;
    case MMB_OP_PRE_DEC:
      v = isl_val_negone(g_isl_ctx);
    break;
    default:
      MMB_ERR("Unsupported operation in loop increment expression\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break; 
  }


  // Convert to pet expr
  pet_expr *e = pet_expr_new_int(v);
  if(!e) {
    MMB_ERR("Failed to extract pet expression from integer\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  *pe_inc = e;

BAILOUT:
  return stat;
}

mmbError extract_pet_tree_from_nest(mmbLoopNest *nest, pet_tree **tree) {
  mmbError stat = MMB_OK;

  if(nest->block->n_stmt != 1) {
    MMB_ERR("Only exactly one statment in loop nest currently supported\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  if(nest->block->stmts[0]->type != MMB_STMT_LOOP) {
    MMB_ERR("Statement in loop nest must be outer loop\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Reserve space in the tree for one statement 
  pet_tree *t = pet_tree_new_block(g_isl_ctx, 1, 1);

  pet_tree *t_child;
  stat = extract_pet_tree_from_stmt(nest->block->stmts[0], &t_child);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to extract pet tree from outer loop\n");
    goto BAILOUT;
  }
  t = pet_tree_block_add_child(t, t_child);
  if(!t) {
    MMB_ERR("Failed to add child tree to loop nest pet tree\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  
  *tree = t;
BAILOUT:
  return stat;
}

mmbError extract_pet_tree_from_stmt(mmbStmt *stmt, pet_tree **tree) {
  mmbError stat = MMB_OK;

  pet_tree *t;
  switch(stmt->type) {
    case MMB_STMT_ACCESS:
      stat = extract_pet_tree_from_access_stmt(&stmt->u.access, &t);
      if(stat != MMB_OK) {
        MMB_ERR("Unable to extract pet_tree from access statement\n");
        goto BAILOUT;
      }
    break;
    case MMB_STMT_LOOP:
      stat = extract_pet_tree_from_loop_stmt(stmt->u.loop, &t);
      if(stat != MMB_OK) {
        MMB_ERR("Unable to extract pet_tree from loop statement\n");
        goto BAILOUT;
      }
    break;
    default:
      MMB_ERR("Unsupported statement in loop\n");
    break;
  }
  
  *tree = t;
BAILOUT:
  return stat;
}


mmbError extract_pet_tree_from_access_stmt(mmbAccess *access, pet_tree **tree) {
  mmbError stat = MMB_OK;

  if(!access) {
    return MMB_INVALID_ARG;
  }

  // Create index expr for access to data ref 
  isl_space *space;
	space = isl_space_alloc(isl_id_get_ctx(access->ref->id), 0, 0, 0);
	space = isl_space_set_tuple_id(space, isl_dim_out, isl_id_copy(access->ref->id));
  pet_expr *ref_expr =  pet_expr_from_index(isl_multi_pw_aff_zero(space));
  
  if(!ref_expr) {
    MMB_ERR("Failed to create expr from access expr data reference\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  
  // If array, extend the expression with the subscript access
  if(access->ref->type == MMB_DATA_REF_ARRAY) {
    // Create index expr for first dimension of access
    pet_expr *ref_index_expr;
    stat = mmb_expr_to_pet_expr(access->expr->args[0], &ref_index_expr);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to create index expr from access expr arg\n");
      goto BAILOUT;
    }
    // Combine the two
    ref_expr = pet_expr_access_subscript(ref_expr, ref_index_expr);

    // Extend the express with any further dimensions of the array access
    for(unsigned i = 1; i < access->expr->n_args; i++) {
      pet_expr *dim_index;
      stat = mmb_expr_to_pet_expr(access->expr->args[i], &dim_index);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to create index expr from access expr dim %d\n", i);
        goto BAILOUT;
      }
      ref_expr = pet_expr_access_subscript(ref_expr, dim_index);
      if(!ref_expr) {
        MMB_ERR("Failed to extend dimension of access expr\n");
        stat = MMB_ERROR;
        goto BAILOUT;
      }
    }
  }

  // pet_expr *pe_access;
  // Assuming all data references are signed...
  int type_size = -access->ref->type_size * 8;
  pet_expr *access_expr = pet_expr_set_type_size(ref_expr, type_size);
  // Depth of array mmbAccesses are equal to number of args in the access expr
  // Depth of scalar mmbAccesses is 1
  int depth = (access->ref->type == MMB_DATA_REF_ARRAY) ? access->expr->n_args : 1;
  access_expr = pet_expr_access_set_depth(access_expr, depth);

  // Mark as a read or write
  switch(access->type) {
    case MMB_EXPR_ACCESS_READ:
      access_expr = pet_expr_access_set_read(access_expr, 1);
    break;
    case MMB_EXPR_ACCESS_WRITE:
      // Pet access exprs are by default reads, so must set read to 0
      access_expr = pet_expr_access_set_read(access_expr, 0);
      access_expr = pet_expr_access_set_write(access_expr, 1);
    break;
    default:
      MMB_ERR("Unsupported access type\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }


  // Build a tree
  // see extract(__isl_take pet_expr...)
  pet_tree *expr_tree = pet_tree_new_expr(access_expr);
  if(!expr_tree) {
    MMB_ERR("Failed to create access expression tree\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  char *indent = strdup("");
  if(!indent) {
    MMB_ERR("Failed to alloc memory for indent\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  pet_loc *expr_loc = pet_loc_alloc(g_isl_ctx, 0, 1, g_line_no, indent);
  if(!expr_loc) {
    MMB_ERR("Failed to create access expression location\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  g_line_no++;
  *tree = pet_tree_set_loc(expr_tree, expr_loc);

BAILOUT:
  return MMB_OK;
}

mmbError extract_pet_tree_from_loop_stmt(mmbLoop *loop, pet_tree **tree) {
  mmbError stat = MMB_OK;

  if(loop->block->n_stmt == 0) {
    MMB_ERR("Cannot extract pet tree from empty loop\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  // Pull induction variable out of loop->init and build pet expr pe_iv
  pet_expr *pe_iv;
  stat = extract_pe_iv_from_loop_init(loop, &pe_iv);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to extract induction variable from loop init\n");
    goto BAILOUT;
  }
  // Check if iv is only local to the loop (i.e. declared in the loop init)
  bool pe_iv_local;
  stat = mmb_expr_is_decl(loop->init, &pe_iv_local);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to check decl status of induction variable in loop init\n");
    goto BAILOUT;
  }

  // Pull assignment expression out of loop-> init and build pet expr pe_init
  pet_expr *pe_init;
  stat = extract_pe_init_from_loop_init(loop, &pe_init);
  if(stat != MMB_OK) {
    MMB_ERR("Could not extract pet expr from loop init mmbExpr\n");
    goto BAILOUT;
  }

  // Pull condition expression out of loop->cond and build pet expr pet_cond
  pet_expr *pe_cond;
  stat = extract_pe_cond_from_loop_cond(loop, &pe_cond);
  if(stat != MMB_OK) {
    MMB_ERR("Could not extract pet expr from loop cond mmbExpr\n");
    goto BAILOUT;
  }

  // Pull increment expression out of loop-> inc and build pet expr pet_inc
  pet_expr *pe_inc;
  stat = extract_pe_inc_from_loop_inc(loop, &pe_inc);
  if(stat != MMB_OK) {
    MMB_ERR("Could not extract pet expr from loop inc mmbExpr\n");
    goto BAILOUT;
  }

  // Run extract tree on the body
  pet_tree *body = pet_tree_new_block(g_isl_ctx, 1, loop->block->n_stmt);
  for(unsigned i = 0; i < loop->block->n_stmt; i++) {
    pet_tree *tree_child;
    stat = extract_pet_tree_from_stmt(loop->block->stmts[i], &tree_child);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to extract pet tree from statement in loop\n");
      goto BAILOUT;
    }
    body = pet_tree_block_add_child(body, tree_child);
    if(!tree) {
      MMB_ERR("Failed to add child tree to loop tree\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }
  
  // Build the pet for loop
  pet_tree *t = pet_tree_new_for(0, pe_iv_local, pe_iv, pe_init, 
                                pe_cond,	pe_inc, body);

  if(!t) {
    MMB_ERR("Failed to create new pet tree for loop\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  *tree = t;
BAILOUT:
  return stat;
}

#endif