/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_EXPR_H
#define MMB_EXPR_H

#include <stdbool.h>

#include "isl/ctx.h"
#include "isl/id.h"

#include "../common/mmb_error.h"

typedef enum mmbExprType {
  MMB_EXPR_UNDEFINED = 0,
  MMB_EXPR_INT,
  MMB_EXPR_CONST_PAR,
  MMB_EXPR_ITERATOR,
  MMB_EXPR_UNARY_OP,
  MMB_EXPR_BINARY_OP,
  MMB_EXPR_ACCESS,
  MMB_EXPR_TYPE__MAX
}mmbExprType;

typedef enum mmbOpType {
  MMB_OP_UNDEFINED = 0,
  MMB_OP_ASSIGN,
  MMB_OP_ADD,
  MMB_OP_SUB,
  MMB_OP_LT,
  MMB_OP_GT,
  MMB_OP_PRE_INC,
  MMB_OP_PRE_DEC,
  MMB_OP_TYPE__MAX
} mmbOpType;

typedef enum mmbExprAccessType {
  MMB_EXPR_ACCESS_UNDEFINED = 0,
  MMB_EXPR_ACCESS_READ,
  MMB_EXPR_ACCESS_WRITE,
  MMB_EXPR_ACCESS_TYPE__MAX
} mmbExprAccessType;

typedef struct mmbItId {
  isl_id *id;
  int type_signed; 
  int type_size;
} mmbItId;

typedef struct mmbCpId {
  isl_id *id;
  int type_signed; 
  int type_size;
} mmbCpId;

typedef struct mmbOp {
  mmbOpType type;
  bool is_decl;
} mmbOp;

typedef struct mmbExpr {
  mmbExprType type;
  unsigned n_args;
  struct mmbExpr **args;

  union {
    mmbOp op;
    struct mmbItId *itid;
    struct mmbCpId *cpid;
    int i;
  }u;
} mmbExpr;


mmbError mmb_expr_create_int(int i, mmbExpr **out_expr);
mmbError mmb_expr_create_const_parameter(const char* id, 
                                          mmbExpr ** out_expr);
mmbError mmb_expr_create_iterator(const char* id, 
                                    mmbExpr ** out_expr);
mmbError mmb_expr_create_unary_op(mmbOpType type, mmbExpr *arg, 
                                  mmbExpr **out_expr);
mmbError mmb_expr_create_binary_op(mmbOpType type, mmbExpr *lhs,
                                    mmbExpr *rhs, mmbExpr ** out_expr);
mmbError mmb_expr_create_binary_decl_op(mmbOpType type, mmbExpr *lhs,
                                        mmbExpr *rhs, mmbExpr ** out_expr);
mmbError mmb_expr_create_access(mmbExpr *access, mmbExpr ** out_expr);
mmbError mmb_expr_create_copy(mmbExpr *in_expr, mmbExpr **out_expr);
mmbError mmb_expr_extend_access_dim(mmbExpr *access, mmbExpr *new_dim_expr);
mmbError mmb_expr_op_is_assignment(mmbExpr *expr, bool *assignment);
mmbError mmb_expr_op_is_compound_assignment(mmbExpr *expr, 
                                            bool *cassignment);
mmbError mmb_expr_print(mmbExpr *in_expr, int indent);
mmbError mmb_expr_destroy(mmbExpr *in_expr);

mmbError mmb_expr_is_decl(mmbExpr *in_expr, bool* out_result);

const char* mmb_expr_access_type_to_string(mmbExprAccessType type);
const char* mmb_expr_op_type_to_string(mmbOpType type);

#endif
