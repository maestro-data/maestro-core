#ifndef MMB_COST_MODEL_H
#define MMB_COST_MODEL_H

#include "mmb_error.h"

typedef struct s_mmbCostModelHandle mmbCostModelHandle;

mmbError mmb_cost_model_initialize(mmbCostModelHandle **cm_handle);
mmbError mmb_cost_model_write_hw_devices(mmbCostModelHandle *cm_handle);
mmbError mmb_cost_model_finalize(mmbCostModelHandle *cm_handle);

#endif
