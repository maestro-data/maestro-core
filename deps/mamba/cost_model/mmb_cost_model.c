#include "mmb_cost_model.h"
#define COSTMODEL_USE_MAMBA
#include "mamba_interface.h"
#include "hwloc.h"
#include "mmb_error.h"
#include "mmb_logging.h"

typedef struct s_mmbCostModelHandle {
C_MambaHardware * hw;
} mmbCostModelHandle;

mmbError mmb_cost_model_initialize(mmbCostModelHandle **cm_handle) {
  mmbError stat = MMB_OK;
  mmbCostModelHandle *cm = (mmbCostModelHandle*)malloc(sizeof(mmbCostModelHandle));
  if(!cm) {
    MMB_ERR("Failed to allocate cost model handle\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  hwloc_topology_t  *hwloc_topo;
  stat = mmb_get_hwloc_topology(&hwloc_topo);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get internalhwloc topology\n");
    goto BAILOUT;
  }
  cm->hw = create_cm_mamba_hardware(*hwloc_topo);
  /* Should check result of create_cm_mamba_hardware here */
  
  *cm_handle = cm;
BAILOUT:
  return stat;
}

mmbError mmb_cost_model_write_hw_devices(mmbCostModelHandle *cm_handle) {
  mmbError stat = MMB_OK;
  cm_mamba_hw_write_devices(cm_handle->hw);
  /* Should check result of cm_mamba_hw_write_devices here */
BAILOUT:
  return stat;
}

mmbError mmb_cost_model_finalize(mmbCostModelHandle *cm_handle) {
  mmbError stat = MMB_OK;
  free_cm_mamba_hardware(cm_handle->hw);
  /* Should check result of free_cm_mamba_hardware here */
BAILOUT:
  return stat;
}