/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_I_STRATEGY_STATISTICS_H
#define MMB_I_STRATEGY_STATISTICS_H

#include <stddef.h>
#include <stdarg.h>
#include <assert.h>
#include <pthread.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"

#include "../i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Statistics Strategy
 * @{ */

/**
 * @name Private API
 * @{ */

/**
 * @brief Data recorded for out of the allocations
 */
struct mmb_strat_stats_data {
  pthread_mutex_t stats_mtx;
  size_t current_allocations;
  size_t current_allocated;
  size_t total_allocations;
  size_t total_allocated;
};

static inline void mmb_strat_stats_lock_data(struct mmb_strat_stats_data *data)
{
  int stat = pthread_mutex_lock(&data->stats_mtx);
  assert(0 == stat);
  (void) stat;
}

static inline void mmb_strat_stats_unlock_data(struct mmb_strat_stats_data *data)
{
  int stat = pthread_mutex_unlock(&data->stats_mtx);
  assert(0 == stat);
  (void) stat;
}

/**
 * @brief Check the interface can be used for statistics recording.
 *
 * @param [in] interface A mmbMemInterface handle. Cannot be \c NULL.
 *
 * @return \c MMB_OK if the interface is valid, an error type otherwise.
 */
mmbError mmb_strat_stats_check_interface(mmbMemInterface *interface);

/**
 * @brief Size of the data structure example.
 */
#define mmb_strat_data_size__stats ((mmb_strat_data_size)sizeof(struct mmb_strat_stats_data))

/**
 * @brief Initialize the parameters used to gather statistics.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to initialize
 * @param [in] data Pointer to the strategy specific data (i.e.,
 *                  \p struct mmb_strat_stats_data)
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_init_interface_va__stats(mmbMemInterface *interface,
                                            void *data,
                                            va_list args);

/**
 * @brief Finalize the parameters used to gather statistics.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to finalize
 * @param [in] data Pointer to the strategy specific data (i.e.,
 *                  \p struct mmb_strat_stats_data)
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_finalize_interface_va__stats(mmbMemInterface *interface,
                                                void *data,
                                                va_list args);

/**
 * @brief Initialize the parameters used to gather statistics.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to initialize
 * @param [in] data Pointer to the strategy specific data (i.e.,
 *                  \p struct mmb_strat_stats_data)
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_init_interface_va__stats(mmbMemInterface *interface,
                                            void *data,
                                            va_list args);

/**
 * @brief Finalize the parameters used to gather statistics.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to finalize
 * @param [in] data Pointer to the strategy specific data (i.e.,
 *                  \p struct mmb_strat_stats_data)
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_finalize_interface_va__stats(mmbMemInterface *interface,
                                                void *data,
                                                va_list args);

/**
 * @brief Update statistics about the memory operations.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Handle to the mmbMemInterface structure used to
 *                       allocate.
 * @param [in] opts Pointer to the source's allocation options handle.  May be
 *                  \c NULL.
 * @param [in] data Pointer to the strategy specific data (i.e.,
 *                  \p struct mmb_strat_stats_data).
 * @param [out] allocation Handle to an data as provided by the memory
 *                         interface.
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_allocate_va__stats(const size_t n_bytes,
                                      mmbMemInterface *interface,
                                      const mmbAllocateOptions *opts,
                                      void *data, void **allocation, va_list args);

/**
 * @brief Update statistics about the memory operations.
 *
 * @param [in] allocation Allocation handle pointing to the data to be free'd
 *                        by the memory interface.
 * @param [in] data Pointer to the strategy specific data (i.e.,
 *                  \p struct mmb_strat_stats_data).
 * @param [in] interface Handle to the interface used for allocating
 *                       \p allocation.
 * @param [in] opts Pointer to the source's allocation options handle.  May be
 *                  \c NULL.
 * @param [in] n_bytes Amount of bytes to free from the memory.
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_free_va__stats(void *allocation, void *data,
                                  mmbMemInterface *interface,
                                  const mmbAllocateOptions *opts,
                                  const size_t n_bytes, va_list args);

/**
 * @brief Update statistics about the memory operations.
 *
 * @param [out] dst Handle to the written data.
 * @param [in] dst_data Pointer to the strategy specific data (i.e.,
 *                      \p struct mmb_strat_thread_safe_data).
 * @param [in] dst_inter Interface managing the given destination data.
 * @param [in] dst_opts Pointer to the source's allocation options handle.  May
 *                      be \c NULL.
 * @param [in] doffset Offset in each dimensions at the begining of the
 *                     destination allocation.
 * @param [in] dpitch Pitch for each dimension of destination memory
 * @param [in] src Handle to the read data.
 * @param [in] src_inter Interface managing the given source data.
 * @param [in] src_opts Pointer to the source's allocation options handle.  May
 *                      be \c NULL.
 * @param [in] soffset Offset in each dimension at the begining of the source
 *                     allocation.
 * @param [in] spitch Pitch for each dimension of source memory.
 * @param [in] ndims Diemnsionality of both allocations and of the copied seciton.
 * @param [in] dims Dimensions of the copied section.  The first dimension is
 *                  given in bytes, then following dimensions are given in
 *                  number of previous dimensions.
 * @param [in] args Pointer to the followings parameters as set in the strategy
 *                  builder.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_copy_nd_va__stats(void *dst, void *dst_data,
                                     mmbMemInterface *dst_inter,
                                     const mmbAllocateOptions *dst_opts,
                                     const size_t *doffset, const size_t *dpitch,
                                     const void *src,
                                     const mmbMemInterface *src_inter,
                                     const mmbAllocateOptions *src_opts,
                                     const size_t *soffset, const size_t *spitch,
                                     const size_t ndims, const size_t *dims,
                                     va_list args);

/**  @} */

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_I_STRATEGY_STATISTICS_H */
