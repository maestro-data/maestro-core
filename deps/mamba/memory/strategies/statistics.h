/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_STRATEGY_STATISTICS_H
#define MMB_STRATEGY_STATISTICS_H

#include <stddef.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Statistics Strategy
 * @{ */

/**
 * @name Public API
 * @{ */

/**
 * @brief Include the statistics recording initialisation to the execution path.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to initialize
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_init_interface__stats(mmbMemInterface *interface);

/**
 * @brief Include the statistics recording finalization to the execution path.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to finalize
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_finalize_interface__stats(mmbMemInterface *interface);

/**
 * @brief Include the statistics recording to the allocation execution path.
 *
 * @param [in] n_bytes Size of the requested allocation
 * @param [in] interface Handle to the mmbMemInterface structure used to
 *                       allocate
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [out] allocation Handle to a pre-allocated structure as provided by the
 *                         memory interface
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_allocate__stats(const size_t n_bytes,
                                   mmbMemInterface *interface,
                                   const mmbAllocateOptions *opts,
                                   mmbAllocation *allocation);

/**
 * @brief Include the statistics recording to the free execution path.
 *
 * @param [in] allocation Allocation handle pointing to the data to be free'd
 *                        by the memory interface
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_free__stats(mmbAllocation *allocation);

/**
 * @brief Include the statistics recording to the copy execution path.
 *
 * @param [out] dst Handle to the written data.
 * @param [in] dst_data Pointer to the strategy specific data (i.e.,
 *                      \p struct mmb_strat_thread_safe_data).
 * @param [in] dst_inter Interface managing the given destination data.
 * @param [in] dst_opts Pointer to the source's allocation options handle.  May
 *                      be \c NULL.
 * @param [in] doffset Offset in each dimensions at the begining of the
 *                     destination allocation.
 * @param [in] dpitch Pitch for each dimension of destination memory
 * @param [in] src Handle to the read data.
 * @param [in] src_inter Interface managing the given source data.
 * @param [in] src_opts Pointer to the source's allocation options handle.  May
 *                      be \c NULL.
 * @param [in] soffset Offset in each dimension at the begining of the source
 *                     allocation.
 * @param [in] spitch Pitch for each dimension of source memory.
 * @param [in] ndims Diemnsionality of both allocations and of the copied seciton.
 * @param [in] dims Dimensions of the copied section.  The first dimension is
 *                  given in bytes, then following dimensions are given in
 *                  number of previous dimensions.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_copy_nd__stats(mmbAllocation *dest,
                                  const size_t *doffset, const size_t *dpitch,
                                  const mmbAllocation *src,
                                  const size_t *soffset, const size_t *spitch,
                                  const size_t ndims, const size_t *dims);

/**  @} */

/**  @} */

/**
 * @name Pooled Strategy with Statistics
 * @{ */

/**
 * @name Public API
 * @{ */

/**  @} */

/**  @} */

/**
 * @brief Include the statistics recording initialisation to the execution path.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to initialize
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_init_interface__pooled_stats(mmbMemInterface *interface);

/**
 * @brief Include the statistics recording finalization to the execution path.
 *
 * @param [in] interface Handle to the mmbMemInterface structure to finalize
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_finalize_interface__pooled_stats(mmbMemInterface *interface);

/**
 * @brief Include the statistics recording to the allocation execution path.
 *
 * @param [in] n_bytes Size of the requested allocation
 * @param [in] interface Handle to the mmbMemInterface structure used to
 *                       allocate
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [out] allocation Handle to a pre-allocated structure as provided by the
 *                         memory interface
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_allocate__pooled_stats(const size_t n_bytes,
                                          mmbMemInterface *interface,
                                          const mmbAllocateOptions *opts,
                                          mmbAllocation *allocation);

/**
 * @brief Include the statistics recording to the free execution path.
 *
 * @param [in] allocation Allocation handle pointing to the data to be free'd
 *                        by the memory interface
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_free__pooled_stats(mmbAllocation *allocation);

/**
 * @brief Include the statistics recording to the copy execution path.
 *
 * @param [inout] dest Handle to the allocation structre of the written data
 * @param [in] doffset Offset in each dimensions at the begining of the
 *                     destination allocation.
 * @param [in] dpitch Pitch for each dimension of destination memory
 * @param [in] src Handle to the allocation structure of the read data
 * @param [in] soffset Offset in each dimension at the begining of the source
 *                     allocation.
 * @param [in] spitch Pitch for each dimension of source memory.
 * @param [in] ndims Diemnsionality of both allocations and of the copied seciton.
 * @param [in] dims Dimensions of the copied section.  The first dimension is
 *                  given in bytes, then following dimensions are given in
 *                  number of previous dimensions.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_copy_nd__pooled_stats(mmbAllocation *dest,
                                         const size_t *doffset,
                                         const size_t *dpitch,
                                         const mmbAllocation *src,
                                         const size_t *soffset,
                                         const size_t *spitch,
                                         const size_t ndims,
                                         const size_t *dims);

/**  @} */

/**  @} */

#endif /* MMB_STRATEGY_STATISTICS_H */
