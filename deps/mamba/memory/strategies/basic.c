/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "apply.h"
#include "i_strategy.h"
#include "i_basic.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name "None" Strategy
 * @{ */

/**
 * @name Public API
 * @{ */

mmbError mmb_strat_init_interface__basic(mmbMemInterface *interface)
{
  return mmb_strat_init_interface__apply(interface,
                        mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface,
                        NULL);
}

mmbError mmb_strat_finalize_interface__basic(mmbMemInterface *interface)
{
  return mmb_strat_finalize_interface__apply(interface,
                        mmb_strategies_va_table[MMB_STRATEGY_NONE].finalize_interface,
                        NULL);
}

mmbError mmb_strat_allocate__basic(const size_t n_bytes,
                                   mmbMemInterface *interface,
                                   const mmbAllocateOptions *opts,
                                   mmbAllocation *allocation)
{
  return mmb_strat_allocate__apply(n_bytes, interface, opts, allocation,
                                   mmb_strategies_va_table[MMB_STRATEGY_NONE].allocate,
                                   NULL);
}

mmbError mmb_strat_free__basic(mmbAllocation *allocation)
{
  return mmb_strat_free__apply(allocation,
                               mmb_strategies_va_table[MMB_STRATEGY_NONE].free, NULL);
}

mmbError mmb_strat_copy_nd__basic(mmbAllocation *dst,
                                  const size_t *doffset, const size_t *dpitch,
                                  const mmbAllocation *src,
                                  const size_t *soffset, const size_t *spitch,
                                  const size_t ndims, const size_t *dims)
{
  return mmb_strat_copy_nd__apply(
      dst, doffset, dpitch, src, soffset, spitch, ndims, dims,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].copy_nd, NULL);
}

/**  @} */

/**  @} */

/**  @} */

/**  @} */
