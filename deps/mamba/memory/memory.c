/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>

#include "mmb_logging.h"
#include "mmb_memory.h"

#include "i_memory.h"
#include "memory_config.h"
#include "mmb_memory_options.h"
#include "mmb_numa_sizes.h"
#include "statistics.h"
#include "mmb_provider_options.h"

#if ENABLE_DISCOVERY
#include "topology.h"

static const int MMB_DISCOVERY_ENABLED = 1;

#else

static const int MMB_DISCOVERY_ENABLED = 0;

#endif /* ENABLE_DISCOVERY */

#include "interface.h"
#include "allocation.h"

#include "strategy.h"
#include "strategies/basic.h"
#include "strategies/pooled.h"
#include "strategies/thread_safe.h"
#include "strategies/statistics.h"

static inline void mmb_manager_lock(mmbManager *mngr)
{
  int stat = pthread_mutex_lock(&mngr->spaces_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_manager_unlock(mmbManager *mngr)
{
  int stat = pthread_mutex_unlock(&mngr->spaces_mtx);
  assert(0 == stat);
  (void) stat;
}

static const struct mmb_dispatch_strategy_entry
mmb_strategies_table[MMB_STRATEGY__MAX] = {
  [MMB_STRATEGY_NONE]
    = { .name = "NONE",
        .init               = mmb_strat_init_interface__basic,
        .finalize           = mmb_strat_finalize_interface__basic,
        .allocate           = mmb_strat_allocate__basic,
        .free               = mmb_strat_free__basic,
        .copy_nd            = mmb_strat_copy_nd__basic,
    },
  [MMB_POOLED]
    = { .name = "POOLED",
        .init               = mmb_strat_init_interface__pooled,
        .finalize           = mmb_strat_finalize_interface__pooled,
        .allocate           = mmb_strat_allocate__pooled,
        .free               = mmb_strat_free__pooled,
        .copy_nd            = mmb_strat_copy_nd__pooled,
    },
  [MMB_THREAD_SAFE]
    = { .name = "THREAD SAFE",
        .init               = mmb_strat_init_interface__thread_safe,
        .finalize           = mmb_strat_finalize_interface__thread_safe,
        .allocate           = mmb_strat_allocate__thread_safe,
        .free               = mmb_strat_free__thread_safe,
        .copy_nd            = mmb_strat_copy_nd__thread_safe,
    },
  [MMB_STATISTICS]
    = { .name = "STATISTICS RECORDS",
        .init               = mmb_strat_init_interface__stats,
        .finalize           = mmb_strat_finalize_interface__stats,
        .allocate           = mmb_strat_allocate__stats,
        .free               = mmb_strat_free__stats,
        .copy_nd            = mmb_strat_copy_nd__stats,
    },
  [MMB_POOLED_STATISTICS]
    = { .name = "POOLED with STATISTICS RECORDS",
        .init               = mmb_strat_init_interface__pooled_stats,
        .finalize           = mmb_strat_finalize_interface__pooled_stats,
        .allocate           = mmb_strat_allocate__pooled_stats,
        .free               = mmb_strat_free__pooled_stats,
        .copy_nd            = mmb_strat_copy_nd__pooled_stats,
    },
};

/* Possibly modifiable at configure/compile time and at runtime with
 * environment variables. */
static const mmbDefaultContext GLOBAL_DEFAULT_CONTEXT = {
  .provider          = MMB_CONFIG_PROVIDER_DEFAULT,
  .strategy          = MMB_CONFIG_STRATEGY_DEFAULT,
  .name              = MMB_CONFIG_INTERFACE_NAME_DEFAULT,
  .execution_context = { MMB_CPU, MMB_CONFIG_EXECUTION_CONTEXT_GPU_DEFAULT },
};

/**
 * @brief Translate the \p in_strat to a value that can be used by the strategy
 *        building blocks.
 *
 * @param [in] in_strat Requested strategy (can be \c MMB_STRATEGY_DEFAULT).
 * @param [in] default_strategy The value to assign \p out_strat if \p in_strat
 *                              equals \c MMB_STRATEGY_DEFAULT).
 * @param [out] out_strat The proper strategy value (cannot be
 *                        \c MMB_STRATEGY_DEFAULT).
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError
check_and_set_mmbstrategy(const mmbStrategy in_strat,
                          const mmbStrategy default_strategy,
                          mmbStrategy *out_strat)
{
  mmbError status = MMB_OK;

  /* Check parameters */
  if (NULL == out_strat) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  switch (in_strat) {
    case MMB_STRATEGY_ANY:
    case MMB_STRATEGY_DEFAULT:
      *out_strat = default_strategy;
      break;
    case MMB_POOLED:
    case MMB_THREAD_SAFE:
    case MMB_STRATEGY_NONE:
    case MMB_STATISTICS:
    case MMB_POOLED_STATISTICS:
      *out_strat = in_strat;
      break;
    case MMB_LIMITED_GREEDY:
      MMB_WARN("This strategy (%s) is not yet available.\n",
           mmb_strategy_get_string(in_strat));
      status = MMB_UNIMPLEMENTED;
      break;
    default:
      MMB_ERR("Invalid strategy requested (%d).\n", in_strat);
      status = MMB_INVALID_STRATEGY;
  }
  return status;
}

/**
 * @brief Translate the \p in_provider to a value that can be used by the
 *        strategy building blocks.
 *
 * @param [in] in_provider Requested provider (can be \c MMB_PROVIDER_DEFAULT).
 * @param [in] default_provider The value to assign \p out_provider if
 *                              \p in_provider equals \c MMB_PROVIDER_DEFAULT).
 * @param [out] out_provider The proper provider value (cannot be
 *                           \c MMB_PROVIDER_DEFAULT).
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError
check_and_set_mmbprovider(const mmbProvider in_provider,
                          const mmbProvider default_provider,
                          mmbProvider *out_provider)
{
  mmbError status = MMB_OK;

  /* Check parameters */
  if (NULL == out_provider) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  switch (in_provider) {
    case MMB_PROVIDER_ANY:
    case MMB_PROVIDER_DEFAULT:
      *out_provider = default_provider;
      break;
    case MMB_SYSTEM:
    case MMB_SICM:
    case MMB_UMPIRE:
    case MMB_JEMALLOC:
      *out_provider = in_provider;
      break;
    default:
      MMB_ERR("Invalid memory provider requested (%d).\n", in_provider);
      status = MMB_INVALID_PROVIDER;
  }
  return status;
}

/**
 * @brief Translate the \p in_ex_con to a value that can be used by the space.
 *
 * @param [in] in_ex_con Requested execution context (can be
 *                       \c MMB_EXECUTION_CONTEXT_DEFAULT).
 * @param [in] default_ex_con The value to assign \p out_ex_con if \p in_ex_con
 *                            equals \c MMB_EXECUTION_CONTEXT_DEFAULT).
 * @param [out] out_ex_con The proper execution context value (cannot be
 *                         \c MMB_EXCUTION_CONTEXT_DEFAULT).
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError
check_and_set_mmbexecutioncontext(const mmbExecutionContext in_ex_con,
                                  const mmbExecutionContext default_ex_con,
                                  mmbExecutionContext *out_ex_con)
{
  mmbError status = MMB_OK;

  /* Check parameters */
  if (NULL == out_ex_con) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  switch (in_ex_con) {
    case MMB_EXECUTION_CONTEXT_DEFAULT:
      if (MMB_EXECUTION_CONTEXT_NONE == default_ex_con) {
        MMB_ERR("No default execution context available for the requested layer.\n");
        status = MMB_INVALID_EXECUTION_CONTEXT;
      } else {
        *out_ex_con = default_ex_con;
      }
      break;
    case MMB_CPU:
    case MMB_GPU_CUDA:
    case MMB_GPU_HIP:
    case MMB_OPENCL:
      *out_ex_con = in_ex_con;
      break;
    default:
      MMB_ERR("Invalid execution context requested (%d).\n", in_ex_con);
      status = MMB_INVALID_EXECUTION_CONTEXT;
  }
  return status;
}

/**
 * @brief Translate the \p in_name to a value that can be used by the space.
 *
 * If \p in_name is \c NULL or points to an empty string, \p default_in_name is
 * used (which can be \c NULL).
 *
 * @param [in] in_name      Requested name (can be
 *                          \c NULL or any string).
 * @param [in] default_name The value to assign \p out_name if \p in_name
 *                          equals \c NULL or an empty string.
 * @param [out] out_name    The proper name
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError
check_and_set_interface_name(const char *in_name, const char *default_name,
                             const char **out_name)
{
  if (NULL == in_name || *in_name == '\0')
    *out_name = default_name;
  else
    *out_name = in_name;
  return MMB_OK;
}

/**
 * @brief Check the validity of the value of \p layer.
 *
 * @param [in] layer The input layer
 * @return \c true is layer is a valid value.
 */
static bool check_mmbmemlayer(const mmbMemLayer layer)
{
  return MMB_DRAM <= layer && MMB_MEMLAYER__MAX > layer;
}

/**
 * @name mmbMemSpace
 * @{ */

/**
 * @name Internal API
 * @{ */

static inline void mmb_memspace_lock_interfaces(mmbMemSpace *space)
{
  int stat = pthread_mutex_lock(&space->interfaces_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_unlock_interfaces(mmbMemSpace *space)
{
  int stat = pthread_mutex_unlock(&space->interfaces_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_lock_defaults(mmbMemSpace *space)
{
  int stat = pthread_mutex_lock(&space->defaults_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_unlock_defaults(mmbMemSpace *space)
{
  int stat = pthread_mutex_unlock(&space->defaults_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_lock(mmbMemSpace *space)
{
  mmb_memspace_lock_interfaces(space);
  mmb_memspace_lock_defaults(space);
  mmb_memspace_lock_size(space);
  mmb_memspace_rdlock_max_bytes(space);
  mmb_memspace_lock_numa_sizes(space);
}
static inline void mmb_memspace_unlock(mmbMemSpace *space)
{
  mmb_memspace_unlock_size(space);
  mmb_memspace_unlock_defaults(space);
  mmb_memspace_unlock_interfaces(space);
  mmb_memspace_unlock_max_bytes(space);
  mmb_memspace_unlock_numa_sizes(space);
}

static inline void
mmb_memspace_insert_interface(mmbMemSpace *space, mmbMemInterface *interface)
{
  MMB_CDL_INSERT_INTERFACE(space->interfaces, interface);
  space->num_interfaces += 1;
}

static inline void
mmb_memspace_delete_interface(mmbMemSpace *space, mmbMemInterface *interface)
{
  MMB_CDL_DELETE_INTERFACE(space->interfaces, interface);
  space->num_interfaces -= 1;
}

static inline mmbError
mmb_memspace_add_named_interface(mmbMemSpace *space, mmbMemInterface *interface)
{
  int err;
  mmb_hash_iter_t key;
  if (NULL == space) {
    MMB_ERR("Invalid space handle, cannot be NULL.\n");
    return MMB_INVALID_SPACE;
  }
  if (NULL == interface || NULL == interface->name || '\0' == interface->name[0]) {
    MMB_ERR("Invalid interface handle. You cannot insert NULL into the hashmap.\n");
    return MMB_INVALID_INTERFACE;
  }
  key = mmb_hash_put_interface(space->named_interfaces, interface->name, &err);
  if (0 > err) {
    MMB_ERR("Error while adding key \"%s\" to the named interface. (%d)\n",
            interface->name, err);
    return MMB_ERROR;
  }
  mmb_hash_value_interface(space->named_interfaces, key) = interface;
  return MMB_OK;
}

static inline mmbError
mmb_memspace_interface_name_exists_ret_key(mmbMemSpace *space, const char *name, mmb_hash_iter_t *key)
{
  if (NULL == space) {
    MMB_ERR("Invalid space handle, cannot be NULL.\n");
    return MMB_INVALID_SPACE;
  }
  if (NULL == name || '\0' == *name) {
    MMB_ERR("Invalid name, cannot be NULL nor empty.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == key) {
    MMB_ERR("Invalid key pointer, cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *key = mmb_hash_get_interface(space->named_interfaces, name);
  return MMB_OK;
}

static inline mmbError
mmb_memspace_interface_name_exists(mmbMemSpace *space, const char *name)
{
  mmb_hash_iter_t key;
  mmbError stat = MMB_OK;
  stat = mmb_memspace_interface_name_exists_ret_key(space, name, &key);
  if (MMB_OK != stat) {
    MMB_ERR("Error when looking for the interface in the hashmap.\n");
    return stat;
  }
  if (mmb_hash_end_interface(space->named_interfaces) == key)
    return MMB_NOT_FOUND;
  else
    return MMB_OK;
}

static inline mmbError
mmb_memspace_get_named_interface(mmbMemSpace *space, const char *name,
                                 mmbMemInterface **interface)
{
  mmb_hash_iter_t key;
  mmbError stat = MMB_OK;
  stat = mmb_memspace_interface_name_exists_ret_key(space, name, &key);
  if (MMB_OK != stat) {
    MMB_ERR("Error when looking for the interface in the hashmap.\n");
    return stat;
  }
  if (mmb_hash_end_interface(space->named_interfaces) != key)
    *interface = mmb_hash_value_interface(space->named_interfaces, key);
  else
    stat = MMB_NOT_FOUND;
  return stat;
}

static inline mmbError
mmb_memspace_delete_named_interface(mmbMemSpace *space,
                                    const mmbMemInterface *interface)
{
  mmb_hash_iter_t key;
  mmbError stat = MMB_OK;
  if (NULL == space) {
    MMB_ERR("Invalid space handle, cannot be NULL.\n");
    return MMB_INVALID_SPACE;
  }
  if (NULL == interface || NULL == interface->name) {
    MMB_ERR("Invalid interface handle. You cannot remove NULL from the hashmap.\n");
    return MMB_INVALID_INTERFACE;
  }
  key = mmb_hash_get_interface(space->named_interfaces, interface->name);
  if (mmb_hash_end_interface(space->named_interfaces) != key) {
    mmb_hash_del_interface(space->named_interfaces, key);
  } else {
    stat = MMB_NOT_FOUND;
  }
  return stat;
}

static mmbError
mmb_memspace_destroy_interface_no_check(mmbMemSpace *space,
                                        mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  if (!(NULL == interface->name || '\0' == interface->name[0])) {
    stat = mmb_memspace_delete_named_interface(space, interface);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to remove interface from the space.\n");
      return MMB_OK;
    }
  }
  stat = mmb_meminterface_finalize(interface);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to finalize memory interface.\n");
    return MMB_OK;
  }
  mmb_memspace_delete_interface(space, interface);
  stat = mmb_meminterface_destroy(interface);
  if (stat != MMB_OK) {
    MMB_ERR("Failed to destroy memory interface.\n");
    return MMB_OK;
  }

  return stat;
}

static mmbError
mmb_memspace_create(const mmbMemLayer layer, const bool is_numa_aware,
                    const size_t in_max_bytes,
                    const mmbNumaSizes *in_numa_sizes,
                    const mmbExecutionContext ex_context,
                    const mmbProvider default_provider,
                    const mmbStrategy default_strat,
                    const char *default_name,
                    const mmbProviderOptions default_provider_opts,
                    const mmbDefaultContext *global_default_ctx,
                    mmbAllocStats *stats,
                    mmbMemSpace **out_space)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  if (NULL == out_space) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  if (!check_mmbmemlayer(layer)) {
    MMB_ERR("Invalid layer requested (%d).\n", layer);
    stat = MMB_INVALID_LAYER;
    goto BAILOUT;
  }
  mmbStrategy strategy = global_default_ctx->strategy;
  stat = check_and_set_mmbstrategy(default_strat, strategy, &strategy);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  mmbProvider provider = global_default_ctx->provider;
  stat = check_and_set_mmbprovider(default_provider, provider, &provider);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  const char *name = global_default_ctx->name;
  stat = check_and_set_interface_name(default_name, name, &name);
  if (MMB_OK != stat) {
    return stat;
  }
  mmbExecutionContext ex_con;
  ex_con = global_default_ctx->execution_context[MMB_GDRAM == layer];
  stat = check_and_set_mmbexecutioncontext(ex_context, ex_con, &ex_con);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }

  /* Check whether the requested provider can provide support
   * for the requested layer */
  bool check_provider;
  stat = mmb_provider_compatibility_check(provider, layer, ex_con, is_numa_aware,
                                          &check_provider);
  if (MMB_OK != stat) {
    goto BAILOUT;
  } else if (false == check_provider) {
    stat = MMB_INVALID_PROVIDER;
    MMB_WARN("The requested provider (%s) cannot provide support for the requested "
         "memory layer (%s) with the required execution context (%s).\n",
         mmb_provider_get_string(provider), mmb_layer_get_string(layer),
         mmb_execution_context_get_string(ex_con));
    goto BAILOUT;
  }

  /* For now, NUMA aware spaces cannot support booling strategies. */
  if (is_numa_aware) {
    switch (strategy) {
      case MMB_POOLED:
      case MMB_POOLED_STATISTICS:
        stat = MMB_UNSUPPORTED;
        MMB_ERR("You cannot use pooled strategies with a NUMA aware space.\n");
        goto BAILOUT;
      default:
        break;
    }
  }

  if (NULL == stats) {
    stat = MMB_INVALID_ARG;
    MMB_ERR("Invalid handle to statistics hashmap.\n");
    goto BAILOUT;
  }

  /* Check whether we are NUMA aware or not. If not, compute the max_bytes
   * value and the number of elements to be added to the VLA. */
  size_t max_bytes = 0;
  size_t n_numa_sizes = 0;
  if (!is_numa_aware) {
    max_bytes = in_max_bytes;
  } else {
    n_numa_sizes = in_numa_sizes->size;
  }

  /* This should be a space specific initialisation
   * e.g. to set default interface (at compile time) */
  mmbMemSpace *s = calloc(1, sizeof(mmbMemSpace) + sizeof(size_t[n_numa_sizes]));
  if (s == NULL) {
    MMB_ERR("Failed to allocate memory space.\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  s->layer = layer;
  s->default_strategy = strategy;
  s->default_provider = provider;
  s->default_name = name;
  s->is_numa_aware = is_numa_aware;
  s->max_bytes = max_bytes;
  s->ex_context = ex_con;
  s->stats = stats;

  /* Copy numa sizes and update max_bytes */
  for (size_t id = 0; n_numa_sizes > id; ++id) {
    const size_t size = (size_t) in_numa_sizes->d[id];
    s->numa_sizes[id] = size;
    s->max_bytes += size;
  }

  /* Initialise the hashmap */
  s->named_interfaces = mmb_hash_init_interface();

  /* Initialize the mutexes */
  {
    int status;
    status = pthread_mutex_init(&s->defaults_mtx, NULL);
    if (0 != status) {
      MMB_ERR("Unable to initialize defaults' mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      free(s);
      goto BAILOUT;
    }
    status = pthread_mutex_init(&s->interfaces_mtx, NULL);
    if (0 != status) {
      MMB_ERR("Unable to initialize space's mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      free(s);
      goto BAILOUT;
    }
    status = pthread_mutex_init(&s->alloc_size_mtx, NULL);
    if (0 != status) {
      MMB_ERR("Unable to initialize allocated size's mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      free(s);
      goto BAILOUT;
    }
    status = pthread_rwlock_init(&s->max_bytes_mtx, NULL);
    if (0 != status) {
      MMB_ERR("Unable to initialize read-write lock on space's maximum allocated "
          "size (err=%d).\n", status);
      stat = MMB_ERROR;
      free(s);
      goto BAILOUT;
    }
    status = pthread_mutex_init(&s->numa_sizes_mtx, NULL);
    if (0 != status) {
      MMB_ERR("Unable to initialize per node NUMA sizes set's mutex (err=%d).\n",
              status);
      stat = MMB_ERROR;
      free(s);
      goto BAILOUT;
    }
  }

  /* Create default interface */
  mmbMemInterface *interface;
  stat = mmb_meminterface_create(provider, default_provider_opts, strategy, name, &interface);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to allocate memory interface.\n");
    free(s);
    goto BAILOUT;
  }
  interface->space = s;
  stat = mmb_meminterface_init(interface);
  if (stat != MMB_OK) {
    MMB_ERR("Failed to initialise memory interface.\n");
    mmb_meminterface_destroy(interface);
    free(s);
    goto BAILOUT;
  }
  stat = mmb_memspace_add_named_interface(s, interface);
  if (MMB_OK != stat) {
    mmb_meminterface_finalize(interface);
    mmb_meminterface_destroy(interface);
    goto BAILOUT;
  }
  mmb_memspace_insert_interface(s, interface);

  *out_space = s;
BAILOUT:
  return stat;
}

static mmbError mmb_memspace_destroy(mmbMemSpace *space)
{
  mmbError stat = MMB_OK;
  /* Check parameter */
  if (NULL == space) {
    MMB_WARN("Invalid space (cannot be NULL).\n");
    return MMB_INVALID_SPACE;
  }

  /* Free all interfaces */
  mmbMemInterface *interface, *tmp1, *tmp2;
  MMB_CDL_FOREACH_INTERFACE_SAFE(space->interfaces, interface, tmp1, tmp2) {
    stat = mmb_memspace_destroy_interface_no_check(space, interface);
    if (MMB_OK != stat) {
      MMB_WARN("Failed to destroy memory interface.\n");
      goto BAILOUT;
    }
  }

  /* Destroy the hashmap */
  mmb_hash_destroy_interface(space->named_interfaces);

  /* Destroy space mutexes */
  {
    int status;
    status = pthread_mutex_destroy(&space->defaults_mtx);
    if (0 != status) {
      MMB_ERR("Unable to destroy defaults' mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
    status = pthread_mutex_destroy(&space->interfaces_mtx);
    if (0 != status) {
      MMB_ERR("Unable to destroy space's mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
    status = pthread_mutex_destroy(&space->alloc_size_mtx);
    if (0 != status) {
      MMB_ERR("Unable to destroy allocated size's mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
    status = pthread_rwlock_destroy(&space->max_bytes_mtx);
    if (0 != status) {
      MMB_ERR("Unable to destroy read-write lock on space's maximum allocated "
              "(err=%d).\n", status);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }

  free(space);
BAILOUT:
  return stat;
}

/**  @} */

mmbError mmb_memspace_configure_defaults(const struct mmbManager *mngr,
                                         const struct mmbMemSpaceConfig *config_opts,
                                         mmbMemSpace *space)
{
  mmbError stat = MMB_OK;
  if (NULL == space) {
    MMB_ERR("Invalid space (cannot be NULL).\n");
    return MMB_INVALID_SPACE;
  }
  if (NULL == config_opts) {
    MMB_WARN("NULL pointer given for configure option, no change to be done.\n");
    return MMB_OK;
  }

  mmbProvider provider = config_opts->interface_opts.provider;
  mmbStrategy strategy = config_opts->interface_opts.strategy;
  const char *name = config_opts->interface_opts.name;

  mmb_memspace_lock_defaults(space);
  if (MMB_PROVIDER_ANY != provider) {
    stat = check_and_set_mmbprovider(provider, mngr->default_ctx.provider,
                                     &provider);
    if (MMB_OK != stat) {
      goto UNLOCK_BAILOUT;
    }
  } else {
    provider = space->default_provider;
  }
  if (MMB_STRATEGY_ANY != strategy) {
    stat = check_and_set_mmbstrategy(strategy, mngr->default_ctx.strategy,
                                     &strategy);
    if (MMB_OK != stat) {
      goto UNLOCK_BAILOUT;
    }
  } else {
    strategy = space->default_strategy;
  }
  if (NULL != name && '\0' != *name) {
    stat = check_and_set_interface_name(name, mngr->default_ctx.name, &name);
    if (MMB_OK != stat) {
      goto UNLOCK_BAILOUT;
    }
  } else {
    name = space->default_name;
  }
  stat = mmb_memspace_resize(&config_opts->size_opts, space);
  if (MMB_OK != stat) {
    goto UNLOCK_BAILOUT;
  }

  /* Update only if no error occured */
  space->default_provider = provider;
  space->default_strategy = strategy;
  space->default_name = name;

UNLOCK_BAILOUT:
  mmb_memspace_unlock_defaults(space);

  return stat;
}

mmbError mmb_memspace_resize(const mmbSizeConfig *size_opts, mmbMemSpace *space)
{
  mmbError stat = MMB_OK;
  /* check parameters */
  if (NULL == size_opts) { // Should never happen
    stat = MMB_INVALID_ARG;
    MMB_ERR("Invalid size argument.\n");
    goto BAILOUT;
  }
  if (NULL == space) {
    stat = MMB_INVALID_SPACE;
    MMB_ERR("Invalid space (cannot be NULL).\n");
    goto BAILOUT;
  }
  if (space->is_numa_aware) {
    stat = MMB_INVALID_SPACE;
    MMB_ERR("NUMA aware spaces cannot be resized.\n");
    goto BAILOUT;
  }
  /* Set size if not given MMB_SET_ANY */
  size_t new_size = 0;
  switch (size_opts->action) {
    case MMB_SIZE_ANY:
      goto BAILOUT;
    case MMB_SIZE_SET:
      new_size = size_opts->mem_size;
      /* fallthrough */
    case MMB_SIZE_NONE:
      mmb_memspace_wrlock_max_bytes(space);
      mmb_memspace_lock_size(space);
      if (new_size < space->allocated_bytes) {
        MMB_ERR("The size requested (%zu) cannot be smaller than the already "
            "allocated size (%zu).\n", new_size, space->allocated_bytes);
        stat = MMB_INVALID_SIZE;
      } else {
        space->max_bytes = new_size;
      }
      mmb_memspace_unlock_size(space);
      mmb_memspace_unlock_max_bytes(space);
      break;
    default:
      MMB_ERR("Invalid size action parameter.\n");
      stat = MMB_INVALID_ARG;
  }
BAILOUT:
  return stat;
}

mmbError
mmb_memspace_create_interface(mmbMemSpace *space,
                              const struct mmbMemInterfaceConfig *config_opts,
                              mmbMemInterface **out_interface)
{
  mmbError stat = MMB_OK;
  mmbMemInterface *interface = NULL;

  /* check parameters */
  mmbProvider provider;
  mmbProviderOptions provider_opts;
  mmbStrategy strategy;
  const char *name;

  stat = mmb_meminterface_set_parameters(space, config_opts,
                                         &provider, &provider_opts, &strategy, &name);
  if (MMB_OK != stat) {
    MMB_ERR("Invalid set of parameters for interface request.\n");
    return stat;
  } 
  if (NULL == out_interface) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL != name && '\0' != *name) {
    stat = mmb_memspace_interface_name_exists(space, name);
    if (MMB_OK == stat) {
      MMB_ERR("Invalid name, already exists.\n");
      return MMB_INVALID_ARG;
    } else if (MMB_NOT_FOUND != stat) {
      MMB_ERR("Error occured while checking interface name validity.\n");
      return stat;
    }
  }
  /* For now, NUMA aware spaces cannot support booling strategies. */
  if (space->is_numa_aware) {
    switch (strategy) {
      case MMB_POOLED:
      case MMB_POOLED_STATISTICS:
        MMB_ERR("You cannot use pooled strategies with a NUMA aware space.\n");
        return MMB_UNSUPPORTED;
      default:
        break;
    }
  }

  mmb_memspace_lock_interfaces(space);

  stat = mmb_meminterface_create(provider, provider_opts, strategy, name, &interface);
  if (MMB_OK != stat) {
    goto unlock_end;
  }
  interface->space = space;
  stat = mmb_meminterface_init(interface);
  if (MMB_OK != stat) {
    mmb_meminterface_destroy(interface);
    goto unlock_end;
  }
  if (NULL != name && '\0' != *name) {
    stat = mmb_memspace_add_named_interface(space, interface);
    if (MMB_OK != stat) {
      mmb_meminterface_finalize(interface);
      mmb_meminterface_destroy(interface);
      goto unlock_end;
    }
  }
  mmb_memspace_insert_interface(space, interface);

  *out_interface = interface;

unlock_end:
  mmb_memspace_unlock_interfaces(space);

  return stat;
}

mmbError mmb_memspace_request_interface(mmbMemSpace *space,
                                        const struct mmbMemInterfaceConfig *config_opts,
                                        mmbMemInterface **out_interface)
{
  mmbError stat = MMB_OK;

  /* check parameters */
  mmbProvider provider;
  mmbProviderOptions provider_opts;
  mmbStrategy strategy;
  const char *name;
  stat = mmb_meminterface_set_parameters(space, config_opts,
                                         &provider, &provider_opts, &strategy, &name);
  if (MMB_OK != stat) {
    MMB_ERR("Invalid set of parameters for interface request.\n");
    return stat;
  }

  mmb_memspace_lock_interfaces(space);

  stat = mmb_memspace_get_named_interface(space, name, out_interface);
  if (MMB_NOT_FOUND == stat) {
    MMB_ERR("Interface \"%s\" does not exist in the given space.\n", name);
  }
#ifndef NDEBUG
  /* in debug mode, check the strategy and the provider against the requested
   * parameters. */
  if (MMB_OK == stat &&
       (   (MMB_STRATEGY_ANY != config_opts->strategy
            && strategy != (*out_interface)->strategy)
        && (MMB_PROVIDER_ANY != config_opts->provider
            && provider != (*out_interface)->provider)
       )
     )
  {
    MMB_ERR("The strategy and/or provider of the returned interface does "
            "not correspond to the one requested.\n");
  }
#endif

  mmb_memspace_unlock_interfaces(space);

  return stat;
}

mmbError mmb_memspace_destroy_interface(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;

  /* check parameters */
  stat = mmb_meminterface_check_params(interface);
  if (MMB_OK != stat) {
    return stat;
  }
  if (NULL != interface->name && 0 == strcmp("default", interface->name)) {
    MMB_ERR("Invalid interface: you cannot destroy the default interface.");
    return MMB_INVALID_INTERFACE;
  }

  mmb_memspace_lock_interfaces(interface->space);
  stat = mmb_memspace_destroy_interface_no_check(interface->space, interface);
  mmb_memspace_unlock_interfaces(interface->space);

  return stat;
}

mmbError mmb_memspace_get_layer(const mmbMemSpace *space, mmbMemLayer *layer)
{
  if (NULL == space) {
    MMB_ERR("Invalid space handle. Cannot be NULL.\n");
    return MMB_INVALID_SPACE;
  }
  if (NULL == layer) {
    MMB_ERR("Invalid pointer to layer. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *layer = space->layer;
  return MMB_OK;
}

mmbError mmb_memspace_get_execution_context(const mmbMemSpace *space,
                                            mmbExecutionContext *ex_con)
{
  if (NULL == space) {
    MMB_ERR("Invalid space handle. Cannot be NULL.\n");
    return MMB_INVALID_SPACE;
  }
  if (NULL == ex_con) {
    MMB_ERR("Invalid pointer to execution context. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *ex_con = space->ex_context;
  return MMB_OK;
}

mmbError mmb_memspace_check_params(const mmbMemSpace *space)
{
  mmbError stat = MMB_OK;
  if (NULL == space) {
    MMB_WARN("Invalid space (cannot be NULL).\n");
    return MMB_INVALID_SPACE;
  } else {
    if (!check_mmbmemlayer(space->layer)) {
      MMB_WARN("Invalid layer handle in given interface (cannot be NULL).\n");
      return MMB_INVALID_SPACE;
    }
    mmbExecutionContext ex_con;
    stat = check_and_set_mmbexecutioncontext(space->ex_context,
                                             MMB_EXECUTION_CONTEXT_NONE, &ex_con);
    if (MMB_OK != stat) {
      MMB_WARN("Invalid execution context in the memory space.\n");
      return MMB_INVALID_SPACE;
    }
    mmbProvider provider = MMB_PROVIDER__MAX;
    stat = check_and_set_mmbprovider(space->default_provider, provider, &provider);
    if (MMB_OK != stat || MMB_PROVIDER__MAX == provider) {
      MMB_WARN("Invalid provider in memory space.\n");
      return MMB_INVALID_SPACE;
    }
    mmbStrategy strategy = MMB_STRATEGY__MAX;
    stat = check_and_set_mmbstrategy(space->default_strategy, strategy, &strategy);
    if (MMB_OK != stat || MMB_STRATEGY__MAX == strategy) {
      MMB_WARN("Invalid strategy in memory space.\n");
      return MMB_INVALID_SPACE;
    }
  }
  return stat;
}

/**  @} */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Internal API
 * @{ */

mmbError
mmb_meminterface_set_parameters(const mmbMemSpace *space,
                                const struct mmbMemInterfaceConfig *config_opts,
                                mmbProvider *pprovider, mmbProviderOptions *pprovider_opts,
                                mmbStrategy *pstrategy, const char **pname)
{
  mmbError stat = MMB_OK;
  if (NULL == space) {
    MMB_ERR("Invalid space (cannot be NULL).\n");
    return MMB_INVALID_SPACE;
  }
  stat = mmb_memspace_check_params(space);
  if (MMB_OK != stat) {
    return stat;
  }
  const mmbMemInterfaceConfig default_config = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  if (NULL == config_opts) {
    config_opts = &default_config;
  }
  const char *name = NULL;
  stat = check_and_set_interface_name(config_opts->name, name, &name);
  if (MMB_OK != stat) {
    return stat;
  }
  mmbStrategy strategy = space->default_strategy;
  stat = check_and_set_mmbstrategy(config_opts->strategy, strategy, &strategy);
  if (MMB_OK != stat) {
    return stat;
  }
  mmbProvider provider = space->default_provider;
  stat = check_and_set_mmbprovider(config_opts->provider, provider, &provider);
  if (MMB_OK != stat) {
    return stat;
  }

  /* Check whether the requested provider can provide support
   * for the requested layer */
  bool check_provider;
  stat = mmb_provider_compatibility_check(provider, space->layer, space->ex_context,
                                          space->is_numa_aware, &check_provider);
  if (MMB_OK != stat) {
    return stat;
  } else if (false == check_provider) {
    MMB_WARN("The requested provider (%s) cannot provide support for the requested "
         "memory layer (%s) with the required execution context (%s).\n",
         mmb_provider_get_string(provider), mmb_layer_get_string(space->layer),
         mmb_execution_context_get_string(space->ex_context));
    stat = MMB_INVALID_PROVIDER;
    return stat;
  }

  /* We just pass the provider options through here, these are copied during 
   * interface construction and validated during interface initialisation */
  *pprovider_opts = config_opts->provider_opts;

  *pprovider = provider;
  *pstrategy = strategy;
  *pname = name;

  return stat;
}

mmbError mmb_meminterface_create(const mmbProvider provider,
                                 mmbProviderOptions provider_opts,
                                 const mmbStrategy strategy,
                                 const char *name,
                                 mmbMemInterface **out_interface)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  if (NULL == out_interface) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  size_t alloc_size_data;
  stat = mmb_strat_get_data_size(provider, strategy, &alloc_size_data);
  if (MMB_OK != stat) {
    return stat;
  }

  mmbMemInterface *interface;
  interface = calloc(1, alloc_size_data + sizeof *interface);
  if (interface == NULL) {
    MMB_ERR("Failed to allocate new memory space (out of memory).\n");
    return MMB_OUT_OF_MEMORY;
  }

  /* Take ownership of interface options */
  interface->provider = provider;
  interface->strategy = strategy;
  if (NULL == name || *name == '\0')
    interface->name = NULL;
  else if (NULL == (interface->name = strdup(name)))
    MMB_WARN("Unable to duplicate interface's name. "
             "Interface won't be hashed.\n");

  interface->provider_opts = provider_opts;


  *out_interface = interface;

  return stat;
}

mmbError mmb_meminterface_destroy(mmbMemInterface *interface)
{
  if (MMB_OK != mmb_meminterface_check_params(interface)) {
    MMB_WARN("Memory interface already freed or invalid interface handle.\n");
    return MMB_INVALID_ARG;
  }
  free(interface->name);
  free(interface);
  return MMB_OK;
}

mmbError mmb_meminterface_init(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_meminterface_check_params(interface);
  if (MMB_OK != stat) {
    return stat;
  }
  stat = mmb_strategies_table[interface->strategy]
          .init(interface);
  return stat;
}

mmbError mmb_meminterface_finalize(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_meminterface_check_params(interface);
  if (MMB_OK != stat) {
    return stat;
  }
  stat = mmb_strategies_table[interface->strategy]
          .finalize(interface);
  return stat;
}

mmbError mmb_meminterface_check_params(const mmbMemInterface *interface)
{
  if (NULL == interface) {
    MMB_WARN("Invalid interface (cannot be NULL).\n");
    return MMB_INVALID_INTERFACE;
  } else if (NULL == interface->space) {
    MMB_WARN("Invalid space handle in given interface (cannot be NULL).\n");
    return MMB_INVALID_INTERFACE;
  }
  return MMB_OK;
}

/**  @} */

mmbError mmb_meminterface_get_layer(const mmbMemInterface *interface,
                                    mmbMemLayer *layer)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  return mmb_memspace_get_layer(interface->space, layer);
}

mmbError mmb_meminterface_get_execution_context(const mmbMemInterface *interface,
                                                mmbExecutionContext *ex_con)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  return mmb_memspace_get_execution_context(interface->space, ex_con);
}

mmbError mmb_meminterface_get_space(mmbMemInterface *interface,
                                    mmbMemSpace **space)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  if (NULL == space) {
    MMB_ERR("Invalid pointer to space handle. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *space = interface->space;
  return MMB_OK;
}

mmbError mmb_meminterface_get_provider(const mmbMemInterface *interface,
                                       mmbProvider *provider)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  if (NULL == provider) {
    MMB_ERR("Invalid pointer to provider. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *provider = interface->provider;
  return MMB_OK;
}

mmbError mmb_meminterface_get_strategy(const mmbMemInterface *interface,
                                       mmbStrategy *strategy)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  if (NULL == strategy) {
    MMB_ERR("Invalid pointer to strategy. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *strategy = interface->strategy;
  return MMB_OK;
}

mmbError mmb_meminterface_get_name(const mmbMemInterface *interface,
                                   char **out_name)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  if (NULL == out_name) {
    MMB_ERR("Invalid pointer to string. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  char *name = NULL;
  if (NULL != interface->name) {
    name = strdup(interface->name);
    if (NULL == name) {
      MMB_ERR("Unable to duplicate interface's name.\n");
      return MMB_OUT_OF_MEMORY;
    }
  }
  *out_name = name;
  return MMB_OK;
}


/**  @} */

mmbError mmb_allocation_create_wrapped(void* in_ptr, const size_t n_bytes,
                                       mmbMemInterface *interface,
                                       mmbAllocation **out_allocation)
{
  return mmb_allocation_create_wrapped_opts(in_ptr, n_bytes, interface,
                                            NULL, out_allocation);
}

mmbError mmb_allocation_create_wrapped_opts(void* in_ptr,
                                            const size_t n_bytes,
                                            mmbMemInterface *interface,
                                            const mmbAllocateOptions *opts,
                                            mmbAllocation **out_allocation)
{
  mmbError stat = MMB_OK;

  /* Check_parameters */
  stat = mmb_meminterface_check_params(interface);
  if (MMB_OK != stat) {
    return stat;
  }
  if (NULL == in_ptr && 0 < n_bytes) {
    MMB_ERR("Providing a NULL pointer as buffer with a positive size "
            "is erroneous.\n");
    return MMB_INVALID_SIZE;
  }

  stat = mmb_alloc_allocate(in_ptr, n_bytes, interface, opts, false, out_allocation);
  if (MMB_OK != stat) {
    MMB_WARN("Failed to allocate structure for mmbAllocation object.\n");
  }
  return stat;
}

mmbError mmb_allocation_destroy(mmbAllocation *in_allocation)
{
  /* Check parameters */
  mmbError stat = mmb_alloc_check_params(in_allocation);
  if (MMB_OK != stat) {
    return stat;
  }
  if (in_allocation->owned) {
    return mmb_free(in_allocation);
  } else {
    return mmb_alloc_free(in_allocation);
  }
}

mmbError mmb_allocate(const size_t n_bytes, mmbMemInterface *interface,
                      mmbAllocation **out_allocation)
{
  return mmb_allocate_opts(n_bytes, interface, NULL, out_allocation);
}

mmbError mmb_allocate_opts(const size_t n_bytes,
                           mmbMemInterface *interface,
                           const mmbAllocateOptions *opts,
                           mmbAllocation **out_allocation)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  stat = mmb_meminterface_check_params(interface);
  if (MMB_OK != stat) {
    return stat;
  }
  if (NULL == out_allocation) {
    MMB_ERR("Out parameter \"out_alloc\" cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  mmbAllocation *a;
  stat = mmb_alloc_allocate(NULL, n_bytes, interface, opts, true, &a);
  if (stat != MMB_OK) {
    MMB_WARN("Could not allocate mmbAllocation object.\n");
    return stat;
  }
  
  if(n_bytes > 0) {
    stat = mmb_strategies_table[interface->strategy]
            .allocate(n_bytes, interface, opts, a);
    /* set structure if everything went smoothly. Otherwise, free allocation and
    * propagate error. */
    if (MMB_OK != stat) {
      mmb_alloc_free(a);
      return stat;
    }
  }

  *out_allocation = a;

  return stat;
}

mmbError mmb_free(mmbAllocation *allocation)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  stat = mmb_alloc_check_params(allocation);
  if (MMB_OK != stat) {
    MMB_ERR("Failed mmb_alloc_check_params.\n");
    return stat;
  }

  if (false == allocation->owned) {
    MMB_ERR("Wrong allocation handle. Allocation should be owned.\n");
    return MMB_INVALID_ARG;
  }

  if(allocation->n_bytes > 0) {
    stat = mmb_strategies_table[allocation->interface->strategy]
            .free(allocation);
    if (MMB_OK != stat) {
      MMB_ERR("Failed strategy free on allocation.\n");
      return stat;
    }
  }

  stat = mmb_alloc_free(allocation);
  if (MMB_OK != stat) {
    MMB_ERR("Failed mmb_alloc_free on allocation.\n");
    return stat;
  }
  return stat;
}

mmbError mmb_copy(mmbAllocation *dst, mmbAllocation *src)
{
  size_t n_bytes = dst->n_bytes > src->n_bytes ? src->n_bytes : dst->n_bytes;
  return mmb_copy_1d(dst, 0, src, 0, n_bytes);
}

mmbError
mmb_copy_1d(mmbAllocation *dst, const size_t doffset,
            mmbAllocation *src, const size_t soffset,
            const size_t width)
{
  const size_t one = 1;
  return mmb_copy_nd(dst, &doffset, &one, src, &soffset, &one, 1, &width);
}

mmbError
mmb_copy_2d(mmbAllocation *dst, const size_t dxoffset, const size_t dyoffset,
            const size_t dxpitch,
            mmbAllocation *src, const size_t sxoffset, const size_t syoffset,
            const size_t sxpitch,
            const size_t width, const size_t height)
{
  const size_t doffset[2] = { dyoffset, dxoffset };
  const size_t dpitch[2] = { dxpitch, 1 };
  const size_t soffset[2] = { syoffset, sxoffset };
  const size_t spitch[2] = { sxpitch, 1 };
  const size_t dims[2] = { height, width };
  return mmb_copy_nd(dst, doffset, dpitch, src, soffset, spitch, 2, dims);
}

mmbError
mmb_copy_3d(mmbAllocation *dst, const size_t dxoffset, const size_t dyoffset,
            const size_t dzoffset, const size_t dxpitch, const size_t dypitch,
            mmbAllocation *src, const size_t sxoffset, const size_t syoffset,
            const size_t szoffset, const size_t sxpitch, const size_t sypitch,
            const size_t width, const size_t height, const size_t depth)
{
  const size_t doffset[3] = { dzoffset, dyoffset, dxoffset };
  const size_t dpitch[3] = { dxpitch, dypitch, 1 };
  const size_t soffset[3] = { szoffset, syoffset, sxoffset };
  const size_t spitch[3] = { sxpitch, sypitch, 1 };
  const size_t dims[3] = { depth, height, width };
  return mmb_copy_nd(dst, doffset, dpitch, src, soffset, spitch, 3, dims);
}

mmbError
mmb_copy_nd(mmbAllocation *dst, const size_t *doffset, const size_t *dpitch,
            mmbAllocation *src, const size_t *soffset, const size_t *spitch,
            const size_t ndims, const size_t *dims)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  stat = mmb_alloc_check_params(dst);
  if (MMB_OK != stat) {
    return stat;
  }
  stat = mmb_alloc_check_params(src);
  if (MMB_OK != stat) {
    return stat;
  }

  /* If both are zero-sized, nothing to do */
  if(src->n_bytes == 0 && dst->n_bytes == 0) {
    return stat;
  }

  /* If only one is non-zero sized we fail */
  if(dst->n_bytes == 0){
    MMB_ERR("Cannot copy to zero byte allocation object\n");
    return MMB_ERROR;
  }
  if(src->n_bytes == 0){
    MMB_ERR("Cannot copy from zero byte allocation object\n");
    return MMB_ERROR;
  }

  /* Check if we can reach beyond the allocation bounds */
  size_t end_doffset = dpitch[ndims-1];
  for (size_t d = 0; ndims > d; ++d) {
    end_doffset += (dims[d]-1 + doffset[d]) * dpitch[d];
  }
  if (dst->n_bytes < end_doffset) {
    MMB_ERR("The requested copy cannot write ouf of the allocation bounds.\n");
    return MMB_OUT_OF_BOUNDS;
  }
  size_t end_soffset = spitch[ndims-1];
  for (size_t d = 0; ndims > d; ++d) {
    end_soffset += (dims[d]-1 + soffset[d]) * spitch[d];
  }
  if (src->n_bytes < end_soffset) {
    MMB_ERR("The requested copy cannot read ouf of the allocation bounds.\n");
    return MMB_OUT_OF_BOUNDS;
  }

  /* Do copy */
  return mmb_strategies_table[dst->interface->strategy]
          .copy_nd(dst, doffset, dpitch, src, soffset, spitch, ndims, dims);
}

mmbError mmb_allocation_get_slice(mmbAllocation *in_alloc,
                                  const ptrdiff_t offset, const size_t n_bytes,
                                  mmbAllocation **out_allocation)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  stat = mmb_alloc_check_params(in_alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  /* Check slice validity */
  if(in_alloc->n_bytes == 0 && offset > 0 && n_bytes > 0) {
    MMB_ERR("Cannot get non-zero size slice from non-zero offset of 0 size allocation\n");
    return MMB_ERROR;    
  }
  if(offset < 0) {
    MMB_ERR("Cannot get negative slice of allocation\n");
    return MMB_ERROR;
  }
  if(((char*)in_alloc->ptr + offset + (ptrdiff_t)n_bytes) > 
        (char*)in_alloc->ptr+(ptrdiff_t)in_alloc->n_bytes) {
    MMB_ERR("Slice overruns original allocation boundaries\n");
    return MMB_ERROR;
  }
  /* Get slice */
  void *in_ptr = (char *)in_alloc->ptr + offset;
  mmbMemInterface *interface = in_alloc->interface;
  stat = mmb_allocation_create_wrapped(in_ptr, n_bytes, interface, out_allocation);
  if (MMB_OK != stat) {
    MMB_WARN("Unable to wrap the sub slice of the given allocation.\n");
  }
  return stat;
}

static mmbError mmb_manager_initialize(mmbManager *mngr)
{
  mmbError stat = MMB_OK;

  /* Initialize mutex */
  {
    int status = pthread_mutex_init(&mngr->spaces_mtx, NULL);
    if (0 != status) {
      MMB_ERR("Unable to initialize the manager's mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }

  /* Check the validity of compile defined GLOBAL_DEFAULT_CONTEXT variable */
  mmbDefaultContext default_ctx = GLOBAL_DEFAULT_CONTEXT;
  const char *global_default_provider_env = MMB_CONFIG_PROVIDER_DEFAULT_ENV_NAME;
  const char *global_default_provider_str = getenv(global_default_provider_env);
  if (NULL != global_default_provider_str) {
    /* provider overwritten at run-time in environment */
    default_ctx.provider = mmb_provider_get_symbol(global_default_provider_str);
  } else {
    global_default_provider_str = mmb_provider_get_string(default_ctx.provider);
  }
  stat = check_and_set_mmbprovider(default_ctx.provider, MMB_PROVIDER__MAX,
                                   &default_ctx.provider);
  if (MMB_OK != stat || MMB_PROVIDER__MAX == default_ctx.provider) {
      MMB_ERR("Default memory provider misconfigured (%s). Please rebuild the "
          "library with another value or set environment variable %s.\n",
          global_default_provider_str, global_default_provider_env);
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  const char *global_default_strategy_env = MMB_CONFIG_STRATEGY_DEFAULT_ENV_NAME;
  const char *global_default_strategy_str = getenv(global_default_strategy_env);
  if (NULL != global_default_strategy_str) {
    /* strategy overwritten at run-time in environment */
    default_ctx.strategy = mmb_strategy_get_symbol(global_default_strategy_str);
  } else {
    global_default_strategy_str = mmb_strategy_get_string(default_ctx.strategy);
  }
  stat = check_and_set_mmbstrategy(default_ctx.strategy, MMB_STRATEGY__MAX,
                                   &default_ctx.strategy);
  if (MMB_OK != stat || MMB_STRATEGY__MAX == default_ctx.strategy) {
      MMB_ERR("Default strategy misconfigured (%s). Please rebuild the "
          "library with another value or set environment variable %s.\n",
          global_default_strategy_str, global_default_strategy_env);
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  const char *global_default_name_env = MMB_CONFIG_INTERFACE_NAME_DEFAULT_ENV_NAME;
  const char *global_default_name_str = getenv(global_default_name_env);
  if (NULL != global_default_name_str) {
    /* default name overwritten at run-time in environment */
    default_ctx.name = global_default_name_str;
  } else {
    global_default_name_str = default_ctx.name;
  }
  const char *global_default_ex_con_env = MMB_CONFIG_EXECUTION_CONTEXT_GPU_DEFAULT_ENV_NAME;
  const char *global_default_ex_con_str = getenv(global_default_ex_con_env);
  if (NULL != global_default_ex_con_str) {
    /* execution context overwritten at run-time in environment */
    default_ctx.execution_context[1] =
      mmb_execution_context_get_symbol(global_default_ex_con_str);
  } else {
    global_default_ex_con_str =
      mmb_execution_context_get_string(default_ctx.execution_context[1]);
  }
  if (MMB_EXECUTION_CONTEXT_NONE != default_ctx.execution_context[1])
    stat = check_and_set_mmbexecutioncontext(default_ctx.execution_context[1],
                                             MMB_EXECUTION_CONTEXT__MAX,
                                             &default_ctx.execution_context[1]);
  if (MMB_OK != stat || MMB_EXECUTION_CONTEXT__MAX == default_ctx.execution_context[1]) {
      MMB_ERR("Invalid default execution context requested for GPUs (%s). Please "
          "rebuild the library with another value or set environment variable "
          "%s.\n", global_default_ex_con_str, global_default_ex_con_env);
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  mngr->default_ctx = default_ctx;

  /* Initialise the statistics structure */
  stat = mmb_stats_init(&mngr->stats);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }

#if ENABLE_DISCOVERY
  uint64_t size;
  uint64_t layers[MMB_MEMLAYER__MAX] = { 0 };
  uint64_t ex_cons[MMB_EXECUTION_CONTEXT__MAX] = { 0 };
  uint64_t *numa_sizes[MMB_MEMLAYER__MAX] = { NULL };

  stat = mmb_topology_init(&mngr->topology, layers, ex_cons, numa_sizes);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }

  mmbMemSpaceConfig space_conf = {
    .interface_opts = {
      .provider = default_ctx.provider,
      .strategy = default_ctx.strategy,
      .name = default_ctx.name,
    },
  };

  for (int layer = MMB_DRAM; MMB_MEMLAYER__MAX > layer; ++layer) {
    size = layers[layer];
    if (MMB_GDRAM != layer && 0 < size) {
      /* register memory */
      space_conf.size_opts.is_numa_aware = false;
      space_conf.size_opts.mem_size = size;
      stat = mmb_manager_register_memory(mngr, layer, MMB_CPU, &space_conf, NULL);
      if (MMB_OK == stat) {
        MMB_DEBUG("layer: %s, context: MMB_CPU, size: %" PRIu64 "\n",
              mmb_layer_get_string(layer), size);
      } else {
        MMB_WARN("Unable to register MMB_CPU for layer #%d.\n", layer);
        stat = MMB_OK;
      }
    }
  }
  for (int ec = MMB_CPU; MMB_EXECUTION_CONTEXT__MAX > ec; ++ec) {
    /* Check which contexts are valid */
    size = ex_cons[ec];
    if (0 < size) {
      /* register memory */
      space_conf.size_opts.is_numa_aware = false;
      space_conf.size_opts.mem_size = size;
      stat = mmb_manager_register_memory(mngr, MMB_GDRAM, ec, &space_conf, NULL);
      if (MMB_OK == stat) {
        MMB_DEBUG("layer: MMB_GDRAM, context: %s, size: %" PRIu64 "\n",
              mmb_execution_context_get_string(ec), size);
      } else {
        MMB_WARN("Unable to register execution context %s for GDRAM.\n",
             mmb_execution_context_get_string(ec));
        stat = MMB_OK;
      }
    }
  }
  /* Free memory allocated in mmb_topology_init() */
  free(numa_sizes[0]);
#endif /* ENABLE_DISCOVERY */

BAILOUT:
  return stat;
}

mmbError mmb_manager_create(mmbManager **out_mngr)
{
  /* Creates a new manager instance */
  mmbError stat = MMB_OK;

  /* Check parameters */
  if (NULL == out_mngr) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  mmbManager *mngr = calloc(1, sizeof(mmbManager));
  if (NULL == mngr) {
    MMB_ERR("Error, malloc of manager failed.\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  stat = mmb_manager_initialize(mngr);
  if (MMB_OK != stat) {
    MMB_WARN("Failed to initializing manager (%s).\n",
         mmb_error_description(stat));
    goto BAILOUT;
  }

  *out_mngr = mngr;
BAILOUT:
  return stat;
}

mmbError mmb_manager_destroy(mmbManager *mngr)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  if (NULL == mngr) {
    MMB_WARN("Invalid manager handle (cannot be NULL).\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmb_manager_lock(mngr);

  /* free all spaces manager handles */
  if (mngr->num_spaces != 0) {
    for (size_t i = 0; i < mngr->num_spaces ; i++) {
      stat =  mmb_memspace_destroy(mngr->spaces[i]);
      if (stat != MMB_OK) {
        MMB_WARN("Unable to free memory space.\n");
        goto BAILOUT;
      }
    }
  }
  free(mngr->spaces);

  mmb_manager_unlock(mngr);

  /* Finalize mutex */
  {
    int status = pthread_mutex_destroy(&mngr->spaces_mtx);
    if (0 != status) {
      MMB_ERR("Unable to destroy the manager's mutex (err=%d).\n", status);
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }

  /* Finalise statistics */
  stat = mmb_stats_finalize(&mngr->stats);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }

#if ENABLE_DISCOVERY
  mmb_topology_finalize(mngr->topology);
#endif /* ENABLE_DISCOVERY */

  /* free the manager */
  free(mngr);

BAILOUT:
  return stat;
}

mmbError mmb_manager_register_memory(mmbManager *mngr, const mmbMemLayer layer,
                                     const mmbExecutionContext ex_context,
                                     const mmbMemSpaceConfig *config_opts,
                                     mmbMemSpace **new_space)
{
  mmbError stat = MMB_OK;
  mmbMemSpace *space = NULL;

  /* Check parameters */
  if (NULL == mngr) {
    MMB_ERR("Invalid manager provided (cannot be NULL).\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == config_opts) {
    MMB_ERR("Invalid configuration handle (cannot be NULL).\n");
    return MMB_INVALID_ARG;
  } else if (config_opts->size_opts.is_numa_aware) {
    const size_t n_numa_sizes = config_opts->size_opts.numa_sizes.size;
    const size_t *numa_sizes_ptr = config_opts->size_opts.numa_sizes.d;
    if (0 != n_numa_sizes && NULL == numa_sizes_ptr) {
      MMB_ERR("Invalid array of numa node sizes. "
              "\"numa_max\" is %zu, but \"numa_sizes\" is NULL.\n",
              n_numa_sizes);
        return MMB_INVALID_ARG;
    }
  }
  if (!check_mmbmemlayer(layer)) {
    MMB_ERR("Invalid memory layer provided.\n");
    return MMB_INVALID_LAYER;
  }
  mmbExecutionContext ex_con;
  ex_con = mngr->default_ctx.execution_context[MMB_GDRAM == layer];
  stat = check_and_set_mmbexecutioncontext(ex_context, ex_con, &ex_con);
  if (MMB_OK != stat) {
    MMB_ERR("Invalid execution context provided for layer %s.\n",
            mmb_layer_get_string(layer));
    return stat;
  }

  /* creates a new manager instance and keeps track of spaces */

  switch (layer) {
    case MMB_DRAM:
    case MMB_GDRAM:
    case MMB_HBM:
    case MMB_NVDIMM:
      break;
    default:
      MMB_ERR("Only currently supported layers are %s, %s, %s and %s.\n",
           mmb_layer_get_string(MMB_DRAM),
           mmb_layer_get_string(MMB_GDRAM),
           mmb_layer_get_string(MMB_HBM),
           mmb_layer_get_string(MMB_NVDIMM));
      stat = MMB_UNIMPLEMENTED;
      goto BAILOUT;
  }

  mmb_manager_lock(mngr);

  /* Extend the number of slots for the spaces if required */
  if (mngr->num_spaces == mngr->max_spaces) {
    size_t max_spaces = mngr->max_spaces + MMB_MAX_SPACES;
    size_t realloc_size = sizeof(mmbMemSpace[max_spaces]);
    mmbMemSpace **spaces = realloc(mngr->spaces, realloc_size);
    if (NULL == spaces) {
      /* Same as missing space for adding a new interface to a space. See
       * function mmb_memspace_request_interface. */
      MMB_ERR("Error, MAX_SPACES exceeded (%d) and unable to reallocate!\n",
          mngr->max_spaces);
      stat = MMB_OUT_OF_MEMORY;
      goto UNLOCK_BAILOUT;
    }
    mngr->spaces = spaces;
    mngr->max_spaces = max_spaces;
  }

  stat = mmb_memspace_create(layer, config_opts->size_opts.is_numa_aware,
                             config_opts->size_opts.mem_size,
                             &config_opts->size_opts.numa_sizes,
                             ex_con,
                             config_opts->interface_opts.provider,
                             config_opts->interface_opts.strategy,
                             config_opts->interface_opts.name,
                             config_opts->interface_opts.provider_opts,
                             &mngr->default_ctx,
                             &mngr->stats,
                             &space);
  if (stat != MMB_OK) {
    MMB_WARN("Unable to create the requested new memspace (%s)\n",
         mmb_error_description(stat));
    goto UNLOCK_BAILOUT;
  }
  mngr->spaces[mngr->num_spaces++] = space;

UNLOCK_BAILOUT:
  mmb_manager_unlock(mngr);

BAILOUT:
  /* Returns newly created handle (if any space was created) */
  if (NULL != space && NULL != new_space) {
    *new_space = space;
  }

  return stat;
}

mmbError mmb_manager_request_space(mmbManager *mngr, const mmbMemLayer layer,
                                   const mmbExecutionContext ex_context,
                                   const mmbMemSpaceConfig *config_opts,
                                   mmbMemSpace **out_space)
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  if (NULL == mngr) {
    MMB_ERR("Invalid manager handle (cannot be NULL).\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  if (NULL == out_space) {
    MMB_ERR("Out parameter cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == config_opts) {
    MMB_ERR("Invalid configuration handle (cannot be NULL).\n");
    return MMB_INVALID_ARG;
  }
  if (!check_mmbmemlayer(layer)) {
    MMB_ERR("Invalid layer requested (%d).\n", layer);
    stat = MMB_INVALID_LAYER;
    goto BAILOUT;
  }
  mmbExecutionContext ex_con;
  ex_con = mngr->default_ctx.execution_context[MMB_GDRAM == layer];
  stat = check_and_set_mmbexecutioncontext(ex_context, ex_con, &ex_con);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  size_t request_size;
  switch (config_opts->size_opts.action) {
    case MMB_SIZE_ANY:
      request_size = 0;
      break;
    case MMB_SIZE_SET:
      request_size = config_opts->size_opts.mem_size;
      break;
    default:
      MMB_ERR("Invalid size option action value. Valid values are %s or %s.\n",
          mmb_size_config_action_get_string(MMB_SIZE_ANY),
          mmb_size_config_action_get_string(MMB_SIZE_SET));
      goto BAILOUT;
  }

  /* TODO: Replace with hash lookup */
  for (size_t i = 0; i < mngr->num_spaces; i++) {
    mmbMemSpace *sp = mngr->spaces[i];
    /* Protected reading of values that may be changed while we read */
    mmb_memspace_lock_defaults(sp);
    mmb_memspace_lock_size(sp);
    mmb_memspace_rdlock_max_bytes(sp);
    const mmbProvider space_default_provider = sp->default_provider;
    const mmbStrategy space_default_strategy = sp->default_strategy;
    const size_t available_size = sp->max_bytes - sp->allocated_bytes;
    mmb_memspace_unlock_defaults(sp);
    mmb_memspace_unlock_size(sp);
    mmb_memspace_unlock_max_bytes(sp);
    /* Test value */
    if (layer == sp->layer && ex_con == sp->ex_context
        && (MMB_PROVIDER_ANY == config_opts->interface_opts.provider
            || config_opts->interface_opts.provider == space_default_provider)
        && (MMB_STRATEGY_ANY == config_opts->interface_opts.strategy
            || config_opts->interface_opts.strategy == space_default_strategy)
        && request_size <= available_size
       ) {
      *out_space = mngr->spaces[i];
      goto BAILOUT;
    }
  }

  MMB_WARN("Space not found for layer \"%s\" with execution context \"%s\"\n",
       mmb_layer_get_string(layer), mmb_execution_context_get_string(ex_con));
  stat = MMB_NOT_FOUND;

BAILOUT:
  return stat;
}

mmbError mmb_manager_report_state(mmbManager *mngr, FILE *out)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == mngr) {
    MMB_ERR("Invalid manager handle parameter (cannot be NULL).\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == out) {
    MMB_ERR("Invalid output stream (cannot be NULL).\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  const unsigned LABEL_LEN = 15;
  const unsigned TAB_INDENT = 6;
  mmb_manager_lock(mngr);
  fprintf(out, "[");
  for (size_t i = 0; mngr->num_spaces > i; ++i) {
    fprintf(out, "\n%*s%2zu: {\n", 2, "", i);
    const unsigned INDENT = TAB_INDENT + 2;
    mmbMemSpace *sp = mngr->spaces[i];
    /* Locking info */
    mmb_memspace_lock(sp);
    /* Print info */
    fprintf(out, "%*s%*s: %s,\n", INDENT, "", LABEL_LEN,
        "layer", mmb_layer_get_string(sp->layer));
    fprintf(out, "%*s%*s: %s,\n", INDENT, "", LABEL_LEN,
        "exe context", mmb_execution_context_get_string(sp->ex_context));
    fprintf(out, "%*s%*s: %s,\n", INDENT, "", LABEL_LEN,
        "def. provider", mmb_provider_get_string(sp->default_provider));
    fprintf(out, "%*s%*s: %s,\n", INDENT, "", LABEL_LEN,
        "def. strategy", mmb_strategy_get_string(sp->default_strategy));
    fprintf(out, "%*s%*s: %s,\n", INDENT, "", LABEL_LEN,
        "def. name", sp->default_name);
    fprintf(out, "%*s%*s: %s,\n", INDENT, "", LABEL_LEN,
        "is_numa_aware", sp->is_numa_aware ? "yes" : "no");
    fprintf(out, "%*s%*s: %zu,\n", INDENT, "", LABEL_LEN,
        "max_bytes", sp->max_bytes);
    fprintf(out, "%*s%*s: %zu,\n", INDENT, "", LABEL_LEN,
        "allocated bytes", sp->allocated_bytes);
    fprintf(out, "%*s%*s: %zu,\n", INDENT, "", LABEL_LEN,
        "available bytes", sp->max_bytes - sp->allocated_bytes);
    fprintf(out, "%*s%*s: %zu\n", INDENT, "", LABEL_LEN,
        "#interfaces", sp->num_interfaces);
    fprintf(out, "%*s    }%c", 2, "", mngr->num_spaces - 1 > i ? ',' : '\n');
    /* Unlocking info */
    mmb_memspace_unlock(sp);
  }
  fprintf(out, "]\n");
  fflush(stdout);
  mmb_manager_unlock(mngr);
BAILOUT:
  return stat;
}

/* Getter for hwloc topology */
mmbError mmb_manager_get_topology(mmbManager *mngr, hwloc_topology_t **topo)
{
  mmbError stat = MMB_OK;
  int discovery_enabled = 0;
  stat = mmb_manager_discovery_is_enabled(&discovery_enabled);
  if (MMB_OK != stat)
    goto BAILOUT;
  else if (!discovery_enabled) {
    MMB_ERR("Discovery is not available.  "
            "The library has to be reconfigured with the option "
            "`--enable_discovery=yes`.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  if(!topo) {
    MMB_ERR("Topology arg may not be null\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if(!mngr) {
    MMB_ERR("Manager arg may not be null\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  
  *topo = &mngr->topology;

BAILOUT:
  return stat;
}

mmbError mmb_manager_discovery_is_enabled(int *is_enabled)
{
  mmbError stat = MMB_OK;
  if (NULL == is_enabled) {
    MMB_ERR("is_enabled arg may not be NULL.\n");
    stat = MMB_INVALID_ARG;
  } else
    *is_enabled = MMB_DISCOVERY_ENABLED;
  return stat;
}

mmbError mmb_manager_get_interface_current_statistics(mmbManager *mngr,
                                                      mmbMemInterface *interface,
                                                      size_t *n_allocations,
                                                      size_t *bytes_allocated)
{
  if (NULL == mngr) {
    MMB_ERR("Invalid memory manager. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  return mmb_stats_get_current(&mngr->stats, interface, n_allocations, bytes_allocated);
}

mmbError mmb_manager_get_interface_total_statistics(mmbManager *mngr,
                                                    mmbMemInterface *interface,
                                                    size_t *n_allocations,
                                                    size_t *bytes_allocated)
{
  if (NULL == mngr) {
    MMB_ERR("Invalid memory manager. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle. Cannot be NULL.\n");
    return MMB_INVALID_INTERFACE;
  }
  return mmb_stats_get_total(&mngr->stats, interface, n_allocations, bytes_allocated);
}

mmbError mmb_manager_get_stats_record_count(mmbManager *mngr, size_t *count)
{
  if (NULL == mngr) {
    MMB_ERR("Invalid manager handle parameter (cannot be NULL).\n");
    return MMB_INVALID_ARG;
  }
  return mmb_stats_get_record_count(&mngr->stats, count);
}

