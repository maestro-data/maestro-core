/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <hwloc.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "i_memory.h"
#include "topology.h"

static mmbError
mmb_topology_check_numanodes(const hwloc_topology_t topology,
                             uint64_t *layers, uint64_t * restrict numa_sizes[])
{
  mmbError stat = MMB_OK;
  hwloc_obj_t obj = NULL;
  while (NULL != (obj = hwloc_get_next_obj_by_type(topology,
                                                   HWLOC_OBJ_NUMANODE, obj))) {
    /* Detect the memory layer */
    if (HWLOC_OBJ_NUMANODE == obj->type) {
      MMB_NOISE("    numa node\n");
      mmbMemLayer layer;
      const hwloc_obj_t numanode = obj;
      if (NULL == numanode->subtype) {
        if (NULL != hwloc_obj_get_info_by_name(numanode, "DaxDevice")) {
          MMB_NOISE("      nvdimm, DAXDevice, kmem management\n");
          layer = MMB_NVDIMM;
        } else {
          MMB_NOISE("      dram (no subtype)\n");
          layer = MMB_DRAM;
        }
      } else if (0 == strncmp("MCDRAM", numanode->subtype, 6)) {
        MMB_NOISE("      mcdram\n");
        layer = MMB_HBM;
      } else if (0 == strncmp("GPUMemory", numanode->subtype, 9)) {
        MMB_NOISE("      gdram\n");
        /* hwloc hides these nodes by default to avoid the system to allocate
         * memory there with interleaving policies. See
         * https://github.com/open-mpi/hwloc/wiki/Heterogeneous-Memory for
         * details. */
        layer = MMB_GDRAM;
      } else {
        MMB_NOISE("      dram (%s)\n", numanode->subtype);
        layer = MMB_DRAM;
      }
      /* Add detected memory */
      const uint64_t local_memory = numanode->attr->numanode.local_memory;
      layers[layer] += local_memory;
      numa_sizes[layer][numanode->os_index] += local_memory;
      MMB_NOISE("numa id: %d, layer: %s, size: %" PRIu64 "\n",
                numanode->os_index, mmb_layer_get_string(layer), local_memory);
    } else {
      MMB_NOISE("    not a numa node (\"%s\")\n", obj->name);
      /* Keep the ERR here as this case should never happen. It happening would
       * be a sign of an issue/change in the hwloc library behaviour/API. */
      MMB_ERR("Error while iterating NUMA Nodes.\n");
      stat = MMB_ERROR;
    }
  }
  return stat;
}

static mmbError
mmb_topology_check_gpus(const hwloc_topology_t topology, uint64_t *ex_cons)
{
  mmbError stat = MMB_OK;
  hwloc_obj_t obj = NULL;
  while (NULL != (obj = hwloc_get_next_osdev(topology, obj))) {
    /* Size is the memory size as reported by hwloc,
     * the factor is in case size is in kB.
     */
    uint64_t size = 0, factor = 1;
    const char *size_str = "";
    mmbExecutionContext ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;

    switch (obj->attr->osdev.type) {
      case HWLOC_OBJ_OSDEV_COPROC:
      case HWLOC_OBJ_OSDEV_GPU:
        {
          const char *backend = hwloc_obj_get_info_by_name(obj, "Backend");
          if (NULL != backend) {
            if (0 == strncmp("CUDA", backend, 4)) {
              MMB_NOISE("CUDA\n");
              factor = 1024;
              ex_con = MMB_GPU_CUDA;
              size_str = hwloc_obj_get_info_by_name(obj, "CUDAGlobalMemorySize");
            } else if (0 == strncmp("OpenCL", backend, 6)) {
              MMB_NOISE("OpenCL\n");
              factor = 1024;
              ex_con = MMB_OPENCL;
              size_str = hwloc_obj_get_info_by_name(obj, "OpenCLGlobalMemorySize");
            } else if (0 == strncmp("NVML", backend, 4)) {
              /* NVIDIA Management Library */
              MMB_NOISE("NVML\n");
            } else if (0 == strncmp("RSMI", backend, 4)) {
	       ex_con = MMB_GPU_HIP; 
	       size_str = NULL;
	       /* We cannot get size from hwloc for rocm devices yet */
               /*factor = 1024; 
               size_str = hwloc_obj_get_info_by_name(obj, "Size"); 
               MMB_NOISE("ROCm device, size: %s\n", size_str); */
	       MMB_NOISE("ROCm Device\n");
            }
            else {
              MMB_NOISE("Unknown coproc or gpu: %s\n", backend);
            }
          } /* silently ignore if no backend is provided */
        } /* FALLTHROUGH */
      default:
        break;
    }
    /* Check if the context is valid, i.e., if we detected a proper one */
    if (ex_con != MMB_EXECUTION_CONTEXT_DEFAULT) {
      if(NULL == size_str) {
	/* No size, use MAX */
	ex_cons[ex_con] = UINT64_MAX;
      } else if (1 != sscanf(size_str, "%" SCNu64, &size)) {
	/* Invalid size, warn but still use MAX */
        MMB_WARN("Unable to read the attribute value \"%s\".\n", size_str);
	ex_cons[ex_con] = UINT64_MAX;
      } else {
	/* Use available size */
        ex_cons[ex_con] += size * factor;
      }
    }
  }
  return stat;
}

mmbError
mmb_topology_init(hwloc_topology_t *topology, uint64_t * restrict layers,
                  uint64_t * restrict ex_cons, uint64_t * restrict numa_sizes[])
{
  mmbError stat = MMB_OK;

  /* Check parameters */
  if (NULL == topology) {
    MMB_ERR("Invalid topology handle (cannot be NULL).\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == layers) {
    MMB_ERR("Invalid layers uint64_t array. Should be at least %d.\n",
        MMB_MEMLAYER__MAX);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == ex_cons) {
    MMB_ERR("Invalid ex_cons uint64_t array. Should be at least %d.\n",
        MMB_EXECUTION_CONTEXT__MAX);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == numa_sizes) {
    MMB_ERR("Invalid numa_sizes[] uint64_t array. Should be at least %d.\n",
        MMB_MEMLAYER__MAX);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  /* Init and load of the topology using libhwloc */
  if (0 != hwloc_topology_init(topology)) {
    MMB_ERR("Error while initializing topology.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  hwloc_topology_set_all_types_filter(*topology, HWLOC_TYPE_FILTER_KEEP_ALL);
  hwloc_topology_set_io_types_filter(*topology, HWLOC_TYPE_FILTER_KEEP_IMPORTANT);

  if (0 != hwloc_topology_load(*topology)) {
    hwloc_topology_destroy(*topology); /* Free the structure */
    MMB_ERR("Error while loading topology.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  /* initialize the nodesets */
  int nnumanodes = hwloc_get_nbobjs_by_type(*topology, HWLOC_OBJ_NUMANODE);
  uint64_t *sizes = calloc(MMB_MEMLAYER__MAX, nnumanodes * sizeof *sizes);
  if (NULL == sizes) {
    MMB_ERR("Unable to allocate memory for NUMA sizes.\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  for (mmbMemLayer layer = MMB_DRAM; MMB_MEMLAYER__MAX > layer; ++layer) {
    numa_sizes[layer] = &sizes[layer * nnumanodes];
  }

  /* memory nodes */
  stat = mmb_topology_check_numanodes(*topology, layers, numa_sizes);
  if (MMB_OK != stat) {
    MMB_WARN("Error(s) occured during topology traversal of NUMA nodes.\n");
  }

  /* GPUs / Execution context */
  stat = mmb_topology_check_gpus(*topology, ex_cons);
  if (MMB_OK != stat) {
    MMB_WARN("Error(s) occured during topology traversal of GPUs.\n");
  }

 BAILOUT:
  if (MMB_OK != stat) {
    MMB_WARN("The result may not reflect perfectly the actual topology due to "
         "errors occuring during the topology discovery.\n");
  }
  return stat;
}

void mmb_topology_finalize(hwloc_topology_t topology)
{
  hwloc_topology_destroy(topology);
}
