/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_ALLOCATION_H
#define MMB_ALLOCATION_H

#include <stdlib.h>
#include <stdbool.h>

#include "../common/mmb_error.h"
#include "mmb_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbAllocation
 * @{ */

/**
 * @name Internal API
 * @{ */

mmbError mmb_alloc_allocate(void *ptr, const size_t n_bytes,
                            mmbMemInterface *interface,
                            const mmbAllocateOptions *opts,
                            const bool owned, mmbAllocation **alloc);

mmbError mmb_alloc_free(mmbAllocation *alloc);

mmbError mmb_alloc_initialize(void);
mmbError mmb_alloc_finalize(void);

/**
 * @brief Check the validity and coherency of the given allocaiton handle.
 *
 * This function may issue warning before returning the proper mmbError value.
 *
 * @param [in] alloc Allocation handle to be checked.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_alloc_check_params(const mmbAllocation *alloc);

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_ALLOCATION_H */
