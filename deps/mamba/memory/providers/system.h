/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_SYSTEM_H
#define MMB_SYSTEM_H

#include <stdlib.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"
#include "../i_memory.h"
#include "system_cuda.h"
#include "system_opencl.h"
#include "system_hip.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name GNU/Linux system memory provider
 * @{ */

/**
 * @brief Initialize the provider internal structure when OpenCL is enabled.
 *
 * @param [in] interface Handle to the structure to initialize.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_init__system(mmbMemInterface *interface);

/**
 * @brief Finalize the internal structure of the provider when OpenCL is
 *        enabled.
 *
 * @param [in] interface Handle to the structure to finalize.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_finalize__system(mmbMemInterface *interface);

/**
 * @brief Check whether the requested layer is supported by the system
 *        provider.
 *
 * @param [in] layer Requested layer.
 * @param [out] is_supported Handle for returned value.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_supports__system(const mmbMemLayer layer, bool need_numa_support,
                              bool *is_supported);

/**
 * @name GNU/Linux system DRAM memory provider
 * @{ */

typedef struct mmbSystem {
   char* p; //placeholder
} mmbSystem;

/**
 * @brief size of the \p mmbSystem structure.
 *
 * This parameter is required to initialize the static table in memory.c.
 * This value is used to compute the required size for an
 * \c struct mmbMemInterface.
 */
#define mmb_data_size__system sizeof(mmbSystem)

/**
 * @brief Allocate \p n_bytes of data in DRAM in a Unix environment.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Handle to the interface data structure.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [out] allocation Pointer to the allocated data.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocate__system_malloc(const size_t n_bytes,
                                     mmbMemInterface *interface,
                                     const mmbAllocateOptions *opts,
                                     void **allocation);

/**
 * @brief Free the memory and structures allocated by
 *        \p mmb_allocate__system_malloc.
 *
 * @param [in] allocation Data to be free'd.
 * @param [in] inter Interface managing the given data.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [in] n_bytes Size of the allocation to free.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_free__system_malloc(void *allocation, mmbMemInterface *inter,
                                 const mmbAllocateOptions *opts,
                                 const size_t n_bytes);

/**
 * @brief Copy a n-D matrix from \p src to \p dst.
 *
 * @param [out] dst Handle to the written data.
 * @param [in] dst_data Pointer to the strategy specific data (i.e.,
 *                      \p struct mmb_strat_thread_safe_data).
 * @param [in] dst_inter Interface managing the given destination data.
 * @param [in] dst_opts Pointer to the source's allocation options handle.  May
 *                      be \c NULL.
 * @param [in] doffset Offset in each dimensions at the begining of the
 *                     destination allocation.
 * @param [in] dpitch Pitch for each dimension of destination memory
 * @param [in] src Handle to the read data.
 * @param [in] src_inter Interface managing the given source data.
 * @param [in] src_opts Pointer to the source's allocation options handle.  May
 *                      be \c NULL.
 * @param [in] soffset Offset in each dimension at the begining of the source
 *                     allocation.
 * @param [in] spitch Pitch for each dimension of source memory.
 * @param [in] ndims Diemnsionality of both allocations and of the copied seciton.
 * @param [in] dims Dimensions of the copied section.  The first dimension is
 *                  given in bytes, then following dimensions are given in
 *                  number of previous dimensions.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_copy_nd__system_malloc(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *soffset, const size_t *spitch,
                                    const size_t ndims, const size_t *dims);

/**  @} */

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_SYSTEM_H */
