/*
 * Copyright (C) 2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include <umpire/interface/umpire.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../interface.h"
#include "umpire.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Umpire memory provider
 * @{ */

/**
 * @brief Register a memory pointer to Umpire so an operation can be executed
 *        on it.
 *
 * @param [inout]   rm The resource manager, as wrapped by the Umpire library.
 * @param [in]      ptr The pointer to be registered.
 * @param [in]      size The size of the memory segment to be registered.
 * @param [in]      allocator The allocator corresponding to the memory layer,
 *                            as wrapped by the Umpire library.
 * @return \c MMB_OK on success.
 */
extern mmbError
mmb_umpire_resourcemanager_register_ptr(umpire_resourcemanager *rm,
                                        void *ptr, size_t size,
                                        umpire_allocator *allocator);

static
mmbError mmb_copy_1d__umpire(void *dst, const size_t doffset,
                             const void *src, const size_t soffset,
                             const size_t width);

/**
 * @brief Deregister a memory pointer from Umpire.
 *
 * @param [inout]   rm The resource manager, as wrapped by the Umpire library.
 * @param [in]      ptr The registered pointer.
 * @return \c MMB_OK on success.
 */
extern mmbError
mmb_umpire_resourcemanager_deregister_ptr(umpire_resourcemanager *rm, void *ptr);

/**
 * @brief Instance of the umpire::ResourceManager.
 */
static umpire_resourcemanager resource_manager = {};

struct mmb_umpire_allocation_data {
  umpire_allocator ptr;
  size_t id;
  char *name;
};

static struct mmb_umpire_allocation_data allocators[MMB_UMPIRE_ALLOCATOR__MAX] = {
  [MMB_UMPIRE_ALLOCATOR_HOST] = { .name = "HOST" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA] = { .name = "DEVICE" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA_CONSTANT] = { .name = "" },
  [MMB_UMPIRE_ALLOCATOR_HOST_CUDA_PINNED] = { .name = "PINNED" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA_UM] = { .name = "UM" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_HIP] = { .name = "DEVICE" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_HIP_CONSTANT] = { .name = "" },
  [MMB_UMPIRE_ALLOCATOR_HOST_HIP_PINNED] = { .name = "PINNED" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_SYCL] = { .name = "DEVICE" },
  [MMB_UMPIRE_ALLOCATOR_HOST_SYCL_PINNED] = { .name = "PINNED" },
  [MMB_UMPIRE_ALLOCATOR_DEVICE_SYCL_UM] = { .name = "UM" },
  [MMB_UMPIRE_ALLOCATOR_OPENMP_TARGET] = { .name = "" },
};

/**
 * @brief Translate the enumerate value from mamba to mmbUmpireAllocator id.
 *
 * @param [in] layer The memory layer requested.
 * @param [out] allocator_id The id coresponding to the requested layer and
 *                           execution context
 *
 * @return \c MMB_OK on success, \c MMB_INVALID_LAYER if the requested layer
 *         support is not provided by Umpire.
 */
static inline mmbError
mmb_umpire_translate_device_tag(const mmbMemLayer layer,
                                const mmbExecutionContext ex_con,
                                enum mmbUmpireAllocator *allocator_id)
{
  enum mmbUmpireAllocator alloc_id = MMB_UMPIRE_ALLOCATOR_INVALID;
  mmbError stat = MMB_OK;
  switch (layer) {
    case MMB_DRAM:
      if (MMB_CPU == ex_con)
        alloc_id = MMB_UMPIRE_ALLOCATOR_HOST;
      else if (MMB_GPU_CUDA == ex_con)
        alloc_id = MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA;
      break;
    case MMB_GDRAM:
      if (MMB_CPU == ex_con) {
#if defined(UMPIRE_ENABLE_CUDA)
#  if defined(UMPIRE_ENABLE_PINNED)
        alloc_id = MMB_UMPIRE_ALLOCATOR_HOST_CUDA_PINNED;
#  elif defined(UMPIRE_ENABLE_UM)
        alloc_id = MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA_UM;
#  else
        stat = MMB_INVALID_EXECUTION_CONTEXT;
        MMB_ERR("Incompatible or invalid execution context requested.\n");
#  endif /* UMPIRE_ENALE_CUDA */

#elif defined(UMPIRE_ENABLE_HIP)
        stat = MMB_INVALID_EXECUTION_CONTEXT;
        MMB_ERR("Incompatible or invalid execution context requested.\n");
#else
        stat = MMB_INVALID_EXECUTION_CONTEXT;
        MMB_ERR("Incompatible or invalid execution context requested.\n");
#endif /* UMPIRE_ENABLE_HIP */
      } else if (MMB_GPU_CUDA == ex_con)
        alloc_id = MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA;
      break;
    default:
      stat = MMB_INVALID_LAYER;
      MMB_ERR("Incompatible or invalid layer requested.\n");
      break;
  }
  *allocator_id = alloc_id;
  return stat;
}

mmbError mmb_init__umpire(const mmbProviderInitParam *param)
{
  (void) param;
  MMB_DEBUG("Calling mmb_init__umpire()\n");
  umpire_resourcemanager_get_instance(&resource_manager);
  if (0 == resource_manager.addr) {
    MMB_ERR("Unable to retrieve Umpire's resource manager. Abort.\n");
    return MMB_ERROR;
  }
  for (enum mmbUmpireAllocator id = 0; id < MMB_UMPIRE_ALLOCATOR__MAX; ++id) {
    switch (id) {
      case MMB_UMPIRE_ALLOCATOR_HOST:

#if defined(UMPIRE_ENABLE_CUDA)
      case MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA:
      case MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA_CONSTANT:
#  if defined(UMPIRE_ENABLE_PINNED)
      case MMB_UMPIRE_ALLOCATOR_HOST_CUDA_PINNED:
#  elif defined(UMPIRE_ENABLE_UM)
      case MMB_UMPIRE_ALLOCATOR_DEVICE_CUDA_UM:
#  endif
#endif /* defined(UMPIRE_ENABLE_CUDA) */

#if defined(UMPIRE_ENABLE_HIP)
      case MMB_UMPIRE_ALLOCATOR_DEVICE_HIP:
      case MMB_UMPIRE_ALLOCATOR_DEVICE_HIP_CONSTANT:
#  if defined(UMPIRE_ENABLE_PINNED)
      case MMB_UMPIRE_ALLOCATOR_HOST_HIP_PINNED:
#  elif defined(UMPIRE_ENABLE_UM)
#  endif
#endif /* defined(UMPIRE_ENABLE_HIP) */

#if defined(UMPIRE_ENABLE_SYCL)
      case MMB_UMPIRE_ALLOCATOR_DEVICE_SYCL:
#  if defined(UMPIRE_ENABLE_PINNED)
      case MMB_UMPIRE_ALLOCATOR_HOST_SYCL_PINNED:
#  elif defined(UMPIRE_ENABLE_UM)
      case MMB_UMPIRE_ALLOCATOR_DEVICE_SYCL_UM:
#  endif
#endif /* defined(UMPIRE_ENABLE_SYCL) */

#if defined(UMPIRE_ENABLE_OPENMP_TARGET)
      case MMB_UMPIRE_ALLOCATOR_OPENMP_TARGET:
#endif /* defined(UMPIRE_ENABLE_OPENMP_TARGET) */

        if ( '\0' != *allocators[id].name ) {
          umpire_resourcemanager_get_allocator_by_name_bufferify(&resource_manager,
                                                                 allocators[id].name,
                                                                 strlen(allocators[id].name),
                                                                 &allocators[id].ptr);
          allocators[id].id = umpire_allocator_get_id(&allocators[id].ptr);
        }
        break;
      default:
        continue;
    }
  }
  return MMB_OK;
}

mmbError mmb_finalize__umpire(void)
{
  MMB_DEBUG("Calling mmb_finalize__umpire()\n");
  for (enum mmbUmpireAllocator id = 0; id < MMB_UMPIRE_ALLOCATOR__MAX; ++id) {
    if (NULL != allocators[id].ptr.addr)
      umpire_allocator_release(&allocators[id].ptr);
  }
  resource_manager = (const umpire_resourcemanager){ 0 };
  return MMB_OK;
}

mmbError mmb_init_interface__umpire(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  enum mmbUmpireAllocator allocator_id;

  struct mmbUmpire *umpire_data = (struct mmbUmpire *)interface->data;
  if (NULL == umpire_data) {
    MMB_ERR("Interface has invalid data handle.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  const mmbMemLayer layer = interface->space->layer;
  const mmbExecutionContext ex_con = interface->space->ex_context;
  stat = mmb_umpire_translate_device_tag(layer, ex_con, &allocator_id);
  if (MMB_OK != stat) {
    MMB_WARN("Interface cannot be initialized for layer %d.\n", layer);
    goto BAILOUT;
  }
  umpire_data->allocator = &allocators[allocator_id].ptr;
BAILOUT:
  return stat;
}

mmbError mmb_finalize_interface__umpire(mmbMemInterface *interface)
{
  (void) interface;
  return MMB_OK;
}

mmbError mmb_supports__umpire(const mmbMemLayer layer, bool need_numa_support,
                              bool *is_supported)
{
  if (NULL == is_supported) {
    return MMB_INVALID_ARG;
  }
  switch (layer) {
    case MMB_DRAM:
#if defined(UMPIRE_ENABLE_CUDA)
      /* || defined(UMPIRE_ENABLE_HCC) Not supported yet */
    case MMB_GDRAM:
#endif /* Support from GPU */
      *is_supported = true;
      break;
    case MMB_HBM:
    case MMB_NVDIMM:
    case MMB_NADRAM:
    case MMB_SSD:
    default:
      *is_supported = false;
      break;
  }
  /* No NUMA support */
  *is_supported &= !need_numa_support;
  return MMB_OK;
}

mmbError mmb_allocate__umpire(const size_t n_bytes,
                              mmbMemInterface *interface,
                              const mmbAllocateOptions *opts,
                              void **out_allocation)
{
  (void) opts;
  mmbError stat = MMB_ERROR;

  MMB_DEBUG("Calling mmb_allocate__umpire()\n");

  struct mmbUmpire *umpire_data = (struct mmbUmpire *)interface->data;
  if (NULL == umpire_data) {
    MMB_ERR("Interface has invalid data handle.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  void *ptr = umpire_allocator_allocate(umpire_data->allocator, n_bytes);
  if (NULL == ptr) {
    MMB_ERR("Could not allocate %zu bytes for data.\n", n_bytes);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *out_allocation = ptr;
  stat = MMB_OK;

BAILOUT:
  return stat;
}

mmbError mmb_free__umpire(void *allocation, mmbMemInterface *interface,
                          const mmbAllocateOptions *opts,
                          const size_t n_bytes)
{
  (void) opts;
  (void) n_bytes;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling mmb_free__umpire()\n");

  struct mmbUmpire *umpire_data = (struct mmbUmpire *)interface->data;
  if (NULL == umpire_data) {
    MMB_ERR("Interface has invalid data handle.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  umpire_allocator_deallocate(umpire_data->allocator, allocation);

BAILOUT:
  return stat;
}

static
mmbError mmb_copy_1d__umpire(void *dst,
                             const size_t doffset,
                             const void *src,
                             const size_t soffset,
                             const size_t width)
{
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling mmb_copy_1d__umpire()\n");

  dst = (unsigned char *) dst + doffset;
  src = (const unsigned *) src + soffset;

  MMB_DEBUG("mmb_copy_1d__umpire copying %zu bytes from %p to %p\n", width, src, dst);

  memcpy(dst, src, width);

  return stat;
}

mmbError mmb_copy_nd__umpire(void *dst, mmbMemInterface *dst_inter,
                             const mmbAllocateOptions *dst_opts,
                             const size_t *doffset, const size_t *dpitch,
                             const void *src, const mmbMemInterface *src_inter,
                             const mmbAllocateOptions *src_opts,
                             const size_t *soffset, const size_t *spitch,
                             const size_t ndims, const size_t *dims)
{
  (void) dst_inter;
  (void) dst_opts;
  (void) src_inter;
  (void) src_opts;
  (void) dpitch;
  (void) spitch;

  /* Check parameters */
  if (NULL == dst || NULL == src) {
    return MMB_INVALID_ARG;
  }
  if (MMB_CPU != dst_inter->space->ex_context
      || MMB_CPU != src_inter->space->ex_context) {
    return MMB_INVALID_EXECUTION_CONTEXT;
  }

  switch (ndims) {
    case 1:
      return mmb_copy_1d__umpire(dst, doffset[0] * dpitch[0],
                                 src, soffset[0] * spitch[0],
                                 dims[0] * spitch[0]);
    default:
      return MMB_UNSUPPORTED;
  }
}

/**  @} */

/**  @} */

/**  @} */
