/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <jemalloc/jemalloc.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../interface.h"

/* headers specific to each external malloc providers. */
#include "conf_malloc_providers.h"

#define MMB_JEMALLOC_MALLOC(sz) PREFIXED_MALLOC(MMB_JEMALLOC_PREFIX, sz)
#define MMB_JEMALLOC_FREE(ptr) PREFIXED_FREE(MMB_JEMALLOC_PREFIX, ptr)

mmbError mmb_init__jemalloc(mmbMemInterface *interface)
{
  (void) interface;
  MMB_DEBUG("Calling jemalloc initializer()\n");
  return MMB_OK;
}

mmbError mmb_finalize__jemalloc(mmbMemInterface *interface)
{
  (void) interface;
  MMB_DEBUG("Calling jemalloc finalizer()\n");
  return MMB_OK;
}

mmbError mmb_supports__jemalloc(const mmbMemLayer layer, bool need_numa_support,
                                bool *is_supported)
{
  if (NULL == is_supported) {
    return MMB_INVALID_ARG;
  }
  switch (layer) {
    case MMB_DRAM:
      *is_supported = true;
      break;
    case MMB_GDRAM:
    case MMB_HBM:
    case MMB_NVDIMM:
    case MMB_NADRAM:
    case MMB_SSD:
    default:
      *is_supported = false;
      break;
  }
  /* No NUMA support */
  *is_supported &= !need_numa_support;
  return MMB_OK;
}

/**
 * @name jemalloc allocation
 * @{ */

mmbError mmb_allocate__jemalloc(const size_t n_bytes,
                                mmbMemInterface *interface,
                                const mmbAllocateOptions *opts,
                                void **out_allocation)
{
  (void) opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling jemalloc allocator()\n");

  if (MMB_DRAM != interface->space->layer) {
    stat = MMB_INVALID_LAYER;
    goto BAILOUT;
  }

  void *ptr = MMB_JEMALLOC_MALLOC(n_bytes);
  if(NULL == ptr) {
    MMB_ERR("Could not allocate %zu bytes for mmbAllocation\n", n_bytes);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *out_allocation = ptr;

BAILOUT:
  return stat;
}

mmbError mmb_free__jemalloc(void *allocation, mmbMemInterface *inter,
                            const mmbAllocateOptions *opts,
                            const size_t n_bytes)
{
  (void) opts;
  (void) n_bytes;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling jemalloc deallocator(%p)\n", allocation);

  if (MMB_DRAM != inter->space->layer) {
    stat = MMB_INVALID_LAYER;
    goto BAILOUT;
  }

  MMB_JEMALLOC_FREE(allocation);

BAILOUT:
  return stat;
}

/**  @} */

