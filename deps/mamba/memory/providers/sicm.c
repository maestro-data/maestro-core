/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include <sicm_low.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../interface.h"
#include "../numa_utils.h"
#include "sicm.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name SICM memory provider
 * @{ */

/**
 * @brief list of devices as returned by sicm_init() from sicm_low
 */
static sicm_device_list devices = {};

/**
 * @brief Translate the enumerate value from mamba to a SICM device tag.
 *
 * @param [in] layer The memory layer requested.
 * @param [out] out_tag The equivalent tag for SICM.
 *
 * @return \c MMB_OK on success, \c MMB_INVALID_LAYER if the requested layer
 *         support is not provided by sicm.
 */
static inline mmbError
mmb_sicm_translate_device_tag(const mmbMemLayer layer, sicm_device_tag *out_tag)
{
  sicm_device_tag tag = INVALID_TAG;
  mmbError stat = MMB_INVALID_LAYER;
  switch (layer) {
    case MMB_DRAM:
      tag = SICM_DRAM;
      stat = MMB_OK;
      break;
    case MMB_HBM:
/* Tests taken from https://github.com/lanl/SICM/blob/master/src/low/sicm_low.c */
#if defined(__x86_64__) /* KNL */
      tag = SICM_KNL_HBM;
      stat = MMB_OK;
#elif defined(__powerpc__) /* PowerPC */
      tag = SICM_POWERPC_HBM;
      stat = MMB_OK;
#endif
      break;
    case MMB_NVDIMM:
/* Tests taken from https://github.com/lanl/SICM/blob/master/src/low/sicm_low.c */
#if defined(__x86_64__)
      tag = SICM_OPTANE;
      stat = MMB_OK;
#endif
      break;
    default:
      MMB_ERR("Incompatible or invalid layer requested.\n");
      break;
  }
  *out_tag = tag;
  return stat;
}

mmbError mmb_init__sicm(const mmbProviderInitParam *param)
{
  (void) param;
  MMB_DEBUG("Calling mmb_init__sicm()\n");
  devices = sicm_init();
  if (0 == devices.count) {
    MMB_ERR("No suitable devices found from SICM. Abort.\n");
    return MMB_ERROR;
  }
  return MMB_OK;
}

mmbError mmb_finalize__sicm(void)
{
  MMB_DEBUG("Calling mmb_finalize__sicm()\n");
  devices = (const sicm_device_list){ 0 };
  sicm_fini();
  return MMB_OK;
}

mmbError mmb_init_interface__sicm(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  sicm_device_tag dev_tag = INVALID_TAG;

  struct mmbSICM *sicm_data = (struct mmbSICM *)interface->data;
  if (NULL == sicm_data) {
    MMB_ERR("Interface has invalid data handle.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  const mmbMemLayer layer = interface->space->layer;
  stat = mmb_sicm_translate_device_tag(layer, &dev_tag);
  if (MMB_OK != stat) {
    MMB_WARN("Interface cannot be initialized for layer %d.\n", layer);
    goto BAILOUT;
  }
  sicm_data->device = sicm_find_device(&devices, dev_tag, 0, NULL);
BAILOUT:
  return stat;
}

mmbError mmb_finalize_interface__sicm(mmbMemInterface *interface)
{
  (void) interface;
  return MMB_OK;
}

mmbError mmb_supports__sicm(const mmbMemLayer layer, bool need_numa_support,
                            bool *is_supported)
{
  if (NULL == is_supported) {
    return MMB_INVALID_ARG;
  }
  switch (layer) {
    case MMB_DRAM:
    case MMB_HBM:
    case MMB_NVDIMM:
      *is_supported = true;
      break;
    case MMB_GDRAM:
    case MMB_NADRAM:
    case MMB_SSD:
    default:
      *is_supported = false;
      break;
  }
#if HAVE_NUMAIF_H
  need_numa_support = true;
#else
  need_numa_support = !need_numa_support;
#endif
  *is_supported &= need_numa_support;
  return MMB_OK;
}

mmbError mmb_allocate__sicm(const size_t n_bytes,
                            mmbMemInterface *interface,
                            const mmbAllocateOptions *opts,
                            void **out_allocation)
{
  mmbError stat = MMB_ERROR;

  MMB_DEBUG("Calling mmb_allocate__sicm()\n");

  struct mmbSICM *sicm_data = (struct mmbSICM *)interface->data;
  if (NULL == sicm_data) {
    MMB_ERR("Interface has invalid data handle.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  void *ptr = sicm_device_alloc(sicm_data->device, n_bytes);
  if (NULL == ptr) {
    MMB_ERR("Could not allocate %zu bytes for data.\n", n_bytes);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  stat = MMB_OK;

  if (opts && opts->is_numa_allocation) {
    stat = mmb_numa_move_page(&ptr, n_bytes, opts->numa.node_id);
    if (MMB_OK != stat)
      mmb_free__sicm(ptr, interface, opts, n_bytes);
  }

  *out_allocation = ptr;

BAILOUT:
  return stat;
}

mmbError mmb_free__sicm(void *allocation, mmbMemInterface *interface,
                        const mmbAllocateOptions *opts,
                        const size_t n_bytes)
{
  (void) opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling mmb_free__sicm()\n");

  struct mmbSICM *sicm_data = (struct mmbSICM *)interface->data;
  if (NULL == sicm_data) {
    MMB_ERR("Interface has invalid data handle.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  sicm_device_free(sicm_data->device, allocation, n_bytes);

BAILOUT:
  return stat;
}

/**  @} */

/**  @} */

/**  @} */
