/*
 * Copyright (C) 2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>

#include <umpire/interface/umpire.h>
#include <umpire/Allocator.hpp>
#include <umpire/ResourceManager.hpp>

extern "C" {
#include "../../common/mmb_error.h"
}

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Umpire memory provider
 * @{ */

mmbError mmb_umpire_resourcemanager_register_ptr(umpire_resourcemanager *rm,
                                                 void *ptr, std::size_t size,
                                                 umpire_allocator *allocator)
{
  /* Get Allocation Strategy */
  umpire::Allocator *Alloc_this =
    static_cast<umpire::Allocator *>(allocator->addr);
  umpire::strategy::AllocationStrategy *strat = Alloc_this->getAllocationStrategy();
  /* Register allocation */
  umpire::ResourceManager *RM_this =
    static_cast<umpire::ResourceManager *>(rm->addr);
  RM_this->registerAllocation(ptr, {ptr, size, strat});
  /* Create AllocationRecord */
  /* umpire::util::AllocationRecord record = */
  /*   new umpire::util::AllocationRecord({ptr, size, strat}); */
  /* RM_this->registerAllocation(ptr, record); */
  return MMB_OK;
}

mmbError mmb_umpire_resourcemanager_deregister_ptr(umpire_resourcemanager *rm,
                                                   void *ptr)
{
  /* Deregister allocation */
  umpire::ResourceManager *RM_this =
    static_cast<umpire::ResourceManager *>(rm->addr);
  RM_this->deregisterAllocation(ptr);
  /* umpire::util::AllocationRecord record = RM_this->deregisterAllocation(ptr); */
  /* delete record; */
  return MMB_OK;
}

/**  @} */

/**  @} */

/**  @} */
