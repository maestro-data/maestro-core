/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "../common/mmb_error.h"

#include "mmb_memory.h"
#include "i_memory.h"
#include "strategies/i_statistics.h"

/**
 * @brief Initialize global parameters of the statistics strategy.
 *
 * @param [out] allocation_stats_records Handle to the hashmap. Cannot be \c NULL.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_stats_init(mmbAllocStats *allocation_stats_records);

/**
 * @brief Finalize statistics recording hashmap used in the statistics strategy.
 *
 * @param [out] allocation_stats_records Handle to the hashmap. Cannot be \c NULL.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_stats_finalize(mmbAllocStats *allocation_stats_records);

mmbError mmb_stats_register_interface(mmbAllocStats *allocation_stats_records,
                                      mmbMemInterface *interface,
                                      struct mmb_strat_stats_data *data);

mmbError mmb_stats_deregister_interface(mmbAllocStats *allocation_stats_records,
                                        mmbMemInterface *interface,
                                        struct mmb_strat_stats_data *data);

mmbError mmb_stats_get_current(mmbAllocStats *allocation_stats_records,
                               mmbMemInterface *interface,
                               size_t *n_allocations,
                               size_t *bytes_allocated);

mmbError mmb_stats_get_total(mmbAllocStats *allocation_stats_records,
                             mmbMemInterface *interface,
                             size_t *n_allocations,
                             size_t *bytes_allocated);

/**
 * @brief Returns the number of records stored into the hashmap.
 *
 * @param [in] allocation_stats_records Hashmap handle. Cannot be \c NULL.
 * @param [out] count Handle to an already allocated variable to return the size.
 *                    Cannot be \c NULL.
 *
 * @return \c MMB_OK on succes, an error type otherwise.
 */
mmbError
mmb_stats_get_record_count(mmbAllocStats *allocation_stats_records, size_t *count);

