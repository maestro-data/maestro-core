#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#include "mmb_error.h"
#include "mmb_memory.h"
#include "mmb_logging.h"

#include "i_memory.h"
#include "strategy.h"
#include "interface.h"
#include "numa_utils.h"
#include "providers/system.h"
#include "providers/system_cuda.h"
#include "providers/system_opencl.h"
#include "providers/system_hip.h"
#if HAVE_SICM
#include "providers/sicm.h"
#endif /* HAVE_SICM */
#if HAVE_UMPIRE
#include "providers/umpire.h"
#endif /* HAVE_UMPIRE */
#if HAVE_JEMALLOC
#include "providers/jemalloc.h"
#endif /* HAVE_JEMALLOC */

#include "strategies/apply.h"
#include "strategies/i_strategy.h"
#include "strategies/i_basic.h"
#include "strategies/i_pooled.h"
#include "strategies/i_thread_safe.h"
#include "strategies/i_statistics.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @brief Provider dispatch table.
 *
 * We use C99 named initializers, and rely on C static data being 0-initialized
 * so that the 'enabled' fields of the entries are 0 if we actually stored an
 * entry for the respective substrate.
 */
static const struct mmb_dispatch_provider_entry
mmb_providers_table[MMB_PROVIDER__MAX] = {
  [MMB_SYSTEM] = {
    .name      = "SYSTEM",
    .data_size = mmb_data_size__system,
    .supports  = mmb_supports__system,
    .ops = {
      [MMB_CPU] = {
        .enabled  = true,
        .init     = mmb_init__system,
        .finalize = mmb_finalize__system,
        .allocate = mmb_allocate__system_malloc,
        .free     = mmb_free__system_malloc,
        .copy_nd  = mmb_copy_nd__system_malloc,
      },
#if HAVE_CUDA
      [MMB_GPU_CUDA] = {
        .enabled  = true,
        .init     = mmb_init__system,
        .finalize = mmb_finalize__system,
        .allocate = mmb_allocate__system_cuda,
        .free     = mmb_free__system_cuda,
        .copy_nd  = mmb_copy_nd__system_cuda,
      },
#endif /* HAVE_CUDA */
#if HAVE_ROCM
      [MMB_GPU_HIP] = {
        .enabled  = true,
        .init     = mmb_init__system,
        .finalize = mmb_finalize__system,
        .allocate = mmb_allocate__system_hip,
        .free     = mmb_free__system_hip,
        .copy_nd  = mmb_copy_nd__system_hip,
      },
#endif /* HAVE_ROCM */
#if HAVE_OPENCL
      [MMB_OPENCL] = {
        .enabled  = true,
        .init     = mmb_init__system,
        .finalize = mmb_finalize__system,
        .allocate = mmb_allocate__system_opencl,
        .free     = mmb_free__system_opencl,
        .copy_nd  = mmb_copy_nd__system_opencl,
      },
#endif /* HAVE_OPENCL */
    }
  },
#if HAVE_SICM
  [MMB_SICM] = {
    .name      = "SICM_LOW",
    .data_size = mmb_data_size__sicm,
    .supports  = mmb_supports__sicm,
    .init      = mmb_init__sicm,
    .finalize  = mmb_finalize__sicm,
    .ops = {
      [MMB_CPU]
        = {
          .enabled  = true,
          .init     = mmb_init_interface__sicm,
          .finalize = mmb_finalize_interface__sicm,
          .allocate = mmb_allocate__sicm,
          .free     = mmb_free__sicm,
          .copy_nd  = mmb_copy_nd__system_malloc,
        },
    }
  },
#endif /* HAVE_SICM */
#if HAVE_UMPIRE
  [MMB_UMPIRE] = {
    .name      = "UMPIRE",
    .data_size = mmb_data_size__umpire,
    .supports  = mmb_supports__umpire,
    .init      = mmb_init__umpire,
    .finalize  = mmb_finalize__umpire,
    .ops = {
      [MMB_CPU]
        = {
          .enabled  = true,
          .init     = mmb_init_interface__umpire,
          .finalize = mmb_finalize_interface__umpire,
          .allocate = mmb_allocate__umpire,
          .free     = mmb_free__umpire,
          .copy_nd  = mmb_copy_nd__umpire,
        },
#  if HAVE_CUDA
      [MMB_GPU_CUDA] = {
        .enabled  = true,
        .init     = mmb_init_interface__umpire,
        .finalize = mmb_finalize_interface__umpire,
        .allocate = mmb_allocate__umpire,
        .free     = mmb_free__umpire,
        .copy_nd  = mmb_copy_nd__umpire,
      },
#  endif /* HAVE_CUDA */
    }
  },
#endif /* HAVE_UMPIRE */
#if HAVE_JEMALLOC
  [MMB_JEMALLOC] = {
    .name      = "JEMALLOC",
    .data_size = mmb_data_size__jemalloc,
    .supports  = mmb_supports__jemalloc,
    .ops = {
      [MMB_CPU] = {
        .enabled  = true,
        .init     = mmb_init__jemalloc,
        .finalize = mmb_finalize__jemalloc,
        .allocate = mmb_allocate__jemalloc,
        .free     = mmb_free__jemalloc,
        .copy_nd  = mmb_copy_nd__system_malloc,
      },
    }
  },
#endif /* HAVE_JEMALLOC */
};

/**
 * @brief Strategies dispatch table.
 *
 * This table is used to retrieve the addresses of the functions used as
 * building blocks when defining a new mmbStrategy in a strategy builder.
 */
const struct mmb_dispatch_strategy_va_entry
mmb_strategies_va_table[MMB_STRATEGY__MAX] = {
  [MMB_STRATEGY_NONE]
    = {
        .init_interface     = mmb_strat_init_interface_va__basic,
        .finalize_interface = mmb_strat_finalize_interface_va__basic,
        .allocate           = mmb_strat_allocate_va__basic,
        .free               = mmb_strat_free_va__basic,
        .copy_nd            = mmb_strat_copy_nd_va__basic,
        .data_size          = mmb_strat_data_size__basic,
    },
  [MMB_POOLED]
    = {
        .init               = mmb_strat_init__pooled,
        .init_interface     = mmb_strat_init_interface_va__pooled,
        .finalize_interface = mmb_strat_finalize_interface_va__pooled,
        .allocate           = mmb_strat_allocate_va__pooled,
        .free               = mmb_strat_free_va__pooled,
        .copy_nd            = mmb_strat_copy_nd_va__pooled,
        .data_size          = mmb_strat_data_size__pooled,
    },
  [MMB_THREAD_SAFE]
    = {
        .init_interface     = mmb_strat_init_interface_va__thread_safe,
        .finalize_interface = mmb_strat_finalize_interface_va__thread_safe,
        .allocate           = mmb_strat_allocate_va__thread_safe,
        .free               = mmb_strat_free_va__thread_safe,
        .copy_nd            = mmb_strat_copy_nd_va__thread_safe,
        .data_size          = mmb_strat_data_size__thread_safe,
    },
  [MMB_STATISTICS]
    = {
        .init_interface     = mmb_strat_init_interface_va__stats,
        .finalize_interface = mmb_strat_finalize_interface_va__stats,
        .allocate           = mmb_strat_allocate_va__stats,
        .free               = mmb_strat_free_va__stats,
        .copy_nd            = mmb_strat_copy_nd_va__stats,
        .data_size          = mmb_strat_data_size__stats,
    },
};

mmbError mmb_strat_get_data_size(const mmbProvider provider,
                                 const mmbStrategy strategy,
                                 size_t *out_data_size)
{
  mmbError stat = MMB_INVALID_ARG;
  size_t data_size = 0;
  if (NULL == out_data_size) {
    MMB_ERR("Invalid pointer \"out_data_size\".\n");
    goto BAILOUT;
  }
  switch (provider) {
    case MMB_SYSTEM:
#if HAVE_SICM
    case MMB_SICM:
#endif /* HAVE_SICM */
#if HAVE_UMPIRE
    case MMB_UMPIRE:
#endif /* HAVE_UMPIRE */
#if HAVE_JEMALLOC
    case MMB_JEMALLOC:
#endif /* HAVE_JEMALLOC */
      data_size += mmb_provider_get_data_size(provider);
      break;
    default:
      MMB_ERR("Invalid provider data size requested.\n");
      stat = MMB_INVALID_PROVIDER;
      goto BAILOUT;
  }
  switch (strategy) {
    case MMB_STRATEGY_NONE:
    case MMB_POOLED:
    case MMB_THREAD_SAFE:
    case MMB_STATISTICS:
      data_size += mmb_strategies_va_table[strategy].data_size;
      break;
    case MMB_POOLED_STATISTICS:
      data_size += mmb_strategies_va_table[MMB_POOLED].data_size;
      data_size += mmb_strategies_va_table[MMB_STATISTICS].data_size;
      break;
    default:
      MMB_ERR("Invalid strategy data size requested.\n");
      stat = MMB_INVALID_STRATEGY;
      goto BAILOUT;
  }
  *out_data_size = data_size;
  stat = MMB_OK;
BAILOUT:
  return stat;
}

mmbError mmb_provider_compatibility_check(const mmbProvider provider,
                                          const mmbMemLayer layer,
                                          const mmbExecutionContext ex_con,
                                          bool need_numa_support,
                                          bool *is_available)
{
  mmbError stat = MMB_OK;
  if (NULL == is_available) {
    return MMB_INVALID_ARG;
  } else if (MMB_PROVIDER__MAX <= provider) {
    return MMB_INVALID_PROVIDER;
  }
  bool avail = mmb_providers_table[provider].ops[ex_con].enabled;
  if (avail)
    stat = mmb_providers_table[provider].supports(layer, need_numa_support, &avail);
  *is_available = avail;
  return stat;
}

size_t mmb_provider_get_data_size(const mmbProvider provider)
{
  if (0 > provider || MMB_PROVIDER__MAX < provider) {
    MMB_ERR("Invalid provider requested (%d).\n", provider);
    return 0;
  }
  return mmb_providers_table[provider].data_size;
}

mmbError mmb_init_providers(const mmbProviderInitParamSet params)
{
  mmbError stat = MMB_OK;
  if (NULL == params) {
    MMB_WARN("Param list empty, use default parameters.\n");
  }
  mmbProvider provider;
  const mmbProviderInitParam *param = params;
  for (provider = MMB_SYSTEM; provider < MMB_PROVIDER__MAX; ++provider) {
    if (NULL != mmb_providers_table[provider].init)
      stat = mmb_providers_table[provider].init(param);
    if (MMB_OK != stat) {
      MMB_WARN("Failed to initialize provider %s (err=%d)\n",
          mmb_provider_get_string(provider), stat);
      goto BAILOUT;
    }
    param = (NULL == params) ? params : param + 1;
  }
BAILOUT:
  return stat;
}

mmbError mmb_finalize_providers(void)
{
  mmbError stat = MMB_OK;
  mmbProvider provider;
  for (provider = MMB_SYSTEM; provider < MMB_PROVIDER__MAX; ++provider) {
    if (NULL != mmb_providers_table[provider].finalize)
      stat = mmb_providers_table[provider].finalize();
    if (MMB_OK != stat)
      MMB_WARN("Failed to finalize provider %s (err=%d)\n",
          mmb_provider_get_string(provider), stat);
  }
  return stat;
}

mmbError mmb_init_strategies(const mmbStrategyInitParamSet params)
{
  mmbError stat = MMB_OK;
  if (NULL == params) {
    MMB_WARN("Param list empty, use default parameters.\n");
  }
  mmbStrategy strategy;
  const mmbStrategyInitParam *param = params;
  for (strategy = MMB_STRATEGY_NONE; strategy < MMB_STRATEGY__MAX; ++strategy) {
    if (NULL != mmb_strategies_va_table[strategy].init)
      stat = mmb_strategies_va_table[strategy].init(param);
    if (MMB_OK != stat) {
      MMB_WARN("Failed to initialize strategy %s (err=%d)\n",
          mmb_strategy_get_string(strategy), stat);
      goto BAILOUT;
    }
    param = (NULL == params) ? params : param + 1;
  }
BAILOUT:
  return stat;
}

mmbError mmb_finalize_strategies(void)
{
  mmbError stat = MMB_OK;
  mmbStrategy strategy;
  for (strategy = MMB_STRATEGY_NONE; strategy < MMB_STRATEGY__MAX; ++strategy) {
    if (NULL != mmb_strategies_va_table[strategy].finalize)
      stat = mmb_strategies_va_table[strategy].finalize();
    if (MMB_OK != stat)
      MMB_WARN("Failed to finalize strategy %s (err=%d)\n",
          mmb_strategy_get_string(strategy), stat);
  }
  return stat;
}

/** va_args versions */

/**
 * @name Private API
 *
 * Because of the need to access mmb_providers_table for the MMB_STRATEGY_NONE
 * strategy functions, these functions are to be found in this file (the
 * provider dispatch table is static, so not exposed outside this file).
 *
 * @{ */

mmbError mmb_strat_init_interface_va__basic(mmbMemInterface *interface,
                                            void *data,
                                            va_list args)
{
  (void) data;
  (void) args;
  const mmbProvider provider = interface->provider;
  const mmbExecutionContext ex_con = interface->space->ex_context;
  return mmb_providers_table[provider].ops[ex_con].init(interface);
}

mmbError mmb_strat_finalize_interface_va__basic(mmbMemInterface *interface,
                                                void *data,
                                                va_list args)
{
  (void) data;
  (void) args;
  const mmbProvider provider = interface->provider;
  const mmbExecutionContext ex_con = interface->space->ex_context;
  return mmb_providers_table[provider].ops[ex_con].finalize(interface);
}

mmbError mmb_strat_allocate_va__basic(const size_t n_bytes,
                                      mmbMemInterface *interface,
                                      const mmbAllocateOptions *opts,
                                      void *data,
                                      void **allocation,
                                      va_list args)
{
  (void) data;
  (void) args;
  mmbError stat = MMB_OK;
  const mmbProvider provider = interface->provider;
  const mmbExecutionContext ex_con = interface->space->ex_context;

  /* Secure the access to the max_bytes field in the space */
  mmb_memspace_rdlock_max_bytes(interface->space);

  /* Check we arent over the limit */
  mmb_memspace_lock_size(interface->space);
  {
    const size_t total_bytes = interface->space->allocated_bytes + n_bytes;
    if (total_bytes > interface->space->max_bytes) {
      MMB_WARN("Allocation failed - memory space is full\n");
      stat = MMB_INVALID_SIZE;
      mmb_memspace_unlock_size(interface->space);
      goto BAILOUT;
    }
    /* Pre-reserve the allocated bytes.  In case of error when allocating, we
     * will adjust the value after the call to allocate. */
    interface->space->allocated_bytes += n_bytes;
  }
  mmb_memspace_unlock_size(interface->space);

  if (opts && opts->is_numa_allocation) {
    /* Check the limit for the requested NUMA node */
    mmb_memspace_lock_numa_sizes(interface->space);
    if (n_bytes < interface->space->numa_sizes[opts->numa.node_id]) {
      interface->space->numa_sizes[opts->numa.node_id] -= n_bytes;
    } else {
      stat = MMB_OUT_OF_MEMORY;
      MMB_ERR("Unable to allocate %zu bytes in NUMA node %d.\n",
              n_bytes, opts->numa.node_id);
    }
    mmb_memspace_unlock_numa_sizes(interface->space);
  }

  if (MMB_OK == stat) {
    stat = mmb_providers_table[provider].ops[ex_con]
              .allocate(n_bytes, interface, opts, allocation);
  }

  if (MMB_OK != stat) {
    /* Adjust value on error */
    mmb_memspace_lock_size(interface->space);
    interface->space->allocated_bytes -= n_bytes;
    mmb_memspace_unlock_size(interface->space);
  } else if ((NULL == opts || !opts->is_numa_allocation) && interface->space->is_numa_aware) {
    /* Update the size in the NUMA node record */
    /* Find the appropriate node id */
    int numa_node_id;
    /* Page needs to be actually mapped (hence accessed) before testing the node.
     * Otherwise, the status returned will be -14 (-EFAULT). */
    memset(*allocation, 0, 1);
    stat = mmb_numa_get_node_id(allocation, 1, &numa_node_id);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to retrieve allocation's NUMA node id.\n");
    } else {
      /* Update value */
      MMB_NOISE("Updating NUMA node %d\n", numa_node_id);
      mmb_memspace_lock_numa_sizes(interface->space);
      interface->space->numa_sizes[numa_node_id] -= n_bytes;
      mmb_memspace_unlock_numa_sizes(interface->space);
    }
  }

BAILOUT:
  mmb_memspace_unlock_max_bytes(interface->space);

  return stat;
}

mmbError mmb_strat_free_va__basic(void *alloc,
                                  void *data,
                                  mmbMemInterface *interface,
                                  const mmbAllocateOptions *opts,
                                  const size_t n_bytes,
                                  va_list args)
{
  (void) data;
  (void) args;
  mmbError stat = MMB_OK;
  const mmbProvider provider = interface->provider;
  const mmbExecutionContext ex_con = interface->space->ex_context;
  stat = mmb_providers_table[provider].ops[ex_con]
            .free(alloc, interface, opts, n_bytes);
  if (MMB_OK == stat) {
    mmb_memspace_lock_size(interface->space);
    interface->space->allocated_bytes -= n_bytes;
    mmb_memspace_unlock_size(interface->space);
    /* Update NUMA sizes */
    if (opts->is_numa_allocation) {
      mmb_memspace_lock_numa_sizes(interface->space);
      interface->space->numa_sizes[opts->numa.node_id] += n_bytes;
      mmb_memspace_unlock_numa_sizes(interface->space);
    }
  }
  return stat;
}

mmbError mmb_strat_copy_nd_va__basic(void *dst, void *dst_data,
                                     mmbMemInterface *dst_interface,
                                     const mmbAllocateOptions *dst_opts,
                                     const size_t *doffset, const size_t *dpitch,
                                     const void * src,
                                     const mmbMemInterface *src_interface,
                                     const mmbAllocateOptions *src_opts,
                                     const size_t *soffset, const size_t *spitch,
                                     const size_t ndims, const size_t *dims,
                                     va_list args)
{
  (void) dst_data;
  (void) args;
  mmbError stat;
  const mmbProvider dst_provider = dst_interface->provider;
  const mmbProvider src_provider = src_interface->provider;
  const mmbExecutionContext dst_ex_con = dst_interface->space->ex_context;
  const mmbExecutionContext src_ex_con = src_interface->space->ex_context;
  stat = mmb_providers_table[dst_provider].ops[dst_ex_con]
            .copy_nd(dst, dst_interface, dst_opts, doffset, dpitch,
                     src, src_interface, src_opts, soffset, spitch,
                     ndims, dims);
  if (MMB_INVALID_PROVIDER == stat || MMB_INVALID_LAYER == stat
      || MMB_INVALID_EXECUTION_CONTEXT == stat || MMB_UNSUPPORTED == stat) {
    stat = mmb_providers_table[src_provider].ops[src_ex_con]
              .copy_nd(dst, dst_interface, dst_opts, doffset, dpitch,
                       src, src_interface, src_opts, soffset, spitch,
                       ndims, dims);
  }
  return stat;
}

/**  @} */

/**  @} */

/**  @} */

/**  @} */
