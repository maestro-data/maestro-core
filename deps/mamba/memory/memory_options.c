/*
 * Copyright (C) 2020      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"
#include "memory_options.h"
#include "mmb_memory_options.h"

mmbError mmb_memory_options_create_default(mmbMemoryOptions **mem_opts)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == mem_opts) {
    MMB_ERR("Invalid out parameter \"mem_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mmbMemoryOptions *opts = malloc(sizeof *opts);
  if (NULL == opts) {
    MMB_ERR("Failed to allocate memory for default memory options.\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *opts = (const mmbMemoryOptions) {
    .providers_opts = MMB_PROVIDER_DEFAULT_INIT_PARAMS,
    .strategies_opts = MMB_STRATEGY_DEFAULT_INIT_PARAMS,
  };
  *mem_opts = opts;
BAILOUT:
  return stat;
}

mmbError mmb_memory_options_destroy(mmbMemoryOptions *mem_opts)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == mem_opts) {
    MMB_WARN("Already destroyed handle or invalid pointer (%p).\n", mem_opts);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  free(mem_opts);
BAILOUT:
  return stat;
}

mmbError
mmb_memory_options_strategy_pooled_set_pool_size(mmbMemoryOptions *mem_opts,
                                                 size_t new_size)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == mem_opts) {
    MMB_ERR("Invalid memory options handle. Handle cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mem_opts->strategies_opts[MMB_POOLED].pooled.pool_size = new_size;
BAILOUT:
  return stat;
}


mmbError
mmb_meminterface_config_create_default(mmbMemInterfaceConfig **interface_opts)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == interface_opts) {
    MMB_ERR("Invalid out parameter \"interface_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mmbMemInterfaceConfig *opts = malloc(sizeof *opts);
  if (NULL == opts) {
    MMB_ERR("Failed to allcoate memory for default memory interface options.\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *opts = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  *interface_opts = opts;
BAILOUT:
  return stat;
}

mmbError
mmb_meminterface_config_destroy(mmbMemInterfaceConfig *interface_opts)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == interface_opts) {
    MMB_WARN("Already destroyed handle or invalid pointer (%p).\n", interface_opts);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  free(interface_opts);
BAILOUT:
  return stat;
}

mmbError
mmb_meminterface_config_set_default(mmbMemInterfaceConfig *interface_opts)
{
  mmbError stat = MMB_OK;
  const mmbMemInterfaceConfig conf = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  stat = mmb_meminterface_config_set_provider(interface_opts, conf.provider);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  stat = mmb_meminterface_config_set_strategy(interface_opts, conf.strategy);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  stat = mmb_meminterface_config_set_name(interface_opts, conf.name);
BAILOUT:
  return stat;
}

mmbError
mmb_meminterface_config_set_provider(mmbMemInterfaceConfig *interface_opts,
                                     mmbProvider provider)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == interface_opts) {
    MMB_ERR("Invalid out parameter \"interface_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (MMB_PROVIDER_ANY > provider || MMB_PROVIDER__MAX <= provider) {
    MMB_ERR("Invalid provider requested (%d).\n", provider);
    stat = MMB_INVALID_PROVIDER;
    goto BAILOUT;
  }
  interface_opts->provider = provider;
BAILOUT:
  return stat;
}

mmbError
mmb_meminterface_config_set_strategy(mmbMemInterfaceConfig *interface_opts,
                                     mmbStrategy strategy)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == interface_opts) {
    MMB_ERR("Invalid out parameter \"interface_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (MMB_STRATEGY_ANY > strategy || MMB_STRATEGY__MAX <= strategy) {
    MMB_ERR("Invalid strategy requested (%d).\n", strategy);
    stat = MMB_INVALID_STRATEGY;
    goto BAILOUT;
  }
  interface_opts->strategy = strategy;
BAILOUT:
  return stat;
}

mmbError
mmb_meminterface_config_set_name(mmbMemInterfaceConfig *interface_opts,
                                 const char *name)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == interface_opts) {
    MMB_ERR("Invalid out parameter \"interface_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == name)
    name = "";
  interface_opts->name = name;
BAILOUT:
  return stat;
}


static inline mmbError
mmb_memspace_config_create(const mmbMemSpaceConfig *conf,
                           mmbMemSpaceConfig **space_opts)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == space_opts) {
    MMB_ERR("Invalid out parameter \"space_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == conf) { // Unlikely
    MMB_ERR("Invalid configuration given; cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mmbMemSpaceConfig *opts = malloc(sizeof *opts);
  if (NULL == opts) {
    MMB_ERR("Failed to allcoate memory for default memory space options.\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *opts = *conf;
  *space_opts = opts;
BAILOUT:
  return stat;
}

mmbError mmb_memspace_config_create_init_default(mmbMemSpaceConfig **space_opts)
{
  return mmb_memspace_config_create(&MMB_MEMSPACE_CONFIG_INIT_DEFAULT, space_opts);
}

mmbError mmb_memspace_config_create_default(mmbMemSpaceConfig **space_opts)
{
  return mmb_memspace_config_create(&MMB_MEMSPACE_CONFIG_DEFAULT, space_opts);
}

mmbError mmb_memspace_config_destroy(mmbMemSpaceConfig *space_opts)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == space_opts) {
    MMB_WARN("Already destroyed handle or invalid pointer (%p).\n", space_opts);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  free(space_opts);
BAILOUT:
  return stat;
}

mmbError mmb_memspace_config_size(mmbMemSpaceConfig *space_opts,
                                  mmbSizeConfigAction action, size_t size)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == space_opts) {
    MMB_ERR("Invalid out parameter \"space_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (MMB_SIZE_NONE > action || MMB_SIZE_INVALID <= action) {
    MMB_ERR("Invalid action requested (%d).\n", action);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (MMB_SIZE_ANY == action)
    size = 0;
  else if (MMB_SIZE_NONE == action)
    size = space_opts->size_opts.mem_size;
  space_opts->size_opts =
    (const mmbSizeConfig) { .action = action, .mem_size = size };
BAILOUT:
  return stat;
}

mmbError mmb_memspace_config_set_size(mmbMemSpaceConfig *space_opts, size_t size)
{
  return mmb_memspace_config_size(space_opts, MMB_SIZE_SET, size);
}

mmbError mmb_memspace_config_any_size(mmbMemSpaceConfig *space_opts)
{
  return mmb_memspace_config_size(space_opts, MMB_SIZE_ANY, 0);
}

mmbError mmb_memspace_config_interface(mmbMemSpaceConfig *space_opts,
                                       mmbProvider provider,
                                       mmbStrategy strategy,
                                       const char *name)
{
  mmbError stat = MMB_OK;
  stat = mmb_memspace_config_interface_set_provider(space_opts, provider);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  stat = mmb_memspace_config_interface_set_strategy(space_opts, strategy);
  if (MMB_OK != stat) {
    goto BAILOUT;
  }
  stat = mmb_memspace_config_interface_set_name(space_opts, name);
BAILOUT:
  return stat;
}

mmbError mmb_memspace_config_interface_set_default(mmbMemSpaceConfig *space_opts)
{
  const mmbMemInterfaceConfig conf = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  return mmb_memspace_config_interface(space_opts, conf.provider, conf.strategy, conf.name);
}

mmbError mmb_memspace_config_interface_set_provider(mmbMemSpaceConfig *space_opts,
                                                    mmbProvider provider)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == space_opts) {
    MMB_ERR("Invalid out parameter \"space_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  stat = mmb_meminterface_config_set_provider(&space_opts->interface_opts,
                                              provider);
BAILOUT:
  return stat;
}

mmbError mmb_memspace_config_interface_set_strategy(mmbMemSpaceConfig *space_opts,
                                                    mmbStrategy strategy)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == space_opts) {
    MMB_ERR("Invalid out parameter \"space_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  stat = mmb_meminterface_config_set_strategy(&space_opts->interface_opts,
                                              strategy);
BAILOUT:
  return stat;
}

mmbError mmb_memspace_config_interface_set_name(mmbMemSpaceConfig *space_opts,
                                                const char *name)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == space_opts) {
    MMB_ERR("Invalid out parameter \"space_opts\" cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  stat = mmb_meminterface_config_set_name(&space_opts->interface_opts, name);
BAILOUT:
  return stat;
}
