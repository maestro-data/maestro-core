/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "i_memory.h"
#include "allocation.h"

#define N_ELT_FACTORY ((4<<10)/sizeof(mmbAllocation))

typedef struct mmbAllocationFactory {
  mmbAllocation ptrs[N_ELT_FACTORY][8 * sizeof(uint64_t)];
  uint64_t avail[N_ELT_FACTORY];
  struct mmbAllocationFactory *next;
} mmbAllocationFactory;

static mmbAllocationFactory *global_factory = NULL;
/* protecting global_factory */
static pthread_mutex_t global_factory_mtx = PTHREAD_MUTEX_INITIALIZER;
#define LOCK_GLOBAL_FACTORY do {                                  \
    MMB_DEBUG("Locking global factory\n");                        \
    int _gflstat = pthread_mutex_lock(&global_factory_mtx);       \
    assert(_gflstat==0);                                          \
    (void) _gflstat;                                              \
  } while(0)

#define UNLOCK_GLOBAL_FACTORY do {                                \
    MMB_DEBUG("Unlocking global factory\n");                      \
    int _gflstat = pthread_mutex_unlock(&global_factory_mtx);     \
    assert(_gflstat==0);                                          \
    (void) _gflstat;                                              \
  } while(0)

/* must be called under lock */
static mmbError mmb_alloc_factory_new(mmbAllocationFactory **out_factory)
{
  /* Check parameter */
  if (NULL == out_factory) {
    MMB_ERR("Out parameter \"out_factory\" cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  /* Allocate 0 initialized structure */
  mmbAllocationFactory *new_fact = calloc(1, sizeof *new_fact);
  if (NULL == new_fact) {
    MMB_ERR("Unable to allocate mmbAllocation factory.\n");
    return MMB_OUT_OF_MEMORY;
  }
  memset(new_fact->avail, ~0, sizeof new_fact->avail);
  *out_factory = new_fact;
  return MMB_OK;
}

/* must be called under lock */
static mmbError mmb_alloc_factory_delete(mmbAllocationFactory *in_factory)
{
  /* Check parameter */
  if (NULL == in_factory) {
    MMB_WARN("Invalid factory handle.\n");
    return MMB_INVALID_ARG;
  }
  /* Check if unfree'd mmbAllocation left */
  /* block is (8 * sizeof(unsigned int)), i.e., one bit-field. */
  for (size_t block = 0; N_ELT_FACTORY > block; ++block) {
    if (0 != (~in_factory->avail[block])) {
      MMB_WARN("block %zu: avail=%x\n", block, in_factory->avail[block]);
      for (size_t it = 0; 8 * sizeof *in_factory->avail > it; ++it) {
        if ((~in_factory->avail[block]) & (1<<it)) {
          MMB_WARN("Allocation pending of size %zu at address %p.\n",
               in_factory->ptrs[block][it].n_bytes,
               in_factory->ptrs[block][it].ptr);
        }
      }
    }
  }
  free(in_factory);
  return MMB_OK;
}

mmbError mmb_alloc_check_params(const mmbAllocation *alloc)
{
  if (NULL == alloc) {
    MMB_WARN("Invalid allocation handle.\n");
    return MMB_INVALID_ALLOCATION;
  }
  if (MMB_OK != mmb_meminterface_check_params(alloc->interface)) {
    MMB_WARN("Invalid interface handle in allocation handle.\n");
    return MMB_INVALID_ALLOCATION;
  }
  return MMB_OK;
}

mmbError mmb_alloc_initialize(void)
{
  mmbError stat = MMB_OK;
  LOCK_GLOBAL_FACTORY;
  if (NULL == global_factory) {
    stat = mmb_alloc_factory_new(&global_factory);
  }
  UNLOCK_GLOBAL_FACTORY;
  return stat;
}

mmbError mmb_alloc_finalize(void)
{
  mmbError stat;
  mmbAllocationFactory *fact = NULL;

  LOCK_GLOBAL_FACTORY;

  for (; NULL != global_factory; global_factory = fact) {
    fact = global_factory->next;
    stat = mmb_alloc_factory_delete(global_factory);
    if (MMB_OK != stat) {
      MMB_WARN("Error when deallocating the mmbAllocation factory.\n");
      goto BAILOUT_UNLOCK;
    }
  }
BAILOUT_UNLOCK:
  UNLOCK_GLOBAL_FACTORY;

  return stat;
}

#define DUP_MASK_64(m) ((uint64_t)(((m)<<32)|(m)))
#define DUP_MASK_32(m) ((uint64_t)(((m)<<16)|(m)))
#define DUP_MASK_16(m) ((uint64_t)(((m)<<8)|(m)))
#define DUP_MASK_8(m)  ((uint64_t)(((m)<<4)|(m)))
#define DUP_MASK_4(m)  ((uint64_t)(((m)<<2)|(m)))
#define DUP_MASK_2(m)  ((uint64_t)(((m)<<1)|(m)))

static const uint64_t bitmasks[] = {
  ~DUP_MASK_32(DUP_MASK_16(DUP_MASK_8(DUP_MASK_4(DUP_MASK_2(1))))),
  // 0b11111111111111...00000000
  ~DUP_MASK_64(DUP_MASK_16(DUP_MASK_8(DUP_MASK_4(DUP_MASK_2(1))))),
  // 0b11111111000000...00000000
  ~DUP_MASK_64(DUP_MASK_32(DUP_MASK_8(DUP_MASK_4(DUP_MASK_2(1))))),
  // 0b11110000111100...11110000
  ~DUP_MASK_64(DUP_MASK_32(DUP_MASK_16(DUP_MASK_4(DUP_MASK_2(1))))),
  // 0b11001100110011...11001100
  ~DUP_MASK_64(DUP_MASK_32(DUP_MASK_16(DUP_MASK_8(DUP_MASK_2(1))))),
  // 0b10101010101010...10101010
  ~DUP_MASK_64(DUP_MASK_32(DUP_MASK_16(DUP_MASK_8(DUP_MASK_4(1))))),
};

mmbError mmb_alloc_allocate(void *ptr, const size_t n_bytes,
                            mmbMemInterface *interface,
                            const mmbAllocateOptions *opts,
                            const bool owned, mmbAllocation **alloc)
{
  /* Check parameter */
  mmbError stat;

  if (NULL == alloc) {
    MMB_ERR("Out parameter \"alloc\" cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  LOCK_GLOBAL_FACTORY;

  if (NULL == global_factory) {
    MMB_ERR("Factory not initialized.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT_UNLOCK;
  }
  /* Look for an available slot */
  mmbAllocation *new_alloc = NULL;
  mmbAllocationFactory *factory = global_factory;
  for (; NULL != factory; factory = factory->next) {
    for (size_t block = 0; N_ELT_FACTORY > block; ++block) {
      if (0 != factory->avail[block]) {
        /* binary search within one block by applying masks (first left-most bit) */
        uint64_t avail = factory->avail[block];
        const size_t nbitmasks = sizeof bitmasks/sizeof *bitmasks;
        size_t it = 0, offset_it = 1 << (nbitmasks-1);
        for (const uint64_t *mask = bitmasks; &bitmasks[nbitmasks] > mask; ++mask) {
          if (avail & *mask) {
            avail &= *mask;
            it += offset_it;
          }
          offset_it >>= 1;
        }
        new_alloc = &factory->ptrs[block][it];
        factory->avail[block] -= ((uint64_t)1) << it;
        goto SET_AND_OUT;
      }
    }
  }
  /* if this point is reached no suitable available entry has been found.
   * Allocate a new set of mmbAllocation's. */
  stat = mmb_alloc_factory_new(&factory);
  if (MMB_OK != stat) {
    MMB_WARN("Unable to allocate new set of mmbAllocation. Allocation fails.\n");
    goto BAILOUT_UNLOCK;
  }
  factory->next = global_factory;
  global_factory = factory;
  /* Allocate at last block possible as we know it is necessarily available. */
  new_alloc = &factory->ptrs[N_ELT_FACTORY-1][0];
  factory->avail[N_ELT_FACTORY-1] -= 1;

SET_AND_OUT:
  if (NULL != opts)
    memcpy(&new_alloc->opts, opts, sizeof *opts);
  else
    memset(&new_alloc->opts, 0, sizeof *opts);
  new_alloc->ptr = ptr;
  new_alloc->n_bytes = n_bytes;
  new_alloc->interface = interface;
  new_alloc->owned = owned;
  *alloc = new_alloc;
  stat = MMB_OK;

BAILOUT_UNLOCK:
  UNLOCK_GLOBAL_FACTORY;

  return stat;
}

mmbError mmb_alloc_free(mmbAllocation *alloc)
{
  mmbError stat;
  /* Check parameter */
  stat = mmb_alloc_check_params(alloc);
  if (MMB_OK != stat) {
    MMB_WARN("Invalid \"alloc\" handle.\n");
    return MMB_INVALID_ALLOCATION;
  }
  LOCK_GLOBAL_FACTORY;

  if (NULL == global_factory) {
    MMB_ERR("Factory not initialized.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT_UNLOCK;
  }
  /* Find proper set that contains the allocated alloc. */
  for (mmbAllocationFactory *factory = global_factory;
       NULL != factory;
       factory = factory->next) {
    if (alloc >= *factory->ptrs && alloc < &factory->ptrs[N_ELT_FACTORY][0]) {
      const ptrdiff_t offset = alloc - &factory->ptrs[0][0];
      const size_t block = offset / (8 * sizeof *factory->avail);
      const size_t index = offset % (8 * sizeof *factory->avail);
      factory->avail[block] |= ((uint64_t)1) << index;
      stat = MMB_OK;
      goto BAILOUT_UNLOCK;
    }
  }
  /* Could not find the proper set : incoherent state that should have happen */
  MMB_ERR("Impossible to find the corresponding mmbAllocation record.\n");
  stat = MMB_ERROR;

BAILOUT_UNLOCK:
  UNLOCK_GLOBAL_FACTORY;
  return stat;
}

mmbError mmb_allocation_get_ptr(const mmbAllocation *in_alloc, void **out_ptr)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(in_alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  if (NULL == out_ptr) {
    MMB_ERR("Invalid \"out_ptr\" handle. Parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *out_ptr = in_alloc->ptr;
  return stat;
}

mmbError mmb_allocation_get_space(const mmbAllocation *in_alloc,
                                  mmbMemSpace **space)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(in_alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  return mmb_meminterface_get_space(in_alloc->interface, space);
}

mmbError mmb_allocation_get_layer(const mmbAllocation *alloc, mmbMemLayer *layer)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  return mmb_meminterface_get_layer(alloc->interface, layer);
}

mmbError mmb_allocation_get_execution_context(const mmbAllocation *alloc,
                                              mmbExecutionContext *ex_context)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  return mmb_meminterface_get_execution_context(alloc->interface, ex_context);
}

mmbError mmb_allocation_get_interface(const mmbAllocation *in_alloc,
                                      mmbMemInterface **interface)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(in_alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  if (NULL == interface) {
    MMB_ERR("Invalid \"interface\" handle. Parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *interface = in_alloc->interface;
  return stat;
}

mmbError mmb_allocation_get_provider(const mmbAllocation *alloc,
                                     mmbProvider *provider)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  return mmb_meminterface_get_provider(alloc->interface, provider);
}

mmbError mmb_allocation_get_strategy(const mmbAllocation *alloc,
                                     mmbStrategy *strat)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  return mmb_meminterface_get_strategy(alloc->interface, strat);
}

mmbError mmb_allocation_get_options(const mmbAllocation * restrict alloc,
                                    mmbAllocateOptions * restrict out_options)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  stat = mmb_alloc_check_params(alloc);
  if (MMB_OK != stat) {
    return stat;
  }
  if (NULL == out_options) {
    MMB_ERR("Invalid \"out_options\" handle. Parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  memcpy(out_options, &alloc->opts, sizeof *out_options);
  return stat;
}

mmbError mmb_allocate_options_create(mmbAllocateOptions **opts)
{
  /* Check parameters */
  if (NULL == opts) {
    MMB_ERR("Invalid pointer for return value. \"opts\" cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  mmbAllocateOptions *new_opts = calloc(1, sizeof *new_opts);
  if (NULL == new_opts) {
    MMB_ERR("Unable to allocate the mmbAllocateOptions object. Out of memory.\n");
    return MMB_OUT_OF_MEMORY;
  }
  *opts = new_opts;
  return MMB_OK;
}

mmbError mmb_allocate_options_destroy(mmbAllocateOptions *opts)
{
  /* Check parameters */
  if (NULL == opts) {
    MMB_ERR("Invalid reference or object already free'd.\n");
    return MMB_INVALID_ARG;
  }
  free(opts);
  return MMB_OK;
}
