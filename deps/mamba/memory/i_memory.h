/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef I_MMB_MEMORY_H
#define I_MMB_MEMORY_H

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>

#if ENABLE_DISCOVERY
#include <hwloc.h>
#else 
typedef void* hwloc_topology_t;
#endif /* ENABLE_DISCOVERY */

#include "../common/utlist.h"
#include "../common/i_mmb_hash.h"

#include "../common/mmb_error.h"
#include "mmb_memory.h"
#include "memory_config.h"
#include "mmb_memory_options.h"
#include "mmb_provider_options.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbAllocStats
 * @{ */

/**
 * @name Internal API
 * @{ */

struct mmbAllocStats {
  mmbAllocStatsHash *hash;
  pthread_mutex_t hash_mtx;
};
typedef struct mmbAllocStats mmbAllocStats;

/**  @} */

/**  @} */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Internal API
 * @{ */

struct mmbMemInterface {
  mmbProvider         provider;
  mmbProviderOptions  provider_opts;
  mmbStrategy         strategy;
  char         *name;
  struct mmbMemSpace *space;
  struct {
    struct mmbMemInterface *prev, *next;
  } link;
  unsigned char data[];
};

#define MMB_CDL_INSERT_INTERFACE(head, add) \
  CDL_PREPEND2(head, add, link.prev, link.next)

#define MMB_CDL_DELETE_INTERFACE(head, del) \
  CDL_DELETE2(head, del, link.prev, link.next)

#define MMB_CDL_FOREACH_INTERFACE(head, el) \
  CDL_FOREACH2(head, el, link.next)

#define MMB_CDL_FOREACH_INTERFACE_SAFE(head, el, tmp1, tmp2)    \
  CDL_FOREACH_SAFE2(head, el, tmp1, tmp2, link.prev, link.next)

/**
 * @brief Check if the parameters coherency in the structure.
 *
 * This function is used for parameter checking in other functions inside the
 * library.
 *
 * @param [in] interface An handle allocated with \p mmb_meminterface_create.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_meminterface_check_params(const mmbMemInterface *interface);

/**
 * @brief Set the parameters used to create a new interface.
 *
 * @param [in]  space       The space issuing the interface.
 * @param [in]  config_opts Set of requested options for the interface.
 * @param [out] provider    Handle for provider to use to create the new
 *                          interface.
 * @param [out] strategy    Handle for strategy to use to create the new
 *                          interface.
 * @param [out] name        Handle for the string to use as name to create the
 *                          new interface.
 * @return \c MMB_OK on success, an error type otherzise.
 */
mmbError
mmb_meminterface_set_parameters(const mmbMemSpace *space,
                                const struct mmbMemInterfaceConfig *config_opts,
                                mmbProvider *provider, mmbProviderOptions* provider_opts,
                                mmbStrategy *strategy, const char **name);
/**  @} */

/**  @} */

/**
 * @name mmbMemSpace
 * @{ */

/**
 * @name Internal API
 * @{ */

struct mmbMemSpace {
  mmbMemLayer   layer;
  mmbExecutionContext ex_context;
  pthread_mutex_t defaults_mtx;
  mmbProvider   default_provider;
  mmbStrategy   default_strategy;
  const char *  default_name;
  struct mmbAllocStats *stats;
  pthread_rwlock_t max_bytes_mtx;
  size_t        max_bytes;
  size_t        num_interfaces;
  pthread_mutex_t interfaces_mtx;
  struct mmbMemInterface *interfaces;
  mmbMemInterfaceHash *named_interfaces;
  pthread_mutex_t alloc_size_mtx;
  size_t        allocated_bytes;
  pthread_mutex_t numa_sizes_mtx;
  bool          is_numa_aware;
  size_t        numa_sizes[];
};

static inline void mmb_memspace_lock_size(mmbMemSpace *space)
{
  int stat = pthread_mutex_lock(&space->alloc_size_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_unlock_size(mmbMemSpace *space)
{
  int stat = pthread_mutex_unlock(&space->alloc_size_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_rdlock_max_bytes(mmbMemSpace *space)
{
  int stat = pthread_rwlock_rdlock(&space->max_bytes_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_wrlock_max_bytes(mmbMemSpace *space)
{
  int stat = pthread_rwlock_wrlock(&space->max_bytes_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_unlock_max_bytes(mmbMemSpace *space)
{
  int stat = pthread_rwlock_unlock(&space->max_bytes_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_lock_numa_sizes(mmbMemSpace *space)
{
  int stat = pthread_mutex_lock(&space->numa_sizes_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_memspace_unlock_numa_sizes(mmbMemSpace *space)
{
  int stat = pthread_mutex_unlock(&space->numa_sizes_mtx);
  assert(0 == stat);
  (void) stat;
}

mmbError mmb_memspace_resize(const mmbSizeConfig *size_opts, mmbMemSpace *space);

/**  @} */

/**  @} */

/**
 * @name mmbManager
 * @{ */

/**
 * @name Internal API
 * @{ */

typedef struct {
  mmbProvider provider;
  mmbStrategy strategy;
  const char *name;
  mmbExecutionContext execution_context[2];
} mmbDefaultContext;

struct mmbManager {
  size_t num_spaces;
  size_t max_spaces;
  struct mmbMemSpace **spaces;
  pthread_mutex_t spaces_mtx;
  mmbDefaultContext default_ctx;
  struct mmbAllocStats stats;
  hwloc_topology_t topology;
};

mmbError mmb_manager_get_topology(mmbManager *mngr, hwloc_topology_t **topo);

/**  @} */

/**  @} */

/**
 * @name Output for MAMBA symbols API
 * @{ */

#define MMB_CASE_TO_STR(a) case a: return #a
#define MMB_CASE_FROM_STR(var, name) MMB_CASE_FROM_ALT_STR(var, #name, name)
#define MMB_CASE_FROM_ALT_STR(var, str, name) if (0 == strcmp(str, var)) return name
inline static const char *
mmb_layer_get_string(const mmbMemLayer layer)
{
  switch (layer) {
    MMB_CASE_TO_STR(MMB_DRAM);
    MMB_CASE_TO_STR(MMB_GDRAM);
    MMB_CASE_TO_STR(MMB_HBM);
    MMB_CASE_TO_STR(MMB_NVDIMM);
    MMB_CASE_TO_STR(MMB_NADRAM);
    MMB_CASE_TO_STR(MMB_SSD);
    default: return "(invalid)";
  }
}
inline static mmbMemLayer
mmb_layer_get_symbol(const char * layer)
{
  if (NULL != layer) {
    MMB_CASE_FROM_STR(layer, MMB_DRAM);
    MMB_CASE_FROM_STR(layer, MMB_GDRAM);
    MMB_CASE_FROM_STR(layer, MMB_HBM);
    MMB_CASE_FROM_STR(layer, MMB_NVDIMM);
    MMB_CASE_FROM_STR(layer, MMB_NADRAM);
    MMB_CASE_FROM_STR(layer, MMB_SSD);
  }
  return MMB_MEMLAYER__MAX;
}
inline static const char *
mmb_execution_context_get_string(const mmbExecutionContext execution_context)
{
  switch (execution_context) {
    MMB_CASE_TO_STR(MMB_CPU);
    MMB_CASE_TO_STR(MMB_GPU_CUDA);
    MMB_CASE_TO_STR(MMB_GPU_HIP);
    MMB_CASE_TO_STR(MMB_OPENCL);
    MMB_CASE_TO_STR(MMB_EXECUTION_CONTEXT_NONE);
    default: return "(invalid)";
  }
}
inline static mmbExecutionContext
mmb_execution_context_get_symbol(const char * execution_context)
{
  if (NULL != execution_context) {
    MMB_CASE_FROM_STR(execution_context, MMB_CPU);
    MMB_CASE_FROM_STR(execution_context, MMB_GPU_CUDA);
    MMB_CASE_FROM_STR(execution_context, MMB_GPU_HIP);
    MMB_CASE_FROM_STR(execution_context, MMB_OPENCL);
    MMB_CASE_FROM_STR(execution_context, MMB_EXECUTION_CONTEXT_NONE);
    MMB_CASE_FROM_ALT_STR(execution_context, "NONE", MMB_EXECUTION_CONTEXT_NONE);
  }
  return MMB_EXECUTION_CONTEXT__MAX;
}
inline static const char *
mmb_provider_get_string(const mmbProvider provider)
{
  switch (provider) {
    MMB_CASE_TO_STR(MMB_SYSTEM);
    MMB_CASE_TO_STR(MMB_SICM);
    MMB_CASE_TO_STR(MMB_UMPIRE);
    MMB_CASE_TO_STR(MMB_JEMALLOC);
    default: return "(invalid)";
  }
}
inline static mmbProvider
mmb_provider_get_symbol(const char * provider)
{
  if (NULL != provider) {
    MMB_CASE_FROM_STR(provider, MMB_SYSTEM);
    MMB_CASE_FROM_STR(provider, MMB_SICM);
    MMB_CASE_FROM_STR(provider, MMB_UMPIRE);
    MMB_CASE_FROM_STR(provider, MMB_JEMALLOC);
  }
  return MMB_PROVIDER__MAX;
}
inline static const char *
mmb_strategy_get_string(const mmbStrategy strategy)
{
  switch (strategy) {
    MMB_CASE_TO_STR(MMB_STRATEGY_NONE);
    MMB_CASE_TO_STR(MMB_THREAD_SAFE);
    MMB_CASE_TO_STR(MMB_LIMITED_GREEDY);
    MMB_CASE_TO_STR(MMB_POOLED);
    MMB_CASE_TO_STR(MMB_STATISTICS);
    MMB_CASE_TO_STR(MMB_POOLED_STATISTICS);
    default: return "(invalid)";
  }
}
inline static mmbStrategy
mmb_strategy_get_symbol(const char * strategy)
{
  if (NULL != strategy) {
    MMB_CASE_FROM_STR(strategy, MMB_STRATEGY_NONE);
    MMB_CASE_FROM_STR(strategy, MMB_THREAD_SAFE);
    MMB_CASE_FROM_STR(strategy, MMB_LIMITED_GREEDY);
    MMB_CASE_FROM_STR(strategy, MMB_POOLED);
    MMB_CASE_FROM_STR(strategy, MMB_STATISTICS);
    MMB_CASE_FROM_STR(strategy, MMB_POOLED_STATISTICS);
  }
  return MMB_STRATEGY__MAX;
}
inline static const char *
mmb_size_config_action_get_string(const mmbSizeConfigAction size_config_action)
{
  switch (size_config_action) {
    MMB_CASE_TO_STR(MMB_SIZE_NONE);
    MMB_CASE_TO_STR(MMB_SIZE_ANY);
    MMB_CASE_TO_STR(MMB_SIZE_SET);
    default: return "(invalid)";
  }
}
inline static mmbSizeConfigAction
mmb_size_config_action_get_symbol(const char * size_config_action)
{
  if (NULL != size_config_action) {
    MMB_CASE_FROM_STR(size_config_action, MMB_SIZE_NONE);
    MMB_CASE_FROM_STR(size_config_action, MMB_SIZE_ANY);
    MMB_CASE_FROM_STR(size_config_action, MMB_SIZE_SET);
  }
  return MMB_SIZE_INVALID;
}
#undef MMB_CASE_TO_STR
#undef MMB_CASE_FROM_STR
#undef MMB_CASE_FROM_ALT_STR

/**  @} */

/**  @} */

#endif /* I_MMB_MEMORY_H */

