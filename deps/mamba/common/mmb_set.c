/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>

#include "mmb_error.h"
#include "mmb_set.h"

/** Generate the body of the functions for each type of mmbSet */
#define GENERATE_MMB_SET_TYPE_FCT(T)                                        \
  s_mmbSet_insert_fct(T)                                                    \
  s_mmbSet_set_fct(T)                                                       \
  s_mmbSet_create_fct(T)                                                    \
  s_mmbSet_resize_fct(T)                                                    \
  s_mmbSet_copy_fct(T)                                                      \
  s_mmbSet_copy_buffer_fct(T)                                               \
  s_mmbSet_fill_fct(T)                                                      \
  s_mmbSet_destroy_fct(T)                                                   \
  s_mmbSet_cmp_fct(T)                                                       \
  s_mmbSet_prod_fct(T)                                                      \
  s_mmbSet_set_buf_addr_fct(T)                                              \
  s_mmbSet_get_size_fct(T)                                                  \
  s_mmbSet_get_sizeof_fct(T)                                                \

#define s_mmbSet_create_fct(T)                                              \
  s_mmbSet_create(T)                                                        \
{                                                                           \
  mmbError stat = MMB_OK;                                                   \
  mmbSet_name(T) *set = calloc(1, sizeof *set);                             \
  if (NULL == set)                                                          \
    return MMB_OUT_OF_MEMORY;                                               \
  if(0 < nelt) {                                                            \
    set->d = calloc(nelt, sizeof *set->d);                                  \
    if (NULL == set->d) {                                                   \
      free(set);                                                            \
      return MMB_OUT_OF_MEMORY;                                             \
    }                                                                       \
  }                                                                         \
  set->size = nelt;                                                         \
  *out_set = set;                                                           \
  return stat;                                                              \
}

#define s_mmbSet_insert_fct(T)                                              \
  s_mmbSet_insert(T)                                                        \
{                                                                           \
  if (pos > in_set->size)                                                   \
    return MMB_INVALID_ARG;                                                 \
  in_set->d[pos] = *elt;                                                    \
  return MMB_OK;                                                            \
}

#define s_mmbSet_set_fct(T)                                                 \
  s_mmbSet_set(T)                                                           \
{                                                                           \
  in_set->d[0] = s0;                                                        \
  for(size_t i = 1; in_set->size > i; ++i)                                  \
    in_set->d[i] = va_arg(vargs, T);                                        \
  return MMB_OK;                                                            \
}

#define s_mmbSet_copy_fct(T)                                                \
  s_mmbSet_copy(T)                                                          \
{                                                                           \
  mmbError stat = MMB_OK;                                                   \
  if(NULL == src || NULL == dst)                                            \
    return MMB_INVALID_ARG;                                                 \
  stat = mmbSet_ ## T ## _resize(dst, src->size);                           \
  if(stat != MMB_OK)                                                        \
    return stat;                                                            \
  return mmbSet_ ## T ## _copy_buffer(dst->d, src);                         \
}

#define s_mmbSet_copy_buffer_fct(T)                                         \
  s_mmbSet_copy_buffer(T)                                                   \
{                                                                           \
  mmbError stat = MMB_OK;                                                   \
  if(NULL == dst || NULL == src || NULL == src->d)                          \
    return MMB_INVALID_ARG;                                                 \
  memcpy(dst, src->d, src->size * sizeof *src->d);                          \
  return stat;                                                              \
}

#define s_mmbSet_fill_fct(T)                                                \
  s_mmbSet_fill(T)                                                          \
{                                                                           \
  mmbSet_name(T) src_set = {nelt, (T *) src};                               \
  return mmbSet_ ## T ## _copy(dst, &src_set);                              \
}

#define s_mmbSet_resize_fct(T)                                              \
  s_mmbSet_resize(T)                                                        \
{                                                                           \
  if(NULL == in_set)                                                        \
    return MMB_INVALID_ARG;                                                 \
  if(nelt == in_set->size)                                                  \
    return MMB_OK;                                                          \
  if(nelt == 0) {                                                           \
    free(in_set->d);                                                        \
  } else {                                                                  \
    void *data;                                                             \
    if(in_set->size == 0) {                                                 \
      data = malloc(nelt * sizeof *in_set->d);                              \
    } else {                                                                \
      data = realloc(in_set->d, nelt * sizeof *in_set->d);                  \
    }                                                                       \
    if(NULL == data) {                                                      \
      return MMB_OUT_OF_MEMORY;                                             \
    }                                                                       \
    memset(data, 0, nelt * sizeof *in_set->d);                              \
    in_set->d = data;                                                       \
  }                                                                         \
  in_set->size = nelt;                                                      \
  return MMB_OK;                                                            \
}

#define s_mmbSet_destroy_fct(T)                                             \
  s_mmbSet_destroy(T)                                                       \
{                                                                           \
  mmbError stat = MMB_OK;                                                   \
  if (NULL == in_set)                                                       \
    return MMB_INVALID_ARG;                                                 \
  stat = mmbSet_ ## T ## _resize(in_set, 0);                                \
  if(stat != MMB_OK)                                                        \
    return stat;                                                            \
  free(in_set);                                                             \
  return stat;                                                              \
}

#define s_mmbSet_cmp_fct(T)                                                 \
  s_mmbSet_cmp(T)                                                           \
{                                                                           \
  if (s1->size == s2->size) {                                               \
    for (size_t cmp_iter = 0; s1->size > cmp_iter; cmp_iter += 1) {         \
      if (s1->d[cmp_iter] != s2->d[cmp_iter])                               \
        return s2->d[cmp_iter] - s1->d[cmp_iter];                           \
    }                                                                       \
  } else {                                                                  \
    return (int)(s2->size - s1->size);                                      \
  }                                                                         \
  return 0;                                                                 \
}

#define s_mmbSet_prod_fct(T)                                                \
  s_mmbSet_prod(T)                                                          \
{                                                                           \
  if (NULL == in_set)                                                       \
    return MMB_INVALID_ARG;                                                 \
  T total = 1;                                                              \
  for(size_t i = 0; i < in_set->size; i++) {                                \
    total *= in_set->d[i];                                                  \
  }                                                                         \
  *out_prod = total;                                                        \
  return MMB_OK;                                                            \
}

#define s_mmbSet_set_buf_addr_fct(T)                                        \
  s_mmbSet_set_buf_addr(T)                                                  \
{                                                                           \
  if (NULL == in_set)                                                       \
    return MMB_INVALID_ARG;                                                 \
  in_set->d = buffer;                                                       \
  return MMB_OK;                                                            \
}

#define s_mmbSet_get_size_fct(T)                                            \
  s_mmbSet_get_size(T)                                                      \
{                                                                           \
  if (NULL == in_set)                                                       \
    return MMB_INVALID_ARG;                                                 \
  *size = in_set->size;                                                     \
  return MMB_OK;                                                            \
}

#define s_mmbSet_get_sizeof_fct(T)                                          \
  s_mmbSet_get_sizeof(T)                                                    \
{                                                                           \
  if (NULL == in_set)                                                       \
    return MMB_INVALID_ARG;                                                 \
  *size = sizeof(mmbSet_name(T)) + in_set->size * sizeof(*in_set->d);       \
  return MMB_OK;                                                            \
}

GENERATE_MMB_SET_TYPE_FCT(size_t)

GENERATE_MMB_SET_TYPE_FCT(uint64_t)
