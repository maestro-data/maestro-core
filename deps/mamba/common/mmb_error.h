/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_ERROR_H
#define MMB_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif

enum e_mmbError {
  MMB_OK = 0,
  MMB_ERROR,
  MMB_OUT_OF_MEMORY,
  MMB_OUT_OF_BOUNDS,
  MMB_NOT_FOUND,
  MMB_INVALID_ARG,
  MMB_INVALID_LAYER,
  MMB_INVALID_PROVIDER,
  MMB_INVALID_INTERFACE,
  MMB_INVALID_EXECUTION_CONTEXT,
  MMB_INVALID_STRATEGY,
  MMB_INVALID_ALLOCATION,
  MMB_INVALID_SIZE,
  MMB_INVALID_SPACE,
  MMB_INVALID_ARRAY,
  MMB_UNIMPLEMENTED,
  MMB_UNSUPPORTED,
  MMB_ERROR__MAX
};

typedef enum e_mmbError mmbError;

const char* mmb_error_description(mmbError err);

#ifdef __cplusplus
}
#endif

#endif
