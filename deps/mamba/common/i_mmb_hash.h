#ifndef MMB_HASH_H
#define MMB_HASH_H

#include "khash.h"

/* Tests used from ../common/khash.h lines 134-144 */
#if UINT_MAX == 0xffffffffu
#define PRImmbHashInt32 "u"
#elif ULONG_MAX == 0xffffffffu
#define PRImmbHashInt32 "lu"
#endif

/* khint_t is a typedef of khint32_t ../common/khash.h line 162 */
#define PRImmbHashInt PRImmbHashInt32
/* khiter_t is a typedef of khint_t ../common/khash.h line 163 */
#define PRImmbHashIter PRImmbHashInt

#if ULONG_MAX == ULLONG_MAX
#define PRImmbHashInt64 "lu"
#else
#define PRImmbHashInt64 "llu"
#endif

typedef khiter_t mmb_hash_iter_t;
typedef khint_t mmb_hash_int_t;
typedef khint32_t mmb_hash_int32_t;
typedef khint64_t mmb_hash_int64_t;

#define mmb_hash_foreach(hashmap, id, value, body) kh_foreach(hashmap, id, value, body)
#define mmb_hash_end(hashmap) kh_end(hashmap)
#define mmb_hash_value(hashmap, iter) kh_value(hashmap, iter)
#define mmb_hash_size(hashmap) kh_size(hashmap)

/* mmbArrayTiles */

struct mmbArrayTile;
KHASH_MAP_INIT_INT64(ArrayTiles, struct mmbArrayTile *)
typedef struct mmbArrayTileHash {
  khash_t(ArrayTiles) h;
} mmbArrayTileHash;

#define mmb_hash_init_array_tiles() ((mmbArrayTileHash *)(kh_init(ArrayTiles)))
#define mmb_hash_destroy_array_tiles(hashmap) kh_destroy(ArrayTiles, &(hashmap)->h)
#define mmb_hash_get_array_tiles(hashmap, index) kh_get(ArrayTiles, &(hashmap)->h, index)
#define mmb_hash_put_array_tiles(hashmap, index, ret) kh_put(ArrayTiles, &(hashmap)->h, index, ret)
#define mmb_hash_del_array_tiles(hashmap, key) kh_del(ArrayTiles, &(hashmap)->h, key)
#define mmb_hash_foreach_array_tiles(hashmap, id, tmp_tile, body) mmb_hash_foreach(&(hashmap)->h, id, tmp_tile, body)
#define mmb_hash_end_array_tiles(hashmap) mmb_hash_end(&(hashmap)->h)
#define mmb_hash_value_array_tiles(hashmap, iter) mmb_hash_value(&(hashmap)->h, iter)
#define mmb_hash_size_array_tiles(hashmap) mmb_hash_size(&(hashmap)->h)

struct mmbMeminterface;
KHASH_MAP_INIT_STR(StrInterface, struct mmbMemInterface *);
typedef khash_t(StrInterface) mmbMemInterfaceHash;

#define mmb_hash_init_interface() kh_init(StrInterface)
#define mmb_hash_destroy_interface(hashmap) kh_destroy(StrInterface, hashmap)
#define mmb_hash_get_interface(hashmap, index) kh_get(StrInterface, hashmap, index)
#define mmb_hash_put_interface(hashmap, index, ret) kh_put(StrInterface, hashmap, index, ret)
#define mmb_hash_del_interface(hashmap, key) kh_del(StrInterface, hashmap, key)
#define mmb_hash_foreach_interface(hashmap, id, tmp_interface, body) mmb_hash_foreach(hashmap, id, tmp_interface, body)
#define mmb_hash_end_interface(hashmap) mmb_hash_end(hashmap)
#define mmb_hash_value_interface(hashmap, iter) mmb_hash_value(hashmap, iter)
#define mmb_hash_size_interface(hashmap) mmb_hash_size(hashmap)

/* mmbAllocStats */

struct mmb_strat_stats_data;
KHASH_MAP_INIT_INT64(Stats, struct mmb_strat_stats_data *)
typedef khash_t(Stats) mmbAllocStatsHash;

#define mmb_hash_init_alloc_stats() kh_init(Stats)
#define mmb_hash_destroy_alloc_stats(hashmap) kh_destroy(Stats, hashmap)
#define mmb_hash_get_alloc_stats(hashmap, index) kh_get(Stats, hashmap, (mmb_hash_int64_t)index)
#define mmb_hash_put_alloc_stats(hashmap, index, ret) kh_put(Stats, hashmap, (mmb_hash_int64_t)index, ret)
#define mmb_hash_del_alloc_stats(hashmap, key) kh_del(Stats, hashmap, key)
#define mmb_hash_foreach_alloc_stats(hashmap, id, tmp_data, body) mmb_hash_foreach(hashmap, id, tmp_data, body)
#define mmb_hash_end_alloc_stats(hashmap) mmb_hash_end(hashmap)
#define mmb_hash_value_alloc_stats(hashmap, iter) mmb_hash_value(hashmap, iter)
#define mmb_hash_size_alloc_stats(hashmap) mmb_hash_size(hashmap)

#endif
