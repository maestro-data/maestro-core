/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_error.h"
#include "mmb_logging.h"

static
const char * const mmb_error_table[MMB_ERROR__MAX] = {
  [MMB_OK]                        = "success",
  [MMB_ERROR]                     = "generic error",
  [MMB_OUT_OF_MEMORY]             = "out of memory",
  [MMB_NOT_FOUND]                 = "requested item not found",
  [MMB_INVALID_ARG]               = "invalid argument",
  [MMB_INVALID_LAYER]             = "operation impossible for the given layer",
  [MMB_INVALID_PROVIDER]          = "provider unavailable",
  [MMB_INVALID_INTERFACE]         = "no valid interface available",
  [MMB_INVALID_EXECUTION_CONTEXT] = "execution context unavailable",
  [MMB_INVALID_STRATEGY]          = "no valid strategy available",
  [MMB_INVALID_ALLOCATION]        = "allocation handle invalid or in inconsistent state",
  [MMB_INVALID_SIZE]              = "invalid size request",
  [MMB_UNIMPLEMENTED]             = "unimplemented",
};

const char *
mmb_error_description(mmbError err)
{
  if(err>=MMB_ERROR__MAX) {
    MMB_ERR("mmbError: %d not listed in error table\n", (int)err );
    return NULL;
  }
  return mmb_error_table[err];
}
