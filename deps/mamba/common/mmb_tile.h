/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_TILE_H
#define MMB_TILE_H


#include <pthread.h>
#include "mmb_layout.h"
#include "mmb_dimensions.h"
#include "mmb_index.h"
#include "mmb_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

enum e_mmbTileCacheUsePolicy {
  MMB_TILECACHE_READ_WRITE = 0,
  MMB_TILECACHE_READ = 1,
  MMB_TILECACHE_UNUSED = 2,
  MMB_TILECACHE_MAX
};

typedef enum e_mmbTileCacheUsePolicy mmbTileCacheUsePolicy;
typedef struct mmbArrayTile {
  mmbAllocation *allocation;
  int allocation_owned;
  mmbMemInterface *interface;
  mmbAccessType access;
  mmbLayout *layout;
  /* Lower and upper indices in tile allocation*/
  size_t *lower;
  size_t *upper;
  size_t *dim;
  /* Lower and upper indices in array */
  size_t *alower;
  size_t *aupper;
  /* Dimensions of allocation containing tile */
  size_t *abs_dim;
  /* For hashing */
  size_t tidx;
  /* Doubly linked list of tile versions */
  struct mmbArrayTile* prev;
  struct mmbArrayTile* next;
  /** Device version of this tile structure for device side operations. */
  mmbAllocation *space_local_metadata;
  /** Protect threaded access to tile management routines */
  pthread_mutex_t lock;
  mmbTileCacheUsePolicy cache_policy;
} mmbArrayTile;

struct mmbArrayTileHash;
typedef struct s_mmbArray mmbArray;
typedef struct s_mmbTiling {
  mmbDimensions tile_size;
  mmbDimensions tile_count;
  size_t total;
  struct mmbArrayTileHash *tiles;
} mmbTiling;

enum e_mmbMergeType {
  MMB_DISCARD = 0,
  MMB_OVERWRITE = 1,
  MMB_MERGETYPE_MAX
};

typedef struct s_mmbTileOptions{
  mmbTileCacheUsePolicy cache_policy;  
} mmbTileOptions;

typedef enum e_mmbMergeType mmbMergeType;


mmbError mmb_tile_create(mmbArray *in_mba, mmbIndex* in_idx, 
                         mmbTileOptions *in_opts, mmbArrayTile **out_tile);
mmbError mmb_tile_create_1d(mmbArray *in_mba, size_t in_idx, 
                            mmbTileOptions *in_opts, mmbArrayTile **out_tile);
mmbError mmb_tile_create_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                            mmbTileOptions *in_opts, mmbArrayTile **out_tile);
mmbError mmb_tile_create_3d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                            size_t in_idx2, mmbTileOptions *in_opts, 
                            mmbArrayTile **out_tile);

mmbError mmb_tile_update(mmbArray *in_mba, mmbIndex *in_idx, 
                         mmbArrayTile **inout_tile);
mmbError mmb_tile_update_1d(mmbArray *in_mba, size_t in_idx, 
                         mmbArrayTile **inout_tile);
mmbError mmb_tile_update_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1, 
                         mmbArrayTile **inout_tile);
mmbError mmb_tile_update_3d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                         size_t in_idx3, mmbArrayTile **inout_tile);

/**
 * @brief      Allocate data for tile
 *
 * @param      in_tile  Initialised tile to allocate data for
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_alloc_data(mmbArrayTile *in_tile);

/**
 * @brief      Copy data from src tile to dst tile, performing 
 *             layout transformations where necessary
 *
 * @param      dst_tile  The destination tile
 * @param      src_tile  The source tile
 *
 * @return     { description_of_the_return_value }
 */
mmbError mmb_tile_copy(mmbArrayTile *dst_tile, mmbArrayTile *src_tile);


/**
 * @brief      Duplicate a tile to another memory tier, layout, or access type
 *             returning a new tile 
 *
 * @param[in]  in_tile      The input tile to be duplicated.
 * @param[in]  in_interface The input interface to use to duplicate.
 * @param[in]  in_access    The input access type to duplicate to.
 * @param[in]  in_layout    The input layout to duplicate to.
 * @param[out] out_tile     The output tile.
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_duplicate(mmbArrayTile *in_tile, mmbMemInterface *in_interface,
                            mmbAccessType in_access, mmbLayout *in_layout,  
                            mmbArrayTile **out_tile);

/**
 * @brief      Return a valid pointer to a tile structure that can be used on a
 *             device that does not shared the same addressing space as the
 *             rest of the program (e.g., inside a GPU kernel).
 *
 *             The returned handle is not to be free'd by the user and still is
 *             owned by the library.
 *
 * @param[in]  in_tile    The input tile returned by \c mmb_tile_duplicate.
 * @param[out] out_tile   The output tile. Cannot be \c NULL.
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_get_space_local_handle(mmbArrayTile *in_tile,
                                         mmbArrayTile **out_tile);

/**
 * @brief      Retunrs the dimensionality of a tile
 *
 * @param[in]  in_tile    The input tile for enquiry
 * @param[out] n_dims     The number of dimensions in the tile
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_get_n_dims(mmbArrayTile *in_tile,
                             size_t *n_dims);

/**
 * @brief      Move a tile to another memory tier, layout, or access type
 *             Modifies the input tile
 *
 * @param[in]  in_tile      The input tile to be migrated.
 * @param[in]  in_interface The input interface to the memory to migrate to.
 * @param[in]  in_access    The input access type to migrate to.
 * @param[in]  in_layout    The input layout to migrate to.
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_migrate(mmbArrayTile *in_tile, mmbMemInterface *in_interface,
                          mmbAccessType in_access, mmbLayout *in_layout);


/**
 * @brief      Merge a duplicated tile with its parent 
 *
 * @param      in_tile   The input tile to be merged
 * @param[in]  in_merge  The type of merge 
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_merge(mmbArrayTile *in_tile, mmbMergeType in_merge);


/**
 * @brief      Free a tile
 *
 * @param      in_tile   In tile
 *
 * @return     MMB_OK on success
 */
mmbError mmb_tile_destroy(mmbArrayTile *in_tile);

mmbError mmb_tile_lower_byte_offset(mmbArrayTile *tile, size_t *n_bytes);

#ifdef __cplusplus
}
#endif

#endif
