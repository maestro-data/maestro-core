/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_error.h"
#include "mmb_index.h"
#include <stdarg.h>

const mmbIndex mmb_index_empty = s_mmbSet_empty(size_t);

mmbError mmb_index_create(const size_t ndim, mmbIndex **out_idx) {
  return mmbSet_size_t_create(ndim, out_idx);
}

mmbError mmb_index_create_fill(const size_t ndim, const size_t* values, mmbIndex **out_idx) {
  mmbError stat = mmbSet_size_t_create(ndim, out_idx);
  if(stat != MMB_OK)
    return stat;
  return mmbSet_size_t_fill(*out_idx, values, ndim);
}

mmbError mmb_index_set(mmbIndex *in_idx, size_t idN, ...) {
    va_list args;
    va_start(args, idN);
    mmbError stat = mmbSet_size_t_set(in_idx, idN, args);
    va_end(args);
    return stat;
}

mmbError mmb_index_copy(mmbIndex *dst, const mmbIndex *src) {
  return mmbSet_size_t_copy(dst, src);
}

mmbError mmb_index_copy_buffer(size_t *dst, const mmbIndex *src) {
  return mmbSet_size_t_copy_buffer(dst, src);
}

mmbError mmb_index_fill(mmbIndex *dst, const size_t *src, const size_t nelt) {
  return mmbSet_size_t_fill(dst, src, nelt);
}

mmbError mmb_index_resize(mmbIndex *in_idx, const size_t ndim) {
  return mmbSet_size_t_resize(in_idx, ndim);
}

mmbError mmb_index_destroy(mmbIndex *in_idx) {
  return mmbSet_size_t_destroy(in_idx);
}

int mmb_index_cmp(const mmbIndex *i1, const mmbIndex *i2) {
  return mmbSet_size_t_cmp(i1, i2);
}

mmbError mmb_index_prod(const mmbIndex *in_idx, size_t *out_prod) {
  return mmbSet_size_t_prod(in_idx, out_prod);
}

mmbError mmb_index_set_buf_addr(mmbIndex *in_idx, size_t *buffer) {
  return mmbSet_size_t_set_buf_addr(in_idx, buffer);
}

mmbError mmb_index_get_size(const mmbIndex *in_idx, size_t *size) {
  return mmbSet_size_t_get_size(in_idx, size);
}

mmbError mmb_index_get_sizeof(const mmbIndex *in_idx, size_t *size) {
  return mmbSet_size_t_get_sizeof(in_idx, size);
}
