/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include "mamba.h"

#include "mmb_tile.h"
#include "mmb_tile_iterator.h"
#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"
#include "i_mmb_hash.h"

/* protecting array state */
static inline void mmb_array_lock(mmbArray *mba) {
  int stat = pthread_mutex_lock(&mba->lock);
  assert(stat==0);
  (void) stat;
}
static inline void mmb_array_unlock(mmbArray *mba) {
  int stat = pthread_mutex_unlock(&mba->lock);
  assert(0 == stat);
  (void) stat;
}

/* prototypes for private lockfree versions of array functions */
mmbError mmb_array_tile_lockfree(mmbArray *in_mba, mmbDimensions *in_dims);
mmbError mmb_array_untile_lockfree(mmbArray *in_mba);

/**
 * @brief Unique function to allocate to create a new mmb_array
 *
 * This function if here to unify the process between \p mmb_array_create and
 * \p mmb_array_create_wrapped.  It stays static to avoid errors from user
 * input.  If \p in_data is \c NULL, then the function allocates the data using
 * the memory broker, and \p data_owned flag is set.
 *
 * @param[in]  in_data      Data from user.  May be \c NULL.
 * @param[in]  in_dims      Dimensions of data.
 * @param[in]  in_layout    Layout of user data.
 * @param[in]  in_interface Memory interface to be used to allocate data.
 * @param[in]  in_access    Data access rights
 * @param[out] out_mba      The output mamba array.
 *
 * @return     \c MMB_OK on success.
 */
static inline mmbError mmb_array_new(void *in_data,
                                     mmbDimensions *in_dims,
                                     mmbLayout *in_layout,
                                     mmbMemInterface *in_interface,
                                     mmbAccessType in_access,
                                     mmbArray **out_mba)
{
  mmbError stat = MMB_OK;

  if(!in_dims || !in_layout || !out_mba) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == in_interface) {
    MMB_ERR("Invalid memory interface handle (in_interface cannot be NULL).\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmbArray *mba = calloc(1, sizeof *mba);
  if (!mba) {
    MMB_ERR("Failed to allocate memory for mamba array object\n");
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  stat = mmb_layout_create_copy(in_layout, &mba->layout);
  if (stat != MMB_OK) {
    MMB_ERR("Failed to copy-construct mmbLayout\n");
    goto BAILOUT;
  }

  mba->interface = in_interface;
  mba->access = in_access;

  size_t n_bytes;
  stat = mmb_layout_buffer_size(in_layout, in_dims, &n_bytes);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to compute layout dimensions.\n");
    goto BAILOUT;
  }

  mmbAllocation *allocation;
  if (in_data) {
    stat = mmb_allocation_create_wrapped(in_data, n_bytes, in_interface, &allocation);
    if (MMB_OK != stat) {
      MMB_ERR("Failed to create the appropriate wrapped allocation.\n");
      goto BAILOUT;
    }
  } else {
    stat = mmb_allocate(n_bytes, in_interface, &allocation);
    if (MMB_OK != stat) {
      MMB_ERR("Failed to allocate memory for mmbArray.\n");
      goto BAILOUT;
    }
  }
  mba->allocation = allocation;

  mba->data_owned = (NULL == in_data);

  stat = mmb_dimensions_copy(&mba->dims, in_dims);
  if (stat != MMB_OK) {
    MMB_ERR("Failed to copy mmb_dimensions\n");
    goto BAILOUT;
  }

  int rc = pthread_mutex_init(&mba->lock, NULL);
  if(rc != 0) {
    MMB_ERR("Failed to initialise array mutex\n");
    goto BAILOUT;
  }

  rc = pthread_rwlock_init(&mba->tiling_lock, NULL);
  if(rc != 0) {
    MMB_ERR("Failed to initialise array tiling rwlock\n");
    goto BAILOUT;
  }

  *out_mba = mba;
BAILOUT:
  return stat;
}

mmbError mmb_array_create(mmbDimensions *in_dims,
                          mmbLayout *in_layout,
                          mmbMemInterface *in_interface,
                          mmbAccessType in_access,
                          mmbArray **out_mba)
{
  return mmb_array_new(NULL, in_dims, in_layout, in_interface, in_access, out_mba);
}

mmbError mmb_array_create_wrapped(void *in_data,
                                  mmbDimensions *in_dims,
                                  mmbLayout *in_layout,
                                  mmbMemInterface *in_interface,
                                  mmbAccessType in_access,
                                  mmbArray **out_mba)
{
  if (!in_data) {
    MMB_ERR("Creating a wrapped mamba array requires a valid data ptr.\n");
    return MMB_INVALID_ARG;
  }
  return mmb_array_new(in_data, in_dims, in_layout, in_interface, in_access, out_mba);
}

mmbError mmb_array_destroy(mmbArray *in_mba) {
  mmbError stat = MMB_OK;
  if (!in_mba)
    return stat;

  /* Free array data */
  stat = mmb_allocation_destroy(in_mba->allocation);
  CHECK_STATUS(stat, "Failed to destroy mmbArray data allocation\n", BAILOUT);

  /* Free remaining array components */
  stat = mmb_layout_destroy(in_mba->layout);
  CHECK_STATUS(stat, "Failed to destroy mmbLayout\n", BAILOUT);

  stat = mmb_dimensions_resize(&in_mba->dims, 0);
  CHECK_STATUS(stat, "Failed to free mmbArray dimensions\n", BAILOUT);

  if(in_mba->tiling) {
    stat = mmb_array_untile(in_mba);
    CHECK_STATUS(stat, "Failed to untile mmbArray\n", BAILOUT);
  }

  int rc = pthread_mutex_destroy(&in_mba->lock);
  if(rc != 0) {
    MMB_ERR("Failed to destroy array mutex\n");
    goto BAILOUT;
  }

  rc = pthread_rwlock_destroy(&in_mba->tiling_lock);
  if(rc != 0) {
    MMB_ERR("Failed to destroy array tiling rwlock\n");
    goto BAILOUT;
  }

  free(in_mba);
BAILOUT:
  return stat;
}

mmbError mmb_array_copy(mmbArray *dst, mmbArray *src) {
  mmbError stat = MMB_OK;

  /* Validate */
  if (!dst || !src){
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }
  if(dst==src) {
    MMB_ERR("src==dst, cannot copy array to itself.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmb_array_lock(src);
  mmb_array_lock(dst);
  /* If src is empty, no copy necessary */
  size_t total_elements = 0;
  stat = mmb_dimensions_prod(&src->dims, &total_elements);
  CHECK_STATUS(stat, "Unable to calculate array size\n", BAILOUT_UNLOCK);

  if(total_elements == 0) {
    stat = MMB_OK;
    goto BAILOUT_UNLOCK;
  }

  /* Dst must be writeable */
  if(!(dst->access & MMB_WRITE)){
    MMB_ERR("Destination array must be writeable\n");
    stat = MMB_ERROR;
    goto BAILOUT_UNLOCK;
  }

  /* Dimensions and element size must match */
  int dims_cmp = mmb_dimensions_cmp(&dst->dims, &src->dims);
  if(dims_cmp) {
    MMB_ERR("src and dst arrays are not the same size\n");
    stat = MMB_ERROR;
    goto BAILOUT_UNLOCK;
  }

  int element_size_same = (dst->layout->element.size_bytes ==
                            src->layout->element.size_bytes) ? 1 : 0;

  if(!element_size_same) {
    MMB_ERR("SRC and DST arrays must have matching element size\n");
    stat = MMB_ERROR;
    goto BAILOUT_UNLOCK;
  }

  /* We will clear tiling if it exists */
  if(dst->tiling || src->tiling) {
    MMB_WARN("Either or both args are pre-tiled arrays, tiling will be removed during copy.\n");
  }

  /* Get a single tile for each array */
  stat = mmb_array_tile_lockfree(dst, &dst->dims);
  CHECK_STATUS(stat, "Failed to tile dst mmbArray for copy\n", BAILOUT_UNLOCK);

  stat = mmb_array_tile_lockfree(src, &src->dims);
  CHECK_STATUS(stat, "Failed to tile src mmbArray for copy\n", BAILOUT_UNLOCK);

  mmbIndex *zero_idx;
  stat = mmb_index_create(dst->layout->n_dims, &zero_idx);
  CHECK_STATUS(stat, "Failed to create mmbIndex object\n", BAILOUT_UNLOCK);

  mmbArrayTile *tile_dst, *tile_src;
  stat = mmb_tile_at(dst, zero_idx, &tile_dst);
  CHECK_STATUS(stat, "Failed to get dst tile for array copy\n", BAILOUT_UNLOCK);

  stat = mmb_tile_at(src, zero_idx, &tile_src);
  CHECK_STATUS(stat, "Failed to get src tile for array copy\n", BAILOUT_UNLOCK);

  /* Do the copy */
  stat = mmb_tile_copy(tile_dst, tile_src);
  CHECK_STATUS(stat, "Failed to copy src to dst tiles for mmbArray copy\n", BAILOUT_UNLOCK);

  stat = mmb_index_destroy(zero_idx);
  CHECK_STATUS(stat, "Failed to destroy mmbIndex object\n", BAILOUT_UNLOCK);

  stat = mmb_array_untile_lockfree(dst);
  CHECK_STATUS(stat, "Failed to untile dst mmbArray for copy\n", BAILOUT_UNLOCK);

  stat = mmb_array_untile_lockfree(src);
  CHECK_STATUS(stat, "Failed to untile src mmbArraymmbArray for copy\n", BAILOUT_UNLOCK);

BAILOUT_UNLOCK:
  mmb_array_unlock(src);
  mmb_array_unlock(dst);
BAILOUT:
  return stat;
}

mmbError mmb_array_reshape(mmbArray *in_mba, mmbDimensions *in_dims) {
  mmbError stat = MMB_OK;
  if (!in_mba ) {
    MMB_ERR("Array can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }
  if (!in_dims) {
    MMB_ERR("Reshape dimensions can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if(!in_mba->data_owned){
    MMB_ERR("Cannot reshape wrapped array\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  /* Nothing to do if in_dims match existing dimensions */
  if(mmb_dimensions_cmp(&in_mba->dims, in_dims) == 0){
    goto BAILOUT;
  }

  /* Lock array during reshaping */
  mmb_array_lock(in_mba);

  /* Must untile array to reshape */
  if(in_mba->tiling){
    stat = mmb_array_untile_lockfree(in_mba);
    CHECK_STATUS(stat, "Unable to remove array tiling\n", BAILOUT_UNLOCK);
  }

  size_t n_bytes;
  stat = mmb_layout_buffer_size(in_mba->layout, in_dims, &n_bytes);
  CHECK_STATUS(stat, "Unable to compute layout dimensions.\n", BAILOUT_UNLOCK);

  /* Just free and allocate again, data is lost */
  stat = mmb_free(in_mba->allocation);
  CHECK_STATUS(stat, "Failed to free Mamba array memory\n", BAILOUT_UNLOCK);

  stat = mmb_allocate(n_bytes, in_mba->interface, &in_mba->allocation);
  CHECK_STATUS(stat, "Failed to allocate memory for mmbArray.\n", BAILOUT_UNLOCK);

  stat = mmb_dimensions_copy(&in_mba->dims, in_dims);
  CHECK_STATUS(stat, "Failed to update array dimensions\n", BAILOUT_UNLOCK);

BAILOUT_UNLOCK:
  mmb_array_unlock(in_mba);

BAILOUT:
  return stat;
}

mmbError mmb_array_describe(mmbArray *in_mba, FILE* in_stream) {
  mmbError stat = MMB_OK;
  if (!in_mba ) {
    MMB_ERR("Array can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }

  if (!in_stream ) {
    MMB_ERR("Stream ptr can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  /* Shape */
  fprintf(in_stream, "Array shape: {");
  for(unsigned i = 0; i < in_mba->layout->n_dims-1; i++){
    fprintf(in_stream, "%zu,", in_mba->dims.d[i]);
  }
  fprintf(in_stream,"%zu}\n", in_mba->dims.d[in_mba->layout->n_dims-1]);
  /* Layout */
  fprintf(in_stream, "Layout: Type: %s\n", mmb_layout_type_to_str(in_mba->layout->type));
  fprintf(in_stream, "\tNo. Dimensions: %zu\n", in_mba->layout->n_dims);
  fprintf(in_stream, "\tElement type: %s\n", mmb_element_type_to_str(in_mba->layout->element.type));
  fprintf(in_stream, "\tElement size: %zu\n", in_mba->layout->element.size_bytes);
  fprintf(in_stream, "\tElement order: %s\n", mmb_layout_order_to_str(in_mba->layout->element.order));

  if(in_mba->layout->type == MMB_REGULAR_BLOCK) {
    fprintf(in_stream, "\tBlock order: %s\n", mmb_layout_order_to_str(in_mba->layout->block.order));
    fprintf(in_stream, "\tBlock shape: {");
    for(unsigned i = 0; i < in_mba->layout->block.dimensions.size-1; i++){
      fprintf(in_stream, "%zu,", in_mba->layout->block.dimensions.d[i]);
    }
    fprintf(in_stream,"%zu}\n", in_mba->layout->block.dimensions.d[in_mba->layout->block.dimensions.size-1]);
  } else if(in_mba->layout->type == MMB_IRREGULAR) {
    fprintf(in_stream, "\tIrregular block output not supported\n");
    /*
    for(unsigned i = 0; i < in_mba->layout->irregular.n_blocks; i++) {
      fprintf(in_stream, "\tBlock %u, Offset %zu, Length %zu\n", i,
              in_mba->layout->irregular.offsets[i],
              in_mba->layout->irregular.lengths[i]);
    }
    */
  }

  /* TODO: Detailed padding info */
  if(in_mba->layout->pad.element_is_padded)
    fprintf(in_stream, "\tElement is padded\n");

  if(in_mba->layout->pad.dimension_is_padded)
    fprintf(in_stream, "\tDimension is padded\n");


  /* Tiling */
  bool is_tiled;
  stat = mmb_array_is_tiled(in_mba, &is_tiled);
  fprintf(in_stream, "Tiling: %s\n", (is_tiled ? "Yes" : "No"));
  if(is_tiled) {
    fprintf(in_stream, "Tile shape: {");
    for(unsigned i = 0; i < in_mba->tiling->tile_size.size-1; i++){
      fprintf(in_stream, "%zu,", in_mba->tiling->tile_size.d[i]);
    }
    fprintf(in_stream,"%zu}\n", in_mba->tiling->tile_size.d[in_mba->tiling->tile_size.size-1]);
  }

  /* Data characteristics */
  fprintf(in_stream,"Data owned: %s\n", (in_mba->data_owned ? "Yes" : "No"));
  /* TODO: Access type stringification */

  /* Allocation */
  fprintf(in_stream,"Allocation size: %zu\n", in_mba->allocation->n_bytes);
  fprintf(in_stream,"Allocation owned: %s\n", (in_mba->allocation->owned ? "Yes" : "No"));

  /* Memory interface */
  /* TODO: Add to_str for interface components in memory lib */

BAILOUT:
  return stat;
}

mmbError mmb_array_is_empty(mmbArray *in_mba, bool *out_empty) {
  mmbError stat = MMB_OK;
  if (!in_mba) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }
  if (!out_empty) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  *out_empty = false;
  size_t product;
  stat = mmb_dimensions_prod(&in_mba->dims, &product);
  CHECK_STATUS(stat, "Failed to compute array dimensions product\n", BAILOUT);
  if(product == 0)
    *out_empty = true;
BAILOUT:
  return stat;
}

mmbError mmb_array_is_tiled(mmbArray *in_mba, bool *out_is_tiled) {
 mmbError stat = MMB_OK;
  /* Validate */
  if (!in_mba) {
    MMB_ERR("Array can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }
  if (!out_is_tiled) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  if(in_mba->tiling != NULL)
    *out_is_tiled = true;
  else
    *out_is_tiled = false;

BAILOUT:
  return stat;
}

mmbError mmb_array_tile(mmbArray *in_mba, mmbDimensions *in_dims) {
  mmbError stat = MMB_OK;
  if (!in_mba ) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }
  mmb_array_lock(in_mba);
  stat = mmb_array_tile_lockfree(in_mba, in_dims);
  CHECK_STATUS(stat, "Failed to tile mmbArray", BAILOUT_UNLOCK);
BAILOUT_UNLOCK:
  mmb_array_unlock(in_mba);
BAILOUT:
  return stat;
}

mmbError mmb_array_tile_single(mmbArray *in_mba, mmbArrayTile **out_tile) {
  mmbError stat = MMB_OK;
  if (!in_mba ) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mmb_array_lock(in_mba);
  stat = mmb_array_tile_lockfree(in_mba, &in_mba->dims);
  mmb_array_unlock(in_mba);
  CHECK_STATUS(stat, "Failed to tile array\n", BAILOUT);

  mmbIndex *idx_a;
  stat = mmb_index_create(in_mba->layout->n_dims, &idx_a);
  CHECK_STATUS(stat, "Failed to create index for single array tiling\n", BAILOUT);

  stat = mmb_tile_at(in_mba, idx_a, out_tile);
  CHECK_STATUS(stat, "Failed to get single array tile\n", BAILOUT);

BAILOUT:
  return stat;
}

mmbError mmb_array_tile_lockfree(mmbArray *in_mba, mmbDimensions *in_dims) {
  mmbError stat = MMB_OK;
  /* Validate */
  if (!in_mba ) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }

  bool empty;
  stat = mmb_array_is_empty(in_mba, &empty);
  if(empty) {
    MMB_ERR("Cannot tile empty array\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  /* Use array dims for tile if argument was NULL */
  if (!in_dims){
    in_dims = &in_mba->dims;
  }

  size_t product;
  stat = mmb_dimensions_prod(in_dims, &product);
  if(product == 0) {
    MMB_ERR("Cannot tile array with tile shape containing a zero dimension\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  if (in_mba->tiling) {
    stat = mmb_array_untile_lockfree(in_mba);
    CHECK_STATUS(stat, "Failed to remove previous mmbArray tiling\n", BAILOUT);
  }

  if (in_mba->dims.size != in_dims->size) {
    MMB_ERR("Number of tile dimensions must match mmbArray layout dimensions\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  size_t elements_per_tile = 0;
  stat = mmb_dimensions_prod(in_dims, &elements_per_tile);
  CHECK_STATUS(stat, "Unable to calculate size of tile\n", BAILOUT);
  if(elements_per_tile == 0) {
    MMB_ERR("All dimensions must be non-zero, tile must have at least one element.\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  MMB_DEBUG("Creating tiling.\n");
  for(unsigned i = 0; i < in_dims->size; i++){
    MMB_DEBUG("Tiling dimension %d: %zu\n", i, in_dims->d[i]);
  }

  /* Store tile dimensions */
  mmbTiling *t = (mmbTiling *)calloc(1, sizeof(mmbTiling));
  stat = mmb_dimensions_copy(&t->tile_size, in_dims);
  CHECK_STATUS(stat, "Failed to copy tile dimensions\n", BAILOUT);

  stat = mmb_dimensions_resize(&t->tile_count, in_mba->dims.size);
  CHECK_STATUS(stat, "Failed to resize tile count dimensions\n", BAILOUT);

  /* Count tiles */
  t->total = 1;
  for (unsigned i = 0; i < in_mba->dims.size; i++) {
    t->tile_count.d[i] = (in_mba->dims.d[i] + (t->tile_size.d[i] - 1)) /
                         t->tile_size.d[i];
    t->total *= t->tile_count.d[i];
  }

  MMB_DEBUG("Total tiles: %zu\n", t->total);

  /* Init hashmap */
  t->tiles = mmb_hash_init_array_tiles();

  in_mba->tiling = t;

BAILOUT:
  return stat;
}

mmbError mmb_array_untile(mmbArray *in_mba){
  mmb_array_lock(in_mba);
  mmbError stat = mmb_array_untile_lockfree(in_mba);
  mmb_array_unlock(in_mba);
  CHECK_STATUS(stat, "Failed to untile_lockfree array", BAILOUT);
BAILOUT:
  return stat;
}

mmbError mmb_array_untile_lockfree(mmbArray *in_mba){
  mmbError stat = MMB_OK;

  if(!in_mba) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARRAY;
    goto BAILOUT;
  }
  /* Clear tile hashmap */
  if(!in_mba->tiling)
      return stat;

  mmbArrayTile *tile;
  size_t id;
  mmb_hash_foreach_array_tiles(in_mba->tiling->tiles, id, tile,
    {
      /* Satisfy compiler warning, id is required here but not used */
      (void)id;
      /* Free stack of tiles in LIFO order */
      while(tile->next != NULL)
        tile = tile->next;

      while(tile->prev != NULL){
        mmbArrayTile *prev = tile->prev;
        stat = mmb_tile_destroy(tile);
        if(stat != MMB_OK) {
          MMB_ERR("Failed to destroy tile during untiling\n");
        }
        tile = prev;
      }
      mmb_tile_destroy(tile);
    }
  )
  mmb_hash_destroy_array_tiles(in_mba->tiling->tiles);
  

  /* Cleanup */
  stat = mmb_dimensions_resize(&in_mba->tiling->tile_size, 0);
  CHECK_STATUS(stat, "Failed to resize tile size dimensions\n", BAILOUT);

  stat = mmb_dimensions_resize(&in_mba->tiling->tile_count, 0);
  CHECK_STATUS(stat, "Failed to resize tile count dimensions\n", BAILOUT);

  free(in_mba->tiling);
  in_mba->tiling = NULL;
BAILOUT:
  return stat;
}

mmbError mmb_array_get_layout(mmbArray *mba, mmbLayout **layout)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  if (!layout) {
    MMB_ERR("Invalid pointer to layout handle. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *layout = mba->layout;
  return MMB_OK;
}

mmbError mmb_array_get_dimensions(mmbArray *mba, mmbDimensions **dimensions)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  if (!dimensions) {
    MMB_ERR("Invalid pointer to dimensions handle. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *dimensions = &mba->dims;
  return MMB_OK;
}

/* memcpy-like semantics - cannot copy mba->dims to a ref to mba->dims */
mmbError mmb_array_copy_dimensions(mmbArray *mba, mmbDimensions *dimensions)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  if (!dimensions) {
    MMB_ERR("Invalid pointer to dimensions object. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  mmb_array_lock(mba);
  mmbError stat = mmb_dimensions_copy(dimensions, &mba->dims);
  mmb_array_unlock(mba);
  CHECK_STATUS(stat, "Failed to copy mmbDimensions object\n", BAILOUT);

BAILOUT:
  return stat;
}

mmbError mmb_array_get_layer(const mmbArray *mba, mmbMemLayer *layer)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  return mmb_meminterface_get_layer(mba->interface, layer);
}

mmbError mmb_array_get_execution_context(const mmbArray *mba,
                                         mmbExecutionContext *ex_con)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  return mmb_meminterface_get_execution_context(mba->interface, ex_con);
}

mmbError mmb_array_get_space(mmbArray *mba, mmbMemSpace **space)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  return mmb_meminterface_get_space(mba->interface, space);
}

mmbError mmb_array_get_provider(const mmbArray *mba, mmbProvider *provider)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  return mmb_meminterface_get_provider(mba->interface, provider);
}

mmbError mmb_array_get_strategy(const mmbArray *mba, mmbStrategy *strategy)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  return mmb_meminterface_get_strategy(mba->interface, strategy);
}

mmbError mmb_array_get_interface(mmbArray *mba, mmbMemInterface **interface)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  if (!interface) {
    MMB_ERR("Invalid pointer to interface handle. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *interface = mba->interface;
  return MMB_OK;
}

mmbError mmb_array_get_access_type(const mmbArray *mba, mmbAccessType *access)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  if (!access) {
    MMB_ERR("Invalid pointer to access. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *access = mba->access;
  return MMB_OK;
}

mmbError mmb_array_owns_data(const mmbArray *mba, bool *data_owned)
{
  if (!mba) {
    MMB_ERR("Invalid array handle (NULL).\n");
    return MMB_INVALID_ARRAY;
  }
  if (!data_owned) {
    MMB_ERR("Invalid pointer to boolean. Cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  *data_owned = mba->data_owned;
  return MMB_OK;
}
