/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_SET_H
#define MMB_SET_H

#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>

#include "mmb_error.h"

/** Defines the final symbol to be used */
#define mmbSet_name(T) mmbSet_ ## T

/** Generate the prototypes for each type of mmbSet */
#define GENERATE_MMB_SET_TYPE(T)                                            \
  typedef struct {                                                          \
    size_t size;                                                            \
    T *d;                                                                   \
  } mmbSet_name(T);                                                         \
  s_mmbSet_create(T);                                                       \
  s_mmbSet_insert(T);                                                       \
  s_mmbSet_set(T);                                                          \
  s_mmbSet_resize(T);                                                       \
  s_mmbSet_copy(T);                                                         \
  s_mmbSet_copy_buffer(T);                                                  \
  s_mmbSet_fill(T);                                                         \
  s_mmbSet_destroy(T);                                                      \
  s_mmbSet_cmp(T);                                                          \
  s_mmbSet_prod(T);                                                         \
  s_mmbSet_set_buf_addr(T);                                                 \
  s_mmbSet_get_size(T);                                                     \
  s_mmbSet_get_sizeof(T)

#ifdef __cplusplus
#define s_mmbSet_empty(T) static_cast<mmbSet_name(T)>({0, NULL})
#else
#define s_mmbSet_empty(T) {0, NULL}
#endif

#define s_mmbSet_create(T)                                                  \
  mmbError mmbSet_ ## T ## _create(const size_t nelt, mmbSet_name(T) **out_set)

#define s_mmbSet_insert(T)                                                  \
  mmbError mmbSet_ ## T ## _add(mmbSet_name(T) *in_set, T *elt, size_t pos)

#define s_mmbSet_set(T)                                                     \
  mmbError mmbSet_ ## T ## _set(mmbSet_name(T) *in_set, T s0, va_list vargs)

#define s_mmbSet_copy(T)                                                    \
  mmbError mmbSet_ ## T ## _copy(mmbSet_name(T) *dst, const mmbSet_name(T) *src)

#define s_mmbSet_copy_buffer(T)                                             \
  mmbError mmbSet_ ## T ## _copy_buffer(T *dst, const mmbSet_name(T) *src)

#define s_mmbSet_fill(T)                                                    \
  mmbError mmbSet_ ## T ## _fill(mmbSet_name(T) *dst,                       \
    const T *src, const size_t nelt)

#define s_mmbSet_resize(T)                                                  \
  mmbError mmbSet_ ## T ## _resize(mmbSet_name(T) *in_set, const size_t nelt)

#define s_mmbSet_destroy(T)                                                 \
  mmbError mmbSet_ ## T ## _destroy(mmbSet_name(T) *in_set)

#define s_mmbSet_cmp(T)                                                     \
  int mmbSet_ ## T ## _cmp(const mmbSet_name(T) *s1, const mmbSet_name(T) *s2)

#define s_mmbSet_prod(T)                                                    \
  mmbError mmbSet_ ## T ## _prod(const mmbSet_name(T) *in_set, T *out_prod)

#define s_mmbSet_set_buf_addr(T)                                            \
  mmbError mmbSet_ ## T ## _set_buf_addr(mmbSet_name(T) *in_set, T *buffer)

#define s_mmbSet_get_size(T)                                                \
  mmbError mmbSet_ ## T ## _get_size(const mmbSet_name(T) *in_set, size_t *size)

#define s_mmbSet_get_sizeof(T)                                              \
  mmbError mmbSet_ ## T ## _get_sizeof(const mmbSet_name(T) *in_set, size_t *size)

#ifdef __cplusplus
extern "C" {
#endif

GENERATE_MMB_SET_TYPE(size_t);

GENERATE_MMB_SET_TYPE(uint64_t);

#ifdef __cplusplus
}
#endif

#endif
