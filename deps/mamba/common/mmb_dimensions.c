/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_error.h"
#include "mmb_dimensions.h"
#include "mmb_set.h"

const mmbDimensions mmb_dimensions_empty = s_mmbSet_empty(size_t);

mmbError mmb_dimensions_create(const size_t ndim, mmbDimensions **out_dims) {
  return mmbSet_size_t_create(ndim, out_dims);
}

mmbError mmb_dimensions_set(mmbDimensions *in_dims, size_t dimN, ...) {
  va_list args;
  va_start(args, dimN);
  mmbError stat = mmbSet_size_t_set(in_dims, dimN, args);
  va_end(args);
  return stat;
}

mmbError mmb_dimensions_create_fill(const size_t ndim, const size_t* values, mmbDimensions **out_dims) {
  mmbError stat = mmbSet_size_t_create(ndim, out_dims);
  if(stat != MMB_OK)
    return stat;
  return mmbSet_size_t_fill(*out_dims, values, ndim);
}

mmbError mmb_dimensions_copy(mmbDimensions * restrict dst, const mmbDimensions * restrict src) {
  return mmbSet_size_t_copy(dst, src);
}

mmbError mmb_dimensions_copy_buffer(size_t *dst, const mmbDimensions *src) {
  return mmbSet_size_t_copy_buffer(dst, src);
}

mmbError mmb_dimensions_fill(mmbDimensions *dst, const size_t *src, const size_t nelt) {
  return mmbSet_size_t_fill(dst, src, nelt);
}

mmbError mmb_dimensions_resize(mmbDimensions *in_dims, const size_t ndim) {
  return mmbSet_size_t_resize(in_dims, ndim);
}

mmbError mmb_dimensions_destroy(mmbDimensions *in_dims) {
  return mmbSet_size_t_destroy(in_dims);
}

int mmb_dimensions_cmp(const mmbDimensions *d1, const mmbDimensions *d2) {
  return mmbSet_size_t_cmp(d1, d2);
}

mmbError mmb_dimensions_prod(const mmbDimensions *in_dims, size_t *out_prod) {
  return mmbSet_size_t_prod(in_dims, out_prod);
}

mmbError mmb_dimensions_set_buf_addr(mmbDimensions *in_dims, size_t *buffer) {
  return mmbSet_size_t_set_buf_addr(in_dims, buffer);
}

mmbError mmb_dimensions_get_size(const mmbDimensions *in_dims, size_t *size) {
  return mmbSet_size_t_get_size(in_dims, size);
}

mmbError mmb_dimensions_get_sizeof(const mmbDimensions *in_dims, size_t *size) {
  return mmbSet_size_t_get_sizeof(in_dims, size);
}
