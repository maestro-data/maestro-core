#include <stdbool.h>
#include "mmb_tile.h"
#include "mmb_logging.h"
#include "mmb_memory.h"
#include "mamba.h"

mmbError mmb_tile_get_data_f(int rank,
			     mmbArrayTile *in_tile,
			     void **allocation_ptr,
			     size_t *lower,
			     size_t *upper,
			     size_t *alower,
			     size_t *aupper,
			     size_t *dim,
			     size_t *abs_dim,
			     bool *is_contiguous ){
  size_t r = (size_t) rank;

   /* Validate args */
  if(in_tile == NULL)
    return MMB_ERROR;

  if(in_tile->allocation == NULL)
    return MMB_ERROR;

  *allocation_ptr = (void*)(in_tile->allocation->ptr);
  //printf("allocation ptr = %p\n",*allocation_ptr);
  mmbError stat = MMB_OK;

  // r = rank = dst_tile->layout->n_dims
  for(unsigned i = 0; i < r; i++) {
    lower[i]=in_tile->lower[i]+1;
    upper[i]=in_tile->upper[i];
    alower[i]=in_tile->alower[i]+1;
    aupper[i]=in_tile->aupper[i];
    dim[i]=in_tile->dim[i];
    abs_dim[i]=in_tile->abs_dim[i];
  }

  *is_contiguous = in_tile->access & MMB_CONTIGUOUS;
  
  return stat;
}
			     
