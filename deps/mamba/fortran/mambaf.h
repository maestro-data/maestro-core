! Mamba Fortran include file
! Only needed if the application needs to use index macros for tile access

#define MMB_IDX_2D_NORM(T, TP, I, J )					\
    TP( ((J-1)+T%lower(1)-1)*T%abs_dim(1) + (T%lower(1)-1)+(I) )

#define MMB_IDX_2D(T, TP, I, J )		\
    TP( (J-1)*T%abs_dim(1) + (I) )


#define MMB_IDX_1D(T, TP, I, J )			\
    TP( I )





