! Fortran interface to Mamba C API
MODULE MAMBA

  USE, INTRINSIC :: ISO_C_BINDING, only :&
       C_PTR, C_INTPTR_T, C_INT64_T,     &
       C_SIZE_T, C_CHAR,                 &
       C_NULL_PTR,                       &
       C_NULL_CHAR,                       &
       C_bool,                           &
       C_int,                            &
       C_uintptr_t    => C_INTPTR_T,     &
       C_voidptr      => C_ptr,          &
       mmbMemSpace => C_PTR, &
       mmbMemInterface => C_PTR, &
       mmbDimensions => C_PTR, &
       mmbLayout => C_PTR, &
       mmbLayoutPadding => C_PTR, &
       mmbArray => C_PTR, &
       C_mmbArrayTile => C_PTR, &
       mmbTiling => C_PTR, &
       mmbTileIterator => C_PTR, &
       mmbAccessType => C_PTR,  &
       mmbIndex => C_PTR, &
       mmbSizeKind => C_SIZE_T, &
       mmbIndexKind => C_SIZE_T
  
  ! mmb_error.h
  ENUM, BIND(C)
     ENUMERATOR ::                  &
          MMB_OK                         =  0, &
          MMB_ERROR                          , &
          MMB_OUT_OF_MEMORY                  , &
          MMB_OUT_OF_BOUNDS                  , &
          MMB_NOT_FOUND                      , &
          MMB_INVALID_ARG                    , &
          MMB_INVALID_LAYER                  , &
          MMB_INVALID_PROVIDER               , &
          MMB_INVALID_INTERFACE              , &
          MMB_INVALID_EXECUTION_CONTEXT      , &
          MMB_INVALID_STRATEGY               , &
          MMB_INVALID_ALLOCATION             , &
          MMB_INVALID_SIZE                   , &
          MMB_INVALID_SPACE                  , &
          MMB_INVALID_ARRAY                  , &
          MMB_UNIMPLEMENTED                  , &
          MMB_UNSUPPORTED                    , &
          MMB_ERROR__MAX
  END ENUM

  INTEGER, parameter :: mmbErrorKind = kind(MMB_ERROR__MAX)
  private i64,dp
  integer(selected_int_kind(9)),parameter :: i64=selected_int_kind(18)
  integer(selected_int_kind(9)),parameter :: dp=kind(1d0)
  
  ! then define a type, making sure the size fits whatever the C
  ! compiler choses for enums (gcc might do short enums for instance)
!  TYPE, BIND(C) :: mmbError
!     INTEGER(KIND=KIND(MMB_status__MAX)) :: code
!  END TYPE mmbError

  ! mmb_memory.h mmbMemLayer   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::                  &
          MMB_DRAM          =  0, &
          MMB_GDRAM             , &
          MMB_HBM               , &
          MMB_NVDIMM            , & ! Non-Volatile
          MMB_NADRAM            , & ! Network Attached
          MMB_SSD               , &
          MMB_MEMLAYER__MAX
  END ENUM
  
  ! mmb_memory.h mmbStrategy   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::                  &
          MMB_ANY_STRATEGY      =  -2, &
          MMB_STRATEGY_DEFAULT  =  -1, &
          MMB_NO_STRAT          =   0, &
          MMB_THREAD_SAFE       =   1, &
          MMB_LIMITED_GREEDY    =   2, &
          MMB_POOLED            =   3, &
          MMB_STATISTICS        =   4, &
          MMB_POOLED_STATISTICS =   5, &
          MMB_STRATEGY__MAX     =   6
  END ENUM

  ! mmb_memory.h mmbProvider   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::                             &
          MMB_ANY_PROVIDER               =  -2, &
          MMB_PROVIDER_DEFAULT           =  -1, &
          MMB_NATIVE                     =  0, &
          MMB_SICM                       =  1, &
          MMB_UMPIRE                     =  2, &
          MMB_JEMALLOC                   =  3, &
          MMB_PROVIDER__MAX    =  4
  END ENUM

  ! mmb_memory.h mmbExecutionContext   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::                             &
          MMB_NO_EXECUTION_CONTEXT       = -2, &
          MMB_EXECUTION_CONTEXT_DEFAULT  = -1, &
          MMB_CPU                        =  0, &
          MMB_GPU_CUDA                   =  1, &
          MMB_GPU_HIP                    =  2, &
          MMB_OPENCL                     =  3, &
          MMB_EXECUTION_CONTEXT__MAX     =  4
  END ENUM

  ! mmb_layout.h mmbAccessType   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::    &
          MMB_READ           =  1, &
          MMB_WRITE          =  2, &
          MMB_READ_WRITE     =  3, &
          MMB_WRITE_LOCKED   =  4, &
          MMB_CONTIGUOUS     =  8, &
          MMB_ACCESSTYPE_MAX 
  END ENUM

  ! mmb_layout.h mmbLayoutElementType   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::    &
          MMB_LAYOUT_ELEMENTTYPE_NONE = 0, &
          MMB_ELEMENT, &
          MMB_STRUCTURE, &
          MMB_SOA, &
          MMB_LAYOUT_ELEMENTTYPE_MAX 
  END ENUM

  ! mmb_layout.hmmbLayoutOrder   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::    &
          MMB_LAYOUT_ORDER_NONE = 0, &
          MMB_ROWMAJOR, &
          MMB_COLMAJOR, & 
          MMB_GENERIC_ND, &
          MMB_LAYOUT_ORDER_MAX
  END ENUM

  ! mmb_memory_options.h mmbSizeConfigAction   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::    &
          MMB_SIZE_NONE       = -2, &
          MMB_SIZE_ANY        = -1, &
          MMB_SIZE_SET        =  0, &
          MMB_SIZE_INVALID     
  END ENUM

  ! mmb_TILE.h mmbMergeType   C API CONSISTENCY CHECK NEEDED
  ENUM, BIND(C)
     ENUMERATOR ::    &
          MMB_DISCARD         = 0, &
          MMB_OVERWRITE       = 1, &
          MMB_MERGETYPE_MAX
  END ENUM

  ! mmb_logging.h 
  ! Log level, these are #defines in the C header
  INTEGER, parameter :: MMB_LOG_ERR   = 0, &
                        MMB_LOG_WARN  = 1, &
                        MMB_LOG_INFO  = 2, &
                        MMB_LOG_DEBUG = 3, &
                        MMB_LOG_NOISE = 4
  
  INTEGER, parameter :: mmbAccessTypeKind = kind(MMB_ACCESSTYPE_MAX)
  INTEGER, parameter :: mmbProviderKind = kind(MMB_PROVIDER__MAX)
  INTEGER, parameter :: mmbStrategyKind = kind(MMB_STRATEGY__MAX)
  
  TYPE, BIND(C) :: mmbAllocation
     TYPE(C_PTR) :: ptr
     INTEGER(mmbSizeKind) :: n_bytes
     TYPE(mmbMemInterface) :: interface
     LOGICAL(c_bool) :: owned
  END TYPE mmbAllocation

  TYPE, BIND(C) :: mmbProviderOptions
     TYPE(C_PTR) :: ptr=C_NULL_PTR
  END TYPE mmbProviderOptions

  TYPE, BIND(C) :: mmbMemInterfaceConfig_c
     INTEGER(mmbProviderKind) :: provider
     TYPE(mmbProviderOptions) :: provider_opts
     INTEGER(mmbStrategyKind) :: strategy
!     CHARACTER(KIND=C_CHAR), dimension(64) :: name
     TYPE(C_PTR) :: name
  END TYPE mmbMemInterfaceConfig_c
  TYPE :: mmbMemInterfaceConfig
     INTEGER(mmbProviderKind) :: provider
     TYPE(mmbProviderOptions) :: provider_opts
     INTEGER(mmbStrategyKind) :: strategy
     CHARACTER(len=64) :: name
  END TYPE mmbMemInterfaceConfig

  TYPE, BIND(C) :: mmbSizeConfig
    INTEGER(kind(MMB_SIZE_INVALID)) :: actio
    LOGICAL(C_BOOL) :: is_numa_aware
    ! TODO - this is a union in the C interface
    INTEGER(c_size_t) ::  mem_size
    INTEGER(mmbSizeKind) :: numa_padding_union_fix=0 !TODO
  END TYPE mmbSizeConfig
  
  TYPE :: mmbMemSpaceConfig
    TYPE(mmbSizeConfig) :: size_opts
    TYPE(mmbMemInterfaceConfig) ::  interface_opts
  END TYPE mmbMemSpaceConfig
  TYPE, BIND(C) :: mmbMemSpaceConfig_c
    TYPE(mmbSizeConfig) :: size_opts
    TYPE(mmbMemInterfaceConfig_c) ::  interface_opts
  END TYPE mmbMemSpaceConfig_c

  ! TODO MMB_MEMSPACE_CONFIG_DEFAULT
  TYPE(mmbMemInterfaceConfig),parameter :: MMB_MEMINTERFACE_CONFIG_DEFAULT=&
       mmbMemInterfaceConfig(MMB_ANY_PROVIDER,mmbProviderOptions(),MMB_ANY_STRATEGY,"")
  TYPE(mmbMemSpaceConfig),parameter :: MMB_MEMSPACE_CONFIG_DEFAULT=&
    mmbMemSpaceConfig(mmbSizeConfig(MMB_SIZE_ANY,.false.,1000),MMB_MEMINTERFACE_CONFIG_DEFAULT)
  
  TYPE :: mmbArrayTile
     TYPE(C_mmbArrayTile) :: c_tile ! The C tile structure
     INTEGER :: rank=0
     TYPE(C_PTR) :: ptr
     !! Decide if padding should be raised from layout to here
     ! Lower and upper indices in tile allocation
     INTEGER(mmbIndexKind), allocatable :: lower(:)
     INTEGER(mmbIndexKind), allocatable :: upper(:)
     ! lower and upper indices in array (0:n+1 in 1:n in fortran)
     ! Virtual lower and upper indices used for slicing and may be useful
     ! for other applications such as halo management and multi-level tiling
     INTEGER(mmbIndexKind), allocatable :: alower(:)
     INTEGER(mmbIndexKind), allocatable :: aupper(:)
     ! Tile dimensions
     INTEGER(mmbIndexKind), allocatable :: dim(:)
     ! Dimension of allocation containing tile
     ! Absolute dimensions, required when tile is not a contiguous allocation
     INTEGER(mmbIndexKind), allocatable :: abs_dim(:)
     LOGICAL is_contiguous
  END TYPE mmbArrayTile

!!  TYPE, BIND(C) :: mmbArray
!!     TYPE(mmbAllocation) allocation
!!     TYPE(mmbLayout) layout
!!     TYPE(mmbMemSpace) space
!!     TYPE(mmbTiling) tiling
!!     TYPE(mmbAccessType) access
!!     TYPE(mmbDimensions) :: dims
!!     INTEGER(c_int) data_owned
!!  END TYPE mmbArray

  TYPE, BIND(C) :: mmbOptions
   INTEGER :: FILL_IN_THIS_STRUCTURE
  END Type mmbOptions

  PRIVATE mmb_tile_sync
  
  INTERFACE mmb_tile_get_pointer
    MODULE PROCEDURE mmb_tile_get_pointer_1d_r
    MODULE PROCEDURE mmb_tile_get_pointer_1d_r8
    MODULE PROCEDURE mmb_tile_get_pointer_1d_c
    MODULE PROCEDURE mmb_tile_get_pointer_1d_c16
    MODULE PROCEDURE mmb_tile_get_pointer_1d_i
    MODULE PROCEDURE mmb_tile_get_pointer_1d_i8
    
    MODULE PROCEDURE mmb_tile_get_pointer_2d_r
    MODULE PROCEDURE mmb_tile_get_pointer_2d_r8
    MODULE PROCEDURE mmb_tile_get_pointer_2d_c
    MODULE PROCEDURE mmb_tile_get_pointer_2d_c16
    MODULE PROCEDURE mmb_tile_get_pointer_2d_i
    MODULE PROCEDURE mmb_tile_get_pointer_2d_i8

    MODULE PROCEDURE mmb_tile_get_pointer_3d_r
    MODULE PROCEDURE mmb_tile_get_pointer_3d_r8
    MODULE PROCEDURE mmb_tile_get_pointer_3d_c
    MODULE PROCEDURE mmb_tile_get_pointer_3d_c16
    MODULE PROCEDURE mmb_tile_get_pointer_3d_i
    MODULE PROCEDURE mmb_tile_get_pointer_3d_i8
  END INTERFACE mmb_tile_get_pointer

  !! functions
  INTERFACE

    ! Todo Needs argument
    FUNCTION mmb_init_i (opts) BIND(C,name='mmb_init')
      IMPORT :: mmbErrorKind,mmbOptions
      INTEGER(mmbErrorKind) :: mmb_init_i
      TYPE(mmbOptions), intent(in), optional :: opts
    END FUNCTION mmb_init_i
    
    FUNCTION mmb_finalize_i () BIND(C,name='mmb_finalize')
      IMPORT :: mmbErrorKind
      INTEGER(mmbErrorKind) :: mmb_finalize_i
    END FUNCTION mmb_finalize_i

   FUNCTION mmb_memspace_config_create_default_i(config) BIND(C,name='mmb_memspace_config_create_default')
     IMPORT :: mmbErrorKind,mmbMemSpaceConfig_c
     INTEGER(mmbErrorKind) :: mmb_memspace_config_create_default_i
     TYPE(mmbMemSpaceConfig_c), intent(out) :: config
   END FUNCTION mmb_memspace_config_create_default_i

   FUNCTION mmb_register_memory_i(layer,ex_context,config_opts,new_space) BIND(C,name='mmb_register_memory')
      IMPORT :: mmbErrorKind,mmbMemSpace,mmbMemSpaceConfig_c,MMB_MEMLAYER__MAX
      INTEGER(mmbErrorKind) :: mmb_register_memory_i
      INTEGER(kind(MMB_MEMLAYER__MAX)),intent(IN),value :: layer
      INTEGER(kind(MMB_EXECUTION_CONTEXT__MAX)),intent(in), value :: ex_context
      TYPE(mmbMemSpaceConfig_c), intent(in) :: config_opts
      TYPE(mmbMemSpace), intent(out), optional :: new_space
    END FUNCTION mmb_register_memory_i

    FUNCTION mmb_request_space_i(layer,ex_context,config_opts,new_space) BIND(C,name='mmb_request_space')
      IMPORT :: mmbErrorKind,mmbMemSpace,mmbMemSpaceConfig_c,MMB_MEMLAYER__MAX
      INTEGER(mmbErrorKind) :: mmb_request_space_i
      INTEGER(kind(MMB_MEMLAYER__MAX)),intent(in),value :: layer
      INTEGER(kind(MMB_EXECUTION_CONTEXT__MAX)),intent(in), value :: ex_context
      TYPE(mmbMemSpaceConfig_c), intent(in),optional :: config_opts
      TYPE(mmbMemSpace), intent(out) :: new_space
    END FUNCTION mmb_request_space_i

    FUNCTION mmb_request_interface_i(space,config_opts,new_interface) BIND(C,name='mmb_request_interface')
     IMPORT :: mmbErrorKind,mmbMemSpace,mmbMemInterfaceConfig_c,mmbMemInterface
     INTEGER(mmbErrorKind) :: mmb_request_interface_i
     TYPE(mmbMemSpace), intent(in),value :: space
     TYPE(mmbMemInterfaceConfig_c), intent(in),optional :: config_opts
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbMemInterface), intent(out) :: new_interface
    END FUNCTION mmb_request_interface_i
    
    FUNCTION mmb_dimensions_create_fill_i(rank, dim_arr, out_dims) BIND(C,name='mmb_dimensions_create_fill')
      IMPORT :: mmbErrorKind,mmbDimensions,mmbIndexKind,C_Size_t
      INTEGER(mmbErrorKind) :: mmb_dimensions_create_fill_i
      INTEGER(mmbIndexKind), intent(in), value :: rank
      INTEGER(mmbIndexKind), intent(in), dimension(*) :: dim_arr
      TYPE(mmbDimensions), intent(out) :: out_dims
    END FUNCTION mmb_dimensions_create_fill_i

    FUNCTION mmb_dimensions_destroy_i(in_dims) BIND(C,name="mmb_dimensions_destroy")
      IMPORT :: mmbErrorKind,mmbDimensions
      INTEGER(mmbErrorKind) :: mmb_dimensions_destroy_i
      TYPE(mmbDimensions), intent(in),value :: in_dims
    END FUNCTION mmb_dimensions_destroy_i

    FUNCTION mmb_layout_create_regular_nd_i(element_size_bytes,n_dims,&
       element_order,padding,layout) BIND(C,name='mmb_layout_create_regular_nd')
      IMPORT :: mmbErrorKind,mmbLayout,mmbLayoutPadding,mmbSizeKind,&
                mmbIndexKind,MMB_LAYOUT_ORDER_MAX
      INTEGER(mmbErrorKind) :: mmb_layout_create_regular_1d_i
      INTEGER(mmbSizeKind), intent(in), value :: element_size_bytes
      INTEGER(mmbIndexKind), intent(in), value :: n_dims
      INTEGER(kind(MMB_LAYOUT_ORDER_MAX)), intent(in), value :: element_order
      TYPE(mmbLayoutPadding), intent(in), value :: padding
      TYPE(mmbLayout), intent(out) :: layout
    END FUNCTION mmb_layout_create_regular_nd_i

    FUNCTION mmb_layout_create_regular_1d_i(element_size_bytes,padding,layout) BIND(C,name='mmb_layout_create_regular_1d')
      IMPORT :: mmbErrorKind,mmbLayout,mmbLayoutPadding,mmbSizeKind,C_Size_t
      INTEGER(mmbErrorKind) :: mmb_layout_create_regular_1d_i
      INTEGER(mmbSizeKind), intent(in), value :: element_size_bytes
      TYPE(mmbLayoutPadding), intent(in), value :: padding
      TYPE(mmbLayout), intent(out) :: layout
    END FUNCTION mmb_layout_create_regular_1d_i
    
    FUNCTION mmb_layout_padding_create_zero() BIND(C)
      IMPORT :: mmbLayoutPadding
      TYPE(mmbLayoutPadding) mmb_layout_padding_create_zero
    END FUNCTION mmb_layout_padding_create_zero      

    FUNCTION mmb_layout_destroy_i(in_layout) BIND(C,name="mmb_layout_destroy")
      IMPORT :: mmbErrorKind,mmbLayout
      INTEGER(mmbErrorKind) :: mmb_layout_destroy_i
      TYPE(mmbLayout), intent(in),value :: in_layout
    END FUNCTION mmb_layout_destroy_i

    FUNCTION mmb_array_create_i(dims,layout,mem_interface,access,mmb_array) BIND(C,name='mmb_array_create')
      IMPORT :: mmbErrorKind,mmbDimensions,mmbLayout,mmbMemInterface
      IMPORT :: mmbAccessTypeKind,mmbArray
      INTEGER(mmbErrorKind) :: mmb_array_create_i
      TYPE(mmbDimensions), intent(in), value :: dims
      TYPE(mmbLayout), intent(in),value :: layout
      TYPE(mmbMemInterface), intent(in), value :: mem_interface
      INTEGER(mmbAccessTypeKind), intent(in), value :: access
      TYPE(mmbArray), intent(out) :: mmb_array
    END FUNCTION mmb_array_create_i

    !deprecated
    FUNCTION mmb_array_create_wrapped_i(in_data,dims,layout,mem_interface,access,mmb_array) BIND(C,name='mmb_array_create_wrapped')
      IMPORT :: mmbErrorKind,mmbDimensions,mmbLayout,mmbMemInterface
      IMPORT :: mmbAccessTypeKind,mmbArray,C_ptr
      INTEGER(mmbErrorKind) :: mmb_array_create_i
      TYPE(C_ptr), intent(in), value :: in_data
      TYPE(mmbDimensions), intent(in), value :: dims
      TYPE(mmbLayout), intent(in),value :: layout
      TYPE(mmbMemInterface), intent(in),value :: mem_interface
      INTEGER(mmbAccessTypeKind), intent(in), value :: access
      TYPE(mmbArray), intent(out) :: mmb_array
    END FUNCTION mmb_array_create_wrapped_i

    FUNCTION mmb_array_create_wrapped_f(in_data,dims,layout,mem_interface,&
              access,mmb_array) BIND(C,name='mmb_array_create_wrapped_f')
      IMPORT :: mmbErrorKind,mmbDimensions,mmbLayout,mmbMemInterface
      IMPORT :: mmbAccessTypeKind,mmbArray,C_ptr
      INTEGER(mmbErrorKind) :: mmb_array_create_i
      TYPE(*), dimension(..), intent(in) :: in_data
      TYPE(mmbDimensions), intent(in), value :: dims
      TYPE(mmbLayout), intent(in),value :: layout
      TYPE(mmbMemInterface), intent(in),value :: mem_interface
      INTEGER(mmbAccessTypeKind), intent(in), value :: access
      TYPE(mmbArray), intent(out) :: mmb_array
    END FUNCTION mmb_array_create_wrapped_f

    FUNCTION mmb_array_tile_i(mmb_array,dims) BIND(C,name='mmb_array_tile')
      IMPORT :: mmbErrorKind,mmbDimensions,mmbArray
      INTEGER(mmbErrorKind) :: mmb_array_tile_i
      TYPE(mmbArray), intent(in),value :: mmb_array
      TYPE(mmbDimensions), intent(in),value :: dims
    END FUNCTION mmb_array_tile_i

    FUNCTION mmb_array_untile_i(mmb_array) BIND(C,name='mmb_array_untile')
      IMPORT :: mmbErrorKind,mmbArray
      INTEGER(mmbErrorKind) :: mmb_array_tile_i
      TYPE(mmbArray), intent(in),value :: mmb_array
    END FUNCTION mmb_array_untile_i

    FUNCTION mmb_array_copy_i(dst,src) BIND(C,name="mmb_array_copy")
      IMPORT :: mmbErrorKind,mmbArray
      INTEGER(mmbErrorKind) :: mmb_array_copy_i
      TYPE(mmbArray), intent(in),value :: dst
      TYPE(mmbArray), intent(in),value :: src
    END FUNCTION mmb_array_copy_i

    FUNCTION mmb_array_destroy_i(in_mba) BIND(C,name="mmb_array_destroy")
      IMPORT :: mmbErrorKind,mmbArray
      INTEGER(mmbErrorKind) :: mmb_array_destroy_i
      TYPE(mmbArray), intent(in),value :: in_mba
    END FUNCTION mmb_array_destroy_i
    
    FUNCTION mmb_tile_get_n_dims_i(in_tile,n_dims) BIND(C,name="mmb_tile_get_n_dims")
      IMPORT :: mmbErrorKind,C_mmbArrayTile,mmbIndexKind
      INTEGER(mmbErrorKind) :: mmb_tile_get_n_dims_i
      TYPE(C_mmbArrayTile), intent(in), value :: in_tile
      INTEGER(mmbIndexKind), intent(out) :: n_dims
    END FUNCTION mmb_tile_get_n_dims_i

    FUNCTION mmb_tile_duplicate_i(in_tile,in_interface,in_access, &
                                 in_layout,out_tile) BIND(C,name='mmb_tile_duplicate')
      IMPORT :: mmbErrorKind,C_mmbArrayTile,mmbMemInterface,mmbAccessTypeKind,&
                mmbLayout
      INTEGER(mmbErrorKind) :: mmb_tile_duplicate_i
      TYPE(C_mmbArrayTile), intent(in), value :: in_tile
      TYPE(mmbMemInterface), intent(in), value :: in_interface
      INTEGER(mmbAccessTypeKind), intent(in), value :: in_access
      TYPE(mmbLayout), intent(in),value :: in_layout
      TYPE(C_mmbArrayTile), intent(out) :: out_tile
    END FUNCTION mmb_tile_duplicate_i

    FUNCTION mmb_tile_at_i(in_mba,in_idx,out_tile) BIND(C,name='mmb_tile_at')
      IMPORT :: mmbErrorKind,mmbArray,C_mmbArrayTile,mmbIndex
      INTEGER(mmbErrorKind) :: mmb_tile_at_i
      TYPE(mmbArray), intent(in), value :: in_mba
      TYPE(mmbIndex), intent(in), value :: in_idx
      TYPE(C_mmbArrayTile), intent(out) :: out_tile
    END FUNCTION mmb_tile_at_i

    FUNCTION mmb_tile_at_1d_i(in_mba,idx1,out_tile) BIND(C,name='mmb_tile_at_1d')
      IMPORT :: mmbErrorKind,mmbArray,C_mmbArrayTile,mmbIndexKind
      INTEGER(mmbErrorKind) :: mmb_tile_at_1d_i
      TYPE(mmbArray), intent(in), value :: in_mba
      INTEGER(mmbIndexKind), intent(in), value :: idx1
      TYPE(C_mmbArrayTile), intent(out) :: out_tile
    END FUNCTION mmb_tile_at_1d_i

    FUNCTION mmb_tile_at_2d_i(in_mba,idx1,idx2,out_tile) BIND(C,name='mmb_tile_at_2d')
      IMPORT :: mmbErrorKind,mmbArray,C_mmbArrayTile,mmbIndexKind
      INTEGER(mmbErrorKind) :: mmb_tile_at_2d_i
      TYPE(mmbArray), intent(in), value :: in_mba
      INTEGER(mmbIndexKind), intent(in), value :: idx1
      INTEGER(mmbIndexKind), intent(in), value :: idx2
      TYPE(C_mmbArrayTile), intent(out) :: out_tile
    END FUNCTION mmb_tile_at_2d_i
    
    FUNCTION mmb_tile_at_3d_i(in_mba,idx1,idx2,idx3,out_tile) BIND(C,name='mmb_tile_at_3d')
      IMPORT :: mmbErrorKind,mmbArray,C_mmbArrayTile,mmbIndexKind
      INTEGER(mmbErrorKind) :: mmb_tile_at_3d_i
      TYPE(mmbArray), intent(in), value :: in_mba
      INTEGER(mmbIndexKind), intent(in), value :: idx1
      INTEGER(mmbIndexKind), intent(in), value :: idx2
      INTEGER(mmbIndexKind), intent(in), value :: idx3
      TYPE(C_mmbArrayTile), intent(out) :: out_tile
    END FUNCTION mmb_tile_at_3d_i
    
    FUNCTION mmb_tile_get_space_local_handle_i(in_tile,out_tile) BIND(C,name='mmb_tile_get_space_local_handle')
      IMPORT :: mmbErrorKind,C_mmbArrayTile
      INTEGER(mmbErrorKind) :: mmb_tile_get_space_local_handle_i
      TYPE(C_mmbArrayTile), intent(in), value :: in_tile
      TYPE(C_mmbArrayTile), intent(out) :: out_tile
    END FUNCTION mmb_tile_get_space_local_handle_i

    FUNCTION mmb_tile_merge_i(in_tile,in_merge) BIND(C,name='mmb_tile_merge')
      IMPORT :: mmbErrorKind,C_mmbArrayTile
      INTEGER(mmbErrorKind) :: mmb_tile_merge_i
      TYPE(C_mmbArrayTile), intent(in), value :: in_tile
      INTEGER(kind(MMB_MERGETYPE_MAX)), intent(in), value :: in_merge
    END FUNCTION mmb_tile_merge_i

    FUNCTION mmb_tile_destroy_i(in_tile) BIND(C,name='mmb_tile_destroy')
      IMPORT :: mmbErrorKind,C_mmbArrayTile
      INTEGER(mmbErrorKind) :: mmb_tile_destroy_i
      TYPE(C_mmbArrayTile), intent(in), value :: in_tile
    END FUNCTION mmb_tile_destroy_i
    
    FUNCTION mmb_tile_count_i(mmb_array,count) BIND(C,name='mmb_tile_count')
      IMPORT :: mmbErrorKind,mmbArray,mmbSizeKind
      INTEGER(mmbErrorKind) :: mmb_tile_count_i
      TYPE(mmbArray), intent(in),value :: mmb_array
      INTEGER(mmbSizeKind), intent(out) :: count
    END FUNCTION mmb_tile_count_i

    FUNCTION mmb_tiling_dimensions_i(mmb_array,out_dims) BIND(C,name='mmb_tiling_dimensions')
      IMPORT :: mmbErrorKind,mmbArray,mmbDimensions
      INTEGER(mmbErrorKind) :: mmb_tiling_dimensions_i
      TYPE(mmbArray), intent(in),value :: mmb_array
      TYPE(mmbDimensions), intent(out) :: out_dims
    END FUNCTION mmb_tiling_dimensions_i

    FUNCTION mmb_dimensions_copy_toarray_i(out_dimsarr,in_dims) BIND(C,name='mmb_dimensions_copy_toarray_f')
      IMPORT :: mmbErrorKind,mmbDimensions,mmbIndexKind
      INTEGER(mmbErrorKind) :: mmb_dimensionis_copy_toarray_i
      TYPE(mmbDimensions), intent(in),value :: in_dims
      INTEGER(mmbIndexKind), intent(out), dimension(*) :: out_dimsarr
    END FUNCTION mmb_dimensions_copy_toarray_i
    
    FUNCTION mmb_tile_iterator_create_i(mmb_array,tile_iterator) BIND(C,name='mmb_tile_iterator_create')
      IMPORT :: mmbErrorKind,mmbArray,mmbTileIterator
      INTEGER(mmbErrorKind) :: mmb_tile_iterator_create_i
      TYPE(mmbArray), intent(in),value :: mmb_array
      TYPE(mmbTileIterator), intent(out) :: tile_iterator
    END FUNCTION mmb_tile_iterator_create_i

    FUNCTION mmb_tile_iterator_destroy_i(tile_iterator) BIND(C,name='mmb_tile_iterator_destroy')
      IMPORT :: mmbErrorKind,mmbTileIterator
      INTEGER(mmbErrorKind) :: mmb_tile_iterator_destroy_i
      TYPE(mmbTileIterator), intent(in),value :: tile_iterator
    END FUNCTION mmb_tile_iterator_destroy_i

    FUNCTION mmb_tile_iterator_count_i(tile_iterator,count) BIND(C,name='mmb_tile_iterator_count')
      IMPORT :: mmbErrorKind,mmbTileIterator,mmbSizeKind
      INTEGER(mmbErrorKind) :: mmb_tile_iterator_count_i
      TYPE(mmbTileIterator), intent(in),value :: tile_iterator
      INTEGER(mmbSizeKind), intent(out) :: count
    END FUNCTION mmb_tile_iterator_count_i

    FUNCTION mmb_tile_iterator_first_i(tile_iterator,tile) BIND(C,name='mmb_tile_iterator_first')
      IMPORT :: mmbErrorKind,mmbTileIterator,C_mmbArrayTile
      INTEGER(mmbErrorKind) :: mmb_tile_iterator_first_i
      TYPE(mmbTileIterator), intent(in), value :: tile_iterator
      TYPE(C_mmbArrayTile), intent(out) :: tile
    END FUNCTION mmb_tile_iterator_first_i

    FUNCTION mmb_tile_iterator_next_i(tile_iterator,tile) BIND(C,name='mmb_tile_iterator_next')
      IMPORT :: mmbErrorKind,mmbTileIterator,C_mmbArrayTile
      INTEGER(mmbErrorKind) :: mmb_tile_iterator_next_i
      TYPE(mmbTileIterator), intent(in), value :: tile_iterator
      TYPE(C_mmbArrayTile), intent(out) :: tile
    END FUNCTION mmb_tile_iterator_next_i

    FUNCTION mmb_index_create_i(ndim,idx) BIND(C,name='mmb_index_create')
      IMPORT :: mmbErrorKind,mmbIndex,C_SIZE_T
      INTEGER(C_SIZE_T), intent(in), value :: ndim
      INTEGER(mmbErrorKind) :: mmb_index_create_i
      TYPE(mmbIndex), intent(out) :: idx
    END FUNCTION mmb_index_create_i

    FUNCTION mmb_index_destroy_i(in_idx) BIND(C,name="mmb_index_destroy")
      IMPORT :: mmbErrorKind,mmbIndex
      INTEGER(mmbErrorKind) :: mmb_index_destroy_i
      TYPE(mmbIndex), intent(in),value :: in_idx
    END FUNCTION mmb_index_destroy_i

    FUNCTION mmb_index_set_i(idx,id0) BIND(C,name='mmb_index_set')
      IMPORT :: mmbErrorKind,mmbIndex,C_SIZE_T
      INTEGER(mmbErrorKind) :: mmb_index_set_i
      TYPE(mmbIndex), intent(in), value :: idx
      INTEGER(C_SIZE_T), intent(in), value :: id0
    END FUNCTION mmb_index_set_i

    FUNCTION mmb_logging_set_level_i(level) BIND(C, name='mmb_logging_set_debug_level')
      IMPORT :: mmbErrorKind
      INTEGER(mmbErrorKind) :: mmb_logging_set_level_i
      INTEGER, intent(in),value :: level
    END FUNCTION mmb_logging_set_level_i

    FUNCTION mmb_tile_get_data_f(rank,tile,ptr,lower,upper,&
                                 alower,aupper,dim,abs_dim,is_contiguous) BIND(C)
      IMPORT :: mmbErrorKind,C_mmbArrayTile,C_ptr,C_int,C_Bool
      INTEGER(mmbErrorKind) :: get_tile_data_f
      INTEGER(C_int),intent(in),value :: rank
      TYPE(C_mmbArrayTile), intent(in), value :: tile
      TYPE(C_ptr), intent(out) :: ptr
      TYPE(C_ptr), intent(in),value :: lower
      TYPE(C_ptr), intent(in),value :: upper
      TYPE(C_ptr), intent(in),value :: alower
      TYPE(C_ptr), intent(in),value :: aupper
      TYPE(C_ptr), intent(in),value :: dim
      TYPE(C_ptr), intent(in),value :: abs_dim
      LOGICAL(C_Bool), intent(out) :: is_contiguous
    END FUNCTION mmb_tile_get_data_f

   END INTERFACE

 CONTAINS

   SUBROUTINE mmb_init (in_opts,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbOptions), intent(in), optional :: in_opts

     rerr = mmb_init_i(in_opts)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_init

   SUBROUTINE mmb_finalize (err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr

     rerr = mmb_finalize_i()
     if (present(err)) err = rerr
   END SUBROUTINE mmb_finalize

   SUBROUTINE mmb_memspace_config_create_default(config,err)
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbMemSpaceConfig_c), intent(out) :: config

     rerr = mmb_memspace_config_create_default_i(config)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_memspace_config_create_default
     
   ! new_space can be null
   SUBROUTINE mmb_register_memory(layer,ex_context,config_opts,new_space,err) 
     USE, INTRINSIC :: ISO_C_BINDING, only : C_loc
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(kind(MMB_MEMLAYER__MAX)),intent(IN),value :: layer
     INTEGER(kind(MMB_EXECUTION_CONTEXT__MAX)),intent(in), value :: ex_context
     TYPE(mmbMemSpaceConfig), intent(in), optional :: config_opts
     TYPE(mmbMemSpace), intent(out), optional :: new_space
     TYPE(mmbMemSpaceConfig_c) :: config_opts_c
     CHARACTER(KIND=C_CHAR), dimension(64),target :: name
     
     if (present(config_opts)) then
        config_opts_c%size_opts = config_opts%size_opts
        config_opts_c%interface_opts%provider = &
             config_opts%interface_opts%provider
        config_opts_c%interface_opts%provider_opts = &
             config_opts%interface_opts%provider_opts
        config_opts_c%interface_opts%strategy = &
             config_opts%interface_opts%strategy
!        config_opts_c%interface_opts%name = &
        !          trim(config_opts%interface_opts%name)//C_NULL_CHAR
        name = trim(config_opts%interface_opts%name)//C_NULL_CHAR
        config_opts_c%interface_opts%name = c_loc(name)
      else
       call mmb_memspace_config_create_default(config_opts_c)
      end if

     rerr = mmb_register_memory_i(layer,ex_context,config_opts_c,new_space)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_register_memory

   ! third arg can be null
   SUBROUTINE mmb_request_space(layer,ex_context,config_opts,new_space,err) 
     USE, INTRINSIC :: ISO_C_BINDING, only : C_loc
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(kind(MMB_MEMLAYER__MAX)),intent(in) :: layer
     INTEGER(kind(MMB_EXECUTION_CONTEXT__MAX)),intent(in) :: ex_context
     TYPE(mmbMemSpaceConfig), intent(in), optional :: config_opts
     TYPE(mmbMemSpaceConfig_c) :: config_opts_c
     TYPE(mmbMemSpace), intent(out) :: new_space

     if (present(config_opts)) then
        config_opts_c%size_opts = config_opts%size_opts
        config_opts_c%interface_opts%provider = &
             config_opts%interface_opts%provider
        config_opts_c%interface_opts%provider_opts = &
             config_opts%interface_opts%provider_opts
        config_opts_c%interface_opts%strategy = &
             config_opts%interface_opts%strategy
!        config_opts_c%interface_opts%name = &
!          trim(config_opts%interface_opts%name)//C_NULL_CHAR
       rerr = mmb_request_space_i(layer,ex_context,config_opts_c,new_space)
      else
        rerr = mmb_request_space_i(layer,ex_context,new_space=new_space)
      end if

      if (present(err)) err = rerr
   END SUBROUTINE mmb_request_space

   ! interface config_opts can be null
   SUBROUTINE mmb_request_interface(space,config_opts,new_interface,err) 
     IMPLICIT NONE
     TYPE(mmbMemSpace), intent(in),value :: space
     TYPE(mmbMemInterfaceConfig), intent(in), optional :: config_opts
     TYPE(mmbMemInterfaceConfig_c) :: config_opts_c
     TYPE(mmbMemInterface), intent(out) :: new_interface
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr

     if (present(config_opts)) then
        config_opts_c%provider = config_opts%provider
        config_opts_c%provider_opts = config_opts%provider_opts
        config_opts_c%strategy = config_opts%strategy
!        config_opts_c%name = trim(config_opts%name)//C_NULL_CHAR
        rerr = mmb_request_interface_i(space,config_opts_c,new_interface)
      else
        rerr = mmb_request_interface_i(space,new_interface=new_interface)
      end if

     if (present(err)) err = rerr
   END SUBROUTINE mmb_request_interface

   SUBROUTINE mmb_dimensions_create_fill(rank, dim_arr, out_dims,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbIndexKind), intent(in), value :: rank
     INTEGER(mmbIndexKind), intent(in), dimension(*) :: dim_arr
     TYPE(mmbDimensions), intent(out) :: out_dims

     rerr = mmb_dimensions_create_fill_i(rank, dim_arr, out_dims)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_dimensions_create_fill
      
   SUBROUTINE mmb_layout_create_regular_1d(element_size_bytes,padding,layout,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbSizeKind), intent(in), value :: element_size_bytes
     TYPE(mmbLayoutPadding), intent(in), value :: padding
     TYPE(mmbLayout), intent(out) :: layout

     rerr = mmb_layout_create_regular_1d_i(element_size_bytes,padding,layout)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_layout_create_regular_1d

   SUBROUTINE mmb_layout_create_regular_nd(element_size_bytes,n_dims,&
                                           element_order,padding,layout,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbSizeKind), intent(in), value :: element_size_bytes
     INTEGER(mmbIndexKind), intent(in), value :: n_dims
     INTEGER(kind(MMB_LAYOUT_ORDER_MAX)), intent(in), value :: element_order
     TYPE(mmbLayoutPadding), intent(in), value :: padding
     TYPE(mmbLayout), intent(out) :: layout

     rerr = mmb_layout_create_regular_nd_i(element_size_bytes,n_dims,&
                                           element_order,padding,layout)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_layout_create_regular_nd

   SUBROUTINE mmb_layout_destroy(in_layout,err) 
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbLayout), intent(in),value :: in_layout

     rerr = mmb_layout_destroy_i(in_layout)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_layout_destroy

   SUBROUTINE mmb_array_create(dims,layout,mem_interface,access,mmb_array,err) 
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbIndexKind), intent(in), dimension(:) :: dims
     TYPE(mmbLayout), intent(in),value :: layout
     TYPE(mmbMemInterface), intent(in), value :: mem_interface
     INTEGER(mmbAccessTypeKind), intent(in), value :: access
     TYPE(mmbArray), intent(out) :: mmb_array
     TYPE(mmbDimensions) :: c_dims

     call mmb_dimensions_create_fill(size(dims,1,mmbIndexKind),dims,c_dims,rerr)
     if (rerr.ne.MMB_OK) return
     if (present(err)) err = rerr
     rerr = mmb_array_create_i(c_dims,layout,mem_interface,access,mmb_array)
     if (rerr.ne.MMB_OK) return
     if (present(err)) then
        if (rerr.ne.MMB_OK) err = rerr
      end if
     rerr=mmb_dimensions_destroy_i(c_dims)
     if (present(err)) then
        if (rerr.ne.MMB_OK) err = rerr
      end if
     
   END SUBROUTINE mmb_array_create

   !deprecated
   SUBROUTINE mmb_array_create_wrapped_c_ptr(in_data,dims,layout,mem_interface,access,mmb_array,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(C_ptr), intent(in), value :: in_data
     TYPE(mmbDimensions), intent(in), value :: dims
     TYPE(mmbLayout), intent(in),value :: layout
     TYPE(mmbMemInterface), intent(in),value :: mem_interface
     INTEGER(mmbAccessTypeKind), intent(in), value :: access
     TYPE(mmbArray), intent(out) :: mmb_array

     rerr = mmb_array_create_wrapped_i(in_data,dims,layout,mem_interface,access,mmb_array)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_array_create_wrapped_c_ptr

   SUBROUTINE mmb_array_create_wrapped(in_data,dims,layout,mem_interface,access,mmb_array,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(*), dimension(..), intent(in) :: in_data
     INTEGER(mmbIndexKind), intent(in),dimension(:) :: dims
     TYPE(mmbLayout), intent(in),value :: layout
     TYPE(mmbMemInterface), intent(in),value :: mem_interface
     INTEGER(mmbAccessTypeKind), intent(in), value :: access
     TYPE(mmbArray), intent(out) :: mmb_array
     TYPE(mmbDimensions) :: c_dims

     call mmb_dimensions_create_fill(size(dims,1,mmbIndexKind),dims,c_dims,rerr)
     if (rerr.ne.MMB_OK) return
     if (present(err)) err = rerr
     rerr = mmb_array_create_wrapped_f(in_data,c_dims,layout,&
                                           mem_interface,access,mmb_array)

     if (rerr.ne.MMB_OK) return
     if (present(err)) then
        if (rerr.ne.MMB_OK) err = rerr
      end if
     rerr=mmb_dimensions_destroy_i(c_dims)
     if (present(err)) then
        if (rerr.ne.MMB_OK) err = rerr
      end if

   END SUBROUTINE mmb_array_create_wrapped

   SUBROUTINE mmb_array_tile(mmb_array,dims,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_NULL_PTR
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: mmb_array
     INTEGER(mmbIndexKind), intent(in),dimension(:),optional :: dims
     TYPE(mmbDimensions) :: c_dims
     
     if (present(dims)) then
        call mmb_dimensions_create_fill(size(dims,1,mmbIndexKind),dims,&
                                        c_dims,rerr)
        if (rerr.ne.MMB_OK) return
        if (present(err)) err = rerr
        rerr = mmb_array_tile_i(mmb_array,c_dims)
        if (rerr.ne.MMB_OK) return
        if (present(err)) then
           if (rerr.ne.MMB_OK) err = rerr
         end if
        rerr=mmb_dimensions_destroy_i(c_dims)
        if (present(err)) then
           if (err.ne.MMB_OK) err = rerr
         end if
     else
        rerr = mmb_array_tile_i(mmb_array,C_NULL_PTR)
     end if
     
     ! workaround for CCE 10.0.4 bug?
     if (present(err).and.(sqrt(3.0)>3.0)) print *,"mat rerr=",rerr
     if (present(err)) err = rerr

   END SUBROUTINE mmb_array_tile

   SUBROUTINE mmb_array_copy(dst,src,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: dst
     TYPE(mmbArray), intent(in),value :: src

     rerr = mmb_array_copy_i(dst,src)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_array_copy

   SUBROUTINE mmb_array_destroy(in_mba,err) 
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: in_mba

     rerr = mmb_array_destroy_i(in_mba)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_array_destroy
    
   SUBROUTINE mmb_array_untile(mmb_array,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: mmb_array

     rerr = mmb_array_untile_i(mmb_array)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_array_untile

   SUBROUTINE mmb_tile_duplicate(in_tile,in_interface,in_access, &
                                 in_layout,out_tile,err) 
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: in_tile
     TYPE(mmbMemInterface), intent(in), value :: in_interface
     INTEGER(mmbAccessTypeKind), intent(in), value :: in_access
     TYPE(mmbLayout), intent(in),value :: in_layout
     TYPE(mmbArrayTile), intent(out) :: out_tile

     rerr = mmb_tile_duplicate_i(in_tile%c_tile,in_interface,&
                                 in_access,in_layout,out_tile%c_tile)
     call mmb_tile_sync(out_tile)                                 
     if (present(err)) err = rerr

   END SUBROUTINE mmb_tile_duplicate
   
   SUBROUTINE mmb_tile_at(in_mba,in_idx,out_tile,err)
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArray), intent(in), value :: in_mba
     TYPE(mmbIndex), intent(in), value :: in_idx
     TYPE(mmbArrayTile), intent(out) :: out_tile

     rerr = mmb_tile_at_i(in_mba,in_idx,out_tile%c_tile)
     call mmb_tile_sync(out_tile)
     if (present(err)) err = rerr
     
    END SUBROUTINE mmb_tile_at

   SUBROUTINE mmb_tile_at_1d(in_mba,idx1,out_tile,err)
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArray), intent(in), value :: in_mba
     INTEGER(mmbIndexKind), intent(in), value :: idx1
     TYPE(mmbArrayTile), intent(out) :: out_tile

     rerr = mmb_tile_at_1d_i(in_mba,idx1-1,out_tile%c_tile)
     call mmb_tile_sync(out_tile)
     if (present(err)) err = rerr
     
    END SUBROUTINE mmb_tile_at_1d

   SUBROUTINE mmb_tile_at_2d(in_mba,idx1,idx2,out_tile,err)
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArray), intent(in), value :: in_mba
     INTEGER(mmbIndexKind), intent(in), value :: idx1
     INTEGER(mmbIndexKind), intent(in), value :: idx2
     TYPE(mmbArrayTile), intent(out) :: out_tile

     rerr = mmb_tile_at_2d_i(in_mba,idx1-1,idx2-1,out_tile%c_tile)
     call mmb_tile_sync(out_tile)
     if (present(err)) err = rerr
     
    END SUBROUTINE mmb_tile_at_2d

   SUBROUTINE mmb_tile_at_3d(in_mba,idx1,idx2,idx3,out_tile,err)
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArray), intent(in), value :: in_mba
     INTEGER(mmbIndexKind), intent(in), value :: idx1
     INTEGER(mmbIndexKind), intent(in), value :: idx2
     INTEGER(mmbIndexKind), intent(in), value :: idx3
     TYPE(mmbArrayTile), intent(out) :: out_tile

     rerr = mmb_tile_at_3d_i(in_mba,idx1-1,idx2-1,idx3-1,out_tile%c_tile)
     call mmb_tile_sync(out_tile)
     if (present(err)) err = rerr
     
    END SUBROUTINE mmb_tile_at_3d
    
   SUBROUTINE mmb_tile_get_space_local_handle(in_tile,out_tile,err)
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArrayTile), intent(in) :: in_tile
     TYPE(mmbArrayTile), intent(out) :: out_tile

     rerr = mmb_tile_get_space_local_handle_i(in_tile%c_tile,out_tile%c_tile)
     call mmb_tile_sync(out_tile)
     if (present(err)) err = rerr
     
    END SUBROUTINE mmb_tile_get_space_local_handle

   SUBROUTINE mmb_tile_merge(in_tile,in_merge,err) 
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArrayTile), intent(in) :: in_tile
     INTEGER(kind(MMB_MERGETYPE_MAX)), intent(in), value :: in_merge

     rerr = mmb_tile_merge_i(in_tile%c_tile,in_merge)
     ! TODO deallocate this (checked with Tim)
     if (present(err)) err = rerr
    END SUBROUTINE mmb_tile_merge

   SUBROUTINE mmb_tile_destroy(in_tile,err) 
     IMPLICIT none
     INTEGER(mmbErrorKind) :: rerr
     INTEGER(mmbErrorKind),intent(out),optional :: err
     TYPE(mmbArrayTile), intent(in) :: in_tile

     rerr = mmb_tile_destroy_i(in_tile%c_tile)
     ! TODO deallocate this (checked with Tim)
     if (present(err)) err = rerr
    END SUBROUTINE mmb_tile_destroy

   SUBROUTINE mmb_tile_count(mmb_array,count,err) 
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: mmb_array
     INTEGER(mmbSizeKind), intent(out) :: count

     rerr = mmb_tile_count_i(mmb_array,count)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_tile_count

   SUBROUTINE mmb_tiling_dimensions(mmb_array,out_dims,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_loc
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: mmb_array
     TYPE(mmbDimensions) tile_dims
     INTEGER(mmbSizeKind), intent(inout),allocatable :: out_dims(:)
     INTEGER(mmbSizeKind) :: count

     !print *,size(out_dims),out_dims
     rerr = mmb_tile_count_i(mmb_array,count)
     rerr = mmb_tiling_dimensions_i(mmb_array,tile_dims)

     if (present(err)) err = rerr

     if (count>=1) then
        ! allocate(out_dims(count)) 
        rerr = mmb_dimensions_copy_toarray_i(out_dims,tile_dims)
       end if

   END SUBROUTINE mmb_tiling_dimensions

   SUBROUTINE mmb_tile_iterator_create(mmb_array,tile_iterator,err) 
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArray), intent(in),value :: mmb_array
     TYPE(mmbTileIterator), intent(out) :: tile_iterator

     rerr = mmb_tile_iterator_create_i(mmb_array,tile_iterator)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_tile_iterator_create

   SUBROUTINE mmb_tile_iterator_destroy(tile_iterator,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbTileIterator), intent(in),value :: tile_iterator

     rerr = mmb_tile_iterator_destroy_i(tile_iterator)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_tile_iterator_destroy

   SUBROUTINE mmb_tile_iterator_count(tile_iterator,count,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbTileIterator), intent(in),value :: tile_iterator
     INTEGER(mmbSizeKind), intent(out) :: count

     rerr = mmb_tile_iterator_count_i(tile_iterator,count)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_tile_iterator_count

   SUBROUTINE mmb_tile_iterator_first(tile_iterator,tile,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbTileIterator), intent(in), value :: tile_iterator
     TYPE(mmbArrayTile), intent(out) :: tile

     rerr = mmb_tile_iterator_first_i(tile_iterator,tile%c_tile)
     call mmb_tile_sync(tile)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_tile_iterator_first

   SUBROUTINE mmb_tile_iterator_next(tile_iterator,tile,err)
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbTileIterator), intent(in), value :: tile_iterator
     TYPE(mmbArrayTile), intent(out) :: tile

     rerr = mmb_tile_iterator_next_i(tile_iterator,tile%c_tile)
     call mmb_tile_sync(tile)
     if (present(err)) err = rerr
   END SUBROUTINE mmb_tile_iterator_next

   SUBROUTINE mmb_tile_get_pointer_1d_r(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     real, pointer, dimension(:) :: tp
     
     ! There is a special case of >1d tile which is used for index support
     call c_f_pointer(tile%ptr,tp,[size(tile%abs_dim)])
     !print '("ptr=",z0)',tile_data%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_1d_r

   SUBROUTINE mmb_tile_get_pointer_1d_r8(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     real(dp), pointer, dimension(:) :: tp
     
     call c_f_pointer(tile%ptr,tp,[size(tile%abs_dim)])
     !print '("ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_1d_r8

   SUBROUTINE mmb_tile_get_pointer_1d_c(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     complex, pointer, dimension(:) :: tp
     
     call c_f_pointer(tile%ptr,tp,[size(tile%abs_dim)])
     !print '("ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_1d_c

   SUBROUTINE mmb_tile_get_pointer_1d_c16(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     complex(dp), pointer, dimension(:) :: tp
     
     call c_f_pointer(tile%ptr,tp,[size(tile%abs_dim)])
     !print '("ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_1d_c16

   SUBROUTINE mmb_tile_get_pointer_1d_i(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     integer, pointer, dimension(:) :: tp
     
     call c_f_pointer(tile%ptr,tp,[size(tile%abs_dim)])
     !print '("ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_1d_i

   SUBROUTINE mmb_tile_get_pointer_1d_i8(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     integer(i64), pointer, dimension(:) :: tp

     call c_f_pointer(tile%ptr,tp,[size(tile%abs_dim)])
     !print '("ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_1d_i8

   SUBROUTINE mmb_tile_get_pointer_2d_r(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     real, pointer, dimension(:,:) :: tp
     INTEGER tile_rank
     
     tile_rank = rank(tp)
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)
    
     !print '("mmb_tile_get_pointer_2d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_2d_r

   SUBROUTINE mmb_tile_get_pointer_2d_r8(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     real(dp), pointer, dimension(:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)

     !print '("mmb_tile_get_pointer_2d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_2d_r8

   SUBROUTINE mmb_tile_get_pointer_2d_c(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     complex, pointer, dimension(:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)
    
     !print '("mmb_tile_get_pointer_2d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_2d_c

   SUBROUTINE mmb_tile_get_pointer_2d_c16(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     complex(dp), pointer, dimension(:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)
    
     !print '("mmb_tile_get_pointer_2d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_2d_c16

   SUBROUTINE mmb_tile_get_pointer_2d_i(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     integer, pointer, dimension(:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)

     !print '("mmb_tile_get_pointer_2d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_2d_i

   SUBROUTINE mmb_tile_get_pointer_2d_i8(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     integer(i64), pointer, dimension(:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)

     !print '("mmb_tile_get_pointer_2d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_2d_i8

   SUBROUTINE mmb_tile_get_pointer_3d_r(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     real, pointer, dimension(:,:,:) :: tp
     INTEGER tile_rank
     
     tile_rank = rank(tp)
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)
    
     !print '("mmb_tile_get_pointer_3d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_3d_r

   SUBROUTINE mmb_tile_get_pointer_3d_r8(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     real(dp), pointer, dimension(:,:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)

     !print '("mmb_tile_get_pointer_3d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_3d_r8

   SUBROUTINE mmb_tile_get_pointer_3d_c(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     complex, pointer, dimension(:,:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)
    
     !print '("mmb_tile_get_pointer_3d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_3d_c

   SUBROUTINE mmb_tile_get_pointer_3d_c16(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     complex(dp), pointer, dimension(:,:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)
    
     !print '("mmb_tile_get_pointer_3d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_3d_c16

   SUBROUTINE mmb_tile_get_pointer_3d_i(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     integer, pointer, dimension(:,:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)

     !print '("mmb_tile_get_pointer_3d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_3d_i

   SUBROUTINE mmb_tile_get_pointer_3d_i8(tile,tp,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(in) :: tile
     integer(i64), pointer, dimension(:,:,:) :: tp
     
     call c_f_pointer(tile%ptr,tp,tile%abs_dim)

     !print '("mmb_tile_get_pointer_3d_r: ptr=",z0)',tile%ptr
     
   END SUBROUTINE mmb_tile_get_pointer_3d_i8
   
   ! Sync the Fortran tile data with the C tile
   SUBROUTINE mmb_tile_sync(tile,err)
     USE, INTRINSIC :: ISO_C_BINDING, only : C_loc,C_ptr,C_F_POINTER
     IMPLICIT NONE
     INTEGER(mmbErrorKind),intent(out),optional :: err
     INTEGER(mmbErrorKind) :: rerr
     TYPE(mmbArrayTile), intent(inout),target :: tile
     TYPE(C_ptr) ptr
     LOGICAL(C_Bool) c_is_contiguous
     INTEGER(mmbIndexKind) :: n_dims
     INTEGER :: tile_rank

     rerr = mmb_tile_get_n_dims_i(tile%c_tile,n_dims)
     tile_rank = n_dims  ! realistically this does not need to be size_t
     tile%rank = tile_rank
     if (.not.allocated(tile%lower) .or. &
          rank(tile%lower)>tile_rank) then
      allocate(tile%lower(tile_rank))
      allocate(tile%upper(tile_rank))
      allocate(tile%alower(tile_rank))
      allocate(tile%aupper(tile_rank))
      allocate(tile%dim(tile_rank))
      allocate(tile%abs_dim(tile_rank))
     end if
    rerr = mmb_tile_get_data_f(tile_rank,tile%c_tile,ptr,&
          c_loc(tile%lower), c_loc(tile%upper), &
          c_loc(tile%alower), c_loc(tile%aupper), &
          c_loc(tile%dim), c_loc(tile%abs_dim), &
          c_is_contiguous)
    tile%is_contiguous = c_is_contiguous
    tile%ptr = ptr
    if (present(err)) err = rerr
  END SUBROUTINE mmb_tile_sync

  SUBROUTINE mmb_index_create(ndim,idx,err)
    IMPLICIT NONE
    INTEGER(mmbIndexKind), intent(in) :: ndim
    TYPE(mmbIndex), intent(out) :: idx
    INTEGER(mmbErrorKind),intent(out),optional :: err
    INTEGER(mmbErrorKind) :: rerr

    rerr = mmb_index_create_i(ndim,idx)
    if (present(err)) err = rerr
  END SUBROUTINE mmb_index_create

  SUBROUTINE mmb_index_destroy(in_idx,err) 
    IMPLICIT NONE
    INTEGER(mmbErrorKind),intent(out),optional :: err
    INTEGER(mmbErrorKind) :: rerr
    TYPE(mmbIndex), intent(in),value :: in_idx

    rerr = mmb_index_destroy_i(in_idx)
    if (present(err)) err = rerr
  END SUBROUTINE mmb_index_destroy

  ! ToDO this only takes one value at the moment
  SUBROUTINE mmb_index_set(idx,id0,err)
    IMPLICIT NONE
    TYPE(mmbIndex), intent(in),value :: idx
    INTEGER(C_SIZE_T), intent(in) :: id0
    INTEGER(mmbErrorKind),intent(out),optional :: err
    INTEGER(mmbErrorKind) :: rerr

    rerr = mmb_index_set_i(idx,id0-1)
    if (present(err)) err = rerr

  END SUBROUTINE mmb_index_set
  
  SUBROUTINE mmb_logging_set_level(level,err)
    IMPLICIT NONE
    INTEGER, intent(in) :: level
    INTEGER(mmbErrorKind),intent(out),optional :: err
    INTEGER(mmbErrorKind) :: rerr

    rerr = mmb_logging_set_level_i(level)
    if (present(err)) err = rerr
  END SUBROUTINE mmb_logging_set_level
  
 END MODULE MAMBA
   
