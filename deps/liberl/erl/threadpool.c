/* -*- mode:c -*- */
/** @file
 ** @brief Thread pool infrastructure implementation of erl library
 **/
/*
 * Copyright (C) 2018 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE             /* See feature_test_macros(7) */

#include "erl_threadpool.h"
#include "utlist.h"
#include "erl_logging.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <sched.h>

/* we only support Linux-style numa interfacing at this time */
#ifdef HAVE_NUMA
#include <numa.h>
#include <numaif.h>
#endif

/** task status */
enum erl_task_status {
  ERL_TASK_INACTIVE = 1,
  ERL_TASK_ACTIVE,
  ERL_TASK_COMPLETE,
  ERL_TASK_ALLOCATED,
  ERL_TASK_POOLED,
  ERL_TASK_state__MAX
};

struct erl_tp_task_ {
  pthread_mutex_t    mtx;      /**< a per-task mutex */
  pthread_cond_t     cond;     /**< a condition variable to wait for the task */
  erl_tp_task_func   function; /**< the function to be run in the task */
  void              *closure;  /**< argument to be passed to the function */
  enum erl_task_status state;    /**< task state */
  /* task in threadpool queue are in a circular linked list. Durin
   * recycling they're just singly-linked (on a stack) */
  struct erl_tp_task_ *prev; /**< prev in utlist */
  struct erl_tp_task_ *next; /**< next in utlist */
};

/** lock the task structure */
erl_status
erl_task_lock(struct erl_tp_task_ *task)
{
  if(NULL==task)
    return ERL_INVARG;

  int stat = pthread_mutex_lock(&task->mtx);
  if(0!=stat)
    return ERL_FAIL;
  else
    return ERL_OK;
}

/** unlock the task structure */
erl_status
erl_task_unlock(struct erl_tp_task_ *task)
{
  if(NULL==task)
    return ERL_INVARG;

  int stat = pthread_mutex_unlock(&task->mtx);
  if(0!=stat)
    return ERL_FAIL;
  else
    return ERL_OK;
}

struct erl_threadpool_ {
  pthread_mutex_t mtx;                    /**< lock for the pool */
  size_t numthreads;                      /**< number of threads in this pool */
  enum erl_threadpool_type type;         /**< scheduling type */
  pthread_t *threads;                     /**< the threads */
  pthread_cond_t  cond;                   /**< condition variable for threads
                                           *  waiting on queue */
  struct erl_tp_task_ *queue;     /**< circular doubly-linked
                                           * task queue: queued and
                                           * not yet running */
  struct erl_tp_task_ *completed; /**< stack of tasks completed */
};

/** the cleanup handler */
static void
erl__pool_thread_func_cleanup_handler(void *arg)
{
  /* ERL_DEBUG("Cleanup handler called - thread %p canceled\n", pthread_self()); */

  struct erl_threadpool_ *pool = (struct erl_threadpool_ *)arg;
  /* We only push this clean-up handler when we hold the lock */
  pthread_mutex_unlock(&pool->mtx);

  /* ERL_DEBUG("Cleanup handler terminating for pool %p\n", pool); */

  return;
}

/** the task cleanup handler */
static void
erl__pool_thread_task_cleanup_handler(void *arg) {
 

  struct erl_tp_task_ *task = (struct erl_tp_task_ *)arg;
  /* We only push this clean-up handler when we hold the lock */
  pthread_mutex_unlock(&task->mtx);

  return;
}


static inline
struct erl_tp_task_ * erl__pool_thread_fetch_task(struct erl_threadpool_ *pool) {
  int s = 0;
  struct erl_tp_task_ *task;

  switch(pool->type) {
    case ERL_SCHED_FIFO:
      #if 0
      ERL_DEBUG("Pool: %p, queue %p, next %p, prev %p\n",
              pool, pool->queue,
              pool->queue->next, pool->queue->prev);
      {
        size_t len;
        struct erl_tp_task_ *tmp;
        CDL_COUNT(pool->queue,tmp,len);
        ERL_DEBUG("Pool queue %p length %d\n", pool->queue, len);
      }
      #endif
      task = pool->queue->prev;
      #if 0
        ERL_DEBUG("Pool queue %p selected task %p\n", pool->queue, task);
      #endif
        CDL_DELETE(pool->queue,task);
      #if 0
        ERL_DEBUG("Pool queue %p, deleted task %p\n", pool->queue, task);
        if(pool->queue!=NULL)
          ERL_DEBUG("Pool queue %p, prev %p, next %p\n",
              pool->queue->prev, pool->queue->next);
      #endif
      break;
    default:
      ERL_INFO("unsupported thread pool scheduling strategy: %d\n",pool->type);
      /* fallthrough */
    case ERL_SCHED_RANDOM:
      ERL_INFO("random threadpool not really random\n");
      /* fallthrough */
    case ERL_SCHED_LIFO:
      /* pop one */
      task = pool->queue;
      CDL_DELETE(pool->queue,task);
      task->next = NULL;
      break;
    }
  return task;
}

/* run task, locking it */
static inline
int erl__pool_thread_execute_task(struct erl_threadpool_ *pool, struct erl_tp_task_ *task) {
  int s = 0;
  
  ERL_INFO("Thread %p picked task %p\n", pthread_self(), task); 
  s=pthread_mutex_lock(&task->mtx);
  if(0!=s) {
    ERL_ERR("Failed to lock task %p: %d, dropping it\n", task, s);
    /* FIXME: maybe store in 'failed' list? */
  } else {
    pthread_cleanup_push(erl__pool_thread_task_cleanup_handler, task);
    task->next=NULL;
    task->prev=NULL;
    task->state=ERL_TASK_ACTIVE;
    erl_status task_status = task->function(task->closure, task);
    if(ERL_OK!=task_status) {
      ERL_INFO("Task %p completed with error: %d\n", task_status);
    }
    task->state=ERL_TASK_COMPLETE;
    /* ERL_DEBUG("Task %p completed\n", task); */

    /* store in 'completed' list */
    LL_PREPEND(pool->completed,task);

    s=pthread_cond_broadcast(&task->cond);
    if(0!=s) {
      ERL_ERR("Failed to broadcast for task %p observers: %d\n", task, s);
    }

    pthread_cleanup_pop(1);
  }
  return s;
}

/** the thread-in-pool executor function.
 **
 ** Waits on the condition variable of the pool, when signaled picks
 ** pops a task from the queue, executes it, and goes to sleep again.
 **
 ** Will be canceled when the pool gets destroyed.
 **/
static
void *
erl__pool_thread_func(void *arg)
{
  int s = 0; /*to hold status */
  struct erl_tp_task_ *task;
  struct erl_threadpool_ *pool = (struct erl_threadpool_ *)arg;

  /* ERL_DEBUG("Pool %p thread %p started\n", pool, pthread_self()); */
  /* ERL_DEBUG("Pool thread %p locking pool\n", pthread_self()); */
 

  /* Our thread startup may be very slow due to thread scheduling of
   * the system, so that the pool queue has already received the first
   * tasks before we get here. If that happens for all threads (like
   * on a pool with only 1 worker) we may live-lock here: the
   * cond_signal will have already long passed, and we'll never notice
   * that the queue has work.
   *
   * The check also allows us to be prepared for efficient on-demand
   * task spin-up later when the submit function notices that the
   * queue is constantly growing (i.e., not enough tasks to handle
   * them.
   *
   * The alternative would be to have a semaphore counted down so that
   * the pool init function would block until we're ready, but that
   * seems unnecessarily synchronous.
   */

  #ifdef DEBUG
  /* for debugging purpose */
  pthread_mutex_lock(&pool->mtx);
  /* unlock the mutex if we got cancelled while holding the lock */
  pthread_cleanup_push(erl__pool_thread_func_cleanup_handler, pool);
  if(NULL!=pool->queue) {
    size_t len;
    CDL_COUNT(pool->queue,task,len);
    ERL_DEBUG("Pool %p thread %p: starting up and queue already non-empty (len %d)\n",
          pool, pthread_self(), len);
  }
  pthread_cleanup_pop(1); /* we release the pool lock for the duration of the task execution */
  #endif

  do {
    pthread_mutex_lock(&pool->mtx);
    /* unlock the mutex if we got cancelled while holding the lock */
    pthread_cleanup_push(erl__pool_thread_func_cleanup_handler, pool);

    while (NULL==pool->queue) {
      /* ERL_DEBUG("Pool thread %p entering cond-wait\n", pthread_self()); */
      s=pthread_cond_wait(&pool->cond, &pool->mtx); /* standard cancellation point */
      if(0!=s) {
        ERL_ERR("Pool %p thread %p: failed to wait for work: %d\n",
          pool, pthread_self(), s);
      }
      /* woke up: check queue */
      ERL_DEBUG("woke up\n");
    }
    /* pool->queue != NULL */
    task = erl__pool_thread_fetch_task(pool);
    
    pthread_cleanup_pop(1); /* we release the pool lock for the duration of the task execution */
   

    s = erl__pool_thread_execute_task(pool, task);
    assert(s==0);
 
    pthread_testcancel();

  } while (1);
  
  return NULL;
}

/** the stack of reusable task structures We intentionally make task
 * structures reusable across task pools for now, so that we get good
 * recycling efficiency. If competition from pools to this pool
 * becomes an issue we could use a lock-less stack (instead of
 * distributing them to the pools).
 */
static
struct erl_tp_task_ *g_recycled_tasks = NULL;
/** a lock for g_recycled_tasks */
pthread_mutex_t g_recycling_lock = PTHREAD_MUTEX_INITIALIZER;

/** re-use a task from g_recycled_tasks, or allocate a new one.
 *   Ensure mutex and cond var in task structure are already
 *   initialized.  Returns NULL on failure.
 */
inline static
struct erl_tp_task_ *
erl__allocate_task(void)
{
  struct erl_tp_task_ *result = NULL;

  int s=pthread_mutex_lock(&g_recycling_lock);
  if(0!=s) {
    ERL_ERR("Failed to lock task recycling lock: %d\n");
    /* fall through to malloc case */
  } else {
    result = g_recycled_tasks;
    if(NULL!=g_recycled_tasks) {
      g_recycled_tasks=g_recycled_tasks->next;
      result->next = NULL;
    }
    s=pthread_mutex_unlock(&g_recycling_lock);
    if(0!=s) {
      ERL_ERR("Failed to unlock task recycling lock: %d\n");
    }
  }

  if(result==NULL) {
    result = malloc(sizeof(struct erl_tp_task_));
#if 0 /* enable to get error-checking mutex */
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERL_ERRORCHECK);
    int s=pthread_mutex_init(&result->mtx, &attr);
#else
    s=pthread_mutex_init(&result->mtx, NULL);
#endif
    if(0!=s) {
      ERL_ERR("Failed to init mutex in task %p: %d (%s)\n", result, s, strerror(s));
      free(result);
      result=NULL;
    } else {
      s=pthread_cond_init(&result->cond, NULL);
      if(0!=s) {
        ERL_ERR("Failed to init cond var in task %p: %d (%s)\n", result, s, strerror(s));
        pthread_mutex_destroy(&result->mtx);
        free(result);
        result=NULL;
      }
    }
  }

  return result;
}
/* actually free resources of task */
static inline
erl_status
erl__deallocate_task(struct erl_tp_task_ *task)
{
  erl_status stat=ERL_OK;
  if(NULL==task)
    return ERL_INVARG;

  assert(task->state!=ERL_TASK_INACTIVE);
  assert(task->state!=ERL_TASK_ACTIVE);

  int s=pthread_mutex_destroy(&task->mtx);
  if(0!=s) {
    ERL_ERR("Failed to destroy mutex of task %p: %d\n", task, s);
    stat=ERL_FAIL;
  }
  s=pthread_cond_destroy(&task->cond);
  if(0!=s) {
    ERL_ERR("Failed to destroy cond var or task %p: %d\n", task, s);
    stat=ERL_FAIL;
  }
  free(task);
  return stat;
}

static const
char * const erl_threadpool_type_name[ERL_sched__MAX] = {
  [0]                 = "INVALID",
  [ERL_SCHED_FIFO]   = "FIFO",
  [ERL_SCHED_LIFO]   = "LIFO",
  [ERL_SCHED_RANDOM] = "RAND",
};

/** We refcount active pools, if the count would go to 0 when
 * a pool is destroyed, we free the recycling queue
 */
static
size_t g_active_pool_counter = 0;
/** a lock for g_active_pool_counter */
pthread_mutex_t g_pool_counter_lock = PTHREAD_MUTEX_INITIALIZER;

static erl_status
erl__clear_task_recycle_queue(void)
{
  erl_status stat = ERL_OK;
  int s = pthread_mutex_lock(&g_recycling_lock);
  if(0!=s) {
    ERL_ERR("Failed to acquire recycling lock: %d\n", s);
    stat = ERL_FAIL;
  } else {
      struct erl_tp_task_ *el,*tmp;
      LL_FOREACH_SAFE(g_recycled_tasks,el,tmp) {
        LL_DELETE(g_recycled_tasks,el);
        stat = erl__deallocate_task(el);
        if(stat != ERL_OK){
          ERL_ERR("Failed to deallocate threadpool task\n");
          break;
        }
      }
      s=pthread_mutex_unlock(&g_recycling_lock);
      if(0!=s) {
        ERL_ERR("Failed to release recycling lock: %d\n");
        stat = ERL_FAIL;
      }
  }

  goto BAILOUT;
BAILOUT:
  return stat;
}

int
erl__get_binding_error(const struct erl_threadpool_attr *attr, int nnodes, int ncpus, int tid)
{
  ERL_ERR("Binding is set to `None`, therefore there is no index to obtain\n");
  return -1;
}
int
erl__get_binding_index_fixed(const struct erl_threadpool_attr *attr, int nnodes, int ncpus, int tid)
{
  return attr->bindings[tid];
}
int
erl__get_binding_index_rr1(const struct erl_threadpool_attr *attr, int nnodes, int ncpus, int tid)
{
  return attr->bindings[(tid % nnodes)];
}
int
erl__get_binding_index_rrall(const struct erl_threadpool_attr *attr, int nnodes, int ncpus, int tid)
{
  int cpus_per_node = ncpus / nnodes;
  int res = ((tid % nnodes) * cpus_per_node) + (tid % cpus_per_node);
  return res;
}


/** public API follows */
erl_status
erl_tp_create(size_t numthreads, const struct erl_threadpool_attr *attr,
              erl_threadpool *result_p)
{
  if(NULL==result_p)
    return ERL_INVOUT;
  *result_p = NULL;
  
  if(attr!=NULL && attr->stacksize != 0) {
    ERL_ERR("thread pool stack unimplemented\n");
    return ERL_UNIMPL;
  }
  
  erl_status stat=ERL_OK;
  int s;

  struct erl_threadpool_ *res = malloc(sizeof(struct erl_threadpool_));
  if(NULL==res) {
    stat=ERL_NOMEM;
    goto BAILOUT;
  }

  res->numthreads = numthreads>0 ? numthreads : 1;
  res->type = ERL_SCHED_DEFAULT;

  res->threads = malloc(sizeof(pthread_t)*res->numthreads);
  if(NULL==res->threads) {
    ERL_ERR("Failed to allocate thread storage\n");
    free(res);
    stat=ERL_NOMEM;
    goto BAILOUT;
  }
  res->queue = NULL;
  res->completed = NULL;

  pthread_mutexattr_t m_attr;
  pthread_mutexattr_init(&m_attr);

#if 0
  pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_ERL_ERRORCHECK);
#endif

  s = pthread_mutex_init(&res->mtx, &m_attr);
  if(0!=s) {
    ERL_ERR("Failed to init thread pool %p's mutex: %d (%s)\n",
            res, s, strerror(s));
    free(res->threads);
    free(res);
    stat=ERL_FAIL;
    goto BAILOUT;
  }

  s= pthread_cond_init(&res->cond, NULL);
  if(0!=s) {
    ERL_ERR("Failed to init thread pool %p's cond var: %d (%s)\n",
            res, s, strerror(s));
    pthread_mutex_destroy(&res->mtx);
    free(res->threads);
    free(res);
    stat=ERL_FAIL;
    goto BAILOUT;
  }

  /* spin up threads */
  if (attr == NULL || attr->bind == ERL_TP_BIND_NONE) {
    size_t i;
    for(i=0; i<res->numthreads; i++) {
      s|=pthread_create(&res->threads[i], NULL,
                        erl__pool_thread_func, res);
      /* threads run 'attached'. destroy will join them */
    }
  } else {
#ifdef HAVE_NUMA
    int nnodes = numa_num_task_nodes();
    int ncpus = numa_num_task_cpus();
    cpu_set_t cpus;
    pthread_attr_t th_attr;
    pthread_attr_init(&th_attr);
    size_t tid;
    for(tid=0; tid<res->numthreads; tid++) {
      CPU_ZERO(&cpus);
      CPU_SET((*erl__get_binding_index[attr->bind]) (attr, nnodes, ncpus, tid), &cpus);
      s |= pthread_attr_setaffinity_np(&th_attr, sizeof(cpu_set_t), &cpus);
      s |= pthread_create(&res->threads[tid],&th_attr,
                     erl__pool_thread_func, res);
    }
    pthread_attr_destroy(&th_attr);
#else /* no numa */
    pthread_cond_destroy(&res->cond);
    pthread_mutex_destroy(&res->mtx);
    free(res->threads);
    free(res);
    stat=ERL_UNIMPL;
    goto BAILOUT;
#endif

  }

  if(0!=s) {
    ERL_ERR("Failure in threads creation in pool %p: %d\n", res, s);
    pthread_cond_destroy(&res->cond);
    pthread_mutex_destroy(&res->mtx);
    free(res->threads);
    free(res);
    stat=ERL_FAIL;
    goto BAILOUT;
  }

  /* FIXME: could use an atomic counter instead */
  s = pthread_mutex_lock(&g_pool_counter_lock);
  if(0!=s) {
    ERL_ERR("Failed to lock erl active pool ref counter for increment: %d\n", s);
    pthread_cond_destroy(&res->cond);
    pthread_mutex_destroy(&res->mtx);
    free(res->threads);
    free(res);
    stat = ERL_FAIL;
    goto BAILOUT;
  }
  ++g_active_pool_counter;
  s = pthread_mutex_unlock(&g_pool_counter_lock);
  if(0!=s) {
    ERL_ERR("Failed to unlock erl active pool ref counter for increment: %d\n", s);
    pthread_cond_destroy(&res->cond);
    pthread_mutex_destroy(&res->mtx);
    free(res->threads);
    free(res);
    stat = ERL_FAIL;
    goto BAILOUT;
  }

  ERL_INFO("Spun up thread pool %p with %d threads, policy %s\n",
           res, res->numthreads,
           erl_threadpool_type_name[res->type]);

  *result_p = res;
  stat = ERL_OK;
  /* ERL_DEBUG("Created pool %p\n", res); */
 BAILOUT:
  return stat;
}

/** sleep time when trying to destroy a pool, but queue is not empty yet */
const struct timespec
ERL_DESTROY_WAIT = {0, (10*1000)}; /* 0s, 10*1000 nanoseconds */

/** Destroy a threadpool
 **/
/* We have a global pool counter, and if that reaches 0 we also free
   the tasks still on the the g_recycled_tasks stack.
*/
erl_status
erl_tp_destroy(erl_threadpool pool)
{
  erl_status stat = ERL_OK;

  /* ERL_DEBUG("cleaning up pool %p\n", pool); */
  if(NULL==pool)
    return ERL_INVARG;

  int s = pthread_mutex_lock(&pool->mtx);
  if(0!=s) {
    ERL_ERR("Failed to lock pool %p: %d\n", pool, s);
    return ERL_FAIL;
  }
  /* check that all tasks are completed */
  while(pool->queue!=NULL) {
    s = pthread_mutex_unlock(&pool->mtx);
    if(0!=s) {
      ERL_ERR("Failed to unlock pool %p: %d\n", pool, s);
      /* still try to go on */
      stat=ERL_FAIL;
    }
    ERL_INFO("pool destroy on pool %p when queue is not yet empty, waiting\n", pool);
    nanosleep(&ERL_DESTROY_WAIT,NULL);
    s = pthread_mutex_lock(&pool->mtx);
    if(0!=s) {
      ERL_ERR("Failed to lock pool %p: %d\n", pool, s);
      stat=ERL_FAIL;
      goto BAILOUT;
    }
  }
  /* queue now empty. User can still be waiting on the jobs, but they
   * own them, so we'll not free them */
  s = pthread_mutex_unlock(&pool->mtx);
  if(0!=s) {
    ERL_ERR("Failed to unlock pool mutex: %d\n", s);
  }

  /* cancel threads. They will acquire the mutex, their cleanup
   * handler will release it.  */
  size_t i;
  for(i=0; i<pool->numthreads; i++) {
    /* ERL_DEBUG("Sending cancel to job %p\n", pool->threads[i]); */
    s = pthread_cancel(pool->threads[i]);
    if(s!=0) {
      stat=ERL_FAIL;
      ERL_ERR("Failed to cancel thread %d in pool %p: %d\n", i, pool, s);
    }
  }
  if(i>0) {
    ERL_DEBUG("Outstanding jobs canceled\n");
  }

  /* join all threads. This ensures we wait for their cancelation */
  for(i=0; i<pool->numthreads; i++) {
    /* ERL_DEBUG("joing thread %p\n", pool->threads[i]); */
    s = pthread_join(pool->threads[i],NULL);
    if(0!=s) {
      ERL_ERR("Failed to join thread %d of pool %p: %d\n", i, pool, s);
      stat=ERL_FAIL;
    }
  }

  /* ERL_DEBUG("destroying cond var \n"); */
  /* clean up pthread things */
  s= pthread_cond_destroy(&pool->cond);
  if(s!=0) {
    stat=ERL_FAIL;
    ERL_ERR("Failed to destroy cond variable in pool %p: %d\n", pool, s);
  }

  /* ERL_DEBUG("destroying mutex\n"); */
  s= pthread_mutex_destroy(&pool->mtx);
  if(0!=s) {
    stat=ERL_FAIL;
    ERL_ERR("Failed to destroy mutex in pool %p: %d (%s)\n", pool, s,
        strerror(s));
  }
  free(pool->threads);
  /* ERL_DEBUG("pool %p content freed\n", pool); */
  free(pool);

  s = pthread_mutex_lock(&g_pool_counter_lock);
  if(0!=s) {
    ERL_ERR("Failed to lock erl active pool ref counter for decrement: %d\n", s);
    stat = ERL_FAIL;
    goto BAILOUT;
  }
  --g_active_pool_counter;
  /* ERL_DEBUG("active pool counter decremented at cleanup of poll %x, now %zu\n", */
  /*       pool, g_active_pool_counter); */
  if(g_active_pool_counter == 0){
    stat = erl__clear_task_recycle_queue();
    if(stat != ERL_OK)
      ERL_ERR("Failed to clear recycle queue during pool %p destruction\n",pool);
  }
  s = pthread_mutex_unlock(&g_pool_counter_lock);
  if(0!=s) {
    ERL_ERR("Failed to unlock erl active pool ref counter for decrement: %d\n", s);
    stat = ERL_FAIL;
    goto BAILOUT;
  }


BAILOUT:
  return stat;
}

erl_status
erl_tp_wait(struct erl_tp_task_ *job, void **closure)
{
  erl_status stat=ERL_OK;
  if(NULL==job)
    return ERL_INVARG;

  int s = pthread_mutex_lock(&job->mtx);
  if(0!=s) {
    ERL_ERR("Failed to lock job %p: %s\n", job, s);
    return ERL_FAIL;
  }

  while(ERL_TASK_COMPLETE!=job->state) {
    s=pthread_cond_wait(&job->cond, &job->mtx);
    if(0!=s) {
      ERL_ERR("Failed to condition-wait on job %p: %d\n", job, s);
      stat=ERL_FAIL;
      goto BAILOUT;
    }
  }
  /* completed */
  *closure = job->closure;

BAILOUT:
  s = pthread_mutex_unlock(&job->mtx);
  if(0!=s) {
    ERL_ERR("Failed to unlock job mutex %p: %d\n", job, s);
    stat=ERL_FAIL;
  }
  return stat;
}


erl_status
erl_tp_task_create(erl_tp_task_func function,
                 void *closure,
                 struct erl_tp_task_ **result_p)
{
  erl_status stat = ERL_OK;
  if(function==NULL)
    return ERL_INVARG;
  if(result_p==NULL)
    return ERL_INVOUT;

  struct erl_tp_task_ *res = erl__allocate_task();
  if(NULL==res)
    return ERL_NOMEM;

  res->state=ERL_TASK_ALLOCATED;
  res->function = function;
  res->closure = closure;
  res->next = NULL;

  *result_p = res;
  goto BAILOUT;
BAILOUT:
  return stat;
}

erl_status
erl_tp_task_destroy(struct erl_tp_task_ *task)
{
  if(NULL==task)
    return ERL_INVARG;
  int s=pthread_mutex_lock(&task->mtx);
  if(0!=s) {
    ERL_ERR("Failed to lock task %p mutex: %d\n", task, s);
  }

  switch(task->state) {
    case ERL_TASK_COMPLETE:
    case ERL_TASK_ALLOCATED:
    case ERL_TASK_POOLED: {
      task->state = ERL_TASK_POOLED;
      s=pthread_mutex_unlock(&task->mtx);
      if(0!=s) {
        ERL_ERR("Failed to unlock task mutex: %d\n", s);
        return ERL_FAIL;
      }
      s=pthread_mutex_lock(&g_recycling_lock);
      if(0!=s) {
        ERL_ERR("Failed to acquire recycling lock: %d\n", s);
        return erl__deallocate_task(task);
      } else {
        LL_PREPEND(g_recycled_tasks, task);
        s=pthread_mutex_unlock(&g_recycling_lock);
        if(0!=s) {
          ERL_ERR("Failed to release recycling lock: %d\n");
        }
        return ERL_OK;
      }
      break; /* ok */
    }
    case ERL_TASK_INACTIVE:
    case ERL_TASK_ACTIVE:
      ERL_ERR("Attempt to destroy task %p in state %d\n", task, task->state);
      break;
    default:
      ERL_ERR("Unexpected task state %d for task %p\n", task->state, task);
      break;
  }

  return ERL_FAIL;
}

erl_status
erl_tp_submit_task(erl_threadpool pool, struct erl_tp_task_ *task)
{
  if(NULL==pool || NULL==task)
    return ERL_INVARG;
  /* ERL_DEBUG("Submit task %p to pool %p, locking task\n", task, pool); */
  int s=pthread_mutex_lock(&task->mtx);

  if(0!=s) {
    ERL_ERR("Failed to lock task %p: %d\n", task, s);
  } else {
    if(task->state!=ERL_TASK_ALLOCATED) {
      ERL_ERR("Task %p has strange state: %d\n", task, task->state);
    }
    assert(task->state==ERL_TASK_ALLOCATED);
    task->state=ERL_TASK_INACTIVE;
    /* ERL_DEBUG("Locking pool %p\n"); */
    s=pthread_mutex_lock(&pool->mtx);
    if(0!=s) {
      ERL_ERR("Failed to lock pool %p: %d (%s)\n", pool, s, strerror(s));
      pthread_mutex_unlock(&task->mtx);
      goto BAILOUT;
    }
    switch(pool->type) {
      case ERL_SCHED_RANDOM:
      case ERL_SCHED_LIFO:
      case ERL_SCHED_FIFO: {
        /* if(pool->queue==NULL) { */
        /*   ERL_DEBUG("Pool %p queue %p, prepending task %p\n", */
        /*         pool, pool->queue, task); */
        /* } else { */
        /*   ERL_DEBUG("Pool %p queue %p, prev %, next %p, prepending task %p\n", */
        /*         pool, pool->queue, pool->queue->prev, pool->queue->next, */
        /*         task); */
        /* } */
        CDL_PREPEND(pool->queue,task);
        /* ERL_DEBUG("... Pool %p queue %p, prev %p, next %p\n", */
        /*       pool, pool->queue, pool->queue->prev, pool->queue->next */
        /*       ); */
        break;
      }
      default:
        ERL_ERR("Unsupported scheduling strategy %p\n", pool->type);
        break;
    }

    /* ERL_DEBUG("Prepended task %p to queue, unlocking task\n", pool); */
    s=pthread_mutex_unlock(&task->mtx);

    /* signal, for someone to pick it up, then unlock */
    /* ERL_DEBUG("Signaling pool availability\n"); */
    s=pthread_cond_signal(&pool->cond);
    if(0!=s) {
      ERL_ERR("Failed to signal pool %p condition var: %d\n", pool, s);
    }
    /* ERL_DEBUG("Unlocking pool %p mutex\n", pool); */

    s=pthread_mutex_unlock(&pool->mtx);
    if(0!=s) {
      ERL_ERR("Failed to unlock pool %p mutex: %d\n", pool, s);
    } else {
      return ERL_OK;
    }
  }

BAILOUT:
  return ERL_FAIL;
}




