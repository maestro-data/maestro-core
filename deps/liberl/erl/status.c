/*
 * Copyright (C) 2021 HPE
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -*- mode:c -*- */
/** @file
 ** @brief Status code implementation of erl library
 **
 **/
#include "erl_status.h"
#include <assert.h>

/** printable explanations for erl_status codes */
static
const char * const erl_status_table[ERL_status__MAX] = {
  [ERL_OK]          = "success",
  [ERL_UNIMPL]      = "unimplemented",
  [ERL_FAIL]        = "generic failure",
  [ERL_INVARG]      = "invalid argument",
  [ERL_INVOUT]      = "invalid output argument handle",
  [ERL_NOMEM]       = "out of memory",
  [ERL_ENOENT]      = "object or key doesnt exist",
  [ERL_EEXISTS]     = "key already exists",
  [ERL_NOSPACE]     = "no more space in container",
  [ERL_ERANGE]      = "object or size too large, would overflow",
  [ERL_UNSUPPORTED] = "transformation or transport unsupported",
  [ERL_UNINITIALIZED] = "component uninitialized",
};

const char *
erl_status_description(erl_status status)
{
  assert(status<ERL_status__MAX);
  return erl_status_table[status];
}
