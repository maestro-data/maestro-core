/*
 * Copyright (C) 2021 HPE
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -*- mode:c -*- */
/** @file
 ** @brief Logging infrastructure implementation of erl library
 **
 **/
#include "erl_logging.h"
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include <assert.h>
#include <string.h>
#include <unistd.h>

/** MIN of two numbers */
#define MIN(x, y) ((x) < (y) ? (x) : (y))

/** number of log levels */
#define ERL_log__MAX 5

/** Indicator characters for log lines to indicate message severity */
static const char *const erl_logging_labels_short[ERL_log__MAX] = {[ERL_LOG_ERR] = "E",
                                                       [ERL_LOG_WARN] = "W",
                                                       [ERL_LOG_INFO] = "I",
                                                       [ERL_LOG_DEBUG] = "D",
                                                       [ERL_LOG_NOISE] = "N"};

/** Indicator characters for log lines to indicate message severity */
static const char *const erl_logging_labels_full[ERL_log__MAX] = {[ERL_LOG_ERR] = "ERL_LOG_ERR",
                                                              [ERL_LOG_WARN] = "ERL_LOG_WARN",
                                                              [ERL_LOG_INFO] = "ERL_LOG_INFO",
                                                              [ERL_LOG_DEBUG] = "ERL_LOG_DEBUG",
                                                              [ERL_LOG_NOISE] = "ERL_LOG_NOISE"};

static pthread_mutex_t global_logging_mtx = PTHREAD_MUTEX_INITIALIZER;
#define LOCK_GLOBAL_LOGGING do {                                  \
    int _gflstat = pthread_mutex_lock(&global_logging_mtx);       \
    assert(_gflstat==0);                                          \
  } while(0);
 
#define UNLOCK_GLOBAL_LOGGING do {                                \
    int _gflstat = pthread_mutex_unlock(&global_logging_mtx);     \
    assert(_gflstat==0);                                          \
  } while(0);


erl_user_log_func g_erl_logging_func = &erl_location_aware_log;

/** debugging level
 *
 * Only messages with level <= g_debug_level will be printed
 *
 **/
static int g_debug_level = ERL_MAX_LOG_LEVEL;

static bool g_logging_initialized = false;
static bool g_log_level_set_from_env = false;
static char *g_env_debug_flag = NULL;

/* A default logging options object, which may be modified by accessors below */
erl_status erl_logging_options_create_default(erl_logging_options **opts) {
  erl_status stat = ERL_OK;
  if(!opts){
    ERL_ERR("erl_logging_options can not be NULL\n");
    stat = ERL_INVARG;
    goto BAILOUT;
  }
  erl_logging_options *l = malloc(sizeof(erl_logging_options));
  if(!l) {
    stat = ERL_NOMEM;
    goto BAILOUT;
  }

 l->max_log_lvl = ERL_LOG_WARN;
 l->user_log_func = NULL;

 *opts = l;

BAILOUT:
  return stat;
}

/* Accessors for logging options */
erl_status erl_logging_options_set_debug_level(erl_logging_options *opts, int level){
  erl_status stat = ERL_OK;
  if(!opts){
    ERL_ERR("erl_logging_options can not be NULL\n");
    stat = ERL_INVARG;
    goto BAILOUT;    
  }
  if (level < 0 || level >= ERL_log__MAX) {
    ERL_ERR("Attempt to set logging options debug level to invalid value %d\n", level);
    stat = ERL_INVARG;
    goto BAILOUT;   
  }
  opts->max_log_lvl = level;
BAILOUT:
  return stat;
}

erl_status erl_logging_options_set_user_log_func(erl_logging_options *opts, erl_user_log_func user_log_func){
  erl_status stat = ERL_OK;
  if(!opts){
    ERL_ERR("erl_logging_options can not be NULL\n");
    stat = ERL_INVARG;
    goto BAILOUT;
  }
  if (user_log_func == NULL) {
    ERL_ERR("Attempt to set logging function to NULL, must be a valid function\n");
    stat = ERL_INVARG;
    goto BAILOUT;
  }
  opts->user_log_func = user_log_func;
BAILOUT:
  return stat;
}

erl_status erl_logging_options_destroy(erl_logging_options *opts) {
  erl_status stat = ERL_OK;
  if (NULL == opts) {
    ERL_WARN("Logging options pointer invalid or already free'd.\n");
    stat = ERL_INVARG;
    goto BAILOUT;
  }
  free(opts);
BAILOUT:
  return stat;
}

/* Accessors for configuring logging infrastructure post-init */
erl_status erl_logging_set_debug_level(int level){
  erl_status stat = ERL_OK;
  if (level < 0 || level >= ERL_log__MAX) {
    ERL_ERR("Attempt to set debug level to invalid value %d\n", level);
    stat = ERL_INVARG;
    goto BAILOUT;
  }
  if(g_log_level_set_from_env) {
    ERL_WARN("Attempt to set debug level via API ignored due to ERL_LOG_LEVEL found in environment\n");
    goto BAILOUT;
  }
  g_debug_level = MIN(ERL_MAX_LOG_LEVEL, level);
BAILOUT:
  return stat;
}

erl_status erl_logging_set_user_log_func(erl_user_log_func user_log_func)
{
  erl_status stat = ERL_OK;
  if (user_log_func == NULL) {
    ERL_ERR("Attempt to set logging function to NULL, must be a valid function\n");
    stat = ERL_INVARG;
    goto BAILOUT;
  }
  g_erl_logging_func = user_log_func;
BAILOUT:
  return stat;
}

erl_status erl_logging_init(erl_logging_options *opts) {
  erl_status stat;
LOCK_GLOBAL_LOGGING;
  if(g_logging_initialized) {
    ERL_ERR("Logging infrastructure already initialized\n");
    stat = ERL_FAIL;
    goto BAILOUT;
  }

  erl_logging_options default_opts = {.max_log_lvl = ERL_LOG_WARN, .user_log_func = NULL};
  if(opts == NULL) {
    opts = &default_opts;
  }

  /* Parse API opts */
  stat = erl_logging_set_debug_level(opts->max_log_lvl);
  if(stat != ERL_OK)
    goto BAILOUT;
  if(opts->user_log_func) {
    stat = erl_logging_set_user_log_func(opts->user_log_func);
    if(stat != ERL_OK)
      goto BAILOUT;
  }

  /* Parse environment opts, which override API opts */
  /* ERL_LOG_LEVEL is not used during user-defined logging */
  if(!opts->user_log_func) {
    g_env_debug_flag = getenv("ERL_LOG_LEVEL");
    if (g_env_debug_flag != NULL) {
      int env_level = atoi(g_env_debug_flag);
      if (env_level < 0 || env_level >= ERL_log__MAX) {
        ERL_WARN("Attempt to set debug level to %s in environment, ignored\n",
              g_env_debug_flag);
      } else {
        g_debug_level = MIN(ERL_MAX_LOG_LEVEL, env_level);
        g_log_level_set_from_env = true;
      }
      ERL_INFO("Log level set to %d from environment\n", g_debug_level);
    }
  }
  g_logging_initialized = true;
BAILOUT:
  UNLOCK_GLOBAL_LOGGING;
  return stat;
}

erl_status erl_logging_finalize() {
  erl_status stat = ERL_OK;
  LOCK_GLOBAL_LOGGING;
  if(!g_logging_initialized) {
    ERL_ERR("Logging infrastructure not initialized\n");
    stat = ERL_FAIL;
    goto BAILOUT;
  }
  g_log_level_set_from_env = false;
  g_logging_initialized = false;
BAILOUT:
  UNLOCK_GLOBAL_LOGGING;
  return stat;
}


void erl_vlocation_aware_log(int level, const char *func, const char *file,
                             int line, const char *fmtstring, va_list argp) {
  assert(level >= 0);
  assert(level < ERL_log__MAX);

  if (level <= g_debug_level) {
    char header[1024] = "";
    snprintf(&header[0], 1024, "[%s] (%ld): %s(%s:%d) %s",
             erl_logging_labels_short[level], (long)getpid(), func,
             file, line, fmtstring);
    vfprintf(stderr, header, argp);
  }
}

void erl_location_aware_log(int level, const char *func, const char *file,
                            int line, const char *fmtstring, ...) {
  va_list ap;
  va_start(ap, fmtstring);
  erl_vlocation_aware_log(level, func, file, line, fmtstring, ap);
  va_end(ap);
}

const char* erl_logging_level_to_str(int level) {
  if(level >= ERL_log__MAX){
    ERL_ERR("liberl log level out of bounds\n");
    return NULL;
  }
  return erl_logging_labels_full[level];
}
