liberl: A set of utilities for ERL research software
======================================

This library contains shared implementations of utility infrastructure for C libraries developed by the HPE HPC/AI EMEA Research Lab (ERL). 

# How to build and run

```bash
autoreconf -i
../configure 
make;
make check;
```


# TODO

Infrastructure to be added/updated, in no particular order:

- Logging extension 
  - threading
  - possible sync with mamba/maestro
- Threadpool extension
  - compile-time swapout for alternatives such as q/argo threads
- Statistics 
- Recursive memlock (Maybe ?)
- ERL hash wrapper
  - Initially wrap k-hash, then extend where necessary
- ERL list wrapper 
  - Initial wrap ut or k list, then extend where necessary
