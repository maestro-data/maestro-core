/*
 * Copyright (C) 2018-2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

/* we rely on 'exit' not being wrapped */
#define CHEAT_NO_WRAP 1

#include "cheat.h"

#include "erl_threadpool.h"
#include <stdlib.h>
#include <unistd.h>

#ifdef HAVE_NUMA
#include <numa.h>
#include <numaif.h>
#endif

#ifdef HAVE_NUMA
#define NUM_THREADS_IN_POOL	10
#define NUM_TASKS_IN_POOL  	100
#else
#define NUM_THREADS_IN_POOL	5
#define NUM_TASKS_IN_POOL  	30
#endif


#define ARRAY_SIZE			10000000
#define REPEATS				10

CHEAT_DECLARE(
    int 
    get_fixed_binding(int nnodes, int ncpus, int tid)
    {
      return tid % ncpus;
    }

    erl_status
    taskfun(void* closure, erl_tp_task task) {
	/* do some work for some time so we can manually check actual CPU usage using `top` */
      double res;
      double* x = malloc(sizeof(double)*ARRAY_SIZE);
      cheat_assert(x != NULL);
      for(int j=0; j<REPEATS; j++) {
        x[0] = 0.1*(j%9+1);
        for(int i=0; i<ARRAY_SIZE-1; i++) 
          x[i+1]=1.5*x[i]*(1-x[i]);
      }
      res = x[ARRAY_SIZE-1];
      free(x);
      return ERL_OK + res; /* some garbage not to lose the loops to compiler optimization */
    }
              )

#ifdef HAVE_NUMA
CHEAT_DECLARE(bool g_numa_support = 1;)
#else
CHEAT_DECLARE(bool g_numa_support = 0;
              int numa_num_task_nodes(void) { return 1; }
              int numa_num_task_cpus(void)  { return 1; })
#endif
    
CHEAT_TEST(threadpool_run_binding,
           erl_threadpool pool;
           int nnodes = numa_num_task_nodes();
	   int ncpus =  numa_num_task_cpus();

           fprintf(stderr, "Working on %d NUMA nodes; %d total CPUs\n", nnodes, ncpus);
           struct erl_threadpool_attr* attr = malloc(sizeof(struct erl_threadpool_attr)
                                                     + sizeof(int) * NUM_THREADS_IN_POOL);
           cheat_assert(attr != NULL);

           attr->stacksize = 0; // default unset 
           attr->type = ERL_SCHED_DEFAULT;
           attr->bind = ERL_TP_BIND_FIXED;

           for (size_t i = 0; i<NUM_THREADS_IN_POOL; i++) {
             attr->bindings[i] = get_fixed_binding(nnodes, ncpus, i);
           }

           if(g_numa_support) {
             cheat_assert(ERL_OK==erl_tp_create(NUM_THREADS_IN_POOL,
                                                attr,&pool));
           } else {
             /* without NUMA support we can only do TP_BIND_NONE: */
             cheat_assert(ERL_UNIMPL==erl_tp_create(NUM_THREADS_IN_POOL,
                                                    attr,&pool));
             cheat_yield();
             attr->bind = ERL_TP_BIND_NONE;
             cheat_assert(ERL_OK==erl_tp_create(NUM_THREADS_IN_POOL,
                                                attr,&pool));
           }
           cheat_yield();
           
           cheat_assert(pool!=NULL);
           for(int i=0; i<NUM_TASKS_IN_POOL; i++) {
             erl_tp_task t;
             cheat_assert(ERL_OK==erl_tp_task_create(taskfun,NULL,&t));
             cheat_assert(ERL_OK==erl_tp_submit_task(pool,t));
             void *closure;
             cheat_assert(ERL_OK==erl_tp_wait(t,&closure));
             cheat_assert(ERL_OK==erl_tp_task_destroy(t));
           }

           cheat_assert(ERL_OK==erl_tp_destroy(pool));
           )


