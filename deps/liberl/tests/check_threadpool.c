/*
 * Copyright (C) 2018-2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

/* we rely on 'exit' not being wrapped */
#define CHEAT_NO_WRAP 1

#include "cheat.h"

#include "erl_threadpool.h"
#include <stdlib.h>

#define NUM_THREADS_IN_POOL  100
#define NUM_TASKS_IN_POOL  10000

CHEAT_DECLARE(
    erl_status
    taskfun(void* closure, erl_tp_task task) {
      return ERL_OK;
    }
              )
CHEAT_TEST(threadpool_run_nowait,
           erl_threadpool pool;
           cheat_assert(ERL_OK==erl_tp_create(NUM_THREADS_IN_POOL,
                                            ERL_TP_DEFAULT_ATTR,&pool));
           cheat_assert(pool!=NULL);
           for(int i=0; i<NUM_TASKS_IN_POOL; i++) {
             erl_tp_task t;
             cheat_assert(ERL_OK==erl_tp_task_create(taskfun,NULL,&t));
             cheat_assert(ERL_OK==erl_tp_submit_task(pool,t));
             void *closure;
             cheat_assert(ERL_OK==erl_tp_wait(t,&closure));
             cheat_assert(ERL_OK==erl_tp_task_destroy(t));
           }
           erl_tp_task tasks_phase_2[NUM_TASKS_IN_POOL];
           for(int i=0; i<NUM_TASKS_IN_POOL; i++) {
             erl_tp_task *t = &(tasks_phase_2[i]);
             cheat_assert(ERL_OK==erl_tp_task_create(taskfun,NULL,t));
             cheat_assert(ERL_OK==erl_tp_submit_task(pool,*t));
           }
           /* we don't wait on them. This checks
            * that destroy waits for work to complete and forces enough
            * work for threads to compete. */

           cheat_assert(ERL_OK==erl_tp_destroy(pool));
           /* now we can clean up without issues and without wait */
           for(int i=0; i<NUM_TASKS_IN_POOL; i++) {
             erl_tp_task t = tasks_phase_2[i];
             cheat_assert(ERL_OK==erl_tp_task_destroy(t));
           }
           )


