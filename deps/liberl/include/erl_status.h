/*
 * Copyright (C) 2021 HPE
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCL`ING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -*- mode:c -*- */
/** @file
 ** @brief Status code definitions and handling
 **
 **/
/** status codes */
#ifndef ERL_STATUS_H_
#define ERL_STATUS_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

/** Status codes reported by the library */
enum erl_status_enum {
  ERL_OK,      /**< operation successful */
  ERL_UNIMPL,  /**< routine not implemented */
  ERL_FAIL,    /**< generic operation failure */
  ERL_INVARG,  /**< invalid argument */
  ERL_INVOUT,  /**< invalid output destination argument */
  ERL_NOMEM,   /**< out of memory */
  ERL_ENOENT,  /**< object or key not found */
  ERL_EEXISTS, /**< object or key already exists */
  ERL_NOSPACE, /**< no more space in container */
  ERL_ERANGE,  /**< object or size too large, would overflow */
  ERL_UNSUPPORTED, /**< unsupported combination of transformation or movement */
  ERL_UNINITIALIZED, /**< component uninitialized */
  ERL_status__MAX
};

/** Library return value code type */
typedef enum erl_status_enum erl_status;

// /** descriptive strings for the error codes returned as @ref erl_status */
// const char *
// erl_status_description(erl_status s);

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif /* ERL_STATUS_H_ */
