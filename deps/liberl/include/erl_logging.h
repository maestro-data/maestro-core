/*
 * Copyright (C) 2021 HPE 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -*- mode:c -*- */
/** @file
 ** @brief Logging infrastructure
 **
 ** This header exposes an API to do sensible logging, from debugging
 ** to production level information
 **
 **/

#ifndef ERL_LOGGING_H_
#define ERL_LOGGING_H_ 1

#include <math.h>
#include <stdarg.h>
#include <stdio.h>

#include <erl_status.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup Logging Logging
 * @{
 *
 * @brief Simple logging API, from debugging to production level information
 *
 */
/** log level for errors */
#define ERL_LOG_ERR 0
/** log level for warnings */
#define ERL_LOG_WARN 1
/** log level for informational messages */
#define ERL_LOG_INFO 2
/** log level for debugging messages */
#define ERL_LOG_DEBUG 3
/** log level for really chatty logging */
#define ERL_LOG_NOISE 4

#ifndef ERL_MAX_LOG_LEVEL
/** compile-time log level cutoff */
#define ERL_MAX_LOG_LEVEL ERL_LOG_DEBUG
#endif

/* Default arg for logging options */
#define ERL_LOGGING_OPTIONS_DEFAULT = NULL

typedef void (*erl_user_log_func)(int level, const char *func, const char *file,
                    int line, const char *fmtstring, ...);  
typedef struct s_erl_logging_options {
  int max_log_lvl;
  erl_user_log_func user_log_func;
} erl_logging_options;

const char* erl_logging_level_to_str(int level);
erl_status erl_logging_options_create_default(erl_logging_options **opts);
erl_status erl_logging_options_set_debug_level(erl_logging_options*opts, int level);
erl_status erl_logging_options_set_user_log_func(erl_logging_options *opts, erl_user_log_func user_log_func);
erl_status erl_logging_options_destroy(erl_logging_options *opts);

erl_status erl_logging_set_debug_level(int level);
erl_status erl_logging_set_user_log_func(erl_user_log_func user_log_func);

erl_status erl_logging_init(erl_logging_options *opts);
erl_status erl_logging_finalize();

/** @brief core logging variadic wrapper
 */
void erl_location_aware_log(int level, const char *func, const char *file,
                    int line, const char *fmtstring, ...);

/** @brief core logging worker function
 */
void erl_vlocation_aware_log(int level, const char *func, const char *file,
                             int line, const char *fmtstring, va_list argp);
extern erl_user_log_func g_erl_logging_func;

/** @brief logging macro 
 */
#define ERL_LOG(level, format, ...)                                                \
  do {                                                                         \
    if (level <= ERL_MAX_LOG_LEVEL)                                            \
      g_erl_logging_func(level, __func__, __FILE__, __LINE__,                      \
                            format, ##__VA_ARGS__);                            \
} while (0)

/** @defgroup LogginMacros Logging Macros
 * @{
 *
 * @brief Intended API
 */

/** error messages */
#define ERL_ERR(format, ...) ERL_LOG(ERL_LOG_ERR, format, ##__VA_ARGS__)
/** warning messages */
#define ERL_WARN(format, ...) ERL_LOG(ERL_LOG_WARN, format, ##__VA_ARGS__)
/** informational messages */
#define ERL_INFO(format, ...) ERL_LOG(ERL_LOG_INFO, format, ##__VA_ARGS__)
/** debug messages */
#define ERL_DEBUG(format, ...) ERL_LOG(ERL_LOG_DEBUG, format, ##__VA_ARGS__)
/** chatty messages */
#define ERL_NOISE(format, ...) ERL_LOG(ERL_LOG_NOISE, format, ##__VA_ARGS__)

/** Convenience macro to check states and goto label **/
#define ERL_CHECK_STATUS(stat, message, label) \
  do {                                     \
    if(stat != ERL_OK) {                   \
      ERL_ERR(message);                        \
      goto label;                          \
    }                                      \
  } while (0)


/** @} (group macros) */

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif /* ERL_LOGGING_H_ */
