#!/usr/bin/env bash
# script to build protobuf and protobuf-c
#
# These should not really be needed for users or even normal
# developers of maestro, but if you change mstro_pool.proto you'll
# need protoc-c.
#
# Note that the compilation results will be installed into the source tree, not a build tree that you might be having if doing out-of-tree builds.

# Copyright (C) 2020 Cray Computer GmbH
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



SCRIPTDIR=`pwd`
UTILROOT=${SCRIPTDIR}/util

set -e

cd protobuf
./autogen.sh
./configure --prefix=${UTILROOT}
make install
cd ..
cd protobuf-c
./autogen.sh
# FIXME: detect CLE and add --use-ld flag
PKG_CONFIG_PATH=${UTILROOT}/lib/pkgconfig
export PKG_CONFIG_PATH
./configure --prefix=${UTILROOT} CC=${CC:cc} CXX=${CXX:c++} \
	    protobuf_CFLAGS="-I${UTILROOT}/include" \
	    prototobuf_LIBS="-L${UTILROOT}/lib -lprotobuf" \
	    PROTOC="${UTILROOT}/bin/protoc"
make
make install

echo "Be sure to 'make distclean' so that the toplevel configure notices that you now have protoc installed"      

			       
